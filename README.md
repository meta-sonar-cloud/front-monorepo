[![pipeline status](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/badges/develop/pipeline.svg)](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/-/commits/develop)


# Smartcoop Front Monorepo
> This monorepo contains web and mobile applications about Smartcoop solutions, developed with React and React Native, respectively.


Here are the packages that are applications into this monorepo: 

###### [`@smartcoop/app`](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/-/tree/develop/packages%2Fmobile%2Fapps%2FSmartcoopApp) - Owner mobile application
###### [`@smartcoop/tech-app`](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/-/tree/develop/packages%2Fmobile%2Fapps%2FSmartcoopTechApp) - Technical mobile application
###### [`@smartcoop/web-app`](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/-/tree/develop/packages%2Fweb%2Fapps%2FSmartcoopWebApp) - Web application


## Getting start

#### Clone and Install
After the repository was cloned, run:

```sh
$ yarn && yarn lerna bootstrap

# if you are using a macOS, run this command to install all pods dependencies
$ yarn lerna run install:ios --stream
```

## Documentation
All web and mobile components that was developed for this project have an example captured by `@smartcoop/doc` package. You can access the online documentation [here](http://localhost:9009).


## Running
#### @smartcoop/doc
```sh
# watch
$ yarn lerna run start --stream --scope @smartcoop/doc

# build
$ yarn lerna run build --stream --scope @smartcoop/doc
# artifacts path: doc/dist/
$ yarn lerna run serve --stream --scope @smartcoop/doc
# localhost:8000 
```

----

#### @smartcoop/app
```sh
# watch
$ yarn lerna run start --stream --scope @smartcoop/app --reset-cache
# the --reset-cache arg is optional

# another terminal:
# ios
$ yarn lerna run ios --stream --scope @smartcoop/app

# android
$ yarn lerna run android --stream --scope @smartcoop/app


# build - android only
$ yarn lerna run build:android --stream --scope @smartcoop/app
# artifacts path: packages/mobile/apps/SmartcoopApp/dist/
```

----

#### @smartcoop/web-app
```sh
# watch
$ yarn lerna run start --stream --scope @smartcoop/web-app
# localhost:3333 

# build
$ yarn lerna run build --stream --scope @smartcoop/web-app
# artifacts path: packages/web/apps/SmartcoopWebApp/build/
$ yarn lerna run serve --stream --scope @smartcoop/web-app
# localhost:8000 

```


### Cache
To clear the caches on mobile applications, you need uninstall the app from your device/emulator, and run the commands before install it again:

```sh
# android - if your device/emulator was connected but it is not found
$ adb kill-server && adb start-server && adb reverse tcp:8081 tcp:8081

$ yarn lerna run clean:watchman --stream

# android
$ yarn lerna run clean:android --stream

# ios - works on macOS only
$ yarn lerna run clean:ios --stream

# ALL clear commands in one - works on macOS only
$ yarn lerna run clean --stream
```


### Environment
All applications already have three environments to run: `development`, `staging` and `production`.

To use one specific env, you need pass the `--env` argument to `watch` or `build` command. The default value of `--env` is `development`.


#### Custom environment
To create your specific environment configuration, create an `.env.*.local` file into the package path, and run the `watch` or `build` command with the `--local` flag, where the `*` is equal to `development`, `staging` or `production`.

You can combine the `--env` with `--local` to run the applications with your environment configuration.
