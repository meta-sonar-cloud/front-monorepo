module.exports = {
  parser: 'babel-eslint',
  parserOptions: {
    ecmaVersion: 2017,
    sourceType: 'module',
    ecmaFeatures: {
      experimentalObjectRestSpread: true,
      jsx: true
    }
  },
  env: {
    browser: true,
    es2020: true,
    node: true,
    jest: true
  },
  extends: [
    'airbnb',
    'plugin:prettier/recommended',
    'plugin:react/recommended',
    'prettier/react',
    'react-app'
  ],
  plugins: [
    'react',
    'import-helpers',
    'prettier'
  ],
  rules: {
    'import-helpers/order-imports': [
      'error',
      {
        newlinesBetween: 'always',
        groups: [
          '/^react/',
          '/^redux/',
          'module',
          '/^lodash/',
          '/^@material-ui/',
          '/^@smartcoop/',
          '/^~/',
          ['parent', 'sibling', 'index']
        ],
        alphabetize: {
          order: 'asc',
          ignoreCase: true
        }
      }
    ],
    'prettier/prettier': 'off',
    'ts': 'off',
    'class-methods-use-this': 'off',
    'template-curly-spacing': [2, 'always'],
    'react/display-name': 'off',
    'react/forbid-prop-types': 'off',
    'react/jsx-curly-spacing': [2, 'always'],
    'react/jsx-filename-extension': 0,
    'react/jsx-props-no-spreading': 0,
    'react/jsx-closing-bracket-location': [1, 'tag-aligned'],
    'react/jsx-no-duplicate-props': [
      2,
      {
        ignoreCase: false
      }
    ],
    'no-underscore-dangle': 'off',
    'import/prefer-default-export': 0,
    'no-trailing-spaces': 'error',
    quotes: ['error', 'single'],
    semi: ['error', 'never'],
    'comma-dangle': ['error', 'never'],
    'import/no-extraneous-dependencies': 0,
    'object-curly-spacing': ['error', 'always'],
    'indent': ['warn', 2, { 'SwitchCase': 1 }],
    'arrow-body-style': ['error', 'as-needed'],
    'no-unused-vars': [
      'error',
      {
        vars: 'all',
        args: 'after-used',
        ignoreRestSiblings: true
      }
    ]
  },
  settings: {
    react: {
      version: 'detect'
    }
  },
  globals: {
    __DEV__: true
  }
}
