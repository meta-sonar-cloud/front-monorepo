const config = require('@smartcoop/eslint-config')

module.exports = {
  ...config,
  root: true
}
