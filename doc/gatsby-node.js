exports.onCreateWebpackConfig = ({ actions, loaders, getConfig, stage }) => {
  const config = getConfig()
  config.module.rules = [
    // Omit the default rule where test === '\.jsx?$'
    ...config.module.rules.filter(
      (rule) => String(rule.test) !== String(/\.jsx?$/)
    ),
    // Recreate it with custom exclude filter
    {
      // Called without any arguments, `loaders.js()` will return an
      // object like:
      // {
      //   options: undefined,
      //   loader: '/path/to/node_modules/gatsby/dist/utils/babel-loader.js',
      // }
      // Unless you're replacing Babel with a different transpiler, you probably
      // want this so that Gatsby will apply its required Babel
      // presets/plugins.  This will also merge in your configuration from
      // `babel.config.js`.
      ...loaders.js(),
      test: /\.jsx?$/,
      // Exclude all node_modules from transpilation, except for 'swiper' and 'dom7'
      exclude: (modulePath) =>
        /node_modules/.test(modulePath) &&
        // eslint-disable-next-line no-useless-escape
        !/node_modules[\/\\](@smartcoop|@meta-react|@meta-awesome|react-native-*|@highcharts)/.test(
          modulePath
        )
    }
  ]
  // This will completely replace the webpack config with the modified object.
  actions.replaceWebpackConfig(config)
  actions.setWebpackConfig({
    resolve: {
      alias: {
        // 'react-native-svg': 'react-svg',
        'react-native': 'react-native-web',
        '@smartcoop/mobile-components/Icon': '@smartcoop/web-components/Icon',
        '@smartcoop/mobile-components/Loader':
          '@smartcoop/web-components/Loader/Loader',
        '@smartcoop/mobile-components/PolygonToSvg':
          '@smartcoop/web-components/PolygonToSvg',
        '@smartcoop/mobile-components/Chart': '@smartcoop/web-components/Chart'
      }
    }
  })
  if (stage === 'build-html') {
    actions.setWebpackConfig({
      module: {
        rules: [
          {
            test: /react-json-view/,
            use: loaders.null()
          },
          {
            test: /leaflet/,
            use: loaders.null()
          },
          {
            test: /react-leaflet/,
            use: loaders.null()
          },
          {
            test: /@highcharts/,
            use: loaders.null()
          }
        ]
      }
    })
  }
}
