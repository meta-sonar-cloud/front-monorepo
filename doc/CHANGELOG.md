# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [0.18.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.17.0...v0.18.0) (2021-04-19)

**Note:** Version bump only for package @smartcoop/doc





# [0.17.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.16.0...v0.17.0) (2021-04-15)

**Note:** Version bump only for package @smartcoop/doc





# [0.16.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.15.0...v0.16.0) (2021-04-13)


### Features

* remove doc comments ([579a51f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/579a51f208bcec7864d60c63af7a746cfecefdbf))
* wip ([e0606ab](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e0606ab15f17990390f972120a71bef8ffb7304e))
* wip ([a1daf61](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a1daf6149af8d967fff4afe4009a7f2a116d8917))







**Note:** Version bump only for package @smartcoop/doc





# [0.15.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.12.0-alpha.1...v0.15.0) (2021-03-23)

**Note:** Version bump only for package @smartcoop/doc





# [0.15.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.12.0-alpha.1...v0.15.0) (2021-03-23)

**Note:** Version bump only for package @smartcoop/doc





# [0.15.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.12.0-alpha.1...v0.15.0) (2021-03-23)

**Note:** Version bump only for package @smartcoop/doc





# [0.14.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.12.0-alpha.1...v0.14.0) (2021-03-23)

**Note:** Version bump only for package @smartcoop/doc





# [0.13.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.12.0-alpha.1...v0.13.0) (2021-03-23)

**Note:** Version bump only for package @smartcoop/doc





# [0.12.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.12.0-alpha.1...v0.12.0) (2021-03-23)

**Note:** Version bump only for package @smartcoop/doc





# [0.12.0-alpha.1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.12.0-alpha.0...v0.12.0-alpha.1) (2021-02-22)

**Note:** Version bump only for package @smartcoop/doc





# [0.12.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.11.0...v0.12.0-alpha.0) (2021-02-12)

**Note:** Version bump only for package @smartcoop/doc





# [0.11.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.11.0-alpha.0...v0.11.0) (2021-02-12)

**Note:** Version bump only for package @smartcoop/doc





# [0.11.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.10.0...v0.11.0-alpha.0) (2021-02-05)

**Note:** Version bump only for package @smartcoop/doc





# [0.10.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.10.0-alpha.0...v0.10.0) (2021-02-01)

**Note:** Version bump only for package @smartcoop/doc





# [0.10.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.9.0...v0.10.0-alpha.0) (2021-01-19)

**Note:** Version bump only for package @smartcoop/doc





# [0.9.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.8.2...v0.9.0) (2021-01-19)

**Note:** Version bump only for package @smartcoop/doc





## [0.8.2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.8.1...v0.8.2) (2021-01-06)

**Note:** Version bump only for package @smartcoop/doc





## [0.8.1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.9.0-alpha.0...v0.8.1) (2021-01-06)

**Note:** Version bump only for package @smartcoop/doc





# [0.9.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.8.0...v0.9.0-alpha.0) (2021-01-05)

**Note:** Version bump only for package @smartcoop/doc





# [0.8.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.8.0-alpha.0...v0.8.0) (2021-01-05)


### Features

* **documentos#687:** created Chart component for mobile ([b0fba2b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b0fba2b4f1be4054ec927ed5dde77178bef16803)), closes [documentos#687](http://gitlab.meta.com.br/documentos/issues/687)





# [0.8.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.7.0...v0.8.0-alpha.0) (2020-12-11)

**Note:** Version bump only for package @smartcoop/doc





# [0.7.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.7.0-alpha.0...v0.7.0) (2020-12-11)

**Note:** Version bump only for package @smartcoop/doc





# [0.7.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.6.0...v0.7.0-alpha.0) (2020-11-27)


### Bug Fixes

* **docz:** fixed docz svg error ([ed5ba20](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ed5ba202d57b60b609039b29f3ef6f5d0181ed81))





# [0.6.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.5.3...v0.6.0) (2020-11-27)



# [0.6.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.5.0...v0.6.0-alpha.0) (2020-11-10)

**Note:** Version bump only for package @smartcoop/doc





# [0.6.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.5.0...v0.6.0-alpha.0) (2020-11-10)
## [0.5.3](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.5.2...v0.5.3) (2020-11-11)

**Note:** Version bump only for package @smartcoop/doc





## [0.5.2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.5.1...v0.5.2) (2020-11-11)

**Note:** Version bump only for package @smartcoop/doc





## [0.5.1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.5.0...v0.5.1) (2020-11-10)

**Note:** Version bump only for package @smartcoop/doc





# [0.5.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.5.0-alpha.0...v0.5.0) (2020-11-10)

**Note:** Version bump only for package @smartcoop/doc





# [0.5.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.4.0...v0.5.0-alpha.0) (2020-10-27)

**Note:** Version bump only for package @smartcoop/doc





# [0.4.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.3.0-alpha.0...v0.4.0) (2020-10-27)


### Bug Fixes

* **ci-cd:** echo ci variables ([c844a59](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c844a59fd79a9fe08a92d991f37b6dc1d64979d7))
* **ci-cd:** fixed build args ([71e3ae7](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/71e3ae71ea47606505946ffccff5296da197788c))
* **ci-cd:** fixed env for mobile and ci-cd pipeline ([8c10dd9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8c10dd9a6b1f88de7982f285315b0d8eaa2825b0))
* **ci-cd:** run jobs ([7f1a225](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7f1a225221e036e065c491c5ca06382f3a9e7675))


### Features

* **ci-cd:** created ci-cd ([95ea44c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/95ea44c9bbe5df17ded2cf836abe4744e5287814))





# [0.3.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.2.0...v0.3.0-alpha.0) (2020-09-28)

**Note:** Version bump only for package @smartcoop/doc




# [0.2.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.2.0-alpha.0...v0.2.0) (2020-09-28)


### Bug Fixes

* **doc:** css html overflow ([3fddc00](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3fddc00476390b1d344a626fc543eddc96b3f161))


### Features

* **documentos#10:** created web login screen ([34ed647](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/34ed647f1ab16e0c1dc45db8455239335ed6286d)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#32:** removed watermelon. added redux saga ([cd97772](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/cd977725490d8654afc2dad1a0e7bc3d6b16b4dd)), closes [documentos#32](http://gitlab.meta.com.br/documentos/issues/32)
* **documentos#57:** created modal component to web and mobile ([c3610ea](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c3610eabd1cecab013f46fe3c116d5f7cea00551)), closes [documentos#57](http://gitlab.meta.com.br/documentos/issues/57) [#18](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/18)





# [0.2.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.1.0...v0.2.0-alpha.0) (2020-09-13)

**Note:** Version bump only for package @smartcoop/doc





# [0.1.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.1.0-alpha.0...v0.1.0) (2020-09-13)


### Bug Fixes

* **forms:** fix validations ([7c413a5](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7c413a5c916867af845ec3726b4c3d2deae0ddec))


### Features

* **#10:** created web InputCpfCnpj with mask and validations ([a28957a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a28957ae1578d15e1d20395b6d3df7124638cdc1)), closes [#10](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/10) [#17](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/17)
* **documentos#10:** applying documentos[#37](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/37) into documentos[#39](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/39) ([c8e043e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c8e043ee97e72ea17b2ebd347852ea2cd999e755)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#10:** mobile inputs styled as material design ([8a9e3cc](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8a9e3cce883733f69190c3eb53ed92aba70e4026)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10) [#7](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/7)
* **documentos#33:** adds loader ([94ba79b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/94ba79b854af259764997a01b23a72d365512e17)), closes [documentos#33](http://gitlab.meta.com.br/documentos/issues/33) [documentos#33](http://gitlab.meta.com.br/documentos/issues/33)





# 0.1.0-alpha.0 (2020-08-21)

**Note:** Version bump only for package @smartcoop/web-docs
