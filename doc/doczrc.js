const { version } = require('@smartcoop/doc/package.json')
const colors = require('@smartcoop/styles/colors')

const mainNodeModules = '../node_modules'
const corePath = '../packages/core'
const mobileComponentsPath = '../packages/mobile/components'
const mobileContainersPath = '../packages/mobile/containers'
const webComponentsPath = '../packages/web/components'
const webContainersPath = '../packages/web/containers'

export default {
  files: [
    'src/pages/*.mdx',
    `${ corePath }/**/*.mdx`,
    `${ mobileComponentsPath }/**/*.mdx`,
    `${ mobileContainersPath }/**/*.mdx`,
    `${ webComponentsPath }/**/*.mdx`,
    `${ webContainersPath }/**/*.mdx`
  ],
  title: `@smartcoop frontend - v${ version }`,
  port: 9009,
  host: '0.0.0.0',
  dest: 'dist',
  menu: [
    'Getting Started',
    'Core',
    'Mobile Components',
    'Web Components'
  ],
  ignore: [
    `${ mobileComponentsPath }/node_modules`,
    `${ mobileContainersPath }/node_modules`,
    `${ webContainersPath }/node_modules`,
    `${ webComponentsPath }/node_modules`,
    `${ corePath }/*/node_modules`
  ],
  docgenConfig: {
    searchPatterns: [
      `${ corePath }/**/*.js`,
      `${ mobileComponentsPath }/**/*.js`,
      `${ mobileContainersPath }/**/*.js`,
      `${ webComponentsPath }/**/*.js`,
      `${ webContainersPath }/**/*.js`,
      `${ mainNodeModules }/@meta-*/**/*.js`
    ]
  },
  themeConfig: {
    fonts: {
      monospace: '\'Fira Code\''
    },
    fontSizes: [15, 13, 14, 14, 20, 24, 32, 48, 64],
    styles: {
      code: {
        fontFamily: 'monospace'
      },
      inlineCode: {
        fontFamily: 'monospace'
      },
      playground: {
      }
    },
    colors: {
      modes: {
        dark: {
          primary: colors.secondary,
          link: colors.secondary,
          sidebar: {
            navLinkActive: colors.secondary
          },
          props: {
            highlight: colors.secondary
          },
          header: {
            bg: colors.primary,
            text: colors.secondary,
            button: {
              bg: colors.backgroundHtml,
              color: colors.secondary
            }
          },
          playground: {
            bg: colors.white,
            color: colors.text
          }
        },
        light: {
          primary: colors.primary,
          link: colors.secondary,
          sidebar: {
            navLinkActive: colors.primary
          },
          props: {
            highlight: colors.primary
          },
          header: {
            bg: colors.backgroundHtml,
            text: colors.primary,
            button: {
              bg: colors.primary,
              color: colors.secondary
            }
          }
        }
      }
    }
  }
}
