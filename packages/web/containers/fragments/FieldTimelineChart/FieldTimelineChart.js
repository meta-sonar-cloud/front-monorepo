/* eslint-disable react/no-this-in-sfc */
import React, { useCallback, useState, useEffect, useRef } from 'react'

import moment from 'moment/moment'
import PropTypes from 'prop-types'

import { useT } from '@smartcoop/i18n'
import { createFieldTimelineChartOptions } from '@smartcoop/utils/charts'
import { momentFriendlyShortDateFormat, momentBackDateFormat } from '@smartcoop/utils/dates'
import Chart from '@smartcoop/web-components/Chart'
import InputDateRange from '@smartcoop/web-components/InputDateRange'


const FieldTimelineChart = ({ field }) => {
  const t = useT()

  const mounted = useRef(false)

  const [dates, setDates] = useState({
    from: moment().startOf('year').format(momentBackDateFormat),
    to: null
  })
  const [chartOptions, setChartOptions] = useState({})

  const getChartOptions = useCallback(
    async (firstTime = false) => {
      const options = await createFieldTimelineChartOptions({
        fieldId: field.id,
        dates: firstTime ? { from: null, to: null } : dates,
        t
      })
      if (mounted.current) {
        setChartOptions(options)
        if(firstTime) {
          setDates({
            from: moment(options.xAxis.categories[0], momentFriendlyShortDateFormat).format(momentBackDateFormat),
            to: moment(options.xAxis.categories.slice(-1)[0], momentFriendlyShortDateFormat).format(momentBackDateFormat)
          })
        }
      }
    },
    [dates, field.id, t]
  )

  useEffect(
    () => {
      if (dates.to) {
        getChartOptions()
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [dates.to]
  )

  useEffect(
    () => {
      getChartOptions(true)
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    []
  )

  useEffect(() => {
    mounted.current = true
    return () => {
      mounted.current = false
    }
  }, [])

  return (
    <div>
      <div style={ { padding: '0 20px' } }>
        <InputDateRange
          detached
          label={ t('period') }
          name="period"
          value={ dates }
          minWidth={ 250 }
          onChange={ setDates }
          style={ { marginBottom: 0 } }
        />
      </div>
      <Chart options={ chartOptions } />
    </div>
  )
}

FieldTimelineChart.propTypes = {
  field: PropTypes.object.isRequired
}

export default FieldTimelineChart
