import React, { useMemo } from 'react'

import moment from 'moment/moment'
import PropTypes from 'prop-types'

import map from 'lodash/map'

import Divider from '@material-ui/core/Divider'

import I18n, { useT } from '@smartcoop/i18n'
import { shoppingCart } from '@smartcoop/icons'
import {
  momentFriendlyDateFormat,
  momentBackDateFormat
} from '@smartcoop/utils/dates'
import Icon from '@smartcoop/web-components/Icon'
import SupplierQuotationStatus from '@smartcoop/web-components/SupplierQuotationStatus'

import {
  OrderContainer,
  PaymentContainer,
  OrderSubTitleItem,
  OrderItem,
  PaymentItem,
  OrderItemText,
  CardStyled,
  ItemStatus,
  ContainerDetails
} from './styles'

const SupplierQuotationDetailsFragment = ({ currentDemand, currentOrganization }) => {
  const t = useT()

  const productUnit = useMemo(() => currentDemand.unitOfMeasures || 'kg', [
    currentDemand.unitOfMeasures
  ])

  const orderData = useMemo(
    () => [
      {
        subtitle: 'product',
        value: currentDemand.product.shortDescription || ''
      },
      {
        subtitle: 'aggregated volume',
        value:
          `${ Number(currentDemand.aggregatedVolume).toLocaleString(
            'pt-BR'
          ) } ${ productUnit }` || ''
      },
      {
        subtitle: 'proposal limit',
        value:
          currentDemand.proposalDeadline &&
          moment(currentDemand.proposalDeadline, momentBackDateFormat).format(
            momentFriendlyDateFormat
          )
      },
      {
        subtitle: 'delivery limit',
        value:
          currentDemand.receiptDeadline &&
          moment(currentDemand.receiptDeadline, momentBackDateFormat).format(
            momentFriendlyDateFormat
          )
      }
    ],
    [currentDemand.aggregatedVolume, currentDemand.product.shortDescription, currentDemand.proposalDeadline, currentDemand.receiptDeadline, productUnit]
  )

  const paymentMethods = useMemo(
    () =>
      map(
        currentDemand.paymentForms,
        ({ deferredPayment, daysAmount }, index) => ({
          value: `${ index + 1 }. ${
            !deferredPayment
              ? t('in cash')
              : `${ t('deferred') } ${ daysAmount } ${ t('day', { howMany: 2 }) }`
          }`
        })
      ),
    [currentDemand.paymentForms, t]
  )

  const proposalStatus = useMemo( () => {
    const actualPurchase = currentDemand.purchaseProposals.find((item) => item.supplierId === currentOrganization.id)

    return actualPurchase ? actualPurchase.status : { slug:'aguardando_proposta', statusName:'Aguardando proposta' }
  }, [currentDemand.purchaseProposals, currentOrganization])

  return (
    <CardStyled
      headerLeft={ <Icon icon={ shoppingCart } size={ 24 } /> }
      title={ t('purchase quotes') }
    >
      <ContainerDetails>
        <Divider />
        <OrderContainer>
          {map(orderData, ({ subtitle, value }) => (
            <OrderItem key={ subtitle }>
              <OrderSubTitleItem>{t(subtitle)}</OrderSubTitleItem>
              <OrderItemText>{value}</OrderItemText>
            </OrderItem>
          ))}
          <ItemStatus>
            <OrderSubTitleItem>Situação da Proposta</OrderSubTitleItem>
            {proposalStatus && (
              <SupplierQuotationStatus
                style={ { padding: '10px 15px', fontSize: 14 } }
                slug={ proposalStatus.slug }
              />
            )}
          </ItemStatus>
        </OrderContainer>
        <PaymentContainer>
          <OrderSubTitleItem>
            <I18n>payment methods</I18n>
          </OrderSubTitleItem>
          <PaymentItem>
            {map(paymentMethods, ({ value }) => (
              <OrderItemText key={ value }>{value}</OrderItemText>
            ))}
          </PaymentItem>
        </PaymentContainer>
      </ContainerDetails>
    </CardStyled>
  )
}

SupplierQuotationDetailsFragment.propTypes = {
  currentDemand: PropTypes.object.isRequired,
  currentOrganization: PropTypes.object.isRequired
}

export default SupplierQuotationDetailsFragment
