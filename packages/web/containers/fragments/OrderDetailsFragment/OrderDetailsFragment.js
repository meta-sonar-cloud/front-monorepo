import React, { useState, useEffect, useMemo } from 'react'
import { useSelector } from 'react-redux'
import { useHistory, useParams } from 'react-router-dom'

import moment from 'moment/moment'
import PropTypes from 'prop-types'

import find from 'lodash/find'
import isEmpty from 'lodash/isEmpty'
import map from 'lodash/map'

import I18n, { useT } from '@smartcoop/i18n'
import { shoppingCart, organizationRounded } from '@smartcoop/icons'
import { selectUserHaveAdministration } from '@smartcoop/stores/authentication/selectorAuthentication'
import {
  selectCurrentOrder,
  selectCurrentOrderProduct
} from '@smartcoop/stores/order/selectorOrder'
import { selectCurrentOrganization } from '@smartcoop/stores/organization/selectorOrganization'
import {
  momentFriendlyDateFormat,
  momentBackDateFormat
} from '@smartcoop/utils/dates'
import { formatCpfCnpj, formatNumber } from '@smartcoop/utils/formatters'
import CardAccordion from '@smartcoop/web-components/CardAccordion'
import Icon from '@smartcoop/web-components/Icon'
import { useRoutes } from '@smartcoop/web-containers/routes/authenticated'

import {
  Title,
  IconContainer,
  Divider,
  Header,
  OrderContainer,
  OrderSubTitleItem,
  OrderItem,
  OrderItemText,
  OrganizationsContainer,
  AccordionTitle,
  AccordionItems,
  AccordionItem,
  OrganizationItem
} from './styles'

const OrderDetailsFragment = ({ organizationsVariant }) => {
  const history = useHistory()
  const t = useT()
  const { routes } = useRoutes()

  const currentOrder = useSelector(selectCurrentOrder)
  const product = useSelector(selectCurrentOrderProduct)
  const currentOrganization = useSelector(selectCurrentOrganization)
  const userIsAdmin = useSelector(selectUserHaveAdministration)

  const [verifiedCurrentOrder, setVerifiedCurrentOrder] = useState(false)
  const { orderId } = useParams()

  const productUnit = useMemo(() => product.unitOfMeasures || 'kg', [product])

  const currentOrganizationPurchase = useMemo(
    () =>
      find(
        currentOrder.organizationPurchases,
        ({ organizationId }) => organizationId === currentOrganization.id
      ),
    [currentOrder.organizationPurchases, currentOrganization.id]
  )

  const orderData = useMemo(
    () => [
      {
        subtitle: 'product',
        value: product.description || ''
      },
      {
        subtitle: 'aggregated volume',
        value:
          `${ formatNumber(currentOrder.aggregatedVolume) } ${ productUnit }` || ''
      },
      {
        subtitle: 'opening date',
        value:
          currentOrder.createdAt &&
          moment(currentOrder.createdAt, momentBackDateFormat).format(
            momentFriendlyDateFormat
          )
      },
      {
        subtitle: 'expiration date',
        value:
          currentOrder.participationDeadline &&
          moment(
            currentOrder.participationDeadline,
            momentBackDateFormat
          ).format(momentFriendlyDateFormat)
      }
    ],
    [
      currentOrder.aggregatedVolume,
      currentOrder.createdAt,
      currentOrder.participationDeadline,
      product.description,
      productUnit
    ]
  )

  const organizations = useMemo(
    () =>
      map(currentOrder.organizationPurchases, (item) => ({
        value: item.organization.tradeName || item.organization.companyName,
        cnpj: formatCpfCnpj(item.organization.companyDocument),
        totalAmount: item.totalAmount,
        deliveryAmount: item.deliveryLocations.length
      })),
    [currentOrder.organizationPurchases]
  )

  const paymentMethods = useMemo(
    () =>
      map(
        currentOrder.paymentForms,
        ({ deferredPayment, daysAmount }, index) => ({
          value: `${ index + 1 }. ${
            !deferredPayment
              ? t('in cash')
              : `${ t('deferred') } ${ daysAmount } ${ t('day', { howMany: 2 }) }`
          }`
        })
      ),
    [currentOrder.paymentForms, t]
  )

  useEffect(() => {
    if (orderId) {
      setVerifiedCurrentOrder(false)
    }
  }, [orderId])

  useEffect(() => {
    if (!isEmpty(currentOrder) && !verifiedCurrentOrder) {
      const isParticipating = currentOrganizationPurchase
      const path = routes.joinOrder.path.replace(':orderId', orderId)
      if (!isParticipating && !userIsAdmin) {
        history.replace(path)
      } else {
        setVerifiedCurrentOrder(true)
      }
    }
  }, [
    currentOrder,
    verifiedCurrentOrder,
    currentOrganization.id,
    history,
    orderId,
    routes.joinOrder,
    currentOrganizationPurchase,
    organizations,
    userIsAdmin
  ])

  return (
    <>
      <Header style={ { paddingLeft: 30 } }>
        <IconContainer>
          <Icon icon={ shoppingCart } size={ 18 } />
        </IconContainer>

        <Title style={ { fontSize: 16, fontWeight: 600 } }>
          <I18n>total purchase order</I18n>
        </Title>
      </Header>
      <OrderContainer>
        {map(orderData, ({ subtitle, value }) => (
          <OrderItem key={ subtitle }>
            <OrderSubTitleItem>{t(subtitle)}</OrderSubTitleItem>
            <OrderItemText>{value}</OrderItemText>
          </OrderItem>
        ))}
      </OrderContainer>
      <OrderItem>
        <OrderSubTitleItem>
          <I18n>payment methods</I18n>
        </OrderSubTitleItem>
        {map(paymentMethods, ({ value }) => (
          <OrderItemText key={ value }>{value}</OrderItemText>
        ))}
      </OrderItem>
      <Divider />
      <OrganizationsContainer>
        <Header>
          <Title style={ { fontSize: 16, fontWeight: 600, paddingLeft: 15 } }>
            <I18n>participating organizations</I18n>
          </Title>
        </Header>
        {organizationsVariant === 'default'
          ? map(organizations, ({ value }) => (
            <OrganizationItem key={ value } style={ { paddingLeft: 30 } }>
              <Icon icon={ organizationRounded } size={ 24 } />
              <OrderItemText style={ { fontSize: 14, paddingLeft: 8 } }>
                {value}
              </OrderItemText>
            </OrganizationItem>
          ))
          : map(
            organizations,
            ({ value, cnpj, deliveryAmount, totalAmount }) => (
              <OrganizationItem
                key={ value }
                style={ { paddingLeft: 30, paddingRight: 30 } }
              >
                <CardAccordion
                  title={ <AccordionTitle>{value}</AccordionTitle> }
                  headerLeft={ <Icon icon={ organizationRounded } size={ 20 } /> }
                >
                  <AccordionItems>
                    <AccordionItem>CNPJ: {cnpj}</AccordionItem>
                    <AccordionItem>{`${ t('total volume') }: ${ formatNumber(
                      totalAmount
                    ) } ${ productUnit }`}</AccordionItem>
                    <AccordionItem>{`${ t(
                      'delivery in'
                    ) }: ${ deliveryAmount } ${ t('place', {
                      howMany: deliveryAmount
                    }) }`}</AccordionItem>
                  </AccordionItems>
                </CardAccordion>
              </OrganizationItem>
            )
          )}
      </OrganizationsContainer>
    </>
  )
}

OrderDetailsFragment.propTypes = {
  organizationsVariant: PropTypes.oneOf(['default', 'accordion'])
}

OrderDetailsFragment.defaultProps = {
  organizationsVariant: 'default'
}

export default OrderDetailsFragment
