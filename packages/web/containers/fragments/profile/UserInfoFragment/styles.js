import styled from 'styled-components'

export const Container = styled.div`
  display: flex;
  /* background: tomato; */
  margin: 40px 180px 40px 120px;
  flex-direction: column;
  flex: 1;
`

export const RowContainer = styled.div`
  display: flex;
  flex-direction: row;
`

export const Item = styled.div`
  margin: 20px;
  margin-left: 0;
  margin-bottom: 0;
  width: 400px;
`

export const Top = styled.div`
  display: flex;
  justify-content: space-between;
`

export const InfoContainer = styled.div`
  display: flex;
  flex-direction: column;
  flex: 1;
`

export const AvatarContainer = styled.div`
  padding: 20px;
  padding-top: 0;
  display: flex;
  justify-content: center;
  align-items: center;
`

export const AvatarNameContainer = styled.div`
`

export const ButtonContainer = styled.div`
`

export const FirstRowContainer = styled(RowContainer)`
  justify-content: flex-start;
`

export const Text = styled.span`
  font-size: 16px;
`

export const Name = styled(Text)`
  font-weight: 700;
  font-size: 17px;
`
