import React, { useMemo, useCallback, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import moment from 'moment/moment'
import PropTypes from 'prop-types'

import { useDialog } from '@smartcoop/dialog'
import I18n, { useT } from '@smartcoop/i18n'
import {
  pencil,
  signature
  // signature
} from '@smartcoop/icons'
import { useSnackbar } from '@smartcoop/snackbar'
import { selectAllModules } from '@smartcoop/stores/authentication/selectorAuthentication'
import { UserActions } from '@smartcoop/stores/user'
import colors from '@smartcoop/styles/colors'
import {
  momentBackDateFormat,
  momentFriendlyDateFormat
} from '@smartcoop/utils/dates'
import { createFormData } from '@smartcoop/utils/files'
import { formatCpfCnpj, formatPhone } from '@smartcoop/utils/formatters'
import AvatarInput from '@smartcoop/web-components/AvatarInput'
import Button from '@smartcoop/web-components/Button'
import Icon from '@smartcoop/web-components/Icon'
import Loader from '@smartcoop/web-components/Loader'
import ChangeSignatureModal from '@smartcoop/web-containers/modals/ChangeSignatureModal/ChangeSignatureModal'
import EditProfileModal from '@smartcoop/web-containers/modals/profile/EditProfileModal'

import {
  Container,
  RowContainer,
  FirstRowContainer,
  Item,
  AvatarContainer,
  Top,
  InfoContainer,
  AvatarNameContainer,
  Name,
  ButtonContainer
} from './styles'

const UserInfoFragment = (props) => {
  const {
    user: { name, dateOfBirth, email, cellPhone, document: doc, profilePhoto }
  } = props

  const t = useT()
  const snackbar = useSnackbar()
  const { createDialog } = useDialog()
  const dispatch = useCallback(useDispatch(), [])

  const allModules = useSelector(selectAllModules)

  const [isAvatarLoading, setIsAvatarLoading] = useState(false)

  const showEletronicSignatureButton = useMemo(
    () => allModules.some((item) => item.slug === 'commercialization'),
    [allModules]
  )

  const formatted = useMemo(
    () => ({
      phone: formatPhone(cellPhone),
      document: formatCpfCnpj(doc),
      birthday: moment(dateOfBirth, momentBackDateFormat).format(
        momentFriendlyDateFormat
      )
    }),
    [cellPhone, doc, dateOfBirth]
  )

  const source = useMemo(() => profilePhoto?.fileUrl ?? null, [profilePhoto])

  const openEditProfileModal = useCallback(() => {
    createDialog({
      id: 'edit-profile',
      Component: EditProfileModal
    })
  }, [createDialog])

  const openChangeSignatureModal = useCallback(() => {
    createDialog({
      id: 'change-signature',
      Component: ChangeSignatureModal
    })
  }, [createDialog])

  const onSuccessAvatarChange = useCallback(() => {
    dispatch(
      UserActions.loadUser(
        () => {
          setIsAvatarLoading(false)
          snackbar.success(
            t('your {this} was updated', {
              gender: 'female',
              this: t('profile picture', { howMany: 1 })
            })
          )
        },
        () => {
          setIsAvatarLoading(false)
        }
      )
    )
  }, [dispatch, snackbar, t])

  const handleAvatarChange = useCallback(
    (file) => {
      setIsAvatarLoading(true)

      dispatch(
        UserActions.saveUserPicture(
          createFormData([file]),
          () => onSuccessAvatarChange(),
          () => setIsAvatarLoading(false)
        )
      )
    },
    [dispatch, onSuccessAvatarChange]
  )

  const renderAvatar = useMemo(
    () =>
      isAvatarLoading ? (
        <Loader width={ 120 } style={ { padding: 0 } } />
      ) : (
        <AvatarInput src={ source } onChange={ handleAvatarChange } />
      ),
    [handleAvatarChange, isAvatarLoading, source]
  )

  return (
    <Container>
      <FirstRowContainer>
        <AvatarContainer>{renderAvatar}</AvatarContainer>
        <InfoContainer>
          <Top>
            <AvatarNameContainer>
              <Name>{name}</Name>
            </AvatarNameContainer>
            <ButtonContainer>
              {showEletronicSignatureButton && (
                <Button
                  id="eletronic-signature"
                  color={ colors.white }
                  style={ { marginRight: 10 } }
                  onClick={ openChangeSignatureModal }
                >
                  <>
                    <Icon
                      icon={ signature }
                      size={ 17 }
                      color={ colors.black }
                      style={ { marginRight: 5 } }
                    />
                    <I18n>eletronic signature</I18n>
                  </>
                </Button>
              )}
              <Button
                id="edit-profile"
                color={ colors.white }
                onClick={ openEditProfileModal }
              >
                <>
                  <Icon icon={ pencil } size={ 17 } style={ { marginRight: 5 } } />
                  <I18n>edit</I18n>
                </>
              </Button>
            </ButtonContainer>
          </Top>
          <RowContainer>
            <Item>
              <I18n>date of birth</I18n>: {formatted.birthday}
            </Item>
            <Item>{email}</Item>
          </RowContainer>
          <RowContainer>
            <Item>CPF/CNPJ: {formatted.document}</Item>
            <Item>{formatted.phone}</Item>
          </RowContainer>
        </InfoContainer>
      </FirstRowContainer>
    </Container>
  )
}

UserInfoFragment.propTypes = {
  user: PropTypes.object.isRequired
}

export default UserInfoFragment
