import styled from 'styled-components'

export const Container = styled.div`
  display: flex;
  /* background: tomato; */
  margin: 40px 180px 40px 120px;
  flex-direction: column;
  flex: 1;
  flex-direction: row;
`

export const RowContainer = styled.div`
  display: flex;
  flex-direction: row;
`

export const Item = styled.div`
  width: 50%;
  margin-right: 100px;
`

export const AvatarNameContainer = styled.div`
`

export const ButtonContainer = styled.div`
`

export const FirstRowContainer = styled(RowContainer)`
  justify-content: space-between;
`

export const Text = styled.span`
  font-size: 16px;
`

export const Name = styled(Text)`
  font-weight: 700;
  font-size: 17px;
`
