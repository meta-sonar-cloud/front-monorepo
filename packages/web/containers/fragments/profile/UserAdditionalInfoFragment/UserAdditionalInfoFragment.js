import React, { useMemo } from 'react'

import PropTypes from 'prop-types'

import { useT } from '@smartcoop/i18n'
import {
  organizationRounded
} from '@smartcoop/icons'

import AddressCardFragment from '../AddressCardFragment'
import ListFragment from '../ListFragment'
import {
  Container,
  Item
} from './styles'



const UserAdditionalInfoFragment = (props) => {
  const {
    user,
    userOrganizations
  } = props
  const t = useT()
  const address = useMemo(
    () => (
      {
        state: user.state ?? null,
        city: user.city ?? null,
        district: user.district ?? null,
        street: user.street ?? null,
        number: user.number ?? null,
        postalCode: user.postalCode ?? null
      }
    ), [user]
  )

  return (
    <Container>
      <Item>
        <AddressCardFragment
          address={ address }
        />
      </Item>
      <Item>
        <ListFragment
          listId='id'
          listKey="companyName"
          title={ t('associated organizations') }
          icon={ organizationRounded }
          list={ userOrganizations }
        />
      </Item>
    </Container>
  )
}

UserAdditionalInfoFragment.propTypes = {
  user: PropTypes.object.isRequired,
  userOrganizations: PropTypes.array.isRequired
}

export default UserAdditionalInfoFragment
