import { makeStyles } from '@material-ui/core/styles'

import colors from '@smartcoop/styles/colors'

export default makeStyles({
  actionButton: {
    '&:hover': {
      backgroundColor: colors.transparent
    }
  }
})
