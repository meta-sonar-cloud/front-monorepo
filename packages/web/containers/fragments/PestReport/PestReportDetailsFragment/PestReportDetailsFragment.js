import React, { useMemo, useCallback } from 'react'

import moment from 'moment/moment'
import PropTypes from 'prop-types'

import isEmpty from 'lodash/isEmpty'
import map from 'lodash/map'

import Divider from '@material-ui/core/Divider'

import { useDialog } from '@smartcoop/dialog'
import I18n, { useT } from '@smartcoop/i18n'
import {
  trash,
  pencil
} from '@smartcoop/icons'
import colors from '@smartcoop/styles/colors'
import { momentBackDateFormat, momentFriendlyDateFormat } from '@smartcoop/utils/dates'
import Icon from '@smartcoop/web-components/Icon'
import IconButton from '@smartcoop/web-components/IconButton'
import ConfirmModal from '@smartcoop/web-components/Modal/ConfirmModal'
import ThumbnailImage from '@smartcoop/web-components/ThumbnailImage'

import {
  DetailsContainer,
  Title,
  Item,
  Info,
  Subtitle,
  BlackSubtitle,
  Text,
  ImagesContainer,
  Actions
} from './styles'

const PestReportDetailsFragment = props => {
  const { pestReport, onDelete , onEdit } = props
  const t = useT()
  const { createDialog } = useDialog()

  const values = useMemo(
    () => (
      {
        type: pestReport?.reportTypes?.name ?? '',
        name: pestReport[pestReport?.reportTypes?.slug]?.name ?? '',
        date: moment(pestReport.occurenceDate, momentBackDateFormat).format(momentFriendlyDateFormat),
        damage: (`${ pestReport?.damage  }%`) ?? '',
        observations: pestReport?.observations ?? '',
        description: pestReport?.description ?? '',
        reportImages: !isEmpty(pestReport?.reportImages) ? pestReport.reportImages : []
      }
    ), [pestReport]
  )

  const onDeleteReport = useCallback(
    () => {
      onDelete(pestReport.id)
    }, [onDelete, pestReport.id]
  )

  const onEditReport = useCallback(
    () => {
      onEdit({ ...pestReport })
    }, [onEdit, pestReport]
  )

  const confirmDelete = useCallback(
    () => (
      createDialog({
        id: 'confirm-delete',
        Component: ConfirmModal,
        props: {
          textWarning: t('are you sure you want to delete the {this}?', {
            gender: 'male',
            howMany: 1,
            this: t('report')
          }),
          onConfirm: onDeleteReport
        }
      })
    ),
    [createDialog, onDeleteReport, t]
  )

  return (
    <DetailsContainer>
      <Title>
        <I18n>occurrence</I18n>
      </Title>
      <Divider/>
      <Item>
        <Info>
          <Subtitle>
            <I18n>
              occurrence
            </I18n>
          </Subtitle>
          <Text>
            {values?.type} { values.name && '-'} {values.name}
          </Text>
        </Info>
        <Actions>
          <IconButton
            id="edit-pest-report"
            color={ colors.white }
            style={ { padding: '5.5px 10px', fontSize: '0.875rem', marginRight: 10 } }
            onClick={ onEditReport }
          >
            <Icon icon={ pencil } size={ 17 } style={ { marginRight: 5 } } />
          </IconButton>
          <IconButton
            id="delete-pest-report"
            style={ { padding: '5.5px 10px', fontSize: '0.875rem', marginRight: 10 } }
            color={ colors.white }
            onClick={ confirmDelete }
          >
            <Icon size={ 17 } icon={ trash } color={ colors.red } />
          </IconButton>
        </Actions>
      </Item>
      <Item>
        <Info>
          <Subtitle>
            <I18n params={ {
              howMany: 1
            } }
            >
              date
            </I18n>
          </Subtitle>
          <Text>
            {values?.date}
          </Text>
        </Info>
        <Info>
          <Subtitle>
            <I18n>
              damage done
            </I18n>
          </Subtitle>
          <Text>
            {values?.damage}
          </Text>
        </Info>
      </Item>
      <Item>
        {values?.description && (
          <Info>
            <Subtitle>
              <I18n>
              description
              </I18n>
            </Subtitle>
            <Text>
              {values?.description}
            </Text>
          </Info>
        )}
        {values?.observations && (
          <Info>
            <Subtitle>
              <I18n>
              observations
              </I18n>
            </Subtitle>
            <Text>
              {values?.observations}
            </Text>
          </Info>
        )}
      </Item>
      {
        !isEmpty(pestReport.reportImages) && (
          <Item>
            <Info>
              <BlackSubtitle>
                <I18n>
                  images
                </I18n>
              </BlackSubtitle>
              <ImagesContainer>
                {map(values?.reportImages, ({ id: fileId, fileUrl }) => (
                  <ThumbnailImage
                    key={ fileId }
                    src={ fileUrl }
                    size={ 80 }
                  />
                ))}
              </ImagesContainer>
            </Info>
          </Item>
        )
      }
    </DetailsContainer>
  )
}

PestReportDetailsFragment.propTypes = {
  pestReport: PropTypes.object.isRequired,
  onDelete: PropTypes.func,
  onEdit: PropTypes.func
}

PestReportDetailsFragment.defaultProps = {
  onEdit: () => {},
  onDelete: () => {}
}

export default PestReportDetailsFragment
