import React, { useCallback, useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import moment from 'moment/moment'
import PropTypes from 'prop-types'

import isEmpty from 'lodash/isEmpty'
import map from 'lodash/map'
import upperFirst from 'lodash/upperFirst'

import Divider from '@material-ui/core/Divider'

import { useDialog } from '@smartcoop/dialog'
import I18n, { useT } from '@smartcoop/i18n'
import {
  arrowDownField,
  arrowUpField,
  calendar,
  // pencil,
  trash,
  dropPercentage,
  wind,
  rain,
  closeBold } from '@smartcoop/icons'
import { useSnackbar } from '@smartcoop/snackbar'
import { CropManagementActions } from '@smartcoop/stores/cropManagement'
import { selectCropManagement } from '@smartcoop/stores/cropManagement/selectorCropManagement'
import { FieldActions } from '@smartcoop/stores/field'
import colors from '@smartcoop/styles/colors'
import { momentFriendlyDateFormat, momentBackDateFormat } from '@smartcoop/utils/dates'
import Button from '@smartcoop/web-components/Button'
import Icon from '@smartcoop/web-components/Icon'
import Loader from '@smartcoop/web-components/Loader'
import ConfirmModal from '@smartcoop/web-components/Modal/ConfirmModal'
import CompleteCropManagementModal from '@smartcoop/web-containers/modals/CompleteCropManagementModal'

import {
  Container,
  Header,
  ManagementName,
  DateContent,
  TextContent,
  WheaterManagement,
  TemperatureContent,
  HumidityText,
  PrecipationText,
  WindText,
  DetailsManagement,
  DetailsText,
  TitleWheater,
  DateText,
  TemperatureText,
  TemperatureNumber,
  TitleInfoTemp,
  TextInfoTemp,
  DateSingle,
  Temperature } from './style'

const CropManagementDetailFragment = (props) => {
  const {
    cropManagementData: externalCropManagementData,
    growingSeasonId,
    closeCropManagement,
    startLoading,
    canConcludeGrowingSeason
  } = props

  const t = useT()
  const snackbar = useSnackbar()
  const { createDialog } = useDialog()
  const dispatch = useCallback(useDispatch(), [])

  const cropManagementData = useSelector(selectCropManagement)

  const [loadingManagement, setLoadingManagement] = useState(false)
  const [disabled, setDisabled] = useState(false)

  useEffect(
    () => {
      setLoadingManagement(true)
      dispatch(
        CropManagementActions.loadCropManagement(
          { growingSeasonId,
            externalCropManagementData,
            cropManagementId: externalCropManagementData.id
          },
          () => setLoadingManagement(false),
          () => setLoadingManagement(false)
        ))
    }
    ,
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [externalCropManagementData]
  )


  const handleCropManagementDelete = useCallback(
    () => {
      createDialog({
        id: 'confirm-delete-cropManagement',
        Component: ConfirmModal,
        props: {
          onConfirm: () => {
            startLoading(true)
            dispatch(FieldActions.deleteOfflineCropManagement(
              growingSeasonId,
              cropManagementData.id,
              () => {
                snackbar.success(
                  t('your {this} was deleted', {
                    howMany: 1,
                    gender: 'male',
                    this: t('cropManagement', { howMany: 1 })
                  })
                )
                startLoading(false)
                closeCropManagement()
              },
              () => startLoading(false)
            ))
          },
          textWarning: t('are you sure you want to delete the {this}?', {
            gender: 'male',
            howMany: 1,
            this: t('cropManagement', { howMany: 1 })
          })
        }
      })
    },
    [closeCropManagement, createDialog, cropManagementData.id, dispatch, growingSeasonId, snackbar, startLoading, t]
  )

  const handleCompleteCropManagement = useCallback(
    () => {
      createDialog({
        id: 'complete-crop-management',
        Component: CompleteCropManagementModal,
        props: {
          cropManagementData,
          growingSeasonId,
          closeCropManagement,
          canConcludeGrowingSeason,
          startLoading: (value) => startLoading(value),
          endLoading: () => startLoading(false)
        }
      })
    },
    [canConcludeGrowingSeason, closeCropManagement, createDialog, cropManagementData, growingSeasonId, startLoading]
  )

  const isDisabled = useCallback(
    () => cropManagementData.realizationDate ? setDisabled(true) : setDisabled(false)
    ,
    [cropManagementData.realizationDate]
  )

  useEffect(() => {
    isDisabled()
  }, [growingSeasonId, isDisabled])


  return(
    <Container>
      {loadingManagement ? (
        <Loader width={ 100 } />
      ) : (
        <>
          <Header>
            {!isEmpty(cropManagementData.operation) && (
              <TextContent>
                <ManagementName>
                  {cropManagementData.operation.name}
                </ManagementName>
                <DateContent>
                  <DateSingle>
                    <I18n>scheduled to:</I18n>
                    <Icon
                      style={ { margin: '0 5px 0 3px' } }
                      size={ 10 } icon={ calendar }
                      color={ colors.green }
                    />
                    {moment(cropManagementData.predictedDate, momentBackDateFormat).format(momentFriendlyDateFormat)}
                  </DateSingle>
                  {!isEmpty(cropManagementData.realizationDate) && (
                    <>
                      <span style={ { margin: '0 5px' } }>-</span>
                      <DateSingle>
                        <I18n>held in:</I18n>
                        <Icon
                          style={ { margin: '0 5px 0 3px' } }
                          size={ 10 } icon={ calendar }
                          color={ colors.grey }
                        />
                        {moment(cropManagementData.realizationDate, momentBackDateFormat).format(momentFriendlyDateFormat)}
                      </DateSingle>
                    </>
                  )}
                </DateContent>
              </TextContent>
            )}
            <div>
              {/* <Button
                id="edit-field"
                color={ colors.white }
                style={ { padding: '5.5px 10px', fontSize: '0.875rem', marginRight: 10 } }
                disabled
              >
                <>
                  <Icon icon={ pencil } size={ 17 } style={ { marginRight: 5 } } />
                  <I18n>edit</I18n>
                </>
              </Button> */}
              <>
                <Button
                  id="delete-field"
                  style={ { padding: '5.5px 10px', fontSize: '0.875rem', marginRight: 10 } }
                  color={ colors.white }
                  onClick={ handleCropManagementDelete }
                >
                  <>
                    <Icon size={ 17 } icon={ trash } color={ colors.red } />
                    <I18n>exclude</I18n>
                  </>
                </Button>
                <Button
                  id="complete-operation"
                  style={ { padding: '5.5px 10px', fontSize: '0.875rem', marginRight: 10 } }
                  color="primary"
                  onClick={ handleCompleteCropManagement }
                  disabled={ disabled }
                >
                  <>
                    <I18n>complete operation</I18n>
                  </>
                </Button>
              </>
              <Button
                id="close-crop-management"
                variant="outlined"
                color={ colors.white }
                style={ { padding: 0, fontSize: '0.875rem' } }
                onClick={ closeCropManagement }
              >
                <Icon size={ 30 } icon={ closeBold } color={ colors.darkGrey }/>
              </Button>
            </div>
          </Header>
          {!isEmpty(cropManagementData.weather) && (
            <>
              <Divider/>
              <WheaterManagement>
                <div>
                  <TitleWheater>
                    <I18n>forecast for realization date</I18n>
                  </TitleWheater>
                  <DateText>{moment(cropManagementData.predictedDate, momentBackDateFormat).format('DD [de] MMMM, dddd')}</DateText>
                </div>
                <TemperatureContent>
                  <Temperature>
                    <TemperatureText>
                      <I18n>max</I18n>
                    </TemperatureText>
                    <TemperatureNumber>{cropManagementData.weather.high}°C</TemperatureNumber>
                    <Icon style={ { marginLeft: '7px' } } size={ 9 } icon={ arrowUpField } color={ colors.orange } />
                  </Temperature>

                  <Temperature>
                    <TemperatureText>
                      <I18n>min</I18n>
                    </TemperatureText>
                    <TemperatureNumber>{cropManagementData.weather.low}°C</TemperatureNumber>
                    <Icon style={ { marginLeft: '7px' } } size={ 9 } icon={ arrowDownField } color={ colors.blue } />
                  </Temperature>
                </TemperatureContent>

                <HumidityText>
                  <TitleInfoTemp>
                    <I18n>humidity</I18n>
                  </TitleInfoTemp>
                  <TextInfoTemp>
                    <Icon style={ { marginRight: '7px' } } size={ 9 } icon={ dropPercentage } color={ colors.blue } />
                    <p>{cropManagementData.weather.humidity}%</p>
                  </TextInfoTemp>
                </HumidityText>

                <PrecipationText>
                  <TitleInfoTemp>
                    <I18n>precipitation</I18n>
                  </TitleInfoTemp>
                  <TextInfoTemp>
                    <Icon style={ { marginRight: '7px' } } size={ 9 } icon={ rain } color={ colors.blue } />
                    <p>{cropManagementData.weather.precipitation}mm</p>
                  </TextInfoTemp>
                </PrecipationText>

                <WindText>
                  <TitleInfoTemp>
                    <I18n>wind</I18n>
                  </TitleInfoTemp>
                  <TextInfoTemp >
                    <Icon style={ { marginRight: '7px' } } size={ 9 } icon={ wind } color={ colors.blue } />
                    <p>{cropManagementData.weather.wind} km/h</p>
                  </TextInfoTemp>
                </WindText>
              </WheaterManagement>
            </>)}
          <Divider/>
          <DetailsManagement>
            {cropManagementData.cropManagementItem && (
              map(cropManagementData.cropManagementItem.items, (value, key) => (
                <DetailsText key={ key }>
                  <div style={ { fontWeight: 600 } }>
                    <I18n>{key}</I18n>
                  </div>
                  <div style={ { fontSize: 16 } }>
                    <p>{value.name ? value.name : upperFirst(value)}</p>
                  </div>
                </DetailsText>
              )))}
            {cropManagementData.observations && (
              <DetailsText>
                <p style={ { fontWeight: 600 } }>
              Observações
                </p>
                <div style={ { fontSize: 16 } }>
                  <p>{cropManagementData.observations}</p>
                </div>
              </DetailsText>
            )}
          </DetailsManagement>
        </>
      )}
    </Container>
  )
}

CropManagementDetailFragment.propTypes = {
  cropManagementData: PropTypes.object,
  growingSeasonId: PropTypes.string,
  closeCropManagement: PropTypes.func,
  canConcludeGrowingSeason: PropTypes.bool,
  startLoading: PropTypes.func
}

CropManagementDetailFragment.defaultProps = {
  cropManagementData: {},
  growingSeasonId: null,
  canConcludeGrowingSeason: false,
  closeCropManagement: () => {},
  startLoading: () => {}
}

export default CropManagementDetailFragment
