import styled from 'styled-components'

export const Top = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-end;
  width: 100%;
  margin-bottom: 20px;
`
