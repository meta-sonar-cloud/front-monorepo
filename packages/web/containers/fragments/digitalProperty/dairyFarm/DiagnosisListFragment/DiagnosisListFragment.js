import React, { useEffect, useMemo, useCallback, useRef, useState } from 'react'
import { useSelector, useDispatch } from 'react-redux'

import moment from 'moment/moment'

import isEmpty from 'lodash/isEmpty'
import values from 'lodash/values'

import { useDialog } from '@smartcoop/dialog'
import I18n, { useT } from '@smartcoop/i18n'
import { getAllAnimalPregnancyDiagnostics } from '@smartcoop/services/apis/smartcoopApi/resources/animalPregnancyDiagnostics'
import { AnimalActions } from '@smartcoop/stores/animal'
import { selectCurrentAnimal } from '@smartcoop/stores/animal/selectorAnimal'
import { InseminationActions } from '@smartcoop/stores/insemination'
import {
  momentBackDateFormat,
  momentFriendlyDateFormat
} from '@smartcoop/utils/dates'
import Button from '@smartcoop/web-components/Button'
import DataTable from '@smartcoop/web-components/DataTable'
import FilterButton from '@smartcoop/web-components/FilterButton'
import FilterDiagnosisActionsModal from '@smartcoop/web-containers/modals/dairyFarm/FilterDiagnosisActionsModal'
import RegisterDiagnosisActionsModal from '@smartcoop/web-containers/modals/dairyFarm/RegisterDiagnosisActionsModal'

import { Top } from './styles'

const DiagnosisListFragment = () => {
  const t = useT()
  const { createDialog } = useDialog()
  const dispatch = useCallback(useDispatch(), [])
  const tableRef = useRef(null)

  const currentAnimal = useSelector(selectCurrentAnimal)

  const [filters, setFilters] = useState({})

  const urlParams = useMemo(() => ({ animalId: currentAnimal.id }), [
    currentAnimal
  ])

  const registerDisabled = useMemo(
    () =>
      currentAnimal?.animalStatus?.name !== 'Inseminada' &&
      currentAnimal?.animalStatus?.name !== 'Inseminada a confirmar' &&
      currentAnimal?.animalStatus?.name !== 'Prenha',
    [currentAnimal]
  )

  const queryParams = useMemo(() => {
    const { earring, ...rest } = filters
    const diagnosis = filters.diagnosisType
      ? { diagnosisType: String(filters.diagnosisType) }
      : {}
    return { ...rest, ...diagnosis }
  }, [filters])

  const columns = useMemo(
    () => [
      {
        title: t('insemination date'),
        field: 'insemination.inseminationDate',
        render: (row) =>
          moment(row.insemination.inseminationDate, momentBackDateFormat).format(
            momentFriendlyDateFormat
          ),
        sorting: true
      },
      {
        title: t('earring'),
        field: 'animal.earring.earringCode',
        sorting: true
      },
      {
        title: t('animal/name'),
        field: 'animal.name',
        sorting: true
      },
      {
        title: t('type of diagnosis'),
        field: 'diagnosisType.typeName',
        sorting: true
      },
      {
        title: t('accomplished date'),
        field: 'realizationDate',
        sorting: true,
        defaultSort: 'desc',
        render: (row) =>
          row.realizationDate
            ? moment(row.realizationDate, momentBackDateFormat).format(
              momentFriendlyDateFormat
            )
            : t('not accomplished', { gender: 'female' })
      },
      {
        title: t('result'),
        field: 'result',
        sorting: true,
        render: (row) => {
          switch (row.result) {
            case 'Positivo':
              return t('pregnant')
            case 'Inconclusivo':
              return t('inconclusive')
            case 'Vazia':
            default:
              return t('empty')
          }
        }
      }
    ],
    [t]
  )

  const reloadDataTable = useCallback(() => {
    tableRef.current.onQueryChange()
  }, [])

  const handleSetFilters = useCallback((filterValues) => {
    setFilters({
      ...filterValues,
      q: filterValues?.earring ?? null
    })
  }, [])

  const openFilterModal = useCallback(() => {
    createDialog({
      id: 'filter-fields',
      Component: FilterDiagnosisActionsModal,
      props: {
        onSubmit: handleSetFilters,
        filters
      }
    })
  }, [createDialog, filters, handleSetFilters])

  // const onDelete = useCallback(
  //   (action) => {
  //     dispatch(
  //       AnimalPregnancyActionsActions.deleteDrying(action.id, () => {
  //         snackbar.success(
  //           t('your {this} was deleted', {
  //             howMany: 1,
  //             gender: 'female',
  //             this: t('action')
  //           })
  //         )
  //         reloadDataTable()
  //       })
  //     )
  //   },
  //   [dispatch, reloadDataTable, snackbar, t]
  // )

  // const handleDelete = useCallback(
  //   (_, action) => {
  //     createDialog({
  //       id: 'confirm-delete',
  //       Component: ConfirmModal,
  //       props: {
  //         onConfirm: () => onDelete(action),
  //         textWarning: t('are you sure you want to remove the {this}?', {
  //           howMany: 1,
  //           gender: 'female',
  //           this: t('action')
  //         })
  //       }
  //     })
  //   },
  //   [createDialog, onDelete, t]
  // )

  const openCreateModal = useCallback(
    (_, data = {}) => {
      createDialog({
        id: 'create-diagnosis',
        Component: RegisterDiagnosisActionsModal,
        props: {
          onSubmit: reloadDataTable,
          data
        }
      })
    },
    [createDialog, reloadDataTable]
  )

  // const handleEdit = useCallback(
  //   (_, row) => {
  //     const { id } = row
  //     dispatch(
  //       AnimalPregnancyDiagnosticsActions.loadPregnancyDiagnostic(
  //         { pregnancyId: id },
  //         (data) => openCreateModal(_, data),
  //         (err) => snackbar.error(err.message)
  //       )
  //     )
  //   },
  //   [dispatch, openCreateModal, snackbar]
  // )


  useEffect(
    () => {
      dispatch(InseminationActions.loadCurrentInsemination())
    },[dispatch]
  )

  useEffect(
    () => {
      dispatch(AnimalActions.loadCurrentAnimal())
    },[dispatch]
  )

  return (
    <>
      <Top>
        <div>
          <FilterButton
            onClick={ openFilterModal }
            isActive={ !values(filters).every(isEmpty) }
          />
        </div>
        <div>
          <Button
            id="register-lot"
            onClick={ openCreateModal }
            color="secondary"
            disabled={ registerDisabled }
          >
            <I18n>register</I18n>
          </Button>
        </div>
      </Top>
      <DataTable
        data={ getAllAnimalPregnancyDiagnostics }
        queryParams={ queryParams }
        columns={ columns }
        urlParams={ urlParams }
        tableRef={ tableRef }
        // onDeleteClick={ handleDelete }
        // onEditClick={ handleEdit }
      />
    </>
  )
}

export default DiagnosisListFragment
