import React, { useState, useCallback, useMemo } from 'react'

import { Report } from 'powerbi-report-component'
import PropTypes from 'prop-types'


const DetecReportFragment = ({
  token,
  organizationId
}) => {
  // eslint-disable-next-line no-unused-vars
  const [report, setReport] = useState(null)
  const [showReport, setShowReport] = useState(true)

  const filters = useMemo(() => ({
    $schema: 'http://powerbi.com/product/schema#basic',
    target: {
      table: 'Visitas',
      column: 'organization_id'
    },
    operator: 'In',
    values: [organizationId]
  }), [organizationId])

  const handleReportLoad = useCallback(async (data) => {
    await data.setFilters([filters])
    if (!showReport) {
      setShowReport(true)
    }

    setReport(data)
  }, [filters, showReport])


  const handleReportRender = useCallback(
    data => setReport(data),
    [setReport]
  )
  return (
    <Report
      tokenType="Aad"
      accessToken={ token }
      embedUrl="https://app.powerbi.com/reportEmbed"
      embedId="02b508b2-5eaa-4ed4-a89c-1fecbda9b0a8"
      pageName="ReportSectionfffcb8d809179faedca9"
      reportMode="View"
      permissions="Read"
      onLoad={ handleReportLoad }
      onRender={ handleReportRender }
      style={ {
        width: '100%',
        height: '200%'
      } }
      extraSettings={ {
        filterPaneEnabled: false,
        navContentPaneEnabled: false,
        hideErrors: false
      } }
    />
  )
}

DetecReportFragment.propTypes = {
  token: PropTypes.string.isRequired,
  organizationId: PropTypes.string.isRequired
}

export default DetecReportFragment
