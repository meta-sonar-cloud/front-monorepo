import React, { useState, useCallback, useMemo } from 'react'
import { useSelector } from 'react-redux'

import { Report } from 'powerbi-report-component'
import PropTypes from 'prop-types'

import { selectUser } from '@smartcoop/stores/user/selectorUser'

const MyFieldProductivityReportFragment = ({ token }) => {
  // eslint-disable-next-line no-unused-vars
  const [report, setReport] = useState(null)
  const [showReport, setShowReport] = useState(true)
  const user = useSelector(selectUser)

  const filters = useMemo(() => ({
    $schema: 'http://powerbi.com/product/schema#basic',
    target: {
      table: 'Colheita',
      column: 'user_id'
    },
    operator: 'In',
    values: [user.id]
  }), [user.id])

  const handleReportLoad = useCallback(async (data) => {
    await data.setFilters([filters])

    if (!showReport) {
      setShowReport(true)
    }

    setReport(data)
  }, [showReport, setReport, setShowReport, filters])


  const handleReportRender = useCallback(
    data => setReport(data),
    [setReport]
  )

  return (
    <Report
      tokenType="Aad"
      accessToken={ token }
      embedUrl="https://app.powerbi.com/reportEmbed"
      embedId="e9a78bc7-bc3e-4da5-8aee-9322bd058501"
      pageName="ReportSection96e2b14273f1a4826549"
      reportMode="View"
      permissions="Read"
      onLoad={ handleReportLoad }
      onRender={ handleReportRender }
      style={ {
        width: '100%',
        height: '200%'
      } }
      extraSettings={ {
        filterPaneEnabled: false,
        navContentPaneEnabled: false,
        hideErrors: false
      } }
    />
  )
}

MyFieldProductivityReportFragment.propTypes = {
  token: PropTypes.string.isRequired
}

export default MyFieldProductivityReportFragment
