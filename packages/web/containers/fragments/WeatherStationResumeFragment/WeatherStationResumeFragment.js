import React, { useMemo } from 'react'
import { useSelector } from 'react-redux'

import moment from 'moment/moment'
import PropTypes from 'prop-types'

import { find, filter } from 'lodash'
import camelCase from 'lodash/camelCase'
import isEmpty from 'lodash/isEmpty'
import map from 'lodash/map'

import I18n from '@smartcoop/i18n'
import {
  rain,
  windmill,
  thermometer,
  sun,
  compass,
  dropPercentage,
  wind,
  battery
} from '@smartcoop/icons'
import { selectCurrentWeatherStation } from '@smartcoop/stores/weatherStations/selectorWeatherStations'
import colors from '@smartcoop/styles/colors'
import Icon from '@smartcoop/web-components/Icon'
import Loader from '@smartcoop/web-components/Loader'

import {
  DetailsContainer,
  ItemData,
  LeftData,
  RightData,
  Item,
  TextUpdate,
  DataWeatherStation,
  BlackSubtitle,
  DividerMui,
  Text
} from './styles'

const WeatherStationResumeFragment = (props) => {
  const { stationName } = props

  const currentWeatherStation = useSelector(selectCurrentWeatherStation)

  const currentItem = useMemo(
    () => ({
      temperaturaDoAr: {
        icon: thermometer,
        color: colors.orange
      },
      direcaoDoVento: {
        icon: compass,
        color: colors.red
      },
      umidadeDoAr: {
        icon: dropPercentage,
        color: colors.blue
      },
      velocidadeDoVento: {
        icon: windmill,
        color: colors.darkGrey
      },
      radiacaoSolarMedia: {
        icon: sun,
        color: colors.yellow
      },
      rajadaDeVento: {
        icon: wind,
        color: colors.blue
      },
      temperaturaDoSolo: {
        icon: thermometer,
        color: colors.orange
      },
      tensaoDaBateria: {
        icon: battery,
        color: colors.green
      },
      precipitacao: {
        icon: rain,
        color: colors.blue
      }
    }),
    []
  )

  const temperatureSoil = useMemo(
    () =>
      find(currentWeatherStation.descriptions, {
        description: 'Temperatura do Solo'
      }),
    [currentWeatherStation]
  )

  const arrayItemDescriptions = useMemo(
    () =>
      filter(
        currentWeatherStation.descriptions,
        (item) => item.description !== 'Temperatura do Solo'
      ),
    [currentWeatherStation]
  )

  return (
    <DetailsContainer>
      <Item>
        <BlackSubtitle>{stationName}</BlackSubtitle>
      </Item>
      <DividerMui />
      {isEmpty(currentWeatherStation) && <Loader />}
      {!isEmpty(currentWeatherStation) && (
        <>
          <Item>
            <div style={ { paddingBottom: 10 } }>
              <I18n as={ BlackSubtitle } params={ { howMany: 1 } }>
                weather station
              </I18n>
            </div>
            <ItemData>
              <DataWeatherStation>
                <BlackSubtitle>{temperatureSoil.description}</BlackSubtitle>
                <Text>
                  {temperatureSoil.value} {temperatureSoil.unit}
                </Text>
              </DataWeatherStation>
              <DataWeatherStation>
                <I18n as={ BlackSubtitle }>depth</I18n>
                <Text>{temperatureSoil.depth}</Text>
              </DataWeatherStation>
            </ItemData>
          </Item>
          <Item>
            <div style={ { paddingBottom: 8 } }>
              <I18n as={ BlackSubtitle } params={ { howMany: 1 } }>
                weather station
              </I18n>
            </div>
            {!isEmpty(arrayItemDescriptions) &&
              map(arrayItemDescriptions, (itemData) => (
                <div key={ itemData?.index }>
                  <ItemData>
                    <LeftData>
                      <Icon
                        icon={
                          currentItem[camelCase(itemData?.description)].icon
                        }
                        size={ 18 }
                        style={ { marginRight: 5 } }
                        color={
                          currentItem[camelCase(itemData?.description)].color
                        }
                      />
                      <Text>{itemData.description}</Text>
                    </LeftData>
                    <RightData>
                      <Text>
                        {itemData.value} {itemData.unit}
                      </Text>
                    </RightData>
                  </ItemData>
                </div>
              ))}
          </Item>

          <DividerMui />

          <Item>
            <TextUpdate>
              {moment(
                currentWeatherStation?.data?.measuredAt,
                'YYYY-MM-DDTHH:mm:ss.SSSSZ'
              ).format('DD/MM/YYYY [às] HH:mm:ss')}
            </TextUpdate>
          </Item>
          {/*
          <Item>
            <Button
              id="temetry-station"
              color="secondary"
              style={ { marginTop: 20, width: '100%' } }
            >
              <I18n>data</I18n>
            </Button>
          </Item> */}
        </>
      )}
    </DetailsContainer>
  )
}

WeatherStationResumeFragment.propTypes = {
  stationName: PropTypes.string
}

WeatherStationResumeFragment.defaultProps = {
  stationName: ''
}

export default WeatherStationResumeFragment
