import React, { useMemo } from 'react'

import PropTypes from 'prop-types'

import toNumber from 'lodash/toNumber'

import DataTable from '@smartcoop/web-components/DataTable'
import SecuritiesMovement from '@smartcoop/web-components/SecuritiesMovement'

const SecuritiesMovementListFragment = (props) => {
  const {
    service,
    ...rest
  } = props

  const columns = useMemo(
    () => [
      {
        title: '',
        render: (row) =>
          <SecuritiesMovement { ...row } positive={ toNumber(row.value) > 0 } />
      }
    ],
    []
  )

  return(
    <>
      <DataTable
        data={ service }
        columns={ columns }
        { ...rest }
        options={ {
          header: false
        } }
        noShadow
      />
    </>
  )
}

SecuritiesMovementListFragment.propTypes = {
  service: PropTypes.func.isRequired
}

export default SecuritiesMovementListFragment
