
import React, { useMemo } from 'react'

import moment from 'moment/moment'
import PropTypes from 'prop-types'

import ceil from 'lodash/ceil'
import toNumber from 'lodash/toNumber'

import { useT } from '@smartcoop/i18n'
import {
  getProductsQuotationByOrg as getProductsQuotationByOrgService
} from '@smartcoop/services/apis/smartcoopApi/resources/productsQuotation'
import { momentBackDateTimeFormat } from '@smartcoop/utils/dates'
import { formatCurrency, formatNumber } from '@smartcoop/utils/formatters'
import DataTable from '@smartcoop/web-components/DataTable'

const ProductsQuotationListFragment = ({ filters, urlParams }) => {
  const t = useT()

  const columns = useMemo(
    () => [
      {
        title: 'Produto',
        field: 'organizationProduct.productName',
        cellStyle: { fontWeight: 'bold' },
        render: (row) => row.organizationProduct?.productName
      },
      {
        title: 'Unidade',
        field: 'unit',
        render: (row) =>
          row.organizationProduct?.conversionFactor !== 1 && row.organizationProduct?.conversionFactor !== undefined
            ? `${ row.organizationProduct?.unitOfMeasuresForConversion } ${ t('of') } ${ formatNumber(ceil(row.organizationProduct?.conversionFactor, 2)) } ${ row.organizationProduct?.measureUnit }`
            : row.organizationProduct?.measureUnit
      },
      {
        title: 'Valor',
        field: 'unitValue',
        render: (row) =>
          row.organizationProduct?.conversionFactor !== 1 && row.organizationProduct?.conversionFactor !== undefined
            ? formatCurrency(toNumber(row.unitValue) * toNumber(row.organizationProduct?.conversionFactor))
            : formatCurrency(row.unitValue)

      },
      {
        title: 'Valido até',
        field: 'expirationDate',
        headerStyle: { whiteSpace: 'nowrap' },
        render: (row) => (moment(row.expirationDate, momentBackDateTimeFormat).format(`DD/MM/YYYY[ ${ t('until') } ]HH[h]mm`))
      }
    ], [t]
  )

  return(
    <DataTable
      data={ getProductsQuotationByOrgService }
      queryParams ={ filters }
      columns={ columns }
      urlParams={ urlParams }
    />
  )
}

ProductsQuotationListFragment.propTypes = {
  filters: PropTypes.object,
  urlParams: PropTypes.object
}

ProductsQuotationListFragment.defaultProps = {
  filters: {},
  urlParams: {}

}

export default ProductsQuotationListFragment
