import React, { useMemo, useCallback, useRef } from 'react'
import { useDispatch } from 'react-redux'

import moment from 'moment/moment'
import PropTypes from 'prop-types'

import { useDialog } from '@smartcoop/dialog'
import { useT } from '@smartcoop/i18n'
import {
  getTechnicalVisits as getTechnicalVisitsService
} from '@smartcoop/services/apis/smartcoopApi/resources/technical'
import { useSnackbar } from '@smartcoop/snackbar'
import { TechnicalActions } from '@smartcoop/stores/technical'
import DataTable from '@smartcoop/web-components/DataTable'
import ConfirmModal from '@smartcoop/web-components/Modal/ConfirmModal'
import TechnicalVisitModal from '@smartcoop/web-containers/modals/technical/TechnicalVisitModal'

const TechnicalVisitList = ({ filters }) => {
  const t = useT()
  const { createDialog } = useDialog()
  const dispatch = useCallback(useDispatch(), [])
  const snackbar = useSnackbar()
  const tableRef = useRef(null)

  const columns = useMemo(
    () => [
      {
        title: 'Data',
        field: 'groupDate',
        render: (row, renderType) => renderType === 'group' ? row : row.groupDate,
        defaultGroupSort: 'desc',
        defaultGroupOrder: 0
      },
      {
        title: 'Hora',
        field: 'visitDateTime',
        align: 'center',
        render: (row) => moment(row.visitDateTime, 'YYYY-MM-DD HH:mm:ss').format('HH:mm')
      },
      {
        title: 'Produtor',
        field: 'name',
        align: 'center',
        render: (row) => row.users[0]?.name
      },
      {
        title: 'Propriedade',
        field: 'name',
        align: 'center',
        render: (row) => row.property?.name
      },
      {
        title: 'Talhão',
        field: 'fieldName',
        align: 'center',
        render: (row) => row.field?.fieldName
      },
      {
        title: 'Cultivo',
        field: 'description',
        align: 'center',
        render: (row) => `${ row.crop?.description || '-' }`
      },
      {
        title: 'Estimativa',
        field: 'productivityEstimate',
        align: 'center',
        render: (row) => `${ row.productivityEstimate || '' } ${ row.estimateUnity ||  '-' }`
      }
    ], []
  )

  const dataTableOption = useMemo(
    () => ({
      grouping: true,
      defaultExpanded: false,
      hideGroupbar: true
    }),
    []
  )

  const onSuccessEditingTechnicalVisit = useCallback(
    () => {
      const { query } = tableRef.current.state
      tableRef.current.onQueryChange({
        ...query,
        page: 0
      })
      snackbar.success(
        t('your {this} was edited', {
          howMany: 1,
          gender: 'female',
          this: t('technical visit')
        })
      )
    },
    [snackbar, t]
  )

  const openDetailsTechnicalVisit = useCallback(
    (_, row) => {
      createDialog({
        id: 'view-edit-technical-visit',
        Component: TechnicalVisitModal,
        props: {
          technicalVisit: row,
          disabled: true,
          onSuccess: onSuccessEditingTechnicalVisit
        }
      })
    },
    [createDialog, onSuccessEditingTechnicalVisit]
  )

  const handleDelete = useCallback(
    (_, row) => {
      createDialog({
        id: 'confirm-delete',
        Component: ConfirmModal,
        props: {
          onConfirm: () => {
            dispatch(TechnicalActions.deleteTechnicalVisit(
              row.id,
              () => {
                snackbar.success(
                  t('your {this} was deleted', {
                    howMany: 1,
                    gender: 'female',
                    this: t('technical visit')
                  })
                )
                const { query } = tableRef.current.state
                tableRef.current.onQueryChange({
                  ...query,
                  page: 0
                })
              }
            ))
          },
          textWarning: t('are you sure you want to delete the {this}?', {
            howMany: 1,
            gender: 'female',
            this: t('technical visit')
          })
        }
      })
    },
    [createDialog, dispatch, snackbar, t]
  )

  return(
    <DataTable
      tableRef={ tableRef }
      columns={ columns }
      data={ getTechnicalVisitsService }
      onRowClick={ openDetailsTechnicalVisit }
      options={ dataTableOption }
      queryParams={ filters }
      onDeleteClick={ handleDelete }
    />
  )
}

TechnicalVisitList.propTypes = {
  filters: PropTypes.object
}

TechnicalVisitList.defaultProps = {
  filters: {}
}

export default TechnicalVisitList
