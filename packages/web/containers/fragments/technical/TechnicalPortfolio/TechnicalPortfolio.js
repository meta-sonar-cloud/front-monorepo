import React, { useMemo, useCallback, useRef } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import trimMask from '@meta-awesome/functions/src/trimMask'
import PropTypes from 'prop-types'

import { useDialog } from '@smartcoop/dialog'
import I18n, { useT } from '@smartcoop/i18n'
import { shield } from '@smartcoop/icons'
import { getTechnicalPortfolio as getTechnicalPortfolioService } from '@smartcoop/services/apis/smartcoopApi/resources/technical'
import { useSnackbar } from '@smartcoop/snackbar'
import {
  selectUserHaveTechnical,
  selectHasOrganizationToken
} from '@smartcoop/stores/authentication/selectorAuthentication'
import { TechnicalActions } from '@smartcoop/stores/technical'
import colors from '@smartcoop/styles/colors'
import { TechnicalPortfolioStatusCode } from '@smartcoop/utils/constants'
import Button from '@smartcoop/web-components/Button'
import DataTable from '@smartcoop/web-components/DataTable'
import Icon from '@smartcoop/web-components/Icon'
import ConfirmModal from '@smartcoop/web-components/Modal/ConfirmModal'
import TechnicalPortfolioStatus from '@smartcoop/web-components/TechnicalPortfolioStatus'

const TechnicalPortfolio = ({ filters }) => {
  const t = useT()
  const dispatch = useCallback(useDispatch(), [])
  const snackbar = useSnackbar()
  const tableRef = useRef(null)
  const { createDialog } = useDialog()

  const hasOrganizationToken = useSelector(selectHasOrganizationToken)
  const userHaveTechnical = useSelector(selectUserHaveTechnical)
  const columns = useMemo(
    () => [
      {
        title: 'Produtor',
        field: 'name'
      },
      {
        title: 'Matrícula',
        field: 'organizationUser[0].registry'
      },
      {
        title: 'Telefone',
        field: 'cellPhone'
      },
      {
        title: 'Email',
        field: 'email'
      },
      {
        title: 'Acesso',
        field: 'status',
        render: (row) => (
          <TechnicalPortfolioStatus
            statusNumber={ row.proprietaryTechnician?.status }
          />
        )
      }
    ],
    []
  )

  const actions = useMemo(
    () => [
      (row) => ({
        position: 'row',
        onClick: () =>
          createDialog({
            id: 'confirm-delete-access',
            Component: ConfirmModal,
            props: {
              onConfirm: () => {
                dispatch(
                  TechnicalActions.requestAccessTechnicalForProprietary(
                    { proprietaryId: row.id },
                    () => {
                      snackbar.success(t('your access request has been sent'))
                      const { query } = tableRef.current.state
                      tableRef.current.onQueryChange(query)
                    }
                  )
                )
              },
              textWarning: t('are you sure you want to send the {this}?', {
                howMany: 1,
                gender: 'female',
                this: t('access request', { howMany: 1 })
              })
            }
          }),
        disabled:
          row.proprietaryTechnician?.status &&
          row.proprietaryTechnician?.status !==
            TechnicalPortfolioStatusCode.NEGADO &&
          row.proprietaryTechnician?.status !==
            TechnicalPortfolioStatusCode.REVOGADO,
        ButtonComponent: (buttonProps) => (
          <div>
            <Button { ...buttonProps } />
          </div>
        ),
        iconButtonProps: {
          id: 'request-permission',
          variant: 'outlined',
          size: 'small',
          color: 'black',
          style: {
            width: 200,
            height: 30,
            fontSize: 14,
            fontWeight: 600,
            borderColor: colors.lightGrey
          }
        },
        icon: () => (
          <>
            <Icon icon={ shield } size={ 14 } style={ { paddingRight: 5 } } />
            <I18n>request permission</I18n>
          </>
        )
      })
    ],
    [createDialog, dispatch, snackbar, t]
  )

  const disabledRowNotAccess = useCallback(
    (row) =>
      row.proprietaryTechnician?.status !==
        TechnicalPortfolioStatusCode.SOMENTE_VISUALIZAÇÃO &&
      row.proprietaryTechnician?.status !==
        TechnicalPortfolioStatusCode.VISUALIZAÇÃO_EDIÇÃO,
    []
  )

  const accessPropertiesToOwner = useCallback(
    (_, row) => {
      !(
        row.proprietaryTechnician?.status !==
          TechnicalPortfolioStatusCode.SOMENTE_VISUALIZAÇÃO &&
        row.proprietaryTechnician?.status !==
          TechnicalPortfolioStatusCode.VISUALIZAÇÃO_EDIÇÃO
      ) &&
        dispatch(
          TechnicalActions.setTechnicalCurrentOwner(
            row,
            () => {},
            (error) => {
              snackbar.error(t(error.message))
            }
          )
        )
    },
    [dispatch, snackbar, t]
  )

  const conditionToDelete = useCallback(
    (row) =>
      row.proprietaryTechnician?.status &&
      row.proprietaryTechnician?.status !==
        TechnicalPortfolioStatusCode.REVOGADO,
    []
  )

  const revokeTechnicalAccess = useCallback(
    (_, row) => {
      createDialog({
        id: 'confirm-delete-access',
        Component: ConfirmModal,
        props: {
          onConfirm: () => {
            dispatch(
              TechnicalActions.revokeTechnicalAccess(
                row.proprietaryTechnician?.id,
                3,
                () => {
                  snackbar.success(t('your access has been removed'))
                  const { query } = tableRef.current.state
                  tableRef.current.onQueryChange(query)
                },
                (err) => {
                  snackbar.error(t(err.message ? err.message : err))
                }
              )
            )
          },
          textWarning: t('are you sure you want to remove the {this}?', {
            howMany: 1,
            gender: 'male',
            this: t('access', { howMany: 1 })
          })
        }
      })
    },
    [createDialog, dispatch, snackbar, t]
  )

  const dataTableOptions = useMemo(
    () => ({
      actionsCellDivStyle: {
        alignItems: 'center'
      }
    }),
    []
  )

  const queryParams = useMemo(
    () => ({
      ...filters,
      cellPhone: filters?.cellPhone ? trimMask(filters.cellPhone) : '',
      document: filters?.document ? trimMask(filters.document) : ''
    }),
    [filters]
  )

  return (
    hasOrganizationToken &&
    userHaveTechnical && (
      <DataTable
        tableRef={ tableRef }
        columns={ columns }
        actions={ actions }
        options={ dataTableOptions }
        onRowClick={ accessPropertiesToOwner }
        data={ getTechnicalPortfolioService }
        onDeleteClick={ revokeTechnicalAccess }
        conditionToDelete={ conditionToDelete }
        disabledRow={ disabledRowNotAccess }
        queryParams={ queryParams }
      />
    )
  )
}

TechnicalPortfolio.propTypes = {
  filters: PropTypes.object
}

TechnicalPortfolio.defaultProps = {
  filters: {}
}

export default TechnicalPortfolio
