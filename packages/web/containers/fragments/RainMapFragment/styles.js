import styled from 'styled-components'

import Card from '@smartcoop/web-components/Card'

export const CardItem = styled(Card).attrs({
  cardStyle: {
    width: '100%',
    padding: 0
  },
  childrenStyle: {
    padding: '0 20px 25px',
    marginTop: -15
  }
})`
  margin-top: 0px;
  margin-bottom: 15px;
  width: 100%;
`

export const DotsContainer = styled.div`
  display: flex;
  width: 100%;
  justify-content: flex-end;
  padding-top: 10px;
  padding-right: 3px;
`

export const MapContainer = styled.div`
  width: 100px;
  min-height: 400px;
`
