import blueOcean from './blue_ocean.png'
import blueSky from './blue_sky.png'
import blue from './blue.png'
import green from './green.png'
import orange from './orange.png'
import purple from './purple.png'
import red from './red.png'
import yellow from './yellow.png'

export {
  blueOcean,
  blueSky,
  blue,
  green,
  orange,
  purple,
  red,
  yellow
}
