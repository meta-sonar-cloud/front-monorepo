import React, { useCallback } from 'react'
import { useSelector , useDispatch } from 'react-redux'

import PropTypes from 'prop-types'

import { useDialog } from '@smartcoop/dialog'
import I18n, { useT } from '@smartcoop/i18n'
import { barnYellow, edit, trash } from '@smartcoop/icons'
import { PropertyActions } from '@smartcoop/stores/property'
import { selectUserCanWrite } from '@smartcoop/stores/user/selectorUser'
import colors from '@smartcoop/styles/colors'
import { formatNumber } from '@smartcoop/utils/formatters'
import Button from '@smartcoop/web-components/Button'
import Icon from '@smartcoop/web-components/Icon'
import VerticalDotsIconButton from '@smartcoop/web-components/IconButton/VerticalDotsIconButton'
import ConfirmModal from '@smartcoop/web-components/Modal/ConfirmModal'
import Popover from '@smartcoop/web-components/Popover'
import EditPropertyModal from '@smartcoop/web-containers/modals/EditPropertyModal'

import {
  CardItem,
  FarmName,
  AreasContainer,
  AreaContainer,
  InfoContentTitle,
  InfoContent,
  DotsContainer,
  ButtonContainer
} from './styles'

const PropertyResumeCard = ({ property }) => {
  const { createDialog } = useDialog()
  const dispatch = useDispatch()
  const t = useT()

  const userWrite = useSelector(selectUserCanWrite)

  const editProperty = useCallback(propertyLocal => {
    dispatch(PropertyActions.setEditPropertyData(propertyLocal.id, propertyLocal))

    createDialog({
      id: 'property-onboarding',
      Component: EditPropertyModal
    })
  }, [createDialog, dispatch])

  const deleteProperty = useCallback(propertyLocal => {
    createDialog({
      id: 'confirm-delete',
      Component: ConfirmModal,
      props: {
        onConfirm: () => {
          dispatch(PropertyActions.deleteProperty(propertyLocal.id))
          dispatch(PropertyActions.saveCurrentProperty({}))
        },
        textWarning: t('are you sure you want to delete the {this}?', {
          howMany: 1,
          gender: 'female',
          this: t('property', {
            howMany: 1
          })
        })
      }
    })
  }, [createDialog, dispatch, t])

  return (
    <CardItem
      headerRight={ (
        <DotsContainer>
          <Popover
            popoverId="property-actions"
            Component={ VerticalDotsIconButton }
            ComponentProps={ {
              color: colors.black,
              edge: 'end',
              iconColor: colors.black
            } }
          >
            <ButtonContainer>
              <Button onClick={ () => editProperty(property) } color={ colors.white } id="edit-property" disabled={ !userWrite }>
                <>
                  <Icon style={ { marginRight: 10 } } icon={ edit } size={ 15 } color={ colors.primary } />
                  <I18n>edit property</I18n>
                </>
              </Button>
              <Button onClick={ () => deleteProperty(property) } color={ colors.white } id="delete-property" disabled={ !userWrite }>
                <>
                  <Icon style={ { marginRight: 10 } } icon={ trash } size={ 15 } color={ colors.red } />
                  <I18n style={ { color: colors.red } }>delete property</I18n>
                </>
              </Button>
            </ButtonContainer>
          </Popover>
        </DotsContainer>
      ) }
    >
      <Icon
        icon={ barnYellow }
        size={ 55 }
      />

      <FarmName>
        {property.name}
      </FarmName>

      <AreasContainer>
        <AreaContainer>
          <I18n as={ InfoContentTitle }>
            total area
          </I18n>
          <InfoContent>
            {formatNumber(property.totalArea)} ha
          </InfoContent>
        </AreaContainer>
      </AreasContainer>

    </CardItem>
  )}

PropertyResumeCard.propTypes = {
  property: PropTypes.object.isRequired
}

export default PropertyResumeCard
