import React from 'react'

import { useDairyFarmPriceChartOptions } from '@smartcoop/utils/charts'
import Chart from '@smartcoop/web-components/Chart'

const DairyFarmPriceChart = () => {

  const chartOptions = useDairyFarmPriceChartOptions()

  return (
    <div style={ { height: 300, width: '100%' } }>
      <Chart
        options={ chartOptions }
        containerProps={ { style: { height: 300 } } }
      />
    </div>
  )
}

export default DairyFarmPriceChart
