/* eslint-disable import/no-dynamic-require */
/* eslint-disable global-require */
import React, { useMemo } from 'react'
import { useSelector } from 'react-redux'

import moment from 'moment'

import padStart from 'lodash/padStart'

import I18n from '@smartcoop/i18n'
import * as weatherIcons  from '@smartcoop/icons/weatherIcons'
import {
  selectCurrent,
  selectNextHour,
  selectNextDays
} from '@smartcoop/stores/weatherForecast/selectorWeatherForecast'
import {
  momentBackDateTimeFormat,
  momentFriendlyExtendedDateFormat,
  momentFriendlyTimeFormat,
  momentFriendlyDateFormat
} from '@smartcoop/utils/dates'
import Icon from '@smartcoop/web-components/Icon'

import {
  Container,
  Header,
  Column,
  Row,
  AverageContainer,
  AverageNumberContainer,
  AverageNumber,
  AverageType,
  Divider,
  Title,
  ContainerHour,
  ContainerCardHours,
  CardHours,
  ContainerDays,
  ScrollDays,
  DayCard,
  DayCircle
} from './styles'

const WeatherForecast = () => {
  const current = useSelector(selectCurrent)
  const nextHours = useSelector(selectNextHour)
  const nextDays = useSelector(selectNextDays)

  const currentDate = useMemo(
    () => moment(current?.date, momentBackDateTimeFormat)
      .format(momentFriendlyExtendedDateFormat),
    [current]
  )

  return (
    <Container>
      <Header>
        <Column style={ { alignItems: 'flex-start' } }>
          <b style={ { marginBottom: 3 } }>{currentDate}</b>
        </Column>
      </Header>
      <Row style={ { justifyContent: 'space-between' } }>
        <Column>
          <AverageContainer>
            <Icon
              icon={ weatherIcons[`icon${ padStart(current?.iconCode, 2, '0') }`] }
              size={ 60 }
            />
            <AverageNumberContainer>
              <AverageNumber>{ Math.round(current?.average) }</AverageNumber>
              <AverageType>°C</AverageType>
            </AverageNumberContainer>
          </AverageContainer>
        </Column>
        <Column>
          <Row>Max { current?.high }°C</Row>
          <Row>Min { current?.low }°C</Row>
        </Column>
        <Column>
          <Row bold>
            <I18n>humidity</I18n>
          </Row>
          <Row>{current?.humidity}%</Row>
        </Column>
        <Column>
          <Row bold>
            <I18n>wind</I18n>
          </Row>
          <Row>{current?.wind} km/h</Row>
        </Column>
        <Column>
          <Row bold>
            <I18n>precipitation</I18n>
          </Row>
          <Row>{current?.precipitation} mm</Row>
        </Column>
      </Row>
      <Divider />
      <ContainerHour>
        <Title>
          <I18n>hourly forecast</I18n>
        </Title>
        <ContainerCardHours>
          { nextHours && nextHours.map(item => (
            <CardHours key={ item.date }>
              <Row>{moment(item.date, momentBackDateTimeFormat).format(momentFriendlyTimeFormat)}</Row>
              <Icon
                icon={ weatherIcons[`icon${ padStart(item.iconCode, 2, '0') }`] }
                size={ 30 }
              />
              <b style={ { marginBottom: 3 } }>{Math.round(item.temperature)}°C</b>
              <Row maxContent>{item.humidity}%</Row>
              <Row maxContent>{item?.precipitation} mm</Row>
              <Row maxContent>{item.wind} km/h</Row>
            </CardHours>
          )) }
        </ContainerCardHours>
      </ContainerHour>
      <Divider />
      <ScrollDays>
        <ContainerDays>
          { nextDays.map(item => (
            <DayCard key={ item.date }>
              <DayCircle>
                <b>{moment(item.date, momentFriendlyDateFormat).format('DD')}</b>
                <span>{moment(item.date, momentFriendlyDateFormat).format('ddd')}</span>
              </DayCircle>
              <Icon
                icon={ weatherIcons[`icon${ padStart(item.iconCode, 2, '0') }`] }
                size={ 30 }
              />
              <Column>
                <Row>{ item.high }°C</Row>
                <Row>{ item.low }°C</Row>
              </Column>
              <Column>
                <Row bold>
                  <I18n>humidity</I18n>
                </Row>
                <Row>{item.humidity}%</Row>
              </Column>
              <Column>
                <Row bold>
                  <I18n>wind</I18n>
                </Row>
                <Row>{item.wind} km/h</Row>
              </Column>
              <Column>
                <Row bold>
                  <I18n>precipitation</I18n>
                </Row>
                <Row>{item?.precipitation} mm</Row>
              </Column>
            </DayCard>
          )) }
        </ContainerDays>
      </ScrollDays>
    </Container>
  )
}

export default WeatherForecast
