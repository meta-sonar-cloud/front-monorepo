import React, { useMemo, useCallback, useRef, useState, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'

import moment from 'moment'
import PropTypes from 'prop-types'

import filter from 'lodash/filter'
import find from 'lodash/find'
import findIndex from 'lodash/findIndex'
import isEmpty from 'lodash/isEmpty'
import size from 'lodash/size'
import sortBy from 'lodash/sortBy'
import uniqBy from 'lodash/uniqBy'

import { ThemeProvider } from '@material-ui/core/styles'

import I18n, { useT } from '@smartcoop/i18n'
import { useSnackbar } from '@smartcoop/snackbar'
import { SateliteSvcActions } from '@smartcoop/stores/sateliteSvc'
import { selectIndicators, selectIndicatorsLoaded, selectHasMissingImages } from '@smartcoop/stores/sateliteSvc/selectorSateliteSvc'
import { colors } from '@smartcoop/styles'
import * as colorPalette from '@smartcoop/utils/colorPalette'
import { momentBackDateFormat } from '@smartcoop/utils/dates'
import CalendarPicker from '@smartcoop/web-components/CalendarPicker'
import ColorPalette from '@smartcoop/web-components/ColorPalette'
import Form from '@smartcoop/web-components/Form'
import CalendarButton from '@smartcoop/web-components/IconButton/CalendarButton'
import DownloadIconButton from '@smartcoop/web-components/IconButton/DownloadIconButton'
import InputSelect from '@smartcoop/web-components/InputSelect'
import Loader from '@smartcoop/web-components/Loader'
import Popover from '@smartcoop/web-components/Popover'

import DateTabs from './DateTabs'
import {
  Container,
  FormContainer,
  FormItem,
  InputGroup,
  DownloadIconContainer,
  LoaderContainer
} from './styles'
import { handleTiffDownload } from './utils'


const FieldIndexesNavigator = ({
  field,
  onChangeImage
}) => {
  const t = useT()
  const formRef = useRef()
  const dispatch = useCallback(useDispatch(), [])
  const snackbar = useSnackbar()

  const [activeTab, setActiveTab] = useState()
  const [activeDate, setActiveDate] = useState()
  const [gettingImage, setGettingImage] = useState(false)
  const [period, setPeriod] = useState({
    from: moment().subtract(1, 'months').format(momentBackDateFormat),
    to: moment().format(momentBackDateFormat)
  })
  const [indicator, setIndicator] = useState()
  const [isFirstRender, setIsFirstRender] = useState(true)

  const indicators = useSelector(selectIndicators)
  const indicatorsLoaded = useSelector(selectIndicatorsLoaded)
  const hasMissingImages = useSelector(selectHasMissingImages)
  const { detached } = useState(true)

  const maxCloudsPercent = useMemo(() => 50, [])

  const indicatorOptions = useMemo(
    () => [
      { label: t('base map'), value: 'trueColor' },
      { label: 'NDVI', value: 'NDVI' },
      { label: 'NDRE', value: 'NDRE' }
    ],
    [t]
  )
  const [calendarValue, setCalendarValue] = useState(period)

  const inputRef = useRef()

  const dates = useMemo(
    () => filter(detached ? period : calendarValue, (date) => !isEmpty(date)),
    [calendarValue, detached, period]
  )
  const indicatorsDates = useMemo(
    () => {
      if (isEmpty(indicators)) return []

      let formattedIndicators = indicators
        .filter(item => item.fieldId === field.id)
        .map(item => ({
          date: item.date,
          isImageTooCloudy: !item.url || item.cloudPercent > maxCloudsPercent,
          cloudPercent: item.cloudPercent
        }))

      formattedIndicators = sortBy(formattedIndicators, item => new Date(item.date))

      return uniqBy(formattedIndicators, item => item.date)
    }, [indicators, field.id, maxCloudsPercent]
  )

  const indicatorImage = useMemo(
    () => {
      if (
        activeDate
        && indicator
        && !isEmpty(indicators)
      ) {
        return find(indicators, { date: activeDate, indicator })?.url
      }
      return null
    },
    [activeDate, indicator, indicators]
  )

  const handleChangeActiveDate = useCallback(
    (_, newTab) => {
      setActiveDate(indicatorsDates[newTab]?.date)
      setActiveTab(newTab)
    },
    [indicatorsDates]
  )

  useEffect(() => {
    if (period.to) {
      dispatch(SateliteSvcActions.searchIndicatorsByDate({
        period,
        fieldId: field.id
      }))
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [dispatch, field.id, period.to])

  useEffect(() => {
    if (!isEmpty(indicatorsDates)) {
      const index = findIndex(indicatorsDates, item => !item.isImageTooCloudy)

      handleChangeActiveDate(undefined, index)
    }
  }, [handleChangeActiveDate, indicatorsDates])

  useEffect(() => {
    onChangeImage(indicatorImage)
  }, [indicatorImage, onChangeImage])

  useEffect(() => () => {
    dispatch(SateliteSvcActions.resetSateliteSvc())
  }, [dispatch])

  useEffect(() => {
    if (!isFirstRender && hasMissingImages) {
      snackbar.warning(t('the satellite images will be shown soon'))
    }
  }, [t, snackbar, hasMissingImages, isFirstRender])

  useEffect(() => {
    setIsFirstRender(false)
  }, [])

  const colorsPalette = useMemo(
    () => colorPalette[indicator] || [],
    [indicator]
  )

  const labels = useMemo(
    () => {
      switch(indicator) {
        case 'NDVI':
          return { startLabel: `0,3 ${ t('low') }`, endLabel: `0,9 ${ t('high') }` }
        case 'NDRE':
          return { startLabel: `0,1 ${ t('low') }`, endLabel: `0,5 ${ t('high') }` }
        default:
          return {}
      }
    },
    [indicator, t]
  )

  const downloadTiff = useCallback(() => {
    const payload = {
      indicator,
      date: activeDate,
      fieldId: field.id
    }

    setGettingImage(true)

    const onSuccess = (url) => {
      handleTiffDownload(url)
      setGettingImage(false)
    }

    const onError = () => setGettingImage(false)

    dispatch(SateliteSvcActions.getGeotiffImage(payload, onSuccess, onError))
  }, [indicator, activeDate, field.id, dispatch])

  const handleChangeDates = useCallback(
    (newDates) => {
      if (!detached && inputRef.current) {
        inputRef.current.setValue(newDates)
      }
      setPeriod(newDates)
      setCalendarValue(newDates)
    },
    [detached]
  )
  const handleCalendarChange = useCallback(
    (newDates) => {
      const [from = '', to = ''] = newDates || []

      handleChangeDates({ from, to })

      if (size(newDates) === 2) {
        // eslint-disable-next-line no-unused-expressions
        () => {}
      }
    },
    [handleChangeDates]
  )
  const onCloseCalendar = useCallback(
    (values) => {
      if (size(values) < 2) {
        handleChangeDates({ from: '', to: '' })
      }
    },
    [handleChangeDates]
  )

  const calendarOptions = useMemo(
    () => ({
      mode: 'range',
      onClose: onCloseCalendar
    }),
    [onCloseCalendar]
  )
  return (
    <>
      <ColorPalette
        colors={ colorsPalette }
        { ...labels }
        style={ {
          padding: 12,
          marginBottom: 2,
          width: 300,
          backgroundColor: colors.shadow,
          borderRadius: 4
        } }
      />

      <Container>
        <div>
          <Form ref={ formRef }>
            <FormContainer>
              <FormItem>
                <InputGroup>
                  <Popover
                    popoverStyle={ { zIndex: 1200 } }
                    anchorOrigin={ {
                      vertical: 'top',
                      horizontal: 'right'
                    } }
                    transformOrigin={ {
                      vertical: 330,
                      horizontal: 0
                    } }
                    popoverId="calendar"
                    Component={ CalendarButton }
                    ComponentProps={ {
                      edge: 'end'
                    } }
                  >
                    <div style={ { padding: '5px 5px 10px' } }>
                      <CalendarPicker
                        withoutInput
                        value={ dates }
                        onChange={ handleCalendarChange }
                        options={ calendarOptions }
                        { ...{ maxDate: moment().format(momentBackDateFormat) } }
                      />
                    </div>
                  </Popover>
                </InputGroup>
              </FormItem>
              <FormItem style={ { width: 160, color: colors.white } }>
                <ThemeProvider
                  theme={
                    (theme) => ({
                      ...theme,
                      overrides: {
                        ...theme.overrides,
                        MuiAutocomplete:{
                          root:{
                            '& *': {
                              color: `${ colors.white } !important`,
                              fill: `${ colors.white } !important`
                            }
                          }
                        }
                      }
                    })
                  }
                >
                  <InputSelect
                    detached
                    label={ t('index',{ howMany: 1 }) }
                    name="indicador"
                    options={ indicatorOptions }
                    onChange={ setIndicator }
                    value={ indicator }
                    fullWidth
                  />
                </ThemeProvider>
              </FormItem>

            </FormContainer>
          </Form>
        </div>
        {indicatorsLoaded ? (
          <>
            {(!isEmpty(indicatorsDates) && activeTab !== undefined && activeTab !== -1)
              ? (
                <>
                  <DateTabs
                    onChange={ handleChangeActiveDate }
                    value={ activeTab }
                    indicators={ indicatorsDates }
                    maxCloudsPercent={ maxCloudsPercent }
                  />
                  <DownloadIconContainer>
                    <DownloadIconButton
                      loading={ gettingImage }
                      onClick={ downloadTiff }
                      size={ 22 }
                      disabled={ !indicator }
                      tooltip={ t('download geotiff') }
                      color={ colors.white }
                    />
                  </DownloadIconContainer>
                </>
              ) : (
                <div
                  style={ {
                    flex: 1,
                    display: 'flex',
                    alignItems: 'center',
                    paddingLeft: 10,
                    color: colors.white
                  } }
                >
                  <I18n>no data found</I18n>
                </div>
              )}
          </>
        ) : (
          <LoaderContainer>
            <Loader width={ 45 } style={ { padding: 0 } } />
          </LoaderContainer>
        )}
      </Container>
    </>
  )
}

FieldIndexesNavigator.propTypes = {
  field: PropTypes.object.isRequired,
  onChangeImage: PropTypes.func
}

FieldIndexesNavigator.defaultProps = {
  onChangeImage: () => {}
}

export default FieldIndexesNavigator
