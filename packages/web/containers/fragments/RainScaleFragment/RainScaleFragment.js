import React from 'react'

import map from 'lodash/map'

import scale from './scale'
import { Container, Item } from './styles'

const RainScaleFragment = () => (
  <Container>
    { map(scale, item => (
      <Item key={ item.color } color={ item.color }>{item.text}</Item>
    )) }
  </Container>
)

export default RainScaleFragment
