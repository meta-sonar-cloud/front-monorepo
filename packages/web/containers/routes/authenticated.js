import React, { useMemo } from 'react'
import { useSelector } from 'react-redux'

import isEmpty from 'lodash/isEmpty'
import omitBy from 'lodash/omitBy'
import size from 'lodash/size'

import { useT } from '@smartcoop/i18n'
import {
  currency,
  currencySignRounded,
  field,
  organization,
  shoppingPlatform,
  socialNetwork,
  tractor,
  cattle,
  weatherStation,
  smallPlant,
  supplier,
  products3d,
  productGroup3d,
  productWall,
  home,
  dashboard,
  arrowsBackAndForth
} from '@smartcoop/icons'
import {
  selectUserIsSupplier,
  selectPermissionsLoaded,
  selectAllModules,
  selectPermissionWeatherStations } from '@smartcoop/stores/authentication/selectorAuthentication'
import { AVAILABLE_MODULES } from '@smartcoop/stores/module'
import { selectCurrentModule } from '@smartcoop/stores/module/selectorModule'
import { selectCurrentOwner } from '@smartcoop/stores/technical/selectorTechnical'
import Icon from '@smartcoop/web-components/Icon'

import ChooseModuleScreen from '../screens/authenticated/ChooseModuleScreen'
import BarterListScreen from '../screens/authenticated/commercialization/barter/BarterListScreen'
import CreateBarterScreen from '../screens/authenticated/commercialization/barter/CreateBarterScreen'
import ProductQuotationListScreen from '../screens/authenticated/commercialization/productQuotation/ProductQuotationListScreen'
import SecuritiesMovementListScreen from '../screens/authenticated/commercialization/securitiesMovement/SecuritiesMovementListScreen'
import NotificationsScreen from '../screens/authenticated/common/NotificationsScreen'
import SocialScreen from '../screens/authenticated/common/SocialScreen'
import Comments from '../screens/authenticated/common/SocialScreen/Comments/Comments'
import WeatherStationMapScreen from '../screens/authenticated/common/wheaterStation/WeatherStationMapScreen'
import DairyFarmDashboardScreen from '../screens/authenticated/digitalProperty/dairyFarm/DairyFarmDashboardScreen'
import ListMilkDeliveryScreen from '../screens/authenticated/digitalProperty/dairyFarm/ListMilkDeliveryScreen'
import ListMilkQualityScreen from '../screens/authenticated/digitalProperty/dairyFarm/ListMilkQualityScreen'
import ListPriceDataScreen from '../screens/authenticated/digitalProperty/dairyFarm/ListPriceDataScreen'
import RegisterAnimalsScreen from '../screens/authenticated/digitalProperty/dairyFarm/RegisterAnimalsScreen'
import FieldDetailsScreen from '../screens/authenticated/digitalProperty/field/FieldDetailsScreen'
import FieldListScreen from '../screens/authenticated/digitalProperty/field/FieldListScreen'
import FieldRegisterScreen from '../screens/authenticated/digitalProperty/field/FieldRegisterScreen'
import GrowingSeasonRegisterScreen from '../screens/authenticated/digitalProperty/growingSeason/GrowingSeasonRegisterScreen'
import MachineryListScreen from '../screens/authenticated/digitalProperty/machinery/MachineryListScreen'
import MachineryRegisterScreen from '../screens/authenticated/digitalProperty/machinery/MachineryRegisterScreen'
import ManagementRegisterScreen from '../screens/authenticated/digitalProperty/management/ManagementRegisterScreen/ManagementScreen'
import PestReportRegisterScreen from '../screens/authenticated/digitalProperty/pestReport/PestReportRegisterScreen'
import PropertyHomeScreen from '../screens/authenticated/digitalProperty/property/PropertyHomeScreen'
import RainRecordListScreen from '../screens/authenticated/digitalProperty/rainRecord/RainRecordList'
import EditProfileScreen from '../screens/authenticated/profile/EditProfileScreen'
import UserAccessScreen from '../screens/authenticated/profile/UserAccessScreen'
import CRMReportScreen from '../screens/authenticated/reports/CRMReportScreen'
import FieldsRankingReportScreen from '../screens/authenticated/reports/FieldsRankingReportScreen'
import AcceptProposalScreen from '../screens/authenticated/shoppingPlatform/order/AcceptProposalScreen'
import JoinOrderScreen from '../screens/authenticated/shoppingPlatform/order/JoinOrderScreen'
import NonComplianceScreen from '../screens/authenticated/shoppingPlatform/order/NonComplianceScreen'
import OrderDetailsScreen from '../screens/authenticated/shoppingPlatform/order/OrderDetailsScreen'
import OrderListScreen from '../screens/authenticated/shoppingPlatform/order/OrderListScreen'
import RegisterOrderScreen from '../screens/authenticated/shoppingPlatform/order/RegisterOrderScreen'
import CreateOrganizationScreen from '../screens/authenticated/shoppingPlatform/organization/CreateOrganizationScreen'
import ListOrganizationScreen from '../screens/authenticated/shoppingPlatform/organization/ListOrganizationScreen'
import CreateProductGroupScreen from '../screens/authenticated/shoppingPlatform/productGroup/CreateProductGroupScreen'
import ListProductGroupScreen from '../screens/authenticated/shoppingPlatform/productGroup/ListProductGroupScreen'
import EditProductScreen from '../screens/authenticated/shoppingPlatform/products/EditProductScreen'
import ProductsListScreen from '../screens/authenticated/shoppingPlatform/products/ProductsListScreen'
import RegisterProductScreen from '../screens/authenticated/shoppingPlatform/products/RegisterProductScreen'
import ProductWall from '../screens/authenticated/shoppingPlatform/productWall/ProductWall'
import ProductWallComments from '../screens/authenticated/shoppingPlatform/productWall/ProductWallComments'
import SupplierQuotationDetailsScreen from '../screens/authenticated/shoppingPlatform/quotation/SupplierQuotationDetailsScreen'
import SupplierQuotationListScreen from '../screens/authenticated/shoppingPlatform/quotation/SupplierQuotationListScreen'
import CreateSupplierScreen from '../screens/authenticated/shoppingPlatform/supplier/CreateSupplierScreen'
import EditSupplierScreen from '../screens/authenticated/shoppingPlatform/supplier/EditSupplierScreen'
import ListSupplierScreen from '../screens/authenticated/shoppingPlatform/supplier/ListSupplierScreen'
import TechnicalAreaScreen from '../screens/authenticated/technical/TechnicalAreaScreen'
import TechnicalCommercializationProductsListScreen from '../screens/authenticated/technical/TechnicalCommercializationProductsListScreen'

export const useRoutes = () => {
  const t = useT()

  const currentModule = useSelector(selectCurrentModule)
  const allModules = useSelector(selectAllModules)
  const userIsSupplier = useSelector(selectUserIsSupplier)
  const permissionsLoaded = useSelector(selectPermissionsLoaded)
  const technicalCurrentOwner = useSelector(selectCurrentOwner)
  const permissionWeatherStations = useSelector(selectPermissionWeatherStations)

  const withoutModuleRoutes = useMemo(() => {
    let response = {}
    if (!isEmpty(allModules)) {
      response = {
        ...response,
        chooseModule: {
          path: '/',
          exact: true,
          Component: ChooseModuleScreen,
          disabled: size(allModules) <= 1
        }
      }
    }
    return response
  }, [allModules])

  const commonRoutes = useMemo(
    () => ({
      // pages like "notifications" or "user profile" can be here
      notifications: {
        path: '/notifications',
        exact: true,
        Component: NotificationsScreen
      },
      editProfile: {
        path: '/profile/edit',
        exact: true,
        Component: EditProfileScreen
      },
      userAccess: {
        path: '/profile/user-access',
        exact: true,
        Component: UserAccessScreen
      }
    }),
    []
  )

  const socialNetworkRoutes = useMemo(
    () => ({
      social: {
        path: '/social',
        exact: true,
        Component: SocialScreen,
        disabled: userIsSupplier,
        menu: {
          exact: false,
          Icon: (props) => <Icon icon={ socialNetwork } { ...props } />,
          title: t('network')
        }
      },
      socialComments: {
        path: '/social/comments/:postId',
        exact: true,
        Component: Comments,
        disabled: userIsSupplier
      }
    }),
    [t, userIsSupplier]
  )

  const weatherStationsRoutes = useMemo(
    () => ({
      weatherStation: {
        path: '/weather-stations',
        exact: true,
        Component: WeatherStationMapScreen,
        menu: {
          exact: false,
          Icon: (props) => <Icon icon={ weatherStation } { ...props } />,
          title: t('weather station', { howMany: 2 })
        }
      }
    }),
    [t]
  )

  const propertyRoutes = useMemo(
    () => ({
      // propertyList: {
      //   path: '/digital-property/properties',
      //   exact: true,
      //   Component: PropertyListScreen,
      //   menu: {
      //     exact: true,
      //     Icon: (props) => <Icon icon={ barn } { ...props } />,
      //     title: t('property', { howMany: 2 })
      //   }
      // },
      propertyHome: {
        path: '/digital-property',
        exact: true,
        Component: PropertyHomeScreen,
        menu: {
          exact: true,
          Icon: (props) => <Icon icon={ home } size={ 28 } { ...props } />,
          title: t('property', { howMany: 1 })
        }
      },
      fieldList: {
        path: '/digital-property/field',
        exact: true,
        Component: FieldListScreen,
        menu: {
          exact: false,
          Icon: (props) => <Icon icon={ field } size={ 22 } { ...props } />,
          title: t('field', { howMany: 2 })
        }
      },
      machineryList: {
        path: '/digital-property/machinery',
        exact: true,
        Component: MachineryListScreen,
        menu: {
          exact: true,
          Icon: (props) => <Icon icon={ tractor } { ...props } />,
          title: t('machinery', { howMany: 2 })
        }
      },
      machineryRegister: {
        path: '/digital-property/machinery/register',
        exact: true,
        Component: MachineryRegisterScreen
      },
      machineryEdit: {
        path: '/digital-property/machinery/edit',
        exact: true,
        Component: MachineryRegisterScreen
      },
      fieldRegister: {
        path: '/digital-property/field/register',
        exact: true,
        Component: FieldRegisterScreen
      },
      fieldEdit: {
        path: '/digital-property/field/edit',
        exact: true,
        Component: FieldRegisterScreen
      },
      fieldDetails: {
        path: '/digital-property/field/:fieldId',
        exact: true,
        Component: FieldDetailsScreen
      },
      growingSeasonRegister: {
        path: '/digital-property/field/growing-season/register',
        exact: true,
        Component: GrowingSeasonRegisterScreen
      },
      pestReportRegister: {
        path: '/digital-property/field/pest-report/register',
        exact: true,
        Component: PestReportRegisterScreen
      },
      pestReportEdit: {
        path: '/digital-property/field/pest-report/edit',
        exact: true,
        Component: PestReportRegisterScreen
      },
      managementRegister: {
        path: '/digital-property/field/management/register',
        exact: true,
        Component: ManagementRegisterScreen
      },
      dairyFarmDashboard: {
        path: '/digital-property/dairy-farm',
        exact: true,
        Component: DairyFarmDashboardScreen,
        menu: {
          exact: false,
          Icon: (props) => <Icon icon={ cattle } { ...props } />,
          title: t('dairy farm', { howMany: 2 })
        }
      },
      listMilkDelivery: {
        path: '/digital-property/dairy-farm/milk-delivery',
        exact: true,
        Component: ListMilkDeliveryScreen
      },
      listMilkQuality: {
        path: '/digital-property/dairy-farm/milk-quality',
        exact: true,
        Component: ListMilkQualityScreen
      },
      listPriceData: {
        path: '/digital-property/dairy-farm/price-data',
        exact: true,
        Component: ListPriceDataScreen
      },
      registerAnimals: {
        path: '/digital-property/dairy-farm/animals/register',
        exact: true,
        Component: RegisterAnimalsScreen
      },
      rainRecordList: {
        path: '/digital-property/rain-record',
        exact: true,
        Component: RainRecordListScreen
      },
      fieldsRankingReport: {
        path: '/digital-property/reports/ranking',
        exact: true,
        Component: FieldsRankingReportScreen,
        menu: {
          exact: false,
          Icon: (props) => <Icon icon={ dashboard } { ...props } />,
          title: t('fields ranking')
        }
      }
    }),
    [t]
  )

  const technicalRoutes = useMemo(
    () => {
      if (isEmpty(technicalCurrentOwner)) {
        return {
          fieldList: {
            path: '/technical',
            exact: true,
            Component: TechnicalAreaScreen,
            menu: {
              exact: false,
              Icon: (props) => <Icon icon={ smallPlant } { ...props } />,
              title: t('technical area')
            }
          }
        }
      }

      return {
        propertyHome: {
          path: '/technical/digital-property',
          exact: true,
          Component: PropertyHomeScreen,
          menu: {
            exact: true,
            Icon: (props) => <Icon icon={ home } size={ 28 } { ...props } />,
            title: t('property', { howMany: 1 })
          }
        },
        fieldList: {
          path: '/technical/digital-property/field',
          exact: true,
          Component: FieldListScreen,
          menu: {
            exact: false,
            Icon: (props) => <Icon icon={ field } size={ 22 } { ...props } />,
            title: t('field', { howMany: 2 })
          }
        },
        rainRecordList: {
          path: '/digital-property/rain-record',
          exact: true,
          Component: RainRecordListScreen
        },
        fieldRegister: {
          path: '/technical/digital-property/field/register',
          exact: true,
          Component: FieldRegisterScreen
        },
        fieldEdit: {
          path: '/technical/digital-property/field/edit',
          exact: true,
          Component: FieldRegisterScreen
        },
        fieldDetails: {
          path: '/technical/digital-property/field/:fieldId',
          exact: true,
          Component: FieldDetailsScreen
        },
        growingSeasonRegister: {
          path: '/technical/digital-property/field/growing-season/register',
          exact: true,
          Component: GrowingSeasonRegisterScreen
        },
        pestReportRegister: {
          path: '/technical/digital-property/field/pest-report/register',
          exact: true,
          Component: PestReportRegisterScreen
        },
        pestReportEdit: {
          path: '/technical/digital-property/field/pest-report/edit',
          exact: true,
          Component: PestReportRegisterScreen
        },
        managementRegister: {
          path: '/technical/digital-property/field/management/register',
          exact: true,
          Component: ManagementRegisterScreen
        },
        dairyFarmDashboard: {
          path: '/digital-property/dairy-farm',
          exact: true,
          Component: DairyFarmDashboardScreen,
          menu: {
            exact: false,
            Icon: (props) => <Icon icon={ cattle } { ...props } />,
            title: t('dairy farm', { howMany: 2 })
          }
        },
        listMilkDelivery: {
          path: '/digital-property/dairy-farm/milk-delivery',
          exact: true,
          Component: ListMilkDeliveryScreen
        },
        listMilkQuality: {
          path: '/digital-property/dairy-farm/milk-quality',
          exact: true,
          Component: ListMilkQualityScreen
        },
        listPriceData: {
          path: '/digital-property/dairy-farm/price-data',
          exact: true,
          Component: ListPriceDataScreen
        },
        registerAnimals: {
          path: '/digital-property/dairy-farm/animals/register',
          exact: true,
          Component: RegisterAnimalsScreen
        },
        fieldsRankingReport: {
          path: '/digital-property/reports/ranking',
          exact: true,
          Component: FieldsRankingReportScreen,
          menu: {
            exact: false,
            Icon: (props) => <Icon icon={ dashboard } { ...props } />,
            title: t('fields ranking')
          }
        },
        productQuotationList: {
          path: '/technical/commercialization/products-withdraw',
          exact: true,
          Component: TechnicalCommercializationProductsListScreen,
          menu: {
            exact: false,
            Icon: (props) => <Icon icon={ currencySignRounded } { ...props } />,
            title: t('commercialization')
          }
        }
      }
    },
    [t, technicalCurrentOwner]
  )

  const shoppingRoutes = useMemo(
    () => ({
      orderList: {
        path: '/shopping-platform/order',
        exact: true,
        Component: OrderListScreen,
        disabled: userIsSupplier,
        menu: {
          exact: false,
          Icon: (props) => <Icon icon={ shoppingPlatform } { ...props } />,
          title: t('order', { howMany: 2 })
        }
      },
      registerOrder: {
        path: '/shopping-platform/order/register',
        exact: true,
        disabled: userIsSupplier,
        Component: RegisterOrderScreen
      },
      orderDetails: {
        path: '/shopping-platform/order/:orderId',
        exact: true,
        disabled: userIsSupplier,
        Component: OrderDetailsScreen
      },
      nonComplianceOrganizationDetails: {
        path:
          '/shopping-platform/order/:orderId/non-compliance/:deliveryLocationId',
        exact: true,
        disabled: userIsSupplier,
        Component: NonComplianceScreen
      },
      acceptOrder: {
        path: '/shopping-platform/order/:orderId/accept',
        exact: true,
        disabled: userIsSupplier,
        Component: AcceptProposalScreen
      },

      joinOrder: {
        path: '/shopping-platform/order/:orderId/join',
        exact: true,
        disabled: userIsSupplier,
        Component: JoinOrderScreen
      },
      supplierQuotationList: {
        path: '/shopping-platform/supplier-quotation',
        exact: true,
        Component: SupplierQuotationListScreen,
        disabled: !userIsSupplier,
        menu: {
          exact: false,
          Icon: (props) => <Icon icon={ currency } { ...props } />,
          title: t('quotation', { howMany: 2 })
        }
      },
      supplierQuotationDetails: {
        path: '/shopping-platform/supplier-quotation/:supplierQuotationId',
        exact: true,
        Component: SupplierQuotationDetailsScreen,
        disabled: !userIsSupplier
      },
      nonComplianceSupplierDetails: {
        path:
          '/shopping-platform/supplier-quotation/:orderId/non-compliance',
        exact: true,
        disabled: !userIsSupplier,
        Component: NonComplianceScreen
      },
      listOrganization: {
        path: '/shopping-platform/organization',
        exact: true,
        Component: ListOrganizationScreen,
        disabled: userIsSupplier,
        menu: {
          exact: false,
          Icon: (props) => <Icon icon={ organization } size={ 30 } { ...props } />,
          title: t('organization', { howMany: 2 })
        }
      },
      createOrganization: {
        path: '/shopping-platform/organization/create',
        exact: true,
        Component: CreateOrganizationScreen,
        disabled: userIsSupplier
      },
      editOrganization: {
        path: '/shopping-platform/organization/edit',
        exact: true,
        Component: CreateOrganizationScreen,
        disabled: userIsSupplier
      },
      listSupplier: {
        path: '/shopping-platform/supplier',
        exact: true,
        Component: ListSupplierScreen,
        disabled: userIsSupplier,
        menu: {
          exact: false,
          Icon: (props) => <Icon icon={ supplier } size={ 30 } { ...props } />,
          title: t('supplier', { howMany: 2 })
        }
      },
      createSupplier: {
        path: '/shopping-platform/supplier/create',
        exact: true,
        Component: CreateSupplierScreen,
        disabled: userIsSupplier
      },
      editSupplier: {
        path: '/shopping-platform/supplier/:organizationId/edit',
        exact: true,
        Component: EditSupplierScreen,
        disabled: userIsSupplier
      },
      productList: {
        path: '/shopping-platform/product',
        exact: true,
        Component: ProductsListScreen,
        disabled: userIsSupplier,
        menu: {
          exact: false,
          Icon: (props) => <Icon icon={ products3d } { ...props } />,
          title: t('products')
        }
      },
      registerProduct: {
        path: '/shopping-platform/product/register',
        exact: true,
        disabled: userIsSupplier,
        Component: RegisterProductScreen
      },
      editProduct: {
        path: '/shopping-platform/product/edit',
        exact: true,
        disabled: userIsSupplier,
        Component: EditProductScreen
      },
      listProductGroup: {
        path: '/shopping-platform/product-group',
        exact: true,
        Component: ListProductGroupScreen,
        disabled: userIsSupplier,
        menu: {
          exact: false,
          Icon: (props) => <Icon icon={ productGroup3d } { ...props } />,
          title: t('product group')
        }
      },
      createProductGroup: {
        path: '/shopping-platform/product-group/create',
        exact: true,
        Component: CreateProductGroupScreen,
        disabled: userIsSupplier
      },
      editProductGroup: {
        path: '/shopping-platform/product-group/edit',
        exact: true,
        Component: CreateProductGroupScreen,
        disabled: userIsSupplier
      },
      productWall: {
        path: '/product-wall',
        exact: true,
        Component: ProductWall,
        disabled: userIsSupplier,
        menu: {
          exact: false,
          Icon: (props) => <Icon icon={ productWall } { ...props } />,
          title: t('product wall')
        }
      },
      productWallComments: {
        path: '/product-wall/comments/:postId',
        exact: true,
        Component: ProductWallComments,
        disabled: userIsSupplier
      }
    }),
    [t, userIsSupplier]
  )

  const commercializationRoutes = useMemo(
    () => ({
      productQuotationList: {
        path: '/commercialization',
        exact: true,
        Component: ProductQuotationListScreen,
        menu: {
          exact: false,
          Icon: (props) => <Icon icon={ currencySignRounded } { ...props } />,
          title: t('commercialization')
        }
      },
      securitiesMovementList: {
        path: '/commercialization/securities-movement',
        exact: true,
        Component: SecuritiesMovementListScreen
      },
      barterList: {
        path: '/barter',
        exact: true,
        Component: BarterListScreen,
        menu: {
          exact: false,
          Icon: (props) => <Icon icon={ arrowsBackAndForth } { ...props } />,
          title: t('barter')
        }
      },
      createBarter: {
        path: '/barter/new',
        exact: true,
        Component: CreateBarterScreen
      },
      editBarter: {
        path: '/barter/:barterOrderNumber/edit',
        exact: true,
        Component: CreateBarterScreen
      }
    }),
    [t]
  )

  const administrationRoutes = useMemo(
    () => ({
      crmReport: {
        path: '/reports/crm',
        exact: true,
        Component: CRMReportScreen,
        menu: {
          exact: false,
          Icon: (props) => <Icon icon={ dashboard } { ...props } />,
          title: t('technical visits')
        }
      }
    }),
    [t]
  )

  const routes = useMemo(() => {
    if (!permissionsLoaded) {
      return {}
    }

    const getRoutesByModule = () => {
      switch (currentModule) {
        case AVAILABLE_MODULES.digitalProperty:
          return {
            ...propertyRoutes,
            ...commonRoutes
          }
        // TODO: Recolocar Rede social em HML/PRD eventualmente
        case AVAILABLE_MODULES.shoppingPlatform:
          return process.env.REACT_APP_AMBIENTE === 'development' ? {
            ...shoppingRoutes,
            ...commonRoutes,
            ...socialNetworkRoutes
          } : {
            ...shoppingRoutes,
            ...commonRoutes
          }
        // TODO: Recolocar Rede social em HML/PRD eventualmente
        case AVAILABLE_MODULES.commercialization:
          return process.env.REACT_APP_AMBIENTE === 'development' ? {
            ...commercializationRoutes,
            ...commonRoutes,
            ...socialNetworkRoutes
          } : {
            ...commercializationRoutes,
            ...commonRoutes
          }
        case AVAILABLE_MODULES.technical:
          if (permissionWeatherStations) {
            return {
              ...technicalRoutes,
              ...commonRoutes,
              ...weatherStationsRoutes
            }
          }
          return {
            ...technicalRoutes,
            ...commonRoutes
          }
        case AVAILABLE_MODULES.administration:
          return {
            ...administrationRoutes,
            ...weatherStationsRoutes
          }
        default:
          return withoutModuleRoutes
      }
    }
    return omitBy(getRoutesByModule(), (route) => route.disabled)
  }, [permissionsLoaded, currentModule, propertyRoutes, commonRoutes, shoppingRoutes, socialNetworkRoutes, commercializationRoutes, permissionWeatherStations, technicalRoutes, administrationRoutes, weatherStationsRoutes, withoutModuleRoutes])

  return { routes }
}
