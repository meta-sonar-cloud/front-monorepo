/* eslint-disable import/no-cycle */
import { useMemo } from 'react'

import CreatePasswordScreen from '../screens/guest/CreatePasswordScreen'
import IdentificationScreen from '../screens/guest/IdentificationScreen'
import LoginScreen from '../screens/guest/LoginScreen'
import OrganizationsScreen from '../screens/guest/OrganizationsScreen'
import RecoverPasswordScreen from '../screens/guest/RecoverPasswordScreen'
import ResetCodeScreen from '../screens/guest/ResetCodeScreen'
import PrivacyTermScreen from '../screens/guest/term/PrivacyTermScreen'
import UseTermScreen from '../screens/guest/term/UseTermScreen'

export const useRoutes = () => {
  const routes = useMemo(() => ({
    login: {
      path: '/',
      exact: true,
      Component: LoginScreen
    },
    identification: {
      path: '/sign-up/identification',
      exact: true,
      Component: IdentificationScreen
    },
    organizations: {
      path: '/sign-up/organizations',
      exact: true,
      Component: OrganizationsScreen
    },
    resetCode: {
      path: '/reset-code',
      exact: true,
      Component: ResetCodeScreen
    },
    createPassword: {
      path: '/create-password',
      exact: true,
      Component: CreatePasswordScreen
    },
    recoverPassword: {
      path: '/recover-password',
      exact: true,
      Component: RecoverPasswordScreen
    },
    privacyTermScreen: {
      path: '/term/privacy',
      exact: true,
      Component: PrivacyTermScreen
    },
    useTermScreen: {
      path: '/term/use',
      exact: true,
      Component: UseTermScreen
    }
  }), [])

  return { routes }
}
