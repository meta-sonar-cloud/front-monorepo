import React from 'react'

import moment from 'moment/moment'
import PropTypes from 'prop-types'

import I18n from '@smartcoop/i18n'
import { edit, frost, hail, trash } from '@smartcoop/icons'
import { momentFriendlyDateFormat } from '@smartcoop/utils/dates'
import { formatNumber } from '@smartcoop/utils/formatters'
import Icon from '@smartcoop/web-components/Icon'
import IconButton from '@smartcoop/web-components/IconButton'
import { ButtonsContainer } from '@smartcoop/web-containers/layouts/AuthenticatedLayout/theme'

import { Line, ItemColumn, Container, ButtonLabel, ItemTitle } from './styles'

const DetailRainRecord = (props) => {
  const { rainRecord, handleEdit, handleDelete } = props

  return (
    <Container>
      <Line>
        <ItemColumn>
          <ItemTitle><I18n>rain detail date</I18n></ItemTitle>
          <span>{moment(rainRecord.occurrenceDate).format(momentFriendlyDateFormat)}</span>
        </ItemColumn>
        <ItemColumn>
          <ItemTitle><I18n>rain quantity</I18n></ItemTitle>
          <span>{`${ formatNumber(rainRecord.rainMm) } mm`}</span>
        </ItemColumn>
      </Line>
      <Line>
        <ItemColumn>
          <ItemTitle><I18n>rain detail record</I18n></ItemTitle>
          {rainRecord.events.map((item) => {
            if( item === 'frost' ){
              return (<Line key={ `event-${ item }` }>
                <Icon icon={ frost } />
                <span key={ `event-${ item }` }><I18n>frost</I18n></span>
              </Line>)
            }
            if( item === 'hail' ){
              return (
                <Line>
                  <Icon icon={ hail } />
                  <span key={ `event-${ item }` }><I18n>hail</I18n></span>
                </Line>
              )
            }
            return <></>
          })}
        </ItemColumn>
      </Line>
      <ButtonsContainer style={ { justifyContent: 'center' } }>
        <IconButton
          id="edit"
          onClick={ () => handleEdit() }
          variant="text"
          style={ { marginRight: 7 } }
        >
          <Icon icon={ edit } size={ 14 } style={ { marginRight: '5px' } }/>
          <ButtonLabel><I18n>edit rain record</I18n></ButtonLabel>
        </IconButton>
        <IconButton
          id="delete"
          onClick={ () => handleDelete() }
          variant="text"
          style={ { marginLeft: 7 } }
        >
          <Icon icon={ trash }  color="#F00" size={ 14 } style={ { marginRight: '5px' } }/>
          <ButtonLabel><I18n>delete rain record</I18n></ButtonLabel>
        </IconButton>
      </ButtonsContainer>
    </Container>
  )
}


DetailRainRecord.propTypes = {
  rainRecord: PropTypes.object,
  handleEdit: PropTypes.func,
  handleDelete: PropTypes.func
}

DetailRainRecord.defaultProps = {
  rainRecord: {},
  handleEdit: () => {},
  handleDelete: () => {}
}

export default DetailRainRecord
