import React from 'react'

import PropTypes from 'prop-types'

import isEmpty from 'lodash/isEmpty'
import map from 'lodash/map'

import I18n, { useT } from '@smartcoop/i18n'
import { emptyCrop } from '@smartcoop/icons'
import EmptyState from '@smartcoop/web-components/EmptyState'
import GrowingSeasonHistoryItem from '@smartcoop/web-components/GrowingSeasonHistoryItem'
import Modal from '@smartcoop/web-components/Modal'

import { Divisor, Row, Content } from './styles'

const GrowingSeasonsHistoryModal = ({ id, open, growingSeasonsHistory, polygonCoordinates, area }) => {
  const t = useT()

  return (
    <Modal
      id={ id }
      open={ open }
      title={
        <Row>
          <I18n>field history</I18n>
        </Row>
      }
      disableFullScreen
      escape
      // classes={ { paper: classes.modalBackground } }
      contentContainerStyle={ { padding: 0, minWidth: 350, maxHeight: 400 } }
      headerProps={ {
        disableTypography: true
      } }
    >
      <>
        <Divisor/>
        <Content>
          { !isEmpty(growingSeasonsHistory) ? (
            map(growingSeasonsHistory, growingSeason => (
              <GrowingSeasonHistoryItem
                key={ growingSeason.id }
                growingSeason={ growingSeason }
                polygonCoordinates={ polygonCoordinates }
                area={ area }
              />
            ))
          ) : (

            <EmptyState
              icon={ emptyCrop }
              text={ t('no crops registered') }
            />
          )}
        </Content>
      </>
    </Modal>
  )}

GrowingSeasonsHistoryModal.propTypes = {
  id: PropTypes.string.isRequired,
  open: PropTypes.bool.isRequired,
  // handleClose: PropTypes.func.isRequired,
  growingSeasonsHistory: PropTypes.array,
  polygonCoordinates: PropTypes.array,
  area: PropTypes.number
}

GrowingSeasonsHistoryModal.defaultProps = {
  growingSeasonsHistory: [],
  polygonCoordinates: [],
  area: 0
}

export default GrowingSeasonsHistoryModal
