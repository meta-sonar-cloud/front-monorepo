import React, { useRef } from 'react'

import PropTypes from 'prop-types'

import { useT } from '@smartcoop/i18n'
import Modal from '@smartcoop/web-components/Modal'
import TechnicalVisitForm from '@smartcoop/web-containers/forms/digitalProperty/technicalVisit/TechnicalVisitForm'

const TechnicalVisitModal = (props) => {
  const { id, open, cultivateName, technicalVisit, disabled, onSuccess, handleClose } = props
  const technicalVisitRegisterFormRef = useRef(null)
  const t = useT()

  return (
    <Modal
      id={ id }
      open={ open }
      title={ t('register technical visit') }
      disableFullScreen
    >
      <TechnicalVisitForm
        ref={ technicalVisitRegisterFormRef }
        cultivateName={ cultivateName }
        handleClose={ handleClose }
        technicalVisit={ technicalVisit }
        disabled={ disabled }
        onSuccess={ onSuccess }
      />
    </Modal>
  )
}

TechnicalVisitModal.propTypes = {
  id: PropTypes.string.isRequired,
  open: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  cultivateName: PropTypes.string,
  technicalVisit: PropTypes.object,
  disabled: PropTypes.bool,
  onSuccess: PropTypes.func
}

TechnicalVisitModal.defaultProps = {
  cultivateName: '',
  technicalVisit: {},
  disabled: false,
  onSuccess: () => {}
}

export default TechnicalVisitModal
