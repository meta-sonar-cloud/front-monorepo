import React, { useMemo, useState, useCallback } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import PropTypes from 'prop-types'


import { find } from 'lodash'

import { useDialog } from '@smartcoop/dialog'
import I18n, { useT } from '@smartcoop/i18n'
import { useSnackbar } from '@smartcoop/snackbar'
import { AuthenticationActions } from '@smartcoop/stores/authentication'
import { selectTerms } from '@smartcoop/stores/authentication/selectorAuthentication'
import Button from '@smartcoop/web-components/Button'
import Loader from '@smartcoop/web-components/Loader'
import Modal from '@smartcoop/web-components/Modal'

import AcceptTermModalConfirm from '../AcceptTermModalConfirm/AcceptTermModalConfirm'
import {
  Container,
  Content,
  Buttons,
  Title,
  Term,
  Item,
  AcceptCheckbox
} from './styles'

const AcceptTermModal = (props) => {
  const {
    id,
    open,
    slug,
    viewOnly,
    handleClose,
    onWillClose,
    onCancel
  } = props

  const [isLoading, setIsLoading] = useState(false)
  const dispatch = useCallback(useDispatch(), [])
  const snackbar = useSnackbar()
  const t = useT()
  const { createDialog } = useDialog()

  const terms = useSelector(selectTerms)

  const [accepted, setAccepted] = useState(false)

  const onClose = useCallback(
    () => {
      onWillClose()
      handleClose()
    }, [handleClose, onWillClose]
  )

  const onSuccess = useCallback(
    () => {
      setIsLoading(false)
      snackbar.success(t('the term was accepted succesfully'))
      onClose()
    }, [onClose, snackbar, t]
  )

  const onError = useCallback(
    () => {
      setIsLoading(false)
    }, []
  )

  const openConfirmModal = useCallback(
    () => {
      createDialog({
        id: `confirm-term-modal-${  slug }`,
        Component: AcceptTermModalConfirm

      })
    }, [createDialog, slug]
  )

  const cancel = useCallback(
    () => {
      onCancel()
      openConfirmModal()
    }, [onCancel, openConfirmModal]
  )

  const onSubmit = useCallback(
    () => {
      if(!viewOnly) {
        setIsLoading(true)
        dispatch(AuthenticationActions.updateTerm(
          { slug },
          () => {onSuccess()},
          (error) => {onError(error)}
        ))
      }
      onClose()
    }, [dispatch, onClose, onError, onSuccess, slug, viewOnly]
  )

  const selectedTerm = useMemo(
    () => find(terms, { termType: { slug } }), [slug, terms]
  )

  const isAccepted = useMemo(
    () => accepted || viewOnly,
    [accepted, viewOnly]
  )

  return (
    <Modal
      id={ id }
      open={ open }
      escape={ false }
      title={ selectedTerm?.termType?.type }
      disableFullScreen
      maxWidth="md"
    >
      {isLoading ? <Loader /> :
        <Container>
          <Content>
            <Item>
              <I18n as={ Title }>
              please read the terms below with attention
              </I18n>
            </Item>
            <Term dangerouslySetInnerHTML={ { __html: (selectedTerm?.description) } } />
            <Item>
              <AcceptCheckbox
                label={ t('i declare that i read and im aware of the norms that are in the term above') }
                value={ accepted }
                checked={ isAccepted }
                onChange={ (value) => {setAccepted(!value)} }
              />
            </Item>
          </Content>
          <Buttons>
            {
              !viewOnly && <Button
                id="cancel-button"
                onClick={ cancel }
                style={ { flex: 1 } }
                variant="outlined"
              >
                <I18n>cancel</I18n>
              </Button>
            }

            <Button
              id="confirm-button"
              onClick={ onSubmit }
              style={ { flex: 1 } }
              disabled={ !isAccepted }
            >
              <I18n>confirm</I18n>
            </Button>
          </Buttons>
        </Container>}
    </Modal>
  )
}

AcceptTermModal.propTypes = {
  id: PropTypes.string.isRequired,
  open: PropTypes.bool.isRequired,
  viewOnly: PropTypes.bool,
  handleClose: PropTypes.func.isRequired,
  onWillClose: PropTypes.func,
  onCancel: PropTypes.func,
  slug: PropTypes.oneOf([
    'privacy-term',
    'social-network-term',
    'commercialization-term',
    'supplier-term',
    'organization-term',
    'use-term'
  ]).isRequired
}

AcceptTermModal.defaultProps = {
  onWillClose: () => {},
  onCancel: () => {},
  viewOnly: false
}



export default AcceptTermModal
