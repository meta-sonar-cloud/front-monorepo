import styled from 'styled-components'

import Typography from '@material-ui/core/Typography'

import colors from '@smartcoop/styles/colors'

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  align-items: center;
  width: 100%;
  padding: 0%;
  padding: 25px 30px;
`

export const HeaderText = styled(Typography)`
  font-size: 22px;
  font-weight: bold;
  text-align: center;
  margin-bottom: 22px;
  color: ${ colors.text };
`

export const MainText = styled(Typography)`
  text-align: center;
  color: ${ colors.text };
  font-weight: bold;
  font-size: 16px;
  margin: 22px 0 30px;
`
