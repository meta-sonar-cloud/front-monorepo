import React, { useCallback, useState, useMemo, useRef } from 'react'
import { useDispatch } from 'react-redux'

import PropTypes from 'prop-types'

import  { useT } from '@smartcoop/i18n'
import { useSnackbar } from '@smartcoop/snackbar'
import { AnimalActions } from '@smartcoop/stores/animal'
import Loader from '@smartcoop/web-components/Loader'
import Modal from '@smartcoop/web-components/Modal'
import RegisterPevForm from '@smartcoop/web-containers/forms/digitalProperty/dairyFarm/RegisterPevForm'

import useStyles, { Divisor } from './styles'

const RegisterPevModal = (props) => {
  const {
    id,
    open,
    handleClose,
    pev,
    reloadDataTable
  } = props

  const formRef = useRef(null)
  const classes = useStyles()
  const snackbar = useSnackbar()
  const dispatch = useCallback(useDispatch(), [])
  const t = useT()

  const isEditing = useMemo(() => !!pev?.id, [pev])

  const [isLoading, setIsLoading] = useState(false)

  const closeModal = useCallback(
    () => {
      setIsLoading(false)
      reloadDataTable()
      handleClose()
    }, [handleClose, reloadDataTable]
  )

  const onSuccess = useCallback(
    () => {
      snackbar.success(
        t(`your {this} was ${  isEditing ? 'edited' :'registered' }`, {
          howMany: 2,
          gender: 'male',
          this: 'PEV'
        })
      )
      closeModal()

    }, [closeModal, isEditing, snackbar, t]
  )

  const handleSubmit = useCallback(
    (data) => {
      setIsLoading(true)
      dispatch(AnimalActions.saveAnimalPev(
        { id: pev?.id, ...data },
        onSuccess,
        () => setIsLoading(false)
      ))
    },
    [dispatch, onSuccess, pev]
  )

  return (
    <Modal
      id={ id }
      open={ open }
      title={ t('register {this}', { this: 'PEV' }) }
      disableFullScreen
      escape
      contentContainerStyle={ { padding: 0, minWidth: 300 } }
      headerProps={ {
        titleClass: classes.title,
        disableTypography: true
      } }
    >
      <>
        <Divisor/>
        {
          isLoading ? <Loader width={ 100 } /> : (
            <RegisterPevForm
              ref={ formRef }
              defaultValues={ pev }
              onSubmit={ handleSubmit }
              onCancel={ closeModal }
              isEditing={ isEditing }
            />
          )
        }
      </>
    </Modal>
  )}

RegisterPevModal.propTypes = {
  id: PropTypes.string.isRequired,
  open: PropTypes.bool.isRequired,
  handleClose: PropTypes.func,
  pev: PropTypes.object,
  reloadDataTable: PropTypes.func
}

RegisterPevModal.defaultProps = {
  handleClose: () => {},
  pev: {},
  reloadDataTable: () => {}
}

export default RegisterPevModal
