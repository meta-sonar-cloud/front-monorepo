import styled from 'styled-components'

import { makeStyles } from '@material-ui/core/styles'
import Typography from '@material-ui/core/Typography'

import colors from '@smartcoop/styles/colors'
import fonts from '@smartcoop/styles/fonts'

export const Identifier = styled.div`
  display: flex;
  justify-content: flex-start;
  align-items: center;
  flex-wrap: wrap;
  margin-top: 14px;
  margin-bottom: 0.35em;
`

export const Text = styled(Typography)`
  font-size: ${ fonts.fontSize.M }px;
  font-weight: ${ fonts.fontWeight.regular };
  font-family: ${ fonts.fontFamilySans };
`

export const CNPJ = styled(Text)`
  font-weight: ${ fonts.fontWeight.semiBold };
`

export const Divisor = styled.hr`
  width: 100%;
  border: none;
  border-bottom: 0.935252px solid ${ colors.lightGrey };
  margin: 0;
`

export const Content = styled.div`
  display: flex;
  flex-direction: column;
  padding: 18px;
`

export const Row = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`

export const GridFile = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr;
  grid-column-gap: 14px;
  margin-top: 12px;
`

export const GroupLocation = styled.div`
  :not(:last-child) {
    margin-bottom: 18px;
  }
`

export default makeStyles({
  modalBackground: {
    backgroundColor: colors.backgroundHtml
  },

  title: {
    fontWeight: 600,
    fontSize: 16,
    lineHeight: '16px',
    padding: '12px 13px 10px 13px'
  }
})
