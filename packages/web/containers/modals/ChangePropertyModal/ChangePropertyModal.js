import React, { useMemo, useCallback } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import PropTypes from 'prop-types'

import find from 'lodash/find'
import isEmpty from 'lodash/isEmpty'
import map from 'lodash/map'


import { useDialog } from '@smartcoop/dialog'
import { useT } from '@smartcoop/i18n'
import { plus, barnRounded, barnRoundedBlackFilling } from '@smartcoop/icons'
import { PropertyActions } from '@smartcoop/stores/property'
import { selectProperties, selectCurrentProperty } from '@smartcoop/stores/property/selectorProperty'
import { selectUserCanWrite } from '@smartcoop/stores/user/selectorUser'
import colors from '@smartcoop/styles/colors'
import Icon from '@smartcoop/web-components/Icon'
import ItemSelection from '@smartcoop/web-components/ItemSelection'
import Modal from '@smartcoop/web-components/Modal'
import CreatePropertyModal from '@smartcoop/web-containers/modals/CreatePropertyModal'

import useStyles from './styles'

const ChangePropertyModal = ({ id, open, handleClose }) => {
  const classes = useStyles()
  const { createDialog, removeDialog } = useDialog()

  const dispatch = useCallback(useDispatch(), [])
  const t = useT()

  const properties = useSelector(selectProperties)
  const userWrite = useSelector(selectUserCanWrite)

  const currentProperty = useSelector(selectCurrentProperty)
  const beforeSelected = useMemo(
    () => currentProperty,
    // eslint-disable-next-line react-hooks/exhaustive-deps
    []
  )

  const options = useMemo(() => (
    map(properties, (item) => (
      {
        value: item.id,
        label: item.name
      }
    ))
  ), [properties])


  const onAddNewArea = useCallback(
    () => {
      removeDialog({ id })
      createDialog({
        id: 'create-property-onboarding',
        Component: CreatePropertyModal
      })
    },
    [createDialog, id, removeDialog]
  )

  const onChange = useCallback(
    (org) => {
      const property = find(properties, (item) => (item.id === org.value))
      dispatch(PropertyActions.saveCurrentProperty(property))
      handleClose()
    },
    [dispatch, handleClose, properties]
  )

  const selectedProperty = useMemo(
    () => {
      if (isEmpty(beforeSelected)) return {}

      return {
        value: beforeSelected.id,
        label: beforeSelected.name
      }
    },
    [beforeSelected]
  )

  const footerButton = () => (
    // eslint-disable-next-line
    <div className={ classes.AddNewAreaContainer } onClick={ () => {onAddNewArea()} }>
      <Icon icon={ plus } size={ 16 }/>
      {t('register new property')}
    </div>
  )

  return (
    <Modal
      id={ id }
      open={ open }
      title={ t('select one property') }
      maxWidth="xs"
      disableFullScreen
      escapeWhenLoading
      escape={ !isEmpty(beforeSelected) }
      classes={ { paper: classes.modalBackground } }
      contentContainerStyle={ {
        padding: '15px 0 0 0'
      } }
      headerProps={ {
        titleClass: classes.title
      } }
      FooterComponent={ userWrite ? footerButton : undefined }
    >
      <ItemSelection
        title={ !isEmpty(beforeSelected) ? t('other properties') : '' }
        options={ options }
        unselectedIcon={ barnRounded }
        unselectedIconColor={ colors.primary }
        selectedIcon={ barnRoundedBlackFilling }
        selectedIconColor={ colors.secondary }
        onChange={ onChange }
        selected={ selectedProperty }
      />
    </Modal>
  )}

ChangePropertyModal.propTypes = {
  id: PropTypes.string.isRequired,
  open: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired
}

export default ChangePropertyModal
