import React, { useCallback, useRef } from 'react'

import PropTypes from 'prop-types'

import I18n from '@smartcoop/i18n'
import Button from '@smartcoop/web-components/Button'
import Modal from '@smartcoop/web-components/Modal'
import SignatureOrderForm from '@smartcoop/web-containers/forms/commercialization/SignatureOrderForm'
import { ButtonsContainer } from '@smartcoop/web-containers/layouts/AuthenticatedLayout/theme'

import useStyles, { Row, Content } from './styles'

const SignatureOrderModal = ({ id, open, handleClose, onSuccess, onCancel }) => {
  const classes = useStyles()
  const signatureElectronicFormRef = useRef(null)

  const clearForm = useCallback(
    () => {
      signatureElectronicFormRef.current.reset()
      handleClose()
      onCancel()
    },
    [handleClose, onCancel]
  )

  return (
    <Modal
      id={ id }
      open={ open }
      title={
        <Row>
          <I18n>electronic confirmation</I18n>
        </Row>
      }
      disableFullScreen
      escape
      classes={ { paper: classes.modalBackground } }
      contentContainerStyle={ { width: 500, padding: 20, paddingTop: 0 } }
      onClose={ clearForm }
    >
      <>
        <Content>
          <SignatureOrderForm
            ref={ signatureElectronicFormRef }
            onSubmit={ onSuccess }
            handleClose={ handleClose }
            withoutSubmitButton
          />
          <ButtonsContainer style={ { marginTop: 18 } }>
            <Button
              id="cancel"
              onClick={ () => clearForm() }
              variant="outlined"
              style={ { marginRight: 7 } }
            >
              <I18n>cancel</I18n>
            </Button>
            <Button
              id="save"
              onClick={ () => signatureElectronicFormRef.current.submit() }
              color="black"
              style={ { marginLeft: 7 } }
            >
              <I18n>confirm</I18n>
            </Button>
          </ButtonsContainer>
        </Content>
      </>
    </Modal>
  )}

SignatureOrderModal.propTypes = {
  id: PropTypes.string.isRequired,
  open: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  onSuccess: PropTypes.func.isRequired,
  onCancel: PropTypes.func
}

SignatureOrderModal.defaultProps = {
  onCancel: () => {}
}

export default SignatureOrderModal
