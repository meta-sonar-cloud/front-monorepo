import React, {  useMemo } from 'react'

import moment from 'moment/moment'
import PropTypes from 'prop-types'

import map from 'lodash/map'

import Typography from '@material-ui/core/Typography'

import I18n, { useT } from '@smartcoop/i18n'
import { download, organizationRounded } from '@smartcoop/icons'
import { getBankSlips } from '@smartcoop/services/apis/smartcoopApi/resources/order'
import colors from '@smartcoop/styles/colors'
import fonts from '@smartcoop/styles/fonts'
import { momentFriendlyDateTimeFormat } from '@smartcoop/utils/dates'
import { formatCpfCnpj } from '@smartcoop/utils/formatters'
import Button from '@smartcoop/web-components/Button'
import DataTable from '@smartcoop/web-components/DataTable'
import Icon from '@smartcoop/web-components/Icon'
import Modal from '@smartcoop/web-components/Modal'

import useStyles, { Divisor, Content, Identifier, Text, CNPJ, GroupLocation } from './styles'

const BankSlipTable = ({ queryParams }) => {
  const t = useT()
  const columns = useMemo(() => [
    {
      title: t('archive', { howMany: 1 }),
      field: 'fileName'
    },
    {
      title: t('update / shipping'),
      field: 'updatedAt',
      render: (row) => (moment(row.updatedAt).format(momentFriendlyDateTimeFormat))
    },
    {
      title: '',
      field: 'download',
      render: (row) => (
        <div style={ { display: 'flex' } }>
          <a href={ row.fileUrl } download>
            <Button
              id="download-bank-splip"
              variant="outlined"
            >
              <>
                <Icon style={ { marginRight: 6 } } size={ 16 } icon={ download }/>
                <I18n>download</I18n>
              </>
            </Button>
          </a>
        </div>
      )
    }
  ], [t])
  return (
    <DataTable
      data={ getBankSlips }
      columns={ columns }
      queryParams ={ queryParams }
      urlParams={ queryParams }
      options={ {
        paging: false
      } }
    />
  )
}

BankSlipTable.propTypes = {
  queryParams: PropTypes.object.isRequired
}

const ViewBankSlipModal = ({ id, open, purchase: { organization, deliveryLocations } }) => {
  const classes = useStyles()

  return (
    <Modal
      id={ id }
      open={ open }
      title={
        <>
          <I18n>view documents</I18n>
          <Identifier>
            <Icon icon={ organizationRounded } color={ colors.green } size={ 25 } />
            <Typography
              variant="body1"
              component="h2"
              align="left"
              style={ {
                paddingLeft: 10,
                paddingRight: 30,
                fontWeight: fonts.fontWeight.bold
              } }
            >
              { organization.companyName }
            </Typography>
            <Text><I18n>company document</I18n> {formatCpfCnpj(organization.companyDocument)}</Text>
          </Identifier>
        </>
      }
      disableFullScreen
      escape
      classes={ { paper: classes.modalBackground } }
      contentContainerStyle={ { padding: 0, minWidth: 300 } }
      headerProps={ {
        titleClass: classes.title,
        disableTypography: true
      } }
    >
      <>
        <Divisor/>
        <Content>
          {map(deliveryLocations, location => (
            <GroupLocation key={ location.id }>
              <CNPJ><I18n>delivery location</I18n>: {formatCpfCnpj(location.organization.companyDocument)}</CNPJ>
              <BankSlipTable
                queryParams={
                  {
                    deliveryLocationId: location.id,
                    organizationPurchaseId: location.organizationPurchaseId
                  }
                }
              />
            </GroupLocation>
          ))}
        </Content>
      </>
    </Modal>
  )}

ViewBankSlipModal.propTypes = {
  id: PropTypes.string.isRequired,
  open: PropTypes.bool.isRequired,
  purchase: PropTypes.object.isRequired
}

export default ViewBankSlipModal
