import React, { useMemo, useCallback, useRef } from 'react'
import { useDispatch } from 'react-redux'

import PropTypes from 'prop-types'

import Divider from '@material-ui/core/Divider'

import userConfigurePermission from '@smartcoop/forms/schemas/property/userConfigurePermission.schema'
import I18n ,{ useT } from '@smartcoop/i18n'
import { useSnackbar } from '@smartcoop/snackbar'
import { TechnicalActions } from '@smartcoop/stores/technical'
import Button from '@smartcoop/web-components/Button'
import Form from '@smartcoop/web-components/Form'
import Modal from '@smartcoop/web-components/Modal'
import RadioGroup from '@smartcoop/web-components/RadioGroup'
import { ButtonsContainer } from '@smartcoop/web-containers/layouts/AuthenticatedLayout/theme'

import { ContainerDataUser, LabelText, TextUser, Row } from './styles'

const ConfigurePermissionModal = (props) => {
  const { id, open, userRequestItem, tableRef, handleClose } = props
  const t = useT()
  const configurePermissionFormRef = useRef()
  const dispatch = useCallback(useDispatch(), [])
  const snackbar = useSnackbar()

  const applicationModeOptions = useMemo(
    () => (
      [
        {
          label: 'Somente visualização',
          value: 4
        },
        {
          label: 'Visualização e edição',
          value: 5
        },
        {
          label: 'Negar acesso',
          value: 2
        }
      ]
    ),
    []
  )

  const permissions = useMemo(
    () => ({
      read: 4,
      readAndWrite: 5,
      denied: 2
    }),
    []
  )

  const handleSubmitConfigurePermission = useCallback(
    (data) => {
      if (data.permission === permissions.denied) {
        dispatch(TechnicalActions.revokeTechnicalAccess(
          userRequestItem.technicianProprietary?.id,
          data.permission,
          () => {
            snackbar.success(
              t('permission change performed successfully')
            )
            const { query } = tableRef.current.state
            tableRef.current.onQueryChange({
              ...query,
              page: 0
            })
          }
        ))
        handleClose()
      } else {
        dispatch(TechnicalActions.acceptTechnicianAccess(
          data.permission === permissions.readAndWrite,
          userRequestItem.technicianProprietary?.id,
          () => {
            snackbar.success(
              t('permission change performed successfully')
            )
            const { query } = tableRef.current.state
            tableRef.current.onQueryChange({
              ...query,
              page: 0
            })
          }
        ))
        handleClose()
      }
    },
    [dispatch, handleClose, permissions.denied, permissions.readAndWrite, snackbar, t, tableRef, userRequestItem.technicianProprietary.id]
  )

  return (
    <Modal
      id={ id }
      open={ open }
      title={
        <Row>
          <I18n>configure permission</I18n>
          <Divider/>
        </Row>
      }
      disableFullScreen
    >
      <>
        <ContainerDataUser>
          <div>
            <LabelText>
              <I18n params={ { howMany: 1 } }>cooperative</I18n>
            </LabelText>
            <TextUser>{userRequestItem.technicianProprietary?.organizations[0]?.tradeName}</TextUser>
          </div>
          <div>
            <LabelText>
              <I18n>user name</I18n>
            </LabelText>
            <TextUser>{userRequestItem.name}</TextUser>
          </div>
        </ContainerDataUser>
        <Form
          ref={ configurePermissionFormRef }
          schemaConstructor={ userConfigurePermission }
          onSubmit={ handleSubmitConfigurePermission }
        >
          <RadioGroup
            style={ { marginBottom: 20, maxWidth: 250 } }
            label={ t('permission') }
            name="permission"
            options={ applicationModeOptions }
            defaultValue={ userRequestItem.technicianProprietary.status || '' }
            variant="column"
          />

          <ButtonsContainer>
            <Button
              id="cancel-configure-permission-form"
              onClick={ handleClose }
              style={ { flex: 1, marginRight: 10 } }
              variant="outlined"
            >
              <I18n>cancel</I18n>
            </Button>

            <Button
              id="submit-configure-permission-form"
              onClick={ () => configurePermissionFormRef.current.submit() }
              style={ { flex: 1 } }
            >
              <I18n>save</I18n>
            </Button>
          </ButtonsContainer>
        </Form>
      </>
    </Modal>
  )
}

ConfigurePermissionModal.propTypes = {
  id: PropTypes.string.isRequired,
  open: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  userRequestItem: PropTypes.object,
  tableRef: PropTypes.any
}

ConfigurePermissionModal.defaultProps = {
  userRequestItem: {},
  tableRef: () => {}
}

export default ConfigurePermissionModal
