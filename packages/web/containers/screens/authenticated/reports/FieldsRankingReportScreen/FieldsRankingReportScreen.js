import React, { useState, useMemo } from 'react'
import { useSelector } from 'react-redux'

import I18n from '@smartcoop/i18n'
import useSmartcoopApi from '@smartcoop/services/hooks/useSmartcoopApi'
import { selectUser } from '@smartcoop/stores/user/selectorUser'
import { colors } from '@smartcoop/styles'
import Loader from '@smartcoop/web-components/Loader'
import MyFieldProductivityReportFragment from '@smartcoop/web-containers/fragments/reports/field/MyFieldProductivityReportFragment'
import MyFieldProductivityVsZoneReportFragment from '@smartcoop/web-containers/fragments/reports/field/MyFieldProductivityVsZoneReportFragment'

import {
  ButtonContainer,
  TabButton
} from './styles'

const FieldsRankingReportScreen = () => {
  const [reportType, setReportType] = useState('only-mine')
  const user = useSelector(selectUser)

  const { data: token, isValidating } = useSmartcoopApi('/auth/microsoft-token')

  const accessToken = useMemo(
    () => token?.accessToken,
    [token]
  )

  const userId = useMemo(() => user?.id, [user])

  const loading = useMemo(
    () => (!accessToken && isValidating) || !userId || !accessToken,
    [accessToken, isValidating, userId]
  )

  const ReportFragment = useMemo(
    () => {
      switch (reportType) {
        case 'mine-vs-others':
          return MyFieldProductivityVsZoneReportFragment
        case 'only-mine':
        default:
          return MyFieldProductivityReportFragment
      }
    },
    [reportType]
  )

  return loading ? (
    <Loader width={ 100 } />
  ) : (
    <div style={ { display: 'flex', flex: 1, flexDirection: 'column' } }>
      <ButtonContainer>
        <TabButton
          id="only-mine"
          onClick={ () => setReportType('only-mine') }
          color={ reportType === 'only-mine' ? colors.black : colors.white }
        >
          <I18n>my property</I18n>
        </TabButton>
        <TabButton
          id="mine-vs-others"
          onClick={ () => setReportType('mine-vs-others') }
          color={ reportType === 'mine-vs-others' ? colors.black : colors.white }
        >
          <I18n>my property vs region</I18n>
        </TabButton>
      </ButtonContainer>
      <div style={ { display: 'flex', flex: 1 } }>
        <ReportFragment token={ accessToken } userId={ userId }/>
      </div>
    </div>
  )
}

export default FieldsRankingReportScreen
