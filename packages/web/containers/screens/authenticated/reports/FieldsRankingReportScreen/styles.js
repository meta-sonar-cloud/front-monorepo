import styled from 'styled-components'

import Button from '@smartcoop/web-components/Button'

export const ButtonContainer = styled.div`
  display: flex;
  flex-direction: row;
  margin: 10px 5px;
`

export const TabButton = styled(Button)`
  flex: none;
  white-space: nowrap;
  margin-right: 12px;
`
