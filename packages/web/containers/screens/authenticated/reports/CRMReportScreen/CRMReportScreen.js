import React, { useState, useMemo } from 'react'
import { useSelector } from 'react-redux'

import I18n from '@smartcoop/i18n'
import useSmartcoopApi from '@smartcoop/services/hooks/useSmartcoopApi'
import { selectCurrentOrganization } from '@smartcoop/stores/organization/selectorOrganization'
import { colors } from '@smartcoop/styles'
import Loader from '@smartcoop/web-components/Loader'
import BenchMarkReportFragment from '@smartcoop/web-containers/fragments/reports/crm/BenchMarkReportFragment'
import DetecReportFragment from '@smartcoop/web-containers/fragments/reports/crm/DetecReportFragment'
import OcorrenciaReportFragment from '@smartcoop/web-containers/fragments/reports/crm/OcorrenciaReportFragment'

import {
  ButtonContainer,
  TabButton
} from './styles'

const CRMReportScreen = () => {
  const [reportType, setReportType] = useState('detec')
  const curentOrganization = useSelector(selectCurrentOrganization)

  const { data: token, isValidating } = useSmartcoopApi('/auth/microsoft-token')

  const accessToken = useMemo(
    () => token?.accessToken,
    [token]
  )

  const organizationId = useMemo(() => curentOrganization?.id, [curentOrganization])

  const loading = useMemo(
    () => (!accessToken && isValidating) || !organizationId || !accessToken,
    [accessToken, isValidating, organizationId]
  )

  const ReportFragment = useMemo(
    () => {
      switch (reportType) {
        case 'occurrence':
          return OcorrenciaReportFragment
        case 'benckmark':
          return BenchMarkReportFragment
        case 'detec':
        default:
          return DetecReportFragment
      }
    },
    [reportType]
  )

  return loading ? (
    <Loader width={ 100 } />
  ) : (
    <div style={ { display: 'flex', flex: 1, flexDirection: 'column' } }>
      <ButtonContainer>
        <TabButton
          id="detec"
          onClick={ () => setReportType('detec') }
          color={ reportType === 'detec' ? colors.black : colors.white }
        >
          <I18n>visits</I18n>
        </TabButton>
        <TabButton
          id="benckmark"
          onClick={ () => setReportType('benckmark') }
          color={ reportType === 'benckmark' ? colors.black : colors.white }
        >
          <I18n>benchmark</I18n>
        </TabButton>
        <TabButton
          id="occurrence"
          onClick={ () => setReportType('occurrence') }
          color={ reportType === 'occurrence' ? colors.black : colors.white }
        >
          <I18n>occurrences</I18n>
        </TabButton>
      </ButtonContainer>
      <div style={ { display: 'flex', flex: 1 } }>
        <ReportFragment token={ accessToken } organizationId={ organizationId }/>
      </div>
    </div>
  )
}

export default CRMReportScreen
