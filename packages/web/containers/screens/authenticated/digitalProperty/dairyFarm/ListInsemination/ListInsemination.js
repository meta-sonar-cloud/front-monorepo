import React, { useEffect, useRef, useCallback, useState, useMemo } from 'react'
import { useSelector, useDispatch } from 'react-redux'

import I18n, { useT } from '@meta-react/i18n'
import moment from 'moment'

import { isEmpty, map, values } from 'lodash'

import { useDialog } from '@smartcoop/dialog'
import { getInseminations as getInseminationsService } from '@smartcoop/services/apis/smartcoopApi/resources/insemination'
import { AnimalActions } from '@smartcoop/stores/animal'
import { selectCurrentAnimal } from '@smartcoop/stores/animal/selectorAnimal'
import { InseminationActions } from '@smartcoop/stores/insemination'
import { momentBackDateFormat, momentFriendlyDateFormat } from '@smartcoop/utils/dates'
import Button from '@smartcoop/web-components/Button'
import DataTable from '@smartcoop/web-components/DataTable'
import FilterButton from '@smartcoop/web-components/FilterButton'
import FilterInseminationModal from '@smartcoop/web-containers/modals/dairyFarm/FilterInseminationModal'
import RegisterInseminationModal from '@smartcoop/web-containers/modals/dairyFarm/RegisterInseminationModal'

import { Top } from './styles'

const ListInsemination = () => {
  const { createDialog } = useDialog()
  const t = useT()
  const tableRef = useRef(null)
  const dispatch = useCallback(useDispatch(), [])

  const [filters, setFilters] = useState({})
  const currentAnimal = useSelector(selectCurrentAnimal)

  const registerDisabled = useMemo(
    () => (
      (currentAnimal?.category !== 'vaca' &&
      currentAnimal?.category !== 'novilha') ||
      (currentAnimal?.animalStatus?.name !== 'Aptas' &&
      currentAnimal?.animalStatus?.name !== 'Vazia' &&
      currentAnimal?.animalStatus?.name !== 'Nenhum')
    ),[currentAnimal]
  )

  const columns = useMemo(
    () => [
      {
        title: t('earring'),
        field: 'animal.earring.earringCode'
      },
      {
        title: `${ t('name')  }/${  t('animal', { howMany: 1 }) }`,
        field: 'animal.name'
      },
      {
        title: t('date', { howMany: 1 }),
        field: 'inseminationDate',
        render: (row) => moment(row.inseminationDate, momentBackDateFormat).format(momentFriendlyDateFormat)
      },
      {
        title: t('bull\'s code'),
        field: 'bull.code'
      },
      {
        title: t('bull\'s name'),
        field: 'bull.name'
      },
      {
        title: t('type'),
        field: 'inseminationType',
        render: (row) => (map(row.inseminationType, ({ name }, index) => index === row.inseminationType.length - 1 ? name : `${ name  }, `))
      }
    ], [t]
  )

  const urlParams = useMemo(
    () => ({
      animalId: currentAnimal?.id
    }), [currentAnimal.id]
  )

  const reloadDataTable = useCallback(
    () => {
      tableRef.current.onQueryChange()
    },
    []
  )

  const handleSetFilters = useCallback(
    (filterValues) => {
      setFilters({
        ...filterValues,
        q: filterValues?.name ?? null
      })
    }, []
  )

  const openFilterModal = useCallback(
    () => {
      createDialog({
        id: 'filter-fields',
        Component: FilterInseminationModal,
        props: {
          onSubmit: handleSetFilters,
          filters
        }
      })
    },
    [createDialog, filters, handleSetFilters]
  )

  useEffect(
    () => {
      dispatch(InseminationActions.loadInseminationTypes())
    },[dispatch]
  )

  useEffect(
    () => {
      dispatch(AnimalActions.loadCurrentAnimal())
    },[dispatch]
  )

  const openCreateModal = useCallback(
    (_, insemination = {}) => {
      createDialog({
        id: 'create-fields',
        Component: RegisterInseminationModal,
        props: {
          onSubmit: reloadDataTable,
          insemination
        }
      })
    },
    [createDialog, reloadDataTable]
  )

  return (
    <>
      <Top>
        <div>
          <FilterButton
            onClick={ openFilterModal }
            isActive={ !values(filters).every(isEmpty) }
          />
        </div>
        <div>
          <Button
            id="register-insemination"
            onClick={ openCreateModal }
            color="secondary"
            disabled={ registerDisabled }
          >
            <I18n params={ { this: t('insemination') } }>{'register {this}'}</I18n>
          </Button>
        </div>
      </Top>
      <div style={ { marginTop: 20, width: 'calc(100% - 30px)' } }>
        <DataTable
          columns={ columns }
          data={ getInseminationsService }
          queryParams={ filters }
          tableRef={ tableRef }
          urlParams={ urlParams }
          onEditClick={ openCreateModal }
        />
      </div>
    </>
  )
}

export default ListInsemination
