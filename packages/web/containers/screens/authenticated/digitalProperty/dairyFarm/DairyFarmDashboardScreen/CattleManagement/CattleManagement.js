import React, { useRef, useCallback, useState, useMemo } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useHistory } from 'react-router-dom'

import I18n, { useT } from '@meta-react/i18n'
import moment from 'moment/moment'

import debounce from 'lodash/debounce'
import isArray from 'lodash/isArray'
import isBoolean from 'lodash/isBoolean'
import isEmpty from 'lodash/isEmpty'
import isObject from 'lodash/isObject'
import isString from 'lodash/isString'
import mapValues from 'lodash/mapValues'
import toString from 'lodash/toString'
import values from 'lodash/values'

import { ThemeProvider } from '@material-ui/core'

import { useDialog } from '@smartcoop/dialog'
import { getAnimals as getAnimalsService } from '@smartcoop/services/apis/smartcoopApi/resources/animal'
import { AnimalActions } from '@smartcoop/stores/animal'
import { selectCurrentProperty } from '@smartcoop/stores/property/selectorProperty'
import { AnimalStatusCode } from '@smartcoop/utils/constants'
import {
  momentBackDateFormat,
  momentBackDateTimeFormat,
  momentFriendlyDateFormat
} from '@smartcoop/utils/dates'
import Button from '@smartcoop/web-components/Button'
import DataTable from '@smartcoop/web-components/DataTable'
import FilterButton from '@smartcoop/web-components/FilterButton'
import VerticalDotsIconButton from '@smartcoop/web-components/IconButton/VerticalDotsIconButton'
import InputSearch from '@smartcoop/web-components/InputSearch'
import MenuPopUp from '@smartcoop/web-components/MenuPopUp'
import Popover from '@smartcoop/web-components/Popover'
import FilterCattleManagementModal from '@smartcoop/web-containers/modals/dairyFarm/FilterCattleManagementModal'
import RegisterAnimalBirthModal from '@smartcoop/web-containers/modals/dairyFarm/RegisterAnimalBirthModal/RegisterAnimalBirthModal'
import RegisterDiagnosisActionsModal from '@smartcoop/web-containers/modals/dairyFarm/RegisterDiagnosisActionsModal'
import RegisterInseminationModal from '@smartcoop/web-containers/modals/dairyFarm/RegisterInseminationModal/RegisterInseminationModal'
import RegisterPevModal from '@smartcoop/web-containers/modals/dairyFarm/RegisterPevModal'
import RegisterPregnancyActionsModal from '@smartcoop/web-containers/modals/dairyFarm/RegisterPregnancyActionsModal'
import { useRoutes } from '@smartcoop/web-containers/routes/authenticated'

import { Top, inputSearchTheme } from './styles'

const CattleManagement = () => {
  const { createDialog } = useDialog()
  const t = useT()
  const tableRef = useRef(null)
  const { routes } = useRoutes()
  const history = useHistory()
  const dispatch = useCallback(useDispatch(), [])

  const [filters, setFilters] = useState({})
  const [filterText, setFilterText] = useState('')
  const [debouncedFilterText, setDebouncedFilterText] = useState('')
  const currentProperty = useSelector(selectCurrentProperty)

  const handleOnRowClick = useCallback(
    (_, currentAnimal = {}) => {
      dispatch(AnimalActions.setCurrentAnimal(currentAnimal))
      dispatch(AnimalActions.setIsDetail(true))
      history.push(routes.registerAnimals.path)
    },
    [dispatch, history, routes.registerAnimals.path]
  )

  const reloadDataTable = useCallback(() => {
    tableRef.current.onQueryChange()
  }, [])

  const registerActionModal = useMemo(
    () => ({
      insemination: RegisterInseminationModal,
      calving: RegisterAnimalBirthModal,
      pregnancyAction: RegisterPregnancyActionsModal,
      pregnancyDiagnosis: RegisterDiagnosisActionsModal,
      pev: RegisterPevModal
    }),
    []
  )

  const openRegisterActionModal = useCallback(
    (type) => {
      createDialog({
        id: 'create-insemination',
        Component: registerActionModal[type],
        props: {
          onSubmit: reloadDataTable,
          onAccept: (currentAnimal) => {history.push(routes.registerAnimals.path, { currentAnimal })}
        }
      })
    },
    [createDialog, history, registerActionModal, reloadDataTable, routes.registerAnimals.path]
  )

  const handleDotsClick = useCallback(
    (row, type) => {
      dispatch(AnimalActions.setCurrentAnimal(row))
      openRegisterActionModal(type)
    },
    [dispatch, openRegisterActionModal]
  )

  const changeNextAction = useMemo(
    () => ({
      inseminar: 'Inseminar',
      diagnostico_1toque: 'Diagnóstico primeiro toque',
      secagem: 'Secagem',
      pre_parto: 'Pré-parto',
      parto: 'Parto',
      liberar_pev: 'Liberar PEV',
      diagnostico_confirmar: 'Diagnóstico confirmar'
    }),
    []
  )

  const columns = useMemo(
    () => [
      {
        title: '',
        disableClick: true,
        render: (row) => (
          <div>
            <Popover
              popoverId="user-menu"
              popoverStyle={ { width: 250 } }
              Component={ VerticalDotsIconButton }
            >
              <div>
                <MenuPopUp
                  options={ [
                    {
                      icon: null,
                      text: t('register insemination'),
                      onClick: () => {
                        handleDotsClick(row, 'insemination')
                      },
                      disabled:
                        (row.category !== 'vaca' &&
                        row.category !== 'novilha') ||
                        (row.animalStatus.id !== AnimalStatusCode.VAZIA &&
                          row.animalStatus.id !== AnimalStatusCode.APTAS &&
                          row.animalStatus.id !== AnimalStatusCode.NENHUM)
                    },
                    {
                      icon: null,
                      text: t('register {this}', {
                        this: t('diagnosis', { howMany: 1 })
                      }),
                      disabled:
                        (row?.category !== 'vaca' &&
                        row?.category !== 'novilha') ||
                        (row.animalStatus.id !== AnimalStatusCode.INSEMINADA &&
                          row.animalStatus.id !== AnimalStatusCode.INSEMINADA_A_CONFIRMAR &&
                          row.animalStatus.id !== AnimalStatusCode.PRENHA),
                      onClick: () => {
                        handleDotsClick(row, 'pregnancyDiagnosis')
                      }
                    },
                    {
                      icon: null,
                      text: t('register {this}', { this: t('other actions') }),
                      disabled:
                        (row?.category !== 'vaca' &&
                        row?.category !== 'novilha') ||
                        row.animalStatus.id !== AnimalStatusCode.PRENHA,
                      onClick: () => {
                        handleDotsClick(row, 'pregnancyAction')
                      }
                    },
                    {
                      icon: null,
                      text: t('register {this}', { this: t('calving') }),
                      disabled:
                        (row?.category !== 'vaca' &&
                        row?.category !== 'novilha') ||
                        (row.animalStatus.id !== AnimalStatusCode.PRENHA &&
                          row.animalStatus.id !== AnimalStatusCode.PEV),
                      onClick: () => {
                        handleDotsClick(row, 'calving')
                      }
                    },
                    {
                      icon: null,
                      text: t('register {this}', { this: t('pev') }),
                      disabled:
                        (row?.category !== 'vaca' &&
                        row?.category !== 'novilha') ||
                        row?.animalStatus.id !== AnimalStatusCode.PEV,
                      onClick: () => {
                        handleDotsClick(row, 'pev')
                      }
                    }
                  ] }
                />
              </div>
            </Popover>
          </div>
        )
      },
      {
        title: t('earring'),
        field: 'earring.earringCode'
      },
      {
        title: t('date of birth'),
        field: 'birthDate',
        render: (row) =>
          moment(row.birthDate, momentBackDateFormat).format(
            momentFriendlyDateFormat
          )
      },
      {
        title: t('lot'),
        field: 'lot.name'
      },
      {
        title: t('status'),
        field: 'animalStatus.name'
      },
      {
        title: t('next action'),
        field: 'nextAction.proximaAcao',
        render: (row) =>
          row.nextAction?.proximaAcao
            ? changeNextAction[row.nextAction?.proximaAcao]
            : '-'
      },
      {
        title: t('next action date'),
        field: 'nextAction.dataProximaAcao',
        render: (row) =>
          row.nextAction?.dataProximaAcao
            ? moment(
              row.nextAction?.dataProximaAcao,
              momentBackDateTimeFormat
            ).format(momentFriendlyDateFormat)
            : '-'
      }
    ],
    [changeNextAction, handleDotsClick, t]
  )

  const mapValuesDeep = useCallback((obj, cb) => {
    if (isArray(obj)) {
      return obj.map((innerObj) => mapValuesDeep(innerObj, cb))
    }
    if (isObject(obj)) {
      return mapValues(obj, (val) => mapValuesDeep(val, cb))
    }
    if (isString(obj) || isBoolean(obj)) {
      return obj
    }
    return cb(obj)
  }, [])

  const tableFilters = useMemo(() => {
    const allFilters = debouncedFilterText
      ? { ...filters, q: debouncedFilterText }
      : filters
    const result = mapValuesDeep(allFilters, toString)
    return result
  }, [debouncedFilterText, filters, mapValuesDeep])

  const debouncedChangeSearchFilter = useCallback(
    debounce((value) => {
      setDebouncedFilterText(value)
    }, 300),
    []
  )

  const onChangeSearchFilter = useCallback(
    (e) => {
      setFilterText(e.target.value)
      debouncedChangeSearchFilter(e.target.value)
    },
    [debouncedChangeSearchFilter]
  )

  const urlParams = useMemo(
    () => ({
      propertyId: currentProperty?.id
    }),
    [currentProperty]
  )

  const openFilterModal = useCallback(() => {
    createDialog({
      id: 'filter-cattle-management',
      Component: FilterCattleManagementModal,
      props: {
        onSubmit: setFilters,
        filters
      }
    })
  }, [createDialog, filters])

  const handleEditAnimal = useCallback(
    (_, currentAnimal = {}) => {
      dispatch(AnimalActions.setCurrentAnimal(currentAnimal))
      history.push(routes.registerAnimals.path, { currentAnimal })
    },
    [dispatch, history, routes.registerAnimals.path]
  )

  const handleRegisterAnimal = useCallback(() => {
    dispatch(AnimalActions.resetCurrentAnimal())
    dispatch(AnimalActions.resetIsDetail())
    history.push(routes.registerAnimals.path)
  }, [dispatch, history, routes.registerAnimals.path])

  return (
    <>
      <Top>
        <div style={ { marginRight: 10 } }>
          <ThemeProvider theme={ inputSearchTheme }>
            <InputSearch
              detached
              onChange={ onChangeSearchFilter }
              value={ filterText }
              placeholder={ t('search') }
              fullWidth
            />
          </ThemeProvider>
        </div>
        <div>
          <FilterButton
            onClick={ openFilterModal }
            isActive={
              !values(
                mapValuesDeep(
                  { ...filters, dead: filters?.dead ? 'dead' : '' },
                  toString
                )
              ).every(isEmpty)
            }
          />
        </div>
        <div>
          <Button
            id="register-animals"
            onClick={ handleRegisterAnimal }
            color="secondary"
          >
            <I18n params={ { howMany: 1, this: t('animal', { howMany: 1 }) } }>
              {'register {this}'}
            </I18n>
          </Button>
        </div>
      </Top>
      <div style={ { marginTop: 20 } }>
        <DataTable
          columns={ columns }
          data={ getAnimalsService }
          onRowClick={ handleOnRowClick }
          queryParams={ tableFilters }
          tableRef={ tableRef }
          urlParams={ urlParams }
          onEditClick={ handleEditAnimal }
        />
      </div>
    </>
  )
}

export default CattleManagement
