import React, { useMemo, useCallback, useEffect } from 'react'
import { useSelector , useDispatch } from 'react-redux'

import Grid from '@material-ui/core/Grid'

import I18n from '@smartcoop/i18n'
import { DairyFarmActions } from '@smartcoop/stores/dairyFarm'
import { selectCurrentSection } from '@smartcoop/stores/dairyFarm/selectorDairyFarm'
import { colors } from '@smartcoop/styles'
import Button from '@smartcoop/web-components/Button'

import CattleManagement from './CattleManagement'
import DairyFarmDashboard from './DairyFarmDashboard/DairyFarmDashboard'
import LotList from './LotList/LotList'
import {
  Container,
  Title,
  Top,
  ActionsContainer,
  Half
} from './styles'

const DairyFarmDashboardScreen = () => {

  const currentSection = useSelector(selectCurrentSection)
  const dispatch = useCallback(useDispatch(), [])


  const renderType = useMemo(
    () => ({

      dairyFarm: <DairyFarmDashboard/>,
      lot:  <LotList />,
      // indicators: <LotList />,
      cattleManagement:<CattleManagement />

    }[currentSection]), [currentSection]
  )

  useEffect(() => () => {
    dispatch(DairyFarmActions.resetCurrentSection())
  }, [dispatch])

  return (
    <Container>
      <>
        <Top>
          <Title>
            <I18n>dairy farm</I18n>
          </Title>
          <ActionsContainer>
            <Grid container>
              <Grid item xs={ 12 } sm={ 12 } md={ 12 }>
                <Half>
                  <div>
                    <Button
                      id="button"
                      color={ currentSection === 'dairyFarm' ? colors.primary : 'white' }
                      style={ { marginRight: 10 } }
                      onClick={ () => dispatch(DairyFarmActions.setCurrentSection('dairyFarm')) }
                    >
                      <I18n>my dairy farm</I18n>
                    </Button>
                  </div>
                  <>
                    {/* <div>
                    <Button
                      id="button"
                      style={ { marginRight: 10 } }
                      color={ currentSection === 'indicators' ? 'primary' : 'white' }
                      onClick={ () => dispatch(DairyFarmActions.setCurrentSection('indicators')) }
                    >
                      <I18n>dairy farm indicators</I18n>
                    </Button>
                  </div> */}
                    <div>
                      <Button
                        id="button"
                        style={ { marginRight: 10 } }
                        color={ currentSection === 'cattleManagement' ? 'primary' : 'white' }
                        onClick={ () => dispatch(DairyFarmActions.setCurrentSection('cattleManagement')) }
                      >
                        <I18n>cattle management</I18n>
                      </Button>
                    </div>
                    <div>
                      <Button
                        id="button"
                        style={ { marginRight: 10 } }
                        color={ currentSection === 'lot' ? 'primary' : 'white' }
                        onClick={ () => dispatch(DairyFarmActions.setCurrentSection('lot')) }
                      >
                        <I18n>lot</I18n>
                      </Button>
                    </div>
                  </>
                </Half>
              </Grid>
            </Grid>
          </ActionsContainer>
        </Top>
        {renderType}
      </>
    </Container>
  )
}

export default DairyFarmDashboardScreen
