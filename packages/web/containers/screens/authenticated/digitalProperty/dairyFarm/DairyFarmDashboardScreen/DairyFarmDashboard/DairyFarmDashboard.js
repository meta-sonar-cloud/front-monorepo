import React, { useMemo, useCallback, useState,useEffect } from 'react'
import { useSelector , useDispatch } from 'react-redux'
import { useHistory } from 'react-router-dom'

import moment from 'moment/moment'

import { isEmpty } from 'lodash'
import find from 'lodash/find'
import map from 'lodash/map'

import Grid from '@material-ui/core/Grid'

import { useDialog } from '@smartcoop/dialog'
import I18n, { useT } from '@smartcoop/i18n'
import { cow, pencil, landProductivityCows, concentratedMilkCows, feedConsumptionCows, milkSurface, lactatingCows } from '@smartcoop/icons'
import { DairyFarmActions } from '@smartcoop/stores/dairyFarm'
import { selectDashboardData } from '@smartcoop/stores/dairyFarm/selectorDairyFarm'
import { PropertyActions } from '@smartcoop/stores/property'
import { selectCurrentProperty } from '@smartcoop/stores/property/selectorProperty'
import { colors } from '@smartcoop/styles'
import { momentBackMonthYearFormat } from '@smartcoop/utils/dates'
import { formatCurrency } from '@smartcoop/utils/formatters'
import Button from '@smartcoop/web-components/Button'
import Icon from '@smartcoop/web-components/Icon'
import InputMonthYear from '@smartcoop/web-components/InputMonthYear'
import MilkDataCard from '@smartcoop/web-components/MilkDataCard'
import Tooltip from '@smartcoop/web-components/Tooltip'
import DairyFarmDeliveryChart from '@smartcoop/web-containers/fragments/DairyFarmDeliveryChart'
import DairyFarmPriceChart from '@smartcoop/web-containers/fragments/DairyFarmPriceChart'
import DairyFarmQualityChart from '@smartcoop/web-containers/fragments/DairyFarmQualityChart'
import CreatePropertyModal from '@smartcoop/web-containers/modals/CreatePropertyModal'
import MilkDeliveryModal from '@smartcoop/web-containers/modals/dairyFarm/MilkDeliveryModal'
import EditPropertyModal from '@smartcoop/web-containers/modals/EditPropertyModal'
import { useRoutes } from '@smartcoop/web-containers/routes/authenticated'

import {
  CardItemTitle,
  Title,
  Content,
  Line,
  GridIndicators
} from './styles'

const DairyFarmDashboard = () => {
  const { createDialog } = useDialog()
  const t = useT()
  const dispatch = useCallback(useDispatch(),[])
  const { routes } = useRoutes()
  const history = useHistory()

  const currentProperty = useSelector(selectCurrentProperty)
  const dairyFarm = useMemo(() => !isEmpty(currentProperty?.dairyFarm) && currentProperty?.dairyFarm[0], [currentProperty])
  const dashboardData = useSelector(selectDashboardData)

  const [date, setDate] = useState(moment().format(momentBackMonthYearFormat))

  const editProperty = useCallback(() => {
    dispatch(PropertyActions.setEditPropertyData(currentProperty.id, currentProperty))

    createDialog({
      id: 'property-onboarding',
      Component: EditPropertyModal
    })
  }, [createDialog, currentProperty, dispatch])


  const onCancel = useCallback(
    () => {
      isEmpty(dairyFarm) && history.push(routes.propertyHome.path)
    },
    [dairyFarm, history, routes.propertyHome.path]
  )

  const openMilkDeliveryModal = useCallback(
    () => {
      createDialog({
        id: 'milk-delivery',
        Component: MilkDeliveryModal,
        props: {
          onCancel,
          dairyFarm
        }
      })
    },
    [createDialog, dairyFarm, onCancel]
  )

  const openCreatePropertyModal = useCallback(
    () => {
      dispatch(PropertyActions.updateOfflineProperty({ data: currentProperty }))
      createDialog({
        id: 'property-onboarding',
        Component: CreatePropertyModal,
        props: {
          isEditing: true
        }
      })
    }, [createDialog, currentProperty, dispatch]
  )

  const cards = useMemo(
    () => [
      {
        slug: 'land-productivity',
        icon: landProductivityCows,
        color: colors.yellow,
        text: t('land productivity'),
        data: find(dashboardData.indicators, { slug: 'land-productivity' }) || {}
      },
      {
        slug: 'cow-productivity',
        icon: cow,
        color: colors.blue,
        text: t('cow productivity'),
        data: find(dashboardData.indicators, { slug: 'cow-productivity' }) || {}
      },
      {
        slug: 'concentrated-milk-relation',
        icon: concentratedMilkCows,
        color: colors.blue,
        text: t('concentrated milk ratio'),
        data: find(dashboardData.indicators, { slug: 'concentrated-milk-relation' }) || {}
      },
      {
        slug: 'milk-surface-area',
        icon: milkSurface,
        color: colors.yellow,
        text: t('milk surface'),
        data: find(dashboardData.indicators, { slug: 'milk-surface-area' }) || {}
      },
      {
        slug: 'monthly-feed-consumption',
        icon: feedConsumptionCows,
        color: colors.yellow,
        text: t('feed consumption'),
        data: find(dashboardData.indicators, { slug: 'monthly-feed-consumption' }) || {}
      },
      {
        slug: 'lactating-cows',
        icon: lactatingCows,
        color: colors.blue,
        text: t('lactating cows'),
        data: find(dashboardData.indicators, { slug: 'lactating-cows' }) || {}
      }
    ],
    [dashboardData.indicators, t]
  )

  const formatValue = useCallback(
    (value) => formatCurrency(value, 0, ',', '.', ''),
    []
  )

  useEffect(() => {
    isEmpty(dairyFarm) && openMilkDeliveryModal()
  }, [dairyFarm, openMilkDeliveryModal])


  useEffect(() => {
    if (!isEmpty(currentProperty)) {
      dispatch(DairyFarmActions.loadDashboardData({ date }))
    }
  }, [currentProperty, date, dispatch])

  return (
    <>
      <Line>
        <InputMonthYear
          detached
          label="Mês/Ano"
          name="monthYear"
          value={ date }
          onChange={ (value) => setDate(value[0]) }
          style={ { marginRight: 10 } }
        />
        <div>
          <Button
            id="edit-field"
            color={ colors.white }
            onClick={ editProperty }
            style={ { marginRight: 10 } }
          >
            <>
              <Icon style={ { marginRight: 6 } } icon={ pencil } size={ 14 }/>
              <I18n>edit property</I18n>
            </>
          </Button>
          <Button
            id="edit-field"
            color={ colors.white }
            onClick={ openMilkDeliveryModal }
          >
            <>
              <Icon style={ { marginRight: 6 } } icon={ pencil } size={ 14 }/>
              <I18n>edit company</I18n>
            </>
          </Button>
        </div>
        <div>
          <Button
            id="button"
            color={ colors.white }
            onClick={ openCreatePropertyModal }
            style={ { marginLeft: 10, display: 'none' } }
          >
            <I18n>edit activities</I18n>
          </Button>
        </div>
      </Line>
      {!isEmpty(dairyFarm) && (
        <Content>
          <Grid container spacing={ 2 }>
            <Grid item xs={ 12 } sm={ 12 } md={ 7 }>
              <CardItemTitle
                title={ <I18n as={ Title }>milk deliveries</I18n> }
                headerRight={ (
                  <div>
                    <Button
                      id="register-delivery"
                      color="white"
                      onClick={ () => history.push(routes.listMilkDelivery.path) }
                    >
                      <I18n>register delivery</I18n>
                    </Button>
                  </div>
                ) }
                childrenStyle={ {
                  paddingTop: 0,
                  paddingBottom: 0,
                  paddingRight: 0,
                  paddingLeft: 0
                } }
              >
                <DairyFarmDeliveryChart />
              </CardItemTitle>
            </Grid>
            <Grid item xs={ 12 } sm={ 12 } md={ 5 }>

              <GridIndicators>
                {map(cards, (card) => (
                  <MilkDataCard
                    key={ card.slug }
                    icon={ card.icon }
                    measureUnit={ card.data?.measureUnit }
                    title={ card.text }
                    color={ card.color }
                  >
                    {
                      dashboardData.indicators ? (
                        formatValue(card.data?.value)
                      ) : (
                        <Tooltip
                          title={
                            t(
                              'it was not possible to calculate, check if the property has the activity {this}',
                              { this: t('dairy farming') }
                            )
                          }
                        >
                          <I18n>does not apply - short version</I18n>
                        </Tooltip>
                      )
                    }
                  </MilkDataCard>
                ))}
              </GridIndicators>
            </Grid>

            <Grid item xs={ 12 } sm={ 12 } md={ 5 }>
              <CardItemTitle
                title={ (
                  <I18n as={ Title } params={ { howMany: 2 } }>
                      price
                  </I18n>
                ) }
                headerRight={ (
                  <div>
                    <Button
                      id="register-price"
                      color="white"
                      onClick={ () => history.push(routes.listPriceData.path) }
                    >
                      <I18n>register price</I18n>
                    </Button>
                  </div>
                ) }
                childrenStyle={ {
                  paddingTop: 0,
                  paddingBottom: 0,
                  paddingRight: 0,
                  paddingLeft: 0
                } }
              >
                <DairyFarmPriceChart />
              </CardItemTitle>
            </Grid>

            <Grid item xs={ 12 } sm={ 12 } md={ 7 }>
              <CardItemTitle
                title={ <I18n as={ Title }>quality analysis</I18n> }
                headerRight={ (
                  <div>
                    <Button
                      id="register-analysis"
                      color="white"
                      onClick={ () => history.push(routes.listMilkQuality.path) }
                    >
                      <I18n>register analysis</I18n>
                    </Button>
                  </div>
                ) }
                childrenStyle={ {
                  paddingTop: 0,
                  paddingBottom: 0,
                  paddingRight: 0,
                  paddingLeft: 0
                } }
                headerStyle={ {
                  marginBottom: 0
                } }
              >
                <DairyFarmQualityChart />
              </CardItemTitle>
            </Grid>
          </Grid>
        </Content>
      )}
    </>
  )
}

export default DairyFarmDashboard
