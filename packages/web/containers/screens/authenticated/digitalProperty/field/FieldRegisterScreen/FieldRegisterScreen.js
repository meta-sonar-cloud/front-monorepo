/* eslint-disable no-shadow */
import React, { useRef, useCallback, useState, useMemo, useEffect } from 'react'
import { useSelector } from 'react-redux'
import { useHistory, useLocation } from 'react-router-dom'

import isEmpty from 'lodash/isEmpty'
import map from 'lodash/map'

import I18n, { useT } from '@smartcoop/i18n'
import { field } from '@smartcoop/icons'
import { useSnackbar } from '@smartcoop/snackbar'
import { selectFields } from '@smartcoop/stores/field/selectorField'
import { selectCurrentProperty } from '@smartcoop/stores/property/selectorProperty'
import colors from '@smartcoop/styles/colors'
import { findMapZoom } from '@smartcoop/utils/maps'
import Button from '@smartcoop/web-components/Button'
import Icon from '@smartcoop/web-components/Icon'
import Maps from '@smartcoop/web-components/Maps'
import Polygon from '@smartcoop/web-components/Maps/components/Polygon'
import FieldForm from '@smartcoop/web-containers/forms/digitalProperty/field/FieldForm/FieldForm'
import { ButtonsContainer } from '@smartcoop/web-containers/layouts/AuthenticatedLayout/theme'
import SplitedScreenLayout from '@smartcoop/web-containers/layouts/SplitedScreenLayout'
import { useRoutes } from '@smartcoop/web-containers/routes/authenticated'


import { Container, Header, Title, IconContainer } from './styles'

const FieldRegisterScreen = () => {
  const activityFormRef = useRef(null)
  const polygonRef = useRef(null)

  const fields = useSelector(selectFields)

  const history = useHistory()
  const { routes } = useRoutes()
  const location = useLocation()
  const snackbar = useSnackbar()
  const t = useT()

  const currentProperty = useSelector(selectCurrentProperty)

  const isEditing = useMemo(() =>  location.pathname.includes('edit'),[location.pathname])

  const [loading, setLoading] = useState(false)
  const [mapZoom, setMapZoom] = useState(location?.state?.mapZoom)

  const [polygonCoordinates, setPolygonCoordinates] = useState(
    (() => {
      if (location.state?.field) {
        return location.state.field.polygonCoordinates
      }
      return []
    })()
  )

  const mapCenter = useMemo(
    () => {
      if (location.state && location.state.region) return location.state.region
      if (location.state?.mapCenterPosition) {
        return location.state.mapCenterPosition
      }
      return currentProperty.geolocalization
    },
    [currentProperty.geolocalization, location.state]
  )

  const submitForms = useCallback(
    () => {
      activityFormRef.current.submit()
    },
    []
  )

  const onSuccess = useCallback(
    (fieldId) => {
      const operationType = isEditing ? 'edited' : 'registered'
      history.replace(routes.fieldDetails.path.replace(':fieldId', fieldId))
      snackbar.success(
        t(`your {this} was ${ operationType }`, {
          howMany: 1,
          gender: 'male',
          this: t('field', { howMany: 1 })
        })
      )
    },
    [history, isEditing, routes.fieldDetails.path, snackbar, t]
  )

  const setFormArea = useCallback(
    (value) => (
      activityFormRef.current.setFieldValue('area', value.toString())
    ),
    []
  )

  const handleClose = useCallback(
    () => (
      history.goBack()
    ),
    [history]
  )

  const handleAddPolygonPoint = useCallback(
    (newPoint) => {
      if (!loading) {
        const newPoints = polygonRef.current.sortNewPoint([
          newPoint.latitude,
          newPoint.longitude
        ])
        setPolygonCoordinates(newPoints)
      }
    },
    [loading]
  )

  useEffect(
    () => {
      if (isEditing && !mapZoom && !isEmpty(polygonCoordinates)) {
        setMapZoom(findMapZoom(polygonCoordinates))
      }
    },
    [isEditing, mapZoom, polygonCoordinates]
  )

  return (
    <SplitedScreenLayout
      title={ { name: t('field', { howMany: 2 }) } }
      divRightStyle={ { padding: 0 } }
      leftChildren={ (
        <Container>
          <Header>
            <IconContainer>
              <Icon icon={ field } size={ 18 } />
            </IconContainer>

            <Title style={ { fontSize: 18, fontWeight: 600 } }>
              <I18n>{isEditing ? 'field edit' : 'field registration'}</I18n>
            </Title>

          </Header>
          <FieldForm
            ref={ activityFormRef }
            onSuccess={ onSuccess }
            onSubmit={ () => setLoading(true) }
            onError={ () => setLoading(false) }
            polygonCoordinates={ polygonCoordinates }
            withoutSubmitButton
          />

          <ButtonsContainer style={ { paddingTop: 30 } }>
            <Button
              id="cancel-field-form"
              onClick={ handleClose }
              style={ { flex: 1 } }
              variant="outlined"
              disabled={ loading }
            >
              <I18n>cancel</I18n>
            </Button>

            <div style={ { width: '10%' } } />

            <Button
              id="submit-field-form"
              onClick={ submitForms }
              style={ { flex: 1 } }
              disabled={ loading }
            >
              <I18n>save</I18n>
            </Button>
          </ButtonsContainer>
        </Container>
      ) }
      rightChildren={ (
        <Maps
          region={ mapCenter }
          zoom={ mapZoom }
          onClick={ handleAddPolygonPoint }
          style={ { minHeight: 500 } }
          cursor="crosshair"
        >
          {map(fields, ({ polygonCoordinates, id }) => (
            <Polygon
              key={ id }
              points={ polygonCoordinates }
              color={ colors.white }
            />
          ))}
          <Polygon
            ref={ polygonRef }
            onChange={ loading ? () => {} : setPolygonCoordinates }
            onChangeArea={ setFormArea }
            color={ colors.secondary }
            points={ polygonCoordinates }
          />
        </Maps>
      ) }
    />
  )}

export default FieldRegisterScreen
