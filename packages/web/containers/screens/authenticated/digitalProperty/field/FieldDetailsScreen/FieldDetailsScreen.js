import React, { useCallback, useRef, useMemo, useEffect, useState } from 'react'
import { ImageOverlay } from 'react-leaflet'
import { useSelector, useDispatch } from 'react-redux'
import { useHistory, useParams } from 'react-router-dom'

import { reduce, size } from 'lodash'
import debounce from 'lodash/debounce'
import isEmpty from 'lodash/isEmpty'
import map from 'lodash/map'
import upperFirst from 'lodash/upperFirst'

import Divider from '@material-ui/core/Divider'

import { useDialog } from '@smartcoop/dialog'
import I18n, { useT } from '@smartcoop/i18n'
import {
  bug,
  chart,
  pencil,
  trash,
  field as fieldIcon,
  bugMarkerRound as bugMarkerRoundIcon,
  emptyCrop,
  emptyManagement,
  cutLayout,
  plantAndHand,
  openLayout } from '@smartcoop/icons'
import { useSnackbar } from '@smartcoop/snackbar'
import { FieldActions } from '@smartcoop/stores/field'
import { selectCurrentField, selectLoadingCurrentField } from '@smartcoop/stores/field/selectorField'
import { selectModuleIsTechnical } from '@smartcoop/stores/module/selectorModule'
import { selectUserCanWrite } from '@smartcoop/stores/user/selectorUser'
import colors from '@smartcoop/styles/colors'
import fonts from '@smartcoop/styles/fonts'
import { getPolygonCenter, findMapZoom } from '@smartcoop/utils/maps'
import AccordionsGroup from '@smartcoop/web-components/AccordionsGroup'
import Button from '@smartcoop/web-components/Button'
import EmptyState from '@smartcoop/web-components/EmptyState'
import GrowingSeasonStatus from '@smartcoop/web-components/GrowingSeasonStatus'
import Icon from '@smartcoop/web-components/Icon'
import Loader from '@smartcoop/web-components/Loader'
import Management from '@smartcoop/web-components/Management'
import Maps from '@smartcoop/web-components/Maps'
import Control from '@smartcoop/web-components/Maps/components/Control'
import Polygon from '@smartcoop/web-components/Maps/components/Polygon'
import PinMarker from '@smartcoop/web-components/Maps/markers/PinMarker'
import ConfirmModal from '@smartcoop/web-components/Modal/ConfirmModal'
import PartialLoading from '@smartcoop/web-components/PartialLoading'
import CropManagementDetailFragment from '@smartcoop/web-containers/fragments/CropManagementDetailFragment'
import FieldIndexesNavigator from '@smartcoop/web-containers/fragments/FieldIndexesNavigator'
import FieldTimelineChart from '@smartcoop/web-containers/fragments/FieldTimelineChart'
import PestReportDetailsFragment from '@smartcoop/web-containers/fragments/PestReport/PestReportDetailsFragment/PestReportDetailsFragment'
import WeatherForecastCard from '@smartcoop/web-containers/fragments/WeatherForecastCard/WeatherForecastCard'
import SplitedScreenLayout from '@smartcoop/web-containers/layouts/SplitedScreenLayout'
import GrowingSeasonsHistoryModal from '@smartcoop/web-containers/modals/DigitalProperty/GrowingSeasonsHistoryModal'
import technicalVisitRegister from '@smartcoop/web-containers/modals/technical/TechnicalVisitModal'
import { useRoutes } from '@smartcoop/web-containers/routes/authenticated'

import {
  Container,
  GrowingContainer,
  LeftContent,
  RightContent,
  GrowingItem,
  TextSowingYear,
  RightContainer,
  MapContainer,
  TitleCropManagements,
  HeaderCropManagementList,
  ContainerListManagements
} from './styles'

const FieldDetailsScreen = () => {
  const history = useHistory()
  const { createDialog } = useDialog()
  const dispatch = useCallback(useDispatch(), [])
  const t = useT()
  const { routes } = useRoutes()
  const { fieldId } = useParams()
  const snackbar = useSnackbar()

  const [showCropManagement, setShowCropManagement] = useState(false)
  const [cropManagementData, setCropManagementData] = useState({})
  const [loadingField, setLoadingField] = useState(false)
  const [indicatorImage, setIndicatorImage] = useState()
  const [opened, setOpened] = useState(true)


  const userWrite = useSelector(selectUserCanWrite)
  const field = useSelector(selectCurrentField)
  const technicalModule = useSelector(selectModuleIsTechnical)
  const loading = useSelector(selectLoadingCurrentField)

  const mapRef = useRef(null)

  const { growingSeasons, growingSeasonsHistoric } = field

  const plagueReports = useMemo(
    () => !isEmpty(growingSeasons) ? growingSeasons[0].plagueReports : [], [growingSeasons]
  )

  const deleteField = useCallback(() => {
    createDialog({
      id: 'confirm-delete',
      Component: ConfirmModal,
      props: {
        onConfirm: () => {
          if (isEmpty(growingSeasons)) {
            dispatch(FieldActions.deleteField(field.id))
            snackbar.success(
              t('your {this} was deleted', {
                howMany: 1,
                gender: 'male',
                this: t('field', { howMany: 1 })
              })
            )
            history.goBack()
          } else {
            snackbar.warning(
              t(
                'it is not possible to exclude fields with registered cultivation'
              )
            )
          }
        },
        textWarning: t('are you sure you want to delete the {this}?', {
          howMany: 1,
          gender: 'male',
          this: t('field', { howMany: 1 })
        })
      }
    })
  }, [createDialog, dispatch, field.id, growingSeasons, history, snackbar, t])

  const growingSeason = useMemo(
    () => (!isEmpty(field.growingSeason) ? field.growingSeason : {}),
    [field.growingSeason]
  )

  const openGrowingSeasonHistory = useCallback(() => {
    createDialog({
      id: 'growing-season-history',
      Component: GrowingSeasonsHistoryModal,
      props: {
        growingSeasonsHistory: growingSeasonsHistoric,
        polygonCoordinates: field.polygonCoordinates,
        area: field.area
      }
    })
  }, [createDialog, field, growingSeasonsHistoric])

  const onSuccessRegisterTechnicalVisit = useCallback(
    () => {
      snackbar.success(
        t('your {this} was registered', {
          howMany: 1,
          gender: 'female',
          this: t('technical visit')
        })
      )
    },
    [snackbar, t]
  )

  const openTechnicalVisitRegister = useCallback(() => {
    createDialog({
      id: 'register-technical-visit',
      Component: technicalVisitRegister,
      props: {
        onSuccess: onSuccessRegisterTechnicalVisit,
        cultivateName: growingSeason?.crop?.description
      }
    })
  }, [createDialog, growingSeason, onSuccessRegisterTechnicalVisit])

  const iconChange = useMemo(
    () => opened ? openLayout : cutLayout,
    [opened]
  )

  const handleCropsRegister = useCallback(() => {
    history.push(routes.growingSeasonRegister.path)
  }, [history, routes.growingSeasonRegister.path])

  const handleManagementRegister = useCallback(() => {
    history.push(routes.managementRegister.path)
  }, [history, routes.managementRegister.path])

  const currentCropManagement = useCallback(
    (item) => {
      if (item === cropManagementData) {
        setShowCropManagement(!showCropManagement)
        setCropManagementData(item)
      } else {
        setShowCropManagement(true)
        setCropManagementData(item)
      }
    },
    [cropManagementData, showCropManagement]
  )

  const handleCropDelete = useCallback(() => {
    createDialog({
      id: 'confirm-delete-crop',
      Component: ConfirmModal,
      props: {
        onConfirm: () => {
          dispatch(
            FieldActions.deleteOfflineGrowingSeason(growingSeason.id, () =>
              snackbar.success(
                t('your {this} was deleted', {
                  howMany: 1,
                  gender: 'male',
                  this: t('crops', { howMany: 1 })
                })
              )
            )
          )
        },
        textWarning: t('are you sure you want to delete the {this}?', {
          howMany: 1,
          gender: 'male',
          this: t('crops', { howMany: 1 })
        })
      }
    })
  }, [createDialog, dispatch, growingSeason.id, snackbar, t])

  const toggleLayout = useCallback(() => {
    setOpened((old)=> !old)
  }, [])

  const statusGrowing = useMemo(() => {
    if (isEmpty(growingSeason.closed)) {
      return 'ativo'
    }
    return 'encerrado'
  }, [growingSeason.closed])

  const mapCenterPosition = useMemo(() => {
    if (!isEmpty(field.polygonCoordinates)) {
      return getPolygonCenter(field.polygonCoordinates)
    }
    return undefined
  }, [field.polygonCoordinates])

  const mapZoom = useMemo(() => {
    if (!isEmpty(field.polygonCoordinates)) {
      return findMapZoom(field.polygonCoordinates)
    }
    return 17
  }, [field.polygonCoordinates])

  const accordionItems = useMemo(
    () => [
      {
        title: t('field timeline'),
        headerLeft: <Icon icon={ chart } size={ 25 } />,
        headerTitleProps: {
          style: {
            fontWeight: fonts.fontWeight.bold,
            textAlign: 'left'
          }
        },
        children: (
          <>
            <Divider style={ { margin: 0 } } />
            <br style={ { clear: 'both' } } />
            <FieldTimelineChart field={ field } />
          </>
        )
      }
    ],
    [field, t]
  )

  const countClosedCropsManagement = useMemo(
    () => (reduce(
      map(growingSeason.cropsManagements, cm => cm.realizationDate ? 1 : 0),
      (acc, cm) => cm + acc
    )), [growingSeason]
  )

  const hasAllCropsManagementClosedButOne = useMemo(
    () => (!isEmpty(growingSeason.cropsManagements) &&
    countClosedCropsManagement === size(growingSeason.cropsManagements) - 1), [countClosedCropsManagement, growingSeason]
  )

  const onPestReportDelete = useCallback(
    (id) => {
      setLoadingField(true)
      dispatch(FieldActions.deleteOfflinePestReport(
        id,
        debounce(()=> setLoadingField(false), 300),
        ()=> setLoadingField(false)
      ))
      snackbar.success(
        t('your {this} was deleted', {
          howMany: 1,
          gender: 'male',
          this: t('report', { howMany: 1 })
        })
      )
    }
    , [dispatch, snackbar, t]
  )

  const onPestReportEdit = useCallback(
    (pestReport) => {
      history.push(routes.pestReportEdit.path,
        {
          pestReport: { ...pestReport },
          field: { ...field },
          map: { mapZoom, mapCenterPosition }
        })
    },
    [field, history, mapCenterPosition, mapZoom, routes.pestReportEdit.path]
  )

  const handlePestReport = useCallback(() => {
    history.push(routes.pestReportRegister.path, { map: { mapZoom, mapCenterPosition }, field: { ...field } })
  }, [field, history, mapCenterPosition, mapZoom, routes.pestReportRegister.path])

  useEffect(() => {
    dispatch(FieldActions.loadCurrentField(fieldId))
  }, [dispatch, fieldId])

  useEffect(()=> {
    mapRef.current && mapRef.current.leafletElement._onResize()
  }, [opened])

  return (
    <SplitedScreenLayout
      title={ { name: t('field', { howMany: 2 }) } }
      divRightStyle={ { padding: 0 } }
      leftChildren={ opened && (
        <PartialLoading open={ loading } width={ 100 }>
          <RightContainer>
            <>
              {!isEmpty(growingSeason?.id) ? (
                <Container>
                  <GrowingContainer>
                    <LeftContent>
                      <I18n
                        params={ { howMany: 1 } }
                        style={ { fontSize: 18, fontWeight: 700 } }
                      >
                    crops
                      </I18n>
                      {!isEmpty(growingSeason.crop) && (
                        <GrowingItem>
                          <p style={ { margin: '10px 0', fontSize: 18, fontWeight: 600 } }>
                            {upperFirst(growingSeason.crop.description)}
                          </p>
                        </GrowingItem>
                      )}
                      {!isEmpty(growingSeason.cultivation) && (
                        <p style={ { margin: 0, fontSize: 16 } }>
                          {growingSeason.cultivation.description}
                        </p>
                      )}
                    </LeftContent>
                    <RightContent>
                      {userWrite && (
                        <div style={ { display: 'flex' } }>
                          <Button
                            id="editCropButton"
                            variant="outlined"
                            style={ { marginRight: 5, minWidth: '30px', minHeight: '30px', border: `1px solid ${ colors.backgroundHtml }`, padding: 0 } }
                            onClick={ handleCropsRegister }
                          >
                            <Icon icon={ pencil } size={ 14 } />
                          </Button>
                          <Button
                            id="deleteCropButton"
                            variant="outlined"
                            style={ { minWidth: '30px', minHeight: '30px', border: `1px solid ${ colors.backgroundHtml }`, padding: 0 } }
                            onClick={ handleCropDelete }
                          >
                            <Icon icon={ trash } size={ 14 } color={ colors.red } />
                          </Button>
                        </div>
                      )}
                      {
                        !isEmpty(field) && (
                          <TextSowingYear>
                            {growingSeason.sowingYear}
                          </TextSowingYear>
                        )
                      }
                      <GrowingSeasonStatus slug={ statusGrowing } />
                    </RightContent>
                  </GrowingContainer>

                  {userWrite && (
                    <>
                      <Button
                        id="pestReport"
                        variant="outlined"
                        style={ {
                          marginTop: 10,
                          border: '1px solid rgba(21, 24, 24, 0.3)'
                        } }
                        onClick={ handlePestReport }
                      >
                        <>
                          <Icon icon={ bug } size={ 16 } style={ { marginRight: 5 } } />
                          <I18n>report occurrence</I18n>
                        </>
                      </Button>
                      <Button
                        id="historyField"
                        variant="outlined"
                        style={ {
                          marginTop: 10,
                          border: '1px solid rgba(21, 24, 24, 0.3)'
                        } }
                        onClick={ openGrowingSeasonHistory }
                      >
                        <I18n>field history</I18n>
                      </Button>
                    </>
                  )}

                  <Divider style={ { margin: '10px 0 0 0' } } />
                </Container>
              ) : (
                <div
                  style={ {
                    display: 'flex',
                    flexDirection: 'column',
                    alignItems: 'center',
                    justifyContent: 'center',
                    margin: '10px 0 ',
                    flex: 1,
                    height: '100%'
                  } }
                >
                  <EmptyState
                    icon={ emptyCrop }
                    text={ t('no crops registered') }
                    action={ !userWrite ? undefined : {
                      text: t('register crops'),
                      onClick: handleCropsRegister
                    } }
                  />
                  <Button
                    id="historyField"
                    variant="outlined"
                    style={ {
                      marginTop: 10,
                      border: '1px solid rgba(21, 24, 24, 0.3)'
                    } }
                    onClick={ openGrowingSeasonHistory }
                  >
                    <I18n>field history</I18n>
                  </Button>
                </div>
              )}
            </>
            <Container>
              {!isEmpty(growingSeason) && (
                <>
                  {!isEmpty(growingSeason.cropsManagements) && (
                    <>
                      <HeaderCropManagementList>
                        <TitleCropManagements>
                          <I18n params={ { howMany: 2 } }>cropManagement</I18n>
                        </TitleCropManagements>
                        {userWrite && (
                          <div>
                            <Button
                              id='register-crop-management'
                              color='secondary'
                              onClick={ handleManagementRegister }
                              disabled={ !userWrite }
                            >
                              <I18n>register management</I18n>
                            </Button>
                          </div>
                        )}
                      </HeaderCropManagementList>
                      <Divider style={ { margin: 0 } } />
                    </>
                  )}
                  <ContainerListManagements>
                    {!isEmpty(growingSeason.cropsManagements) ? (
                      growingSeason.cropsManagements.map((item) => (
                        <Management
                          key={ item.id }
                          date={ item.predictedDate }
                          managementType={ item.operation.name }
                          done={ item.realizationDate }
                          cropManagement={ item }
                          onClick={ currentCropManagement }
                          active={
                            item.id === cropManagementData?.id &&
                            showCropManagement
                          }
                        />
                      ))
                    ) : (
                      <>
                        <div
                          style={ {
                            display: 'flex',
                            alignItems: 'center',
                            justifyContent: 'center',
                            margin: '10px 0 ',
                            flex: 1,
                            height: '100%'
                          } }
                        >
                          <EmptyState
                            icon={ emptyManagement }
                            text={ t('no management registered') }
                            action={ !userWrite ? undefined : {
                              text: t('register management'),
                              onClick: handleManagementRegister
                            } }
                            buttonProps={ {
                              disabled: !userWrite
                            } }
                          />
                        </div>
                      </>
                    )}
                  </ContainerListManagements>
                </>
              )}
            </Container>
          </RightContainer>
        </PartialLoading>)
      }
      rightChildren={
        <>
          {loadingField ? <Loader /> : !isEmpty(field) && (
            <MapContainer>
              <Maps zoom={ mapZoom } region={ mapCenterPosition } ref={ mapRef }>
                <Control position="topcenter">
                  <div
                    style={ {
                      display: 'flex',
                      justifyContent: 'center',
                      width: '100%',
                      padding: '0 10px',
                      paddingRight: '70px'
                    } }
                  >
                    <div
                      style={ {
                        display: 'flex',
                        justifyContent: 'space-between',
                        width: '100%'
                      } }
                    >

                      <div style={ {} }>
                        <Button
                          id="toggle-Layout"
                          color={ colors.white }
                          style={ {
                            padding: '9px 10px',
                            fontSize: '0.875rem',
                            marginRight: 10
                          } }
                          onClick={ toggleLayout }
                        >
                          <Icon
                            icon={ iconChange }
                            size={ 17 }
                          />
                        </Button>

                        {userWrite && (
                          <Button
                            id="edit-field"
                            color={ colors.shadow }
                            style={ {
                              padding: '5.5px 10px',
                              fontColor: colors.white,
                              fontSize: '0.875rem',
                              marginRight: 10
                            } }
                          >
                            <>
                              <Icon
                                icon={ fieldIcon }
                                size={ 17 }
                                style={ { marginRight: 5 } }
                                color={ colors.white }
                              />
                              {field?.fieldName}
                            </>
                          </Button>
                        )}
                      </div>
                      <div>

                        {technicalModule && (
                          <Button
                            id="register-technical-visit"
                            color={ colors.white }
                            style={ {
                              padding: '5.5px 10px',
                              fontSize: '0.875rem',
                              marginRight: 10
                            } }
                            onClick={ openTechnicalVisitRegister }
                          >
                            <>
                              <Icon
                                icon={ plantAndHand }
                                size={ 17 }
                                style={ { marginRight: 5 } }
                              />
                              {t('technical visit')}
                            </>
                          </Button>
                        )}

                        {userWrite && (
                          <Button
                            id="edit-field"
                            color={ colors.white }
                            style={ {
                              padding: '5.5px 10px',
                              fontSize: '0.875rem',
                              marginRight: 10
                            } }
                            onClick={ () =>
                              history.push({
                                pathname: routes.fieldEdit.path,
                                state: { field, mapCenterPosition }
                              })
                            }
                          >
                            <>
                              <Icon
                                icon={ pencil }
                                size={ 17 }
                                style={ { marginRight: 5 } }
                              />
                              <I18n>edit</I18n>
                            </>
                          </Button>
                        )}

                        {userWrite && (
                          <Button
                            id="delete-field"
                            style={ { padding: '9px 0' } }
                            color={ colors.white }
                            onClick={ deleteField }
                          >
                            <Icon size={ 16 } icon={ trash } color={ colors.red } />
                          </Button>
                        )}
                      </div>
                    </div>
                  </div>
                </Control>
                <Control position="topright">
                  <WeatherForecastCard
                    customCoordinates={ mapCenterPosition }
                  />
                </Control>
                <Control position="bottomcenter">
                  <FieldIndexesNavigator
                    field={ field }
                    onChangeImage={ setIndicatorImage }
                  />
                </Control>
                {
                  map(plagueReports, (pestReport) => (
                    <PinMarker
                      key={ pestReport.id }
                      coordinate={ pestReport.pinLocation }
                      customIcon={ bugMarkerRoundIcon }
                      size={ 28 }
                      iconAnchor={ {
                        x: 13,
                        y: 40
                      } }
                    >
                      <PestReportDetailsFragment
                        pestReport={ { ...pestReport } }
                        onDelete={ onPestReportDelete }
                        onEdit={ onPestReportEdit }
                      />
                    </PinMarker>
                  ))
                }
                {indicatorImage ? (
                  <ImageOverlay
                    url={ indicatorImage }
                    bounds={ field.polygonCoordinates }
                  />
                ) : (
                  <Polygon
                    points={ field.polygonCoordinates }
                    color={ colors.secondary }
                  />
                )}
              </Maps>
              {showCropManagement ? (
                <CropManagementDetailFragment
                  closeCropManagement={ () => {
                    setShowCropManagement(false)
                  } }
                  growingSeasonId={ growingSeason.id }
                  cropManagementData={ cropManagementData }
                  canConcludeGrowingSeason={ hasAllCropsManagementClosedButOne }
                />
              ) : (
                <div>
                  <AccordionsGroup
                    items={ accordionItems }
                    toggleOnClick
                    invertArrow
                  />
                </div>
              )}
            </MapContainer>
          )}
        </>
      }
    />
  )
}

export default FieldDetailsScreen
