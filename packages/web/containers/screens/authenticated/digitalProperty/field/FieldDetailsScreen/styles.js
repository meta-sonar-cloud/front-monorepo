import styled from 'styled-components'

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  padding: 10px 20px;
`

export const GrowingContainer = styled.div`
  display: flex;
  justify-content: space-between;
`

export const LeftContent = styled.div`
  display: flex;
  flex-direction: column;
`

export const RightContainer = styled.div`
  display: flex;
  flex-direction: column;
  flex: 1;
  height: 100%;
`
export const RightContent = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-end;
`

export const GrowingItem = styled.div`
  display: flex;
  align-items: center;
  flex: 1;
`

export const TextSowingYear = styled.div`
  margin: 10px 0;
  font-size: 16px;
  font-weight: 600;
`

export const MapContainer = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
`

export const TitleCropManagements = styled.p`
  font-size: 18px;
  font-weight: 600;
  flex: 1;
`

export const HeaderCropManagementList = styled.div`
  display: flex;
  align-items: center;
`

export const ContainerListManagements = styled.div`
  display: flex;
  flex-direction: column;
  overflow: auto;
  flex: 1;
  max-height: calc(100vh - 400px);
`
