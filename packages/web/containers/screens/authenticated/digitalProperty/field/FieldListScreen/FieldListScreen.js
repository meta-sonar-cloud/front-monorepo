import React, { useCallback, useEffect, useMemo, useRef, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useHistory } from 'react-router-dom'

import debounce from 'lodash/debounce'
import isEmpty from 'lodash/isEmpty'
import map from 'lodash/map'
import reduce from 'lodash/reduce'

import { useDialog } from '@smartcoop/dialog'
import I18n, { useT } from '@smartcoop/i18n'
import { emptyFilter, filter, noFieldRegistered, cutLayout, openLayout } from '@smartcoop/icons'
import { FieldActions } from '@smartcoop/stores/field'
import { selectFields } from '@smartcoop/stores/field/selectorField'
import { selectCurrentProperty } from '@smartcoop/stores/property/selectorProperty'
import { selectUserCanWrite } from '@smartcoop/stores/user/selectorUser'
import colors from '@smartcoop/styles/colors'
import { findMapZoom, getPolygonCenter } from '@smartcoop/utils/maps'
import Button from '@smartcoop/web-components/Button'
import EmptyState from '@smartcoop/web-components/EmptyState'
import FieldItem from '@smartcoop/web-components/FieldItem'
import Icon from '@smartcoop/web-components/Icon'
import InputSearch from '@smartcoop/web-components/InputSearch'
import Loader from '@smartcoop/web-components/Loader'
import Maps from '@smartcoop/web-components/Maps'
import Control from '@smartcoop/web-components/Maps/components/Control'
import Polygon from '@smartcoop/web-components/Maps/components/Polygon'
import SplitedScreenLayout from '@smartcoop/web-containers/layouts/SplitedScreenLayout'
import FilterFields from '@smartcoop/web-containers/modals/DigitalProperty/FilterFields'
import { useRoutes } from '@smartcoop/web-containers/routes/authenticated'

import {
  ButtonContainer,
  ButtonFilter,
  ContainerListFields,
  SearchContainer,
  SpaceButton
} from './styles'


const FieldListScreen = () => {
  const t = useT()
  const history = useHistory()
  const mounted = useRef(false)
  const mapRef = useRef(null)

  const { routes } = useRoutes()
  const { createDialog } = useDialog()
  const fields = useSelector(selectFields)
  const userWrite = useSelector(selectUserCanWrite)

  const [filters, setFilters] = useState({})
  const [loading, setLoading] = useState(false)
  const [filterText, setFilterText] = useState('')
  const [propertyHasFields, setPropertyHasFields] = useState(false)
  const [debouncedFilterText, setDebouncedFilterText] = useState('')
  const currentProperty = useSelector(selectCurrentProperty)
  const [opened, setOpened] = useState(true)

  const dispatch = useCallback(useDispatch(), [])

  const allPolygonCoordinates = useMemo(
    () => reduce(
      fields,
      (acc, field) => [
        ...acc,
        ...(field?.polygonCoordinates || [])
      ],
      []
    ),
    [fields]
  )

  const mapRegion = useMemo(
    () =>
      !isEmpty(allPolygonCoordinates)
        ? getPolygonCenter(allPolygonCoordinates)
        : undefined,
    [allPolygonCoordinates]
  )

  const mapZoom = useMemo(
    () =>
      !isEmpty(allPolygonCoordinates)
        ? findMapZoom(allPolygonCoordinates)
        : undefined,
    [allPolygonCoordinates]
  )

  const iconChange = useMemo(
    () => opened ? openLayout : cutLayout,
    [opened]
  )
  const handleParams = useCallback(
    (values) =>
      Object.keys(values)
        .filter((key) => typeof values[key] === 'boolean' || values[key])
        .reduce((prev, curr) => ({ ...prev, [curr]: values[curr] }), {}),
    []
  )

  const params = useMemo(() => {
    const filterParams = {
      ...filters,
      q: debouncedFilterText
    }
    return handleParams(filterParams)
  }, [debouncedFilterText, filters, handleParams])


  const handleRegisterFieldClick = useCallback(
    () => {
      if( mapRef && mapRef.current ) {
        const newRegion = { 'latitude':mapRef.current.leafletElement.getCenter().lat, 'longitude':mapRef.current.leafletElement.getCenter().lng }
        history.push(routes.fieldRegister.path, { region: newRegion, mapZoom: mapRef.current.viewport.zoom || mapZoom || 3 })
      } else history.push(routes.fieldRegister.path)
    }, [history, mapZoom, routes.fieldRegister.path]
  )

  const handleFieldClick = useCallback(
    ({ field }) => {
      history.push(routes.fieldDetails.path.replace(':fieldId', field.id))
    },
    [history, routes.fieldDetails.path]
  )

  const loadFields = useCallback(
    debounce(() => {
      dispatch(
        FieldActions.loadFields(
          params,
          () => setLoading(false),
          () => setLoading(false)
        )
      )
    }, 300),
    [dispatch, params]
  )

  const debouncedChangeSearchFilter = useCallback(
    debounce((value) => {
      setDebouncedFilterText(value)
    }, 300),
    []
  )

  const onChangeSearchFilter = useCallback(
    (e) => {
      setFilterText(e.target.value)
      debouncedChangeSearchFilter(e.target.value)
    },
    [debouncedChangeSearchFilter]
  )

  const handleFilter = useCallback((values) => {
    setLoading(true)
    setFilters(values)
  }, [])

  const filterFields = useCallback(() => {
    createDialog({
      id: 'filter-fields',
      Component: FilterFields,
      props: {
        onSubmit: handleFilter,
        filters
      }
    })
  }, [createDialog, filters, handleFilter])

  const toggleLayout = useCallback(() => {
    setOpened((old)=> !old)
  }, [])

  useEffect(() => {
    if (!isEmpty(currentProperty)) {
      setLoading(true)
      loadFields()
    }
  }, [dispatch, currentProperty, loadFields])

  useEffect(() => {
    dispatch(FieldActions.resetCurrentField())
  }, [dispatch])

  useEffect(() => {
    setPropertyHasFields(false)
    if (!isEmpty(currentProperty) && mounted.current) {
      setFilters({})
      setFilterText('')
      setDebouncedFilterText('')
    }
    mounted.current = true
  }, [currentProperty])

  useEffect(() => {
    if (fields?.length) {
      setPropertyHasFields(true)
    }
  }, [fields])

  useEffect(() => {
    mapRef.current && mapRef.current.leafletElement._onResize()
  }, [opened])

  return (
    <SplitedScreenLayout
      withoutGoBack
      withoutDivider
      title={ { name: t('field', { howMany: 2 }) } }
      divRightStyle={ { padding: 0 } }
      leftActions={ opened && (
        <>
          {propertyHasFields && (
            <SpaceButton>
              <ButtonContainer>
                <ButtonFilter
                  id="filter-field"
                  color={ isEmpty(filters) ? 'white' : 'secondary' }
                  onClick={ filterFields }
                  disabled={ loading }
                >
                  <>
                    <Icon
                      style={ { marginRight: '8px' } }
                      size={ 14 }
                      icon={ filter }
                    />
                    <I18n>filtrate</I18n>
                  </>
                </ButtonFilter>
                {userWrite && (
                  <Button
                    id="register-field"
                    onClick={ handleRegisterFieldClick }
                    color="secondary"
                    disabled={ loading|| isEmpty(currentProperty) }
                  >
                    <I18n>register field</I18n>
                  </Button>
                )}
              </ButtonContainer>
            </SpaceButton>
          )}
        </>
      ) }
      leftChildren={ opened && (
        <>
          {propertyHasFields && (
            <SearchContainer>
              <InputSearch
                style={ { marginTop: 8 } }
                adornmentStyle={ { marginRight: 15 } }
                detached
                onChange={ onChangeSearchFilter }
                value={ filterText }
                placeholder={ t('search') }
                fullWidth
              />
            </SearchContainer>
          )}

          <ContainerListFields isCenter={ isEmpty(fields) }>
            {loading ? (
              <Loader width={ 100 } />
            ) : (
              map(fields, (item) => (
                <FieldItem
                  field={ item }
                  key={ item.id }
                  onClick={ handleFieldClick }
                />
              ))
            )}

            {!loading && !propertyHasFields && (
              <EmptyState
                text={ t('no field registered') }
                icon={ noFieldRegistered }
                buttonProps={ { disabled: isEmpty(currentProperty) } }
                action={ {
                  text: t('register field'),
                  onClick: handleRegisterFieldClick
                } }
              />
            )}
            {!loading && propertyHasFields && isEmpty(fields) && (
              <EmptyState text={ t('no results found') } icon={ emptyFilter } />
            )}
          </ContainerListFields>
        </>
      ) }
      rightChildren={
        <>
          {!isEmpty(allPolygonCoordinates) && (
            <Maps
              ref={ mapRef }
              region={ mapRegion }
              zoom={ mapZoom }
              style={ { flex: 1, overflow: 'none' } }
              cursor="crosshair"
            >
              <Control position="topcenter">
                <div style={ { height: 70, display: 'flex' } }>
                  <div
                    style={ {
                      margin: '10px 20px 10px 10px',
                      justifyContent: 'flex-end'
                    } }
                  >
                    <Button
                      id="toggle-Layout"
                      color={ colors.white }
                      style={ {
                        padding: '7.5px 10px',
                        fontSize: '0.875rem'
                      } }
                      onClick={ toggleLayout }
                    >
                      <Icon
                        icon={ iconChange }
                        size={ 17 }
                        style={ { marginLeft: 5 } }
                      />
                    </Button>
                  </div>
                </div>
              </Control>
              {map(fields, ({ polygonCoordinates, id }) => (
                <Polygon
                  key={ id }
                  points={ polygonCoordinates }
                  color={ colors.white }
                />
              ))}
            </Maps>
          )}
        </>
      }
    />
  )
}

export default FieldListScreen
