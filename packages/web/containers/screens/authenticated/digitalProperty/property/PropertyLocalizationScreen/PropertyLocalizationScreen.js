import React, { useState, useCallback, useMemo, useEffect, useRef } from 'react'
import useGeolocation from 'react-hook-geolocation'
import { useDispatch, useSelector } from 'react-redux'

import PropTypes from 'prop-types'

import isEmpty from 'lodash/isEmpty'

import I18n from '@smartcoop/i18n'
import { marker as markerIcon , markerClosed as markerClosedIcon } from '@smartcoop/icons'
import { PropertyActions } from '@smartcoop/stores/property'
import { selectOfflineProperty } from '@smartcoop/stores/property/selectorProperty'
import { DEFAULT_COORDINATES } from '@smartcoop/utils/maps'
import Card from '@smartcoop/web-components/Card'
import Maps from '@smartcoop/web-components/Maps'
import Control from '@smartcoop/web-components/Maps/components/Control'
import PinMarker from '@smartcoop/web-components/Maps/markers/PinMarker'
import { Title } from '@smartcoop/web-containers/layouts/AuthenticatedLayout/theme'

import { Container, MapContainer } from './styles'

const PropertyLocalizationScreen = ({ onConfirm, confirmedMap }) => {
  const geolocation = useGeolocation()
  const dispatch = useCallback(useDispatch(), [])

  const property = useSelector(selectOfflineProperty)

  const [markerCoordinate, setMarkerCoordinate] = useState(
    property.data.geolocalization.latitude
      ? property.data.geolocalization
      : undefined
  )

  const selectingGeo = useMemo(
    () => !confirmedMap,
    [confirmedMap]
  )

  const center = useMemo(
    () => {
      if (isEmpty(markerCoordinate)) {
        if (
          !geolocation.error
          && geolocation.latitude
          && geolocation.longitude
        ) {
          return {
            latitude: geolocation.latitude,
            longitude: geolocation.longitude
          }
        }
        return DEFAULT_COORDINATES
      }
      return markerCoordinate
    },
    [geolocation, markerCoordinate]
  )


  const defaultZoom = useMemo(() => (markerCoordinate ? 17 : 6), [markerCoordinate])

  const mounted = useRef(true)

  const disabled = useMemo(
    () => isEmpty(markerCoordinate),
    [markerCoordinate]
  )

  const onDragendMarker = useCallback((event) => {
    if (selectingGeo) {
      setMarkerCoordinate({
        latitude: event.target._latlng.lat,
        longitude: event.target._latlng.lng
      })
    }
  }, [selectingGeo])

  const savePropertyLocation = useCallback(
    () => {
      if (!disabled) {

        dispatch(PropertyActions.updateOfflineProperty({
          data: {
            geolocalization: markerCoordinate
          }
        }))

        onConfirm()
      }
    },
    [disabled, dispatch, markerCoordinate, onConfirm]
  )

  useEffect(
    () => {
      if (!mounted.current) {
        setMarkerCoordinate(center)
      }
    },
    [center, mounted]
  )
  useEffect(() => {
    if(markerCoordinate) {
      onConfirm()
      savePropertyLocation()
    }
  },[markerCoordinate, onConfirm, savePropertyLocation])

  useEffect(() => () => {
    mounted.current = false
  }, [mounted])

  return (
    <Container>
      <MapContainer>
        <Maps
          region={ markerCoordinate }
          zoom={ defaultZoom }
          layer="google"
          center={ center }
          onClick={ setMarkerCoordinate }
          cursor="crosshair"
        >

          <Control position="topcenter">
            <Card style={ { marginBottom: 0, marginTop: 0, marginRight: 70 } }>
              <Title align="center" style={ { padding: '0 40px', marginTop: 0, fontSize: 16 } }>
                <I18n>mark your property on the map</I18n>
              </Title>
            </Card>
          </Control>
          <PinMarker
            coordinate={ markerCoordinate }
            draggable={ selectingGeo }
            onDragend={ onDragendMarker }
            customIcon={ selectingGeo ? markerIcon : markerClosedIcon }
          />
        </Maps>
      </MapContainer>
    </Container>
  )
}

PropertyLocalizationScreen.propTypes = {
  onConfirm: PropTypes.func,
  confirmedMap: PropTypes.bool
}

PropertyLocalizationScreen.defaultProps = {
  onConfirm: () => {},
  confirmedMap: false
}

export default PropertyLocalizationScreen
