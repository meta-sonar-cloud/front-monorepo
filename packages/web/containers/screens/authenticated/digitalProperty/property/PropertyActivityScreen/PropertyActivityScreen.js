import React, { useRef } from 'react'

import PropTypes from 'prop-types'

import I18n from '@smartcoop/i18n'
import { barn } from '@smartcoop/icons'
import Button from '@smartcoop/web-components/Button'
import Card from '@smartcoop/web-components/Card'
import Icon from '@smartcoop/web-components/Icon'
import PropertyActivityForm from '@smartcoop/web-containers/forms/digitalProperty/property/PropertyActivityForm'
import { Item, Title, ButtonsContainer } from '@smartcoop/web-containers/layouts/AuthenticatedLayout/theme'

import { IconContainer } from './styles'

const PropertyActivityScreen = (props) => {
  const { onConfirm, onCancel } = props

  const formRef = useRef(null)

  return (
    <Card style={ { marginTop: 0 } } cardStyle={ { minWidth: 450 } }>
      <Item>
        <IconContainer>
          <Icon icon={ barn } size={ 25 } />
        </IconContainer>

        <Title>
          <I18n>property registration</I18n>
        </Title>

        <PropertyActivityForm
          style={ { display: 'flex', flexDirection: 'column' } }
          ref={ formRef }
          onSubmit={ onConfirm }
          withoutSubmitButton
        />

        <ButtonsContainer>
          <Button
            id="cancel-property-activity-details-form"
            onClick={ onCancel }
            variant="outlined"
            style={ { width: '40%', flex: '0 0 auto' } }
          >
            <I18n>go back</I18n>
          </Button>

          <Button
            id="submit-property-activity-form"
            onClick={ () => formRef.current.submit() }
            style={ { width: '40%', flex: '0 0 auto' } }
          >
            <I18n>next</I18n>
          </Button>
        </ButtonsContainer>
      </Item>
    </Card>
  )
}

PropertyActivityScreen.propTypes = {
  onConfirm: PropTypes.func,
  onCancel: PropTypes.func
}

PropertyActivityScreen.defaultProps = {
  onConfirm: () => {},
  onCancel: () => {}
}

export default PropertyActivityScreen
