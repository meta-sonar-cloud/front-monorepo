import React, { useMemo, useCallback, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { useHistory } from 'react-router-dom'

import { useWindowHeight } from '@react-hook/window-size'

import Grid from '@material-ui/core/Grid'
import { useTheme } from '@material-ui/core/styles'
import useMediaQuery from '@material-ui/core/useMediaQuery'

import I18n, { useT } from '@smartcoop/i18n'
import { rain, calendar } from '@smartcoop/icons'
import { PropertyActions } from '@smartcoop/stores/property'
import { selectCurrentProperty, selectReloadData } from '@smartcoop/stores/property/selectorProperty'
import { selectUser } from '@smartcoop/stores/user/selectorUser'
import { colors } from '@smartcoop/styles'
import Icon from '@smartcoop/web-components/Icon'
import PropertyActivitiesChart from '@smartcoop/web-containers/fragments/PropertyActivitiesChart'
import PropertyGrowingSeasonsChart from '@smartcoop/web-containers/fragments/PropertyGrowingSeasonsChart'
import PropertyPendingManagementsList from '@smartcoop/web-containers/fragments/PropertyPendingManagementsList'
import PropertyResumeCard from '@smartcoop/web-containers/fragments/PropertyResumeCard'
import RainMapFragmentCard from '@smartcoop/web-containers/fragments/RainMapFragmentCard'
import WeatherForecastCard from '@smartcoop/web-containers/fragments/WeatherForecastCard'
import { useRoutes } from '@smartcoop/web-containers/routes/authenticated'

import {
  Container,
  CardItemTitle,
  CardItemBackground,
  HeaderTitle,
  CardButton,
  HeaderTitleBold
} from './styles'

const PropertyHomeScreen = () => {
  const user = useSelector(selectUser)
  const currentProperty = useSelector(selectCurrentProperty)
  const t = useT()
  const history = useHistory()
  const { routes } = useRoutes()

  const reloadData = useSelector(selectReloadData)

  const dispatch = useCallback(useDispatch(), [])
  const goToRainRecordList = useCallback(
    () => history.push(routes.rainRecordList.path),
    [history, routes]
  )

  const windowHeight = useWindowHeight()

  const theme = useTheme()
  const breakRightToBottom = useMediaQuery(theme.breakpoints.down('sm'))
  const breakMobile = useMediaQuery(theme.breakpoints.down('xs'))

  useEffect(() => {
    if(reloadData){
      dispatch(PropertyActions.setReloadData(false))
    }
  },[dispatch, reloadData])

  const rightContent = useMemo(
    () => (
      <Grid
        container
        spacing={ 2 }
        style={ breakRightToBottom ? { marginTop: 5, marginBottom: 15 } : undefined }
      >
        <Grid
          item
          xs={ 12 }
          sm={ 6 }
          md={ 12 }
        >
          <Grid container>

            <Grid item xs={ 12 }>
              <WeatherForecastCard />
            </Grid>

            <Grid item xs={ 12 }>
              <CardButton
                id="rain-register"
                onClick={ goToRainRecordList }
              >
                <>
                  <Icon
                    icon={ rain }
                    color={ colors.text }
                    size={ 20 }
                    style={ { marginRight: 10 } }
                  />
                  <I18n>rain record</I18n>
                </>
              </CardButton>
            </Grid>
          </Grid>

          <Grid item xs={ 12 } sm={ 6 } md={ 12 }>
            <RainMapFragmentCard property={ currentProperty } />
          </Grid>

          <Grid item xs={ 12 } sm={ 6 } md={ 12 }>
            <PropertyResumeCard property={ currentProperty } />
          </Grid>
        </Grid>

      </Grid>
    ),
    [breakRightToBottom, currentProperty, goToRainRecordList]
  )

  return (
    <Container>

      <Grid
        container
        spacing={ 2 }
        style={ {
          flex: 1,
          paddingTop: 15,
          paddingBottom: 0,
          paddingLeft: 15,
          paddingRight: 5,
          height: '100%'
        } }
      >

        <Grid
          item
          xs={ 12 }
          sm={ 12 }
          md={ 8 }
          lg={ 9 }
          style={ {
            display: 'flex',
            flexDirection: 'column'
          } }
        >

          <Grid container style={ { flex: '0 1 auto' } }>
            <Grid item xs={ 12 }>
              <CardItemBackground>
                <I18n as={ HeaderTitle } params={ { user: user?.name } }>
                  welcome
                </I18n>
                <HeaderTitleBold>
                  {user?.name}!
                </HeaderTitleBold>
              </CardItemBackground>
            </Grid>
          </Grid>

          {breakRightToBottom && rightContent}

          <Grid
            container
            spacing={ 2 }
            style={ {
              flex: 1,
              paddingBottom: 15,
              flexDirection: breakMobile ? 'column-reverse' : 'row'
            } }
          >
            <Grid
              item xs={ 12 }
              sm={ 5 }
              md={ 4 }
              style={ {
                display: 'flex'
              } }
            >
              <CardItemTitle
                title={ (
                  <div style={ { display: 'flex' } }>
                    <Icon
                      icon={ calendar }
                      color={ colors.text }
                      size={ 20 }
                      style={ { marginRight: 10 } }
                    />
                    <I18n>
                      crops management scheduled
                    </I18n>
                  </div>
                ) }
                style={ {
                  justifyContent: 'flex-start',
                  marginBottom: breakRightToBottom ? 7.5 : 0
                } }
                childrenStyle={ {
                  maxHeight: breakRightToBottom ? 710 : (windowHeight - 265),
                  overflowY: 'auto',
                  padding: 0
                } }
                headerStyle={ { marginBottom: 0 } }
              >
                <PropertyPendingManagementsList property={ currentProperty } />
              </CardItemTitle>
            </Grid>

            <Grid item xs={ 12 } sm={ 7 } md={ 8 } style={ { display: 'flex' } }>
              <Grid container >

                <Grid item xs={ 12 }>
                  <CardItemTitle
                    title={ t('activity', { howMany: 1 }) }
                  >
                    <PropertyActivitiesChart property={ currentProperty } />
                  </CardItemTitle>
                </Grid>

                <Grid item xs={ 12 }>
                  <CardItemTitle
                    title={ t('finality and culture') }
                  >
                    <PropertyGrowingSeasonsChart property={ currentProperty } />
                  </CardItemTitle>
                </Grid>

              </Grid>
            </Grid>

          </Grid>
        </Grid>

        {!breakRightToBottom && (
          <Grid
            item
            xs={ 12 }
            sm={ 12 }
            md={ 4 }
            lg={ 3 }
          >
            <Container style={ { width: '100%' } }>
              {rightContent}
            </Container>
          </Grid>
        )}

      </Grid>
    </Container>
  )
}

PropertyHomeScreen.propTypes = {

}

export default PropertyHomeScreen
