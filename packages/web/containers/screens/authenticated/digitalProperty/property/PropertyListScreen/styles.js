import styled from 'styled-components'

import { colors } from '@smartcoop/styles'
import OriginalCard from '@smartcoop/web-components/Card'

export const Container = styled.div`
  width: 100%;
  padding: 7vh 10vw;
`

export const Header = styled.div`
  display: flex;
  width: 100%;
  justify-content: flex-end;

  button {
    flex: 0 0 200px;

    span {
      font-weight: bold;
    }
  }
`

export const Content = styled.div`
  display: grid;
  grid-template-columns: 32% 32% 32%;
  grid-template-rows: auto;
  column-gap: 2%;
  row-gap: 3vh;
  max-width: 100%;
  
  padding-bottom: 100px;

  margin-top: 40px;

  @media (min-width: 2000px) {
    grid-template-columns: 24.5% 24.5% 24.5% 24.5%;
  }

  @media (max-width: 1200px) {
    grid-template-columns: 49% 49%;
  }

  @media (max-width: 780px) {
    grid-template-columns: 100%;
  }
`

export const Card = styled(OriginalCard)`
  margin: 0;

  > div {
    width: 100%;
    flex-grow: 0;
    min-height: 100px;
    padding: 10px;
  }
`

export const CardHeader = styled.div`
  display: flex;
  align-items: center;

  div:first-child {
    margin-right: 10px;
    margin-left: 5px;
  }

  span {
    font-weight: bold;
  }

  div:last-child {
    margin-left: auto;

    * {
      margin: 0;
      padding: 0;
    }
  }
`

export const CardBody = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  padding: 20px 5%;

  > div:not(:first-of-type) {
    margin-top: 20px;
  }
`

export const Line = styled.div`
  display: flex;
  width: 100%;
`

export const Item = styled.div`
  flex: 1;
  display: flex;
  justify-content: center;
  flex-direction: column;

  > span:first-child {
    font-weight: bold;
    color: ${ colors.darkGrey }
  }
`

export const ButtonContainer = styled.div`
  display: flex;
  flex-direction: column;

  button {
    padding-left: 10px;
    padding-right: 10px;
    border-radius: 0;
    font-size: .9rem;
  }
`