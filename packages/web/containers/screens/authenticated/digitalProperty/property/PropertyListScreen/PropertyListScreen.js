import React, { useCallback } from 'react'
import { useSelector, useDispatch } from 'react-redux'

import { isEmpty, map } from 'lodash'

import { useDialog } from '@smartcoop/dialog'
import I18n from '@smartcoop/i18n'
import {
  barnYellow,
  edit,
  trash
} from '@smartcoop/icons'
import { PropertyActions } from '@smartcoop/stores/property'
import { selectProperties } from '@smartcoop/stores/property/selectorProperty'
import { selectUserCanWrite } from '@smartcoop/stores/user/selectorUser'
import { colors } from '@smartcoop/styles'
import Button from '@smartcoop/web-components/Button'
import Icon from '@smartcoop/web-components/Icon'
import VerticalDotsIconButton from '@smartcoop/web-components/IconButton/VerticalDotsIconButton'
import Popover from '@smartcoop/web-components/Popover'
import CreatePropertyModal from '@smartcoop/web-containers/modals/CreatePropertyModal'
import EditPropertyModal from '@smartcoop/web-containers/modals/EditPropertyModal'

import { Container, Header, Content, Card, CardHeader, CardBody, Line, Item, ButtonContainer } from './styles'

const PropertyListScreen = () => {
  const { createDialog } = useDialog()
  const dispatch = useDispatch()

  const properties = useSelector(selectProperties)
  const userWrite = useSelector(selectUserCanWrite)

  const onConfirm = useCallback(
    () => {
      createDialog({
        id: 'property-onboarding',
        Component: CreatePropertyModal
      })
    },
    [createDialog]
  )

  const editProperty = useCallback(property => {
    dispatch(PropertyActions.setEditPropertyData(property.id, property))

    createDialog({
      id: 'property-onboarding',
      Component: EditPropertyModal
    })
  }, [createDialog, dispatch])

  const deleteProperty = useCallback(property => {
    dispatch(PropertyActions.deleteProperty(property.id))
  }, [dispatch])

  const formatHa = useCallback((string) => Number(string)?.toFixed(2)?.replace('.', ',') || '0,00', [])

  const formatActivities = useCallback((array) => {
    const newArray = map(array, item => item.name)

    return Array.isArray(newArray) && !isEmpty(newArray) && newArray.join('; ')
  }, [])

  return (
    <Container>
      {userWrite && (
        <Header>
          <Button
            id="register-property"
            onClick={ () => {
              onConfirm()
            } }
            color="secondary"
          >
            <I18n>new property</I18n>
          </Button>
        </Header>
      )}
      <Content>
        {map(properties, (property, index) => (
          <Card key={ property.id + index }>
            <CardHeader>
              <Icon icon={ barnYellow } size={ 33 } color={ colors.primary } />
              <span>{property.name}</span>

              {userWrite && (
                <Popover
                  popoverId="property-actions"
                  Component={ VerticalDotsIconButton }
                  ComponentProps={ {
                    color: colors.black,
                    edge: 'end',
                    iconColor: colors.black
                  } }
                >
                  <ButtonContainer>
                    <Button onClick={ () => editProperty(property, index) } color={ colors.white } id="edit-property">
                      <>
                        <Icon style={ { marginRight: 10 } } icon={ edit } size={ 15 } color={ colors.primary } />
                        <I18n>edit property</I18n>
                      </>
                    </Button>
                    <Button onClick={ () => deleteProperty(property, index) } color={ colors.white } id="delete-property">
                      <>
                        <Icon style={ { marginRight: 10 } } icon={ trash } size={ 15 } color={ colors.red } />
                        <I18n style={ { color: colors.red } }>delete property</I18n>
                      </>
                    </Button>
                  </ButtonContainer>
                </Popover>
              )}
            </CardHeader>
            <CardBody>
              <Line>
                <Item>
                  <span><I18n>total area</I18n></span>
                  <span>{formatHa(property.totalArea)} <I18n>ha</I18n></span>
                </Item>
              </Line>
              {!isEmpty(property.activities) && (
                <Line>
                  <Item>
                    <span><I18n params={ { howMany: 2 } }>activity</I18n></span>
                    <span>{formatActivities(property.activities)}</span>
                  </Item>
                </Line>
              )}
            </CardBody>
          </Card>
        ))}
      </Content>
    </Container>
  )
}

export default PropertyListScreen
