import React, { useRef, useCallback, useMemo } from 'react'
import { useSelector } from 'react-redux'
import { useHistory } from 'react-router-dom'

import isEmpty from 'lodash/isEmpty'

import I18n, { useT } from '@smartcoop/i18n'
import { calendar } from '@smartcoop/icons'
import { useSnackbar } from '@smartcoop/snackbar'
import { selectCurrentField } from '@smartcoop/stores/field/selectorField'
import colors from '@smartcoop/styles/colors'
import { getPolygonCenter, findMapZoom } from '@smartcoop/utils/maps'
import Button from '@smartcoop/web-components/Button'
import Icon from '@smartcoop/web-components/Icon'
import Maps from '@smartcoop/web-components/Maps'
import Polygon from '@smartcoop/web-components/Maps/components/Polygon'
import ManagementForm from '@smartcoop/web-containers/forms/digitalProperty/management/ManagementForm'
import { ButtonsContainer } from '@smartcoop/web-containers/layouts/AuthenticatedLayout/theme'
import SplitedScreenLayout from '@smartcoop/web-containers/layouts/SplitedScreenLayout'

import { Container, Header, Title, IconContainer } from './styles'

const Management = () => {
  const managementFormRef = useRef(null)
  const history = useHistory()
  const t = useT()
  const snackbar = useSnackbar()

  const { growingSeason, polygonCoordinates } = useSelector(selectCurrentField)

  const handleClose = useCallback(
    () => {
      history.goBack()
    },
    [history]
  )

  const onSuccess = useCallback(
    () => {
      snackbar.success(
        t('your {this} was registered', {
          howMany: 1,
          gender: 'male',
          this: t('cropManagement', { howMany: 1 })
        })
      )
      handleClose()
    },
    [handleClose, snackbar, t]
  )

  const submitForms = useCallback(
    () => {
      managementFormRef.current.submit()
    },
    []
  )

  const mapCenter = useMemo(
    () => !isEmpty(polygonCoordinates) ? getPolygonCenter(polygonCoordinates) : undefined,
    [polygonCoordinates]
  )
  const mapZoom = useMemo(
    () => !isEmpty(polygonCoordinates) ? findMapZoom(polygonCoordinates) : undefined,
    [polygonCoordinates]
  )

  return (
    <SplitedScreenLayout
      title={ { name: t('field', { howMany: 1 }) } }
      divRightStyle={ { padding: 0 } }
      leftChildren={ (
        <Container>
          <Header>
            <IconContainer>
              <Icon icon={ calendar } size={ 18 } />
            </IconContainer>

            <Title style={ { fontSize: 18, fontWeight: 600 } }>
              <I18n>management register</I18n>
            </Title>

          </Header>

          <ManagementForm
            ref={ managementFormRef }
            withoutSubmitButton
            growingSeason={ growingSeason }
            onSuccess={ onSuccess }
          />

          <ButtonsContainer style={ { paddingTop: 10 } }>
            <Button
              id="cancel-crop-management-form"
              onClick={ handleClose }
              style={ { flex: 1 } }
              variant="outlined"
            >
              <I18n>cancel</I18n>
            </Button>

            <div style={ { width: '10%' } } />

            <Button
              id="submit-crop-management-form"
              onClick={ submitForms }
              style={ { flex: 1 } }
            >
              <I18n>save</I18n>
            </Button>
          </ButtonsContainer>
        </Container>
      ) }
      rightChildren={ (
        <Maps
          zoom={ mapZoom }
          style={ { minHeight: 500 } }
          region={ mapCenter }
        >
          <Polygon
            points={ polygonCoordinates }
            color={ colors.secondary }
          />
        </Maps>
      ) }
    />
  )}

export default Management
