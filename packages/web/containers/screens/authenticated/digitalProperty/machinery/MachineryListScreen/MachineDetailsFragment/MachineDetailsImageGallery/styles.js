import styled from 'styled-components'

import colors from '@smartcoop/styles/colors'

export const CurrentImage = styled.img`
  border-radius: 8px;
  background-color: ${ colors.lightGrey };
  max-width: 640px;
  max-height: 480px;
  width: 100%;
  object-fit: contain;
`

export const Container = styled.div`
  display: flex;
  flex: 1;
  justify-content: center;
  align-content: center;
  margin: 20px;
`

export const ImagesContainer = styled.div`
  display: flex;
  justify-content: center;
  flex-direction: column;
`

export const ThumbsContainer = styled.div`
  display: flex;
  flex-wrap: wrap;
`

export const CurrentImageContainer = styled.div`
  width: 640px;
  height: 480px;
  text-align: center;
  justify-content: center;
  display: flex;
`

export const Thumb = styled.div`
  border-radius: 8px;
  background: ${ colors.lightGrey };
  width: 120px;
  height: 120px;
  margin: 5px;
  cursor: pointer;
  border: 2px solid transparent;
  border-color: ${ props => props.isActive ? colors.yellow : 'transparent' };
  object-fit: contain;

`

export const ThumbImage = styled.img`
  border-radius: 8px;
  background-color: ${ colors.lightGrey };
  height:116px;
  width: 116px;
  object-fit: contain;
`
