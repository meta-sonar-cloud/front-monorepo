import React, { useCallback, useEffect, useMemo, useState } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { useHistory } from 'react-router-dom'

import debounce from 'lodash/debounce'
import isEmpty from 'lodash/isEmpty'
import map from 'lodash/map'

import { useDialog } from '@smartcoop/dialog'
import I18n, { useT } from '@smartcoop/i18n'
import { emptyFilter } from '@smartcoop/icons'
import { useSnackbar } from '@smartcoop/snackbar'
import { MachineActions } from '@smartcoop/stores/machine'
import { selectMachinery } from '@smartcoop/stores/machine/selectorMachine'
import { selectCurrentProperty } from '@smartcoop/stores/property/selectorProperty'
import colors from '@smartcoop/styles/colors'
import Button from '@smartcoop/web-components/Button'
import EmptyState from '@smartcoop/web-components/EmptyState'
import InputSearch from '@smartcoop/web-components/InputSearch'
import Loader from '@smartcoop/web-components/Loader'
import MachineItem from '@smartcoop/web-components/MachineItem'
import ConfirmModal from '@smartcoop/web-components/Modal/ConfirmModal'
import SplitedScreenLayout from '@smartcoop/web-containers/layouts/SplitedScreenLayout'
import { useRoutes } from '@smartcoop/web-containers/routes/authenticated'

import MachineDetailsFragment from './MachineDetailsFragment'
import { ContainerListFields, SpaceButton, SearchContainer, ButtonContainer, TabsContainer } from './styles'

const MachineryListScreen = () => {
  const history = useHistory()
  const { routes } = useRoutes()
  const [loading, setLoading] = useState(false)
  const [activeTab, setActiveTab] = useState('all-machines')
  const [isUserMachine, setIsUserMachine] = useState(false)
  const [selectedMachine, setSelectedMachine] = useState({})
  const [filterText, setFilterText] = useState('')
  const [debouncedFilterText, setDebouncedFilterText] = useState('')
  const t = useT()
  const { createDialog } = useDialog()
  const snackbar = useSnackbar()

  const machinery = useSelector(selectMachinery)
  const currentProperty = useSelector(selectCurrentProperty)

  const dispatch = useCallback(useDispatch(), [])

  const handleMachineryRegisterClick = useCallback(
    () => {
      history.push(routes.machineryRegister.path)
    },
    [history, routes]
  )

  const handleItemClick = useCallback(
    ({ machine }) => {
      setSelectedMachine(machine)
    },
    []
  )

  const handleParams = useCallback(
    (values) => (
      Object.keys(values)
        .filter(
          (key) => typeof values[key] === 'boolean' || values[key]
        )
        .reduce(
          (prev, curr) => ({ ...prev, [curr]: values[curr] }), {}
        )
    ),
    []
  )

  const filters = useMemo(
    () => ({
      my_machines: activeTab === 'my-machines',
      property_id: currentProperty.id
    }), [activeTab, currentProperty]
  )

  const params = useMemo(
    () => {
      const filterParams = ({
        ...filters,
        q: debouncedFilterText
      })
      return handleParams(filterParams)
    },
    [debouncedFilterText, filters, handleParams]
  )

  const loadMachinery = useCallback(
    debounce(
      () => {
        dispatch(MachineActions.loadMachinery(
          params,
          (data) => {
            setSelectedMachine(data[0])
            setLoading(false)
            setIsUserMachine(activeTab === 'my-machines')
          },
          () => setLoading(false)
        ))
      },
      300
    ),
    [dispatch, params]
  )

  const debouncedChangeSearchFilter = useCallback(
    debounce((value) => {
      setDebouncedFilterText(value)
    }, 300),
    []
  )

  const onChangeSearchFilter = useCallback(
    (e) => {
      setFilterText(e.target.value)
      debouncedChangeSearchFilter(e.target.value)
    },
    [debouncedChangeSearchFilter]
  )


  const handleActiveTab = useCallback(
    (tabName) => {
      setLoading(true)
      setActiveTab(tabName)
      setLoading(false)
    }
    ,
    []
  )

  const handleDeleteMachine = useCallback(
    (id) => {
      createDialog({
        id: 'confirm-delete',
        Component: ConfirmModal,
        props: {
          onConfirm: () => {
            setLoading(true)
            dispatch(MachineActions.deleteMachine(id,
              () => {
                snackbar.success(
                  t('your {this} was deleted', {
                    howMany: 1,
                    gender: 'female',
                    this: t('machine', { howMany: 1 })
                  })
                )
                loadMachinery()
              },
              () => {
                setLoading(false)
              }
            ))
          },
          textWarning: t('are you sure you want to delete the machine?')
        }
      })
    },
    [createDialog, dispatch, loadMachinery, snackbar, t]
  )

  useEffect(
    () => {
      if (!isEmpty(currentProperty)) {
        setLoading(true)
        loadMachinery()
      }
    },
    [
      dispatch,
      currentProperty,
      loadMachinery
    ]
  )

  return (
    <SplitedScreenLayout
      withoutGoBack
      withoutDivider
      title={ { name: t('machinery', { howMany: 2 }) } }
      divRightStyle={ { padding: 0, maxHeight: 'calc(100vh - 90px)' } }
      leftActions={ (
        <>
          <SpaceButton>
            <ButtonContainer>
              <Button
                id="register-machinery"
                onClick={ handleMachineryRegisterClick }
                color="secondary"
                disabled={ loading }
              >
                <I18n>register machinery</I18n>
              </Button>
            </ButtonContainer>
          </SpaceButton>
        </>
      ) }
      leftChildren={ (
        <>
          <TabsContainer>
            <Button
              id="all-machines-button"
              onClick={ () => handleActiveTab('all-machines') }
              title={ t('rental machines') }
              variant="text"
              style={ {
                fontSize: 14,
                flex: 1,
                fontWeight: 'bold',
                backgroundColor: 'transparent',
                borderBottom: '5px solid',
                borderColor: `${ activeTab === 'all-machines' ? colors.black : colors.transparent }`,
                color: `${ activeTab === 'all-machines' ? colors.black : colors.grey }`,
                borderRadius: 0
              } }
            >
              <I18n>rental machines</I18n>
            </Button>
            <Button
              id="my-machines-button"
              onClick={ () => handleActiveTab('my-machines') }
              variant="text"
              style={ {
                fontSize: 14,
                flex: 1,
                fontWeight: 'bold',
                backgroundColor: 'transparent',
                borderBottom: '5px solid',
                borderColor: `${ activeTab === 'my-machines' ? colors.black : colors.transparent }`,
                color: `${ activeTab === 'my-machines' ? colors.black : colors.grey }`,
                borderRadius: 0
              } }
            >
              <I18n>my machines</I18n>
            </Button>
          </TabsContainer>
          <SearchContainer>
            <InputSearch
              style={ { marginTop: 8 } }
              adornmentStyle={ { marginRight: 15 } }
              detached
              onChange={ onChangeSearchFilter }
              value={ filterText }
              placeholder={ t('search') }
              fullWidth
            />
          </SearchContainer>
          <ContainerListFields isCenter={ isEmpty(machinery) }>

            {loading ? (
              <Loader width={ 100 } />
            ) :
              (map(machinery, (item) => (
                <MachineItem
                  machine={ item }
                  key={ item.id }
                  onClick={ handleItemClick }
                  active={ item.id === selectedMachine.id }
                />
              )))
            }
            {!loading && isEmpty(machinery) && (
              <EmptyState
                text={ t('no results found') }
                icon={ emptyFilter }
              />
            ) }
          </ContainerListFields>
        </>
      ) }
      rightChildren={ (
        <>
          {
            loading ? <Loader width={ 100 } /> : (
              !isEmpty(selectedMachine) &&
              <MachineDetailsFragment
                machine={ selectedMachine }
                isUserMachine={ isUserMachine }
                onDeleteMachine={ handleDeleteMachine }
              />
            )
          }
          {}
        </>
      ) }
    />
  )}

export default MachineryListScreen
