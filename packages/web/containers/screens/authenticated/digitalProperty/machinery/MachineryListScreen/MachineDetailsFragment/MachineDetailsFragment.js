import React, { useMemo, useCallback } from 'react'
import { useHistory } from 'react-router-dom'

import PropTypes from 'prop-types'

import { isEmpty } from 'lodash'

import Divider from '@material-ui/core/Divider'

import I18n from '@smartcoop/i18n'
import { pencil, trash } from '@smartcoop/icons'
import colors from '@smartcoop/styles/colors'
import Button from '@smartcoop/web-components/Button'
import Icon from '@smartcoop/web-components/Icon'
import { useRoutes } from '@smartcoop/web-containers/routes/authenticated'


import MachineDetailsImageGallery from './MachineDetailsImageGallery/MachineDetailsImageGallery'
import {
  Container,
  ButtonContainer,
  WhiteContainer,
  Title,
  InfoContainer,
  MidContainer,
  DistanceText,
  Subtitle,
  Text,
  TextInfo,
  TextBox,
  TitleScreen,
  FirstContainer,
  Half
} from './styles'

const MachineDetailsFragment = ({ machine, isUserMachine, onDeleteMachine }) => {
  const images = useMemo(() => machine?.machineFiles ?? [], [machine])
  const history = useHistory()
  const { routes } = useRoutes()

  const distance = useMemo(
    () => (machine.property?.distance ? (`${ machine.property.distance  } km`) : ''),
    [machine]
  )

  const editMachine = useCallback(
    () => {
      history.push(routes.machineryEdit.path, { machine: { ...machine } })

    }, [history, machine, routes.machineryEdit.path]
  )

  const deleteMachine = useCallback(
    () => {
      onDeleteMachine(machine.id)
    }, [machine.id, onDeleteMachine]
  )

  return (
    <FirstContainer>
      <TitleScreen>
        <I18n>
          machine details
        </I18n>
        {isUserMachine && <ButtonContainer>
          <Button
            id="web-edit-machine"
            style={ { flex: '0 0 auto' } }
            variant="outlined"
            onClick={ editMachine }
          >
            <>
              <Icon
                icon={ pencil }
                size={ 14 }
                style={ { marginRight: 5 } }
              />
              <I18n>edit</I18n>
            </>
          </Button>
          <Button
            id="deleteCropButton"
            variant="outlined"
            style={ { flex: '0 0 auto' } }
            onClick={ deleteMachine }
          >
            <Icon icon={ trash } size={ 14 } color={ colors.red } />
          </Button>
        </ButtonContainer>
        }
      </TitleScreen>
      <Divider />
      <WhiteContainer>
        <InfoContainer>
          <Title>
            {machine.description}
          </Title>
          <DistanceText>
            {distance}
          </DistanceText>
        </InfoContainer>
        {!isEmpty(images) && ( <>
          <Container>
            <MachineDetailsImageGallery images={ images } />
          </Container>
        </>)
        }
        <MidContainer>
          <Half>
            <Subtitle>
              <I18n>
              informations
              </I18n>
            </Subtitle>
            <InfoContainer>
              <TextBox>
                <TextInfo>
                  <I18n>
                  type
                  </I18n>
                </TextInfo>
                <Text>
                  {machine.machineType.description}
                </Text>
              </TextBox>
              <TextBox>
                <TextInfo>
                  <I18n>
                  model
                  </I18n>
                </TextInfo>
                <Text>
                  {machine.model}
                </Text>
              </TextBox>
              <TextBox>
                <TextInfo>
                  <I18n>
                  brand
                  </I18n>
                </TextInfo>
                <Text>
                  {machine.machineBrand.description}
                </Text>
              </TextBox>
              <TextBox>
                <TextInfo>
                  <I18n params={ {
                    howMany: 1
                  } }
                  >
                  year
                  </I18n>
                </TextInfo>
                <Text>
                  {machine.year}
                </Text>
              </TextBox>
              <TextBox>
                <TextInfo>
                  <I18n>
                  hours
                  </I18n>
                </TextInfo>
                <Text>
                  {machine.availableHours}
                </Text>
              </TextBox>
              <TextBox>
                <TextInfo>
                  <I18n>
                  contact
                  </I18n>
                </TextInfo>
                <Text>
                  {machine.contactInformation}
                </Text>
              </TextBox>
            </InfoContainer>
          </Half>
        </MidContainer>
      </WhiteContainer>
    </FirstContainer>
  )
}

MachineDetailsFragment.propTypes = {
  machine: PropTypes.object.isRequired,
  isUserMachine: PropTypes.bool,
  onDeleteMachine: PropTypes.func
}

MachineDetailsFragment.defaultProps = {
  isUserMachine: false,
  onDeleteMachine: () => {}
}

export default MachineDetailsFragment
