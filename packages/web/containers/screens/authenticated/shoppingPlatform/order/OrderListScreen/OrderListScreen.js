import React, { useCallback, useState, useMemo } from 'react'
import { useSelector } from 'react-redux'
import { useHistory } from 'react-router-dom'

import moment from 'moment/moment'

import { isEmpty } from 'lodash'
import map from 'lodash/map'
import size from 'lodash/size'

import { useDialog } from '@smartcoop/dialog'
import I18n, { useT } from '@smartcoop/i18n'
import { filter, hammer, organizationRounded } from '@smartcoop/icons'
import {
  getOrders as getOrdersService,
  getMyOrdersByOrganization as getMyOrdersByOrganizationService,
  getOrdersByOrganization as getOrdersByOrganizationService
} from '@smartcoop/services/apis/smartcoopApi/resources/order'
import { selectUserHaveAdministration } from '@smartcoop/stores/authentication/selectorAuthentication'
import { selectCurrentOrganization } from '@smartcoop/stores/organization/selectorOrganization'
import colors from '@smartcoop/styles/colors'
import {
  momentFriendlyDateFormat,
  momentBackDateFormat
} from '@smartcoop/utils/dates'
import Avatar from '@smartcoop/web-components/Avatar'
import AvatarGroup from '@smartcoop/web-components/AvatarGroup'
import Button from '@smartcoop/web-components/Button'
import DataTable from '@smartcoop/web-components/DataTable'
import DemandStatus from '@smartcoop/web-components/DemandStatus'
import Icon from '@smartcoop/web-components/Icon'
import useTerm from '@smartcoop/web-containers/hooks/useTerm'
import FilterOrderModal from '@smartcoop/web-containers/modals/shoppingPlatform/FilterOrderModal'
import { useRoutes } from '@smartcoop/web-containers/routes/authenticated'

import useStyles, {
  Container,
  Row,
  Title,
  ButtonContainer,
  SingleButtonContainer,
  Text
} from './styles'

const OrderListScreen = () => {
  const { createDialog } = useDialog()
  const currentOrganization = useSelector(selectCurrentOrganization)
  const [listAll, setListAll] = useState(true)
  const [filters, setFilters] = useState({})

  const history = useHistory()
  const { routes } = useRoutes()
  const t = useT()
  const userIsAdmin = useSelector(selectUserHaveAdministration)
  useTerm('organization-term')

  const classes = useStyles()

  const redirectCreateOrder = useCallback(() => {
    history.push(routes.registerOrder.path)
  }, [history, routes.registerOrder])

  const redirectBySlug = useCallback(
    (slug, proposalDeadline) => {
      switch (slug) {
        case 'aguardando_decisao':
          return routes.orderDetails
        case 'aguardando_resposta':
          if (userIsAdmin && moment(proposalDeadline).isBefore(moment()))
            return routes.acceptOrder
          return routes.orderDetails
        default:
          return routes.orderDetails
      }
    },
    [routes, userIsAdmin]
  )

  const redirectToOrder = useCallback(
    (data) => {
      const {
        id,
        status: { slug },
        proposalDeadline
      } = data
      const route = redirectBySlug(slug, proposalDeadline)
      history.push(route.path.replace(':orderId', id))
    },
    [history, redirectBySlug]
  )

  const columns = useMemo(
    () => [
      {
        title: 'Código',
        field: 'code',
        align: 'center'
      },
      {
        title: 'Produtos',
        field: 'shortDescription',
        cellStyle: { fontWeight: 'bold' },
        sorting: true,
        render: (row) => row.product.shortDescription
      },
      {
        title: 'Abertura',
        field: 'startDate',
        type: 'date',
        sorting: true,
        defaultSort: 'desc',
        render: (row) => moment(row.createdAt).format(momentFriendlyDateFormat)
      },
      {
        title: 'Expiração',
        field: 'participationDeadline',
        type: 'date',
        sorting: true,
        render: (row) =>
          moment(row.participationDeadline, momentBackDateFormat).format(
            momentFriendlyDateFormat
          )
      },
      {
        title: 'Volume Agregado',
        field: 'aggregatedVolume',
        align: 'center',
        headerStyle: { whiteSpace: 'nowrap' },
        cellStyle: { paddingRight: 44 },
        render: (row) =>
          `${ Number(row.aggregatedVolume).toLocaleString('pt-BR') } ${
            row.product.unitOfMeasures ?? ''
          }`
      },
      {
        title: 'Cooperativas',
        field: 'organizationPurchases',
        cellStyle: { fontWeight: 'bold' },
        render: (row) => (
          <Row style={ { flexWrap: 'nowrap' } }>
            <Text style={ { paddingRight: 6 } }>
              {size(row.organizationPurchases)}
            </Text>
            <AvatarGroup max={ 99 }>
              {map(row.organizationPurchases, ({ organization }, index) => (
                <Avatar
                  key={ index }
                  alt={ organization.tradeName || organization.companyName }
                  tooltipProps={ {
                    title: organization.tradeName || organization.companyName,
                    arrow: true,
                    placement: 'top'
                  } }
                  className={ classes.white }
                >
                  <Icon
                    icon={ organizationRounded }
                    color={
                      organization.id === currentOrganization.id
                        ? colors.green
                        : colors.mediumGrey
                    }
                    size={ 25 }
                  />
                </Avatar>
              ))}
            </AvatarGroup>
          </Row>
        )
      },
      {
        title: 'Propostas',
        field: 'purchaseProposals',
        render: (row) => (
          <div style={ { display: 'flex', flexDirection: 'row' } }>
            <Icon
              icon={ hammer }
              size={ 16 }
              color={ colors.secondary }
              style={ { paddingRight: '8px' } }
            />
            <Text>{`${ row.purchaseProposals.length } ${ t('received', {
              gender: 'female',
              howMany: row.purchaseProposals.length
            }) }`}</Text>
          </div>
        )
      },
      {
        title: 'Situação',
        field: 'statusId',
        sorting: true,
        render: (row) => (
          <div style={ { display: 'flex' } }>
            <DemandStatus
              style={ { padding: '6px 10px' } }
              slug={ row.status.slug }
            />
          </div>
        )
      }
    ],
    [classes.white, currentOrganization.id, t]
  )

  const handleFilter = useCallback((values) => setFilters(values), [])

  const filterData = useCallback(() => {
    createDialog({
      id: 'filter-order',
      Component: FilterOrderModal,
      props: {
        onSubmit: handleFilter,
        filters
      }
    })
  }, [createDialog, filters, handleFilter])

  const onRowDisabled = useCallback(
    ({ status: { slug }, purchaseProposals }) =>
      (slug === 'aguardando_resposta' && size(purchaseProposals) === 0) ||
      slug === 'cancelada',
    []
  )

  const onRowDisabledAfterParticipationDeadLine = useCallback(
    (data) =>
      moment().isAfter(moment(data.receiptDeadline)) &&
      !data.organizationPurchases.includes(currentOrganization.id),
    [currentOrganization.id]
  )

  const onRowClick = useCallback(
    (event, data) => {
      if (
        onRowDisabledAfterParticipationDeadLine(data) ||
        !onRowDisabled(data)
      ) {
        redirectToOrder(data)
      }
    },
    [onRowDisabled, onRowDisabledAfterParticipationDeadLine, redirectToOrder]
  )

  const serviceUsers = useMemo(
    () =>
      listAll
        ? getOrdersByOrganizationService
        : getMyOrdersByOrganizationService,
    [listAll]
  )

  const service = useMemo(
    () => (userIsAdmin ? getOrdersService : serviceUsers),
    [serviceUsers, userIsAdmin]
  )

  const urlParams = useMemo(
    () => ({ organizationId: currentOrganization.id }),
    [currentOrganization]
  )

  return (
    <Container>
      <Title>
        <I18n>purchase orders</I18n>
      </Title>

      <Row>
        <ButtonContainer>
          <Button
            id="all-orders"
            onClick={ () => setListAll(true) }
            color={ listAll ? colors.black : colors.white }
            style={ { flex: 'none', whiteSpace: 'nowrap', marginRight: 12 } }
          >
            <I18n params={ { gender: 'female' } }>all</I18n>
          </Button>

          <Button
            id="my-orders"
            onClick={ () => setListAll(false) }
            color={ listAll ? colors.white : colors.black }
            style={ { flex: 'none', whiteSpace: 'nowrap', marginRight: 20 } }
          >
            <I18n>my orders</I18n>
          </Button>

          <Button
            id="filter"
            onClick={ filterData }
            style={ { flex: 'none', whiteSpace: 'nowrap' } }
            color={ isEmpty(filters) ? 'white' : 'secondary' }
          >
            <>
              <Icon style={ { paddingRight: 6 } } icon={ filter } size={ 14 } />
              <I18n>filtrate</I18n>
            </>
          </Button>
        </ButtonContainer>

        <SingleButtonContainer>
          <Button
            id="new-order"
            onClick={ redirectCreateOrder }
            color="secondary"
            style={ { flex: 'none', whiteSpace: 'nowrap' } }
          >
            <I18n>new demand</I18n>
          </Button>
        </SingleButtonContainer>
      </Row>

      {!isEmpty(urlParams.organizationId) && (
        <DataTable
          onRowClick={ onRowClick }
          data={ service }
          queryParams={ filters }
          urlParams={ urlParams }
          columns={ columns }
          disabledRow={ onRowDisabled }
        />
      )}
    </Container>
  )
}

export default OrderListScreen
