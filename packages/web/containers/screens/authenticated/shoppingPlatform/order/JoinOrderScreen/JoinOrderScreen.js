import React, { useCallback, useEffect, useRef, useMemo } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { useParams, useHistory, useLocation } from 'react-router-dom'

import { useT } from '@smartcoop/i18n'
import { OrderActions } from '@smartcoop/stores/order'
import { selectCurrentOrder } from '@smartcoop/stores/order/selectorOrder'
import { selectCurrentOrganization } from '@smartcoop/stores/organization/selectorOrganization'
import JoinOrderForm from '@smartcoop/web-containers/forms/shoppingPlatform/order/JoinOrderForm'
import OrderDetailsFragment from '@smartcoop/web-containers/fragments/OrderDetailsFragment'
import SplitedScreenLayout from '@smartcoop/web-containers/layouts/SplitedScreenLayout'
import { useRoutes } from '@smartcoop/web-containers/routes/authenticated'

import { Container } from './styles'

const JoinOrderScreen = () => {
  const history = useHistory()
  const location = useLocation()
  const { orderId } = useParams()
  const { routes } = useRoutes()

  const t = useT()
  const dispatch = useCallback(useDispatch(), [])

  const joinOrderRef = useRef(null)

  const currentOrder = useSelector(selectCurrentOrder)
  const currentOrganization = useSelector(selectCurrentOrganization)
  const isEditing = useMemo(() => location.state?.isEditing || false, [
    location.state
  ])

  const onSuccess = useCallback(
    () => {
      history.replace(routes.orderDetails.path.replace(':orderId', currentOrder.id))
    },
    [currentOrder.id, history, routes.orderDetails.path])

  useEffect(() => {
    dispatch(
      OrderActions.loadCurrentOrder(
        orderId,
        () => {},
        () => history.replace(routes.orderList.path)
      )
    )
  }, [
    dispatch,
    orderId,
    currentOrganization,
    history,
    routes.orderDetails.path,
    routes.orderList.path
  ])

  useEffect(
    () => () => {
      dispatch(OrderActions.resetCurrentOrder())
    },
    [dispatch]
  )

  useEffect(
    () => () => {
      dispatch(OrderActions.resetCurrentOrder())
    },
    [dispatch]
  )

  return (
    <SplitedScreenLayout
      title={ { name: t('order', { howMany: 2 }) } }
      divRightStyle={ { paddingTop: 67 } }
      leftChildren={ <OrderDetailsFragment /> }
      rightChildren={
        <Container>
          <JoinOrderForm
            ref={ joinOrderRef }
            onSuccess={ onSuccess }
            isEditing={ isEditing }
          />
        </Container>
      }
    />
  )
}

export default JoinOrderScreen
