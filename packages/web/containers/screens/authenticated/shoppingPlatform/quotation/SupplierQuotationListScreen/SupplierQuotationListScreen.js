import React, { useEffect, useCallback, useState, useMemo } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useHistory } from 'react-router-dom'

import moment from 'moment/moment'

import debounce from 'lodash/debounce'
import isEmpty from 'lodash/isEmpty'

import { ThemeProvider } from '@material-ui/core'

import { useDialog } from '@smartcoop/dialog'
import I18n, { useT } from '@smartcoop/i18n'
import { filter } from '@smartcoop/icons'
import { getOrdersBySupplier as getOrdersBySupplierService } from '@smartcoop/services/apis/smartcoopApi/resources/order'
import { AuthenticationActions } from '@smartcoop/stores/authentication'
import { selectCurrentOrganization } from '@smartcoop/stores/organization/selectorOrganization'
import { colors } from '@smartcoop/styles'
import {
  momentFriendlyDateFormat,
  momentBackDateFormat
} from '@smartcoop/utils/dates'
import Button from '@smartcoop/web-components/Button'
import DataTable from '@smartcoop/web-components/DataTable'
import Icon from '@smartcoop/web-components/Icon'
import InputSearch from '@smartcoop/web-components/InputSearch'
import SupplierQuotationStatus from '@smartcoop/web-components/SupplierQuotationStatus'
import useTerm from '@smartcoop/web-containers/hooks/useTerm'
import FilterSupplierModal from '@smartcoop/web-containers/modals/shoppingPlatform/FilterSupplierModal'
import { useRoutes } from '@smartcoop/web-containers/routes/authenticated'

import {
  Container,
  Row,
  Title,
  ButtonContainer,
  SingleItemContainer,
  inputSearchTheme
} from './styles'

const QuotationListScreen = () => {
  const { createDialog } = useDialog()
  const history = useHistory()
  const { routes } = useRoutes()
  const t = useT()
  const dispatch = useCallback(useDispatch(), [])
  useTerm('supplier-term')

  const [filters, setFilters] = useState({})
  const [filterStatus, setFilterStatus] = useState({})
  const [filterText, setFilterText] = useState('')
  const [debouncedFilterText, setDebouncedFilterText] = useState('')
  const currentOrganization = useSelector(selectCurrentOrganization)

  const redirectToQuotationDetail = useCallback(
    (data) => {
      const path = routes.supplierQuotationDetails.path.replace(
        ':supplierQuotationId',
        data.id
      )
      history.push(path, { order: { ...data } })
    },
    [history, routes.supplierQuotationDetails.path]
  )

  const columns = useMemo(
    () => [
      {
        title: 'Produtos',
        field: 'product.description',
        align: 'center',
        cellStyle: { fontWeight: 'bold' }
      },
      {
        title: 'Cód. Demanda',
        field: 'code',
        align: 'center',
        headerStyle: { whiteSpace: 'nowrap' },
        sorting: true,
        defaultSort: 'desc'
      },
      {
        title: 'Cód. Proposta',
        align: 'center',
        headerStyle: { whiteSpace: 'nowrap' },
        render: (row) => {
          const { purchaseProposals } = row
          const supplierPurchase = purchaseProposals.filter(
            (item) => item.supplierId === currentOrganization.id
          )
          const { length } = supplierPurchase

          if (length > 0) {
            return supplierPurchase[0].code
          }
          return '-'
        }
      },
      {
        title: 'Limite para Proposta',
        field: 'proposalDeadline',
        align: 'center',
        headerStyle: { whiteSpace: 'nowrap' },
        render: (row) =>
          row.proposalDeadline
            ? moment(row.proposalDeadline, momentBackDateFormat).format(
              momentFriendlyDateFormat
            )
            : '-',
        sorting: true
      },
      {
        title: 'Limite para Entrega',
        field: 'receiptDeadline',
        align: 'center',
        headerStyle: { whiteSpace: 'nowrap' },
        render: (row) =>
          row.receiptDeadline
            ? moment(row.receiptDeadline, momentBackDateFormat).format(
              momentFriendlyDateFormat
            )
            : '-',
        sorting: true
      },
      {
        title: 'Volume agregado',
        field: 'aggregatedVolume',
        align: 'center',
        headerStyle: { whiteSpace: 'nowrap' },
        render: (row) =>
          `${ Number(row.aggregatedVolume).toLocaleString('pt-BR') } ${
            row.product.unitOfMeasures
          }`
      },
      {
        title: 'Situação',
        field: 'status.slug',
        render: (row) => {
          const proposalDate = moment(row.proposalDeadline)
          const hasProposal = row.purchaseProposals.some(
            (value) => value.supplierId === currentOrganization.id
          )
          let proposal = null
          let slug

          if (hasProposal) {
            proposal = row.purchaseProposals.find(
              (element) => element.supplierId === currentOrganization.id
            )
          }

          if (proposalDate.isBefore(moment()) && !hasProposal) {
            slug = 'expirado'
          } else if (hasProposal) {
            slug = proposal.status.slug
          } else {
            slug = row.status.slug
          }
          return (
            <div style={ { display: 'flex' } }>
              <SupplierQuotationStatus
                style={ { padding: '6px 10px' } }
                slug={ slug }
              />
            </div>
          )
        }
      }
    ],
    [currentOrganization.id]
  )

  const debouncedChangeSearchFilter = useCallback(
    debounce((value) => {
      setDebouncedFilterText(value)
    }, 300),
    []
  )

  const onChangeSearchFilter = useCallback(
    (e) => {
      setFilterText(e.target.value)
      debouncedChangeSearchFilter(e.target.value)
    },
    [debouncedChangeSearchFilter]
  )

  const handleFilter = useCallback((values) => setFilters(values), [])

  const filterData = useCallback(() => {
    createDialog({
      id: 'filter-supplier',
      Component: FilterSupplierModal,
      props: {
        onSubmit: handleFilter,
        filters
      }
    })
  }, [createDialog, filters, handleFilter])

  const onRowDisabled = useCallback(
    (row) => {
      const proposalDate = moment(row.proposalDeadline)
      const hasProposal = row.purchaseProposals.some(
        (value) => value.supplierId === currentOrganization.id
      )
      let slug

      if (proposalDate.isBefore(moment(), 'second') && !hasProposal) {
        slug = 'expirado'
      } else {
        slug = row.status.slug
      }

      return slug === 'expirado'
    },
    [currentOrganization.id]
  )

  const onRowClick = useCallback(
    (event, data) => {
      if (!onRowDisabled(data)) {
        redirectToQuotationDetail(data)
      }
    },
    [onRowDisabled, redirectToQuotationDetail]
  )

  const tableFilters = useMemo(
    () =>
      debouncedFilterText || filterStatus
        ? { ...filters, ...filterStatus, q: debouncedFilterText }
        : { ...filters },
    [debouncedFilterText, filterStatus, filters]
  )

  const urlParams = useMemo(() => ({ supplierId: currentOrganization.id }), [
    currentOrganization.id
  ])

  useEffect(() => {
    dispatch(AuthenticationActions.loadPermissions())
  }, [dispatch])

  return (
    <Container>
      <Title>
        <I18n>purchase quotes</I18n>
      </Title>
      <Row>
        <ButtonContainer>
          <Button
            id="all-quotes"
            onClick={ () => setFilterStatus({}) }
            color={
              filterStatus.statusId !== '1' && filterStatus.statusId !== '4'
                ? colors.black
                : colors.white
            }
            style={ { flex: 'none', whiteSpace: 'nowrap', marginRight: 10 } }
          >
            <I18n params={ { gender: 'female' } }>all</I18n>
          </Button>

          <Button
            id="open"
            onClick={ () =>
              setFilterStatus({
                statusId: '4',
                minProposalDate: moment().format('YYYY-MM-DD')
              })
            }
            color={ filterStatus.statusId === '4' ? colors.black : colors.white }
            style={ { flex: 'none', whiteSpace: 'nowrap', marginRight: 10 } }
          >
            <I18n params={ { gender: 'female' } }>open</I18n>
          </Button>

          <Button
            id="closed"
            onClick={ () => setFilterStatus({ statusId: '2' }) }
            color={ filterStatus.statusId === '2' ? colors.black : colors.white }
            style={ { flex: 'none', whiteSpace: 'nowrap', marginRight: 10 } }
          >
            <I18n params={ { gender: 'female' } }>closed</I18n>
          </Button>
        </ButtonContainer>

        <SingleItemContainer>
          <ThemeProvider theme={ inputSearchTheme }>
            <InputSearch
              detached
              onChange={ onChangeSearchFilter }
              value={ filterText }
              placeholder={ t('search') }
              fullWidth
              style={ { marginRight: 12 } }
            />
          </ThemeProvider>
          <Button
            id="filter"
            onClick={ filterData }
            color={ colors.white }
            style={ { flex: 'none', whiteSpace: 'nowrap' } }
          >
            <>
              <Icon style={ { paddingRight: 6 } } icon={ filter } size={ 14 } />
              <I18n>filtrate</I18n>
            </>
          </Button>
        </SingleItemContainer>
      </Row>

      {!isEmpty(currentOrganization) && (
        <DataTable
          onRowClick={ onRowClick }
          data={ getOrdersBySupplierService }
          urlParams={ urlParams }
          queryParams={ tableFilters }
          columns={ columns }
          disabledRow={ onRowDisabled }
        />
      )}
    </Container>
  )
}

export default QuotationListScreen
