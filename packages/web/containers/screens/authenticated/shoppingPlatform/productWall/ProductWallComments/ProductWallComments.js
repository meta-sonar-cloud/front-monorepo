import React, { useEffect, useCallback,useState } from 'react'
import { useDispatch } from 'react-redux'
import { useLocation, useParams } from 'react-router-dom'

import Backdrop from '@material-ui/core/Backdrop'

import { useDialog } from '@smartcoop/dialog'
import { useT } from '@smartcoop/i18n'
import { ProductWallActions } from '@smartcoop/stores/productWall'
import { colors } from '@smartcoop/styles'
import Button from '@smartcoop/web-components/Button'
import { CircularLoader } from '@smartcoop/web-components/Loader'
import ProductWallFeed from '@smartcoop/web-components/ProductWallFeed'
import ProductWallComment from '@smartcoop/web-containers/fragments/ProductWallComment'
import SplitedScreenLayout from '@smartcoop/web-containers/layouts/SplitedScreenLayout'
import CommentModal from '@smartcoop/web-containers/modals/shoppingPlatform/ProductWall/CommentModal'

import useStyles, { CommentDiv, LabelComments, FlexDiv } from './styles'


const INITIAL_FEED = {
  organization: {
    companyName: '',
    id: '',
    tradeName: ''
  },
  id: '',
  text: '',
  userId: '',
  createdAt: '',
  updatedAt: '',
  totalComments: 0,
  user: {
    id: '',
    name: '',
    email: '',
    city: '',
    state: '',
    image: ''
  }
}

const ProductWallComments = () => {
  const t = useT()
  const params = useParams()
  const classes = useStyles()
  const location = useLocation()
  const { createDialog } = useDialog()
  const dispatch = useCallback(useDispatch(), [])

  const [comments, setComments] = useState([])
  const [loading, setLoading] = useState(true)
  const [originalPost, setOriginalPost] = useState(INITIAL_FEED)


  const { state: { feed } } = location
  const { postId } = params

  useEffect( () => {
    setOriginalPost(feed)
  }, [feed])

  useEffect(() => {
    window.scrollTo(0,0) }, [])

  const dispatchError = () => {
    setLoading(false)
  }

  const loadCommentSuccess = (result) => {
    const { data } = result
    setComments([...data].reverse())
    setOriginalPost(state => ({ ...state, totalComments: result.totalRecords }))
    setLoading(false)
  }

  const getComments = useCallback( () => {
    setLoading(true)
    dispatch(ProductWallActions.loadProductWallComments({ postId }, loadCommentSuccess, dispatchError))
  }, [postId, dispatch])

  useEffect(() => {
    getComments()
  },[getComments])

  const createComment = useCallback(() => {
    createDialog({
      id: 'create-comment',
      Component: CommentModal,
      props: {
        originalPost: feed,
        getComments
      }
    })
  }, [createDialog, feed, getComments])

  return (
    <SplitedScreenLayout
      withGoBackRight
      withoutDivider
      title={ { name: t('product wall') } }
      rightGridStyle={ {
        backgroundColor: colors.backgroundHtml,
        maxWidth: '100%',
        flexBasis: '100%'
      } }
      divRightStyle={ {
        backgroundColor: colors.backgroundHtml,
        width: '100%',
        height: 'auto',
        padding: 0
      } }
      rightChildrenStyle={ { justifyContent: 'center' } }
      rightChildren={ (
        <CommentDiv>
          <ProductWallFeed feed={ originalPost }/>
          <FlexDiv>
            <div><LabelComments>{t('social comments')}</LabelComments></div>
            <div><Button id='button' color='secondary' onClick={ createComment }>{t('social comment')}</Button></div>
          </FlexDiv>
          {comments.map((item)=> <ProductWallComment key={ `comment-${ item.id }` } comment={ item } originalPost={ feed } getComments={ getComments } />)}
          <Backdrop open={ loading } className={ classes.backdrop }>
            <CircularLoader />
          </Backdrop>
        </CommentDiv>
      ) }
    />
  )}

export default ProductWallComments
