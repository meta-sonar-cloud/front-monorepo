import React, { useState, useCallback, useEffect, useRef } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import debounce from 'lodash/debounce'
import map from 'lodash/map'

import { ThemeProvider } from '@material-ui/core'

import { useT } from '@smartcoop/i18n'
import { useSnackbar } from '@smartcoop/snackbar'
import { SocialActions } from '@smartcoop/stores/social'
import { selectPosts } from '@smartcoop/stores/social/selectorSocial'
import { selectUser } from '@smartcoop/stores/user/selectorUser'
import { colors } from '@smartcoop/styles'
import Feed from '@smartcoop/web-components/Feed/Feed'
import InputSearch from '@smartcoop/web-components/InputSearch/InputSearch'
import Post from '@smartcoop/web-components/Post'
import useTerm from '@smartcoop/web-containers/hooks/useTerm'
import { useLayout } from '@smartcoop/web-containers/layouts/AuthenticatedLayout'
import SplitedScreenLayout from '@smartcoop/web-containers/layouts/SplitedScreenLayout'

import { Container, inputSearchTheme, LabelFeed } from './styles'

const SocialScreen = () => {
  const divRef = useRef(null)
  useTerm('social-network-term')

  const loading = useRef(false)
  const t = useT()
  const snackbar = useSnackbar()
  const dispatch = useCallback(useDispatch(), [])

  const { mainRef } = useLayout()

  const [filterText, setFilterText] = useState('')
  const [nextPage, setNextPage] = useState(1)

  const posts = useSelector(selectPosts)
  const user = useSelector(selectUser)

  const loadPosts = useCallback(
    (page, next = nextPage) => {
      if (page && page <= next) {
        loading.current = true
        dispatch(SocialActions.loadPosts(
          { page },
          (paginationInfo) => {
            if (paginationInfo) {
              if (paginationInfo.page >= paginationInfo.totalPages) {
                setNextPage(0)
              } else {
                setNextPage(paginationInfo.page + 1)
              }
            } else {
              setNextPage(0)
            }
            loading.current = false
          },
          () => {
            loading.current = false
          }
        ))
      }
    },
    [dispatch, nextPage]
  )

  const onRefresh = useCallback(
    () => {
      setNextPage(1)
      loadPosts(1, 1)
    },
    [loadPosts]
  )

  const handleChangeSearchFilter = useCallback(
    (event) => {
      setFilterText(event.target.value)
    },
    []
  )

  const handleSubmitPost = useCallback(
    (data) => {
      dispatch(SocialActions.saveOfflinePost(
        data,
        () => {
          snackbar.success(t('your {this} was registered', {
            howMany: 1,
            gender: 'female',
            this: t('post', { howMany: 1 })
          }))
          onRefresh()
        }
      ))
    },
    [dispatch, onRefresh, snackbar, t]
  )

  // infinity scroll => faz o request para pedir novas paginas
  // quando o usuário chega ao final da listagem já carregada
  const infinityScroll = useCallback(
    debounce(() => {
      if (divRef.current && nextPage && !loading.current) {
        const {
          bottom
        } = divRef.current.getBoundingClientRect()
        const distanceOfBottom = bottom - window.innerHeight

        if (distanceOfBottom < 10) {
          loadPosts(nextPage)
        }
      }
    }, 100),
    [
      divRef,
      loading,
      nextPage
    ]
  )

  useEffect(() => {
    const ref = mainRef.current
    ref.addEventListener('scroll', infinityScroll)
    return () => ref.removeEventListener('scroll', infinityScroll)
  }, [infinityScroll, mainRef])


  useEffect(() => {
    onRefresh()
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  return (
    <SplitedScreenLayout
      withoutGoBack
      withoutDivider
      withoutTitle
      divRightStyle={ {
        backgroundColor: colors.white,
        width: '100%',
        height: '100%',
        padding: 0
      } }
      rightChildren={ (
        <Container ref={ divRef }>
          <ThemeProvider theme={ inputSearchTheme }>
            <InputSearch
              detached
              onChange={ handleChangeSearchFilter }
              value={ filterText }
              placeholder={ t('search') }
              fullWidth
              disabled
            />
          </ThemeProvider>

          <Post
            user={ user }
            onSubmit={ handleSubmitPost }
          />

          <LabelFeed>Feed</LabelFeed>

          <div ref={ divRef }>
            {map(posts, post =>
              <Feed key={ post.id } feed={ post } commentClick/>
            )}
          </div>
        </Container>
      ) }
      appendChildren={
        <></>
      }
    />
  )}

export default SocialScreen
