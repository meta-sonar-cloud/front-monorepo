import React, { memo, useMemo, useEffect, useCallback } from 'react'
import { useSelector , useDispatch } from 'react-redux'

import isEmpty from 'lodash/isEmpty'

import { AVAILABLE_MODULES } from '@smartcoop/stores/module'
import { selectCurrentModule } from '@smartcoop/stores/module/selectorModule'
import { UserActions } from '@smartcoop/stores/user'
import { useRoutes } from '@smartcoop/web-containers/routes/authenticated'

import RouterSwitch from '../RouterSwitch'
import AdministrationScreenRouter from './administration/AdministrationScreenRouter'
import CommercializationScreenRouter from './commercialization/CommercializationScreenRouter'
import DigitalPropertyScreenRouter from './digitalProperty/DigitalPropertyScreenRouter'
import ShoppingPlatformScreenRouter from './shoppingPlatform/ShoppingPlatformScreenRouter'
import TechnicalScreenRouter from './technical/TechnicalScreenRouter'

const AuthenticatedScreenRouter = (props) => {
  const { routes } = useRoutes()
  const currentModule = useSelector(selectCurrentModule)
  const dispatch = useCallback(useDispatch(), [])

  useEffect(() => {
    dispatch(UserActions.loadUser())
  }, [dispatch])

  const Router = useMemo(
    () => {
      switch (currentModule) {
        case AVAILABLE_MODULES.digitalProperty:
          return DigitalPropertyScreenRouter
        case AVAILABLE_MODULES.commercialization:
          return CommercializationScreenRouter
        case AVAILABLE_MODULES.shoppingPlatform:
          return ShoppingPlatformScreenRouter
        case AVAILABLE_MODULES.administration:
          return AdministrationScreenRouter
        case AVAILABLE_MODULES.technical:
          return TechnicalScreenRouter
        default:
          return (p) => <RouterSwitch routes={ routes } { ...p } />
      }
    },
    [currentModule, routes]
  )

  return !isEmpty(routes) && <Router { ...props } />
}

export default memo(AuthenticatedScreenRouter)
