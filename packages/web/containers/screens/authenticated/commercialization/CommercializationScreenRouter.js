import React, { memo, useEffect, useCallback } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import { isEmpty } from 'lodash'

import { AuthenticationActions } from '@smartcoop/stores/authentication'
import { selectCurrentOrganization } from '@smartcoop/stores/organization/selectorOrganization'
import { withLayout } from '@smartcoop/web-containers/layouts'
import { useRoutes } from '@smartcoop/web-containers/routes/authenticated'

import RouterSwitch from '../../RouterSwitch'

const CommercializationScreenRouter = (props) => {
  const { routes } = useRoutes()
  const dispatch = useCallback(useDispatch(), [])
  const currentOrganization = useSelector(selectCurrentOrganization)

  useEffect(() => {
    if (!isEmpty(currentOrganization)) {
      dispatch(AuthenticationActions.loadPermissions())
    }
  }, [currentOrganization, dispatch])

  return <RouterSwitch routes={ routes } { ...props } />
}

export default memo(withLayout('authenticated')(CommercializationScreenRouter))
