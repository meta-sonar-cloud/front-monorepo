import React, { useState, useMemo, useCallback, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import moment from 'moment/moment'

import { isEmpty } from 'lodash'

import I18n, { useT } from '@smartcoop/i18n'
import { getAccountSercuritiesMovement } from '@smartcoop/services/apis/smartcoopApi/resources/securitiesMovement'
import { AccountActions } from '@smartcoop/stores/account'
import { selectCurrentAccount } from '@smartcoop/stores/account/selectorAccount'
import { selectCurrentOrganization } from '@smartcoop/stores/organization/selectorOrganization'
import { SecuritiesMovementActions } from '@smartcoop/stores/securitiesMovement'
import { colors } from '@smartcoop/styles'
import { momentBackDateTimeFormat } from '@smartcoop/utils/dates'
import Button from '@smartcoop/web-components/Button'
import DateTabs from '@smartcoop/web-components/DateTabs'
import AccountBalanceListFragment from '@smartcoop/web-containers/fragments/commercialization/AccountBalanceListFragment'
import SecuritiesMovementListFragment from '@smartcoop/web-containers/fragments/commercialization/SecuritiesMovementListFragment'
import SplitedScreenLayout from '@smartcoop/web-containers/layouts/SplitedScreenLayout'

import {
  Container,
  Row,
  ButtonContainer,
  AccountBalanceContainer
} from './styles'

const SecuritiesMovementListScreen = () => {
  const t = useT()
  const dispatch = useCallback(useDispatch(), [])

  const currentAccount = useSelector(selectCurrentAccount)
  const currentOrganization = useSelector(selectCurrentOrganization)

  const [mounted, setMounted] = useState(false)
  const [listType, setListType] = useState('receivable')
  const [initialDate, setInitialDate] = useState()
  const [filterDate, setFilterDate] = useState()

  const urlParams = useMemo(
    () => ({
      accountId: currentAccount.id,
      organizationId: currentOrganization.id
    }),
    [currentAccount.id, currentOrganization.id]
  )

  const queryParams = useMemo(
    () => ({ type: listType, transactionDate: filterDate }),
    [filterDate, listType]
  )

  useEffect(
    () => () => {
      dispatch(AccountActions.resetCurrentAccount())
    },
    [dispatch]
  )

  useEffect(() => {
    dispatch(
      SecuritiesMovementActions.loadSecuritieMovementInitialDate({}, (value) =>
        setInitialDate(value)
      )
    )
  }, [dispatch, mounted, listType, currentAccount, currentOrganization])

  useEffect(() => {
    if (!mounted) {
      setMounted(true)
      dispatch(
        SecuritiesMovementActions.loadSecuritieMovementInitialDate(
          {},
          (value) => setInitialDate(value)
        )
      )
    }
  }, [dispatch, mounted])

  return (
    <SplitedScreenLayout
      title={ {
        name: t('home')
      } }
      divLeftStyle={ {
        borderRight: `1px solid ${ colors.lightGrey }`
      } }
      leftChildren={
        <AccountBalanceContainer>
          <AccountBalanceListFragment withoutHeader clickable withCreditLimit />
        </AccountBalanceContainer>
      }
      divRightStyle={ {
        backgroundColor: colors.white,
        width: '100%',
        height: '100%',
        padding: 0
      } }
      rightChildren={
        <Container>
          <Row>
            <ButtonContainer>
              <Button
                id="receive"
                onClick={ () => setListType('receivable') }
                variant="text"
                style={ {
                  fontSize: 14,
                  flex: 1,
                  fontWeight: 'bold',
                  backgroundColor: 'transparent',
                  borderBottom: `${ listType === 'receivable' ? 4 : 1 }px solid`,
                  borderColor: `${
                    listType === 'receivable' ? colors.black : colors.lightGrey
                  }`,
                  color: `${
                    listType === 'receivable' ? colors.black : colors.grey
                  }`,
                  borderRadius: 0
                } }
              >
                <I18n>titles to receive</I18n>
              </Button>
              <Button
                id="to-pay"
                onClick={ () => setListType('payable') }
                variant="text"
                style={ {
                  fontSize: 14,
                  flex: 1,
                  fontWeight: 'bold',
                  backgroundColor: 'transparent',
                  borderBottom: `${ listType === 'payable' ? 4 : 1 }px solid`,
                  borderColor: `${
                    listType === 'payable' ? colors.black : colors.lightGrey
                  }`,
                  color: `${
                    listType === 'payable' ? colors.black : colors.grey
                  }`,
                  borderRadius: 0
                } }
              >
                <I18n>titles to pay</I18n>
              </Button>
            </ButtonContainer>
          </Row>
          {!isEmpty(initialDate) && (
            <DateTabs
              initialDate={ moment(initialDate, momentBackDateTimeFormat) }
              endDate={ moment() }
              onChange={ (value) => {
                setFilterDate(value.format('YYYY-MM'))
              } }
              currentYearFormat="MMM"
              style={ { padding: '10px 0 10px' } }
              defaultValue={ moment(initialDate, 'YYYY-MM-DD HH:mm:ss') }
              viewLastFirst
            />
          )}
          <SecuritiesMovementListFragment
            service={ getAccountSercuritiesMovement }
            urlParams={ urlParams }
            queryParams={ queryParams }
          />
        </Container>
      }
    />
  )
}

export default SecuritiesMovementListScreen
