import React, { memo, useEffect, useCallback } from 'react'
import { useDispatch } from 'react-redux'

import { TechnicalActions } from '@smartcoop/stores/technical'
import { withLayout } from '@smartcoop/web-containers/layouts'
import { useRoutes } from '@smartcoop/web-containers/routes/authenticated'


import RouterSwitch from '../../RouterSwitch'

const TecnhicalScreenRouter = (props) => {
  const dispatch = useCallback(useDispatch(), [])

  const { routes } = useRoutes()

  useEffect(() => () => {
    dispatch(TechnicalActions.resetTechnicalCurrentOwner())
  }, [dispatch])

  return <RouterSwitch routes={ routes } { ...props } />
}

export default memo(withLayout('authenticated')(TecnhicalScreenRouter))
