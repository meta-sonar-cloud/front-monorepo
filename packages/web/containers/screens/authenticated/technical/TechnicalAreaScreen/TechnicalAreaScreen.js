import React, { useEffect, useMemo, useState, useCallback } from 'react'

import { isEmpty } from 'lodash'

import { useDialog } from '@smartcoop/dialog'
import I18n from '@smartcoop/i18n'
import { filter } from '@smartcoop/icons'
import { colors } from '@smartcoop/styles'
import Button from '@smartcoop/web-components/Button'
import Icon from '@smartcoop/web-components/Icon'
import TechnicalPortfolio from '@smartcoop/web-containers/fragments/technical/TechnicalPortfolio'
import TechnicalVisitList from '@smartcoop/web-containers/fragments/technical/TechnicalVisitList'
import FilterTechnicalPortfolioModal from '@smartcoop/web-containers/modals/technical/FilterTechnicalPortfolioModal'
import FilterTechnicalVisitModal from '@smartcoop/web-containers/modals/technical/FilterTechnicalVisitModal'

import {
  Container,
  Row,
  ButtonContainer,
  TabButton
} from './styles'

const TechnicalAreaScreen = () => {
  const { createDialog } = useDialog()
  const [listType, setListType] = useState('producerPortfolio')
  const [filters, setFilters] = useState({})

  const handleFilter = useCallback(
    (values) => setFilters(values),
    []
  )

  const filterData = useCallback(
    () => {
      createDialog({
        id: 'filter-technical-visit',
        Component: listType === 'producerPortfolio' ? FilterTechnicalPortfolioModal : FilterTechnicalVisitModal,
        props: {
          onSubmit: handleFilter,
          filters
        }
      })
    },
    [createDialog, filters, handleFilter, listType]
  )

  const dataTableList = useMemo(
    () => ({
      'producerPortfolio': <TechnicalPortfolio filters={ filters } />,
      'technicalVisits': <TechnicalVisitList filters={ filters }/>
    }[listType]),
    [filters, listType]
  )

  useEffect(() => {
    setFilters({})
  }, [listType])

  return (
    <Container>
      <Row>
        <ButtonContainer>
          <div>
            <TabButton
              id="producer-portfolio"
              onClick={ () => setListType('producerPortfolio') }
              color={ listType === 'producerPortfolio' ? colors.black : colors.white }
            >
              <I18n>producer portfolio</I18n>
            </TabButton>
            <TabButton
              id="technical-visits"
              onClick={ () => setListType('technicalVisits') }
              color={ listType === 'technicalVisits' ? colors.black : colors.white }
            >
              <I18n>technical visit</I18n>
            </TabButton>
          </div>

          <Button
            id="filter"
            onClick={ filterData }
            color={ isEmpty(filters) ? 'white' : 'secondary' }
            style={ { flex: 'none', whiteSpace: 'nowrap' } }
          >
            <>
              <Icon style={ { paddingRight: 6 } } icon={ filter } size={ 14 }/>
              <I18n>filtrate</I18n>
            </>
          </Button>
        </ButtonContainer>
      </Row>
      <div>
        {dataTableList}
      </div>
    </Container>
  )
}

export default TechnicalAreaScreen
