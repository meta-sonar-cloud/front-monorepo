import React, { useEffect, useCallback } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import { OrganizationActions } from '@smartcoop/stores/organization'
import { selectUserOrganizations } from '@smartcoop/stores/organization/selectorOrganization'
import { selectUser } from '@smartcoop/stores/user/selectorUser'
import UserAdditionalInfoFragment from '@smartcoop/web-containers/fragments/profile/UserAdditionalInfoFragment'
import UserInfoFragment from '@smartcoop/web-containers/fragments/profile/UserInfoFragment'
import HorizontalSplitScreenLayout from '@smartcoop/web-containers/layouts/HorizontalSplitScreenLayout'

const EditProfileScreen = () => {
  const user = useSelector(selectUser)
  const userOrganizations = useSelector(selectUserOrganizations)
  const dispatch = useCallback(useDispatch(), [])


  useEffect(() => {
    dispatch(OrganizationActions.loadUserOrganizations())
  }, [dispatch])

  return (
    <HorizontalSplitScreenLayout
      upperChildren={
        <UserInfoFragment user={ user } />
      }
      underChildren={
        <UserAdditionalInfoFragment
          user={ user }
          userOrganizations={ userOrganizations }
        />
      }
    />
  )
}

export default EditProfileScreen
