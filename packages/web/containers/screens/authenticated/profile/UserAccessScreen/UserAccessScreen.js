import React, { useMemo, useState, useCallback } from 'react'

import debounce from 'lodash/debounce'

import { ThemeProvider } from '@material-ui/core'

import I18n, { useT } from '@smartcoop/i18n'
import { colors } from '@smartcoop/styles'
import InputSearch from '@smartcoop/web-components/InputSearch'
import UserAccessFragment from '@smartcoop/web-containers/fragments/profile/UserAccessFragment'


import {
  Container,
  Row,
  ButtonContainer,
  TabButton,
  inputSearchTheme
} from './styles'

const UserAccessScreen = () => {
  const [listType, setListType] = useState('technicalList')
  const [filterText, setFilterText] = useState('')
  const [debouncedFilterText, setDebouncedFilterText] = useState('')

  const t = useT()

  const dataTableList = useMemo(
    () => ({
      'technicalList': <UserAccessFragment filterText={ { q: debouncedFilterText } }/>
    }[listType]),
    [debouncedFilterText, listType]
  )

  const debouncedChangeSearchFilter = useCallback(
    debounce((value) => {
      setDebouncedFilterText(value)
    }, 300),
    []
  )

  const onChangeSearchFilter = useCallback(
    (e) => {
      setFilterText(e.target.value)
      debouncedChangeSearchFilter(e.target.value)
    },
    [debouncedChangeSearchFilter]
  )

  return (
    <Container>
      <Row>
        <ButtonContainer>
          <TabButton
            id="technical-list"
            onClick={ () => setListType('technicalList') }
            color={ listType === 'technicalList' ? colors.black : colors.white }
          >
            <I18n>technical</I18n>
          </TabButton>
          {/* <TabButton
            id="technical-visits"
            onClick={ () => setListType('technicalVisits') }
            color={ listType === 'technicalVisits' ? colors.black : colors.white }
          >
            <I18n>technical visit</I18n>
          </TabButton> */}
        </ButtonContainer>
        <div>
          <ThemeProvider theme={ inputSearchTheme }>
            <InputSearch
              detached
              onChange={ onChangeSearchFilter }
              value={ filterText }
              placeholder={ t('search') }
              fullWidth
              style={ { marginRight: 12 } }
            />
          </ThemeProvider>
        </div>
      </Row>
      <div>
        {dataTableList}
      </div>
    </Container>
  )
}

export default UserAccessScreen
