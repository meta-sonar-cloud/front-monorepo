import React, { useState, useEffect, useCallback } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import Grid from '@material-ui/core/Grid'

import I18n from '@smartcoop/i18n'
import { barn, currencyWithoutSpace, shoppingPlatform, smallPlant, dashboard } from '@smartcoop/icons'
import {
  selectUserHaveDigitalProperty,
  selectUserHaveCommercialization,
  selectUserHaveShoppingPlatform,
  selectModulesHaveTechnical,
  selectUserHaveAdministration
} from '@smartcoop/stores/authentication/selectorAuthentication'
import { ModuleActions, AVAILABLE_MODULES } from '@smartcoop/stores/module'
import pointsBottomLeft from '@smartcoop/styles/assets/images/points-bottom-left.svg'
import pointsTopRight from '@smartcoop/styles/assets/images/points-top-right.svg'
import logo from '@smartcoop/styles/assets/images/smartcoop-logo-inverted.svg'
import colors from '@smartcoop/styles/colors'
import Icon from '@smartcoop/web-components/Icon'

import useTerm from '../../../hooks/useTerm'
import useStyles, {
  Content,
  Title,
  ButtonCard,
  ButtonCardContent,
  ButtonCardTitle
} from './styles'

const ChooseModuleScreen = () => {
  const dispatch = useCallback(useDispatch(), [])
  const [mounted, setMounted] = useState(false)
  const hasDigitalProperty = useSelector(selectUserHaveDigitalProperty)
  const hasCommercialization = useSelector(selectUserHaveCommercialization)
  const hasShoppingPlatform = useSelector(selectUserHaveShoppingPlatform)
  const hasTechnical = useSelector(selectModulesHaveTechnical)
  const hasAdministration = useSelector(selectUserHaveAdministration)

  const classes = useStyles()
  const [privacyModal, selectedPrivacyTerm] = useTerm('privacy-term', false)
  const [termModal] = useTerm('use-term', false)

  useEffect(() => {
    if(mounted) {
      privacyModal(termModal)
    } else {
      setMounted(true)
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [selectedPrivacyTerm, mounted])

  return (
    <div className={ classes.container }>
      <div className={ classes.contentScroll }>
        <Grid
          container
          direction="row"
          justify="center"
          alignItems="center"
        >
          <Grid item xs={ 11 } sm={ 11 } md={ 11 } lg={ 10 }>
            <Title>
              <I18n>choose a module to continue</I18n>
            </Title>

            <Content>
              {hasDigitalProperty && (
                <ButtonCard
                  id={ AVAILABLE_MODULES.digitalProperty }
                  onClick={ () => dispatch(ModuleActions.initDigitalPropertyModule()) }
                >
                  <ButtonCardContent>
                    <Icon icon={ barn } color={ colors.secondary } size={ 100 } />
                    <ButtonCardTitle>
                      <I18n>
                        digital property
                      </I18n>
                    </ButtonCardTitle>
                  </ButtonCardContent>
                </ButtonCard>
              )}

              {hasCommercialization && (
                <ButtonCard
                  id={ AVAILABLE_MODULES.commercialization }
                  onClick={ () => dispatch(ModuleActions.initCommercializationModule()) }
                >
                  <ButtonCardContent>
                    <Icon icon={ currencyWithoutSpace } color={ colors.secondary } size={ 100 } />
                    <ButtonCardTitle>
                      <I18n>
                        commercialization
                      </I18n>
                    </ButtonCardTitle>
                  </ButtonCardContent>
                </ButtonCard>
              )}

              {hasShoppingPlatform && (
                <ButtonCard
                  id={ AVAILABLE_MODULES.shoppingPlatform }
                  onClick={ () => dispatch(ModuleActions.initShoppingPlatformModule()) }
                >
                  <ButtonCardContent>
                    <Icon icon={ shoppingPlatform } color={ colors.secondary } size={ 100 } />
                    <ButtonCardTitle>
                      <I18n>
                        shopping
                      </I18n>
                    </ButtonCardTitle>
                  </ButtonCardContent>
                </ButtonCard>
              )}

              {hasTechnical && (
                <ButtonCard
                  id={ AVAILABLE_MODULES.technical }
                  onClick={ () => dispatch(ModuleActions.initTechnicalModule()) }
                >
                  <ButtonCardContent>
                    <Icon icon={ smallPlant } color={ colors.secondary } size={ 100 } />
                    <ButtonCardTitle>
                      <I18n>
                        technical area
                      </I18n>
                    </ButtonCardTitle>
                  </ButtonCardContent>
                </ButtonCard>
              )}

              {hasAdministration && (
                <ButtonCard
                  id={ AVAILABLE_MODULES.technical }
                  onClick={ () => dispatch(ModuleActions.initAdministrationModule()) }
                >
                  <ButtonCardContent>
                    <Icon icon={ dashboard } color={ colors.secondary } size={ 100 } />
                    <ButtonCardTitle>
                      <I18n>
                        administration
                      </I18n>
                    </ButtonCardTitle>
                  </ButtonCardContent>
                </ButtonCard>
              )}
            </Content>

            <div className={ classes.logoContainer }>
              <img alt="smartcoop" src={ logo } width={ 150 } />
            </div>
          </Grid>
        </Grid>

      </div>
      <div className={ classes.background }>
        <div className={ classes.backgroundImageLeftContainer }>
          <img alt="background" src={ pointsBottomLeft } className={ classes.backgroundImage } />
        </div>
        <div className={ classes.backgroundImageRightContainer }>
          <img alt="background" src={ pointsTopRight } className={ classes.backgroundImage } />
        </div>
      </div>
    </div>
  )
}

export default ChooseModuleScreen
