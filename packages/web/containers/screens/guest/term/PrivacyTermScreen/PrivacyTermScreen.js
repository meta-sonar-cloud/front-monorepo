import React from 'react'

import TermsFragment from '@smartcoop/web-containers/fragments/TermsFragment'

const PrivacyTermScreen = () =>
  (
    <TermsFragment slug="privacy-term" />
  )


export default PrivacyTermScreen
