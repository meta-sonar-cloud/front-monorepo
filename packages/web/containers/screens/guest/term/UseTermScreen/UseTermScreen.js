import React from 'react'

import TermsFragment from '@smartcoop/web-containers/fragments/TermsFragment'

const UseTermScreen = () =>
  (
    <TermsFragment slug="use-term" />
  )


export default UseTermScreen
