import React, { useCallback } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { useHistory } from 'react-router-dom'


import PropTypes from 'prop-types'

import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import ListItemText from '@material-ui/core/ListItemText'

import { useT } from '@smartcoop/i18n'
import { logout as logoutIcon, user as userIcon, userShield as accessUsers } from '@smartcoop/icons'
import { AuthenticationActions } from '@smartcoop/stores/authentication'
import { selectModuleIsTechnical } from '@smartcoop/stores/module/selectorModule'
import Icon from '@smartcoop/web-components/Icon'
import { useRoutes } from '@smartcoop/web-containers/routes/authenticated'

import useStyles from './styles'

const UserMenu = (props) => {
  const { onClosePopup } = props

  const isTechnicalModule = useSelector(selectModuleIsTechnical)
  const dispatch = useCallback(useDispatch(), [])

  const classes = useStyles()
  const history = useHistory()
  const { routes } = useRoutes()

  const t = useT()

  const handleClick = useCallback(
    (onClick) => {
      onClick()
      onClosePopup()
    },
    [onClosePopup]
  )

  const createOption = useCallback(
    ({ icon, text, onClick }) => (
      <ListItem button onClick={ () => handleClick(onClick) }>
        <ListItemIcon classes={ { root: classes.listItemIcon } }>
          {icon}
        </ListItemIcon>
        <ListItemText primary={ text } />
      </ListItem>
    ),
    [handleClick, classes]
  )

  return (
    <List>
      {createOption({
        icon: <Icon icon={ userIcon } />,
        text: t('edit profile'),
        onClick: () => history.push(routes.editProfile.path)
      })}
      {!isTechnicalModule && createOption({
        icon: <Icon icon={ accessUsers } />,
        text: t('user access'),
        onClick: () => history.push(routes.userAccess.path)
      })}
      {createOption({
        icon: <Icon icon={ logoutIcon } />,
        text: t('logout'),
        onClick: () => {
          dispatch(AuthenticationActions.logout())
        }
      })}
    </List>
  )
}

UserMenu.propTypes = {
  onClosePopup: PropTypes.func
}

UserMenu.defaultProps = {
  onClosePopup: () => {}
}

export default UserMenu
