/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */
import React, {
  useState,
  useMemo,
  useCallback,
  useEffect,
  createContext,
  useContext,
  useRef
} from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { NavLink } from 'react-router-dom'

import clsx from 'clsx'
import PropTypes from 'prop-types'

import filterFP from 'lodash/fp/filter'
import flow from 'lodash/fp/flow'
import mapFP from 'lodash/fp/map'
import isEmpty from 'lodash/isEmpty'
import size from 'lodash/size'

import AppBar from '@material-ui/core/AppBar'
import Badge from '@material-ui/core/Badge'
import Drawer from '@material-ui/core/Drawer'
import Fade from '@material-ui/core/Fade'
import Hidden from '@material-ui/core/Hidden'
import IconButton from '@material-ui/core/IconButton'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import ListItemText from '@material-ui/core/ListItemText'
import { useTheme } from '@material-ui/core/styles'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'
import useMediaQuery from '@material-ui/core/useMediaQuery'

import { useDialog } from '@smartcoop/dialog'
import { useT } from '@smartcoop/i18n'
import {
  leftDoubleArrow,
  hamburguerMenu,
  barnYellow,
  smallPlant,
  currencySignRounded,
  arrowDown,
  exitModule,
  shoppingPlatform,
  dashboard
} from '@smartcoop/icons'
import { selectUserHasMultipleModules } from '@smartcoop/stores/authentication/selectorAuthentication'
import { selectNewNotificationsCount } from '@smartcoop/stores/messaging/selectorMessaging'
import { AVAILABLE_MODULES, ModuleActions } from '@smartcoop/stores/module'
import {
  selectCurrentModule,
  selectModuleIsTechnical
} from '@smartcoop/stores/module/selectorModule'
import {
  selectCurrentOrganization,
  selectUserOrganizationsByModule
} from '@smartcoop/stores/organization/selectorOrganization'
import {
  selectCurrentProperty,
  selectProperties
} from '@smartcoop/stores/property/selectorProperty'
import { TechnicalActions } from '@smartcoop/stores/technical'
import { selectCurrentOwner } from '@smartcoop/stores/technical/selectorTechnical'
import { selectUser } from '@smartcoop/stores/user/selectorUser'
import { colors } from '@smartcoop/styles'
import Icon from '@smartcoop/web-components/Icon'
import BellIconButton from '@smartcoop/web-components/IconButton/BellIconButton'
import UserIconButton from '@smartcoop/web-components/IconButton/UserIconButton'
import Popover from '@smartcoop/web-components/Popover'
import Tooltip from '@smartcoop/web-components/Tooltip'
import NewNotificationsList from '@smartcoop/web-containers/fragments/NewNotificationsList'
import useTerm from '@smartcoop/web-containers/hooks/useTerm'
import ChangeOrganizationModal from '@smartcoop/web-containers/modals/ChangeOrganizationModal'
import ChangePropertyModal from '@smartcoop/web-containers/modals/ChangePropertyModal'
import { version } from '@smartcoop/web-containers/package.json'
import { useRoutes } from '@smartcoop/web-containers/routes/authenticated'

import useStyles, { UserMenuContainer } from './styles'
import UserMenu from './UserMenu'

const LayoutContext = createContext()
export const useLayout = () => useContext(LayoutContext)
function AuthenticatedLayout(props) {
  const { container, children } = props
  const [mounted, setMounted] = useState(false)
  const mainRef = useRef(null)
  const notificationsRef = useRef(null)

  const { createDialog, removeDialog } = useDialog()
  const t = useT()
  const [privacyModal, selectedPrivacyTerm] = useTerm('privacy-term', false)
  const [termModal] = useTerm('use-term', false)

  const dispatch = useCallback(useDispatch(), [])
  const newNotificationsCount = useSelector(selectNewNotificationsCount)

  const properties = useSelector(selectProperties)
  const { organizations, module } = useSelector(selectUserOrganizationsByModule)
  const currentOrganization = useSelector(selectCurrentOrganization)
  const currentModule = useSelector(selectCurrentModule)
  const currentProperty = useSelector(selectCurrentProperty)
  const user = useSelector(selectUser)
  const userHasMultipleModules = useSelector(selectUserHasMultipleModules)

  const isTechnicalModule = useSelector(selectModuleIsTechnical)
  const technicalCurrentOwner = useSelector(selectCurrentOwner)

  const { routes } = useRoutes()

  const classes = useStyles()
  const theme = useTheme()

  const [menuOpen, setMenuOpen] = useState(false)

  const mobileSize = useMediaQuery(theme.breakpoints.down('xs'))

  const handleDrawerToggle = useCallback(() => setMenuOpen(!menuOpen), [
    menuOpen
  ])

  const changePropertyDialog = useCallback(() => {
    if (size(properties) >= 1 || isEmpty(currentProperty)) {
      createDialog({
        id: 'change-property',
        Component: ChangePropertyModal
      })
    }
  }, [createDialog, currentProperty, properties])

  useEffect(() => {
    if (!isEmpty(currentProperty)) {
      removeDialog({ id: 'change-property' })
    }
  }, [currentProperty, removeDialog])

  const changeOrganizationDialog = useCallback(() => {
    if (module === currentModule) {
      if (size(organizations) > 1 || isEmpty(currentOrganization)) {
        createDialog({
          id: 'change-organization',
          Component: ChangeOrganizationModal
        })
      }
    }
  }, [createDialog, currentModule, currentOrganization, module, organizations])

  const changeModule = useCallback(() => {
    if (userHasMultipleModules) {
      dispatch(ModuleActions.exitCurrentModule())
    }
  }, [dispatch, userHasMultipleModules])

  const technicalExitProperty = useCallback(() => {
    dispatch(TechnicalActions.resetTechnicalCurrentOwner())
  }, [dispatch])

  const currentModuleOptions = useMemo(() => {
    switch (currentModule) {
      case AVAILABLE_MODULES.commercialization:
        return {
          icon: currencySignRounded,
          color: colors.secondary,
          name:
            currentOrganization?.tradeName || currentOrganization?.companyName,
          changeDialogMethod: changeOrganizationDialog,
          currentItem: currentOrganization,
          items: organizations
        }
      case AVAILABLE_MODULES.shoppingPlatform:
        return {
          icon: shoppingPlatform,
          color: colors.secondary,
          name:
            currentOrganization?.tradeName || currentOrganization?.companyName,
          changeDialogMethod: changeOrganizationDialog,
          currentItem: currentOrganization,
          items: organizations
        }
      case AVAILABLE_MODULES.administration:
        return {
          icon: dashboard,
          color: colors.secondary,
          name:
            currentOrganization?.tradeName || currentOrganization?.companyName,
          currentItem: currentOrganization,
          changeDialogMethod: changeOrganizationDialog,
          items: organizations
        }
      case AVAILABLE_MODULES.technical:
        return {
          icon: smallPlant,
          color: colors.secondary,
          name: !isEmpty(technicalCurrentOwner)
            ? currentProperty?.name
            : currentOrganization?.tradeName ||
              currentOrganization?.companyName,
          changeDialogMethod: !isEmpty(technicalCurrentOwner)
            ? changePropertyDialog
            : changeOrganizationDialog,
          currentItem: !isEmpty(technicalCurrentOwner)
            ? currentProperty
            : currentOrganization,
          items: !isEmpty(technicalCurrentOwner) ? properties : organizations
        }
      case AVAILABLE_MODULES.digitalProperty:
      default:
        return {
          icon: barnYellow,
          name: currentProperty?.name,
          changeDialogMethod: changePropertyDialog,
          currentItem: currentProperty,
          items: properties
        }
    }
  }, [
    changeOrganizationDialog,
    changePropertyDialog,
    currentModule,
    currentOrganization,
    currentProperty,
    organizations,
    properties,
    technicalCurrentOwner
  ])

  const menuItens = useMemo(
    () =>
      flow(
        filterFP((item) => item.menu),
        mapFP((item) => (
          <NavLink
            key={ item.path }
            to={ item.path }
            exact={
              item.menu?.exact !== undefined ? item.menu.exact : item.exact
            }
            className={ classes.menuLink }
            activeClassName={ classes.menuActive }
          >
            <div className={ classes.activeIndicator } />
            <ListItem classes={ { root: classes.listItem } } button>
              <ListItemIcon classes={ { root: classes.listItemIcon } }>
                <Tooltip title={ item.menu.title } placement="left">
                  <item.menu.Icon />
                </Tooltip>
              </ListItemIcon>
              <Fade in={ menuOpen }>
                <ListItemText
                  classes={ { root: classes.listItemText } }
                  primary={ item.menu.title }
                />
              </Fade>
            </ListItem>
          </NavLink>
        ))
      )(routes),
    [
      routes,
      classes.menuLink,
      classes.menuActive,
      classes.activeIndicator,
      classes.listItem,
      classes.listItemIcon,
      classes.listItemText,
      menuOpen
    ]
  )

  useEffect(() => {
    if (
      isEmpty(currentModuleOptions.currentItem) &&
      !isEmpty(currentModuleOptions.items)
    ) {
      currentModuleOptions.changeDialogMethod()
    }
  }, [changePropertyDialog, currentModuleOptions])

  useEffect(() => {
    if (mounted) {
      privacyModal(termModal)
    } else {
      setMounted(true)
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [selectedPrivacyTerm, mounted])

  const drawer = (
    <div className={ classes.drawerItems }>
      <div className={ classes.menuItensContainer }>
        <List>{menuItens}</List>
      </div>

      {menuOpen && (
        <div className={ classes.drawerFooter }>
          <Typography variant="caption">{`v${ version }`}</Typography>
        </div>
      )}
    </div>
  )

  const layoutProviderValue = useMemo(() => ({ mainRef }), [])

  return (
    <div className={ classes.root }>
      <AppBar color="primary" position="fixed" className={ classes.appBar }>
        <Toolbar classes={ { root: classes.toolbarRoot } }>
          <div className={ classes.toolbarRightContainer }>
            {routes.notifications && (
              <Badge
                color="error"
                badgeContent={ newNotificationsCount }
                max={ 20 }
                overlap="circle"
                anchorOrigin={ {
                  vertical: 'top',
                  horizontal: 'right'
                } }
              >
                <Popover
                  ref={ notificationsRef }
                  popoverId="notifications"
                  Component={ BellIconButton }
                  ComponentProps={ {
                    style: {
                      color: newNotificationsCount ? colors.red : undefined
                    },
                    edge: 'end',
                    iconColor: colors.white,
                    color: 'inherit',
                    parseColor: false
                  } }
                >
                  <NewNotificationsList
                    onSeeAllClick={ () => notificationsRef.current.onClose() }
                  />
                </Popover>
              </Badge>
            )}
            <Popover
              popoverId="user-menu"
              Component={ UserIconButton }
              ComponentProps={ {
                color: 'white',
                edge: 'end',
                iconColor: colors.white,
                userName: user.name
              } }
            >
              <UserMenuContainer style={ { minWidth: 150 } }>
                <UserMenu />
              </UserMenuContainer>
            </Popover>
          </div>

          <div className={ classes.leftWrapper }>
            <IconButton
              color="inherit"
              edge="start"
              onClick={ handleDrawerToggle }
              className={ classes.menuButton }
            >
              <Icon
                icon={ menuOpen ? leftDoubleArrow : hamburguerMenu }
                color={ colors.white }
                size={ 22 }
              />
            </IconButton>

            <div
              className={ clsx(classes.leftContainer, classes.changeArea) }
              onClick={ changeModule }
            >
              <Icon
                icon={ currentModuleOptions.icon }
                size={ 33 }
                color={ currentModuleOptions.color || colors.primary }
              />
              {userHasMultipleModules && (
                <Icon icon={ arrowDown } color={ colors.white } size={ 18 } />
              )}
            </div>

            <div
              className={ clsx(classes.leftContainer, classes.changeProperty) }
              onClick={ currentModuleOptions.changeDialogMethod }
            >
              {currentModuleOptions.name}
              {size(currentModuleOptions.items) > 1 && (
                <Icon icon={ arrowDown } color={ colors.white } size={ 18 } />
              )}
            </div>

            {isTechnicalModule && !isEmpty(technicalCurrentOwner) && (
              <div
                className={ clsx(classes.leftContainer, classes.exitProperty) }
                onClick={ technicalExitProperty }
              >
                <Tooltip title={ t('comeback to technical area') }>
                  <Icon icon={ exitModule } color={ colors.white } size={ 24 } />
                </Tooltip>
              </div>
            )}
          </div>
        </Toolbar>
      </AppBar>

      <nav className={ classes.drawer } aria-label="mailbox folders">
        {/* The implementation can be swapped with js to avoid SEO duplication of links. */}
        <Hidden smUp implementation="css">
          <Drawer
            container={ container }
            variant="temporary"
            anchor={ theme.direction === 'rtl' ? 'right' : 'left' }
            open={ menuOpen && mobileSize }
            onClose={ handleDrawerToggle }
            classes={ {
              paper: classes.drawerPaper
            } }
            ModalProps={ {
              keepMounted: true // Better open performance on mobile.
            } }
          >
            {drawer}
          </Drawer>
        </Hidden>
        <Hidden xsDown implementation="css">
          <Drawer
            variant="permanent"
            className={ clsx(classes.drawer, {
              [classes.drawerOpen]: menuOpen,
              [classes.drawerClose]: !menuOpen
            }) }
            classes={ {
              paper: clsx(classes.drawerPaper, {
                [classes.drawerOpen]: menuOpen,
                [classes.drawerClose]: !menuOpen
              })
            } }
          >
            {drawer}
          </Drawer>
        </Hidden>
      </nav>

      <main id="main" ref={ mainRef } className={ classes.content }>
        {/* TODO toolbar */}
        <div className={ classes.toolbar } />
        <LayoutContext.Provider value={ layoutProviderValue }>
          {children}
        </LayoutContext.Provider>
      </main>
    </div>
  )
}

AuthenticatedLayout.propTypes = {
  /**
   * Injected by the documentation to work in an iframe.
   * You won't need it on your project.
   */
  container: PropTypes.element,
  children: PropTypes.element
}

AuthenticatedLayout.defaultProps = {
  children: null,
  container: null
}

export default AuthenticatedLayout
