import React, { useCallback, forwardRef } from 'react'

import PropTypes from 'prop-types'

import filterProductGroupSchema from '@smartcoop/forms/schemas/shoppingPlatform/productGroup/filterProductGroup.schema'
import I18n, { useT } from '@smartcoop/i18n'
import { getCrops } from '@smartcoop/services/apis/smartcoopApi/resources/crop'
import Button from '@smartcoop/web-components/Button'
import Form from '@smartcoop/web-components/Form'
import InputSelect from '@smartcoop/web-components/InputSelect'
import InputText from '@smartcoop/web-components/InputText'

import { Container, ButtonContainer } from './styles'

const FilterProductGroupForm = forwardRef((props, formRef) => {
  const { withoutSubmitButton, loading, onSubmit, filters } = props

  const t = useT()

  const handleSubmit = useCallback(
    (data) => onSubmit(data),
    [onSubmit]
  )

  return (
    <Container>
      <Form
        style={ { display: 'flex', flexDirection: 'column', width: '100%' } }
        ref={ formRef }
        schemaConstructor={ filterProductGroupSchema }
        onSubmit={ handleSubmit }
      >

        <InputText
          name="q"
          label={ t('product group') }
          fullWidth
          defaultValue={ filters.q }
        />

        <InputText
          name="description"
          label={ t('description') }
          fullWidth
          defaultValue={ filters.description }
        />

        <InputSelect
          label={ t('crop', { howMany: 1 }) }
          name="cropId"
          options={ getCrops }
          defaultValue={ filters.cropId }
          asyncOptionLabelField="description"
        />

        {!withoutSubmitButton && (
          <ButtonContainer>
            <Button
              id="web-order-filter-form-button"
              style={ { width: '48%' } }
              onClick={ () => formRef.current.submit() }
              disabled={ loading }
            >
              <I18n>filter</I18n>
            </Button>
          </ButtonContainer>
        )}
      </Form>
    </Container>
  )
})

FilterProductGroupForm.propTypes = {
  filters: PropTypes.object,
  loading: PropTypes.bool,
  onSubmit: PropTypes.func,
  withoutSubmitButton: PropTypes.bool
}

FilterProductGroupForm.defaultProps = {
  filters: {},
  loading: false,
  onSubmit: () => {},
  withoutSubmitButton: false
}

export default FilterProductGroupForm
