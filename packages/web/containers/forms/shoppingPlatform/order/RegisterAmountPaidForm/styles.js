import styled from 'styled-components'

export const Container = styled.div`
  justify-content: center;
  align-items: center;
  flex: 1 1 auto;
  flex-direction: column;
  width: 300px;
`

export const ButtonContainer = styled.div`
  flex: 1 1 auto;
  justify-content: center;
  align-items: flex-end;
  flex-direction: column;
  width: 100%;
`

export const TextDate = styled.div`
  font-weight: 800;
  font-size: 18px;
  text-align: center;
  padding-bottom: 30px;
`

export const ContentRadioGroup = styled.div`
  margin-top: 15px;
`
