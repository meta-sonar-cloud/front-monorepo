import React, {
  useCallback,
  forwardRef,
  useMemo,
  useState,
  useEffect
} from 'react'
import { useDispatch, useSelector } from 'react-redux'

import moment from 'moment/moment'
import PropTypes from 'prop-types'

import debounce from 'lodash/debounce'
import filter from 'lodash/filter'
import find from 'lodash/find'
import findFP from 'lodash/fp/find'
import flow from 'lodash/fp/flow'
import mapFP from 'lodash/fp/map'
import includes from 'lodash/includes'
import isEmpty from 'lodash/isEmpty'
import map from 'lodash/map'
import reduce from 'lodash/reduce'
import sumBy from 'lodash/sumBy'
import trim from 'lodash/trim'

import { useDialog } from '@smartcoop/dialog'
import registerOrderSchema from '@smartcoop/forms/schemas/shoppingPlatform/order/registerOrder.schema'
import I18n, { useT } from '@smartcoop/i18n'
import { shoppingCart } from '@smartcoop/icons'
import {
  getProducts as getProductsService,
  getProduct
} from '@smartcoop/services/apis/smartcoopApi/resources/product'
import { getProductGroups as getProductGroupsService } from '@smartcoop/services/apis/smartcoopApi/resources/productGroup'
import { useSnackbar } from '@smartcoop/snackbar'
import { OrderActions } from '@smartcoop/stores/order'
import { OrganizationActions } from '@smartcoop/stores/organization'
import {
  selectCurrentOrganization,
  selectCurrentOrganizationAddresses
} from '@smartcoop/stores/organization/selectorOrganization'
import colors from '@smartcoop/styles/colors'
import Button from '@smartcoop/web-components/Button'
import CheckboxTreeView from '@smartcoop/web-components/CheckboxTreeView'
import FilteredCheckboxGroup from '@smartcoop/web-components/FilteredCheckboxGroup'
import Form from '@smartcoop/web-components/Form'
import Icon from '@smartcoop/web-components/Icon'
import InputDate from '@smartcoop/web-components/InputDate'
import InputSelect from '@smartcoop/web-components/InputSelect'
import {
  Item,
  Title,
  ButtonsContainer
} from '@smartcoop/web-containers/layouts/AuthenticatedLayout/theme'
import SplitedScreenLayout from '@smartcoop/web-containers/layouts/SplitedScreenLayout'
import RegistrationAmountPaidModal from '@smartcoop/web-containers/modals/AmountPaid/RegistrationAmountPaidModal'
import ViewListAmountPaidModal from '@smartcoop/web-containers/modals/AmountPaid/ViewListAmountPaidModal'

import AddressWithQuantity from './AddressWithQuantity'
import {
  IconContainer,
  Row,
  TotalQuantityRow,
  TotalQuantity,
  Header,
  Container
} from './styles'

const RegisterOrderForm = forwardRef((props, formRef) => {
  const { onSuccess, onCancel } = props

  const t = useT()
  const dispatch = useCallback(useDispatch(), [])
  const snackbar = useSnackbar()
  const { createDialog } = useDialog()

  const currentOrganization = useSelector(selectCurrentOrganization)
  const currentOrganizationAddresses = useSelector(
    selectCurrentOrganizationAddresses
  )

  const [loading, setLoading] = useState(false)
  const [addressesOptions, setAddressesOptions] = useState([])
  const [productGroup, setProductGroup] = useState(null)
  const [unit, setUnit] = useState('kg')
  const [minReceiptDate, setMinReceiptDate] = useState(
    moment().format('YYYY-MM-DD')
  )
  const [paymentOptions, setPaymentOptions] = useState([
    {
      id: 'vista',
      checked: true,
      disabled: true,
      label: 'À Vista'
    },
    {
      id: 'prazo',
      label: 'À Prazo',
      creatableInput: {
        type: 'inputNumber',
        label: t('quantity of days'),
        name: 'creatableValue',
        maxLength: 3
      },
      options: [
        {
          id: '30',
          checked: true,
          label: `30 ${ t('day', { howMany: 30 }) }`
        },
        {
          id: '90',
          checked: false,
          label: `90 ${ t('day', { howMany: 90 }) }`
        }
      ]
    }
  ])

  const schemaProps = useMemo(
    () => ({
      minReceiptDate
    }),
    [minReceiptDate]
  )

  const totalQuantity = useMemo(
    () => sumBy(addressesOptions, (item) => Number(item.quantity) || 0),
    [addressesOptions]
  )

  const updateAddresses = useCallback(() => {
    setLoading(true)
    dispatch(
      OrganizationActions.loadCurrentOrganizationAddresses(
        () => setLoading(false),
        () => setLoading(false)
      )
    )
  }, [dispatch])

  useEffect(() => {
    updateAddresses()
  }, [updateAddresses])

  const handleSubmit = useCallback(
    ({ deliveryLocations, ...submittedData }) => {
      const addressWithQuantityZero = flow(
        mapFP((opt) => find(addressesOptions, { value: opt })),
        findFP((opt) => !!opt.error)
      )(deliveryLocations)

      if (!addressWithQuantityZero) {
        const paymentForms = reduce(
          submittedData.paymentForms,
          (acc, val, valName) => {
            if (valName === 'prazo') {
              return [
                ...acc,
                ...map(val, (_, days) => ({
                  deferredPayment: true,
                  daysAmount: Number(days)
                }))
              ]
            }
            return [
              ...acc,
              {
                deferredPayment: false,
                daysAmount: 0
              }
            ]
          },
          []
        )

        setLoading(true)
        const data = {
          ...submittedData,
          organizationId: currentOrganization.id,
          paymentForms,
          organizationPurchases: [
            {
              totalAmount: Number(totalQuantity),
              organizationId: currentOrganization.id,
              deliveryLocations: map(deliveryLocations, (address) => ({
                organizationId: address,
                amount: Number(
                  find(addressesOptions, { value: address })?.quantity
                )
              }))
            }
          ]
        }
        dispatch(
          OrderActions.saveOfflineOrder(
            data,
            () => {
              snackbar.success(
                t('your {this} was registered', {
                  howMany: 1,
                  this: t('order', { howMany: 1 }),
                  gender: 'female'
                })
              )
              setLoading(false)
              onSuccess()
            },
            () => setLoading(false)
          )
        )
      }
    },
    [
      addressesOptions,
      currentOrganization.id,
      dispatch,
      onSuccess,
      snackbar,
      t,
      totalQuantity
    ]
  )

  const handleProductChanges = useCallback(
    async (event) => {
      const selectedProduct = (
        await getProduct({ productId: event.target.value, params: {} })
      ).data
      setUnit(selectedProduct?.unitOfMeasures)
      setMinReceiptDate(selectedProduct?.minReceiptDate || minReceiptDate)
    },
    [minReceiptDate]
  )

  const changedProductGroup = useCallback(({ target: { value } }) => {
    setProductGroup({ id: value })
  }, [])

  const defaultPaymentExpanded = useMemo(() => ['prazo'], [])

  const productGroupId = useMemo(() => productGroup?.id, [productGroup])

  const productQueryParams = useMemo(
    () => ({ productGroupId, purchasing: true }),
    [productGroupId]
  )

  const handleAddressQuantityChange = useCallback(
    debounce((id, newOpt) => {
      setAddressesOptions((old) =>
        old.map((opt) => {
          if (opt.value === id) {
            return { ...opt, ...newOpt }
          }
          return opt
        })
      )
    }, 300),
    []
  )

  const filterOptions = useCallback(
    (filterText, items) => {
      if (isEmpty(trim(filterText))) {
        return items
      }
      return filter(items, (item) => {
        const address = find(currentOrganizationAddresses, { id: item.value })
        return includes(address.companyDocument, filterText)
      })
    },
    [currentOrganizationAddresses]
  )

  const RegisterAmountPaid = useCallback(() => {
    createDialog({
      id: 'register-history-price',
      Component: RegistrationAmountPaidModal,
      props: {
        productId: formRef.current?.getFieldValue('productId'),
        productUnit: unit
      }
    })
  }, [createDialog, formRef, unit])

  const ListAmountPaid = useCallback(() => {
    createDialog({
      id: 'complete-crop-management',
      Component: ViewListAmountPaidModal,
      props: {
        productId: formRef.current?.getFieldValue('productId'),
        organizationId: currentOrganization.id
      }
    })
  }, [createDialog, currentOrganization.id, formRef])

  useEffect(() => {
    setAddressesOptions((old) =>
      map(currentOrganizationAddresses, (address) => ({
        value: address.id,
        quantity: find(old, { value: address.id })?.quantity || '',
        // eslint-disable-next-line react/prop-types
        render: ({ option }) => (
          <AddressWithQuantity
            // eslint-disable-next-line react/prop-types
            id={ option.value }
            // eslint-disable-next-line react/prop-types
            checked={ option.checked }
            address={ address }
            onChange={ handleAddressQuantityChange }
            // eslint-disable-next-line react/prop-types
            quantity={ option.quantity }
            // eslint-disable-next-line react/prop-types
            error={ option.error }
            unit={ unit }
          />
        )
      }))
    )
    // disable-eslint-next-line
  }, [
    currentOrganizationAddresses,
    handleAddressQuantityChange,
    unit,
    updateAddresses
  ])

  // useLayoutEffect(() => {
  //   console.clear()
  // },[dispatch])

  return (
    <Form
      ref={ formRef }
      schemaConstructor={ registerOrderSchema }
      schemaProps={ schemaProps }
      onSubmit={ handleSubmit }
      style={ { flex: 1 } }
      loading={ loading }
    >
      <SplitedScreenLayout
        title={ { name: t('order', { howMany: 2 }) } }
        divRightStyle={ { paddingTop: 67 } }
        leftChildren={
          <Container>
            <Header style={ { paddingLeft: 35 } }>
              <IconContainer>
                <Icon icon={ shoppingCart } size={ 18 } />
              </IconContainer>

              <Title style={ { fontSize: 16, fontWeight: 600 } }>
                <I18n>new purchase order</I18n>
              </Title>
            </Header>

            <Item style={ { padding: '0 35px 0 35px' } }>
              <InputSelect
                label={ t('product group') }
                name="productGroupId"
                options={ getProductGroupsService }
                queryParams={ { purchasing: true } }
                onChange={ changedProductGroup }
              />

              <InputSelect
                label={ t('product') }
                name="productId"
                options={ productGroupId ? getProductsService : [] }
                queryParams={ productQueryParams }
                onChange={ handleProductChanges }
                asyncOptionLabelField="description"
              />

              <InputDate
                pickerProps={ {
                  minDate: minReceiptDate
                } }
                style={ { width: '100%' } }
                label={ t('receipt deadline') }
                name="receiptDeadline"
                fullWidth
              />

              <CheckboxTreeView
                id="checkbox-tree-view"
                name="paymentForms"
                label={ t('payment methods') }
                options={ paymentOptions }
                onChangeOptions={ setPaymentOptions }
                defaultExpanded={ defaultPaymentExpanded }
              />
            </Item>
          </Container>
        }
        rightChildren={
          <Container style={ { flex: 1 } }>
            <div style={ { alignSelf: 'flex-end' } }>
              <Button
                id="see-paid-price-history"
                onClick={ ListAmountPaid }
                style={ { width: 'auto', fontWeight: 600, marginRight: 10 } }
                color={ colors.white }
              >
                <I18n>see paid price history</I18n>
              </Button>
              <Button
                id="inform-price-paid"
                onClick={ RegisterAmountPaid }
                style={ { width: 'auto', fontWeight: 600 } }
                color={ colors.white }
                disabled={ !formRef.current?.getFieldValue('productId') }
              >
                <I18n>inform price paid</I18n>
              </Button>
            </div>
            <FilteredCheckboxGroup
              name="deliveryLocations"
              label={ t('select delivery locations') }
              options={ addressesOptions }
              onFilterOptions={ filterOptions }
              inputSearchStyle={ { backgroundColor: colors.white } }
              inputLabel={ t('search delivery location by cnpj') }
            />

            <TotalQuantityRow>
              <Row style={ { flex: 2 } }>
                <TotalQuantity style={ { marginRight: 8 } }>
                  <I18n>total quantity</I18n>:
                </TotalQuantity>
                <TotalQuantity style={ { fontWeight: 'normal' } }>
                  {totalQuantity} {unit}
                </TotalQuantity>
              </Row>

              <ButtonsContainer style={ { flex: 1 } }>
                <Button
                  id="cancel-order-register-form"
                  onClick={ onCancel }
                  style={ { flex: 1 } }
                  variant="outlined"
                >
                  <I18n>cancel</I18n>
                </Button>

                <div style={ { width: '10%' } } />

                <Button
                  id="submit-order-register-form"
                  onClick={ () => formRef.current.submit() }
                  style={ { flex: 1 } }
                >
                  <I18n>save</I18n>
                </Button>
              </ButtonsContainer>
            </TotalQuantityRow>
          </Container>
        }
      />
    </Form>
  )
})

RegisterOrderForm.propTypes = {
  onSuccess: PropTypes.func,
  onCancel: PropTypes.func
}

RegisterOrderForm.defaultProps = {
  onSuccess: () => {},
  onCancel: () => {}
}

export default RegisterOrderForm
