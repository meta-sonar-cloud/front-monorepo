import styled from 'styled-components'

export const Container = styled.div`
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  flex-wrap: wrap;
  width: 100%;
`

export const AddressContainer = styled.div`
  flex: 2;
  min-width: 400px;
  margin-bottom: 8px;
`

export const QuantityContainer = styled.div`
  flex: 1;
  min-width: 150px;
  margin-top: 10px;
`
