import React, { useCallback, useEffect, useState } from 'react'

import PropTypes from 'prop-types'
import * as Yup from 'yup'

import validateDataBySchema from '@smartcoop/forms/src/utils/validateDataBySchema'
import minNumberValue from '@smartcoop/forms/validators/minNumberValue.validator'
import { useT } from '@smartcoop/i18n'
import Address from '@smartcoop/web-components/Address'
import InputUnit from '@smartcoop/web-components/InputUnit'

import { Container, AddressContainer, QuantityContainer } from './styles'

const AddressWithQuantity = (props) => {
  const {
    id,
    address,
    onChange,
    checked,
    quantity: externalQuantity,
    error,
    unit,
    disabled
  } = props
  const t = useT()

  const [quantity, setQuantity] = useState(externalQuantity)

  const handleQuantityChange = useCallback(
    ({ target }) => {
      setQuantity(target.value)
    },
    []
  )

  useEffect(() => {
    async function validate() {
      let newError
      try {
        await validateDataBySchema({
          data: { quantity },
          schema: Yup.object().shape({
            quantity: minNumberValue({ t, field: t('quantity') })(Yup.string())
          })
        })
      } catch (err) {
        if (err.formError) {
          newError = err.messages.quantity
        }
      } finally {
        onChange(id, { quantity, error: newError })
      }
    }

    if (checked) {
      validate()
    }
  }, [checked, id, onChange, quantity, t])

  useEffect(() => {
    if (!checked) {
      onChange(id, { quantity: '0', error: undefined })
    }
  }, [checked, id, onChange])

  useEffect(() => {
    if (externalQuantity !== quantity) {
      setQuantity(externalQuantity)
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [externalQuantity])

  return (
    <Container>
      <AddressContainer>
        <Address fields={ address } />
      </AddressContainer>
      <QuantityContainer>
        <InputUnit
          unit={ unit }
          label={ t('quantity') }
          onChange={ handleQuantityChange }
          disabled={ disabled || !checked }
          value={ quantity }
          error={ error }
          style={ { width: '100%' } }
          detached
        />
      </QuantityContainer>
    </Container>
  )
}

AddressWithQuantity.propTypes = {
  id: PropTypes.string.isRequired,
  address: PropTypes.object.isRequired,
  onChange: PropTypes.func,
  checked: PropTypes.bool,
  disabled: PropTypes.bool,
  quantity: PropTypes.string,
  error: PropTypes.string,
  unit: PropTypes.string
}

AddressWithQuantity.defaultProps = {
  onChange: () => {},
  checked: false,
  quantity: '0',
  error: undefined,
  unit: null,
  disabled: false
}


export default AddressWithQuantity
