import React, { forwardRef, useCallback, useMemo, useState, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import moment from 'moment/moment'
import PropTypes from 'prop-types'

import debounce from 'lodash/debounce'
import filter from 'lodash/filter'
import find from 'lodash/find'
import findFP from 'lodash/fp/find'
import flow from 'lodash/fp/flow'
import mapFP from 'lodash/fp/map'
import includes from 'lodash/includes'
import isEmpty from 'lodash/isEmpty'
import map from 'lodash/map'
import sumBy from 'lodash/sumBy'
import trim from 'lodash/trim'

import { useDialog } from '@smartcoop/dialog'
import joinOrderSchema from '@smartcoop/forms/schemas/shoppingPlatform/order/joinOrder.schema'
import I18n, { useT } from '@smartcoop/i18n'
import { OrderActions } from '@smartcoop/stores/order'
import { selectCurrentOrder, selectCurrentOrderProduct } from '@smartcoop/stores/order/selectorOrder'
import { OrganizationActions } from '@smartcoop/stores/organization'
import { selectCurrentOrganization, selectCurrentOrganizationAddresses } from '@smartcoop/stores/organization/selectorOrganization'
import { colors } from '@smartcoop/styles'
import { formatCpfCnpj } from '@smartcoop/utils/formatters'
import Button from '@smartcoop/web-components/Button'
import FilteredCheckboxGroup from '@smartcoop/web-components/FilteredCheckboxGroup'
import Form from '@smartcoop/web-components/Form'
import AddressWithQuantity from '@smartcoop/web-containers/forms/shoppingPlatform/order/RegisterOrderForm/AddressWithQuantity'
import { ButtonsContainer } from '@smartcoop/web-containers/layouts/AuthenticatedLayout/theme'
import RegistrationAmountPaidModal from '@smartcoop/web-containers/modals/AmountPaid/RegistrationAmountPaidModal'
import ViewListAmountPaidModal from '@smartcoop/web-containers/modals/AmountPaid/ViewListAmountPaidModal'

import { Row, TotalQuantityRow, TotalQuantity } from './styles'

const JoinOrderForm = forwardRef((props, formRef) => {
  const { onSuccess, isEditing } = props

  const t = useT()
  const dispatch = useCallback(useDispatch(), [])
  const { createDialog } = useDialog()

  const currentOrganizationAddresses = useSelector(selectCurrentOrganizationAddresses)
  const currentOrganization = useSelector(selectCurrentOrganization)
  const product = useSelector(selectCurrentOrderProduct)

  const currentOrder = useSelector(selectCurrentOrder)

  const [loading, setLoading] = useState(false)
  const [addressesOptions, setAddressesOptions] = useState([])

  const productUnit = useMemo(
    () => product.unitOfMeasures || 'kg',
    [product]
  )

  const totalQuantity = useMemo(
    () => sumBy(addressesOptions, ( item ) => Number(item.quantity) || 0),
    [addressesOptions]
  )

  const disableActions = useMemo(() => {
    if (!isEmpty(currentOrder)) {
      return (currentOrder.status?.slug !== 'aberta' &&
        (moment().isAfter(moment(currentOrder.participationDeadline))))
    }
    return true
  }
  , [currentOrder])

  const activeOrganizationPurchase = useMemo(
    () => find(currentOrder.organizationPurchases, ({ organizationId }) => organizationId === currentOrganization.id),
    [currentOrder.organizationPurchases, currentOrganization.id]
  )

  const updateAddresses = useCallback(() => {
    setLoading(true)
    dispatch(OrganizationActions.loadCurrentOrganizationAddresses(() => setLoading(false), () => setLoading(false)))
  },[dispatch])

  useEffect(() => {
    updateAddresses()
  },[updateAddresses])

  const handleSubmit = useCallback(
    ({ deliveryLocations }) => {
      const addressWithQuantityZero = flow(
        mapFP(opt => find(addressesOptions, { value: opt })),
        findFP((opt) => !!opt.error)
      )(deliveryLocations)

      if (!addressWithQuantityZero) {
        setLoading(true)
        const data = {
          totalAmount: Number(totalQuantity),
          organizationId: currentOrganization.id,
          deliveryLocations: map(deliveryLocations, address => ({
            organizationId: address,
            amount: Number(find(addressesOptions, { value: address })?.quantity)
          }))
        }

        dispatch(OrderActions.joinOfflineOrder(
          data,
          isEditing,
          activeOrganizationPurchase?.id,
          () => {
            setLoading(false)
            onSuccess()
          },
          () => setLoading(false)
        ))
      }
    },
    [activeOrganizationPurchase, addressesOptions, currentOrganization.id, dispatch, isEditing, onSuccess, totalQuantity]
  )

  const handleAddressQuantityChange = useCallback(
    debounce((id, newOpt) => {
      setAddressesOptions((old) => old.map((opt) => {
        if (opt.value === id) {
          return { ...opt, ...newOpt }
        }
        return opt
      }))
    }, 300),
    []
  )

  const filterOptions = useCallback(
    (filterText, items) => {
      if (isEmpty(trim(filterText))) {
        return items
      }
      return filter(
        items,
        (item) => {
          const address = find(currentOrganizationAddresses, { id: item.value })
          return (includes(address.companyDocument, filterText) || includes(formatCpfCnpj(address.companyDocument), filterText)) && !item.checked
        }
      )
    },
    [currentOrganizationAddresses]
  )


  const RegisterAmountPaid = useCallback(
    () => {
      createDialog({
        id: 'register-history-price',
        Component: RegistrationAmountPaidModal,
        props: {
          productId:  product.id,
          productUnit
        }
      })
    },
    [createDialog, product.id, productUnit]
  )

  const ListAmountPaid = useCallback(
    () => {
      createDialog({
        id: 'complete-crop-management',
        Component: ViewListAmountPaidModal,
        props: {
          productId:  product.id,
          organizationId: currentOrganization.id
        }
      })
    },
    [createDialog, currentOrganization.id, product.id]
  )

  useEffect(
    () => {
      setAddressesOptions((old) =>
        map(currentOrganizationAddresses, (address) => ({
          value: (address.id).toString(),
          quantity: (find(old, { value: address.id })?.quantity || 0).toString(),
          // eslint-disable-next-line react/prop-types
          render: ({ option }) => (
            <AddressWithQuantity
              // eslint-disable-next-line react/prop-types
              id={ option.value }
              // eslint-disable-next-line react/prop-types
              checked={ option.checked }
              address={ address }
              onChange={ handleAddressQuantityChange }
              // eslint-disable-next-line react/prop-types
              quantity={ option.quantity }
              // eslint-disable-next-line react/prop-types
              error={ option.error }
              unit={ productUnit }
              disabled={ disableActions }
            />
          )
        }))
      )
    },
    [currentOrganizationAddresses, disableActions, handleAddressQuantityChange, productUnit]
  )

  useEffect(
    () => {
      setAddressesOptions((old) =>
        isEmpty(old) ?
          old :
          map(old, (item) => {
            const deliveryLocation = find(activeOrganizationPurchase?.deliveryLocations, ({ organizationId }) => organizationId === item.value)

            return ({
              ...item,
              quantity: (deliveryLocation?.amount || find(old, { value: item.value })?.quantity || 0).toString()
            })
          })
      )

      const items = map(activeOrganizationPurchase?.deliveryLocations, ({ organizationId }) => organizationId)

      formRef.current.setFieldValue('deliveryLocations', items)
    },
    [activeOrganizationPurchase, currentOrganizationAddresses, formRef]
  )

  return (
    <>
      <div style={ { alignSelf: 'flex-end' } }>
        <Button
          id="open-register-purchase"
          onClick={ ListAmountPaid }
          style={ { width: 'auto', fontWeight: 600, marginRight: 10 } }
          color={ colors.white }
        >
          <I18n>see paid price history</I18n>
        </Button>
        <Button
          id="open-register-purchase"
          onClick={ RegisterAmountPaid }
          style={ { width: 'auto', fontWeight: 600 } }
          color={ colors.white }
        >
          <I18n>inform price paid</I18n>
        </Button>
      </div>
      <Form
        ref={ formRef }
        schemaConstructor={ joinOrderSchema }
        onSubmit={ handleSubmit }
        style={ { flex: 1 } }
        loading={ loading }
      >
        <FilteredCheckboxGroup
          name="deliveryLocations"
          label={ t('participate in the purchase') }
          options={ addressesOptions }
          onFilterOptions={ filterOptions }
          inputSearchStyle={ { backgroundColor: colors.white } }
          inputLabel={ t('search delivery location by cnpj') }
          disabled={ disableActions }
        />

        <TotalQuantityRow>
          <Row style={ { flex: 2 } }>
            <TotalQuantity style={ { marginRight: 8 } }>
              <I18n>total quantity</I18n>:
            </TotalQuantity>
            <TotalQuantity style={ { fontWeight: 'normal' } }>
              { totalQuantity } { productUnit }
            </TotalQuantity>
          </Row>

          <ButtonsContainer style={ { flex: 1 } }>
            <Button
              id="submit-order-register-form"
              color={ colors.secondary }
              onClick={ () => formRef.current.submit() }
              style={ { flex: 1 } }
              disabled={ disableActions }
            >
              <I18n>{ isEditing ? 'save' : 'participate'}</I18n>
            </Button>
          </ButtonsContainer>
        </TotalQuantityRow>
      </Form>
    </>
  )
})

JoinOrderForm.propTypes = {
  onSuccess: PropTypes.func,
  isEditing: PropTypes.bool
}

JoinOrderForm.defaultProps = {
  onSuccess: () => {},
  isEditing: false
}

export default JoinOrderForm
