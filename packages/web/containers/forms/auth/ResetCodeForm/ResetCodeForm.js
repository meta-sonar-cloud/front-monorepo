import React, { useCallback, forwardRef } from 'react'
import { useDispatch } from 'react-redux'

import PropTypes from 'prop-types'

import { useDialog } from '@smartcoop/dialog'
import resetCode from '@smartcoop/forms/schemas/auth/resetcode.schema'
import I18n, { useT } from '@smartcoop/i18n'
import { AuthenticationActions } from '@smartcoop/stores/authentication'
import Button from '@smartcoop/web-components/Button'
import Form from '@smartcoop/web-components/Form'
import InputCode from '@smartcoop/web-components/InputCode'
import ErrorModal from '@smartcoop/web-containers/modals/ErrorModal'
import LoadingModal from '@smartcoop/web-containers/modals/LoadingModal'

import { Container, ButtonContainer } from './styles'

const ResetCodeForm = forwardRef((props, formRef) => {
  const { withoutSubmitButton, loading, onSubmit } = props

  const handleKeyDown = useCallback(
    (event) => {
      if (event.keyCode === 13) {
        formRef.current.submit()
      }
    },
    [formRef]
  )

  const dispatch = useCallback(useDispatch(), [])
  const { createDialog, removeDialog } = useDialog()

  const t = useT()

  const handleResetCodeSubmit = useCallback(
    (data) => {
      createDialog({
        id: 'loading',
        Component: LoadingModal
      })

      const removeLoader = () => {
        setTimeout(() => removeDialog({ id: 'loading' }), 150)
      }

      dispatch(AuthenticationActions.validateVerificationCode(
        data.code,
        () => {
          removeLoader()
          onSubmit()
        },
        () => {
          createDialog({
            id: 'user-not-found',
            Component: ErrorModal,
            props: {
              message: t('invalid verification code')
            }
          })
          removeLoader()
        }
      ))
    },
    [createDialog, dispatch, onSubmit, removeDialog, t]
  )

  return (
    <Container>
      <Form
        ref={ formRef }
        schemaConstructor={ resetCode }
        onSubmit={ handleResetCodeSubmit }
      >
        <InputCode
          name="code"
          label={ t('code') }
          disabled={ loading }
          fullWidth
          onKeyDown={ handleKeyDown }
        />

        {!withoutSubmitButton && (
          <ButtonContainer>
            <Button
              id="reset-code-submit"
              style={ { width: '48%' } }
              onClick={ () => formRef.current.submit() }
              disabled={ loading }
            >
              <I18n>next</I18n>
            </Button>
          </ButtonContainer>
        )}
      </Form>
    </Container>
  )
})

ResetCodeForm.propTypes = {
  loading: PropTypes.bool,
  onSubmit: PropTypes.func,
  withoutSubmitButton: PropTypes.bool
}

ResetCodeForm.defaultProps = {
  loading: false,
  onSubmit: () => {},
  withoutSubmitButton: false
}

export default ResetCodeForm
