import styled from 'styled-components'

export const Container = styled.div`
  flex: 1 1 auto;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  width: 100%;
`
