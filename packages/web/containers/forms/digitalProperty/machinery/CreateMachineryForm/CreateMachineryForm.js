import React, { useCallback, forwardRef, useMemo } from 'react'

import PropTypes from 'prop-types'

import machinerySchema from '@smartcoop/forms/schemas/machinery/machineryRegister.schema'
import I18n, { useT } from '@smartcoop/i18n'
import {
  getMachinesTypes as getMachinesTypesService,
  getMachinesBrands as getMachinesBrandsService
} from '@smartcoop/services/apis/smartcoopApi/resources/machine'
import { selectGetProperties as getPropertiesService } from '@smartcoop/services/apis/smartcoopApi/resources/property'
import Button from '@smartcoop/web-components/Button'
import Form from '@smartcoop/web-components/Form'
import InputNumber from '@smartcoop/web-components/InputNumber'
import InputPhone from '@smartcoop/web-components/InputPhone'
import InputSelect from '@smartcoop/web-components/InputSelect'
import InputText from '@smartcoop/web-components/InputText'
import RadioGroup from '@smartcoop/web-components/RadioGroup'
import { Title } from '@smartcoop/web-containers/layouts/AuthenticatedLayout/theme'

import { Container, ButtonContainer, InputContainer } from './styles'

const CreateMachineryForm = forwardRef((props, formRef) => {
  const { withoutSubmitButton, defaultValues, loading, onSubmit } = props

  const t = useT()

  const handleSubmit = useCallback(
    (data) => {
      onSubmit(data)
    },
    [onSubmit]
  )

  const availableOptions = useMemo(
    () => (
      [
        {
          label: t('available'),
          value: true
        },
        {
          label: t('unavailable'),
          value: false
        }
      ]
    ),
    [t]
  )


  return (
    <Container>
      <Form
        style={ { display: 'flex', flexDirection: 'column', width: '100%' } }
        ref={ formRef }
        schemaConstructor={ machinerySchema }
        onSubmit={ handleSubmit }
      >
        <Title style={ { marginTop: 0, fontSize: 16 } }>
          <I18n>rental availability</I18n>
        </Title>
        <RadioGroup
          label=''
          name="available"
          options={ availableOptions }
          variant="row"
          style={ { paddingBottom: 20 } }
          defaultValue={ defaultValues.available }
        />

        <InputText
          name="description"
          label={ t('description') }
          defaultValue={ defaultValues.description }
        />

        <InputSelect
          label={ t('property', { howMany: 1 }) }
          name="propertyId"
          options={ getPropertiesService }
          defaultValue={ defaultValues.property.id }
        />

        <InputSelect
          label={ t('type', { howMany: 1 }) }
          name="machineTypeId"
          options={ getMachinesTypesService }
          asyncOptionLabelField="description"
          defaultValue={ defaultValues.machineType.id }

        />

        <InputSelect
          label={ t('brand', { howMany: 1 }) }
          name="machineBrandId"
          options={ getMachinesBrandsService }
          asyncOptionLabelField="description"
          defaultValue={ defaultValues.machineBrand.id }
        />
        <InputText
          name="model"
          label={ t('model') }
          disabled={ loading }
          defaultValue={ defaultValues.model }
        />


        <InputContainer>
          <InputNumber
            name="year"
            label={ t('year', { howMany: 1 }) }
            maxLength={ 4 }
            style={ { marginRight: 5 } }
            defaultValue={ defaultValues.year }
          />
          <InputNumber
            name="availableHours"
            label={ t('hours') }
            defaultValue={ defaultValues.availableHours }
          />
        </InputContainer>
        <InputPhone
          label={ t('phone') }
          name="phone"
          defaultValue={ defaultValues.phone }
        />
        <InputText
          name="contactInformation"
          label={ t('observations') }
          defaultValue={ defaultValues.contactInformation }
        />

        {!withoutSubmitButton && (
          <ButtonContainer>
            <Button
              id="web-field-form-button"
              style={ { width: '48%' } }
              onClick={ () => formRef.current.submit() }
              disabled={ loading }
            >
              <I18n>next</I18n>
            </Button>
          </ButtonContainer>
        )}
      </Form>
    </Container>
  )
})

CreateMachineryForm.propTypes = {
  loading: PropTypes.bool,
  onSubmit: PropTypes.func,
  withoutSubmitButton: PropTypes.bool,
  defaultValues: PropTypes.object.isRequired
}

CreateMachineryForm.defaultProps = {
  loading: false,
  onSubmit: () => {},
  withoutSubmitButton: false
}

export default CreateMachineryForm
