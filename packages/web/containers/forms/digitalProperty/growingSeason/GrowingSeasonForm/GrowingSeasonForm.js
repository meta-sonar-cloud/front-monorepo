import React, { useCallback, forwardRef, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import PropTypes from 'prop-types'

import growingSeasonRegisterSchema from '@smartcoop/forms/schemas/growingSeason/growingSeasonRegister.schema'
import I18n, { useT } from '@smartcoop/i18n'
import { getCrops as getCropsService } from '@smartcoop/services/apis/smartcoopApi/resources/crop'
import { getCultivationsGoal as getCultivationsGoalService } from '@smartcoop/services/apis/smartcoopApi/resources/cultivationsGoal'
import { FieldActions } from '@smartcoop/stores/field'
import { selectCurrentField } from '@smartcoop/stores/field/selectorField'
import Button from '@smartcoop/web-components/Button'
import Form from '@smartcoop/web-components/Form'
import InputNumber from '@smartcoop/web-components/InputNumber'
import InputSelect from '@smartcoop/web-components/InputSelect'
import InputText from '@smartcoop/web-components/InputText'

import { Container, ButtonContainer } from './styles'

const GrowingSeasonForm = forwardRef((props, formRef) => {
  const { withoutSubmitButton, onSuccess, style, growingSeason } = props
  const dispatch = useCallback(useDispatch(), [])
  const t = useT()
  const [otherCrop, setOtherCrop] = useState(null)

  const currentField = useSelector(selectCurrentField)

  const handleSubmit = useCallback(
    (data) => {
      dispatch(FieldActions.saveOfflineGrowingSeason(
        {
          ...data,
          cropId: otherCrop ? null : data.cropId,
          otherCrop,
          closed: false
        },
        onSuccess
      ))
    },
    [dispatch, onSuccess, otherCrop]
  )

  const handleCropChange = useCallback(
    (_, selectedItem) => {
      if (selectedItem?.created) {
        setOtherCrop(selectedItem.value)
      } else {
        setOtherCrop(null)
      }
    },
    []
  )

  return (
    <Container>
      <Form
        style={ { ...style } }
        ref={ formRef }
        schemaConstructor={ growingSeasonRegisterSchema }
        onSubmit={ handleSubmit }
      >
        <InputText
          label={ t('field', { howMany: 1 }) }
          value={ currentField.fieldName }
          fullWidth
          detached
          disabled
        />

        <InputSelect
          label={ t('crop', { howMany: 1 }) }
          name="cropId"
          options={ getCropsService }
          asyncOptionLabelField="description"
          onChange={ handleCropChange }
          creatable
          defaultValue={ growingSeason.cropId }
        />

        <InputNumber
          maxLength={ 4 }
          label={ t('sowing year') }
          name="sowingYear"
          fullWidth
          defaultValue={ growingSeason.sowingYear }
        />

        <InputSelect
          label={ t('goal') }
          name="cultivationGoalId"
          options={ getCultivationsGoalService }
          asyncOptionLabelField="description"
          defaultValue={ growingSeason.cultivationGoalId }
        />

        {!withoutSubmitButton && (
          <ButtonContainer>
            <Button
              id="web-property-identification-form-button"
              style={ { width: '48%' } }
              onClick={ () => formRef.current.submit() }
            >
              <I18n>next</I18n>
            </Button>
          </ButtonContainer>
        )}
      </Form>
    </Container>
  )
})

GrowingSeasonForm.propTypes = {
  onSuccess: PropTypes.func,
  withoutSubmitButton: PropTypes.bool,
  style: PropTypes.object,
  growingSeason: PropTypes.object
}

GrowingSeasonForm.defaultProps = {
  onSuccess: () => {},
  withoutSubmitButton: false,
  style: {},
  growingSeason: {}
}

export default GrowingSeasonForm
