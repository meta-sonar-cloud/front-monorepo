import React, { forwardRef, useMemo, useState, useEffect, useCallback } from 'react'

import isEmpty from 'lodash/isEmpty'

import { useT } from '@smartcoop/i18n'
import { getProducts as getProductsService } from '@smartcoop/services/apis/smartcoopApi/resources/product'
import { getProductGroups as getProductGroupsService } from '@smartcoop/services/apis/smartcoopApi/resources/productGroup'
import InputHour from '@smartcoop/web-components/InputHour'
import InputSelect from '@smartcoop/web-components/InputSelect'
import InputUnit from '@smartcoop/web-components/InputUnit'

const PhytosanitaryApplicationForm = forwardRef(() => {
  const t = useT()
  const [productGroupSlug, setProductGroupSlug] = useState('')
  const [currentProductGroup, setCurrentProductGroup] = useState({})

  const productGroupOptions = useMemo(
    () => (
      [
        {
          label: 'Herbicidas',
          value: 'herbicidas'
        },
        {
          label: 'Inseticida',
          value: 'inseticida'
        },
        {
          label: 'Fungicida',
          value: 'fungicida'
        }
        // {
        //   label: 'Fertilizante foliar ',
        //   value: 'fertilizante-foliar '
        // }
      ]
    ),
    []
  )

  const unitsOptions = useMemo(
    () => (
      [
        {
          label: 'kg/ha',
          value: 'kg/ha'
        },
        {
          label: 'mg/ha',
          value: 'mg/ha'
        },
        {
          label: 'l/ha',
          value: 'l/ha'
        },
        {
          label: 'ml/ha',
          value: 'ml/ha'
        },
        {
          label: 'ton/ha',
          value: 'ton/ha'
        }
      ]
    ),
    []
  )

  const handleProductGroup = useCallback(
    (selectedItem) => {
      setProductGroupSlug(selectedItem.target.value)
    },
    []
  )

  const productQueryParams = useMemo(
    () => (!isEmpty(currentProductGroup.data?.data[0]?.id) ? { productGroupId: currentProductGroup.data?.data[0]?.id } : {}),
    [currentProductGroup.data]
  )

  useEffect(() => {
    (async () => {
      setCurrentProductGroup(await getProductGroupsService({ slug: productGroupSlug }))
    })()
  }, [productGroupSlug])

  return (
    <>
      <InputSelect
        label={ t('product group') }
        name="productGroup"
        options={ productGroupOptions }
        onChange={ handleProductGroup }
      />

      <InputSelect
        label={ t('products') }
        name="products"
        options={ getProductsService }
        queryParams={ productQueryParams }
        asyncOptionLabelField="shortDescription"
        asyncOptionValueField="shortDescription"
        disabled={ !productGroupSlug }
      />

      <InputHour
        name="hour"
        label={ t('hour') }
        fullWidth
      />

      <div style={ { display: 'flex' } }>
        <div style={ { flex: 1, marginRight: 10 } }>
          <InputUnit
            name="dosePhyntosanitary"
            label={ t('dosePhyntosanitary') }
            maxLength={ 5 }
            type="integer"
            fullWidth
          />
        </div>
        <div>
          <InputSelect
            label={ t('unit') }
            name="unit"
            options={ unitsOptions }
          />
        </div>
      </div>
    </>
  )
})

export default PhytosanitaryApplicationForm
