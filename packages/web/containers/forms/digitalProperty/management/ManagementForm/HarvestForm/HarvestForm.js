import React, { forwardRef, useMemo, useState, useCallback } from 'react'
import { useSelector } from 'react-redux'

import lowerCase from 'lodash/lowerCase'

import { useT } from '@smartcoop/i18n'
import { selectCurrentField } from '@smartcoop/stores/field/selectorField'
import InputNumber from '@smartcoop/web-components/InputNumber'
import InputPercentage from '@smartcoop/web-components/InputPercentage'
import InputSelect from '@smartcoop/web-components/InputSelect'
import InputUnit from '@smartcoop/web-components/InputUnit'

const HarvestForm = forwardRef(() => {
  const t = useT()
  const [unit, setUnit] = useState('kg/ha')

  const currentField = useSelector(selectCurrentField)

  const { crop } = currentField.growingSeason

  const handleUnitsChange = useCallback(
    (selectedItem) => {
      setUnit(selectedItem.target.value)
    },
    []
  )

  const unitProductivity = useMemo(
    () => (
      [
        {
          label: 'kg/ha',
          value: 'kg/ha'
        },
        {
          label: 'sacas/ha',
          value: 'sacas/ha'
        }
      ]
    ),
    []
  )

  const inputProductivity = useMemo(
    () => {
      const inputTypeProductivity = {
        'kg/ha':
          <InputNumber
            maxLength={ 5 }
            label={ t('productivity') }
            name="productivityKg"
            fullWidth
          />,
        'sacas/ha':
          <InputUnit
            maxLength={ 5 }
            label={ t('productivity') }
            name="productivityBags"
            decimalScale={ 1 }
            fullWidth
          />
      }
      return inputTypeProductivity[unit]

    },
    [t, unit]
  )

  const complementaryForm = useMemo(
    () => {
      const cropSelectForm = {
        'trigo':
          <InputPercentage
            name="ph"
            label="ph"
            maxLength={ 2 }
            maxValue={ 99 }
            unit="%"
            type="integer"
            fullWidth
          />,
        'cevada':
          <>
            <InputPercentage
              name="ph"
              label="ph"
              maxLength={ 2 }
              maxValue={ 99 }
              type="integer"
              fullWidth
            />
            <InputPercentage
              name="germinationProductivity"
              label={ t('germination') }
              type="integer"
              fullWidth
            />
          </>,
        'aveia':
          <InputPercentage
            name="ph"
            label="ph"
            maxLength={ 2 }
            maxValue={ 99 }
            type="integer"
            fullWidth
          />,
        'arroz':
          <>
            <InputPercentage
              name="germinationProductivity"
              label={ t('germination') }
              type="integer"
              fullWidth
            />
            <InputPercentage
              name="wholeGrain"
              label={ t('whole grain') }
              type="integer"
              fullWidth
            />
            <InputPercentage
              name="shellAndBran"
              label={ t('shell and bran') }
              type="integer"
              fullWidth
            />
          </>
      }

      return cropSelectForm[lowerCase(crop.description)]
    },
    [crop.description, t]
  )

  return (
    <>
      <InputSelect
        label={ t('unit') }
        name="unitProductivity"
        options={ unitProductivity }
        onChange={ handleUnitsChange }
        defaultValue="kg/ha"
      />

      { unit && (
        inputProductivity
      )}

      { crop.description && (
        complementaryForm
      )}

    </>
  )
})

export default HarvestForm
