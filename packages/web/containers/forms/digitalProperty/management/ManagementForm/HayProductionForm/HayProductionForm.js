import React, { forwardRef } from 'react'

import { useT } from '@smartcoop/i18n'
import InputNumber from '@smartcoop/web-components/InputNumber'
import InputUnit from '@smartcoop/web-components/InputUnit'

const HayProductionForm = forwardRef(() => {
  const t = useT()

  return (
    <>
      <InputNumber
        name="balesRollsPerHa"
        label={ t('number of bales/rolls per ha') }
        maxLength={ 3 }
        type="integer"
        fullWidth
      />

      <InputUnit
        name="balesWeightRollsPerHa"
        label={ t('Bale / hay roll weight') }
        maxLength={ 3 }
        unit="kg/ha"
        type="integer"
        fullWidth
      />
    </>
  )
})

export default HayProductionForm
