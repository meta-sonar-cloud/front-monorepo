import React, { useCallback, forwardRef, useState, useMemo, useEffect } from 'react'
import { useDispatch } from 'react-redux'

import PropTypes from 'prop-types'

import omitBy from 'lodash/omitBy'


import managementSchemaRegister from '@smartcoop/forms/schemas/management/managementRegister.schema'
import I18n, { useT } from '@smartcoop/i18n'
import { getOperations, getOperation } from '@smartcoop/services/apis/smartcoopApi/resources/operations'
import { FieldActions } from '@smartcoop/stores/field'
import Button from '@smartcoop/web-components/Button'
import Form from '@smartcoop/web-components/Form'
import InputDate from '@smartcoop/web-components/InputDate'
import InputSelect from '@smartcoop/web-components/InputSelect'
import InputText from '@smartcoop/web-components/InputText'
import FertilizationForm from '@smartcoop/web-containers/forms/digitalProperty/management/ManagementForm/FertilizationForm'
import GrazingForm from '@smartcoop/web-containers/forms/digitalProperty/management/ManagementForm/GrazingForm'
import HarvestForm from '@smartcoop/web-containers/forms/digitalProperty/management/ManagementForm/HarvestForm'
import HayProductionForm from '@smartcoop/web-containers/forms/digitalProperty/management/ManagementForm/HayProductionForm'
import IrrigationForm from '@smartcoop/web-containers/forms/digitalProperty/management/ManagementForm/IrrigationForm'
import OthersForm from '@smartcoop/web-containers/forms/digitalProperty/management/ManagementForm/OthersForm'
import PhytosanitaryApplicationForm from '@smartcoop/web-containers/forms/digitalProperty/management/ManagementForm/PhytosanitaryApplicationForm/PhytosanitaryApplicationForm'
import SeedingForm from '@smartcoop/web-containers/forms/digitalProperty/management/ManagementForm/SeedingForm'
import SilageProductionForm from '@smartcoop/web-containers/forms/digitalProperty/management/ManagementForm/SilageProductionForm'

import { Container, ButtonContainer } from './styles'

const ManagementForm = forwardRef((props, formRef) => {
  const { withoutSubmitButton, onSuccess, style } = props
  const dispatch = useCallback(useDispatch(), [])
  const t = useT()
  const [operationId, setOperationId] = useState('')
  const [currentOperation, setcurrentOperation] = useState({})

  const handleSubmit = useCallback(
    (data) => {
      // Precisa remover o 'productGroupSeeding' das keysDontHaveInItems, pois é um campo que deve ser exibido no frot
      // essa medida foi tomada temporariamente para a exibição, fazendo o que ele não seja mostrado no front, evitando o 'bug'
      const keysDontHaveInItems = ['observations', 'operationId', 'predictedDate']
      const items = omitBy(data, (item, key) => keysDontHaveInItems.includes(key))
      dispatch(FieldActions.saveOfflineCropManagement(
        {
          operationSlug: currentOperation.slug,
          predictedDate: data.predictedDate,
          observations: data.observations,
          items
        }
      ))
      onSuccess()
    },
    [currentOperation.slug, dispatch, onSuccess]
  )

  const handleOperationChange = useCallback(
    (selectedItem) => {
      setOperationId(selectedItem.target.value)
    },
    []
  )

  useEffect(() => {
    const getOperationById = async () => {
      setcurrentOperation(await getOperation(operationId))
    }
    getOperationById()

  }, [operationId])


  const operationSelected = useMemo(
    () => ({
      'semeadura': <SeedingForm />,
      'fertilizacao': <FertilizationForm />,
      'aplicacao-fitossanitaria': <PhytosanitaryApplicationForm />,
      'colheita': <HarvestForm />,
      'irrigacao': <IrrigationForm />,
      'producao-de-feno': <HayProductionForm />,
      'pastejo': <GrazingForm />,
      'outros': <OthersForm />,
      'producao-de-silagem':<SilageProductionForm />
    }[currentOperation.slug]),
    [currentOperation.slug]
  )

  return (
    <Container>
      <Form
        style={ { ...style } }
        ref={ formRef }
        schemaConstructor={ managementSchemaRegister }
        onSubmit={ handleSubmit }
      >

        <InputDate
          name="predictedDate"
          label={ t('select one date') }
          fullWidth
        />

        <InputSelect
          label={ t('operation') }
          name="operationId"
          options={ getOperations }
          onChange={ handleOperationChange }
          asyncOptionLabelField="name"
        />

        {operationSelected}

        <InputText
          name="observations"
          label={ t('observation', { howMany: 2 }) }
          fullWidth
          multiline
          rows={ 5 }
        />

        {!withoutSubmitButton && (
          <ButtonContainer>
            <Button
              id="web-property-identification-form-button"
              style={ { width: '48%' } }
              onClick={ () => formRef.current.submit() }
            >
              <I18n>next</I18n>
            </Button>
          </ButtonContainer>
        )}
      </Form>
    </Container>
  )
})

ManagementForm.propTypes = {
  onSuccess: PropTypes.func,
  withoutSubmitButton: PropTypes.bool,
  style: PropTypes.object
}

ManagementForm.defaultProps = {
  onSuccess: () => {},
  withoutSubmitButton: false,
  style: {}
}

export default ManagementForm
