import React, { forwardRef, useMemo, useState, useCallback } from 'react'
import { useSelector } from 'react-redux'

import isEmpty from 'lodash/isEmpty'

import { useT } from '@smartcoop/i18n'
import { getProducts as getProductsService } from '@smartcoop/services/apis/smartcoopApi/resources/product'
import { getProductGroups as getProductGroupsService } from '@smartcoop/services/apis/smartcoopApi/resources/productGroup'
import { selectCurrentField } from '@smartcoop/stores/field/selectorField'
import InputSelect from '@smartcoop/web-components/InputSelect'
import InputUnit from '@smartcoop/web-components/InputUnit'
import RadioGroup from '@smartcoop/web-components/RadioGroup'

const SeedingForm = forwardRef(() => {
  const [productGroupSlug, setProductGroupSlug] = useState('')
  const t = useT()

  const field = useSelector(selectCurrentField)

  const { growingSeason } = field

  const selectOptions = useMemo(
    () => (
      [
        {
          label: t('yes'),
          value: 'Sim'
        },
        {
          label: t('no'),
          value: 'Não'
        }
      ]
    ),
    [t]
  )


  const handleProductGroup = useCallback(
    (selectedItem) => {
      setProductGroupSlug(selectedItem.target.value)
    },
    []
  )


  const productQueryParams = useMemo(
    () => (!isEmpty(productGroupSlug) ? { productGroupId: productGroupSlug } : {}),
    [productGroupSlug]
  )

  return (
    <>
      <InputSelect
        label={ t('productGroup') }
        name="productGroupSeeding"
        options={ getProductGroupsService }
        queryParams={ { cropId: growingSeason.cropId } }
        onChange={ handleProductGroup }
        asyncOptionLabelField="name"
      />

      <InputSelect
        label={ t('cultivate') }
        name="cultivate"
        options={ getProductsService }
        queryParams={ productQueryParams }
        asyncOptionLabelField="shortDescription"
        asyncOptionValueField="shortDescription"
        disabled={ !productGroupSlug }
      />


      <InputUnit
        maxLength={ 5 }
        label={ t('number of plants per linear meter') }
        name="numberOfPlants"
        type="float"
        decimalScale={ 1 }
        fullWidth
      />

      <InputUnit
        name="lineSpacing"
        label={ t('line spacing') }
        maxLength={ 2 }
        unit="cm"
        type="integer"
        fullWidth
      />

      <RadioGroup
        style={ { marginBottom: 10 } }
        label={ t('replanting?') }
        name="replanting"
        options={ selectOptions }
        variant="row"
      />

      <InputUnit
        name="germination"
        label={ t('germination') }
        maxLength={ 3 }
        max={ 100 }
        unit="%"
        type="integer"
        fullWidth
      />

      <InputUnit
        name="vigor"
        label={ t('vigor') }
        maxLength={ 3 }
        max={ 100 }
        unit="%"
        type="integer"
        fullWidth
      />
    </>
  )
})

export default SeedingForm
