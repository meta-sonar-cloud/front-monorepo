import React, { forwardRef } from 'react'

import { useT } from '@smartcoop/i18n'
import InputUnit from '@smartcoop/web-components/InputUnit'

const SilageProductionForm = forwardRef(() => {
  const t = useT()

  return (
    <>
      <InputUnit
        name="productionSilage"
        label={ t('production') }
        maxLength={ 4 }
        unit="t/ha"
        decimalScale={ 1 }
        type="float"
      />
    </>
  )
})

export default SilageProductionForm
