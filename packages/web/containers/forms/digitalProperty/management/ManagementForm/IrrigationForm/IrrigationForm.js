import React, { forwardRef, useMemo } from 'react'

import { useT } from '@smartcoop/i18n'
import InputUnit from '@smartcoop/web-components/InputUnit'
import RadioGroup from '@smartcoop/web-components/RadioGroup'

const IrrigationForm = forwardRef(() => {
  const t = useT()

  const typesOfIrrigation = useMemo(
    () => (
      [
        {
          label: t('superficial'),
          value: t('superficial')
        },
        {
          label: t('sprinkler'),
          value: t('sprinkler')
        },
        {
          label: t('center pivot'),
          value: t('center pivot')
        }
      ]
    ),
    [t]
  )

  return (
    <>
      <InputUnit
        name="irrigationPlates"
        label={ t('irrigation plates') }
        maxLength={ 3 }
        unit="mm"
        type="integer"
        fullWidth
      />

      <RadioGroup
        style={ { marginBottom: 10 } }
        label={ t('type') }
        name="typeOfIrrigation"
        options={ typesOfIrrigation }
        variant="row"
      />
    </>
  )
})

export default IrrigationForm
