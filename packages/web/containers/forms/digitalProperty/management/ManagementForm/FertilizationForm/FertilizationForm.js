import React, { forwardRef, useMemo, useState, useCallback, useEffect } from 'react'

import isEmpty from 'lodash/isEmpty'

import { useT } from '@smartcoop/i18n'
import { getProducts as getProductsService } from '@smartcoop/services/apis/smartcoopApi/resources/product'
import { getProductGroups as getProductGroupsService } from '@smartcoop/services/apis/smartcoopApi/resources/productGroup'
import InputPercentage from '@smartcoop/web-components/InputPercentage'
import InputSelect from '@smartcoop/web-components/InputSelect'
import InputUnit from '@smartcoop/web-components/InputUnit'
import RadioGroup from '@smartcoop/web-components/RadioGroup'

const FertilizationForm = forwardRef(() => {
  const [product, setProduct] = useState('')
  const t = useT()
  const [productGroup, setProductGroup] = useState({})

  const handleProductChange = useCallback(
    (selectedItem) => {
      setProduct(selectedItem.target.value)
    },
    []
  )

  const typeOfApplicationOptions = useMemo(
    () => (
      [
        {
          label: t('base'),
          value: 'base'
        },
        {
          label: t('coverage'),
          value: 'cobertura'
        }
      ]
    ),
    [t]
  )

  const applicationModeOptions = useMemo(
    () => (
      [
        {
          label: t('line'),
          value: 'linha'
        },
        {
          label: t('haul'),
          value: 'lanço'
        }
      ]
    ),
    [t]
  )

  const comboConcentrations = useMemo(
    () => {
      if(product === 'formulado') {
        return (
          <>
            <InputPercentage
              name="n"
              label="N"
              type="integer"
              fullWidth
            />

            <InputPercentage
              name="p"
              label="P"
              type="integer"
              fullWidth
            />

            <InputPercentage
              name="k"
              label="K"
              type="integer"
              fullWidth
            />
          </>
        )
      }
      return <></>
    },
    [product]
  )

  const unitsOptions = useMemo(
    () => (
      [
        {
          label: 'kg/ha',
          value: 'kg/ha'
        },
        {
          label: 'mg/ha',
          value: 'mg/ha'
        },
        {
          label: 'l/ha',
          value: 'l/ha'
        },
        {
          label: 'ml/ha',
          value: 'ml/ha'
        },
        {
          label: 'ton/ha',
          value: 'ton/ha'
        }
      ]
    ),
    []
  )

  const productGroupId = useMemo(
    () => (
      productGroup.data?.data.find(item => (
        item.slug === 'fertilizante'
      ))
    )?.id
    ,[productGroup]
  )

  const productQueryParams = useMemo(
    () => (!isEmpty(productGroupId) ? { productGroupId } : {}),
    [productGroupId]
  )

  useEffect(() => {
    (async () => {
      setProductGroup(await getProductGroupsService({ slug: 'fertilizante' }))
    })()
  }, [])

  return (
    <>
      {!isEmpty(productGroup) && (
        <>
          <InputSelect
            label={ t('product') }
            name="product"
            options={ getProductsService }
            queryParams={ productQueryParams }
            onChange={ handleProductChange }
            asyncOptionLabelField="shortDescription"
            asyncOptionValueField="slug"
          />

          <div style={ { display: 'flex' } }>
            <div style={ { flex: 1, marginRight: 10 } }>
              <InputUnit
                name="dose"
                label={ t('dose') }
                maxLength={ 5 }
                type="integer"
                fullWidth
              />
            </div>
            <div>
              <InputSelect
                label={ t('unit') }
                name="unit"
                options={ unitsOptions }
              />
            </div>
          </div>

          {comboConcentrations}

          <RadioGroup
            style={ { marginBottom: 10 } }
            label={ t('application mode') }
            name="applicationMode"
            options={ applicationModeOptions }
            variant="row"
          />

          <RadioGroup
            style={ { marginBottom: 10 } }
            label={ t('type of application') }
            name="typeOfApplication"
            options={ typeOfApplicationOptions }
            variant="row"
          />
        </>
      )}
    </>
  )
})

export default FertilizationForm
