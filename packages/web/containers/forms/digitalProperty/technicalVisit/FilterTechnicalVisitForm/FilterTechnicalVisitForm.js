import React, { useCallback, forwardRef, useState, useMemo, useEffect } from 'react'

import PropTypes from 'prop-types'

import filterTechnicalVisit from '@smartcoop/forms/schemas/property/filterTechnicalVisit.schema'
import I18n, { useT } from '@smartcoop/i18n'
import {
  getFieldsByPropertyId as getFieldsByPropertyIdService
} from '@smartcoop/services/apis/smartcoopApi/resources/field'
import {
  getPropertiesByOwner as getPropertiesByOwnerService
} from '@smartcoop/services/apis/smartcoopApi/resources/property'
import {
  getTechnicalVisitUsersList as getTechnicalVisitUsersListService
} from '@smartcoop/services/apis/smartcoopApi/resources/technical'
import Button from '@smartcoop/web-components/Button'
import Form from '@smartcoop/web-components/Form'
import InputDateRange from '@smartcoop/web-components/InputDateRange'
import InputSelect from '@smartcoop/web-components/InputSelect'

import { Container, ButtonContainer } from './styles'

const FilterTechnicalVisitForm = forwardRef((props, formRef) => {
  const { withoutSubmitButton, loading, onSubmit, filters } = props
  const [proprietaryId, setProprietaryId] = useState('')
  const [propertyId, setPropertyId] = useState('')
  const [fieldId, setFieldId] = useState('')
  const t = useT()

  const handleSubmit = useCallback(
    (data) => onSubmit(data),
    [onSubmit]
  )

  const depsProperty = useMemo(
    () => [proprietaryId],
    [proprietaryId]
  )

  const depsField = useMemo(
    () => [propertyId],
    [propertyId]
  )

  const urlParamsProperty = useMemo(
    () => ({ ownerId: proprietaryId }),
    [proprietaryId]
  )

  const urlParamsField = useMemo(
    () => ({ propertyId }),
    [propertyId]
  )

  const propertySelectField = useMemo(
    () => formRef.current?.getFieldRef('propertyId'),
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [formRef, propertyId]
  )

  const fieldSelectField = useMemo(
    () => formRef.current?.getFieldRef('fieldId'),
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [formRef, fieldId]
  )

  const loadFieldOptions = useCallback(
    async (...params) => {
      const result = await getFieldsByPropertyIdService(...params)
      return {
        data: {
          data: result
        }
      }
    },
    []
  )

  useEffect(() => {
    if(!proprietaryId){
      formRef.current.reset()
    }
  }, [formRef, proprietaryId])

  useEffect(() => {
    if(!propertyId && propertySelectField && fieldSelectField){
      propertySelectField.resetField()
      fieldSelectField.resetField()
    }
  }, [fieldSelectField, propertyId, formRef, propertySelectField])

  useEffect(() => {
    if((!fieldId || !propertyId) && fieldSelectField){
      fieldSelectField.resetField()
    }
  }, [fieldId, fieldSelectField, formRef, propertyId])

  useEffect(() => {
    if(filters.proprietary){
      setProprietaryId(filters.proprietary)
    }

    if(filters.propertyId){
      setPropertyId(filters.propertyId)
    }

    if(filters.fieldId){
      setFieldId(filters.fieldId)
    }
  }, [filters])

  return (
    <Container>
      <Form
        style={ { display: 'flex', flexDirection: 'column', width: '100%' } }
        ref={ formRef }
        schemaConstructor={ filterTechnicalVisit }
        onSubmit={ handleSubmit }
      >
        <InputSelect
          label={ t('producer', { gender: 'male', howMany: 1 }) }
          name="proprietary"
          options={ getTechnicalVisitUsersListService }
          onChange={ ({ target }) => setProprietaryId(target.value) }
          defaultValue={ filters.proprietary }
          asyncOptionLabelField="name"
        />
        <InputSelect
          label={ t('property', { howMany: 1 }) }
          name="propertyId"
          options={ getPropertiesByOwnerService }
          onChange={ ({ target }) => setPropertyId(target.value) }
          urlParams={ urlParamsProperty }
          asyncOptionLabelField="name"
          defaultValue={ filters.propertyId }
          deps={ depsProperty }
          loaderEnable={ !!proprietaryId }
          disabled={ !proprietaryId }
        />
        <InputSelect
          label={ t('field', { howMany: 1 }) }
          name="fieldId"
          options={ loadFieldOptions }
          urlParams={ urlParamsField }
          asyncOptionLabelField="fieldName"
          onChange={ ({ target }) => setFieldId(target.value) }
          deps={ depsField }
          defaultValue={ filters.fieldId }
          loaderEnable={ !!propertyId }
          disabled={ !propertyId }
        />
        <div style={ { paddingBottom: '20px' } }>
          <InputDateRange
            label={ t('period') }
            name="rangeDate"
            defaultValue={ { from: filters.startDate, to: filters.endDate } || { from: undefined, to: undefined } }
          />
        </div>

        {!withoutSubmitButton && (
          <ButtonContainer>
            <Button
              id="web-order-filter-form-button"
              style={ { width: '48%' } }
              onClick={ () => formRef.current.submit() }
              disabled={ loading }
            >
              <I18n>filter</I18n>
            </Button>
          </ButtonContainer>
        )}
      </Form>
    </Container>
  )
})

FilterTechnicalVisitForm.propTypes = {
  filters: PropTypes.object.isRequired,
  loading: PropTypes.bool,
  onSubmit: PropTypes.func,
  withoutSubmitButton: PropTypes.bool
}

FilterTechnicalVisitForm.defaultProps = {
  loading: false,
  onSubmit: () => {},
  withoutSubmitButton: false
}

export default FilterTechnicalVisitForm
