import React, { useCallback, forwardRef, useMemo, useState } from 'react'
import { useDispatch } from 'react-redux'

import moment from 'moment'
import PropTypes from 'prop-types'

import isEmpty from 'lodash/isEmpty'
import omitBy from 'lodash/omitBy'

import { useDialog } from '@smartcoop/dialog'
import TechnicalVisit from '@smartcoop/forms/schemas/property/technicalVisit.schema'
import I18n, { useT } from '@smartcoop/i18n'
import { TechnicalActions } from '@smartcoop/stores/technical'
import Button from '@smartcoop/web-components/Button'
import Form from '@smartcoop/web-components/Form'
import InputDate from '@smartcoop/web-components/InputDate'
import InputHour from '@smartcoop/web-components/InputHour'
import InputNumber from '@smartcoop/web-components/InputNumber'
import InputSelect from '@smartcoop/web-components/InputSelect'
import InputText from '@smartcoop/web-components/InputText'
import ConfirmModal from '@smartcoop/web-components/Modal/ConfirmModal'
import { ButtonsContainer } from '@smartcoop/web-containers/layouts/AuthenticatedLayout/theme'

import { Container, TextDate } from './styles'

const TechnicalVisitForm = forwardRef((props, formRef) => {
  const { style, cultivateName, technicalVisit, disabled, onSuccess, handleClose } = props
  const dispatch = useCallback(useDispatch(), [])
  const [ isEditing, setIsEditing] = useState(disabled)
  const { createDialog } = useDialog()
  const t = useT()


  const handleSubmitRegisterTechnicalVisit = useCallback(
    (data) => {
      const items = omitBy(data, (item) => item === '')
      if(!items.productivityEstimate || !items.estimateUnity) {
        createDialog({
          id: 'confirm-register-technical-visit',
          Component: ConfirmModal,
          props: {
            onConfirm: () => {
              dispatch(TechnicalActions.saveTechnicalVisit(
                {
                  ...items,
                  id: technicalVisit.id || undefined,
                  cropId: technicalVisit.cropId,
                  growingSeasonId: technicalVisit.growingSeasonId,
                  visitDateTime: moment(`${ items.dateVisit } ${ items.hour }`, 'YYYY-MM-DD HH:mm').format('YYYY-MM-DD HH:mm')
                }
              ))
              onSuccess()
              handleClose()
            },
            textWarning: t('you have empty fields, do you want to register like this?')
          }
        })
      } else {
        dispatch(TechnicalActions.saveTechnicalVisit(
          {
            ...items,
            id: technicalVisit.id || undefined,
            cropId: technicalVisit.cropId,
            growingSeasonId: technicalVisit.growingSeasonId,
            visitDateTime: moment(`${ items.dateVisit } ${ items.hour }`, 'YYYY-MM-DD HH:mm').format('YYYY-MM-DD HH:mm')
          }
        ))
        onSuccess()
        handleClose()
      }
    },
    [createDialog, dispatch, handleClose, onSuccess, t, technicalVisit.cropId, technicalVisit.growingSeasonId, technicalVisit.id]
  )

  const dateAndTime = useMemo(
    () => ({
      'hour': moment(`${ technicalVisit.visitDateTime }`, 'YYYY-MM-DD HH:mm').format('HH:mm'),
      'date': moment(`${ technicalVisit.visitDateTime }`, 'YYYY-MM-DD HH:mm').format('YYYY-MM-DD')
    }),
    [technicalVisit.visitDateTime]
  )

  const unitProductivity = useMemo(
    () => (
      [
        {
          label: 'kg/ha',
          value: 'kg/ha'
        },
        {
          label: 'sacas/ha',
          value: 'sacas/ha'
        }
      ]
    ),
    []
  )

  return (
    <Container>
      <Form
        style={ { ...style } }
        ref={ formRef }
        schemaConstructor={ TechnicalVisit }
        onSubmit={ handleSubmitRegisterTechnicalVisit }
      >
        <div style={ { display: 'flex' } }>
          <div style={ { marginRight: 10, flex: 1 } }>
            <TextDate>
              <I18n>data of field</I18n>
            </TextDate>
            <InputText
              name='cultivate'
              label='cultivo'
              value={ cultivateName || technicalVisit?.crop?.description }
              disabled
              detached
              fullWidth
            />
            <div style={ { display: 'flex' } }>
              <InputDate
                name="dateVisit"
                label="Data"
                style={ { marginRight: 5, flex: 1 } }
                defaultValue={ dateAndTime?.date }
                disabled={ isEditing }
              />
              <InputHour
                name="hour"
                label={ t('hour') }
                style={ { maxWidth: 120 } }
                defaultValue={ dateAndTime ? dateAndTime?.hour : '' }
                disabled={ isEditing }
              />
            </div>
          </div>
          <div style={ { flex: 1 } }>
            <TextDate>
              <I18n>productivity estimate</I18n>
            </TextDate>
            <InputSelect
              label={ t('unit') }
              name="estimateUnity"
              options={ unitProductivity }
              defaultValue={ technicalVisit?.estimateUnity }
              disabled={ isEditing }
              fullWidth
            />
            <InputNumber
              maxLength={ 5 }
              label={ t('productivity') }
              name="productivityEstimate"
              disabled={ isEditing }
              defaultValue={ `${ technicalVisit?.productivityEstimate }` }
              fullWidth
            />
          </div>
        </div>
        <InputText
          name="observations"
          label={ t('observation', { howMany: 2 }) }
          fullWidth
          multiline
          defaultValue={ technicalVisit?.observations }
          disabled={ isEditing }
          rows={ 5 }
        />

        <ButtonsContainer>
          <Button
            id="cancel-technical-visit-form"
            onClick={ handleClose }
            style={ { flex: 1, marginRight: 10 } }
            variant="outlined"
          >
            <I18n>cancel</I18n>
          </Button>

          {!isEmpty(technicalVisit) && isEditing && (
            <Button
              id="edit-technical-visit-form"
              onClick={ () => setIsEditing(!isEditing) }
              style={ { flex: 1, marginRight: 10 } }
              color="secondary"
            >
              <I18n>edit</I18n>
            </Button>
          )}

          {!isEditing && (
            <Button
              id="submit-technical-visit-form"
              onClick={ () => formRef.current.submit() }
              style={ { flex: 1 } }
            >
              <I18n>save</I18n>
            </Button>
          )}
        </ButtonsContainer>
      </Form>
    </Container>
  )
})

TechnicalVisitForm.propTypes = {
  style: PropTypes.object,
  handleClose: PropTypes.func,
  cultivateName: PropTypes.string,
  technicalVisit: PropTypes.object,
  disabled: PropTypes.bool,
  onSuccess: PropTypes.func
}

TechnicalVisitForm.defaultProps = {
  style: {},
  handleClose: () => {},
  cultivateName: '',
  technicalVisit: {},
  disabled: false,
  onSuccess: () => {}
}

export default TechnicalVisitForm
