import React, { useCallback, forwardRef, useState, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'

import moment from 'moment/moment'
import PropTypes from 'prop-types'

import Grid from '@material-ui/core/Grid'

import registerPregnancyActionsSchema from '@smartcoop/forms/schemas/dairyFarm/registerPregnancyActions.schema'
import I18n, { useT } from '@smartcoop/i18n'
import { useSnackbar } from '@smartcoop/snackbar'
import { selectCurrentAnimal } from '@smartcoop/stores/animal/selectorAnimal'
import { LotActions } from '@smartcoop/stores/lot'
import {
  selectLotsOptions,
  selectPreDefinedLots
} from '@smartcoop/stores/lot/selectorLot'
import { selectCurrentProperty } from '@smartcoop/stores/property/selectorProperty'
import { colors } from '@smartcoop/styles'
import { momentBackDateFormat } from '@smartcoop/utils/dates'
import Button from '@smartcoop/web-components/Button'
import Form from '@smartcoop/web-components/Form'
import InputDate from '@smartcoop/web-components/InputDate'
import InputSelect from '@smartcoop/web-components/InputSelect'
import InputText from '@smartcoop/web-components/InputText'

import {
  Container,
  FormContainer,
  ButtonsContainer,
  ButtonContainer,
  Item
} from './styles'

const RegisterPregnancyActionsForm = forwardRef((props, formRef) => {
  const {
    withoutSubmitButton,
    defaultValues,
    loading,
    onSubmit,
    onCancel
  } = props

  const t = useT()
  const snackbar = useSnackbar()
  const dispatch = useCallback(useDispatch(), [])

  const [disableLot, setDisableLot] = useState(false)
  const [statusDoesNotMatchWithType, setStatusDoesNotMatchWithType] = useState(
    false
  )

  const lotsOptions = useSelector(selectLotsOptions)
  const preDefinedLots = useSelector(selectPreDefinedLots)
  const currentAnimal = useSelector(selectCurrentAnimal)
  const { id: propertyId } = useSelector(selectCurrentProperty)

  useEffect(() => {
    dispatch(
      LotActions.loadLots(
        { propertyId },
        () => {},
        (err) => {
          snackbar.error(t(err.message))
        }
      )
    )
  }, [dispatch, propertyId, snackbar, t])

  useEffect(() => {
    const { type } = defaultValues
    if (type !== 'aborto') {
      setDisableLot(true)
    }
  }, [defaultValues])

  const handleSubmit = useCallback(
    (data) => {
      const { earring, ...rest } = data
      onSubmit(rest)
    },
    [onSubmit]
  )

  const handleTypeChange = (value) => {
    switch (value) {
      case 'aborto':
        setDisableLot(false)
        formRef.current.getFieldRef('lotId').setValue('')
        setStatusDoesNotMatchWithType(
          currentAnimal?.animalStatus.name !== 'Prenha'
        )
        break
      case 'pre-parto':
        setDisableLot(true)
        formRef.current
          .getFieldRef('lotId')
          .setValue(preDefinedLots?.prepartum?.value)
        setStatusDoesNotMatchWithType(
          currentAnimal?.animalStatus?.name !== 'Prenha'
        )
        break
      case 'secagem':
        setDisableLot(true)
        formRef.current
          .getFieldRef('lotId')
          .setValue(preDefinedLots?.drying?.value)
        setStatusDoesNotMatchWithType(
          currentAnimal?.animalStatus.name !== 'Prenha' ||
            currentAnimal?.category !== 'vaca'
        )
        break
      default:
    }
  }

  return (
    <Container>
      <Form
        style={ { display: 'flex', flexDirection: 'column', width: '100%' } }
        ref={ formRef }
        schemaConstructor={ registerPregnancyActionsSchema }
        onSubmit={ handleSubmit }
      >
        <FormContainer>
          <Grid container style={ { justifyContent: 'space-between' } }>
            <Item>
              <InputText
                style={ { display: 'none' } }
                name="animalId"
                fullWidth
                defaultValue={ defaultValues?.animalId }
                disabled
              />
              <InputText
                label={ t('earring') }
                name="earring"
                fullWidth
                defaultValue={ defaultValues?.earring }
                disabled
              />
            </Item>
            <Item>
              <InputSelect
                label={ t('type') }
                name="type"
                options={ [
                  {
                    label: t('drying'),
                    value: 'secagem'
                  },
                  {
                    label: t('prepartum'),
                    value: 'pre-parto'
                  },
                  {
                    label: t('abortion'),
                    value: 'aborto'
                  }
                ] }
                onChange={ ({ target: { value } }) => handleTypeChange(value) }
                defaultValue={ defaultValues?.type }
              />
            </Item>
            <Item>
              <InputSelect
                label={ t('lot') }
                name="lotId"
                options={ lotsOptions }
                defaultValue={ defaultValues?.animal?.lot?.id }
                disabled={ disableLot }
              />
            </Item>
            <Item>
              <InputDate
                label={ t('accomplished date') }
                name="accomplishedDate"
                pickerProps={ {
                  maxDate: moment().format()
                } }
                fullWidth
                defaultValue={
                  defaultValues?.accomplishedDate
                    ? moment(
                      defaultValues?.accomplishedDate,
                      momentBackDateFormat
                    )
                    : moment()
                }
              />
            </Item>
            {statusDoesNotMatchWithType && (
              <Item style={ { marginBottom: 15 } }>
                <I18n style={ { color: colors.red } }>
                  invalid animal status or category
                </I18n>
              </Item>
            )}
          </Grid>
        </FormContainer>

        {!withoutSubmitButton && (
          <ButtonsContainer>
            <ButtonContainer>
              <Button
                id="web-cancel-form-button"
                onClick={ onCancel }
                color={ colors.white }
                disabled={ loading }
              >
                <I18n>cancel</I18n>
              </Button>
            </ButtonContainer>
            <ButtonContainer>
              <Button
                id="web-save-form-button"
                onClick={ () => formRef.current.submit() }
                disabled={ loading || statusDoesNotMatchWithType }
              >
                <I18n>register</I18n>
              </Button>
            </ButtonContainer>
          </ButtonsContainer>
        )}
      </Form>
    </Container>
  )
})

RegisterPregnancyActionsForm.propTypes = {
  loading: PropTypes.bool,
  onSubmit: PropTypes.func,
  onCancel: PropTypes.func,
  withoutSubmitButton: PropTypes.bool,
  defaultValues: PropTypes.object.isRequired
}

RegisterPregnancyActionsForm.defaultProps = {
  loading: false,
  onSubmit: () => {},
  onCancel: () => {},
  withoutSubmitButton: false
}

export default RegisterPregnancyActionsForm
