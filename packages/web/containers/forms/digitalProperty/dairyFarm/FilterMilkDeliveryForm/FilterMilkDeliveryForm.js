import React, { useCallback, forwardRef, useMemo } from 'react'

import PropTypes from 'prop-types'

import filterMilkDeliverySchema from '@smartcoop/forms/schemas/dairyFarm/filterMilkDelivery.schema'
import I18n, { useT } from '@smartcoop/i18n'
import Button from '@smartcoop/web-components/Button'
import Form from '@smartcoop/web-components/Form'
import InputDate from '@smartcoop/web-components/InputDate'
import InputSelect from '@smartcoop/web-components/InputSelect'

import { Container, ButtonContainer } from './styles'

const FilterMilkDeliveryForm = forwardRef((props, formRef) => {
  const { withoutSubmitButton, loading, onSubmit, filters } = props

  const t = useT()

  const handleSubmit = useCallback(
    (data) => onSubmit(data),
    [onSubmit]
  )

  const statusOptions = useMemo(
    () => [
      {
        label: t('manual'),
        value: 'manual'
      },
      {
        label: t('definitive'),
        value: 'definitive'
      },
      {
        label: t('depending on approval'),
        value: 'depending-on-approval'
      }
    ],
    [t]
  )

  return (
    <Container>
      <Form
        style={ { display: 'flex', flexDirection: 'column', width: '100%' } }
        ref={ formRef }
        schemaConstructor={ filterMilkDeliverySchema }
        onSubmit={ handleSubmit }
      >

        <InputDate
          label="Data Volume"
          name="volumeDate"
          fullWidth
          defaultValue={ filters.volumeDate }
        />
        <InputSelect
          label="Status"
          name="status"
          options={ statusOptions }
          style={ { paddingBottom: 10 } }
          defaultValue={ filters.status }
          clearable="true"
        />

        {!withoutSubmitButton && (
          <ButtonContainer>
            <Button
              id="web-order-filter-form-button"
              style={ { width: '48%' } }
              onClick={ () => formRef.current.submit() }
              disabled={ loading }
            >
              <I18n>filter</I18n>
            </Button>
          </ButtonContainer>
        )}
      </Form>
    </Container>
  )
})

FilterMilkDeliveryForm.propTypes = {
  filters: PropTypes.object,
  loading: PropTypes.bool,
  onSubmit: PropTypes.func,
  withoutSubmitButton: PropTypes.bool
}

FilterMilkDeliveryForm.defaultProps = {
  filters: {},
  loading: false,
  onSubmit: () => {},
  withoutSubmitButton: false
}

export default FilterMilkDeliveryForm
