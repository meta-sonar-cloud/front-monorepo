import React, { useCallback, forwardRef, useMemo, useState, useEffect } from 'react'
import { useDispatch  } from 'react-redux'

import PropTypes from 'prop-types'

import { isEmpty } from 'lodash'

import milkDeliverySchema from '@smartcoop/forms/schemas/dairyFarm/milkDelivery.schema'
import I18n,  { useT } from '@smartcoop/i18n'
import { getOrganizationsByUser as getOrganizationsByUserService } from '@smartcoop/services/apis/smartcoopApi/resources/organization'
import { useSnackbar } from '@smartcoop/snackbar'
import { DairyFarmActions } from '@smartcoop/stores/dairyFarm'
import Button from '@smartcoop/web-components/Button'
import Form from '@smartcoop/web-components/Form'
import InputSelect from '@smartcoop/web-components/InputSelect'
import InputText from '@smartcoop/web-components/InputText'
import Loader from '@smartcoop/web-components/Loader'
import RadioGroup from '@smartcoop/web-components/RadioGroup'
import { ButtonsContainer } from '@smartcoop/web-containers/layouts/AuthenticatedLayout/theme'

import { Container } from './styles'

const MilkDeliveryForm = forwardRef((props, formRef) => {
  const { handleClose, defaultValues, clearForm } = props

  const t = useT()

  const dispatch = useCallback(useDispatch(), [])

  const [tamboType, setTamboType] = useState(defaultValues.isCooperative ? 'cooperative' : 'third party')
  const [textInputValue, setTextInputValue] = useState('')
  const [selectInputValue, setSelectInputValue] = useState('')
  const [loading, setLoading] = useState(false)

  const snackbar = useSnackbar()

  const onSuccess = useCallback(
    () => {
      snackbar.success(
        t('your {this} was registered', {
          howMany: 1,
          gender: 'male',
          this: t('dairy farm', { howMany: 1 })
        })
      )
      handleClose()
    },
    [handleClose, snackbar, t]
  )

  const handleSubmit = useCallback(
    (data) => {
      setLoading(true)
      dispatch(DairyFarmActions.saveTamboType(
        data,
        () => {
          onSuccess()
          setLoading(false)
        },
        () => {
          setLoading(false)
        }
      ))
    },
    [dispatch, onSuccess]
  )

  const modalOptions = useMemo(
    () => (
      [
        {
          label: t('cooperative', { howMany: 1 }),
          value: 'cooperative'
        },
        {
          label: t('others'),
          value: 'third party'
        }
      ]
    ),
    [t]
  )

  const isDisabled = useMemo(
    () => tamboType === 'third party' ? isEmpty(textInputValue) : isEmpty(selectInputValue),
    [selectInputValue, tamboType, textInputValue]
  )

  useEffect(() => {
    setSelectInputValue(defaultValues?.organizationId)
    setTextInputValue(defaultValues?.companyName)
  }, [defaultValues])

  const stateRadioGroup = useMemo(
    () => (
      tamboType === 'third party' ?
        <InputText
          name="companyName"
          label={ t('enter the company name') }
          defaultValue={ defaultValues.companyName }
          onChange={ ({ target }) => setTextInputValue(target.value) }
        />
        :
        <InputSelect
          label={ t('select one organization') }
          name="organizationId"
          asyncOptionValueField='id'
          asyncOptionLabelField='tradeName'
          options={ getOrganizationsByUserService }
          queryParams={ { type: 1, isSubsidiary: false } }
          style={ { paddingBottom: 10 } }
          clearable="true"
          defaultValue={ defaultValues.organizationId }
          onChange={ ({ target }) => setSelectInputValue(target.value) }
        />
    ),
    [defaultValues, t, tamboType]
  )
  return loading ? <Loader width={ 100 } /> : (
    <Container>
      <Form
        style={ { display: 'flex', flexDirection: 'column', width: '100%' } }
        ref={ formRef }
        schemaConstructor={ milkDeliverySchema }
        onSubmit={ handleSubmit }
      >
        <RadioGroup
          onChange={ (e) => setTamboType(e.target?.value) }
          label=''
          variant="row"
          name="tamboType"
          options={ modalOptions }
          style={ { paddingBottom: 20 } }
          defaultValue={ tamboType }
        />
        {stateRadioGroup}
        <ButtonsContainer>
          <Button
            id="clear"
            onClick={ () => clearForm() }
            variant="outlined"
            style={ { marginRight: 7 } }
          >
            <I18n>cancel</I18n>
          </Button>
          <Button
            id="save"
            onClick={ () => formRef.current.submit() }
            color="black"
            style={ { marginLeft: 7 } }
            disabled={ isDisabled }
          >
            <I18n>save</I18n>
          </Button>
        </ButtonsContainer>
      </Form>
    </Container>
  )
})
MilkDeliveryForm.propTypes = {
  handleClose: PropTypes.func.isRequired,
  defaultValues: PropTypes.object.isRequired,
  clearForm: PropTypes.func
}

MilkDeliveryForm.defaultProps = {
  clearForm: () => {}
}
export default MilkDeliveryForm
