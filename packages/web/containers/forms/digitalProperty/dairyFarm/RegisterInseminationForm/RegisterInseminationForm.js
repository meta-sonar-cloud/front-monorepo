import React, {
  useState,
  useMemo,
  useEffect,
  useCallback,
  forwardRef
} from 'react'
import { useSelector } from 'react-redux'

import moment from 'moment'
import PropTypes from 'prop-types'

import { includes, isEmpty, map } from 'lodash'

import Grid from '@material-ui/core/Grid'

import registerInseminationSchema from '@smartcoop/forms/schemas/dairyFarm/registerInsemination.schema'
import I18n, { useT } from '@smartcoop/i18n'
import { getAnimals as getAnimalsService } from '@smartcoop/services/apis/smartcoopApi/resources/animal'
import { getBulls as getBullsService } from '@smartcoop/services/apis/smartcoopApi/resources/animalBulls'
import { selectCurrentProperty } from '@smartcoop/stores/property/selectorProperty'
import { colors } from '@smartcoop/styles'
import Button from '@smartcoop/web-components/Button'
import Form from '@smartcoop/web-components/Form'
import InputDate from '@smartcoop/web-components/InputDate'
import InputSelect from '@smartcoop/web-components/InputSelect'
import InputText from '@smartcoop/web-components/InputText'

import {
  Container,
  ButtonsContainer,
  ButtonContainer,
  FormContainer,
  Item,
  Label
} from './styles'

const RegisterInseminationForm = forwardRef((props, formRef) => {
  const { defaultValues, onSubmit, onCancel, loading } = props

  const [typeValue, setTypeValue] = useState([])
  const currentProperty = useSelector(selectCurrentProperty)

  const defaultOptions = useMemo(
    () => [
      {
        label: 'IATF',
        value: 'IATF'
      },
      {
        label: 'CIO Visual',
        value: 'CIO Visual'
      },
      {
        label: 'CIO monitorado',
        value: 'CIO monitorado'
      },
      {
        label: 'TE',
        value: 'TE'
      },
      {
        label: 'Monta',
        value: 'Monta'
      }
    ],
    []
  )
  const [options, setOptions] = useState(defaultOptions)

  const t = useT()

  const handleSubmit = useCallback(
    (data) => {
      onSubmit({
        ...data,
        inseminationType: map(typeValue, (v) => ({ name: v, id: 0 })),
        bullId: !isEmpty(data.bullId) ? data.bullId : null,
        embryoBull: !isEmpty(data.embryoBull) ? data.embryoBull : null,
        embryoMother: !isEmpty(data.embryoMother) ? data.embryoMother : null,
        internal: includes(typeValue, 'Monta')
      })
    },
    [onSubmit, typeValue]
  )

  const isMonta = useMemo(() => includes(typeValue, 'Monta'), [typeValue])

  const isTypeTe = useMemo(() => includes(typeValue, 'TE'), [typeValue])

  const handleSetTypeValue = useCallback((values) => {
    setTypeValue(values)
  }, [])

  useEffect(() => {
    if (isEmpty(typeValue)) {
      setOptions(defaultOptions)
    }

    if (includes(typeValue, 'Monta')) {
      setOptions([
        {
          label: 'Monta',
          value: 'Monta'
        }
      ])
    }

    if (includes(typeValue, 'TE')) {
      setOptions([
        {
          label: 'TE',
          value: 'TE'
        }
      ])
    }

    if (
      includes(typeValue, 'IATF') ||
      includes(typeValue, 'CIO monitorado') ||
      includes(typeValue, 'CIO Visual')
    ) {
      setOptions([
        {
          label: 'IATF',
          value: 'IATF'
        },
        {
          label: 'CIO Visual',
          value: 'CIO Visual'
        },
        {
          label: 'CIO monitorado',
          value: 'CIO monitorado'
        }
      ])
    }
  }, [defaultOptions, typeValue])

  const service = useMemo(
    () => (includes(typeValue, 'Monta') ? getAnimalsService : getBullsService),
    [typeValue]
  )

  const urlParams = useMemo(
    () => ({
      propertyId: currentProperty.id
    }),
    [currentProperty.id]
  )

  const queryParams = useMemo(() => {
    if (isMonta) {
      return { category: 'touro' }
    }
    return {}
  }, [isMonta])

  const selectBull = useMemo(
    () => (
      <InputSelect
        name="bullId"
        label={ t('bull\'s name') }
        options={ service }
        urlParams={ urlParams }
        queryParams={ queryParams }
        asyncOptionLabelField="nameAndCode"
        defaultValue={ defaultValues.bullId || defaultValues.internalBullId }
        key={ options }
      />
    ),
    [defaultValues, options, queryParams, service, t, urlParams]
  )

  useEffect(() => {
    if (!isEmpty(defaultValues?.inseminationType)) {
      setTypeValue(map(defaultValues.inseminationType, (i) => i.name))
    }
  }, [defaultValues])

  return (
    <Container>
      <Form
        style={ { display: 'flex', flexDirection: 'column', width: '100%' } }
        ref={ formRef }
        schemaConstructor={ registerInseminationSchema }
        onSubmit={ handleSubmit }
      >
        <FormContainer>
          <Grid container style={ { justifyContent: 'space-between' } }>
            <Item>
              <InputDate
                label={ t('insemination date') }
                name="inseminationDate"
                fullWidth
                defaultValue={ defaultValues.inseminationDate }
                pickerProps={ {
                  maxDate: moment().format()
                } }
              />
            </Item>
            <Item>
              <InputSelect
                label={ t('type') }
                placeholder={ t('type') }
                name="inseminationType"
                options={ options }
                fullWidth
                defaultValue={ defaultValues.inseminationType }
                multiple
                detached
                value={ typeValue }
                onChange={ handleSetTypeValue }
              />
            </Item>
            <Item>{!isTypeTe && selectBull}</Item>
            {!isMonta && (
              <Item>
                <InputText
                  label={ t('inseminator') }
                  name="inseminator"
                  placeholder={ t('inseminator') }
                  defaultValue={ defaultValues.inseminator }
                  fullWidth
                />
              </Item>
            )}
            {isTypeTe && (
              <>
                <I18n as={ Label }>embryo&apos;s data</I18n>
                <Item>
                  <InputText
                    label={ t('father') }
                    name="embryoBull"
                    fullWidth
                    placeholder={ t('father') }
                    defaultValue={ defaultValues.embryoBull }
                  />
                </Item>
                <Item>
                  <InputText
                    label={ t('mother') }
                    name="embryoMother"
                    fullWidth
                    placeholder={ t('mother') }
                    defaultValue={ defaultValues.embryoMother }
                  />
                </Item>
              </>
            )}
          </Grid>
        </FormContainer>
        <ButtonsContainer>
          <ButtonContainer>
            <Button
              id="web-cancel-form-button"
              onClick={ onCancel }
              color={ colors.white }
              disabled={ loading }
            >
              <I18n>cancel</I18n>
            </Button>
          </ButtonContainer>
          <ButtonContainer>
            <Button
              id="web-save-form-button"
              onClick={ () => formRef.current.submit() }
              disabled={ loading || isEmpty(typeValue) }
            >
              <I18n>save</I18n>
            </Button>
          </ButtonContainer>
        </ButtonsContainer>
      </Form>
    </Container>
  )
})

RegisterInseminationForm.propTypes = {
  loading: PropTypes.bool,
  onSubmit: PropTypes.func,
  onCancel: PropTypes.func,
  defaultValues: PropTypes.object.isRequired
}

RegisterInseminationForm.defaultProps = {
  loading: false,
  onSubmit: () => {},
  onCancel: () => {}
}

export default RegisterInseminationForm
