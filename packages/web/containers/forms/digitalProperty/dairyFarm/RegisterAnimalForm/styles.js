import styled from 'styled-components'

import Card from '@smartcoop/web-components/Card'

export const Container = styled(Card).attrs({
  cardStyle: {
    width: '100%',
    padding: 15,
    maxWidth: 1024
  },
  style: {
    width: '100%',
    alignItems: 'flex-start'
  }
})``

export const ButtonContainer = styled.div`
  display: flex;
  justify-content: flex-end;
  align-items: flex-end;
  width: 100%;
`
export const InputContainer = styled.div`
  display: flex;
  justify-content: space-between;
`

export const GridContainerAnimal = styled.div`
  display: grid;
  grid-template-columns: repeat(2, 1fr);
  grid-gap: 15px;
`
