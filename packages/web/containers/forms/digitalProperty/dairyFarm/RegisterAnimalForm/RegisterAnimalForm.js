import React, {
  useCallback,
  forwardRef,
  useRef,
  useMemo,
  useState
} from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { useHistory } from 'react-router-dom'

import moment from 'moment/moment'

import { find } from 'lodash'
import isEmpty from 'lodash/isEmpty'
import lowerCase from 'lodash/lowerCase'
import omitBy from 'lodash/omitBy'
import toNumber from 'lodash/toNumber'

import { useDialog } from '@smartcoop/dialog'
import registerAnimal from '@smartcoop/forms/schemas/dairyFarm/registerAnimal.schema'
import registerNewAnimal from '@smartcoop/forms/schemas/dairyFarm/registerNewAnimal.schema'
import I18n, { useT } from '@smartcoop/i18n'
import { getAnimals as getAnimalsService } from '@smartcoop/services/apis/smartcoopApi/resources/animal'
import { getAnimalBreeds as getAnimalBreedsService } from '@smartcoop/services/apis/smartcoopApi/resources/animalBreed'
import { getBulls as getBullsService } from '@smartcoop/services/apis/smartcoopApi/resources/animalBulls'
import { getAnimalReasons as getAnimalReasonsService } from '@smartcoop/services/apis/smartcoopApi/resources/animalReason'
import { getAnimalStatus as getAnimalStatusService } from '@smartcoop/services/apis/smartcoopApi/resources/animalStatus'
import { getLots as getLotsService } from '@smartcoop/services/apis/smartcoopApi/resources/lot'
import { useSnackbar } from '@smartcoop/snackbar'
import { AnimalActions } from '@smartcoop/stores/animal'
import {
  selectCurrentAnimal,
  selectIsDetail
} from '@smartcoop/stores/animal/selectorAnimal'
import { DairyFarmActions } from '@smartcoop/stores/dairyFarm'
import { selectLotsOptions } from '@smartcoop/stores/lot/selectorLot'
import { selectCurrentProperty } from '@smartcoop/stores/property/selectorProperty'
import {
  SLUG_ANIMALS_STATUS,
  TYPE_ANIMALS,
  ANIMALS_CATEGORY
} from '@smartcoop/utils/constants'
import { momentBackDateFormat } from '@smartcoop/utils/dates'
import Button from '@smartcoop/web-components/Button'
import Form from '@smartcoop/web-components/Form'
import InputDate from '@smartcoop/web-components/InputDate'
import InputSelect from '@smartcoop/web-components/InputSelect'
import InputText from '@smartcoop/web-components/InputText'
import LoadingModal from '@smartcoop/web-containers/modals/LoadingModal'

import { Container, ButtonContainer, GridContainerAnimal } from './styles'

const RegisterAnimalForm = forwardRef(() => {
  const t = useT()
  const history = useHistory()
  const snackbar = useSnackbar()
  const registerFormRef = useRef(null)
  const dispatch = useCallback(useDispatch(), [])

  const isDetail = useSelector(selectIsDetail)
  const lotsOptions = useSelector(selectLotsOptions)
  const currentAnimal = useSelector(selectCurrentAnimal)
  const currentProperty = useSelector(selectCurrentProperty)
  const { id: propertyId } = useSelector(selectCurrentProperty)

  const { createDialog, removeDialog } = useDialog()

  const [slaughterDate, setSlaughterDate] = useState(
    currentAnimal?.slaughterDate || ''
  )

  const isNewAnimal = useMemo(() => {
    if (currentAnimal.isNewAnimal) {
      return true
    }
    return false
  }, [currentAnimal.isNewAnimal])

  const onSuccess = useCallback(() => {
    snackbar.success(
      t(
        currentAnimal?.id
          ? 'your {this} was edited'
          : 'your {this} was registered',
        {
          howMany: 1,
          gender: 'male',
          this: t('animal', { howMany: 1 })
        }
      )
    )
    dispatch(DairyFarmActions.setCurrentSection('cattleManagement'))
    history.goBack()
  }, [currentAnimal, dispatch, history, snackbar, t])

  const handleSubmit = useCallback(() => {
    createDialog({
      id: 'loading',
      Component: LoadingModal,
      props: {
        message: t('saving your {this}', {
          howMany: 1,
          gender: 'male',
          this: t('animal', { howMany: 1 })
        })
      }
    })

    const removeLoader = () => {
      setTimeout(() => removeDialog({ id: 'loading' }), 150)
    }

    const newData = omitBy(
      registerFormRef.current.getData(),
      (item) => item === ''
    )
    dispatch(
      AnimalActions.saveAnimal(
        {
          fatherTable: TYPE_ANIMALS.bulls,
          maternalGrandfatherTable: TYPE_ANIMALS.bulls,
          maternalGreatGrandfatherTable: TYPE_ANIMALS.bulls,
          ...currentAnimal,
          ...newData
        },
        () => {
          removeLoader()
          onSuccess()
        },
        removeLoader
      )
    )
  }, [createDialog, currentAnimal, dispatch, onSuccess, removeDialog, t])

  const optionsCategory = useMemo(
    () => [
      {
        label: 'Touro',
        value: 'touro'
      },
      {
        label: 'Boi',
        value: 'boi'
      },
      {
        label: 'Terneira',
        value: 'terneira'
      },
      {
        label: 'Novilha',
        value: 'novilha'
      },
      {
        label: 'Vaca',
        value: 'vaca'
      }
    ],
    []
  )

  const optionsMotivation = useMemo(
    () => [
      {
        label: 'Descarte',
        value: 'descarte'
      },
      {
        label: 'Morte',
        value: 'morte'
      },
      {
        label: 'Venda',
        value: 'venda'
      }
    ],
    []
  )

  const queryParamsStatus = useMemo(
    () =>
      !isEmpty(currentAnimal)
        ? {}
        : {
          slug: [
            SLUG_ANIMALS_STATUS.nenhum,
            SLUG_ANIMALS_STATUS.aptas,
            SLUG_ANIMALS_STATUS.vazia
          ]
        },
    [currentAnimal]
  )

  const defaultLot = useMemo(() => {
    if (currentAnimal.isNewAnimal) {
      const terneiraLot = find(lotsOptions, (item) => item.label === 'Terneira')

      return terneiraLot.value
    }
    return currentAnimal?.lot?.id
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [currentAnimal, lotsOptions])

  return (
    <Container>
      <Form
        style={ { display: 'flex', flexDirection: 'column', width: '100%' } }
        ref={ registerFormRef }
        schemaConstructor={ isNewAnimal ? registerNewAnimal : registerAnimal }
        onSubmit={ handleSubmit }
      >
        <GridContainerAnimal>
          <InputText
            label={ t('{this} name', {
              gender: 'male',
              this: t('animal', { howMany: 1 })
            }) }
            style={ { marginBottom: 0 } }
            name="name"
            defaultValue={ currentAnimal?.name }
            disabled={ isDetail }
          />

          <InputText
            label={ t('earring') }
            name="earringCode"
            style={ { marginBottom: 0 } }
            defaultValue={ currentAnimal?.earring?.earringCode }
            disabled={ isDetail }
          />
          <InputSelect
            label={ t('category') }
            name="category"
            options={ optionsCategory }
            style={ { marginBottom: 0 } }
            defaultValue={ lowerCase(currentAnimal?.category) }
            disabled={ isDetail }
          />

          <InputDate
            label={ t('date of birth') }
            name="birthDate"
            style={ { marginBottom: 0 } }
            pickerProps={ {
              maxDate: moment().format()
            } }
            defaultValue={
              currentAnimal?.birthDate
                ? moment(currentAnimal?.birthDate, momentBackDateFormat).format(
                  momentBackDateFormat
                )
                : ''
            }
            disabled={ isDetail || currentAnimal.isNewAnimal }
            fullWidth
          />

          <InputSelect
            label={ t('lot') }
            name="lotId"
            style={ { marginBottom: 0 } }
            options={ getLotsService }
            urlParams={ { propertyId } }
            defaultValue={ defaultLot }
            disabled={ isDetail }
          />

          <InputSelect
            label={ t('predominant race') }
            name="predominantBreedId"
            options={ getAnimalBreedsService }
            style={ { marginBottom: 0 } }
            defaultValue={ currentAnimal?.predominantBreed?.id }
            disabled={ isDetail }
          />

          <InputSelect
            label={ t('status') }
            name="statusId"
            style={ { marginBottom: 0 } }
            options={ getAnimalStatusService }
            queryParams={ queryParamsStatus }
            required
            defaultValue={ currentAnimal?.animalStatus?.id && toNumber(currentAnimal?.animalStatus?.id) }
            disabled={
              (!currentAnimal.isNewAnimal &&
                (isDetail || !isEmpty(currentAnimal))) ||
              currentAnimal.isNewAnimal
            }
          />

          {!currentAnimal.isNewAnimal && (
            <InputDate
              label={ t('date of discharge') }
              name="slaughterDate"
              style={ { marginBottom: 0 } }
              pickerProps={ {
                maxDate: moment().format()
              } }
              defaultValue={
                currentAnimal?.slaughterDate
                  ? moment(currentAnimal?.slaughterDate, momentBackDateFormat)
                  : ''
              }
              disabled={ isDetail }
              onChange={ setSlaughterDate }
              fullWidth
            />
          )}

          {!isEmpty(slaughterDate) && (
            <>
              <InputSelect
                name="motivation"
                label={ t('select the reason for the discharge') }
                style={ { marginBottom: 0 } }
                options={ optionsMotivation }
                defaultValue={ currentAnimal?.motivation }
                disabled={ isDetail }
                fullWidth
              />

              <InputSelect
                label={ t('reason') }
                name="slaughterReasonId"
                options={ getAnimalReasonsService }
                style={ { marginBottom: 0 } }
                asyncOptionLabelField="description"
                defaultValue={ currentAnimal?.slaughterReason?.id }
                disabled={ isDetail }
              />
            </>
          )}

          {!currentAnimal.isNewAnimal && (
            <>
              <InputSelect
                label={ t('{this} code', {
                  this: t('father')
                }) }
                name="fatherCode"
                style={ { marginBottom: 0 } }
                options={ getBullsService }
                asyncOptionLabelField="nameAndCode"
                asyncOptionValueField="id"
                disabled={ isDetail }
                defaultValue={ currentAnimal?.fatherBull?.id }
              />
              <InputSelect
                label={ t('{this} code', {
                  this: t('mother')
                }) }
                name="motherEarringId"
                style={ { marginBottom: 0 } }
                options={ getAnimalsService }
                asyncOptionLabelField="nameAndEarring"
                asyncOptionValueField="earring.id"
                urlParams={ { propertyId: currentProperty?.id } }
                queryParams={ { category: ANIMALS_CATEGORY.vaca } }
                disabled={ isDetail }
                defaultValue={ currentAnimal?.motherEarring?.id }
              />
              <InputSelect
                label={ t('{this} code', {
                  this: t('maternal grandfather')
                }) }
                name="maternalGrandfatherCode"
                style={ { marginBottom: 0 } }
                options={ getBullsService }
                asyncOptionLabelField="nameAndCode"
                disabled={ isDetail }
                defaultValue={ currentAnimal?.grandfatherBull?.id }
              />
              <InputSelect
                label={ t('{this} code', {
                  this: t('great grandmother')
                }) }
                name="maternalGreatGrandfatherCode"
                style={ { marginBottom: 0 } }
                options={ getBullsService }
                asyncOptionLabelField="nameAndCode"
                disabled={ isDetail }
                defaultValue={ currentAnimal?.greatGrandfatherBull?.id }
              />
            </>
          )}
        </GridContainerAnimal>
        <ButtonContainer>
          {!isEmpty(currentAnimal) && (
            <div>
              <Button
                id="edit-animal"
                variant="outlined"
                style={ { marginTop: 30, marginRight: 10 } }
                onClick={ () => dispatch(AnimalActions.setIsDetail(!isDetail)) }
              >
                <I18n>{isDetail ? 'edit' : 'cancel'}</I18n>
              </Button>
            </div>
          )}
          {!isDetail && (
            <div>
              <Button
                id="register-animal"
                style={ { marginTop: 30 } }
                color="secondary"
                onClick={ () => registerFormRef.current.submit() }
              >
                <I18n>complete registration</I18n>
              </Button>
            </div>
          )}
        </ButtonContainer>
      </Form>
    </Container>
  )
})

export default RegisterAnimalForm
