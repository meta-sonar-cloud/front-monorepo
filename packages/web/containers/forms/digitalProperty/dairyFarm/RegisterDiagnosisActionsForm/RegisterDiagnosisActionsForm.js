import React, { useCallback, forwardRef } from 'react'
import { useSelector } from 'react-redux'

import moment from 'moment/moment'
import PropTypes from 'prop-types'

import Grid from '@material-ui/core/Grid'

import registerDiagnosisActionsSchema from '@smartcoop/forms/schemas/dairyFarm/registerDiagnosisActions.schema'
import I18n, { useT } from '@smartcoop/i18n'
import { getAnimalPregnancyDiagnosticsTypes } from '@smartcoop/services/apis/smartcoopApi/resources/animalPregnancyDiagnostics'
import { selectCurrentAnimal } from '@smartcoop/stores/animal/selectorAnimal'
import { selectCurrentInsemination } from '@smartcoop/stores/insemination/selectorInsemination'
import { colors } from '@smartcoop/styles'
import Button from '@smartcoop/web-components/Button'
import Form from '@smartcoop/web-components/Form'
import InputDate from '@smartcoop/web-components/InputDate'
import InputSelect from '@smartcoop/web-components/InputSelect'
import InputText from '@smartcoop/web-components/InputText'

import {
  Container,
  FormContainer,
  ButtonsContainer,
  ButtonContainer,
  Item
} from './styles'

const RegisterDiagnosisActionsForm = forwardRef((props, formRef) => {
  const {
    withoutSubmitButton,
    defaultValues,
    loading,
    onSubmit,
    onCancel
  } = props

  const t = useT()
  const currentInsemination = useSelector(selectCurrentInsemination)

  const currentAnimal = useSelector(selectCurrentAnimal)

  const handleSubmit = useCallback(
    (data) => {
      const { earring, ...rest } = data
      onSubmit({ ...rest })
    },
    [onSubmit]
  )

  return (
    <Container>
      <Form
        style={ { display: 'flex', flexDirection: 'column', width: '100%' } }
        ref={ formRef }
        schemaConstructor={ registerDiagnosisActionsSchema }
        onSubmit={ handleSubmit }
      >
        <FormContainer>
          <Grid container style={ { justifyContent: 'space-between' } }>
            <Item>
              <InputText
                style={ { display: 'none' } }
                name="animalId"
                fullWidth
                defaultValue={ defaultValues?.animalId }
                disabled
              />
              <InputText
                label={ t('earring') }
                name="earring"
                fullWidth
                defaultValue={ defaultValues?.earring }
                disabled
              />
            </Item>
            <Item>
              <InputSelect
                label={ t('type of diagnosis') }
                name="diagnosisTypeId"
                options={ getAnimalPregnancyDiagnosticsTypes }
                urlParams={ { animalId: currentAnimal.id } }
                asyncOptionLabelField="typeName"
                asyncOptionValueField="id"
                defaultValue={ defaultValues.diagnosisTypeId }
              />
            </Item>
            <Item>
              <InputDate
                label={ t('accomplished date') }
                name="realizationDate"
                defaultValue={ defaultValues.realizationDate }
                pickerProps={ {
                  maxDate: moment().format(),
                  minDate: currentInsemination?.inseminationDate
                } }
                fullWidth
              />
            </Item>
            <Item>
              <InputSelect
                label={ t('result') }
                name="result"
                defaultValue={ defaultValues.result }
                options={ [
                  {
                    label: t('pregnant'),
                    value: 'Positivo'
                  },
                  {
                    label: t('empty'),
                    value: 'Vazia'
                  },
                  {
                    label: t('inconclusive'),
                    value: 'Inconclusivo'
                  }
                ] }
              />
            </Item>
          </Grid>
        </FormContainer>

        {!withoutSubmitButton && (
          <ButtonsContainer>
            <ButtonContainer>
              <Button
                id="web-cancel-form-button"
                onClick={ onCancel }
                color={ colors.white }
                disabled={ loading }
              >
                <I18n>cancel</I18n>
              </Button>
            </ButtonContainer>
            <ButtonContainer>
              <Button
                id="web-save-form-button"
                onClick={ () => formRef.current.submit() }
                disabled={ loading }
              >
                <I18n>register</I18n>
              </Button>
            </ButtonContainer>
          </ButtonsContainer>
        )}
      </Form>
    </Container>
  )
})

RegisterDiagnosisActionsForm.propTypes = {
  loading: PropTypes.bool,
  onSubmit: PropTypes.func,
  onCancel: PropTypes.func,
  withoutSubmitButton: PropTypes.bool,
  defaultValues: PropTypes.object.isRequired
}

RegisterDiagnosisActionsForm.defaultProps = {
  loading: false,
  onSubmit: () => {},
  onCancel: () => {},
  withoutSubmitButton: false
}

export default RegisterDiagnosisActionsForm
