import React, { useCallback } from 'react'

import PropTypes from 'prop-types'

import  { useT } from '@smartcoop/i18n'
import { useSnackbar } from '@smartcoop/snackbar'
import { convertFileSize, isValidSize } from '@smartcoop/utils/files'
import Button from '@smartcoop/web-components/Button'

import useStyles from './styles'

const InputFile = (props) => {
  const {
    idInput,
    maxFileSize,
    maxNumberFile,
    onChange,
    inputProps,
    buttonProps,
    children
  } = props

  const classes = useStyles()
  const snackbar = useSnackbar()
  const t = useT()

  const handleChange = useCallback(
    (event) => {
      const { files } = event.target

      if (files.length > maxNumberFile) {
        snackbar.error(
          t('the number of files exceeds the limit of {this}', {
            this: maxNumberFile
          })
        )
      } else {
        for (const file of files) {
          if (!isValidSize(file, maxFileSize)) {
            snackbar.error(
              t('the size of one or more files exceeds {this}', {
                this: convertFileSize(maxFileSize)
              })
            )
            return
          }
        }
        onChange(event)
      }
    },
    [maxFileSize, maxNumberFile, onChange, snackbar, t]
  )

  return (
    <label htmlFor={ idInput }>
      <input
        className={ classes.input }
        id={ idInput }
        type="file"
        onChange={ handleChange }
        { ...inputProps }
      />
      <Button
        component="span"
        { ...buttonProps }
      >
        {children}
      </Button>
    </label>
  )
}

InputFile.propTypes = {
  idInput: PropTypes.string.isRequired,
  maxNumberFile: PropTypes.number,
  maxFileSize: PropTypes.number,
  onChange: PropTypes.func,
  inputProps: PropTypes.object,
  buttonProps: PropTypes.object,
  children: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.element
  ])
}

InputFile.defaultProps = {
  onChange: () => {},
  maxNumberFile: 20,
  maxFileSize: 5242880,
  inputProps: {},
  buttonProps: {},
  children: null
}

export default InputFile
