import React from 'react'

import PropTypes from 'prop-types'

import isEmpty from 'lodash/isEmpty'

import CircularProgress from '@material-ui/core/CircularProgress'

import { useStyles } from './styles'

function CircularLoader(props) {
  const {
    label,
    marginTop,
    style,
    className,
    fullSpace,
    ...otherProps
  } = props

  const classes = useStyles()

  const loader = (
    <div className={ classes.labeledLoaderContainer }>
      <CircularProgress
        className={ [classes.progress, className].join(' ') }
        style={ {
          ...style,
          marginTop: marginTop || undefined
        } }
        { ...otherProps }
      />
      {!isEmpty(label) && <span className={ classes.label }>{label}</span>}
    </div>
  )

  if (fullSpace) {
    return (
      <div className={ classes.container }>
        {loader}
      </div>
    )
  }

  return loader
}

CircularLoader.propTypes = {
  size: PropTypes.number,
  label: PropTypes.string,
  marginTop: PropTypes.number,
  style: PropTypes.object,
  className: PropTypes.string,
  fullSpace: PropTypes.bool,
  variant: PropTypes.oneOf(['determinate', 'indeterminate', 'static'])
}

CircularLoader.defaultProps = {
  size: 40,
  label: '',
  marginTop: 30,
  style: {},
  className: null,
  fullSpace: false,
  variant: 'indeterminate'
}

export default CircularLoader
