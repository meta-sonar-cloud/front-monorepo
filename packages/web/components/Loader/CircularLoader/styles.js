import { makeStyles } from '@material-ui/core/styles'

export const useStyles = makeStyles(() => ({
  container: {
    display: 'flex',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  progress: {
    // margin: theme.spacing.unit * 2
    marginTop: 30
    // marginLeft: 15
  },
  labeledLoaderContainer: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center'
  },
  label: {
    padding: '10px 0'
  }
}))
