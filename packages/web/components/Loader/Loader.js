import AnimatedLoader from './AnimatedLoader/AnimatedLoader'
import CircularLoader from './CircularLoader/CircularLoader'
import LinearLoader from './LinearLoader/LinearLoader'

export { LinearLoader, CircularLoader }
export default AnimatedLoader
