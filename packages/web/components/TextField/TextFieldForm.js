import React, {
  forwardRef,
  useEffect,
  useState,
  useCallback,
  useMemo,
  useImperativeHandle
} from 'react'

import PropTypes from 'prop-types'
import uuid from 'short-uuid'

import { FieldProvider, useField } from '@smartcoop/forms'

import TextFieldStyled from './TextFieldStyled'

const TextFieldForm = forwardRef((props, externalRef) => {
  const {
    defaultValue,
    zIndex,
    onFieldValueChange,
    transformValue,
    validateOnBlur,
    ...rest
  } = props

  const [value, setValue] = useState(defaultValue)
  const [mounted, setMounted] = useState(false)

  const {
    fieldName,
    mask,
    error,
    fieldRef,
    handleChangeNative,
    handleBlur,
    required,
    resetField,
    externalOnChange,
    validateField
  } = useField()

  const id = useMemo(
    () => `${ fieldName }-${ uuid().new() }`,
    [fieldName]
  )

  const handleChange = useCallback(
    (event) => {
      setValue(event.target.value)
    },
    []
  )

  useEffect(() => {
    const newValue = transformValue(value)
    onFieldValueChange(newValue)
    if (mounted) {
      handleChangeNative(newValue)
    } else {
      setMounted(true)
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [value])

  const imperativeHandles = useMemo(
    () => ({
      defaultValue,
      setValue,
      resetField,
      externalOnChange,
      validateField
    }),
    [defaultValue, externalOnChange, resetField, validateField]
  )

  useImperativeHandle(fieldRef, () => ({
    ...fieldRef.current,
    ...imperativeHandles
  }))

  useImperativeHandle(externalRef, () => ({
    ...fieldRef.current,
    ...imperativeHandles
  }))

  // TODO disabled input style
  return (
    <TextFieldStyled
      { ...rest }
      ref={ fieldRef }
      id={ id }
      value={ value }
      onChange={ handleChange }
      onBlur={ handleBlur }
      mask={ mask }
      error={ error }
      required={ required }
      validateOnBlur={ validateOnBlur }
    />
  )
})

TextFieldForm.propTypes = {
  defaultValue: PropTypes.any,
  transformValue: PropTypes.func,
  onFieldValueChange: PropTypes.func,
  zIndex: PropTypes.number,
  validateOnBlur: PropTypes.bool
}

TextFieldForm.defaultProps = {
  defaultValue: '',
  transformValue: v => v,
  onFieldValueChange: () => {},
  zIndex: undefined,
  validateOnBlur: true
}

const Field = forwardRef(({ path, ...props }, ref) => (
  <FieldProvider
    ref={ ref }
    { ...props }
    registerFieldOptions={ { path } }
    FieldComponent={ TextFieldForm }
  />
))

Field.propTypes = {
  name: PropTypes.string.isRequired,
  disabled: PropTypes.bool,
  onChange: PropTypes.func,
  onBlur: PropTypes.func,
  transformValue: PropTypes.func,
  onFieldValueChange: PropTypes.func,
  path: PropTypes.string,
  defaultValue: PropTypes.any,
  numberFormatProps: PropTypes.object,
  helperText: PropTypes.string,
  validateOnBlur: PropTypes.bool
}

Field.defaultProps = {
  disabled: false,
  onChange: () => {},
  transformValue: v => v,
  onFieldValueChange: () => {},
  onBlur: () => {},
  path: 'value',
  defaultValue: '',
  numberFormatProps: undefined,
  helperText: null,
  validateOnBlur: true
}

export default Field
