import React from 'react'

import TextField from '@smartcoop/web-components/TextField'

const InputText = (props) =><TextField { ...props } type="text" />

export default InputText
