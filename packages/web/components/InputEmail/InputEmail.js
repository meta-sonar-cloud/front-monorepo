import React from 'react'

import TextField from '@smartcoop/web-components/TextField'

const InputEmail = (props) => <TextField { ...props } type="email" />

export default InputEmail
