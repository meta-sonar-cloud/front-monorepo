import React, { useMemo, useState, useCallback } from 'react'

import moment from 'moment/moment'
import PropTypes from 'prop-types'

import map from 'lodash/map'
import size from 'lodash/size'

import { arrowRightSimple } from '@smartcoop/icons'
import { colors } from '@smartcoop/styles'
import { generateArrayOfDate } from '@smartcoop/utils/dates'
import Icon from '@smartcoop/web-components/Icon'
import IconButton from '@smartcoop/web-components/IconButton'

import { Container, Tab } from './styles'

const DateTabs = props => {
  const {
    endDate,
    initialDate,
    mode,
    format,
    currentYearFormat,
    onChange,
    tabsQuantityToBeShown,
    navigateQuantity,
    viewLastFirst,
    defaultValue,
    ...rest
  } = props

  const [selectedItem, setSelectedItem] = useState(defaultValue)

  const arrayOfDates = useMemo(
    () => generateArrayOfDate(initialDate, endDate, mode),
    [endDate, initialDate, mode]
  )

  const arraySize = useMemo(
    () => size(arrayOfDates),
    [arrayOfDates]
  )

  const generateNewIndex = useCallback(
    (oldIndex, displacement) => {
      let newIndex = oldIndex + displacement
      newIndex = newIndex > arraySize-tabsQuantityToBeShown ? arraySize-tabsQuantityToBeShown : newIndex
      return newIndex <= 0 ? 0 : newIndex
    },
    [arraySize, tabsQuantityToBeShown]
  )

  const [index, setIndex] = useState(viewLastFirst ? generateNewIndex(0, arraySize-tabsQuantityToBeShown) : 0)

  const dates = useMemo(
    () => arrayOfDates.slice(index, index + tabsQuantityToBeShown > arraySize ? arraySize : index + tabsQuantityToBeShown),
    [arrayOfDates, arraySize, index, tabsQuantityToBeShown]
  )

  const disabledLeftButton = useMemo(
    () => index <= 0,
    [index]
  )

  const disabledRightButton = useMemo(
    () => index + tabsQuantityToBeShown >= arraySize,
    [arraySize, index, tabsQuantityToBeShown]
  )

  const handleIndexChange = useCallback(
    (displacement) => {
      setIndex(generateNewIndex(index, displacement))
    },
    [generateNewIndex, index]
  )

  return (
    <Container { ...rest }>
      <IconButton
        onClick={ () => handleIndexChange(-navigateQuantity) }
        disabled={ disabledLeftButton }
      >
        <Icon size={ 16 } style={ { transform: 'rotate(180deg)' } } icon={ arrowRightSimple } color={ disabledLeftButton ? colors.grey : colors.black } />
      </IconButton>      {
        map(dates, (date) => (
          <div>
            <Tab
              onClick={ () => {setSelectedItem(date); onChange(date)} }
              active={ date.isSame(selectedItem, mode) }
              color={ date.isSame(selectedItem, mode) ? colors.blackLight : colors.backgroundHtml }
            >
              {/* Check if the date is in the actual year and change the exibition */}
              { moment().isSame(date, 'year') ? date.format(currentYearFormat || format) : date.format(format) }
            </Tab>
          </div>
        ))
      }
      <IconButton
        onClick={ () => handleIndexChange(navigateQuantity) }
        disabled={ disabledRightButton }
      >
        <Icon size={ 16 } icon={ arrowRightSimple } color={ disabledRightButton ? colors.grey : colors.black } />
      </IconButton>
    </Container>
  )
}

DateTabs.propTypes = {
  initialDate: PropTypes.object.isRequired,
  endDate: PropTypes.object,
  mode: PropTypes.oneOf(['day', 'month', 'year']),
  format: PropTypes.string,
  currentYearFormat: PropTypes.string,
  tabsQuantityToBeShown: PropTypes.number,
  navigateQuantity: PropTypes.number,
  viewLastFirst: PropTypes.bool,
  defaultValue: PropTypes.object,
  onChange: PropTypes.func
}

DateTabs.defaultProps = {
  endDate: moment(),
  mode: 'month',
  format: 'MMM/YY',
  currentYearFormat: null,
  tabsQuantityToBeShown: 10,
  navigateQuantity: 3,
  viewLastFirst: false,
  defaultValue: null,
  onChange: () => {}
}

export default DateTabs
