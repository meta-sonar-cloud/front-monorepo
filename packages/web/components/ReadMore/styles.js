import styled from 'styled-components'

export const Link = styled.span`
  cursor: pointer;
  text-decoration: underline;
`
