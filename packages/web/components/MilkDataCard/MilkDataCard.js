import React from 'react'

import PropTypes from 'prop-types'

import { colors } from '@smartcoop/styles'
import Icon from '@smartcoop/web-components/Icon'

import {
  Container,
  LeftSide,
  RightSide,
  Line,
  Unit,
  Title,
  Big
} from './styles'

const MilkDataCard = props => {
  const {
    title,
    measureUnit,
    color,
    icon,
    iconSize,
    children,
    style
  } = props
  return (
    <Container style={ style }>
      <LeftSide backgroundColor={ color }>
        <Icon
          icon={ icon }
          color={ color }
          size={ iconSize }
        />
      </LeftSide>
      <RightSide>
        <Line>
          <Title>{title}</Title>
          <Unit>{measureUnit}</Unit>
        </Line>
        <Big>
          {children}
        </Big>
      </RightSide>
    </Container>
  )
}

MilkDataCard.propTypes = {
  color: PropTypes.string,
  title: PropTypes.string,
  icon: PropTypes.any,
  children: PropTypes.any,
  iconSize: PropTypes.number,
  measureUnit: PropTypes.string,
  style: PropTypes.object
}

MilkDataCard.defaultProps = {
  color: colors.blue,
  children: null,
  icon: null,
  iconSize: 38,
  title: '-',
  measureUnit: '-',
  style: {}
}

export default MilkDataCard
