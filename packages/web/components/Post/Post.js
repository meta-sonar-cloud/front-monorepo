import React, { useRef, useCallback } from 'react'

import PropTypes from 'prop-types'

import { ThemeProvider } from '@material-ui/core'
import Divider from '@material-ui/core/Divider'

import postSchema from '@smartcoop/forms/schemas/social/post.schema'
import I18n, { useT } from '@smartcoop/i18n'
import { publicType } from '@smartcoop/icons'
import { getInterestAreas } from '@smartcoop/services/apis/smartcoopApi/resources/interestAreas'
import colors from '@smartcoop/styles/colors'
import Avatar from '@smartcoop/web-components/Avatar'
import Button from '@smartcoop/web-components/Button'
import Icon from '@smartcoop/web-components/Icon'
import InputSelect from '@smartcoop/web-components/InputSelect'
import InputText from '@smartcoop/web-components/InputText'

import { Container, Name, Header, Identifier, Body, Footer, textFieldTheme, Privacy, Col, Type, ColFooter2, ColFooter1 } from './styles'

const Post = ({ style, user, onSubmit, textFieldProps, buttonProps }) =>  {
  const t = useT()
  const formRef = useRef(null)

  const handleSubmit = useCallback(
    (data) => {
      formRef.current.reset()
      onSubmit(data)
    },
    [onSubmit]
  )

  return (
    <ThemeProvider theme={ textFieldTheme }>
      <Container
        style={ style }
        ref={ formRef }
        schemaConstructor={ postSchema }
        onSubmit={ handleSubmit }
      >
        <Header>
          <Identifier>
            <Avatar alt={ user.name } src={ user.image } />
            <Col>
              <Name>{user.name}</Name>
              <Privacy>
                <Icon icon={ publicType } size={ 14 } color={ colors.green } />
                <Type>{t('public')}</Type>
              </Privacy>
            </Col>
          </Identifier>
        </Header>
        <Body>

          <InputText
            placeholder={ t('what would you like to share with the network?').replace('\n', ' ') }
            name="text"
            fullWidth
            multiline
            rows={ 4 }
            style={ { marginBottom: 0 } }
            { ...textFieldProps }
          />
        </Body>
        <Divider style={ { margin: 0 } } />
        <Footer>
          <ColFooter1>
            <InputSelect
              multiple
              label={ t('add tags') }
              name="interestAreas"
              options={ getInterestAreas }
              style= { { marginBottom: 0 } }
            />
          </ColFooter1>
          <ColFooter2>
            <div>
              <Button
                id="button"
                color="secondary"
                onClick={ () => formRef.current.submit() }
                { ...buttonProps }
              >
                <I18n>publish</I18n>
              </Button>
            </div>
          </ColFooter2>
        </Footer>
      </Container>

    </ThemeProvider>
  )}

Post.propTypes = {
  style: PropTypes.object,
  user: PropTypes.shape({
    name: PropTypes.string,
    image: PropTypes.string }),
  onSubmit: PropTypes.func,
  textFieldProps: PropTypes.object,
  buttonProps: PropTypes.object
}

Post.defaultProps = {
  style: {},
  user: {
    name: '',
    image: ''
  },
  onSubmit: () => {},
  textFieldProps: {},
  buttonProps: {}
}

export default Post
