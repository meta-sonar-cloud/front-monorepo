import React from 'react'

import PropTypes from 'prop-types'

import { useT } from '@smartcoop/i18n'
import { expandMore } from '@smartcoop/icons'
import { colors } from '@smartcoop/styles'
import Icon from '@smartcoop/web-components/Icon'

import IconButton from '../IconButton'

const UserIconButton = ({ iconColor, ...props }) => {
  const t = useT()
  return (
    <IconButton tooltip={ t('retract') } { ...props }>
      <Icon icon={ expandMore } color={ iconColor } />
    </IconButton>
  )
}

UserIconButton.propTypes = {
  iconColor: PropTypes.string
}

UserIconButton.defaultProps = {
  iconColor: colors.mutedText
}

export default UserIconButton
