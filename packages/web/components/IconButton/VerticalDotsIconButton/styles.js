import { makeStyles } from '@material-ui/core/styles'

import { colors } from '@smartcoop/styles'

const hexToRgba = require('hex-to-rgba')

const useStyles = makeStyles(() => ({
  root: {
    borderRadius: 50,
    '&:hover': {
      backgroundColor: hexToRgba(colors.white, 0.1)
    }
  }
}))

export default useStyles
