import React from 'react'

import PropTypes from 'prop-types'

import { useT } from '@smartcoop/i18n'
import { previousPage } from '@smartcoop/icons'
import { colors } from '@smartcoop/styles'
import Icon from '@smartcoop/web-components/Icon'

import IconButton from '../IconButton'

const PreviousPageIconButton = ({ iconColor, ...props }) => {
  const t = useT()
  return (
    <IconButton tooltip={ t('datagrid pagination previous tooltip') } { ...props }>
      <Icon size={ 14 } icon={ previousPage } color={ iconColor } />
    </IconButton>
  )
}

PreviousPageIconButton.propTypes = {
  iconColor: PropTypes.string
}

PreviousPageIconButton.defaultProps = {
  iconColor: colors.mutedText
}

export default PreviousPageIconButton
