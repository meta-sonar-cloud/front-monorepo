import React from 'react'

import PropTypes from 'prop-types'

import { useT } from '@smartcoop/i18n'
import { arrowDown } from '@smartcoop/icons'
import { colors } from '@smartcoop/styles'
import Icon from '@smartcoop/web-components/Icon'

import IconButton from '../IconButton'

const ArrowDownIconButton = ({ iconColor, ...props }) => {
  const t = useT()
  return (
    <IconButton tooltip={ t('expand') } { ...props }>
      <Icon icon={ arrowDown } color={ iconColor } />
    </IconButton>
  )
}

ArrowDownIconButton.propTypes = {
  iconColor: PropTypes.string
}

ArrowDownIconButton.defaultProps = {
  iconColor: colors.mutedText
}

export default ArrowDownIconButton
