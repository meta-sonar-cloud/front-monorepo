import React from 'react'

import PropTypes from 'prop-types'

import { useT } from '@smartcoop/i18n'
import { closeBold } from '@smartcoop/icons'
import { colors } from '@smartcoop/styles'
import Icon from '@smartcoop/web-components/Icon'

import IconButton from '../IconButton'

const CloseIconButton = ({ iconColor, ...props }) => {
  const t = useT()
  return (
    <IconButton tooltip={ t('close') } { ...props }>
      <Icon icon={ closeBold } color={ iconColor } />
    </IconButton>
  )
}

CloseIconButton.propTypes = {
  iconColor: PropTypes.string
}

CloseIconButton.defaultProps = {
  iconColor: colors.mutedText
}

export default CloseIconButton
