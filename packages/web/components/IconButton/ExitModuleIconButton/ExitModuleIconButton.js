import React from 'react'

import PropTypes from 'prop-types'

import { useT } from '@smartcoop/i18n'
import { exitModule } from '@smartcoop/icons'
import { colors } from '@smartcoop/styles'
import Icon from '@smartcoop/web-components/Icon'

import IconButton from '../index'

const ExitModuleIconButton = ({ iconColor, ...props }) => {
  const t = useT()

  return (
    <IconButton tooltip={ t('exit module') } { ...props }>
      <Icon icon={ exitModule } color={ iconColor } />
    </IconButton>
  )
}


ExitModuleIconButton.propTypes = {
  iconColor: PropTypes.string
}

ExitModuleIconButton.defaultProps = {
  iconColor: colors.mutedText
}

export default ExitModuleIconButton
