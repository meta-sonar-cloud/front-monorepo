import React from 'react'

import PropTypes from 'prop-types'

import { useT } from '@smartcoop/i18n'
import { exitFullScreen } from '@smartcoop/icons'
import { colors } from '@smartcoop/styles'
import Icon from '@smartcoop/web-components/Icon'

import IconButton from '../IconButton'

const ExitFullScreenIconButton = ({ iconColor, ...props }) => {
  const t = useT()
  return (
    <IconButton tooltip={ t('exit full screen') } { ...props }>
      <Icon icon={ exitFullScreen } color={ iconColor } />
    </IconButton>
  )
}

ExitFullScreenIconButton.propTypes = {
  iconColor: PropTypes.string
}

ExitFullScreenIconButton.defaultProps = {
  iconColor: colors.mutedText
}

export default ExitFullScreenIconButton
