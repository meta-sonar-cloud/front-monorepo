import React from 'react'

import PropTypes from 'prop-types'

import { useT } from '@smartcoop/i18n'
import { chevronRight } from '@smartcoop/icons'
import { colors } from '@smartcoop/styles'
import Icon from '@smartcoop/web-components/Icon'

import IconButton from '../IconButton'

const UserIconButton = ({ iconColor, ...props }) => {
  const t = useT()
  return (
    <IconButton tooltip={ t('expand') } { ...props }>
      <Icon icon={ chevronRight } color={ iconColor } />
    </IconButton>
  )
}

UserIconButton.propTypes = {
  iconColor: PropTypes.string
}

UserIconButton.defaultProps = {
  iconColor: colors.mutedText
}

export default UserIconButton
