import React from 'react'

import PropTypes from 'prop-types'

import { useT } from '@smartcoop/i18n'
import { firstPage } from '@smartcoop/icons'
import { colors } from '@smartcoop/styles'
import Icon from '@smartcoop/web-components/Icon'

import IconButton from '../IconButton'

const FirstPageIconButton = ({ iconColor, ...props }) => {
  const t = useT()
  return (
    <IconButton tooltip={ t('datagrid pagination first tooltip') } { ...props }>
      <Icon size={ 14 } icon={ firstPage } color={ iconColor } />
    </IconButton>
  )
}

FirstPageIconButton.propTypes = {
  iconColor: PropTypes.string
}

FirstPageIconButton.defaultProps = {
  iconColor: colors.mutedText
}

export default FirstPageIconButton
