import React from 'react'

import codeMask from '@smartcoop/forms/masks/code.mask'
import InputNumber from '@smartcoop/web-components/InputNumber'

const InputCode = props => (
  <InputNumber
    { ...props }
    setMask={ codeMask }
    maxLength={ 11 }
  />
)

export default InputCode
