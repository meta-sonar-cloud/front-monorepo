import React from 'react'

import PropTypes from 'prop-types'

import isEmpty from 'lodash/isEmpty'
import map from 'lodash/map'

import I18n from '@smartcoop/i18n'
import colors from '@smartcoop/styles/colors'
import Card from '@smartcoop/web-components/Card'
import PolygonToSvg from '@smartcoop/web-components/PolygonToSvg'

import {
  ContainerField,
  TextGroup,
  TextLeft,
  TextRight,
  TextBold,
  Text,
  CropTextGroup,
  CropText,
  Badge
} from './styles'

const GrowingSeasonHistoryItem = (props) => {
  const {
    growingSeason,
    polygonCoordinates,
    area
  } = props

  return (
    <Card
      style={ { marginBottom: 17, marginTop: 0 } }
      cardStyle={ { padding: '14px', width: '100%', marginBottom: 0 } }
    >
      <ContainerField>
        {!isEmpty(polygonCoordinates) && (
          <PolygonToSvg
            shape={ map(polygonCoordinates, ([ lat, lng ]) => ({ lat, lng })) }
            color={ colors.orange }
            width={ 40 }
          />
        )}
        <TextGroup>
          <TextLeft>
            <CropTextGroup>
              <CropText>
                {growingSeason.crop?.description}
              </CropText>
              <CropText>
                {growingSeason.sowingYear}
              </CropText>
            </CropTextGroup>
            <Text>{growingSeason.cultivationGoal?.description}</Text>
            <Text>{parseFloat(area).toFixed(2)} ha</Text>
          </TextLeft>
          <TextRight style={ { justifyContent: 'space-between' } }>
            <TextBold>{growingSeason.sowingYear}</TextBold>
            <div>
              <Badge
                backgroundColorBadge="(89, 89, 89, 0.1)"
                colorBadge="(89, 89, 89, 1)"
              >
                <I18n params={ { gender: 'male' } }>closed</I18n>
              </Badge>
            </div>
          </TextRight>
        </TextGroup>
      </ContainerField>
    </Card>
  )
}

GrowingSeasonHistoryItem.propTypes = {
  growingSeason: PropTypes.object,
  polygonCoordinates: PropTypes.array,
  area: PropTypes.number
}

GrowingSeasonHistoryItem.defaultProps = {
  growingSeason: {},
  polygonCoordinates: [],
  area: 0
}

export default GrowingSeasonHistoryItem
