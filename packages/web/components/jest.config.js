const jestConfig = require('@smartcoop/jest-config')

module.exports = {
  ...jestConfig,
  testEnvironment: 'jsdom',
  setupFiles: ['./jest.setup.js']
}
