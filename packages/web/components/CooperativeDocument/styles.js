import styled from 'styled-components'

import colors from '@smartcoop/styles/colors'
import Button from '@smartcoop/web-components/Button'


export const Container = styled.div`
  display: flex;
  flex-wrap: wrap;
  align-items: center;
`

export const ArchiveName = styled.div`
  flex: 1;
  font-size: 16px;
  color: ${ colors.darkGrey };
  margin: 0;
`

export const DeleteDocument = styled(Button)`
  min-width: 32px;
  height: 32px;

  :not(:last-child) {
    margin-right: 5px;
  }
`

export const DownloadDocument = styled(Button)`
  font-weight: 600;
  height: 32px;

  :hover {
    text-decoration: none;
  }
`
