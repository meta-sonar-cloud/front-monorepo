import React from 'react'

import PropTypes from 'prop-types'

import Link from '@material-ui/core/Link'

import I18n from '@smartcoop/i18n'
import { trash, download } from '@smartcoop/icons'
import colors from '@smartcoop/styles/colors'
import Icon from '@smartcoop/web-components/Icon'

import {
  ArchiveName,
  Container,
  DeleteDocument,
  DownloadDocument
} from './styles'

const CooperativeDocument = ({
  documentName,
  deleteDocument,
  downloadDocument
}) => (
  <Container>
    <ArchiveName>
      <p>{documentName}</p>
    </ArchiveName>
    {(deleteDocument || downloadDocument) &&
      <div>
        {deleteDocument &&
          <DeleteDocument
            id="delete-document"
            variant="outlined"
            onClick={ deleteDocument }
          >
            <Icon size={ 14 } icon={ trash } color={ colors.red }/>
          </DeleteDocument>
        }
        {downloadDocument &&
          <DownloadDocument
            id="download-document"
            variant="outlined"
            component={ Link }
            href={ downloadDocument }
            download={ documentName }
          >
            <>
              <Icon style={ { marginRight: 5 } } size={ 16 } icon={ download }/>
              <I18n>download</I18n>
            </>
          </DownloadDocument>
        }
      </div>
    }
  </Container>
)

CooperativeDocument.propTypes = {
  documentName: PropTypes.string,
  deleteDocument: PropTypes.func,
  downloadDocument: PropTypes.string
}

CooperativeDocument.defaultProps = {
  documentName: null,
  deleteDocument: null,
  downloadDocument: ''
}

export default CooperativeDocument
