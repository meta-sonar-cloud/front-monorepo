import React from 'react'

import PropTypes from 'prop-types'

import { colors } from '@smartcoop/styles'
import { CloseRoundedIconButton } from '@smartcoop/web-components/IconButton'

import { Preview } from './styles'

const ThumbnailImage = ({ src, style, size, onClose }) => (
  <Preview style={ style } src={ src } size={ size }>
    {onClose &&
      <CloseRoundedIconButton
        size="small"
        marginHorizontal
        marginVertical
        onClick={ onClose }
        iconBorder={ colors.grey }
        iconColor={ colors.darkGrey }
      />
    }
  </Preview>
)

ThumbnailImage.propTypes = {
  src: PropTypes.string.isRequired,
  style: PropTypes.object,
  size: PropTypes.number,
  onClose: PropTypes.func
}

ThumbnailImage.defaultProps = {
  style: {},
  size: 150,
  onClose: null
}

export default ThumbnailImage
