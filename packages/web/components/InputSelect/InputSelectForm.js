import React, {
  forwardRef,
  useEffect,
  useState,
  useCallback,
  useMemo,
  useImperativeHandle
} from 'react'

import PropTypes from 'prop-types'
import uuid from 'short-uuid'

import { FieldProvider, useField } from '@smartcoop/forms'

import InputSelectStyled from './InputSelectStyled'

const InputSelectForm = forwardRef((props, externalRef) => {
  const {
    defaultValue,
    zIndex,
    onFieldValueChange,
    transformValue,
    ...rest
  } = props

  const [value, setValue] = useState(defaultValue)
  const [asyncValue, setAsyncValue] = useState({})
  const [mounted, setMounted] = useState(false)

  const {
    fieldName,
    error,
    fieldRef,
    handleChangeNative,
    handleBlur,
    required,
    resetField,
    externalOnChange,
    validateField
  } = useField()

  const id = useMemo(
    () => `${ fieldName }-${ uuid().new() }`,
    [fieldName]
  )

  const handleChange = useCallback(
    (val, asyncVal) => {
      setValue(val)
      setAsyncValue(asyncVal)
    },
    []
  )

  useEffect(() => {
    const newValue = transformValue(value)
    onFieldValueChange(newValue, asyncValue)
    if (mounted) {
      handleChangeNative(newValue)
    } else {
      setMounted(true)
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [value, asyncValue])

  const imperativeHandles = useMemo(
    () => ({
      value,
      defaultValue,
      setValue,
      resetField,
      externalOnChange,
      validateField
    }),
    [defaultValue, externalOnChange, resetField, validateField, value]
  )

  useImperativeHandle(fieldRef, () => ({
    ...fieldRef.current,
    ...imperativeHandles
  }))

  useImperativeHandle(externalRef, () => ({
    ...fieldRef.current,
    ...imperativeHandles
  }))

  // TODO disabled input style
  return (
    <InputSelectStyled
      { ...rest }
      id={ id }
      value={ value }
      onChange={ handleChange }
      onBlur={ handleBlur }
      error={ error }
      required={ required }
    />
  )
})

InputSelectForm.propTypes = {
  defaultValue: PropTypes.any,
  transformValue: PropTypes.func,
  onFieldValueChange: PropTypes.func,
  zIndex: PropTypes.number
}

InputSelectForm.defaultProps = {
  defaultValue: '',
  transformValue: v => v,
  onFieldValueChange: () => {},
  zIndex: undefined
}

const Field = forwardRef(({ path, ...props }, ref) => (
  <FieldProvider
    ref={ ref }
    { ...props }
    registerFieldOptions={ { path } }
    FieldComponent={ InputSelectForm }
  />
))

Field.propTypes = {
  name: PropTypes.string.isRequired,
  disabled: PropTypes.bool,
  onChange: PropTypes.func,
  onBlur: PropTypes.func,
  transformValue: PropTypes.func,
  onFieldValueChange: PropTypes.func,
  path: PropTypes.string,
  defaultValue: PropTypes.any,
  numberFormatProps: PropTypes.object,
  helperText: PropTypes.string
}

Field.defaultProps = {
  disabled: false,
  onChange: () => {},
  transformValue: v => v,
  onFieldValueChange: () => {},
  onBlur: () => {},
  path: 'value',
  defaultValue: '',
  numberFormatProps: undefined,
  helperText: null
}

export default Field
