import styled from 'styled-components'

export const ContainerField = styled.div`
  display: flex;
`

export const TextGroup = styled.div`
  display: flex;
  justify-content: space-between;
  width: 100%;
`

export const TextLeft = styled.div`
  margin-left: 10px;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
`
export const TextRight = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-end;
  justify-content: center;
`
export const TextBold = styled.span`
  margin: 0;
  font-weight: 600;
`

export const Text = styled.span`
  margin: 0;
`

export const CropTextGroup = styled.span`
  display: flex;
`

export const CropText = styled.span`
  margin: 0 5px 0 0;
`

export const TemperatureText = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
`
