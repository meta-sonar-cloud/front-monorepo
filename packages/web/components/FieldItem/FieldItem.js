import React from 'react'

import PropTypes from 'prop-types'

import isEmpty from 'lodash/isEmpty'
import map from 'lodash/map'

import I18n from '@smartcoop/i18n'
import { arrowUpField, arrowDownField } from '@smartcoop/icons'
import colors from '@smartcoop/styles/colors'
import Card from '@smartcoop/web-components/Card'
import Icon from '@smartcoop/web-components/Icon'
import PolygonToSvg from '@smartcoop/web-components/PolygonToSvg'

import {
  ContainerField,
  TextGroup,
  TextLeft,
  TextRight,
  TextBold,
  Text,
  CropTextGroup,
  CropText,
  TemperatureText
} from './styles'

const FieldItem = (props) => {
  const {
    field,
    onClick
  } = props

  const { growingSeason, currentWeather } = field

  return (
    <Card
      style={ { marginBottom: 17, marginTop: 0, cursor: 'pointer' } }
      cardStyle={ { padding: '14px', width: '100%', marginBottom: 0 } }
      onClick={ () => onClick({ field }) }
    >
      <ContainerField>
        <PolygonToSvg
          shape={ map(field.polygonCoordinates, ([ lat, lng ]) => ({ lat, lng })) }
          color={ colors.orange }
          width={ 40 }
        />
        <TextGroup>
          <TextLeft>
            <TextBold>{field.fieldName}</TextBold>
            {
              !isEmpty(growingSeason) ? (
                <CropTextGroup>
                  <CropText>
                    {growingSeason.crop.description}
                  </CropText>
                  <CropText>
                    {growingSeason.sowingYear}
                  </CropText>
                </CropTextGroup>
              )
                :
                <Text style={ { fontStyle: 'oblique' } }>
                  <I18n>no crop</I18n>
                </Text>
            }
            <Text>{parseFloat(field.area).toFixed(2)} ha</Text>
          </TextLeft>
          <TextRight>
            <TemperatureText>
              <Icon style={ { marginRight: '7px' } } size={ 14 } icon={ arrowUpField } color={ colors.orange } />
              <Text>{currentWeather.high}°C</Text>
            </TemperatureText>
            <TemperatureText >
              <Icon style={ { marginRight: '7px' } } size={ 14 } icon={ arrowDownField } color={ colors.blue } />
              <Text>{currentWeather.low}°C</Text>
            </TemperatureText>
            <div>
              <Text style={ { margin: 0, fontWeight: 600 } }>{currentWeather.precipitation} mm</Text>
            </div>
          </TextRight>
        </TextGroup>
      </ContainerField>
    </Card>
  )
}

FieldItem.propTypes = {
  field: PropTypes.shape({
    currentWeather: PropTypes.object,
    precipitation: PropTypes.number,
    fieldName: PropTypes.string,
    growingSeason: PropTypes.object,
    area: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    polygonCoordinates: PropTypes.array,
    plantation: PropTypes.string
  }),
  onClick: PropTypes.func
}

FieldItem.defaultProps = {
  field: {
    fieldName: null,
    growingSeason: {},
    area: 0,
    polygonCoordinates: [],
    plantation: null,
    currentWeather: {}
  },
  onClick: () => {}
}

export default FieldItem
