import React, { forwardRef, useMemo } from 'react'

import PropTypes from 'prop-types'

import CheckboxTreeViewForm from './CheckboxTreeViewForm'
import CheckboxTreeViewStyled from './CheckboxTreeViewStyled'

const CheckboxTreeView = forwardRef((props, ref) => {
  const {
    detached,
    value,
    ...rest
  } = props

  const Input = useMemo(
    () => detached ? CheckboxTreeViewStyled : CheckboxTreeViewForm,
    [detached]
  )

  return (
    <Input
      ref={ ref }
      value={ value }
      { ...rest }
    />
  )
})

CheckboxTreeView.propTypes = {
  detached: PropTypes.bool,
  value: PropTypes.any
}

CheckboxTreeView.defaultProps ={
  detached: false,
  value: []
}

export default CheckboxTreeView
