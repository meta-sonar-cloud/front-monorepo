import styled from 'styled-components'

import Typography from '@material-ui/core/Typography'

import fonts from '@smartcoop/styles/fonts'

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`
export const Label = styled(Typography)`
  font-size: 16px;
  font-family: ${ fonts.fontFamilyMontserrat };
  margin-top: 24px;
  margin-bottom: 18px;
`
export const Content = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`
