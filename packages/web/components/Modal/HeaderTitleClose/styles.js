import { makeStyles } from '@material-ui/core/styles'

export default makeStyles({
  header: {
    display: 'inline-flex',
    flex: '0 0 auto',
    justifyContent: 'space-between',
    alignItems: 'flex-start'
  },
  title: {
    flex: 'auto !important',
    // paddingTop: 10,
    paddingBottom: 10
  }
})
