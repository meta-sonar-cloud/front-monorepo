# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [0.18.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.17.0...v0.18.0) (2021-04-19)

**Note:** Version bump only for package @smartcoop/web-components





# [0.17.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.16.0...v0.17.0) (2021-04-15)


### Features

* resolve [#1639](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1639) ([28b04f1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/28b04f12bf5ddd480881a3560ffdbc1d1badd018))
* resolve [#1714](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1714) ([2ad674a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2ad674a12ad0c343aa6a18d39fb71a140b38e86a))
* resolve [#1735](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1735) ([cda6387](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/cda6387c44735527fe95bf60838cc9098715d96e))
* **documentos#1715:** renaming things and logic changes ([7b0ad3c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7b0ad3c5f4fd006fd458cfb31d4817270f4d561b)), closes [documentos#1715](http://gitlab.meta.com.br/documentos/issues/1715)





# [0.16.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.15.0...v0.16.0) (2021-04-13)


### Bug Fixes

* **auth:** fixed first refresh token and added free actions ([fc2eea9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/fc2eea987612d3f4d4c783bb59cb3df82cc6a4d1))
* **documentos#1531:** fixed datatable click ([2ebb096](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2ebb0965ae11b722de6c65bf2111f32e8080a8a0))


### Features

* merge changes ([35a5835](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/35a58359f51dd7ff362918bbaf494d1507786e25))
* resolve [#1610](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1610) ([45d2b5c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/45d2b5c20b9547eb42254db798132901759a8665))
* wip ([892dd6c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/892dd6ce5a4659ed7e9d0093f4161605065b7f0a))
* wip ([d3c26a9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d3c26a9195ee68484db448458a0df57f8b0c3336))
* wip ([5a9d8df](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/5a9d8df9fcc4ad90d70ffe06170102ae56700983))
* **#1484:** create screens stations and administration module mobile ([becfd36](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/becfd36d9d7c1dceda69af09426beba688a84127)), closes [#1484](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1484) [#484](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/484)
* **#1484:** integration with backend weather stations details ([c776b4e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c776b4eb5720ad698dc40ce90bc003c42b18027a)), closes [#1484](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1484) [#484](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/484)
* **documentos#1325:** created PEV on web ([d0d6dda](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d0d6ddae85eeab6cd3b022ac0cf40c8db8aa7d06)), closes [documentos#1325](http://gitlab.meta.com.br/documentos/issues/1325)
* **documentos#1495:** modals, saga and item ([a04f400](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a04f400ff9eb8feddfbef787e6b8fd7121985631)), closes [documentos#1495](http://gitlab.meta.com.br/documentos/issues/1495)
* **documentos#1579:** creates dropdown menu ([3726ef9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3726ef98f33b057b5f269598894e2cc7174b512c)), closes [documentos#1579](http://gitlab.meta.com.br/documentos/issues/1579)
* **documentos#1579:** insemination mobile ([767d69b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/767d69b03b2e183e6a70f74b4bf648f21bedea84)), closes [documentos#1579](http://gitlab.meta.com.br/documentos/issues/1579)







**Note:** Version bump only for package @smartcoop/web-components





# [0.15.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.12.0-alpha.1...v0.15.0) (2021-03-23)


### Bug Fixes

* **#1234:** fix insert close points ([0e820cf](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0e820cff7268cb770b810e22fbbda74f0434a458)), closes [#442](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/442)
* fixed account balance exibition ([471fde2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/471fde2d64c49d2586f4d89e4c8b6f1bf73cb424))
* **#1311:** adjustment key ([70a2b1b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/70a2b1b07a1b6f371ef513987b65caa1d455eb74)), closes [#1311](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1311) [#400](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/400)
* **#1311:** fix change property modal ([0b5d5d9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0b5d5d97a29f47cdd82f475eae3c0d59d5fd4f21)), closes [#400](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/400)
* **documentos#1292:** fixed securitie movement item ([e343ae9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e343ae932b04d967685d0c41319e159990c5d507))
* fixed input select ([b125f96](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b125f9643051abba586b0e0ddeaed1a387ccba08))


### Features

* change mdx for inputSelect ([88aff92](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/88aff9238f87cd2a8a94a80f8e495477fcd9b8a6))
* other changes to [#1523](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1523) ([737f718](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/737f718ce1eb816ff75e8762ce9dba84a86f7af7))
* resolve [#1523](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1523) ([bf353c3](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/bf353c3e7fceecbc43a53bd729d0b13e82c0ed40))
* resolve [#1523](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1523) ([96fdec6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/96fdec6fb3946d9faedecbb6bef56d3158f42a20))
* **#1288:** adjustment key item selection ([fed84a4](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/fed84a4ee1f32a4aed07de584ddd9a10dce5f5fe)), closes [#1288](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1288)
* **#1288:** adjustment select organization ([2dbbc9c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2dbbc9c4234ad1a408da856e1c2deeccc814f993)), closes [#1288](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1288) [#395](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/395)
* **999:** mapa de chuva web ([bffe52a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/bffe52a6d6356b79488ef867aabc3eb384af2435))
* **documentos#1282:** added ie field to barter package join ([63b6be4](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/63b6be4de967544edeebb2305f87ccb4c67a0ce7)), closes [documentos#1282](http://gitlab.meta.com.br/documentos/issues/1282)
* **documentos#1292:** created date tabs component ([e360ec8](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e360ec8da3b744d7ca6f17f2aaac23a5d1fcd5df)), closes [documentos#1292](http://gitlab.meta.com.br/documentos/issues/1292)
* **documentos#1336:** rainMap zindex fix ([4db9a07](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4db9a07f6ea781a146375dc9ae2b104263c472b7)), closes [documentos#1336](http://gitlab.meta.com.br/documentos/issues/1336)
* **documentos#1399:** adds v1/ suffix in api urls ([bbc3d71](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/bbc3d71a3089018ec73fc225f8c9ff8aadb24294))
* **documentos#1436:** changes in commercialization ([f4edf6b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f4edf6b977ef4c7a8fe514f844097ab2703e1572)), closes [documentos#1436](http://gitlab.meta.com.br/documentos/issues/1436)
* **documentos#1554:** alterada exibição de matricula ([a21246b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a21246b5122acd568b4b0168d61321bb485e65d8)), closes [documentos#1554](http://gitlab.meta.com.br/documentos/issues/1554)
* changed web color palette style ([c8583d8](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c8583d82ae0aae5884e2f449e1766c144a309747))
* fixed input select ([3463a24](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3463a2410020e27157cbd6867797d46755255d07))
* merge develop ([2731148](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2731148d7380e3ab5c8ec38d198b1773467e4364))
* merge develop ([6aa2ea4](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/6aa2ea420e712fa7901e347ae890fb7dcb3e475f))
* resolve [#1211](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1211) ([899a510](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/899a510c50c2dd0ba6556b94ad5f7630d25c8db7))
* resolve [#1263](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1263) ([efc6098](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/efc60984160ce7c80c866f7dbfb7c9bf3edb00ab))
* resolve [#1271](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1271) ([1d8c833](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1d8c833327f9a24e71badb366b576d88108628e3))
* resolve [#1272](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1272) ([49e9e11](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/49e9e11adad5f373bf053a92ea032c958609a5a6))
* resolve [#1335](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1335) ([f662ba6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f662ba67a2a9bd9d6f651c741f5889ebc32b5efb))
* resolve [#1335](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1335) ([6f4e819](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/6f4e819baa9fd1662f335ffba7a84f740613f13d))
* wip ([b3b6eb5](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b3b6eb57ca79dc596447e6b748d58c3b74087f14))
* wip ([2cf8dd6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2cf8dd6979ab9a9ee338858a586f2587ca18899e))
* wip ([16b3eef](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/16b3eef778dda67fd818f50aaaefb2eb54901e34))
* **documentos#1273:** created web colors pallete component ([3d949a0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3d949a06972f0a658c94f3f81c79ad35869084e1)), closes [documentos#1273](http://gitlab.meta.com.br/documentos/issues/1273)
* wip [#1263](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1263) ([2c5d1ee](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2c5d1eecda67d9cd8c5d9a02b0057972b12598bd))
* **documentos#1116:** tambo for web ([bb95716](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/bb95716083aa68b842490db35a2a34f292726f62)), closes [documentos#1116](http://gitlab.meta.com.br/documentos/issues/1116)





# [0.15.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.12.0-alpha.1...v0.15.0) (2021-03-23)


### Bug Fixes

* **#1234:** fix insert close points ([0e820cf](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0e820cff7268cb770b810e22fbbda74f0434a458)), closes [#442](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/442)
* fixed account balance exibition ([471fde2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/471fde2d64c49d2586f4d89e4c8b6f1bf73cb424))
* **#1311:** adjustment key ([70a2b1b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/70a2b1b07a1b6f371ef513987b65caa1d455eb74)), closes [#1311](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1311) [#400](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/400)
* **#1311:** fix change property modal ([0b5d5d9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0b5d5d97a29f47cdd82f475eae3c0d59d5fd4f21)), closes [#400](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/400)
* **documentos#1292:** fixed securitie movement item ([e343ae9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e343ae932b04d967685d0c41319e159990c5d507))
* fixed input select ([b125f96](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b125f9643051abba586b0e0ddeaed1a387ccba08))


### Features

* change mdx for inputSelect ([88aff92](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/88aff9238f87cd2a8a94a80f8e495477fcd9b8a6))
* other changes to [#1523](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1523) ([737f718](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/737f718ce1eb816ff75e8762ce9dba84a86f7af7))
* resolve [#1523](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1523) ([bf353c3](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/bf353c3e7fceecbc43a53bd729d0b13e82c0ed40))
* resolve [#1523](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1523) ([96fdec6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/96fdec6fb3946d9faedecbb6bef56d3158f42a20))
* **#1288:** adjustment key item selection ([fed84a4](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/fed84a4ee1f32a4aed07de584ddd9a10dce5f5fe)), closes [#1288](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1288)
* **#1288:** adjustment select organization ([2dbbc9c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2dbbc9c4234ad1a408da856e1c2deeccc814f993)), closes [#1288](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1288) [#395](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/395)
* **999:** mapa de chuva web ([bffe52a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/bffe52a6d6356b79488ef867aabc3eb384af2435))
* **documentos#1282:** added ie field to barter package join ([63b6be4](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/63b6be4de967544edeebb2305f87ccb4c67a0ce7)), closes [documentos#1282](http://gitlab.meta.com.br/documentos/issues/1282)
* **documentos#1292:** created date tabs component ([e360ec8](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e360ec8da3b744d7ca6f17f2aaac23a5d1fcd5df)), closes [documentos#1292](http://gitlab.meta.com.br/documentos/issues/1292)
* **documentos#1336:** rainMap zindex fix ([4db9a07](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4db9a07f6ea781a146375dc9ae2b104263c472b7)), closes [documentos#1336](http://gitlab.meta.com.br/documentos/issues/1336)
* **documentos#1399:** adds v1/ suffix in api urls ([bbc3d71](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/bbc3d71a3089018ec73fc225f8c9ff8aadb24294))
* **documentos#1436:** changes in commercialization ([f4edf6b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f4edf6b977ef4c7a8fe514f844097ab2703e1572)), closes [documentos#1436](http://gitlab.meta.com.br/documentos/issues/1436)
* **documentos#1554:** alterada exibição de matricula ([a21246b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a21246b5122acd568b4b0168d61321bb485e65d8)), closes [documentos#1554](http://gitlab.meta.com.br/documentos/issues/1554)
* changed web color palette style ([c8583d8](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c8583d82ae0aae5884e2f449e1766c144a309747))
* fixed input select ([3463a24](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3463a2410020e27157cbd6867797d46755255d07))
* merge develop ([2731148](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2731148d7380e3ab5c8ec38d198b1773467e4364))
* merge develop ([6aa2ea4](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/6aa2ea420e712fa7901e347ae890fb7dcb3e475f))
* resolve [#1211](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1211) ([899a510](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/899a510c50c2dd0ba6556b94ad5f7630d25c8db7))
* resolve [#1263](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1263) ([efc6098](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/efc60984160ce7c80c866f7dbfb7c9bf3edb00ab))
* resolve [#1271](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1271) ([1d8c833](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1d8c833327f9a24e71badb366b576d88108628e3))
* resolve [#1272](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1272) ([49e9e11](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/49e9e11adad5f373bf053a92ea032c958609a5a6))
* resolve [#1335](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1335) ([f662ba6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f662ba67a2a9bd9d6f651c741f5889ebc32b5efb))
* resolve [#1335](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1335) ([6f4e819](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/6f4e819baa9fd1662f335ffba7a84f740613f13d))
* wip ([b3b6eb5](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b3b6eb57ca79dc596447e6b748d58c3b74087f14))
* wip ([2cf8dd6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2cf8dd6979ab9a9ee338858a586f2587ca18899e))
* wip ([16b3eef](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/16b3eef778dda67fd818f50aaaefb2eb54901e34))
* **documentos#1273:** created web colors pallete component ([3d949a0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3d949a06972f0a658c94f3f81c79ad35869084e1)), closes [documentos#1273](http://gitlab.meta.com.br/documentos/issues/1273)
* wip [#1263](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1263) ([2c5d1ee](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2c5d1eecda67d9cd8c5d9a02b0057972b12598bd))
* **documentos#1116:** tambo for web ([bb95716](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/bb95716083aa68b842490db35a2a34f292726f62)), closes [documentos#1116](http://gitlab.meta.com.br/documentos/issues/1116)





# [0.15.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.12.0-alpha.1...v0.15.0) (2021-03-23)


### Bug Fixes

* **#1234:** fix insert close points ([0e820cf](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0e820cff7268cb770b810e22fbbda74f0434a458)), closes [#442](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/442)
* fixed account balance exibition ([471fde2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/471fde2d64c49d2586f4d89e4c8b6f1bf73cb424))
* **#1311:** adjustment key ([70a2b1b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/70a2b1b07a1b6f371ef513987b65caa1d455eb74)), closes [#1311](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1311) [#400](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/400)
* **#1311:** fix change property modal ([0b5d5d9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0b5d5d97a29f47cdd82f475eae3c0d59d5fd4f21)), closes [#400](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/400)
* **documentos#1292:** fixed securitie movement item ([e343ae9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e343ae932b04d967685d0c41319e159990c5d507))
* fixed input select ([b125f96](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b125f9643051abba586b0e0ddeaed1a387ccba08))


### Features

* change mdx for inputSelect ([88aff92](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/88aff9238f87cd2a8a94a80f8e495477fcd9b8a6))
* other changes to [#1523](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1523) ([737f718](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/737f718ce1eb816ff75e8762ce9dba84a86f7af7))
* resolve [#1523](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1523) ([bf353c3](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/bf353c3e7fceecbc43a53bd729d0b13e82c0ed40))
* resolve [#1523](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1523) ([96fdec6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/96fdec6fb3946d9faedecbb6bef56d3158f42a20))
* **#1288:** adjustment key item selection ([fed84a4](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/fed84a4ee1f32a4aed07de584ddd9a10dce5f5fe)), closes [#1288](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1288)
* **#1288:** adjustment select organization ([2dbbc9c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2dbbc9c4234ad1a408da856e1c2deeccc814f993)), closes [#1288](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1288) [#395](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/395)
* **999:** mapa de chuva web ([bffe52a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/bffe52a6d6356b79488ef867aabc3eb384af2435))
* **documentos#1282:** added ie field to barter package join ([63b6be4](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/63b6be4de967544edeebb2305f87ccb4c67a0ce7)), closes [documentos#1282](http://gitlab.meta.com.br/documentos/issues/1282)
* **documentos#1292:** created date tabs component ([e360ec8](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e360ec8da3b744d7ca6f17f2aaac23a5d1fcd5df)), closes [documentos#1292](http://gitlab.meta.com.br/documentos/issues/1292)
* **documentos#1336:** rainMap zindex fix ([4db9a07](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4db9a07f6ea781a146375dc9ae2b104263c472b7)), closes [documentos#1336](http://gitlab.meta.com.br/documentos/issues/1336)
* **documentos#1399:** adds v1/ suffix in api urls ([bbc3d71](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/bbc3d71a3089018ec73fc225f8c9ff8aadb24294))
* **documentos#1436:** changes in commercialization ([f4edf6b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f4edf6b977ef4c7a8fe514f844097ab2703e1572)), closes [documentos#1436](http://gitlab.meta.com.br/documentos/issues/1436)
* **documentos#1554:** alterada exibição de matricula ([a21246b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a21246b5122acd568b4b0168d61321bb485e65d8)), closes [documentos#1554](http://gitlab.meta.com.br/documentos/issues/1554)
* changed web color palette style ([c8583d8](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c8583d82ae0aae5884e2f449e1766c144a309747))
* fixed input select ([3463a24](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3463a2410020e27157cbd6867797d46755255d07))
* merge develop ([2731148](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2731148d7380e3ab5c8ec38d198b1773467e4364))
* merge develop ([6aa2ea4](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/6aa2ea420e712fa7901e347ae890fb7dcb3e475f))
* resolve [#1211](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1211) ([899a510](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/899a510c50c2dd0ba6556b94ad5f7630d25c8db7))
* resolve [#1263](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1263) ([efc6098](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/efc60984160ce7c80c866f7dbfb7c9bf3edb00ab))
* resolve [#1271](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1271) ([1d8c833](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1d8c833327f9a24e71badb366b576d88108628e3))
* resolve [#1272](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1272) ([49e9e11](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/49e9e11adad5f373bf053a92ea032c958609a5a6))
* resolve [#1335](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1335) ([f662ba6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f662ba67a2a9bd9d6f651c741f5889ebc32b5efb))
* resolve [#1335](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1335) ([6f4e819](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/6f4e819baa9fd1662f335ffba7a84f740613f13d))
* wip ([b3b6eb5](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b3b6eb57ca79dc596447e6b748d58c3b74087f14))
* wip ([2cf8dd6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2cf8dd6979ab9a9ee338858a586f2587ca18899e))
* wip ([16b3eef](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/16b3eef778dda67fd818f50aaaefb2eb54901e34))
* **documentos#1273:** created web colors pallete component ([3d949a0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3d949a06972f0a658c94f3f81c79ad35869084e1)), closes [documentos#1273](http://gitlab.meta.com.br/documentos/issues/1273)
* wip [#1263](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1263) ([2c5d1ee](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2c5d1eecda67d9cd8c5d9a02b0057972b12598bd))
* **documentos#1116:** tambo for web ([bb95716](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/bb95716083aa68b842490db35a2a34f292726f62)), closes [documentos#1116](http://gitlab.meta.com.br/documentos/issues/1116)





# [0.14.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.12.0-alpha.1...v0.14.0) (2021-03-23)


### Bug Fixes

* **#1234:** fix insert close points ([0e820cf](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0e820cff7268cb770b810e22fbbda74f0434a458)), closes [#442](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/442)
* fixed account balance exibition ([471fde2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/471fde2d64c49d2586f4d89e4c8b6f1bf73cb424))
* **#1311:** adjustment key ([70a2b1b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/70a2b1b07a1b6f371ef513987b65caa1d455eb74)), closes [#1311](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1311) [#400](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/400)
* **#1311:** fix change property modal ([0b5d5d9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0b5d5d97a29f47cdd82f475eae3c0d59d5fd4f21)), closes [#400](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/400)
* **documentos#1292:** fixed securitie movement item ([e343ae9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e343ae932b04d967685d0c41319e159990c5d507))
* fixed input select ([b125f96](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b125f9643051abba586b0e0ddeaed1a387ccba08))


### Features

* change mdx for inputSelect ([88aff92](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/88aff9238f87cd2a8a94a80f8e495477fcd9b8a6))
* other changes to [#1523](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1523) ([737f718](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/737f718ce1eb816ff75e8762ce9dba84a86f7af7))
* resolve [#1523](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1523) ([bf353c3](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/bf353c3e7fceecbc43a53bd729d0b13e82c0ed40))
* resolve [#1523](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1523) ([96fdec6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/96fdec6fb3946d9faedecbb6bef56d3158f42a20))
* **#1288:** adjustment key item selection ([fed84a4](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/fed84a4ee1f32a4aed07de584ddd9a10dce5f5fe)), closes [#1288](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1288)
* **#1288:** adjustment select organization ([2dbbc9c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2dbbc9c4234ad1a408da856e1c2deeccc814f993)), closes [#1288](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1288) [#395](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/395)
* **999:** mapa de chuva web ([bffe52a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/bffe52a6d6356b79488ef867aabc3eb384af2435))
* **documentos#1282:** added ie field to barter package join ([63b6be4](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/63b6be4de967544edeebb2305f87ccb4c67a0ce7)), closes [documentos#1282](http://gitlab.meta.com.br/documentos/issues/1282)
* **documentos#1292:** created date tabs component ([e360ec8](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e360ec8da3b744d7ca6f17f2aaac23a5d1fcd5df)), closes [documentos#1292](http://gitlab.meta.com.br/documentos/issues/1292)
* **documentos#1336:** rainMap zindex fix ([4db9a07](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4db9a07f6ea781a146375dc9ae2b104263c472b7)), closes [documentos#1336](http://gitlab.meta.com.br/documentos/issues/1336)
* **documentos#1399:** adds v1/ suffix in api urls ([bbc3d71](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/bbc3d71a3089018ec73fc225f8c9ff8aadb24294))
* **documentos#1436:** changes in commercialization ([f4edf6b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f4edf6b977ef4c7a8fe514f844097ab2703e1572)), closes [documentos#1436](http://gitlab.meta.com.br/documentos/issues/1436)
* **documentos#1554:** alterada exibição de matricula ([a21246b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a21246b5122acd568b4b0168d61321bb485e65d8)), closes [documentos#1554](http://gitlab.meta.com.br/documentos/issues/1554)
* changed web color palette style ([c8583d8](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c8583d82ae0aae5884e2f449e1766c144a309747))
* fixed input select ([3463a24](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3463a2410020e27157cbd6867797d46755255d07))
* merge develop ([2731148](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2731148d7380e3ab5c8ec38d198b1773467e4364))
* merge develop ([6aa2ea4](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/6aa2ea420e712fa7901e347ae890fb7dcb3e475f))
* resolve [#1211](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1211) ([899a510](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/899a510c50c2dd0ba6556b94ad5f7630d25c8db7))
* resolve [#1263](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1263) ([efc6098](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/efc60984160ce7c80c866f7dbfb7c9bf3edb00ab))
* resolve [#1271](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1271) ([1d8c833](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1d8c833327f9a24e71badb366b576d88108628e3))
* resolve [#1272](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1272) ([49e9e11](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/49e9e11adad5f373bf053a92ea032c958609a5a6))
* resolve [#1335](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1335) ([f662ba6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f662ba67a2a9bd9d6f651c741f5889ebc32b5efb))
* resolve [#1335](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1335) ([6f4e819](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/6f4e819baa9fd1662f335ffba7a84f740613f13d))
* wip ([b3b6eb5](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b3b6eb57ca79dc596447e6b748d58c3b74087f14))
* wip ([2cf8dd6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2cf8dd6979ab9a9ee338858a586f2587ca18899e))
* wip ([16b3eef](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/16b3eef778dda67fd818f50aaaefb2eb54901e34))
* **documentos#1273:** created web colors pallete component ([3d949a0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3d949a06972f0a658c94f3f81c79ad35869084e1)), closes [documentos#1273](http://gitlab.meta.com.br/documentos/issues/1273)
* wip [#1263](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1263) ([2c5d1ee](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2c5d1eecda67d9cd8c5d9a02b0057972b12598bd))
* **documentos#1116:** tambo for web ([bb95716](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/bb95716083aa68b842490db35a2a34f292726f62)), closes [documentos#1116](http://gitlab.meta.com.br/documentos/issues/1116)





# [0.13.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.12.0-alpha.1...v0.13.0) (2021-03-23)


### Bug Fixes

* **#1234:** fix insert close points ([0e820cf](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0e820cff7268cb770b810e22fbbda74f0434a458)), closes [#442](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/442)
* fixed account balance exibition ([471fde2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/471fde2d64c49d2586f4d89e4c8b6f1bf73cb424))
* **#1311:** adjustment key ([70a2b1b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/70a2b1b07a1b6f371ef513987b65caa1d455eb74)), closes [#1311](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1311) [#400](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/400)
* **#1311:** fix change property modal ([0b5d5d9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0b5d5d97a29f47cdd82f475eae3c0d59d5fd4f21)), closes [#400](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/400)
* **documentos#1292:** fixed securitie movement item ([e343ae9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e343ae932b04d967685d0c41319e159990c5d507))
* fixed input select ([b125f96](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b125f9643051abba586b0e0ddeaed1a387ccba08))


### Features

* change mdx for inputSelect ([88aff92](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/88aff9238f87cd2a8a94a80f8e495477fcd9b8a6))
* other changes to [#1523](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1523) ([737f718](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/737f718ce1eb816ff75e8762ce9dba84a86f7af7))
* resolve [#1523](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1523) ([bf353c3](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/bf353c3e7fceecbc43a53bd729d0b13e82c0ed40))
* resolve [#1523](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1523) ([96fdec6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/96fdec6fb3946d9faedecbb6bef56d3158f42a20))
* **#1288:** adjustment key item selection ([fed84a4](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/fed84a4ee1f32a4aed07de584ddd9a10dce5f5fe)), closes [#1288](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1288)
* **#1288:** adjustment select organization ([2dbbc9c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2dbbc9c4234ad1a408da856e1c2deeccc814f993)), closes [#1288](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1288) [#395](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/395)
* **999:** mapa de chuva web ([bffe52a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/bffe52a6d6356b79488ef867aabc3eb384af2435))
* **documentos#1282:** added ie field to barter package join ([63b6be4](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/63b6be4de967544edeebb2305f87ccb4c67a0ce7)), closes [documentos#1282](http://gitlab.meta.com.br/documentos/issues/1282)
* **documentos#1292:** created date tabs component ([e360ec8](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e360ec8da3b744d7ca6f17f2aaac23a5d1fcd5df)), closes [documentos#1292](http://gitlab.meta.com.br/documentos/issues/1292)
* **documentos#1336:** rainMap zindex fix ([4db9a07](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4db9a07f6ea781a146375dc9ae2b104263c472b7)), closes [documentos#1336](http://gitlab.meta.com.br/documentos/issues/1336)
* **documentos#1399:** adds v1/ suffix in api urls ([bbc3d71](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/bbc3d71a3089018ec73fc225f8c9ff8aadb24294))
* **documentos#1436:** changes in commercialization ([f4edf6b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f4edf6b977ef4c7a8fe514f844097ab2703e1572)), closes [documentos#1436](http://gitlab.meta.com.br/documentos/issues/1436)
* **documentos#1554:** alterada exibição de matricula ([a21246b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a21246b5122acd568b4b0168d61321bb485e65d8)), closes [documentos#1554](http://gitlab.meta.com.br/documentos/issues/1554)
* changed web color palette style ([c8583d8](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c8583d82ae0aae5884e2f449e1766c144a309747))
* fixed input select ([3463a24](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3463a2410020e27157cbd6867797d46755255d07))
* merge develop ([2731148](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2731148d7380e3ab5c8ec38d198b1773467e4364))
* merge develop ([6aa2ea4](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/6aa2ea420e712fa7901e347ae890fb7dcb3e475f))
* resolve [#1211](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1211) ([899a510](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/899a510c50c2dd0ba6556b94ad5f7630d25c8db7))
* resolve [#1263](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1263) ([efc6098](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/efc60984160ce7c80c866f7dbfb7c9bf3edb00ab))
* resolve [#1271](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1271) ([1d8c833](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1d8c833327f9a24e71badb366b576d88108628e3))
* resolve [#1272](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1272) ([49e9e11](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/49e9e11adad5f373bf053a92ea032c958609a5a6))
* resolve [#1335](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1335) ([f662ba6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f662ba67a2a9bd9d6f651c741f5889ebc32b5efb))
* resolve [#1335](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1335) ([6f4e819](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/6f4e819baa9fd1662f335ffba7a84f740613f13d))
* wip ([b3b6eb5](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b3b6eb57ca79dc596447e6b748d58c3b74087f14))
* wip ([2cf8dd6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2cf8dd6979ab9a9ee338858a586f2587ca18899e))
* wip ([16b3eef](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/16b3eef778dda67fd818f50aaaefb2eb54901e34))
* **documentos#1273:** created web colors pallete component ([3d949a0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3d949a06972f0a658c94f3f81c79ad35869084e1)), closes [documentos#1273](http://gitlab.meta.com.br/documentos/issues/1273)
* wip [#1263](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1263) ([2c5d1ee](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2c5d1eecda67d9cd8c5d9a02b0057972b12598bd))
* **documentos#1116:** tambo for web ([bb95716](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/bb95716083aa68b842490db35a2a34f292726f62)), closes [documentos#1116](http://gitlab.meta.com.br/documentos/issues/1116)





# [0.12.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.12.0-alpha.1...v0.12.0) (2021-03-23)


### Bug Fixes

* **#1234:** fix insert close points ([0e820cf](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0e820cff7268cb770b810e22fbbda74f0434a458)), closes [#442](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/442)
* fixed account balance exibition ([471fde2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/471fde2d64c49d2586f4d89e4c8b6f1bf73cb424))
* **#1311:** adjustment key ([70a2b1b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/70a2b1b07a1b6f371ef513987b65caa1d455eb74)), closes [#1311](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1311) [#400](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/400)
* **#1311:** fix change property modal ([0b5d5d9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0b5d5d97a29f47cdd82f475eae3c0d59d5fd4f21)), closes [#400](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/400)
* **documentos#1292:** fixed securitie movement item ([e343ae9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e343ae932b04d967685d0c41319e159990c5d507))
* fixed input select ([b125f96](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b125f9643051abba586b0e0ddeaed1a387ccba08))


### Features

* change mdx for inputSelect ([88aff92](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/88aff9238f87cd2a8a94a80f8e495477fcd9b8a6))
* other changes to [#1523](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1523) ([737f718](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/737f718ce1eb816ff75e8762ce9dba84a86f7af7))
* resolve [#1523](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1523) ([bf353c3](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/bf353c3e7fceecbc43a53bd729d0b13e82c0ed40))
* resolve [#1523](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1523) ([96fdec6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/96fdec6fb3946d9faedecbb6bef56d3158f42a20))
* **#1288:** adjustment key item selection ([fed84a4](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/fed84a4ee1f32a4aed07de584ddd9a10dce5f5fe)), closes [#1288](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1288)
* **#1288:** adjustment select organization ([2dbbc9c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2dbbc9c4234ad1a408da856e1c2deeccc814f993)), closes [#1288](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1288) [#395](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/395)
* **999:** mapa de chuva web ([bffe52a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/bffe52a6d6356b79488ef867aabc3eb384af2435))
* **documentos#1282:** added ie field to barter package join ([63b6be4](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/63b6be4de967544edeebb2305f87ccb4c67a0ce7)), closes [documentos#1282](http://gitlab.meta.com.br/documentos/issues/1282)
* **documentos#1292:** created date tabs component ([e360ec8](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e360ec8da3b744d7ca6f17f2aaac23a5d1fcd5df)), closes [documentos#1292](http://gitlab.meta.com.br/documentos/issues/1292)
* **documentos#1336:** rainMap zindex fix ([4db9a07](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4db9a07f6ea781a146375dc9ae2b104263c472b7)), closes [documentos#1336](http://gitlab.meta.com.br/documentos/issues/1336)
* **documentos#1399:** adds v1/ suffix in api urls ([bbc3d71](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/bbc3d71a3089018ec73fc225f8c9ff8aadb24294))
* **documentos#1436:** changes in commercialization ([f4edf6b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f4edf6b977ef4c7a8fe514f844097ab2703e1572)), closes [documentos#1436](http://gitlab.meta.com.br/documentos/issues/1436)
* **documentos#1554:** alterada exibição de matricula ([a21246b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a21246b5122acd568b4b0168d61321bb485e65d8)), closes [documentos#1554](http://gitlab.meta.com.br/documentos/issues/1554)
* changed web color palette style ([c8583d8](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c8583d82ae0aae5884e2f449e1766c144a309747))
* fixed input select ([3463a24](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3463a2410020e27157cbd6867797d46755255d07))
* merge develop ([2731148](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2731148d7380e3ab5c8ec38d198b1773467e4364))
* merge develop ([6aa2ea4](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/6aa2ea420e712fa7901e347ae890fb7dcb3e475f))
* resolve [#1211](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1211) ([899a510](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/899a510c50c2dd0ba6556b94ad5f7630d25c8db7))
* resolve [#1263](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1263) ([efc6098](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/efc60984160ce7c80c866f7dbfb7c9bf3edb00ab))
* resolve [#1271](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1271) ([1d8c833](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1d8c833327f9a24e71badb366b576d88108628e3))
* resolve [#1272](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1272) ([49e9e11](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/49e9e11adad5f373bf053a92ea032c958609a5a6))
* resolve [#1335](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1335) ([f662ba6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f662ba67a2a9bd9d6f651c741f5889ebc32b5efb))
* resolve [#1335](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1335) ([6f4e819](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/6f4e819baa9fd1662f335ffba7a84f740613f13d))
* wip ([b3b6eb5](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b3b6eb57ca79dc596447e6b748d58c3b74087f14))
* wip ([2cf8dd6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2cf8dd6979ab9a9ee338858a586f2587ca18899e))
* wip ([16b3eef](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/16b3eef778dda67fd818f50aaaefb2eb54901e34))
* **documentos#1273:** created web colors pallete component ([3d949a0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3d949a06972f0a658c94f3f81c79ad35869084e1)), closes [documentos#1273](http://gitlab.meta.com.br/documentos/issues/1273)
* wip [#1263](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1263) ([2c5d1ee](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2c5d1eecda67d9cd8c5d9a02b0057972b12598bd))
* **documentos#1116:** tambo for web ([bb95716](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/bb95716083aa68b842490db35a2a34f292726f62)), closes [documentos#1116](http://gitlab.meta.com.br/documentos/issues/1116)





# [0.12.0-alpha.1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.12.0-alpha.0...v0.12.0-alpha.1) (2021-02-22)


### Bug Fixes

* **#1173:** adjustment pagesize in datatable web ([99afe90](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/99afe90c86488c97358206162382cc32d60208ec)), closes [#1173](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1173)
* **documentos#1240:** bug fixes ([bb4562a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/bb4562aa03fb046c9ee90103777f114de1d30b8d)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **maps:** fixed PinMarker ([4783c8a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4783c8afe33fbb81ada0444c8e3761ff45277b33))
* fixed confirm modal ([8435f02](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8435f02a398be28d52485a3b4e747d28d23fa018))
* fixed input select width ([7a569d3](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7a569d3d93875d267ee82f98148f2ee5e9eaba02))


### Features

* **#1173:** create onchange checkboxGroup ([79f1e28](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/79f1e286ed67a77ea3b3d167bc427180d424f6cd)), closes [#1173](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1173)
* **#1173:** input selected web show list asc ([c59cbd7](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c59cbd7b5499335127988e67b51c414968babbc0)), closes [#1173](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1173)
* added disabled prop to delete icon ([57348c1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/57348c10fb93d958cfd44b6573973fa20e1699bf))
* added new props to confirm modal ([2c1a6b6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2c1a6b6e58dc7fccf0f806d14aa7818cf3386fc8))
* added new props to confirm modal ([5e1b23f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/5e1b23fddf5356284a69ab366786ba04fab066a6))
* added read less option to read more ([e75ded3](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e75ded37f45d87444202a9f556ca663f3363085e))
* added style to input quantity ([e7139bd](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e7139bdfe872c27e2fbd0410d16a6a8cb05ee8e8))
* changed read less in read more component ([caef46e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/caef46e3859e8429da64a328a37548b1e3db9c04))
* merge develop ([14437f2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/14437f2c05646aadaf548086d38f70dcf5054eda))
* merge develop ([79eb412](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/79eb412585a0950ecf6c39cb7132c1648068fbca))
* resolve [#1223](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1223) ([63b4f42](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/63b4f420a8b31ba0dd810591d679e5140ce6596f))
* resolve [#1244](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1244) ([2bdd8b6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2bdd8b6fb2f2d71672a5688054bf25a4d4db000b))
* **documentos#1233:** adjustments to satelite image view ([7067ebd](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7067ebd59103007d509c352ae4f045b067b2157c)), closes [documentos#1233](http://gitlab.meta.com.br/documentos/issues/1233)
* **documentos#535:** finished home screen for mobile ([b595eee](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b595eeed9efc2c6371bb04276a22abfd74a1d9db)), closes [documentos#535](http://gitlab.meta.com.br/documentos/issues/535)
* resolve [#992](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/992) ([3cc24d7](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3cc24d721b1e0e8243e9ec84d51677e3ee50de32))
* **documentos#987:** adds dark mode in datatable ([c262636](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c262636aae03cacf80d855f9ecbfdc244a1626f7)), closes [documentos#987](http://gitlab.meta.com.br/documentos/issues/987)





# [0.12.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.11.0...v0.12.0-alpha.0) (2021-02-12)

**Note:** Version bump only for package @smartcoop/web-components





# [0.11.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.11.0-alpha.0...v0.11.0) (2021-02-12)


### Features

* **documentos#1173:** adds charts ([7497cb6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7497cb68e4dcb19e8ad8f8460d68fe0dac33426a)), closes [documentos#1173](http://gitlab.meta.com.br/documentos/issues/1173)
* **documentos#1179:** docs ([a1812c8](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a1812c8062507dbb2729ec3edf47573950d4c59a)), closes [documentos#1179](http://gitlab.meta.com.br/documentos/issues/1179)
* **documentos#1179:** milk data card in web ([31e1154](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/31e115406a69aaa5e5a8d49d4adb02764668f428)), closes [documentos#1179](http://gitlab.meta.com.br/documentos/issues/1179)
* **web:** added InputQuantity component ([ef37950](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ef37950d52cd8e8601e940a9a2987458c32db9b1))
* resolve [#1141](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1141) ([33c9450](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/33c9450d4700ff90c457690f3ca76fe207678734))
* wip ([0f84c73](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0f84c73ffacca6596a527731c452d161b482a790))
* wip ([ec87885](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ec878853d3b3be26e7a97c2a2a81698f4d5532e8))





# [0.11.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.10.0...v0.11.0-alpha.0) (2021-02-05)


### Features

* **documentos#1123:** adds new prop to maps to choose view style ([602d845](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/602d845fbe292cb871588b608cb1981dd3b9cf14)), closes [documentos#1123](http://gitlab.meta.com.br/documentos/issues/1123)
* **technical:** turn on permissions ([5f75712](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/5f75712c9ebd3e83ec4cb86762ac20ddb0696720))





# [0.10.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.10.0-alpha.0...v0.10.0) (2021-02-01)


### Bug Fixes

* **onboarding:** fixed property location ([dd942a1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/dd942a138a6f2fc50f748097e9038e631605d8fa))
* fixed async input select ([c1cf3e6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c1cf3e631db6342aa86ae1e0b5aaaaffd6b0cf35))
* fixed component documentation ([f35e1dd](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f35e1dd13e84f0096feab22841c2d05f00918dcb))
* **documentos#322:** waiting for user location to render the map ([e3d366c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e3d366ced5a1ac1fc8d7ceaaf7f3bd516212b84e)), closes [documentos#322](http://gitlab.meta.com.br/documentos/issues/322)
* **documentos#994:** securities movement fixes ([8fb505d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8fb505db23743fa82f5674363b022c8a225047d4)), closes [documentos#994](http://gitlab.meta.com.br/documentos/issues/994)
* **release:** fixed docs ([c201b35](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c201b35db9682fd1dbc5e65cd3392622ba871b3b))


### Features

* **#1004:** increscente deps in inputSelectStyled ([ea95c5a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ea95c5a2950c202971cba59cd7f0ec8091847a88)), closes [#1004](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1004)
* **documentos#1080 documentos#1081:** added number format ([ca0f69f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ca0f69f87e8e004f9de34d83a046931f5a21e1e0)), closes [documentos#1080](http://gitlab.meta.com.br/documentos/issues/1080) [documentos#1081](http://gitlab.meta.com.br/documentos/issues/1081)
* **documentos#975:** changed securities movement list ([d19da7d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d19da7d67372b7acb79fdb68e14412883d8e5a8c)), closes [documentos#975](http://gitlab.meta.com.br/documentos/issues/975)
* resolve [#990](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/990) ([0a9226e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0a9226e45797c9e3c129b2919235c48ea23ebd51))
* resolve merge conflicts ([0646a07](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0646a07c03edcc03fe7ad4c3b58200a3fdaf5369))
* resolve merge conflicts ([d76333d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d76333db93b61f9897987ee033d050dcdc48d8fe))
* wip ([8bcec10](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8bcec10b5ecdb4f12f2928c35814d71c8b084c65))
* wip ([4dc386b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4dc386b623a070ce963ddc5f7d948d1e36ab7adf))
* wip ([094d424](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/094d4242f82345f35ecd7b4227e3c497da75c966))
* **ajuste fino talhao:** ajuste fino talhao ([c67fd1f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c67fd1fba259c20f7b41ca91effa926c2273c899))
* **att calendar:** att calendar ([b7e1530](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b7e153074e5ac740c9f448915fd3350358a103e1))
* **documentos#102:** actions props to address ([0ae6f23](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0ae6f2389f1739232432b64524312b21efdf71af)), closes [documentos#102](http://gitlab.meta.com.br/documentos/issues/102)
* **documentos#102:** add new prop to loader ([a58400f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a58400f47a88a3336ad48da08ed88246bf6a92de)), closes [documentos#102](http://gitlab.meta.com.br/documentos/issues/102)
* **documentos#102:** creates avatar input ([ce208b9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ce208b9df3d24ca6d3c70c2f1994f3e897ef3350)), closes [documentos#102](http://gitlab.meta.com.br/documentos/issues/102)
* **documentos#102:** new props emptystate ([382a2c8](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/382a2c876d657bb2b9fb6228f42b0860ab53ed97)), closes [documentos#102](http://gitlab.meta.com.br/documentos/issues/102)
* **documentos#102:** stylizing address ([db57d8c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/db57d8c0b265ddfdbbd1685c5380be12170a131e)), closes [documentos#102](http://gitlab.meta.com.br/documentos/issues/102)
* **documentos#102:** updates address component to accept user addresses ([781ce57](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/781ce57671aaab53a0ae7138ca5b7e386c489a2e)), closes [documentos#102](http://gitlab.meta.com.br/documentos/issues/102)
* wip ([87376e6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/87376e6eac23b997905919a6c85665ee38de740d))





# [0.10.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.9.0...v0.10.0-alpha.0) (2021-01-19)

**Note:** Version bump only for package @smartcoop/web-components





# [0.9.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.8.2...v0.9.0) (2021-01-19)


### Bug Fixes

* fixed datatable ref ([29b74f1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/29b74f12a0e38a18e6d2caf0fb138c37801bb9f8))
* fixed datatable ref ([3e32d54](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3e32d54d4547a84aa0e37ffbcf409fa0f1e00dc5))
* fixed web credit limit style ([12d992b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/12d992bf5c34ea3783d4d7e196eb74f27815483e))


### Features

* added sales order delete ([ff4f588](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ff4f588585eb4c76f9a544ee4c650d5551eae85b))
* merge Develop ([13f52cf](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/13f52cfce97d5e68ce271d48dc7e493ee065e28c))
* merge Develop ([4a43e2a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4a43e2a79e976c5459355a3dd2e0892e8c081e24))
* resolve [#920](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/920) ([d4f7839](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d4f783957ef1027d031ad535835c3b1f9f75a7a3))
* resolve MR Upgrades ([0e069aa](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0e069aa01c95c44efc949d193e7b604da1c2fbc2))
* **close menu map:** close menu map ([af6689e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/af6689e0ea492137bff19f0a2453333baef06d7c))
* **documentos#299:** adds iconAnchor prop to markerLeaflet ([d75e734](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d75e73492255fa3809d2681097577951092f4290)), closes [documentos#299](http://gitlab.meta.com.br/documentos/issues/299)
* **documentos#299:** creates doc about slider ([6d626a6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/6d626a62985a4677f02063466c1f2d86704c6389)), closes [documentos#299](http://gitlab.meta.com.br/documentos/issues/299)
* **documentos#299:** slider styles and component (web) ([240e042](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/240e04278dd962030632d4e355ebfdd9884a7e83)), closes [documentos#299](http://gitlab.meta.com.br/documentos/issues/299)
* **documentos#836:** created notifications list ([f062230](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f062230640b49f3df88ad8a2a6ec5b447913dc69)), closes [documentos#836](http://gitlab.meta.com.br/documentos/issues/836)
* **documentos#836:** dynamic icon ([585a128](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/585a128949f0dc2866f446963da30d5b3bc8ac9a)), closes [documentos#836](http://gitlab.meta.com.br/documentos/issues/836)
* **documentos#836:** fixed green date ([8d0098d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8d0098d8c4d75921bd232c5d1dfb977114b6eed6))
* **maps:** added google layer ([eddf3e9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/eddf3e9e410bcf702861d1ba57aaa02bde87e935))
* added credit limit exibition ([4b887c5](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4b887c524933105faeecafe1109b26c81dcd0352))
* merge develop ([5b164a0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/5b164a012ca32970cb8bf82724781871ab529e58))
* rebase with develop ([c17badd](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c17baddb1b38f98b530f474f11e9bec44987ee96))
* resolve [#894](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/894) ([4fa8b43](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4fa8b43fd81e54697bfe7d3d701ef434fe0798a7))
* wIP ([1fdaea5](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1fdaea5105a25ec89d89395a7db1ab22a87bbc22))
* wIP ([80ae937](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/80ae937854988750ccbe9f424a095c216731f73a))
* wIP Social Comments ([9f3dad3](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9f3dad309defcb5115fd48a60d7158a466a06855))
* **geotiff download:** added button to download a .tiff satelite image ([4ec244c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4ec244cc57d41825fe39934d8c191b0b152bc621))





## [0.8.2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.8.1...v0.8.2) (2021-01-06)


### Features

* **documentos#506:** image gallery on mobile ([0c26416](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0c264165820f39575cb198d12f4e6885a935b81b)), closes [documentos#506](http://gitlab.meta.com.br/documentos/issues/506)
* **property actions:** added actions to delete and edit property ([c80765a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c80765aaf6a2352a2c5ef2666eb6d2301280c0e4))
* detached radio group ([518e4c4](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/518e4c42e3b328356cd03babdaf0d96db074ffe8))
* **documentos#506:** image gallery on mobile ([046379c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/046379cbb7301373864b83008b94e1bea687402d)), closes [documentos#506](http://gitlab.meta.com.br/documentos/issues/506)





## [0.8.1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.9.0-alpha.0...v0.8.1) (2021-01-06)

**Note:** Version bump only for package @smartcoop/web-components





# [0.9.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.8.0...v0.9.0-alpha.0) (2021-01-05)

**Note:** Version bump only for package @smartcoop/web-components





# [0.8.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.8.0-alpha.0...v0.8.0) (2021-01-05)


### Bug Fixes

* fixed input select web ([c316fb8](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c316fb882f2e8f1dec969c994f618c34a3e6b95d))
* **#640:** fix bug in show doc ([c811286](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c811286169a8f319da4ef683c75cc50b08b8f31a)), closes [#182](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/182)
* **321:** ajustes icones ([dd746e0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/dd746e02b42443a2a1b20bf6bb45432d62b5b6b6))
* **documentos#506:** filters ok in web ([a2f7870](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a2f7870a1031f6f3c74b2c8f5a8b054eabc065a5)), closes [documentos#506](http://gitlab.meta.com.br/documentos/issues/506)
* **documentos#53:** removed auto sorting on add polygon point ([0f42fff](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0f42fff9cc7f4beb2d49977f43e18287c6021ab9)), closes [documentos#53](http://gitlab.meta.com.br/documentos/issues/53)
* **documentos#743:** fixed datatable pagination ([319b367](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/319b36796886e6fe19005048d890f24dd496c7e2))
* **icons:** weather icons location ([53b50fd](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/53b50fd4e5df31e43fd7b349fb63f923d18b37ae))


### Features

* **#539:** add new component ([bdb4e28](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/bdb4e282af04668a768ac2f783caaa092720cb05)), closes [#539](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/539) [#174](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/174)
* **#640:** add new component management web ([46fe2d6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/46fe2d6124a2b6347e789f226973d9c1a78b236b)), closes [#640](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/640) [#182](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/182)
* **803:** implementado destaque de melhor proposta ([72903f9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/72903f957ace89b4ed13e0d722bd810871079278))
* **documentos#27:** adds new prop to inputselect ([3fecc14](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3fecc1410132a9a2e0a9123cd2447ebb7edd6639)), closes [documentos#27](http://gitlab.meta.com.br/documentos/issues/27)
* **documentos#491:** create order form ([2b8653f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2b8653f777465d35bc1075bcc7578d4259e88863)), closes [documentos#491](http://gitlab.meta.com.br/documentos/issues/491)
* **documentos#501:** added childrenStyle to card component ([f7bcf20](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f7bcf2070232549593af95dd333e5d5c994ed8ce)), closes [documentos#501](http://gitlab.meta.com.br/documentos/issues/501)
* **documentos#501:** changed radio group error style ([79ea2b5](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/79ea2b5491f6fe08219c769023d39d48ee56b9b3)), closes [documentos#501](http://gitlab.meta.com.br/documentos/issues/501)
* **documentos#501:** changed radio group validation error ([121d759](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/121d759e6237e68c47d84d87f7ec7619515d352f)), closes [documentos#501](http://gitlab.meta.com.br/documentos/issues/501)
* **documentos#501:** disabled inputselect ([ce45383](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ce4538366a9249382532c9e61f5f04755d8d2bb3)), closes [documentos#501](http://gitlab.meta.com.br/documentos/issues/501)
* **documentos#502:** adds border color to active item ([7b9bd09](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7b9bd09edcf3b1676d49b903b45b997540d26783)), closes [documentos#502](http://gitlab.meta.com.br/documentos/issues/502)
* **documentos#502:** loader in machine details ([36af158](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/36af1586a9756b2a97461ee6c5eadb2d10504d0f)), closes [documentos#502](http://gitlab.meta.com.br/documentos/issues/502)
* **documentos#506:** creates mchine item mobiel ([8f88714](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8f8871421b09f5d8d801ef14c5a063c99a8e423d)), closes [documentos#506](http://gitlab.meta.com.br/documentos/issues/506)
* **documentos#507:** adds distance and thumb to machine item ([ca669e3](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ca669e3e0d6ceaecb02145140bd1c7869ca701dc)), closes [documentos#507](http://gitlab.meta.com.br/documentos/issues/507)
* **documentos#507:** style tweaks ([7bd95ef](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7bd95ef0238689ccbc3ebcea2c54caff7c125bef)), closes [documentos#507](http://gitlab.meta.com.br/documentos/issues/507)
* **documentos#508:** machinte item web ([e4e1519](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e4e15196a87ce44ec5ad198129cce171e951eeac)), closes [documentos#508](http://gitlab.meta.com.br/documentos/issues/508)
* **documentos#687:** created Chart component for web ([390fc5f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/390fc5f389c7a0d46b74d8be8d9e749ac955c820)), closes [documentos#687](http://gitlab.meta.com.br/documentos/issues/687)
* **documentos#688:** added AccordinsGroup into FieldDetailsScreen - web ([a8dfac0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a8dfac0f34255ec0aa03f8ad0401842b5c005b1f)), closes [documentos#688](http://gitlab.meta.com.br/documentos/issues/688)
* **documentos#688:** added chart to web ([84a8fcb](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/84a8fcb47c4d7a12a798460379ecc025c71806fb)), closes [documentos#688](http://gitlab.meta.com.br/documentos/issues/688)
* **documentos#688:** adjust field timeline chart ([003022c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/003022c800c0163a13a8abd396551e88f9691481)), closes [documentos#688](http://gitlab.meta.com.br/documentos/issues/688)
* **documentos#688:** created AccordionsGroup component for web ([a443f13](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a443f135cb98edb553d007998965937f46f080ba)), closes [documentos#688](http://gitlab.meta.com.br/documentos/issues/688)
* **documentos#688:** created InputDateRange for mobile ([ef71bf4](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ef71bf437ac74d21d6ac335a9496e4748cf2601a)), closes [documentos#688](http://gitlab.meta.com.br/documentos/issues/688)
* **documentos#688:** created InputDateRange for web ([25b3b3d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/25b3b3d98487e09f0edb61c9e38ec1e5b04d3afd)), closes [documentos#688](http://gitlab.meta.com.br/documentos/issues/688)
* **documentos#688:** finished field details ([f051b4e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f051b4ec43bbd5103f739761adbec3d395561cc2)), closes [documentos#688](http://gitlab.meta.com.br/documentos/issues/688)
* **documentos#71:** changed securities movement component ([2bf9d59](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2bf9d592f7038a758da16b7e5be0e9c2c883889c)), closes [documentos#71](http://gitlab.meta.com.br/documentos/issues/71)
* **documentos#71:** created account balance list component ([b08d5a7](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b08d5a73d43352d8fcc0d2126425b23a226ab868)), closes [documentos#71](http://gitlab.meta.com.br/documentos/issues/71)
* **documentos#71:** created securities movement ([7a9f728](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7a9f7286ebd66cb1b7c1b965c5897b8195ce154a)), closes [documentos#71](http://gitlab.meta.com.br/documentos/issues/71)
* **documentos#79:** finished charts ([2b089cd](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2b089cdf380708c35b8fe4a47f65fd8e20b7523f)), closes [documentos#79](http://gitlab.meta.com.br/documentos/issues/79)
* **documentos#79:** merged with develop ([7538e6f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7538e6fd5b6870ade8c87eb3c9f62e1295990fb3)), closes [documentos#79](http://gitlab.meta.com.br/documentos/issues/79)
* **status:** added "recusado" into demand status ([660c939](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/660c939662edc6aec0119b1c9115ce89e0b5e184))





# [0.8.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.7.0...v0.8.0-alpha.0) (2020-12-11)

**Note:** Version bump only for package @smartcoop/web-components





# [0.7.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.7.0-alpha.0...v0.7.0) (2020-12-11)


### Bug Fixes

* **#407:** adjustment input selection web ([f6a7e45](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f6a7e45baf233dfb0e0e2cd64513a77e95a48ae3)), closes [#407](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/407) [#164](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/164)
* **#518:** fix bugs ([451eaee](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/451eaeefe631cf1ff2a2d580fdb61edfa87d2b17))
* **documentos#223:** fixed demand status validation ([0561a97](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0561a975ad6c1e09df933b3895bb265c6429f0a6))
* **documentos#498:** adds missing status ([750172f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/750172f921db5441f8e68d622f4f0ecbe27b52f9)), closes [documentos#498](http://gitlab.meta.com.br/documentos/issues/498)
* **material-table:** added patch from material-table ([9aa5f72](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9aa5f724d68dd39cba765fb1fa69a44ac217c06b))


### Features

* **#397:** fix a function call ([0d10a65](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0d10a651af83bb531984805cb73a7cdf218b46e6)), closes [#139](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/139)
* **#397:** new file the confirm modal ([7647493](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/764749392a0dde1f30bedbef1ce73f3acf055e51)), closes [#397](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/397) [#139](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/139)
* **#407:** add crop list field ([f6fd382](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f6fd38267e52f7559438e047a62b9e0941deddcc)), closes [#407](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/407) [#159](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/159)
* **#407:** remove console.log ([f767cdd](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f767cdd6ca9eda7b5ee0b7c774346a08d46d0414)), closes [#407](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/407) [#159](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/159)
* **#420:** add news demand status and refactory ([4ced1d3](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4ced1d3c48bc76e546b2a913a23079ba3c9ec3af)), closes [#420](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/420) [#146](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/146)
* **documentos-424:** add ReadMore component ([98ea146](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/98ea146645a1fc9e59f5f78fbe5f84cb1a86c6b3))
* **documentos#195:** added default value to dynamic form ([872590a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/872590a2e1a3ea6fe89a667012151e84f60a6d06)), closes [documentos#195](http://gitlab.meta.com.br/documentos/issues/195)
* **documentos#195:** added disabled property to dynamic form ([a522ccb](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a522ccb03f26daeefd2c0612adc658931b8194cf)), closes [documentos#195](http://gitlab.meta.com.br/documentos/issues/195)
* **documentos#195:** added edit to supplier quotation form ([5f18a4a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/5f18a4a4d60757467695c543b11602bb2722ef81)), closes [documentos#195](http://gitlab.meta.com.br/documentos/issues/195)
* **documentos#195:** added reject, remove and edit supplier quotation ([c6e80bf](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c6e80bfd4545de18ff4c16c214f88d58f085bf29)), closes [documentos#195](http://gitlab.meta.com.br/documentos/issues/195)
* **documentos#195:** creates observationbox ([7c488ac](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7c488ac44c269502beaf3222cc155ac62940a0d1)), closes [documentos#195](http://gitlab.meta.com.br/documentos/issues/195)
* **documentos#195:** fixed default values ([ac87b49](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ac87b49c6394b75ccde6eefe351d2a73e2a3aa1c))
* **documentos#223:** created radio item and added to radio group ([ea98986](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ea989861be6ce6e8b6c54fd17d6ac26a96cea3a8)), closes [documentos#223](http://gitlab.meta.com.br/documentos/issues/223)
* **documentos#223:** fixes react warnings ([e97924f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e97924fafa6fe468da46727f6da4cd1072f78591))
* **documentos#223:** some tweaks and validations ([d1d2fa5](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d1d2fa52b522f24bc480210d81d2ae56bc800177)), closes [documentos#223](http://gitlab.meta.com.br/documentos/issues/223)
* **documentos#233:** validations. fixes and tweaks ([811b105](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/811b105c8eea3fcfbdd07a1858f857874c372620))
* **documentos#262:** style tweaks ([1185766](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1185766272988c14cbdf3c179b3317fca8699b21)), closes [documentos#262](http://gitlab.meta.com.br/documentos/issues/262)
* **documentos#351:** created CardAccordion web component ([14b5569](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/14b5569b6f05fef22af388f5de85c9426b49cf89)), closes [documentos#351](http://gitlab.meta.com.br/documentos/issues/351)
* **documentos#360:** created delivery locations ([295d06d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/295d06d08a8bd875612743c0a181eff07350ef85)), closes [documentos#360](http://gitlab.meta.com.br/documentos/issues/360)
* **documentos#362:** added disabled prop to dynamic form ([dc9d04a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/dc9d04ab91f952eff4deaaef5cc91f2570376061)), closes [documentos#362](http://gitlab.meta.com.br/documentos/issues/362)
* **documentos#398:** radiocard tweaks ([c216f85](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c216f858663787c7fe18575cfdfdab62fa757971)), closes [documentos#398](http://gitlab.meta.com.br/documentos/issues/398)
* **documentos#398:** style tweaks ([abff2c3](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/abff2c381a099250b4dc2011983d24418feda709)), closes [documentos#398](http://gitlab.meta.com.br/documentos/issues/398)
* **documentos#400:** added empty states ([ec28dc9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ec28dc91cc111d229e4d0a981c7acfaf378b984c)), closes [documentos#400](http://gitlab.meta.com.br/documentos/issues/400)
* **documentos#402:** editing growing season ([321da9c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/321da9cb75b830d048c43afa491b4f2911e58b03)), closes [documentos#402](http://gitlab.meta.com.br/documentos/issues/402)
* **documentos#424:** adjust ReadMore component ([bbc0aed](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/bbc0aed02c2e87b575eaa8e0b7dfdddbad944df3)), closes [documentos#424](http://gitlab.meta.com.br/documentos/issues/424)
* **documentos#428:** add Post component ([7f9e9be](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7f9e9be79a4ee8cfb6e61df7f3ea93ae83eee9c2)), closes [documentos#428](http://gitlab.meta.com.br/documentos/issues/428)
* **documentos#451:** created feed on mobile ([90668fe](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/90668fe9e1f1a0924d9e36729b427167cff8ba42)), closes [documentos#451](http://gitlab.meta.com.br/documentos/issues/451)
* **documentos#451:** created publish screen for mobile ([626b575](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/626b575f4021c206b0900c884d2f943d5238c47f)), closes [documentos#451](http://gitlab.meta.com.br/documentos/issues/451)
* **documentos#459:** created social and notifications screens ([98e93e7](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/98e93e7ae3c6e4c8f6cda5ace7518ee36cc86495)), closes [documentos#459](http://gitlab.meta.com.br/documentos/issues/459)





# [0.7.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.6.0...v0.7.0-alpha.0) (2020-11-27)

**Note:** Version bump only for package @smartcoop/web-components





# [0.6.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.5.3...v0.6.0) (2020-11-27)


### Bug Fixes

* **documentos#16:** fixed polygon default on change area function ([581c776](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/581c776d0e8b3552659e67759e3b115aa51bc2ee))
* **documentos#16:** refactoring input select ([c995424](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c995424700e0f1357cd7d0d22965f613f1c051d2)), closes [documentos#16](http://gitlab.meta.com.br/documentos/issues/16)
* **documentos#276:** adjusts ([aeabc03](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/aeabc03ce03342b32e101b75ea727b3c4b752ccc)), closes [documentos#276](http://gitlab.meta.com.br/documentos/issues/276)
* **documentos#276:** changed schema ([9f32131](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9f32131c7bc456f7c69811918e52c20437c17cd6)), closes [documentos#276](http://gitlab.meta.com.br/documentos/issues/276)
* **documentos#292:** fixes documentation of FieldItem ([42bbcc5](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/42bbcc5774db264d60589a5147bc2f3d276b3208))
* **documentos#324:** added require and label styles ([f79aa9f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f79aa9f65f98c6e411cc5d09a2c956d5c25a8bce)), closes [documentos#324](http://gitlab.meta.com.br/documentos/issues/324)


### Features

* **#280:** ajusted styled component ([8560d06](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8560d06fc8d9f21d1655bf625290e2a4c4d19c1d)), closes [#280](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/280) [#87](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/87)
* **#291:** remove warning for dataTable ([d4bf386](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d4bf38683c9103481c56b79845d2e25d880db522)), closes [#291](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/291) [#109](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/109)
* **#375:** add orderBy in header ([730bc26](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/730bc264cdc8be020dbdc21db42be72ec7cc3fc0)), closes [#375](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/375) [#377](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/377) [#122](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/122)
* **#375:** adjustment sorting in options of dataTable ([6df9852](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/6df9852c0fbed1f852b03f0d5ca85caaaa5f4999)), closes [#375](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/375) [#122](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/122)
* **#383:** add snackbar in growing season register ([2aa60d5](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2aa60d52c4bb96e87053e5bec2816e411cd479f4)), closes [#383](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/383) [#129](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/129)
* **#383:** add snackbar in registerGrowingSeasonRegister ([3a65fb2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3a65fb2a1abb4a1f6490dc9d876ebdfbb1219f35)), closes [#383](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/383) [#129](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/129)
* **documentos#107:** refactors function in web ([e16545d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e16545deab850c7d2f25c6891dbe520293e26489)), closes [documentos#107](http://gitlab.meta.com.br/documentos/issues/107)
* **documentos#16:** added on click to component ([9e7ca24](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9e7ca246845ed44dbb128ba85441e22919c2707d)), closes [documentos#16](http://gitlab.meta.com.br/documentos/issues/16)
* **documentos#16:** added onClick to card component ([9810f06](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9810f0673290acdbd541c247e5fca56c696143c2)), closes [documentos#16](http://gitlab.meta.com.br/documentos/issues/16)
* **documentos#16:** added other crop in growing season form ([bae1fe8](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/bae1fe8c08b113c5d396c0ad47fbb365ef95f195)), closes [documentos#16](http://gitlab.meta.com.br/documentos/issues/16)
* **documentos#16:** adds creatable example in web ([abe04cf](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/abe04cf9e40836c82cf276008137faa74dce2719)), closes [documentos#16](http://gitlab.meta.com.br/documentos/issues/16)
* **documentos#16:** adds option to create options in inputselect ([6c9cd16](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/6c9cd162c1793f9830eea0f10dd8cd9fd0a4ab0b)), closes [documentos#16](http://gitlab.meta.com.br/documentos/issues/16)
* **documentos#16:** creates fielditem in mobile ([bc06715](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/bc06715d1be39c257caf15ecf01cfe0ac1d193fb)), closes [documentos#16](http://gitlab.meta.com.br/documentos/issues/16)
* **documentos#194:** created supplier quotation status ([756c009](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/756c0099a5dfcabf8c9981d8c51a6413862a81eb)), closes [documentos#194](http://gitlab.meta.com.br/documentos/issues/194)
* **documentos#276:** adding last request date ([872a5e5](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/872a5e5a8fecb4272707e1ff00ee0e613a7a625b)), closes [documentos#276](http://gitlab.meta.com.br/documentos/issues/276) [#106](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/106)
* **documentos#277:** wip: Added default value to rows per page ([7185d94](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7185d947dd0fe089c9d5a0898c22be0c8cf38c43)), closes [documentos#277](http://gitlab.meta.com.br/documentos/issues/277) [#89](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/89)
* **documentos#277:** wip: added disableClearable prop to web inputSelect ([900f350](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/900f350ef5f10aa56662ba1aa7a90b32e264370c)), closes [documentos#277](http://gitlab.meta.com.br/documentos/issues/277) [#89](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/89)
* **documentos#277:** wip: added rule to pagination according rows per page ([4a0004d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4a0004d2fca2b99ccbe28de67fdeb2fd14bbf3e1)), closes [documentos#277](http://gitlab.meta.com.br/documentos/issues/277) [#89](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/89)
* **documentos#277:** wip: added rule to show arrows ([cf09183](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/cf091835189d06f57a877be4b74a76d8c76c845d)), closes [documentos#277](http://gitlab.meta.com.br/documentos/issues/277) [#89](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/89)
* **documentos#277:** wip: created icon buttons to pagination ([3e8ff2a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3e8ff2a59e3eb238cf287c4eb06a363da6a50383)), closes [documentos#277](http://gitlab.meta.com.br/documentos/issues/277) [#89](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/89)
* **documentos#277:** wip: created pagination to web datatable ([47e72e6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/47e72e6bcf8c2f6b52080150d12dad086b875ed3)), closes [documentos#277](http://gitlab.meta.com.br/documentos/issues/277) [#89](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/89)
* **documentos#277:** wip: fixer rows per page layout ([0cd2048](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0cd20489cb6b8c8b011c9ed55ef48d19ae7f8b06)), closes [documentos#277](http://gitlab.meta.com.br/documentos/issues/277) [#89](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/89)
* **documentos#311:** created month year input for web ([18b0de2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/18b0de23fdb837bf79e06f0cc3d7e1ec24e0b7b6)), closes [documentos#311](http://gitlab.meta.com.br/documentos/issues/311)
* **documentos#316:** adds onkeydown prop on TextFieldStyled ([a3ada0b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a3ada0b2f84644171eb019edd4529ae02a938503)), closes [documentos#316](http://gitlab.meta.com.br/documentos/issues/316)
* **documentos#370:** craeted polygon to svg for mobile ([20de6d0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/20de6d0b22399c622b9a5ff06d4b3837344e8585)), closes [documentos#370](http://gitlab.meta.com.br/documentos/issues/370)
* **documentos#371:** fixes warnings ([9d1149c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9d1149c2ec2c73fa487dcb3b27b6bd3e3d8aee6d))
* **documentos#372:** created snackbar for mobile ([dde3466](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/dde34669dedb5dbd72c806cdab7d68a068c5d076)), closes [documentos#372](http://gitlab.meta.com.br/documentos/issues/372)
* **documentos#381:** add EmptyState component ([9d7a696](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9d7a6967907fc3f1ff79d78a513f555ea998c0eb)), closes [documentos#381](http://gitlab.meta.com.br/documentos/issues/381)
* **documentos#381:** add EmptyState component ([de93f01](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/de93f01fcf047f6ae83225172dca9832e092d04c)), closes [documentos#381](http://gitlab.meta.com.br/documentos/issues/381)
* **documentos#388:** added datatable default error message ([bf6ca70](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/bf6ca70285cbee0a40121f4a0c600471d3aa56fd)), closes [documentos#388](http://gitlab.meta.com.br/documentos/issues/388)
* **documentos#390:** disabled pagination when data is loading ([9033cf3](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9033cf380fe077661224d2efeb9d482ad0fa1aef)), closes [documentos#390](http://gitlab.meta.com.br/documentos/issues/390)



# [0.6.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.5.0...v0.6.0-alpha.0) (2020-11-10)


### Features

* **#279:** add new component. Show the account balance ([efcc3fe](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/efcc3feec8138618c0a62aa43bfb764c97732212)), closes [#279](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/279) [#85](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/85)
* **#279:** adjustment style ([d3f2362](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d3f236287c1705e591ae9eee4334e4b75170ca60)), closes [#279](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/279) [#85](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/85)





# [0.6.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.5.0...v0.6.0-alpha.0) (2020-11-10)

**Note:** Version bump only for package @smartcoop/web-components
## [0.5.2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.5.1...v0.5.2) (2020-11-11)

**Note:** Version bump only for package @smartcoop/web-components





## [0.5.1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.5.0...v0.5.1) (2020-11-10)


### Bug Fixes

* **staging:** fixed http connection and turned of datatable sorting ([eccae09](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/eccae098f786dadcfdc7d24723f9c80df106e6fb))





# [0.5.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.5.0-alpha.0...v0.5.0) (2020-11-10)


### Bug Fixes

* **documentos#252:** returning to page 1 when query change ([23eb6e6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/23eb6e60811e3f65fdd3d89410c309cf6a03a503)), closes [documentos#252](http://gitlab.meta.com.br/documentos/issues/252)
* **documentos#93:** showing select errors ([53c8a49](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/53c8a492768faa7b59ab7099d2610438681683d3)), closes [documentos#93](http://gitlab.meta.com.br/documentos/issues/93)
* **input-select:** fixed onChange for web ([bc8dc71](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/bc8dc71ec2c7ad6934da995c6a028743b1fc007f))
* **inputs:** fixed inputs for web ([0f7f0cb](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0f7f0cb97370283720c8b1d746d9da2b2e0168f9))
* **inputs:** removed console ([e2f85fb](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e2f85fb33724d9fc1545d15f93fd5a2f3205eb05))


### Features

* **documentos#101:** added color property to icon button ([a98a376](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a98a3762c4c161f6b85f9935c7d732d69532b2bf)), closes [documentos#101](http://gitlab.meta.com.br/documentos/issues/101)
* **documentos#101:** added user name to user icon button ([f28727c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f28727c71d061bf2d3c8c2ce004c0a655257a5ff)), closes [documentos#101](http://gitlab.meta.com.br/documentos/issues/101)
* **documentos#101:** tweaks in menu bar ([49cfa69](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/49cfa69c9d72079a7c250689b479b055a9fa500c)), closes [documentos#101](http://gitlab.meta.com.br/documentos/issues/101)
* **documentos#127:** filtering data by statusId ([0bdb172](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0bdb1725ac52e6f6fadf92887589f1c747ebc747)), closes [documentos#127](http://gitlab.meta.com.br/documentos/issues/127)
* **documentos#127:** integrated start date on the filter ([741302a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/741302aa81d5b598322f54d4f549f4571203825e)), closes [documentos#127](http://gitlab.meta.com.br/documentos/issues/127)
* **documentos#205:** added async data to onchange ([673c743](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/673c7430e4a8c1adfa6bf0564f4671a58521391a)), closes [documentos#205](http://gitlab.meta.com.br/documentos/issues/205)
* **documentos#239:** fixed error message ([3552363](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3552363c87d24b055ed57cef256dd3d009b43dbd))
* **documentos#259:** add component polygonToSvg ([ebac1b2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ebac1b2bfccd5a94491948a9a30e01c3a49ec631)), closes [documentos#259](http://gitlab.meta.com.br/documentos/issues/259) [#81](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/81)
* **documentos#259:** add new component FieldItem ([cd42989](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/cd42989f8da8eee012fc2b08eb5c4a31320a51a5)), closes [documentos#259](http://gitlab.meta.com.br/documentos/issues/259) [#81](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/81)
* **documentos#28:** adds RS as default location in web ([20841eb](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/20841eb1ff0b5cd950b134f68ae544d793064498)), closes [documentos#28](http://gitlab.meta.com.br/documentos/issues/28)
* **documentos#49:** added query params to input select ([10b88bb](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/10b88bbeec3ed2e1dd41ea6e1b81214b45e41614)), closes [documentos#49](http://gitlab.meta.com.br/documentos/issues/49)
* **documentos#93:** adds mindate validator, also picker props in inputdate ([7911fde](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7911fdecd504d1884b7f918de61f715266155c27)), closes [documentos#93](http://gitlab.meta.com.br/documentos/issues/93)
* **documentos#93:** created form ([79dc053](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/79dc053e867209bbb6c2f6b345878daef8e2b73d)), closes [documentos#93](http://gitlab.meta.com.br/documentos/issues/93)
* **documentos#93:** listing delivery locations by current organization ([a2aa3e2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a2aa3e2e8612159d9218e609af4ae2357ee675b5)), closes [documentos#93](http://gitlab.meta.com.br/documentos/issues/93)





# [0.5.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.4.0...v0.5.0-alpha.0) (2020-10-27)

**Note:** Version bump only for package @smartcoop/web-components





# [0.4.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.3.0-alpha.0...v0.4.0) (2020-10-27)


### Bug Fixes

* **datatable:** reloading datatable on urlParams/queryParams changed ([e015d64](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e015d64edf0927d5156b14a6c8791a44075acf14))
* **documentos#28:** fixed dynamic form validation ([cac1c66](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/cac1c6690aded2cdd3e5ed5a6bd6360c7f0ed3d5))
* **documentos#28:** fixed maps bottom button margin ([8225971](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8225971a42b9eb77477a05824f9d1cd5c03f9418))
* **documentos#28:** fixes stepper colors ([8dfe174](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8dfe17401b6a7ad4ef6fffbc46694dda13431cef))
* **documentos#49:** fixed text field disabled ([33323d7](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/33323d7e37cd83218f5e2f8f4a497cb16e2161da))
* **documentos#49:** fixs documentation of avatargroup ([96de7e5](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/96de7e5c88ea8b621dc15cce853e917f2256a104)), closes [documentos#49](http://gitlab.meta.com.br/documentos/issues/49)
* **documentos#49:** style, functionalities and lint fixes ([d8d6ee0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d8d6ee0842b7c7eaa369cd5e29cabfe8128dc30d)), closes [documentos#49](http://gitlab.meta.com.br/documentos/issues/49)
* fixed radio group style ([c54a807](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c54a807aa7d222e828afd26911f178387e4f9e00))
* removed unnecessary prop in checkbox group card ([14c2853](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/14c28539b8bdf91ac6be9edae63db396cf6c232c))
* **documentos#53:** fixed documentation ([0088656](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0088656e062a0bd84e83f4341aab9b441661f5c2))


### Features

* **documentos#132:** wIP: Added fonts to web ([c7c16b0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c7c16b0cba1dd3c5c7662e1236c42bd0ef0f3f2e)), closes [documentos#132](http://gitlab.meta.com.br/documentos/issues/132) [#31](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/31)
* **documentos#158:** adds new badge "demand status" ([b1b2c28](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b1b2c28e712a2f43b60e41325993316f2f4bd14c)), closes [documentos#158](http://gitlab.meta.com.br/documentos/issues/158)
* **documentos#164:** created checkbox tree view web component ([467b234](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/467b234bd9cc98ab93b35ed5bf1d3892d27b9f16)), closes [documentos#164](http://gitlab.meta.com.br/documentos/issues/164)
* **documentos#18:** finished input date with calendar picker for web ([c583f68](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c583f68499aeafb5c6bb21d87e53312d350e3fd1)), closes [documentos#18](http://gitlab.meta.com.br/documentos/issues/18)
* **documentos#18:** wip: changed inputName prop on datepicker ([10b2849](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/10b284911af52e2263e7f536190c76c475a6c2de)), closes [documentos#18](http://gitlab.meta.com.br/documentos/issues/18) [#10](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/10)
* **documentos#18:** wip: created datepicker on web ([efa734c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/efa734c4093d4b1109290564f05bc61ece3aae29)), closes [documentos#18](http://gitlab.meta.com.br/documentos/issues/18) [#10](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/10)
* **documentos#181:** datatable with order direction ([bcc185b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/bcc185b35d28ad7d3736875301fe5ce4da54ad1b)), closes [documentos#181](http://gitlab.meta.com.br/documentos/issues/181)
* **documentos#181:** sorting payment options by quantity of days ([2911c42](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2911c42e0a094723b5f6204b737a7ad234980038)), closes [documentos#181](http://gitlab.meta.com.br/documentos/issues/181)
* **documentos#182:** wip: changed login to render slug ([9c2aa6f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9c2aa6fd61b59b5533d7cce96d8e82ef77c557fe)), closes [documentos#182](http://gitlab.meta.com.br/documentos/issues/182) [#55](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/55)
* **documentos#185:** created basic navigation for web and mobile ([4b144ed](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4b144edcd2c7d7338100f5da5105df4ccec87969)), closes [documentos#185](http://gitlab.meta.com.br/documentos/issues/185)
* **documentos#193:** detached based TextField web components from Form logic ([927d97d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/927d97d3d022ead656722945b466a2ce55f737d2)), closes [documentos#193](http://gitlab.meta.com.br/documentos/issues/193)
* **documentos#25:** added web disabled input style ([921b423](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/921b423d32c086cabe6d1981a18c2a1836ed926d)), closes [documentos#25](http://gitlab.meta.com.br/documentos/issues/25)
* **documentos#28:** added minimum number value to dynamic form ([6c3afda](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/6c3afda5bbd086c1793c5780cb99aea427cbdc4b)), closes [documentos#28](http://gitlab.meta.com.br/documentos/issues/28)
* **documentos#28:** adds new prop customIcon at PinMarker ([df983d4](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/df983d465820b6dec752ca51a0882156d4510de5)), closes [documentos#28](http://gitlab.meta.com.br/documentos/issues/28)
* **documentos#49:** added adornment style prop to input icon ([28ebb97](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/28ebb9715162372ecaffb899b7402691a7a80866)), closes [documentos#49](http://gitlab.meta.com.br/documentos/issues/49)
* **documentos#49:** adds avatars ([b28cc03](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b28cc0343bd150981e764a5044eecc431c112d9d)), closes [documentos#49](http://gitlab.meta.com.br/documentos/issues/49)
* **documentos#49:** adds jsonviewer ([d739874](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d739874e16a46e789d0017905d274aa324092bb6)), closes [documentos#49](http://gitlab.meta.com.br/documentos/issues/49)
* **documentos#49:** adds localization in datepicker ([dc74acd](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/dc74acd42e76d6e9c637caf1ec760d821963bdc9)), closes [documentos#49](http://gitlab.meta.com.br/documentos/issues/49)
* **documentos#49:** adds naturalsort to items add in checkboxtreeview ([138a11c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/138a11c5ba521a1e8a00522135fa4b88e8bbf930)), closes [documentos#49](http://gitlab.meta.com.br/documentos/issues/49)
* **documentos#49:** adds style to picker ([309ea70](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/309ea702f912b35bc61b1d34b08e02d2e4805899)), closes [documentos#49](http://gitlab.meta.com.br/documentos/issues/49)
* **documentos#49:** created checkbox with search component ([0283d86](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0283d8644a5bbff71dcdacf29403e8eabdeae8a8)), closes [documentos#49](http://gitlab.meta.com.br/documentos/issues/49)
* **documentos#49:** created DataTable async for web ([609c7cb](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/609c7cb05d0df3503703363912d3bde9ede825b4)), closes [documentos#49](http://gitlab.meta.com.br/documentos/issues/49)
* **documentos#49:** created input icon ([28abc61](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/28abc61aff2a1485638e942d165cd12e714b5349)), closes [documentos#49](http://gitlab.meta.com.br/documentos/issues/49)
* **documentos#49:** created input search ([75dd347](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/75dd3479e0cf205768350211e06ff4702cd35827)), closes [documentos#49](http://gitlab.meta.com.br/documentos/issues/49)
* **documentos#49:** created web adress info ([59bc8b2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/59bc8b2021ae2427aa5f46bf0ff56cc51e85d145)), closes [documentos#49](http://gitlab.meta.com.br/documentos/issues/49)
* **documentos#49:** created web checkbox group card ([61b35aa](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/61b35aa58dc63e763a683db31b11ae1a501177af)), closes [documentos#49](http://gitlab.meta.com.br/documentos/issues/49)
* **documentos#49:** creates Avatar component ([5507cd6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/5507cd6434ec60832052622015af754f1c27eb57)), closes [documentos#49](http://gitlab.meta.com.br/documentos/issues/49)
* **documentos#49:** detaching textfield from form ([654b8ab](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/654b8abbbe32de8be95bf8199b2a2fab2a82dba3)), closes [documentos#49](http://gitlab.meta.com.br/documentos/issues/49)
* **documentos#49:** estilização ([85d4c09](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/85d4c09619526e81aa8bc769d6614a873955a074)), closes [documentos#49](http://gitlab.meta.com.br/documentos/issues/49)
* **documentos#49:** implements customizable input un checkboxtreeitem ([205a92e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/205a92eeea6408396b9a5e8ae222c542889beb88)), closes [documentos#49](http://gitlab.meta.com.br/documentos/issues/49)
* **documentos#49:** merge ([f18397e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f18397e1bdd1e275fb12aeab9ea91f32008ec8e8)), closes [documentos#49](http://gitlab.meta.com.br/documentos/issues/49)
* **documentos#49:** removed name from input search ([854eea6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/854eea635a3f937429d9db32cfae7e95ac2d25d1)), closes [documentos#49](http://gitlab.meta.com.br/documentos/issues/49)
* **documentos#49:** removed style in input icon ([a5095ad](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a5095ad11b1327a0e5bfb68ed6589d74041ad7b7)), closes [documentos#49](http://gitlab.meta.com.br/documentos/issues/49)
* **documentos#49:** removes unused import ([773791f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/773791f3ba6a06482e4a156e2f5abbf6f4013220)), closes [documentos#49](http://gitlab.meta.com.br/documentos/issues/49)
* **documentos#49:** removes unused paymentOptions ([fe34f65](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/fe34f6593b7770c2bcbc6fa2be40219d2084b24a)), closes [documentos#49](http://gitlab.meta.com.br/documentos/issues/49)
* **documentos#49:** simple fix to not overlap ids created by the input ([f0d6c61](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f0d6c6105f1b952671512879f423cbafab38e061))
* **documentos#49:** tweaks in css ([744e282](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/744e2829f0115f33a81dcc99bc30ed5202d33563)), closes [documentos#49](http://gitlab.meta.com.br/documentos/issues/49)
* **documentos#49:** update styles ([21acc66](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/21acc66ca0903e9b87445630d94520e0979531c3)), closes [documentos#49](http://gitlab.meta.com.br/documentos/issues/49)
* **documentos#53:** added style prop to web map ([808aba2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/808aba2be95c57b8cdf20be727ec73884d3e1ca5)), closes [documentos#53](http://gitlab.meta.com.br/documentos/issues/53)
* **documentos#53:** added web map polygon area calc ([1ff12c0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1ff12c0ecd1525e0a4216f49499b9cf8aa3cda21)), closes [documentos#53](http://gitlab.meta.com.br/documentos/issues/53)
* **documentos#53:** created polygon map component ([13306bd](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/13306bd9eabac3c3e46f1810207d6caaa102ea28)), closes [documentos#53](http://gitlab.meta.com.br/documentos/issues/53)
* **documentos#68:** added style prop to icon component ([ee0470b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ee0470bdb293c9da31dd9897c111a92a2d6d2bcb)), closes [documentos#68](http://gitlab.meta.com.br/documentos/issues/68)
* **documentos#68:** changed item select style ([e810953](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e81095349bfbf806d6a4660ce86a45be29326e77)), closes [documentos#68](http://gitlab.meta.com.br/documentos/issues/68)
* **documentos#68:** created item selection component ([556fa00](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/556fa005db03e89eb721a5806633fbadfa355820)), closes [documentos#68](http://gitlab.meta.com.br/documentos/issues/68)
* **documentos#68:** now the itemSelection use a prop to selectedItem ([f3eb1e3](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f3eb1e3038d672fbab396dd7757f1cf7142d475e)), closes [documentos#68](http://gitlab.meta.com.br/documentos/issues/68)
* **documentos#87:** changed kebab formatter function ([fc300bd](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/fc300bd1968f469f3c24511dae285bcf0cd5f7f5)), closes [documentos#87](http://gitlab.meta.com.br/documentos/issues/87) [#22](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/22)
* **documentos#87:** wIP: Added input to checkbox group ([261cfa1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/261cfa1026f56be4b7ea52d0885a195afbcc3094)), closes [documentos#87](http://gitlab.meta.com.br/documentos/issues/87) [#22](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/22)
* **forms:** added form.validateField(fieldName) ([5effc5c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/5effc5cf477a400358ba2d4c162b560e008c4a15))





# [0.3.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.2.0...v0.3.0-alpha.0) (2020-09-28)

**Note:** Version bump only for package @smartcoop/web-components




# [0.2.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.2.0-alpha.0...v0.2.0) (2020-09-28)


### Bug Fixes

* **documentos#10:** passing cpf to create verify code ([4114630](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4114630214e291393665f652dc3407d99d786a18)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#25:** fixed inputfloat initial value documentos[#60](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/60) ([db39793](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/db39793de2916a1baa64543ea9c2ca7671847798))
* **documentos#28:** fixed checkboxbutton style and import of icon ([105f708](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/105f708c5d38a31c09633966d642e6800507f7a3))
* **documentos#28:** fixed stepper style ([9fed4b7](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9fed4b75731bacb99629a527e8e22e5a041e00a5))
* fixed default value in textfield ([eed296b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/eed296b8eefac0f6d86cd65b1ee9bedd6c42bb37))
* fixed textfield id after rebase ([8f7121e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8f7121e385f8f98352ed1c10cfdcaa72270e4266))
* **forms:** fixed form package ([85e9656](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/85e965662d9ebcd4780f737474ef922e4f120d2a))
* **web:** fix login web ([d804623](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d80462386582d32f9739bb7bf3b04054912c956a))


### Features

* **documentos#10:** auto fill forms with user api data ([9909ac6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9909ac6498d857e0f5ecb587d0334fab4b064036)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#10:** created identification and address screen on web ([57b0f74](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/57b0f7432fdabf9ac59cdfa52760d1a56a1e49f2)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#10:** created web login screen ([34ed647](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/34ed647f1ab16e0c1dc45db8455239335ed6286d)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#10:** created web screens ([496ac64](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/496ac645455e7c7e127a68385e9d8fa8b3b4b9f6)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#22:** adds Checkbox in web ([2fe4946](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2fe4946b7c53b1ca3ae9265f97a956aa64e7e87c)), closes [documentos#22](http://gitlab.meta.com.br/documentos/issues/22) [documentos#22](http://gitlab.meta.com.br/documentos/issues/22)
* **documentos#22:** merge ([824f2d2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/824f2d2f9b25aecb685e3b33ab1b4f445665fc08)), closes [documentos#22](http://gitlab.meta.com.br/documentos/issues/22)
* **documentos#25:** added input float web component ([4f70683](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4f70683d8ea633d69a9011a1427ba505f0d39bc1)), closes [documentos#25](http://gitlab.meta.com.br/documentos/issues/25)
* **documentos#25:** added web radio group component documentos[#67](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/67) ([55f04a1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/55f04a16d35940cd8de17f64616c1c63dcf8cb06)), closes [documentos#25](http://gitlab.meta.com.br/documentos/issues/25)
* **documentos#25:** created web input unity documentos[#21](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/21) ([c20cc76](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c20cc76fcf883e035f4bd38dc3eb08005a76713f)), closes [documentos#25](http://gitlab.meta.com.br/documentos/issues/25)
* **documentos#28:** added float option to input unity ([2112ac0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2112ac08a60aac4c49b8cb77e634c771e612754e)), closes [documentos#28](http://gitlab.meta.com.br/documentos/issues/28)
* **documentos#28:** added input unity to dynamic form ([8dbc854](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8dbc854760bcdb1c6c4a8cb33f801a6307a0f064)), closes [documentos#28](http://gitlab.meta.com.br/documentos/issues/28)
* **documentos#28:** created helperText to TextField ([1b22624](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1b22624d77e2e84e3589d454391f39f2bf178984)), closes [documentos#28](http://gitlab.meta.com.br/documentos/issues/28)
* **documentos#28:** created maps for web ([87cc7ec](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/87cc7ec27ec25c531dd748915382294b88f11ba2)), closes [documentos#28](http://gitlab.meta.com.br/documentos/issues/28)
* **documentos#28:** created web dynamic input documentos[#67](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/67) ([c0fc49f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c0fc49f4b10fc19bb6ed8e6b1187872e229bfb3e)), closes [documentos#28](http://gitlab.meta.com.br/documentos/issues/28)
* **documentos#28:** fixed dynamic form style ([7eb5e4b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7eb5e4be336953768743f92dfb2f5153d6083a5c))
* **documentos#28:** maps working into ios devices ([fc82239](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/fc82239ad99a69c194bd84da56611f2433b2def1)), closes [documentos#28](http://gitlab.meta.com.br/documentos/issues/28)
* **documentos#28:** overrides muioutlinedinput class ([349e3fa](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/349e3fac0104076330ed2d325efa606ef9a83f6d)), closes [documentos#28](http://gitlab.meta.com.br/documentos/issues/28)
* **documentos#28:** updated dynamic form functionality ([090305d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/090305d1ddf31f7bab741e22684e38dd85c5fa20)), closes [documentos#28](http://gitlab.meta.com.br/documentos/issues/28)
* **documentos#28:** using useMemo into maps for mobile ([84a8854](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/84a885475de04b870df8d52aa769601b86971f38)), closes [documentos#28](http://gitlab.meta.com.br/documentos/issues/28)
* **documentos#40:** removed Smartcoop text from GuestCard ([cef7dcf](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/cef7dcf30f65da670d1433da7fa4969da75c0066)), closes [documentos#40](http://gitlab.meta.com.br/documentos/issues/40) [#14](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/14)
* **documentos#57:** created modal component to web and mobile ([c3610ea](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c3610eabd1cecab013f46fe3c116d5f7cea00551)), closes [documentos#57](http://gitlab.meta.com.br/documentos/issues/57) [#18](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/18)
* **documentos#57:** fixed eslint warnings ([eb54073](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/eb54073ccffef79348afd2114d33ce3e08191da5)), closes [#18](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/18)
* **documentos#81:** imports datatable to web ([0ae583a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0ae583ad95cb56f98bbe662bb132f0652a1e8a79)), closes [documentos#81](http://gitlab.meta.com.br/documentos/issues/81) [documentos#81](http://gitlab.meta.com.br/documentos/issues/81)
* **documentos#86:** adds stepper ([d3e487d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d3e487daaca6d793b0bdeb82ba5e2c1af252c3dd)), closes [documentos#86](http://gitlab.meta.com.br/documentos/issues/86)
* **documentos#86:** fix docs ([db5792b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/db5792ba3f99fbec85953ab17da6b3735976a1ec))





# [0.2.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.1.0...v0.2.0-alpha.0) (2020-09-13)

**Note:** Version bump only for package @smartcoop/web-components





# [0.1.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.1.0-alpha.0...v0.1.0) (2020-09-13)


### Bug Fixes

* **documentos#10:** fix input number atribute order ([0633549](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/06335496cd0e1b2ec24a5da83e8ff5e62bf4785f))
* **documentos#10:** fixed select \o/ documentos[#30](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/30) ([56ab086](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/56ab08628ed8b0859112cc76f6b2da989098d5a0))
* **forms:** fix validations ([7c413a5](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7c413a5c916867af845ec3726b4c3d2deae0ddec))


### Features

* **#10:** created web InputCpfCnpj with mask and validations ([a28957a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a28957ae1578d15e1d20395b6d3df7124638cdc1)), closes [#10](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/10) [#17](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/17)
* **documentos#10:** applying documentos[#37](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/37) into documentos[#39](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/39) ([c8e043e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c8e043ee97e72ea17b2ebd347852ea2cd999e755)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#10:** created input code documentos[#51](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/51) ([1a9d25d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1a9d25d87fb5371aeab53fe9e8c0b38f4284ccbd)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#10:** created InputCep for mobile and web ([fb3753e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/fb3753e38e6a918c2a11d8b10979bc58f684b612)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10) [#6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/6)
* **documentos#10:** created inputDate web and mobile documentos[#18](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/18) ([fd30c7d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/fd30c7d25403c4fbbef7aeb6ed1d1f6f9ba226f5)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#10:** created InputNumber on mobile and web ([fa73f47](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/fa73f477330111329a6249f7a851e029fc2eede4)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10) [#31](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/31)
* **documentos#10:** created InputPhone on mobile and web ([ca48da4](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ca48da4fe7fa83d3051a9925b73452b5aa4a68ba)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10) [#2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/2)
* **documentos#10:** created select input and perf forms ([e4e83ab](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e4e83ab2bda01613b50076155333d3c7ce487607)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#10:** login and signup screens ([e4b9d02](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e4b9d0278f4785aa31928f9ec3bc6e5de4ff98e2)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#10:** mobile inputs styled as material design ([8a9e3cc](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8a9e3cce883733f69190c3eb53ed92aba70e4026)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10) [#7](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/7)
* **documentos#33:** adds loader ([94ba79b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/94ba79b854af259764997a01b23a72d365512e17)), closes [documentos#33](http://gitlab.meta.com.br/documentos/issues/33) [documentos#33](http://gitlab.meta.com.br/documentos/issues/33)
* **forms:** @smarcoop/forms finished ([d95dc20](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d95dc20603902124c3b35f531986dcb08e108879))
* **forms:** single validate for web and mobile forms ([9bbeb71](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9bbeb71f1a9c0acac8d6e817e614159eda808a5d))
* **mobile-components:** added input masks ([1e5a7fd](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1e5a7fd25a7faec83814cd330741c420f494a4c1))





# 0.1.0-alpha.0 (2020-08-21)


### Bug Fixes

* **test:** fix coverage report ([f774e6a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f774e6a99b31d858d8ea587632aa13bd6d1b103c))
