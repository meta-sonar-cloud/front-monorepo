import React from 'react'
import HeatmapLayer from 'react-leaflet-heatmap-layer'

import PropTypes from 'prop-types'

const Heatmap = ({ points }) => (
  <HeatmapLayer
    // fitBoundsOnLoad
    // fitBoundsOnUpdate
    points={ points }
    longitudeExtractor={ (m) => m[1] }
    latitudeExtractor={ (m) => m[0] }
    intensityExtractor={ (m) => parseFloat(m[2] || '9999') }
  />
)

Heatmap.propTypes = {
  points: PropTypes.arrayOf(PropTypes.array)
}

Heatmap.defaultProps = {
  points: []
}

export default Heatmap
