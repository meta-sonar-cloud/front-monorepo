import React, { useState, useMemo } from 'react'

import PropTypes from 'prop-types'

import CardContent from '@material-ui/core/CardContent'
import Collapse from '@material-ui/core/Collapse'
import Fade from '@material-ui/core/Fade'

import ArrowDownIconButton from '@smartcoop/web-components/IconButton/ArrowDownIconButton'
import ArrowUpIconButton from '@smartcoop/web-components/IconButton/ArrowUpIconButton'

import { Container } from './styles'

const CardAccordion = (props) => {
  const {
    children,
    defaultOpened,
    ...rest
  } = props

  const [opened, setOpened] = useState(defaultOpened)

  const IconButton = useMemo(
    () => opened ? ArrowUpIconButton : ArrowDownIconButton,
    [opened]
  )

  return (
    <Container
      { ...rest }
      headerRight={ <IconButton size="small" onClick={ () => setOpened(old => !old) } /> }
    >
      <Collapse in={ opened }>
        <Fade in={ opened }>
          <CardContent style={ { padding: 0 } }>
            {children}
          </CardContent>
        </Fade>
      </Collapse>
    </Container>
  )
}

CardAccordion.propTypes = {
  children: PropTypes.any,
  defaultOpened: PropTypes.bool
}

CardAccordion.defaultProps = {
  children: null,
  defaultOpened: false
}

export default CardAccordion
