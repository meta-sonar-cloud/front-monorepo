import React from 'react'

import { shallow } from 'enzyme'
import toJson from 'enzyme-to-json'

import Button from '../Button'

describe('Button color primary', () => {
  const spyOnClick = jest.fn()

  const wrapper = shallow(
    <Button
      id="button"
      onClick={ spyOnClick }
      color="primary"
    >
      content
    </Button>
  )

  it('should render correctly', () => {
    expect(toJson(wrapper)).toMatchSnapshot()
  })

  it('should simulate click one time', () => {
    wrapper.find('#button').simulate('click')
    expect(spyOnClick).toBeCalledTimes(1)
  })
})

describe('Button color secondary', () => {
  const spyOnClick = jest.fn()

  const wrapper = shallow(
    <Button
      id="button"
      onClick={ spyOnClick }
      color="secondary"
    >
      content
    </Button>
  )

  it('should render correctly', () => {
    expect(toJson(wrapper)).toMatchSnapshot()
  })

  it('should simulate click one time', () => {
    wrapper.find('#button').simulate('click')
    expect(spyOnClick).toBeCalledTimes(1)
  })
})
