import React, { useMemo } from 'react'

import PropTypes from 'prop-types'

import { isEmpty } from 'lodash'

import { colors } from '@smartcoop/styles'
import Card from '@smartcoop/web-components/Card'

import {
  Container,
  TextGroup,
  TextLeft,
  TextRight,
  TextBold,
  Text,
  ModelYear,
  CurrentImage,
  ImageContainer
} from './styles'

const MachineItem = (props) => {
  const {
    machine,
    active,
    onClick
  } = props

  const selectedImage = useMemo(
    () => !isEmpty(machine?.machineFiles) ? machine.machineFiles[0].fileUrl : null, [machine]
  )

  const distance = useMemo(
    () => (machine.property?.distance ? (`${ machine.property.distance  } km`) : ''),
    [machine]
  )

  return (
    <Card
      style={ { marginBottom: 17, marginTop: 0, cursor: 'pointer' } }
      cardStyle={ { padding: '14px', width: '100%', marginBottom: 0, border: active ? (`2px solid${  colors.yellow }`) : '2px solid transparent' } }
      onClick={ () => onClick({ machine }) }
    >
      <Container>
        <ImageContainer>
          <CurrentImage src={ selectedImage } />
        </ImageContainer>
        <TextGroup>
          <TextLeft>
            <TextBold>{machine.machineType.description}</TextBold>
            <Text>{machine.machineBrand.description}</Text>
            <ModelYear>
              <Text>{machine.model}</Text>
              <Text>{machine.year}</Text>
            </ModelYear>
          </TextLeft>
          <TextRight>
            {distance}
          </TextRight>
        </TextGroup>
      </Container>
    </Card>
  )
}

MachineItem.propTypes = {
  machine: PropTypes.shape({
    machineType: PropTypes.object,
    machineBrand: PropTypes.object,
    year: PropTypes.number,
    model: PropTypes.string,
    distance: PropTypes.string,
    machineFiles: PropTypes.array,
    property: PropTypes.object
  }),
  active: PropTypes.bool,
  onClick: PropTypes.func
}

MachineItem.defaultProps = {
  machine: {
    machineType: {},
    brand: '',
    year: 0,
    model: '',
    distance: '',
    machineFiles: [],
    property: {}
  },
  onClick: () => {},
  active: false
}

export default MachineItem
