const { processEnvWithEnvFile } = require('@smartcoop/env-config')

const processEnvFile = ({ callback = () => {}, options = {}}) => {
  return processEnvWithEnvFile({
    ...options,
    callback: (err, env) => !err && callback(env)
  })
}

module.exports = processEnvFile
