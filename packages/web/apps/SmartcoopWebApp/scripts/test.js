/* eslint-disable no-console */

const { execTerminalCommand } = require('@smartcoop/env-config')

const processEnvFile = require('./utils/processEnvFile')

processEnvFile({
  callback: ({ envFile }) => {
    const childProcess = execTerminalCommand(`env-cmd -f ${ envFile } yarn react-app-rewired test`)

    childProcess.on('error', error => {
      console.error(error)
    })

    childProcess.on('close', code => {
      if (code > 0) {
        console.error(`Command failed with code ${ code }`)
      }
    })
  }
})
