/* eslint-disable no-console */

const { execTerminalCommand } = require('@smartcoop/env-config')

const processEnvFile = require('./utils/processEnvFile')

processEnvFile({
  callback: ({ envFile }) => {
    const childProcess = execTerminalCommand(
      `cra-append-sw -m dev -e ./${ envFile } ./src/firebase-messaging-sw.js`
    )

    childProcess.on('error', error => {
      console.error(error)
    })

    childProcess.on('close', code => {
      if (code > 0) {
        console.error(`Command failed with code ${ code }`)
      } else {
        const childProcess2 = execTerminalCommand(
          `env-cmd -f ${ envFile } yarn react-app-rewired start`
        )

        childProcess2.on('error', error => {
          console.error(error)
        })

        childProcess2.on('close', code2 => {
          if (code2 > 0) {
            console.error(`Command failed with code ${ code2 }`)
          }
        })
      }
    })
  }
})
