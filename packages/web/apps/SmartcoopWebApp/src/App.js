import React, { memo } from 'react'

import { DialogProvider } from '@smartcoop/dialog'
import { I18nProvider, dictionaries } from '@smartcoop/i18n'
import { StoreProvider } from '@smartcoop/stores'
import { SnackbarsProvider } from '@smartcoop/web-components/Snackbar'
import withGlobalStyles from '@smartcoop/web-components/styles/hoc/withGlobalStyles'

import ScreenRouter from '~/screens/ScreenRouter'
import store from '~/store'

import 'moment/locale/pt-br'

// eslint-disable-next-line react/prop-types
const I18nMemorized = memo(({ children }) => (
  <I18nProvider language="pt-BR" dictionaries={ dictionaries }>
    {children}
  </I18nProvider>
))

const ScreenRouterMemorized = memo(() => <ScreenRouter />)

const App = () => (
  <StoreProvider store={ store }>
    <I18nMemorized>
      <SnackbarsProvider>
        <DialogProvider>
          <ScreenRouterMemorized />
        </DialogProvider>
      </SnackbarsProvider>
    </I18nMemorized>
  </StoreProvider>
)

export default withGlobalStyles(App)
