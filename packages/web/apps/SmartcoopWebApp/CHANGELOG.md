# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [0.18.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.17.0...v0.18.0) (2021-04-19)

**Note:** Version bump only for package @smartcoop/web-app





# [0.17.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.16.0...v0.17.0) (2021-04-15)


### Features

* translate errors messages ([6304058](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/6304058c7821fd08696f522cdf10aecdf08b7ed4))





# [0.16.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.15.0...v0.16.0) (2021-04-13)


### Bug Fixes

* **auth:** fixed first refresh token and added free actions ([fc2eea9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/fc2eea987612d3f4d4c783bb59cb3df82cc6a4d1))
* **documentos#1552:** added debounce to snackbar.error from api error ([15a2992](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/15a2992e9d6872c7620780ee223235c60277df7f)), closes [documentos#1552](http://gitlab.meta.com.br/documentos/issues/1552)


### Features

* merge com a develop ([702e143](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/702e1434c3eab83403ccbd51d34a6340a05613b9))
* wip ([a1daf61](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a1daf6149af8d967fff4afe4009a7f2a116d8917))
* **#1484:** create store weather station ([8c38c7c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8c38c7c1bb4b1c1d834d4d7d6509b97095eb8492)), closes [#1484](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1484) [#484](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/484)
* **#1531 #1532:** create list animals, create and edit ([03bc1a2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/03bc1a23a48639d11f58f59faa7868a05a34984f)), closes [#1531](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1531) [#1532](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1532) [#499](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/499)
* **#1531 #1532:** create list animals, create and edit ([97f0423](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/97f04231c8983a8789ece8acba3091fe08fe6591)), closes [#1531](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1531) [#1532](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1532) [#499](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/499)
* **documentos#1316:** added new action ([fc08c4c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/fc08c4cceb5d079c4aaf4449b3f0e12361b2e32c)), closes [documentos#1316](http://gitlab.meta.com.br/documentos/issues/1316)
* **documentos#1316:** added new action ([1ca5cd5](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1ca5cd51161eba75e407cf36c47d3a0b5d0e4344)), closes [documentos#1316](http://gitlab.meta.com.br/documentos/issues/1316)
* **documentos#1495:** modals, saga and item ([a04f400](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a04f400ff9eb8feddfbef787e6b8fd7121985631)), closes [documentos#1495](http://gitlab.meta.com.br/documentos/issues/1495)
* **documentos#1579:** mobile insemination ([1bf225d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1bf225d6f5ffbafb10226bf7c5c77b13a03ed58e)), closes [documentos#1579](http://gitlab.meta.com.br/documentos/issues/1579)
* **documentos#1579:** mobile insemination ([10c2ff2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/10c2ff2171498fa004d5fc58ac3ba9266347753a)), closes [documentos#1579](http://gitlab.meta.com.br/documentos/issues/1579)







**Note:** Version bump only for package @smartcoop/web-app





# [0.15.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.12.0-alpha.1...v0.15.0) (2021-03-23)


### Features

* resolve [#1523](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1523) ([bf353c3](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/bf353c3e7fceecbc43a53bd729d0b13e82c0ed40))
* **documentos#1292:** added securities movement store to web app ([9b8cb3a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9b8cb3a79befa28021c10180f1e1fa3ebf556ffe)), closes [documentos#1292](http://gitlab.meta.com.br/documentos/issues/1292)
* **documentos#1374:** firebase changes ([df6a585](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/df6a585a34570777c468c98c134cd8fbd855601c)), closes [documentos#1374](http://gitlab.meta.com.br/documentos/issues/1374)
* **documentos#1399:** adds v1/ suffix in api urls ([bbc3d71](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/bbc3d71a3089018ec73fc225f8c9ff8aadb24294))
* merge develop ([2731148](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2731148d7380e3ab5c8ec38d198b1773467e4364))
* resolve [#1285](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1285) ([c0b4d91](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c0b4d91ccdb7ff7adcaaf87175f1c32fe17b1c48))
* wip ([16b3eef](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/16b3eef778dda67fd818f50aaaefb2eb54901e34))





# [0.14.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.12.0-alpha.1...v0.14.0) (2021-03-23)


### Features

* resolve [#1523](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1523) ([bf353c3](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/bf353c3e7fceecbc43a53bd729d0b13e82c0ed40))
* **documentos#1292:** added securities movement store to web app ([9b8cb3a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9b8cb3a79befa28021c10180f1e1fa3ebf556ffe)), closes [documentos#1292](http://gitlab.meta.com.br/documentos/issues/1292)
* **documentos#1374:** firebase changes ([df6a585](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/df6a585a34570777c468c98c134cd8fbd855601c)), closes [documentos#1374](http://gitlab.meta.com.br/documentos/issues/1374)
* **documentos#1399:** adds v1/ suffix in api urls ([bbc3d71](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/bbc3d71a3089018ec73fc225f8c9ff8aadb24294))
* merge develop ([2731148](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2731148d7380e3ab5c8ec38d198b1773467e4364))
* resolve [#1285](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1285) ([c0b4d91](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c0b4d91ccdb7ff7adcaaf87175f1c32fe17b1c48))
* wip ([16b3eef](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/16b3eef778dda67fd818f50aaaefb2eb54901e34))





# [0.13.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.12.0-alpha.1...v0.13.0) (2021-03-23)


### Features

* resolve [#1523](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1523) ([bf353c3](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/bf353c3e7fceecbc43a53bd729d0b13e82c0ed40))
* **documentos#1292:** added securities movement store to web app ([9b8cb3a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9b8cb3a79befa28021c10180f1e1fa3ebf556ffe)), closes [documentos#1292](http://gitlab.meta.com.br/documentos/issues/1292)
* **documentos#1374:** firebase changes ([df6a585](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/df6a585a34570777c468c98c134cd8fbd855601c)), closes [documentos#1374](http://gitlab.meta.com.br/documentos/issues/1374)
* **documentos#1399:** adds v1/ suffix in api urls ([bbc3d71](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/bbc3d71a3089018ec73fc225f8c9ff8aadb24294))
* merge develop ([2731148](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2731148d7380e3ab5c8ec38d198b1773467e4364))
* resolve [#1285](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1285) ([c0b4d91](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c0b4d91ccdb7ff7adcaaf87175f1c32fe17b1c48))
* wip ([16b3eef](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/16b3eef778dda67fd818f50aaaefb2eb54901e34))





# [0.12.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.12.0-alpha.1...v0.12.0) (2021-03-23)


### Features

* resolve [#1523](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1523) ([bf353c3](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/bf353c3e7fceecbc43a53bd729d0b13e82c0ed40))
* **documentos#1292:** added securities movement store to web app ([9b8cb3a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9b8cb3a79befa28021c10180f1e1fa3ebf556ffe)), closes [documentos#1292](http://gitlab.meta.com.br/documentos/issues/1292)
* **documentos#1374:** firebase changes ([df6a585](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/df6a585a34570777c468c98c134cd8fbd855601c)), closes [documentos#1374](http://gitlab.meta.com.br/documentos/issues/1374)
* **documentos#1399:** adds v1/ suffix in api urls ([bbc3d71](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/bbc3d71a3089018ec73fc225f8c9ff8aadb24294))
* merge develop ([2731148](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2731148d7380e3ab5c8ec38d198b1773467e4364))
* resolve [#1285](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1285) ([c0b4d91](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c0b4d91ccdb7ff7adcaaf87175f1c32fe17b1c48))
* wip ([16b3eef](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/16b3eef778dda67fd818f50aaaefb2eb54901e34))





# [0.12.0-alpha.1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.12.0-alpha.0...v0.12.0-alpha.1) (2021-02-22)


### Bug Fixes

* **auth:** fixed multiples refresh token ([b976597](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b976597b48bfce6639d77453f295a8a3437d836a))


### Features

* merge develop ([c7c8f96](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c7c8f96d843e6c6e4edffbcef241c3c9f096257b))
* **documentos#1115:** created barter store and api resource ([9a35185](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9a3518557cc700310607d6920ae6f739a4733bb1)), closes [documentos#1115](http://gitlab.meta.com.br/documentos/issues/1115)
* wip ([452dd2d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/452dd2d1eaeeddea616d23ca757bcba9864baab8))
* **documentos#535:** finished home screen for mobile ([b595eee](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b595eeed9efc2c6371bb04276a22abfd74a1d9db)), closes [documentos#535](http://gitlab.meta.com.br/documentos/issues/535)





# [0.12.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.11.0...v0.12.0-alpha.0) (2021-02-12)

**Note:** Version bump only for package @smartcoop/web-app





# [0.11.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.11.0-alpha.0...v0.11.0) (2021-02-12)


### Features

* resolve [#1158](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1158) and [#1159](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1159) ([2f6d494](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2f6d49444320d8c3ba78955b7073e81ca422dd3a))





# [0.11.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.10.0...v0.11.0-alpha.0) (2021-02-05)

**Note:** Version bump only for package @smartcoop/web-app





# [0.10.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.10.0-alpha.0...v0.10.0) (2021-02-01)


### Bug Fixes

* onboarding map location ([ed3d816](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ed3d816045f3b4d6312bd316e7cf063b14198ef3))
* **firebase:** fixed on web ([e048482](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e04848292e5f0c5b775a497e364b39ec0ea3cf8d))


### Features

* **documentos#987:** removes duplicated key in reducer ([36860fe](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/36860fe1ff83250b28e31b880f34d4f6586a4caf)), closes [documentos#987](http://gitlab.meta.com.br/documentos/issues/987)
* resolve merge conflicts ([d76333d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d76333db93b61f9897987ee033d050dcdc48d8fe))
* wip ([ea43347](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ea4334798c6090ec6819e6390596359d2bf439de))
* wip ([094d424](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/094d4242f82345f35ecd7b4227e3c497da75c966))
* **#1003:** install new lib for mobile Accordion ([b3a75f8](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b3a75f8a47a51f23e01893ec58953177c139f3d7)), closes [#1003](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1003)
* **documentos#987:** declares reducer and saga ([0593ecd](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0593ecdbb3845ecf92aad3420f40fe8e258b266f)), closes [documentos#987](http://gitlab.meta.com.br/documentos/issues/987)





# [0.10.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.9.0...v0.10.0-alpha.0) (2021-01-19)

**Note:** Version bump only for package @smartcoop/web-app





# [0.9.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.8.2...v0.9.0) (2021-01-19)


### Bug Fixes

* **documentos#860:** url api ([8a371ad](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8a371ad014bfe8fe248051358eadecbc9a4e7e70)), closes [documentos#860](http://gitlab.meta.com.br/documentos/issues/860)


### Features

* added sales order delete ([ff4f588](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ff4f588585eb4c76f9a544ee4c650d5551eae85b))
* **#901:** create actions for price history ([a60b133](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a60b13355f3b721a6c056f893c82a3838eeb1401)), closes [#901](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/901) [#241](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/241)
* **documentos#299:** http -> https ([1beb6bc](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1beb6bc04fa20480556416d70dadba2bfb391ec7)), closes [documentos#299](http://gitlab.meta.com.br/documentos/issues/299)
* **documentos#836:** finished mobile list ([6cbf8b3](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/6cbf8b38ee2ef54367d0503f607dc38386ca56e8)), closes [documentos#836](http://gitlab.meta.com.br/documentos/issues/836)
* **maps:** added google layer ([eddf3e9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/eddf3e9e410bcf702861d1ba57aaa02bde87e935))





## [0.8.2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.8.1...v0.8.2) (2021-01-06)


### Features

* added smartcoop favicon ([346035b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/346035bdadddb8329b6f4a33447452369fef95aa))





## [0.8.1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.9.0-alpha.0...v0.8.1) (2021-01-06)

**Note:** Version bump only for package @smartcoop/web-app





# [0.9.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.8.0...v0.9.0-alpha.0) (2021-01-05)

**Note:** Version bump only for package @smartcoop/web-app





# [0.8.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.8.0-alpha.0...v0.8.0) (2021-01-05)


### Features

* **documentos#300:** created refresh token mechanism ([54ee2f1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/54ee2f1626aee3e6c60d7a1412305fce7fef5d01)), closes [documentos#300](http://gitlab.meta.com.br/documentos/issues/300)
* **documentos#491:** create order form ([2b8653f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2b8653f777465d35bc1075bcc7578d4259e88863)), closes [documentos#491](http://gitlab.meta.com.br/documentos/issues/491)
* **documentos#506:** declares store in mobile and web ([5dc34d2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/5dc34d209ef99d39b81dce052d2f4d31a1bfee32)), closes [documentos#506](http://gitlab.meta.com.br/documentos/issues/506)





# [0.8.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.7.0...v0.8.0-alpha.0) (2020-12-11)

**Note:** Version bump only for package @smartcoop/web-app





# [0.7.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.7.0-alpha.0...v0.7.0) (2020-12-11)


### Bug Fixes

* **#407:** fix bug that showed snackbar when login fails ([86940eb](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/86940ebeba54da6eba2a629a4a8855a25fd162e6)), closes [#164](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/164)
* **documentos#440:** showing backend message errors ([e3d55ec](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e3d55ec742957bdcfd313571e6336d58cdaee507)), closes [documentos#440](http://gitlab.meta.com.br/documentos/issues/440)


### Features

* **documentos#195:** added reject, remove and edit supplier quotation ([c6e80bf](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c6e80bfd4545de18ff4c16c214f88d58f085bf29)), closes [documentos#195](http://gitlab.meta.com.br/documentos/issues/195)
* **documentos#195:** fixed default values ([ac87b49](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ac87b49c6394b75ccde6eefe351d2a73e2a3aa1c))
* **documentos#402:** editing growing season ([321da9c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/321da9cb75b830d048c43afa491b4f2911e58b03)), closes [documentos#402](http://gitlab.meta.com.br/documentos/issues/402)





# [0.7.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.6.0...v0.7.0-alpha.0) (2020-11-27)

**Note:** Version bump only for package @smartcoop/web-app





# [0.6.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.5.3...v0.6.0) (2020-11-27)


### Features

* **documentos#107:** populates list ([5dadeeb](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/5dadeeb5ce4132191ef29a9fe73b3667ba4b2ca6)), closes [documentos#107](http://gitlab.meta.com.br/documentos/issues/107)
* **documentos#108:** creates storage of accounts and accounts balance ([65f1beb](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/65f1beb25e36efa0f3fb9c2919ad41907cdc87a5)), closes [documentos#108](http://gitlab.meta.com.br/documentos/issues/108)
* **documentos#16:** added growing season store to web and mobile ([d9a586e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d9a586e67f947be1fe933777e864576cf842d89e)), closes [documentos#16](http://gitlab.meta.com.br/documentos/issues/16)



# [0.6.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.5.0...v0.6.0-alpha.0) (2020-11-10)





# [0.6.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.5.0...v0.6.0-alpha.0) (2020-11-10)

**Note:** Version bump only for package @smartcoop/web-app
## [0.5.3](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.5.2...v0.5.3) (2020-11-11)

**Note:** Version bump only for package @smartcoop/web-app





## [0.5.2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.5.1...v0.5.2) (2020-11-11)

**Note:** Version bump only for package @smartcoop/web-app





## [0.5.1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.5.0...v0.5.1) (2020-11-10)


### Bug Fixes

* **staging:** fixed http connection and turned of datatable sorting ([eccae09](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/eccae098f786dadcfdc7d24723f9c80df106e6fb))





# [0.5.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.5.0-alpha.0...v0.5.0) (2020-11-10)

**Note:** Version bump only for package @smartcoop/web-app





# [0.5.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.4.0...v0.5.0-alpha.0) (2020-10-27)

**Note:** Version bump only for package @smartcoop/web-app





# [0.4.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.3.0-alpha.0...v0.4.0) (2020-10-27)


### Features

* **ci-cd:** created ci-cd ([95ea44c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/95ea44c9bbe5df17ded2cf836abe4744e5287814))
* **documentos#135:** sorting out user reducer ([f49641f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f49641f9fe65c0e080a2d5750ec281fd72f52843)), closes [documentos#135](http://gitlab.meta.com.br/documentos/issues/135)
* **documentos#185:** created basic navigation for web and mobile ([4b144ed](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4b144edcd2c7d7338100f5da5105df4ccec87969)), closes [documentos#185](http://gitlab.meta.com.br/documentos/issues/185)
* **documentos#25:** created field store ([0373246](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0373246f26c06e6d33e61ec5bdbbce10822a0c35)), closes [documentos#25](http://gitlab.meta.com.br/documentos/issues/25)
* **services:** turn cloud api on ([e3739f3](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e3739f3cb0a8b8507d60ba2575e02bb1c85b2615))





# [0.3.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.2.0...v0.3.0-alpha.0) (2020-09-28)

**Note:** Version bump only for package @smartcoop/web-app





# [0.2.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.2.0-alpha.0...v0.2.0) (2020-09-28)



### Features

* **documentos#10:** created web login screen ([34ed647](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/34ed647f1ab16e0c1dc45db8455239335ed6286d)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#10:** finished productor onboarding ([4eb4a89](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4eb4a89ed42ba9a8161999c29bf941a95c5c1119)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#10:** login for mobile ([39b6072](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/39b60720db516f0b8e669d8691d4f61e404144e2)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#32:** added database into web ([1f6ad73](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1f6ad73cc2f1de976bce6aab7835337f1f8d2e05)), closes [documentos#32](http://gitlab.meta.com.br/documentos/issues/32)
* **documentos#32:** created @smartcoop/database with watermelondb ([f3040da](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f3040dadc35c09bad7fe4751508b0c4fdfd45426)), closes [documentos#32](http://gitlab.meta.com.br/documentos/issues/32)
* **documentos#32:** created service smartcoopApi ([0568f87](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0568f873e9d2f0d1fccde3f53d4c29651222cadd)), closes [documentos#32](http://gitlab.meta.com.br/documentos/issues/32)
* **documentos#32:** migrated database apps to apps ([1b35dec](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1b35decd02507f4f8ee6cdc1f6e9a06f1c8474f5)), closes [documentos#32](http://gitlab.meta.com.br/documentos/issues/32)
* **documentos#32:** removed watermelon. added redux saga ([cd97772](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/cd977725490d8654afc2dad1a0e7bc3d6b16b4dd)), closes [documentos#32](http://gitlab.meta.com.br/documentos/issues/32)





# [0.2.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.1.0...v0.2.0-alpha.0) (2020-09-13)

**Note:** Version bump only for package @smartcoop/web-app





# [0.1.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.1.0-alpha.0...v0.1.0) (2020-09-13)


### Features

* **documentos#10:** applying documentos[#37](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/37) into documentos[#39](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/39) ([c8e043e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c8e043ee97e72ea17b2ebd347852ea2cd999e755)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#10:** login and signup screens ([e4b9d02](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e4b9d0278f4785aa31928f9ec3bc6e5de4ff98e2)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **forms:** @smarcoop/forms finished ([d95dc20](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d95dc20603902124c3b35f531986dcb08e108879))
* **forms:** single validate for web and mobile forms ([9bbeb71](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9bbeb71f1a9c0acac8d6e817e614159eda808a5d))





# 0.1.0-alpha.0 (2020-08-21)

**Note:** Version bump only for package @smartcoop/web-app
