import React, { useMemo, useCallback, useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import PropTypes from 'prop-types'

import { split } from 'lodash'
import find from 'lodash/find'
import isEmpty from 'lodash/isEmpty'
import map from 'lodash/map'

import { useT } from '@smartcoop/i18n'
import { organizationRounded } from '@smartcoop/icons'
import ItemSelection from '@smartcoop/mobile-components/ItemSelection'
import Modal from '@smartcoop/mobile-components/Modal'
import { getTokenByOrganization } from '@smartcoop/services/apis/smartcoopApi/resources/authentication'
import { useSnackbar } from '@smartcoop/snackbar'
import { AuthenticationActions } from '@smartcoop/stores/authentication'
import {
  selectAccessToken,
  selectRefreshToken
} from '@smartcoop/stores/authentication/selectorAuthentication'
import { selectCurrentModule } from '@smartcoop/stores/module/selectorModule'
import { OrganizationActions } from '@smartcoop/stores/organization'
import {
  selectCurrentOrganization,
  selectUserOrganizationsByModule
} from '@smartcoop/stores/organization/selectorOrganization'
import colors from '@smartcoop/styles/colors'

import { Scroll } from './styles'

const ChangeOrganizationModal = ({ id, open, handleClose }) => {
  const t = useT()
  const snackbar = useSnackbar()
  const dispatch = useCallback(useDispatch(), [])

  const { organizations, module } = useSelector(selectUserOrganizationsByModule)
  const currentModule = useSelector(selectCurrentModule)
  const currentOrganization = useSelector(selectCurrentOrganization)
  const oldAccessToken = useSelector(selectAccessToken)
  const oldRefreshToken = useSelector(selectRefreshToken)

  const [loading, setLoading] = useState(true)
  const [options, setOptions] = useState([])

  const beforeSelected = useMemo(
    () => currentOrganization,
    // eslint-disable-next-line react-hooks/exhaustive-deps
    []
  )

  useEffect(() => {
    if (module === currentModule) {
      const array = map(organizations, (item) => ({
        ...item,
        value: item.id,
        label: `${ item.tradeName || item.companyName } ${
          item.registry ? `- ${ item.registry }` : ''
        }`
      }))
      setOptions(array)
    }
  }, [currentModule, module, organizations])

  const onChange = useCallback(
    async (org) => {
      setLoading(true)

      const organization = find(
        organizations,
        (item) => item.id === org.value && item.registry === org.registry
      )

      getTokenByOrganization(
        {
          accessToken: split(oldAccessToken, ' ')[1],
          refreshToken: oldRefreshToken,
          registry: organization.registry
        },
        { organizationId: organization.id }
      )
        .then(({ accessToken, refreshToken, expiresAt, tokenType }) => {
          dispatch(
            AuthenticationActions.refreshTokenSuccess(
              `${ tokenType } ${ accessToken }`,
              refreshToken,
              expiresAt,
              () => {
                dispatch(
                  OrganizationActions.setCurrentOrganization(
                    organization,
                    () => {
                      setLoading(true)
                      handleClose()
                    },
                    () => setLoading(false)
                  )
                )
              }
            )
          )
        })
        .catch((err) => {
          // eslint-disable-next-line no-console
          console.log(
            '%cMyProject%cline:105%cerr',
            'color:#fff;background:#ee6f57;padding:3px;border-radius:2px',
            'color:#fff;background:#1f3c88;padding:3px;border-radius:2px',
            'color:#fff;background:rgb(96, 143, 159);padding:3px;border-radius:2px',
            err
          )
          snackbar.error(t('an error ocurred'))
          setLoading(false)
        })
    },
    [
      dispatch,
      handleClose,
      oldAccessToken,
      oldRefreshToken,
      organizations,
      snackbar,
      t
    ]
  )

  const selectedOrganization = useMemo(() => {
    if (isEmpty(beforeSelected)) return {}

    return {
      ...beforeSelected,
      value: beforeSelected.id,
      label: `${ beforeSelected.tradeName || beforeSelected.companyName } ${
        beforeSelected.registry ? `- ${ beforeSelected.registry }` : ''
      }`
    }
  }, [beforeSelected])

  const filterShowItems = useCallback(
    (option, selected) =>
      option.registry !== selected.registry || option.value !== selected.value,
    []
  )

  useEffect(() => {
    dispatch(
      OrganizationActions.loadUserOrganizationsByModule(
        () => {},
        () => setLoading(false)
      )
    )
  }, [dispatch])

  useEffect(() => {
    if (!isEmpty(organizations)) {
      if (organizations.length === 1) {
        const array = map(organizations, (item) => ({
          ...item,
          value: item.id,
          label: `${ item.tradeName || item.companyName } ${
            item.registry ? `- ${ item.registry }` : ''
          }`
        }))

        onChange(array[0])
      } else {
        setLoading(false)
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [organizations])

  return (
    <Modal
      id={ id }
      open={ open }
      title={ t('select one organization') }
      maxWidth="xs"
      loading={ loading }
      disableFullScreen
      escapeWhenLoading
      escape={ !isEmpty(beforeSelected) }
    >
      <Scroll>
        <ItemSelection
          title={ !isEmpty(beforeSelected) ? t('other organizations') : '' }
          options={ options }
          filterFunction={ filterShowItems }
          unselectedIcon={ organizationRounded }
          unselectedIconColor={ colors.primary }
          selectedIcon={ organizationRounded }
          selectedIconColor={ colors.green }
          onChange={ onChange }
          selected={ selectedOrganization }
        />
      </Scroll>
    </Modal>
  )
}

ChangeOrganizationModal.propTypes = {
  id: PropTypes.string.isRequired,
  open: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired
}

export default ChangeOrganizationModal
