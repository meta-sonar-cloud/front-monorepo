import React, { useCallback } from 'react'
import { TouchableOpacity } from 'react-native'
import { useDispatch } from 'react-redux'

import PropTypes from 'prop-types'

import I18n, { useT } from '@smartcoop/i18n'
import { barn } from '@smartcoop/icons'
import Button from '@smartcoop/mobile-components/Button'
import Icon from '@smartcoop/mobile-components/Icon'
import Modal from '@smartcoop/mobile-components/Modal'
import { ModuleActions } from '@smartcoop/stores/module'
import colors from '@smartcoop/styles/colors'

import { Container, Text, TextUnderlined } from './styles'

const ZeroPropertiesModal = ({ id, open, handleClose, onConfirm, onCancel }) => {

  const dispatch = useCallback(useDispatch(), [])

  const exitModule = useCallback(
    () => dispatch(ModuleActions.exitCurrentModule()),
    [dispatch]
  )

  const t = useT()
  return (
    <Modal
      id={ id }
      open={ open }
      hideHeader
    >
      <Container>

        <Text style={ { marginTop: 0 } }>
          <I18n>your registration was effected!</I18n>
        </Text>

        <Icon icon={ barn } size={ 85 } />

        <Text>
          <I18n>{'to start, let\'s register your property!'}</I18n>
        </Text>

        <Button
          title={ t('register property') }
          onPress={ () => {
            handleClose()
            onConfirm()
          } }
          backgroundColor={ colors.secondary }
          color={ colors.text }
        >
          <I18n>register property</I18n>
        </Button>

        <TouchableOpacity
          onPress={ () => {
            exitModule()
            handleClose()
            onCancel()
          } }
        >
          <TextUnderlined
            style={ { marginTop: 20 } }
          >
            <I18n>exit module</I18n>
          </TextUnderlined>
        </TouchableOpacity>

      </Container>
    </Modal>
  )
}

ZeroPropertiesModal.propTypes = {
  id: PropTypes.string.isRequired,
  open: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  onConfirm: PropTypes.func,
  onCancel: PropTypes.func
}

ZeroPropertiesModal.defaultProps = {
  onConfirm: () => {},
  onCancel: () => {}
}

export default ZeroPropertiesModal
