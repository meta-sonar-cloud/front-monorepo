import styled from 'styled-components/native'

import colors from '@smartcoop/styles/colors'

export const Container = styled.View`
  flex-direction: column;
  justify-content: space-between;
  align-items: center;
  width: 100%;
  padding: 0%;
  padding: 25px 30px;
`

export const HeaderText = styled.Text`
  font-size: 22px;
  font-weight: bold;
  text-align: center;
  margin-bottom: 22px;
  color: ${ colors.text };
`

export const Text = styled.Text`
  text-align: center;
  color: ${ colors.text };
  font-weight: bold;
  font-size: 16px;
  margin: 20px 0;
`

export const TextUnderlined = styled.Text`
  font-size: 14px;
  text-decoration: underline;
`
