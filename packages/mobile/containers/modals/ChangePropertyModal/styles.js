import styled from 'styled-components/native'

export const Container = styled.View`
  justify-content: space-between;
  align-items: center;
  width: 100%;
  padding: 0%;
`

export const AddNewAreaContainer = styled.TouchableOpacity`
    /* width: 100%; */
    display: flex;
    flex-direction: row;
    justify-content: space-evenly;
    align-items: center;
    font-size: 16px;
    font-weight: 600;
    padding-top: 30px;
`

export const FooterText = styled.Text`
    font-weight: 600;
    font-size: 16px;
    line-height: 16px;
    margin: 10px;
    margin-top: 15px;
    align-items: center;
    align-content: center;
`

export const Scroll = styled.ScrollView.attrs({
  keyboardShouldPersistTaps: 'always'
})`
  width: 250px;
`
