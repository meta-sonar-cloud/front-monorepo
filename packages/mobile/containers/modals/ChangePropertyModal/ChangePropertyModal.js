import React, { useMemo, useCallback, useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import PropTypes from 'prop-types'

import find from 'lodash/find'
import isEmpty from 'lodash/isEmpty'
import map from 'lodash/map'

import { useT } from '@smartcoop/i18n'
import { plus, organizationRounded } from '@smartcoop/icons'
import Icon from '@smartcoop/mobile-components/Icon'
import ItemSelection from '@smartcoop/mobile-components/ItemSelection'
import Modal from '@smartcoop/mobile-components/Modal'
import { useSnackbar } from '@smartcoop/snackbar'
import { PropertyActions } from '@smartcoop/stores/property'
import {
  selectProperties,
  selectCurrentProperty
} from '@smartcoop/stores/property/selectorProperty'
import colors from '@smartcoop/styles/colors'

import { AddNewAreaContainer, FooterText, Scroll } from './styles'

const ChangePropertyModal = ({ id, open, handleClose, onPressButton }) => {
  const t = useT()
  const snackbar = useSnackbar()
  const dispatch = useCallback(useDispatch(), [])

  const properties = useSelector(selectProperties)

  const currentProperty = useSelector(selectCurrentProperty)

  const [loading, setLoading] = useState(false)

  useEffect(() => {
    setLoading(true)
    dispatch(
      PropertyActions.loadProperties(
        () => {
          setLoading(false)
        },
        (err) => {
          snackbar.error(err)
          setLoading(false)
        }
      )
    )
  }, [dispatch, snackbar])

  const beforeSelected = useMemo(
    () => currentProperty,
    // eslint-disable-next-line react-hooks/exhaustive-deps
    []
  )

  const options = useMemo(
    () =>
      map(properties, (item) => ({
        value: item.id,
        label: item.name
      })),
    [properties]
  )

  const handleAddNewArea = useCallback(() => {
    onPressButton()
  }, [onPressButton])

  const selectedProperty = useMemo(() => {
    if (isEmpty(beforeSelected)) return {}

    return {
      value: beforeSelected.id,
      label: beforeSelected.name
    }
  }, [beforeSelected])

  const onChange = useCallback(
    (org) => {
      if (org.value !== selectedProperty?.value) {
        const property = find(properties, (item) => item.id === org.value)
        dispatch(PropertyActions.saveCurrentProperty(property))
      }
      handleClose()
    },
    [dispatch, handleClose, properties, selectedProperty.value]
  )

  const footerButton = () => (
    <AddNewAreaContainer onPress={ handleAddNewArea }>
      <Icon icon={ plus } size={ 16 } />
      <FooterText>{t('register new property')}</FooterText>
    </AddNewAreaContainer>
  )

  return (
    <Modal
      id={ id }
      open={ open }
      title={ t('select one property') }
      maxWidth="xs"
      disableFullScreen
      escapeWhenLoading
      escape={ !isEmpty(beforeSelected) }
      FooterComponent={ onPressButton ? footerButton : undefined }
      loading={ loading }
    >
      <Scroll>
        <ItemSelection
          title={ !isEmpty(beforeSelected) ? t('other properties') : '' }
          options={ options }
          unselectedIcon={ organizationRounded }
          unselectedIconColor={ colors.primary }
          selectedIcon={ organizationRounded }
          selectedIconColor={ colors.green }
          onChange={ onChange }
          selected={ selectedProperty }
        />
      </Scroll>
    </Modal>
  )
}

ChangePropertyModal.propTypes = {
  id: PropTypes.string.isRequired,
  open: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  onPressButton: PropTypes.func
}

ChangePropertyModal.defaultProps = {
  onPressButton: undefined
}

export default ChangePropertyModal
