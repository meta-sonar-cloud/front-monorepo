import React, { useCallback } from 'react'
import { View, Text, TouchableOpacity } from 'react-native'



import PropTypes from 'prop-types'

import I18n from '@smartcoop/i18n'
import { camera, picture } from '@smartcoop/icons'
import Divider from '@smartcoop/mobile-components/Divider'
import Icon from '@smartcoop/mobile-components/Icon'
import Modal from '@smartcoop/mobile-components/Modal'
import colors from '@smartcoop/styles/colors'

import { Container, ButtonGroup, ButtonPicture, Title } from './styles'

const SelectedImageTypeModal = (props) => {
  const {
    open,
    handleClose,
    id,
    openCamera,
    openGallery
  } = props


  const openTypeCamera = useCallback(
    () => {
      openCamera()
      handleClose()
    },
    [handleClose, openCamera]
  )

  const openTypeGallery = useCallback(
    () => {
      openGallery()
      handleClose()
    },
    [handleClose, openGallery]
  )

  return (
    <Modal id={ id } open={ open }>
      <Container>
        <View style={ { alignSelf: 'flex-start' } }>
          <Title>
            <I18n style={ { fontSize: 18 } }>add images</I18n>
          </Title>
        </View>
        <Divider />
        <ButtonGroup>
          <TouchableOpacity onPress={ openTypeCamera }>
            <ButtonPicture>
              <Icon icon={ camera } size={ 41 } color={ colors.darkGrey } />
              <Text>Tirar foto</Text>
            </ButtonPicture>
          </TouchableOpacity>
          <View style={ { width: '5%' } } />
          <TouchableOpacity onPress={ openTypeGallery }>
            <ButtonPicture>
              <Icon icon={ picture } size={ 41 } color={ colors.darkGrey } />
              <Text>Galeria</Text>
            </ButtonPicture>
          </TouchableOpacity>
        </ButtonGroup>
      </Container>
    </Modal>
  )
}

SelectedImageTypeModal.propTypes = {
  id: PropTypes.string.isRequired,
  open: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  openCamera: PropTypes.func,
  openGallery: PropTypes.func
}

SelectedImageTypeModal.defaultProps = {
  openCamera: () => {},
  openGallery: () => {}
}

export default SelectedImageTypeModal
