import styled from 'styled-components/native'

import colors from '@smartcoop/styles/colors'

export const Container = styled.View`
  justify-content: center;
  padding: 0;
  margin: 0;
`

export const ButtonGroup = styled.View`
  flex-direction: row;
  align-self: center;
  margin-top: 20px;
`

export const TextLabel = styled.Text`
  font-size: 16px;
  padding-right: 100px;
  color: ${ colors.mutedText };
  margin: 0;
`

export const TextVariable = styled.Text`
  font-size: 16px;
  color: ${ colors.text };
  margin: 5px 0 15px;
`

