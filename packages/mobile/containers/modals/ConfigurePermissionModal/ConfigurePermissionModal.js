import React, { useCallback, useRef, useMemo } from 'react'
import { View } from 'react-native'
import { useDispatch } from 'react-redux'

import PropTypes from 'prop-types'

import userConfigurePermission from '@smartcoop/forms/schemas/property/userConfigurePermission.schema'
import I18n, { useT } from '@smartcoop/i18n'
import Button from '@smartcoop/mobile-components/Button'
import Divider from '@smartcoop/mobile-components/Divider'
import Form from '@smartcoop/mobile-components/Form'
import Modal from '@smartcoop/mobile-components/Modal'
import RadioGroup from '@smartcoop/mobile-components/RadioGroup'
import { useSnackbar } from '@smartcoop/snackbar'
import { TechnicalActions } from '@smartcoop/stores/technical'
import colors from '@smartcoop/styles/colors'

import { Container, ButtonGroup, TextLabel, TextVariable } from './styles'

const ConfigurePermissionModal = (props) => {
  const {
    open,
    handleClose,
    id,
    onSuccess,
    item
  } = props

  const t = useT()
  const configurePermissionFormRef = useRef()
  const snackbar = useSnackbar()
  const dispatch = useCallback(useDispatch(), [])

  const applicationModeOptions = useMemo(
    () => (
      [
        {
          label: 'Somente visualização',
          value: 4
        },
        {
          label: 'Visualização e edição',
          value: 5
        },
        {
          label: 'Negar acesso',
          value: 2
        }
      ]
    ),
    []
  )

  const permissions = useMemo(
    () => ({
      read: 4,
      readAndWrite: 5,
      denied: 2
    }),
    []
  )

  const handleSubmit = useCallback(
    (data) => {
      if (data.permission === permissions.denied) {
        dispatch(TechnicalActions.revokeTechnicalAccess(
          item.technicianProprietary?.id,
          data.permission,
          () => {
            snackbar.success(
              t('permission change performed successfully')
            )
            handleClose()
            onSuccess()
          }
        ))
      } else {
        dispatch(TechnicalActions.acceptTechnicianAccess(
          data.permission === permissions.readAndWrite,
          item.technicianProprietary?.id,
          () => {
            snackbar.success(
              t('permission change performed successfully')
            )
            handleClose()
            onSuccess()
          }
        ))
      }
    },
    [dispatch, handleClose, item.technicianProprietary.id, onSuccess, permissions.denied, permissions.readAndWrite, snackbar, t]
  )

  const handleClean = useCallback(
    () => {
      handleClose()
    },
    [handleClose]
  )

  return (
    <Modal
      id={ id }
      open={ open }
      title={ t('configure permission') }
    >
      <Container>
        <Divider />
        <View>
          {item.technicianProprietary?.organizations[0] && (
            <View>
              <TextVariable>{item.technicianProprietary?.organizations[0]?.tradeName}</TextVariable>
            </View>
          )}
          <View>
            <TextLabel><I18n>user name</I18n></TextLabel>
            <TextVariable>{item.name}</TextVariable>
          </View>
        </View>
        <Form
          ref={ configurePermissionFormRef }
          schemaConstructor={ userConfigurePermission }
          onSubmit={ handleSubmit }
        >
          <RadioGroup
            style={ { marginBottom: 20, maxWidth: 200 } }
            label={ t('permission') }
            name="permission"
            options={ applicationModeOptions }
            itemStyle={ { paddingTop: 14, paddingBottom: 14, paddingLeft: 11, paddingRight: 11 } }
            defaultValue={ item.technicianProprietary.status || '' }
            variant="column"
          />
          <ButtonGroup>
            <Button
              title={ t('cancel') }
              color={ colors.black }
              variant='outlined'
              onPress={ handleClean }
              style={ { marginRight: '5%' ,minWidth: '40%', maxWidth: '45%', textTransform: 'none' } }
            />
            <Button
              title={ t('save') }
              onPress={ () => configurePermissionFormRef.current.submit() }
              color={ colors.white }
              style={ { backgroundColor: colors.black, minWidth: '40%', maxWidth: '45%',  textTransform: 'none' } }
            />
          </ButtonGroup>
        </Form>
      </Container>
    </Modal>
  )
}

ConfigurePermissionModal.propTypes = {
  id: PropTypes.string.isRequired,
  open: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  onSuccess: PropTypes.func,
  item: PropTypes.object
}

ConfigurePermissionModal.defaultProps = {
  onSuccess: () => {},
  item: {}
}

export default ConfigurePermissionModal
