import React, { useCallback, useRef } from 'react'
import { View } from 'react-native'

import PropTypes from 'prop-types'

import I18n, { useT } from '@smartcoop/i18n'
import { filter } from '@smartcoop/icons'
import Button from '@smartcoop/mobile-components/Button'
import Divider from '@smartcoop/mobile-components/Divider'
import Icon from '@smartcoop/mobile-components/Icon'
import Modal from '@smartcoop/mobile-components/Modal'
import FilterAnimalPevForm from '@smartcoop/mobile-containers/forms/digitalProperty/dairyFarm/FilterAnimalPevForm'
import colors from '@smartcoop/styles/colors'

import { Container, ButtonGroup, Title } from './styles'

const FilterAnimalPevModal = (props) => {
  const {
    open,
    handleClose,
    id,
    onSubmit,
    filters
  } = props

  const t = useT()
  const filterFormRef = useRef(null)

  const handleSubmit = useCallback(
    (value) => {
      onSubmit(value)
      handleClose()
    },
    [handleClose, onSubmit]
  )

  const clearForm = useCallback(
    () => {
      filterFormRef.current.reset()
      onSubmit({})
      handleClose()
    },
    [handleClose, onSubmit]
  )


  return (
    <Modal id={ id } open={ open }>
      <Container>
        <View style={ { alignSelf: 'flex-start' } }>
          <Title>
            <Icon icon={ filter } size={ 18 } />
            <I18n style={ { fontSize: 18 } }>filter</I18n>
          </Title>
        </View>
        <Divider />
        <FilterAnimalPevForm
          ref={ filterFormRef }
          onSubmit={ handleSubmit }
          withoutSubmitButton
          filters={ filters }
        />
        <ButtonGroup>
          <Button
            title={ t('clear') }
            color={ colors.black }
            variant='outlined'
            onPress={ clearForm }
            style={ { marginRight: '5%', minWidth: '35%', maxWidth: '40%', textTransform: 'none' } }
          />
          <Button
            title={ t('filtrate') }
            onPress={ () => filterFormRef.current.submit() }
            color={ colors.white }
            style={ { backgroundColor: colors.black, minWidth: '35%', maxWidth: '40%',  textTransform: 'none' } }
          />
        </ButtonGroup>
      </Container>
    </Modal>
  )
}

FilterAnimalPevModal.propTypes = {
  id: PropTypes.string.isRequired,
  open: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  onSubmit: PropTypes.func,
  filters: PropTypes.object
}

FilterAnimalPevModal.defaultProps = {
  onSubmit: () => {},
  filters: {}
}

export default FilterAnimalPevModal
