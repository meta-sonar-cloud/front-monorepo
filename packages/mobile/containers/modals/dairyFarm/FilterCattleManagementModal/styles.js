import styled from 'styled-components/native'

export const Container = styled.View`
  justify-content: center;
  align-items: center;
  padding: 0;
  margin: 0;
`

export const ButtonGroup = styled.View`
  flex-direction: row;
  margin-top: 20px;
`

export const Title = styled.Text`
  font-weight: 700;
  font-size: 16px;
  margin: 0;
  padding: 0;
`
