import React, { useCallback } from 'react'
import { useDispatch } from 'react-redux'

import PropTypes from 'prop-types'

import I18n, { useT } from '@smartcoop/i18n'
import { notFound } from '@smartcoop/icons'
import Button from '@smartcoop/mobile-components/Button'
import Icon from '@smartcoop/mobile-components/Icon'
import Modal from '@smartcoop/mobile-components/Modal'
import { AuthenticationActions } from '@smartcoop/stores/authentication'

import {
  Container,
  Content,
  Buttons
} from './styles'

const AcceptTermModalConfirm = (props) => {
  const {
    id,
    open,
    handleClose,
    onWillClose,
    onCancel } = props

  const dispatch = useCallback(useDispatch(), [])
  const t = useT()

  const onClose = useCallback(
    () => {
      onWillClose()
      handleClose()
    }, [handleClose, onWillClose]
  )

  const cancel = useCallback(
    () => {
      dispatch(AuthenticationActions.logout())
      onCancel()
    }, [dispatch, onCancel]
  )

  const onSubmit = useCallback(
    () => {
      onClose()
    }, [onClose]
  )


  return (
    <Modal
      id={ id }
      open={ open }
      escape={ false }
      title={ t('attention') }
      disableFullScreen
      maxWidth="md"
    >

      <Container>
        <Content>
          <Content>
            <Icon icon={ notFound } size={ 125 } style={ { padding: 15 } } />
            <I18n>
            you need to accept the term to proceed
            </I18n>
          </Content>
        </Content>
        <Buttons>
          <Button
            id="cancel-button"
            onPress={ cancel }
            style={ { flex: 1 } }
            variant="outlined"
            title={ t('logout') }
          />
          <Button
            id="confirm-button"
            onPress={ onSubmit }
            style={ { flex: 1 } }
            title={ t('confirm') }
          />
        </Buttons>
      </Container>
    </Modal>
  )
}

AcceptTermModalConfirm.propTypes = {
  id: PropTypes.string.isRequired,
  open: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  onWillClose: PropTypes.func,
  onCancel: PropTypes.func
}

AcceptTermModalConfirm.defaultProps = {
  onWillClose: () => {},
  onCancel: () => {}
}



export default AcceptTermModalConfirm
