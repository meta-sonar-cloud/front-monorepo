import React, { useCallback, useRef } from 'react'
import { View } from 'react-native'

import PropTypes from 'prop-types'

import I18n from '@smartcoop/i18n'
import Modal from '@smartcoop/mobile-components/Modal'
import MilkDeliveryForm from '@smartcoop/mobile-containers/forms/digitalProperty/dairyFarm/MilkDeliveryForm/MilkDeliveryForm'

import useStyles, {  Content } from './styles'

const MilkDeliveryModal = ({ id, open, dairyFarm, handleClose, onCancel }) => {
  const classes = useStyles()
  const milkDeliveryFormRef = useRef(null)

  const clearForm = useCallback(
    () => {
      milkDeliveryFormRef.current.reset()
      handleClose()
      onCancel()
    },
    [handleClose, onCancel]
  )

  return (
    <Modal
      id={ id }
      open={ open }
      title={
        <I18n>tell who you deliver your milk production to</I18n>
      }
      disableFullScreen
      escape={ false }
      classes={ { paper: classes.modalBackground } }
      contentContainerStyle={ { padding: 0, minWidth: 300 } }
      headerProps={ {
        titleClass: classes.title
      } }
    >
      <View>
        <Content>
          <MilkDeliveryForm
            ref={ milkDeliveryFormRef }
            handleClose={ handleClose }
            dairyFarm={ dairyFarm }
            clearForm={ clearForm }
          />
        </Content>
      </View>
    </Modal>
  )}

MilkDeliveryModal.propTypes = {
  id: PropTypes.string.isRequired,
  open: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  onCancel: PropTypes.func,
  dairyFarm: PropTypes.object
}

MilkDeliveryModal.defaultProps = {
  onCancel: () => {},
  dairyFarm: {}
}

export default MilkDeliveryModal
