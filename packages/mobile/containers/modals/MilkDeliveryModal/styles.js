import styled from 'styled-components'

import { makeStyles } from '@material-ui/core/styles'

import colors from '@smartcoop/styles/colors'

export const Row = styled.View`
  display: flex;
  flex-direction: row;
`

export const Content = styled.View`
  display: flex;
  flex-direction: column;
`

export default makeStyles({
  modalBackground: {
    backgroundColor: colors.backgroundHtml,
    maxWidth: 420

  },
  title: {
    textAlign: 'center',
    fontWeight: `${ 700  } !important`,
    fontSize: 16,
    lineHeight: '16px',
    padding: '20px 13px 0 20px',
    '& h2': {
      fontSize: '24px!important',
      fontWeight: `${ 'inherit' } !important`
    }
  }

})
