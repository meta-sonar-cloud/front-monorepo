import React, { useCallback, useRef } from 'react'

import PropTypes from 'prop-types'

import I18n, { useT } from '@smartcoop/i18n'
import Button from '@smartcoop/mobile-components/Button'
import Modal from '@smartcoop/mobile-components/Modal'
import SignatureOrderForm from '@smartcoop/mobile-containers/forms/commercialization/SignatureOrderForm'

import useStyles, { Row, Content, ContainerButtons, ButtonGroup } from './styles'

const SignatureOrderModal = ({ id, open, handleClose, onSuccess }) => {
  const classes = useStyles()
  const t = useT()
  const signatureElectronicFormRef = useRef(null)

  const clearForm = useCallback(
    () => {
      signatureElectronicFormRef.current.reset()
      handleClose()
    },
    [handleClose]
  )

  return (
    <Modal
      id={ id }
      open={ open }
      title={
        <Row>
          <I18n>electronic confirmation</I18n>
        </Row>
      }
      disableFullScreen
      escape
      classes={ { paper: classes.modalBackground } }
      contentContainerStyle={ { width: 500, padding: 20, paddingTop: 0 } }
    >
      <>
        <Content>
          <SignatureOrderForm
            ref={ signatureElectronicFormRef }
            onSubmit={ onSuccess }
            handleClose={ handleClose }
            withoutSubmitButton
          />
          <ContainerButtons>
            <ButtonGroup>
              <Button
                title={ t('cancel') }
                variant='outlined'
                onPress={ clearForm }
                style={ { marginRight: '5%', textTransform: 'none' } }
              />
              <Button
                title={ t('confirm') }
                onPress={ () => signatureElectronicFormRef.current?.submit() }
              />
            </ButtonGroup>
          </ContainerButtons>
        </Content>
      </>
    </Modal>
  )}

SignatureOrderModal.propTypes = {
  id: PropTypes.string.isRequired,
  open: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  onSuccess: PropTypes.func.isRequired
}

export default SignatureOrderModal
