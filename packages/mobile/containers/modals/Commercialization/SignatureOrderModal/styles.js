import styled from 'styled-components/native'

import { makeStyles } from '@material-ui/core/styles'

import colors from '@smartcoop/styles/colors'

export const Row = styled.View`
  display: flex;
  flex-direction: row;
`

export const Content = styled.View`
  display: flex;
  flex-direction: column;
  padding: 0 12px 12px 12px;
`
export const ContainerButtons = styled.View`
  width: 100%;
  margin-top: 10px;
`

export const ButtonGroup = styled.View`
  flex-direction: row;
  align-items: center;
  justify-content: center;
`

export default makeStyles({
  modalBackground: {
    backgroundColor: colors.white
  },
  title: {
    fontWeight: 600,
    fontSize: 16,
    lineHeight: '16px',
    padding: '12px 13px 0 13px'
  }
})
