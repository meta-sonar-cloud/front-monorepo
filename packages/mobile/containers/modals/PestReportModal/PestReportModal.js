import React, { useMemo, useCallback } from 'react'

import moment from 'moment/moment'
import PropTypes from 'prop-types'


import isEmpty from 'lodash/isEmpty'
import map from 'lodash/map'

import I18n from '@smartcoop/i18n'
import {  pencil, trash } from '@smartcoop/icons'
import Button from '@smartcoop/mobile-components/Button'
import Divider from '@smartcoop/mobile-components/Divider'
import Icon from '@smartcoop/mobile-components/Icon'
import Modal from '@smartcoop/mobile-components/Modal'
import ThumbnailImage from '@smartcoop/mobile-components/ThumbnailImage'
import colors from '@smartcoop/styles/colors'
import { momentBackDateFormat, momentFriendlyDateFormat } from '@smartcoop/utils/dates'

import {
  Container,
  Scroll,
  DetailsContainer,
  Title,
  Item,
  Info,
  Subtitle,
  BlackSubtitle,
  Text,
  ImagesContainer,
  Actions
} from './styles'

const PestReportModal = ({ id, open, pestReport, onEdit, onDelete, handleClose }) => {
  const values = useMemo(
    () => (
      {
        type: pestReport?.reportTypes?.name ?? '-',
        name: pestReport[pestReport?.reportTypes?.slug]?.name ?? '',
        date: moment(pestReport.occurenceDate, momentBackDateFormat).format(momentFriendlyDateFormat),
        damage: (`${ pestReport?.damage  }%`) ?? '-',
        observations: pestReport?.observations ?? '',
        description: pestReport?.description ?? '',
        reportImages: !isEmpty(pestReport.reportImages) ? pestReport.reportImages : []
      }
    ), [pestReport]
  )

  const deleteReport = useCallback(
    () => {
      onDelete(pestReport.id)
      handleClose()
    }, [handleClose, onDelete, pestReport.id]
  )

  const editReport = useCallback(
    () => {
      onEdit(pestReport)
      handleClose()
    }, [handleClose, onEdit, pestReport]
  )

  return (
    <Modal
      id={ id }
      open={ open }
    >
      <Container>
        <Scroll>
          <DetailsContainer>
            <Title>
              <I18n>occurrence</I18n>
            </Title>
            <Divider style={ { marginBottom: 10 } }/>
            <Item>
              <Info>
                <Subtitle>
                  <I18n>
                occurrence
                  </I18n>
                </Subtitle>
                <Text>
                  {values.type} { values.name && '-'} {values.name}
                </Text>
              </Info>
              <Actions>
                <Button
                  variant="outlined"
                  style={ { marginRight: 5, padding: 8, borderColor: colors.lightGrey } }
                  icon={ <Icon size={ 14 } icon={ pencil } color={ colors.black } /> }
                  backgroundColor={ colors.white }
                  onPress={ editReport }
                />
                <Button
                  variant="outlined"
                  style={ { padding: 8, borderColor: colors.lightGrey } }
                  onPress={ deleteReport }
                  icon={ <Icon size={ 14 } icon={ trash } color={ colors.red } /> }
                  backgroundColor={ colors.white }
                />
              </Actions>
            </Item>
            <Item>
              <Info>
                <Subtitle>
                  <I18n params={ {
                    howMany: 1
                  } }
                  >
                date
                  </I18n>
                </Subtitle>
                <Text>
                  {values.date}
                </Text>
              </Info>
              <Info>
                <Subtitle>
                  <I18n>
                damage done
                  </I18n>
                </Subtitle>
                <Text>
                  {values.damage}
                </Text>
              </Info>
            </Item>
            <Item>
              {!isEmpty(values.description) && (
                <Info>
                  <Subtitle>
                    <I18n as={ Text }>
                description
                    </I18n>
                  </Subtitle>
                  <Text>
                    {values.description}
                  </Text>
                </Info>
              )}
              {!isEmpty(values.observations) && (
                <Info>
                  <Subtitle>
                    <I18n>
                observations
                    </I18n>
                  </Subtitle>
                  <Text>
                    {values.observations}
                  </Text>
                </Info>
              )}
            </Item>
            {
              !isEmpty(pestReport.reportImages) && (
                <Item>
                  <Info>
                    <BlackSubtitle>
                      <I18n>
                  images
                      </I18n>
                    </BlackSubtitle>
                    <ImagesContainer>
                      {map(values.reportImages, ({ id: fileId, fileUrl }) => (
                        <ThumbnailImage
                          key={ fileId }
                          src={ fileUrl }
                          size={ 100 }
                        />
                      ))}
                    </ImagesContainer>
                  </Info>
                </Item>
              )
            }
          </DetailsContainer>
        </Scroll>
      </Container>
    </Modal>
  )
}

PestReportModal.propTypes = {
  id: PropTypes.string.isRequired,
  open: PropTypes.bool.isRequired,
  onDelete: PropTypes.func,
  onEdit: PropTypes.func,
  pestReport: PropTypes.object.isRequired,
  handleClose: PropTypes.func.isRequired
}

PestReportModal.defaultProps = {
  onEdit: () => {},
  onDelete: () => {}
}

export default PestReportModal
