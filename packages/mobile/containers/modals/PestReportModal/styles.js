import styled from 'styled-components/native'

import colors from '@smartcoop/styles/colors'

export const Container = styled.View`
  flex-direction: column;
  justify-content: space-between;
  align-items: center;
  width: 100%;
  padding: 0%;
  overflow: scroll;
`

export const Scroll = styled.ScrollView`
  width: 100%;
`


export const Title = styled.Text`
  font-size: 18px;
  font-weight: 700;
  color: ${ colors.black };
  margin-bottom: 5%;
`

export const BlackSubtitle = styled(Title)`
  font-size: 16px;
`

export const ImagesContainer = styled.View`
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  flex-wrap: wrap;
`

export const Subtitle = styled.Text`
  font-size: 14px;
  color: ${ colors.darkGrey };
`

export const Text = styled.Text`
  font-size: 16px;
  color: ${ colors.black };
  margin-bottom: 5%;
`

export const DetailsContainer = styled.View`
  width: 300px;
  /* height: 400px; */
`

export const Item = styled.View`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  margin-bottom: 10px;
  flex-wrap:wrap;
`

export const Actions = styled.View`
  display: flex;
  flex-direction: row;
  justify-content: flex-end;
`

export const Info = styled.View`
  display: flex;
  flex-direction: column;
`
