import styled from 'styled-components/native'

import { makeStyles } from '@material-ui/core/styles'

import colors from '@smartcoop/styles/colors'

export const Row = styled.View`
  display: flex;
  flex-direction: row;
`

export const Content = styled.View`
  display: flex;
  flex-direction: column;
  padding: 18px;
`

export default makeStyles({
  modalBackground: {
    backgroundColor: colors.backgroundHtml
  },
  title: {
    fontWeight: 600,
    fontSize: 16,
    lineHeight: '16px',
    padding: '12px 13px 0 13px'
  }
})
