import React, { useCallback, useRef } from 'react'

import PropTypes from 'prop-types'

import I18n, { useT } from '@smartcoop/i18n'
import { filter } from '@smartcoop/icons'
import Button from '@smartcoop/mobile-components/Button'
import Divider from '@smartcoop/mobile-components/Divider'
import Icon from '@smartcoop/mobile-components/Icon'
import Modal from '@smartcoop/mobile-components/Modal'
import FilterTechnicalPortfolioForm from '@smartcoop/mobile-containers/forms/digitalProperty/technical/FilterTechnicalPortfolioForm'
import { ButtonsContainer } from '@smartcoop/mobile-containers/layouts/AuthenticatedLayout/theme'
import { colors } from '@smartcoop/styles'

import useStyles, { Row, Content } from './styles'

const FilterTechnicalPortfolioModal = ({ id, open, handleClose, onSubmit, filters }) => {
  const classes = useStyles()
  const FilterTechnicalPortfolioFormRef = useRef(null)
  const t = useT()
  const handleSubmit = useCallback(
    (value) => {
      onSubmit({
        ...value
      })
      handleClose()
    },
    [handleClose, onSubmit]
  )

  const clearForm = useCallback(
    () => {
      FilterTechnicalPortfolioFormRef.current.reset()
      onSubmit({})
      handleClose()
    },
    [handleClose, onSubmit]
  )

  return (
    <Modal
      id={ id }
      open={ open }
      title={
        <Row>
          <Icon style={ { paddingRight: 6 } } icon={ filter } size={ 14 }/>
          <I18n>filtrate</I18n>
        </Row>
      }
      disableFullScreen
      escape
      classes={ { paper: classes.modalBackground } }
      contentContainerStyle={ { padding: 0, minWidth: 300 } }
      headerProps={ {
        titleClass: classes.title,
        disableTypography: true
      } }
    >
      <>
        <Divider/>
        <Content>
          <FilterTechnicalPortfolioForm
            ref={ FilterTechnicalPortfolioFormRef }
            onSubmit={ handleSubmit }
            withoutSubmitButton
            filters={ filters }
          />
          <ButtonsContainer>
            <Button
              id="clear"
              onPress={ () => clearForm() }
              variant="outlined"
              style={ { marginRight: 7 } }
              title={ t('clear') }
            />
            <Button
              id="filtrate"
              onPress={ () => FilterTechnicalPortfolioFormRef.current.submit() }
              color={ colors.white }
              style={ { marginLeft: 7 } }
              title={ t('filtrate') }
            />
          </ButtonsContainer>
        </Content>
      </>
    </Modal>
  )}

FilterTechnicalPortfolioModal.propTypes = {
  id: PropTypes.string.isRequired,
  open: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  onSubmit: PropTypes.func,
  filters: PropTypes.object.isRequired
}

FilterTechnicalPortfolioModal.defaultProps = {
  onSubmit: () => {}
}

export default FilterTechnicalPortfolioModal
