import React, { useCallback } from 'react'
import { TouchableOpacity } from 'react-native'
import { useDispatch } from 'react-redux'

import PropTypes from 'prop-types'

import I18n from '@smartcoop/i18n'
import { successFlower } from '@smartcoop/icons'
import Icon from '@smartcoop/mobile-components/Icon'
import Modal from '@smartcoop/mobile-components/Modal'
import { PropertyActions } from '@smartcoop/stores/property'

import { Container, MainText, HeaderText, MainButton, TextUnderlined } from './styles'

const SuccessModal = (props) => {
  const {
    open,
    handleClose,
    message,
    id,
    buttonText,
    underlinedText,
    onConfirm,
    onUnderlinedClick,
    reloadDataAction
  } = props

  const dispatch = useCallback(useDispatch(), [])

  return (
    <Modal id={ id } open={ open } hideHeader>
      <Container>

        <Icon icon={ successFlower } size={ 125 } />

        <HeaderText>
          <I18n>success</I18n>
        </HeaderText>

        {message && (
          <MainText>
            {message}
          </MainText>
        )}

        <MainButton
          title={ buttonText }
          onPress={ () => {
            reloadDataAction && dispatch(PropertyActions.setReloadData(true))
            handleClose()
            onConfirm()
          } }
          style={ { minWidth: 100, textTransform: 'none' } }
        />

        {underlinedText && (
          <TouchableOpacity
            onPress={ () => {
              reloadDataAction && dispatch(PropertyActions.setReloadData(true))
              handleClose()
              onUnderlinedClick()
            } }
          >
            <TextUnderlined>
              {underlinedText}
            </TextUnderlined>
          </TouchableOpacity>
        )}

      </Container>
    </Modal>
  )
}

SuccessModal.propTypes = {
  id: PropTypes.string.isRequired,
  open: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  message: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.element
  ]),
  buttonText: PropTypes.string,
  underlinedText: PropTypes.string,
  onConfirm: PropTypes.func,
  onUnderlinedClick: PropTypes.func,
  reloadDataAction: PropTypes.bool
}

SuccessModal.defaultProps = {
  message: null,
  buttonText: 'ok',
  underlinedText: null,
  onConfirm: () => {},
  onUnderlinedClick: () => {},
  reloadDataAction: false
}

export default SuccessModal
