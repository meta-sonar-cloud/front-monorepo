import styled from 'styled-components/native'

import Button from '@smartcoop/mobile-components/Button'
import colors from '@smartcoop/styles/colors'

export const Container = styled.View`
  justify-content: center;
  align-items: center;
  width: 100%;
  padding: 0;
`

export const HeaderText = styled.Text`
  margin: 10px;
  font-size: 22px;
  font-weight: bold;
  text-align: center;
  color: ${ colors.text };
`

export const MainText = styled.Text`
  text-align: center;
  color: ${ colors.text };
  font-weight: bold;
  font-size: 16px;
  margin: 0;
`

export const MainButton = styled(Button)`
  margin-top: 20px;
  background-color: ${ colors.black };
  color: ${ colors.white };
`

export const TextUnderlined = styled.Text`
  margin: 22px 0 0;
  font-size: 14px;
  text-decoration: underline;
`
