import React, { useCallback, useRef } from 'react'
import { View } from 'react-native'

import PropTypes from 'prop-types'

import I18n, { useT } from '@smartcoop/i18n'
import { filter } from '@smartcoop/icons'
import Button from '@smartcoop/mobile-components/Button'
import Divider from '@smartcoop/mobile-components/Divider'
import Icon from '@smartcoop/mobile-components/Icon'
import Modal from '@smartcoop/mobile-components/Modal'
import FilterPriceDataForm from '@smartcoop/mobile-containers/forms/digitalProperty/dairyFarm/FilterPriceDataForm'
import colors from '@smartcoop/styles/colors'

import { Container, ButtonGroup, Title } from './styles'

const FilterPriceDataModal = (props) => {
  const {
    open,
    handleClose,
    id,
    onConfirm,
    filters
  } = props

  const t = useT()
  const FilterPriceDataFormRef = useRef(null)

  const handleSubmit = useCallback(
    (data) => {
      onConfirm(data)
      handleClose()
    },
    [handleClose, onConfirm]
  )

  const handleClean = useCallback(
    () => {
      FilterPriceDataFormRef.current.reset()
      onConfirm({})
      handleClose()
    },
    [handleClose, onConfirm]
  )

  return (
    <Modal id={ id } open={ open }>
      <Container>
        <View style={ { alignSelf: 'flex-start' } }>
          <Title>
            <Icon icon={ filter } size={ 18 } />
            <I18n style={ { fontSize: 18 } }>filter</I18n>
          </Title>
        </View>
        <Divider />
        <FilterPriceDataForm
          ref={ FilterPriceDataFormRef }
          onSubmit={ handleSubmit }
          filters={ filters }
          withoutSubmitButton
        />
        <ButtonGroup>
          <Button
            title={ t('clear') }
            color={ colors.black }
            variant='outlined'
            onPress={ handleClean }
            style={ { marginRight: '5%', minWidth: '35%', maxWidth: '40%', textTransform: 'none' } }
          />
          <Button
            title={ t('filtrate') }
            onPress={ () => FilterPriceDataFormRef.current.submit() }
            color={ colors.white }
            style={ { backgroundColor: colors.black, minWidth: '35%', maxWidth: '40%',  textTransform: 'none' } }
          />
        </ButtonGroup>
      </Container>
    </Modal>
  )
}

FilterPriceDataModal.propTypes = {
  id: PropTypes.string.isRequired,
  open: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  onConfirm: PropTypes.func,
  filters: PropTypes.object
}

FilterPriceDataModal.defaultProps = {
  onConfirm: () => {},
  filters: {}
}

export default FilterPriceDataModal
