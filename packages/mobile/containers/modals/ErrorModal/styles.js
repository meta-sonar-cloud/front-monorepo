import styled from 'styled-components/native'

import colors from '@smartcoop/styles/colors'

export const Container = styled.View`
  justify-content: space-between;
  align-items: center;
  width: 100%;
  padding: 0%;
`

export const HeaderText = styled.Text`
  font-size: 22px;
  font-weight: bold;
  text-align: center;
  margin-bottom: 22px;
  color: ${ colors.text };
`

export const MainText = styled.Text`
  text-align: center;
  color: ${ colors.text };
  font-weight: bold;
  font-size: 16px;
  margin: 22px 0 30px;
`
