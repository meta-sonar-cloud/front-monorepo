import React from 'react'

import PropTypes from 'prop-types'

import I18n, { useT } from '@smartcoop/i18n'
import { notFound } from '@smartcoop/icons'
import Button from '@smartcoop/mobile-components/Button'
import Icon from '@smartcoop/mobile-components/Icon'
import Modal from '@smartcoop/mobile-components/Modal'

import { Container, MainText, HeaderText } from './styles'

const ErrorModal = (props) => {
  const { id, open, handleClose, message } = props
  const t = useT()

  return (
    <Modal id={ id } open={ open } hideHeader>
      <Container>

        <HeaderText>
          <I18n>ops</I18n>
        </HeaderText>

        <Icon icon={ notFound } size={ 125 } />

        {message && (
          <MainText>
            {message}
          </MainText>
        )}

        <Button
          title={ t('ok') }
          onPress={ handleClose }
          style={ { minWidth: 100 } }
        />

      </Container>
    </Modal>
  )
}

ErrorModal.propTypes = {
  id: PropTypes.string.isRequired,
  open: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  message: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.element
  ])
}

ErrorModal.defaultProps = {
  message: null
}

export default ErrorModal
