import React, { useRef } from 'react'

import PropTypes from 'prop-types'

import Modal from '@smartcoop/mobile-components/Modal'
import CompleteManagementForm from '@smartcoop/mobile-containers/forms/digitalProperty/management/CompleteManagementForm'

import { Container } from './styles'

const CompleteCropManagementModal = (props) => {
  const {
    open,
    handleClose,
    currentCropManagement,
    canConcludeGrowingSeason,
    id,
    onSuccess
  } = props

  const completeManagementRef = useRef(null)

  return (
    <Modal
      id={ id }
      open={ open }
      hideHeader
      style={ { paddingTop: 50, paddingBottom: 60 } }
    >
      <Container>
        <CompleteManagementForm
          ref={ completeManagementRef }
          currentCropManagement={ currentCropManagement }
          onSuccess={ onSuccess }
          handleClose={ handleClose }
          canConcludeGrowingSeason={ canConcludeGrowingSeason }
        />
      </Container>
    </Modal>
  )
}

CompleteCropManagementModal.propTypes = {
  id: PropTypes.string.isRequired,
  currentCropManagement: PropTypes.object,
  open: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  onSuccess: PropTypes.func,
  canConcludeGrowingSeason: PropTypes.bool
}

CompleteCropManagementModal.defaultProps = {
  currentCropManagement: {},
  onSuccess: () => {},
  canConcludeGrowingSeason: false
}

export default CompleteCropManagementModal
