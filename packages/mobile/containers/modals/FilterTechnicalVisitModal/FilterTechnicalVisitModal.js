import React, { useCallback, useRef } from 'react'
import { View } from 'react-native'

import PropTypes from 'prop-types'

import I18n, { useT } from '@smartcoop/i18n'
import { filter } from '@smartcoop/icons'
import Button from '@smartcoop/mobile-components/Button'
import Divider from '@smartcoop/mobile-components/Divider'
import Icon from '@smartcoop/mobile-components/Icon'
import Modal from '@smartcoop/mobile-components/Modal'
import FilterTechnicalVisitForm from '@smartcoop/mobile-containers/forms/digitalProperty/technical/FilterTechnicalVisitForm'
import colors from '@smartcoop/styles/colors'

import { Container, ButtonGroup, Title } from './styles'

const FilterTechnicalVisitModal = (props) => {
  const {
    open,
    handleClose,
    id,
    onSubmit,
    filters
  } = props

  const t = useT()
  const FilterVisitListRef = useRef(null)

  const handleSubmit = useCallback(
    (data) => {
      onSubmit(data)
      handleClose()
    },
    [handleClose, onSubmit]
  )

  const handleClean = useCallback(
    () => {
      FilterVisitListRef.current.reset()
      onSubmit({})
      handleClose()
    },
    [handleClose, onSubmit]
  )

  return (
    <Modal id={ id } open={ open }>
      <Container>
        <View style={ { alignSelf: 'flex-start' } }>
          <Title>
            <Icon icon={ filter } size={ 18 } />
            <I18n style={ { fontSize: 18 } }>filter</I18n>
          </Title>
        </View>
        <Divider />
        <FilterTechnicalVisitForm
          ref={ FilterVisitListRef }
          onSubmit={ handleSubmit }
          filters={ filters }
          withoutSubmitButton
        />
        <ButtonGroup>
          <Button
            title={ t('clear') }
            color={ colors.black }
            variant='outlined'
            onPress={ handleClean }
            style={ { marginRight: '5%' ,minWidth: '35%', maxWidth: '40%', textTransform: 'none' } }
          />
          <Button
            title={ t('filtrate') }
            onPress={ () => FilterVisitListRef.current.submit() }
            color={ colors.white }
            style={ { backgroundColor: colors.black, minWidth: '35%', maxWidth: '40%',  textTransform: 'none' } }
          />
        </ButtonGroup>
      </Container>
    </Modal>
  )
}

FilterTechnicalVisitModal.propTypes = {
  id: PropTypes.string.isRequired,
  open: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  onSubmit: PropTypes.func,
  filters: PropTypes.object
}

FilterTechnicalVisitModal.defaultProps = {
  onSubmit: () => {},
  filters: {}
}

export default FilterTechnicalVisitModal
