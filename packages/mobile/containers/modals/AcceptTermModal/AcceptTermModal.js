import React, { useMemo, useState, useCallback } from 'react'
import { View } from 'react-native'
import { WebView } from 'react-native-webview'
import { useDispatch, useSelector } from 'react-redux'

import PropTypes from 'prop-types'


import { find } from 'lodash'

import { useDialog } from '@smartcoop/dialog'
import I18n, { useT } from '@smartcoop/i18n'
import Button from '@smartcoop/mobile-components/Button'
import CheckboxButton from '@smartcoop/mobile-components/CheckboxGroup/CheckboxButton'
import Loader from '@smartcoop/mobile-components/Loader'
import Modal from '@smartcoop/mobile-components/Modal'
import { useSnackbar } from '@smartcoop/snackbar'
import { AuthenticationActions } from '@smartcoop/stores/authentication'
import { selectTerms } from '@smartcoop/stores/authentication/selectorAuthentication'
import { colors } from '@smartcoop/styles'

import AcceptTermModalConfirm from '../AcceptTermModalConfirm/AcceptTermModalConfirm'
import {
  Container,
  Content,
  Buttons,
  Title,
  Item,
  WebViewContainer
} from './styles'

const AcceptTermModal = ({
  id,
  open,
  slug,
  handleClose,
  viewOnly,
  onWillClose,
  onCancel }) => {
  const [isLoading, setIsLoading] = useState(false)
  const dispatch = useCallback(useDispatch(), [])
  const snackbar = useSnackbar()
  const t = useT()
  const { createDialog } = useDialog()

  const terms = useSelector(selectTerms)

  const [accepted, setAccepted] = useState(false)

  const onClose = useCallback(
    () => {
      onWillClose()
      handleClose()
    }, [handleClose, onWillClose]
  )

  const onSuccess = useCallback(
    () => {
      setIsLoading(false)
      snackbar.success(t('the term was accepted succesfully'))
      onClose()
    }, [onClose, snackbar, t]
  )

  const onError = useCallback(
    () => {
      setIsLoading(false)
    }, []
  )

  const openConfirmModal = useCallback(
    () => {
      createDialog({
        id: `confirm-term-modal-${  slug }`,
        Component: AcceptTermModalConfirm

      })
    }, [createDialog, slug]
  )

  const cancel = useCallback(
    () => {
      onCancel()
      openConfirmModal()
    }, [onCancel, openConfirmModal]
  )

  const onSubmit = useCallback(
    () => {
      if(!viewOnly) {
        setIsLoading(true)
        dispatch(AuthenticationActions.updateTerm(
          { slug },
          () => {onSuccess()},
          (error) => {onError(error)}
        ))
      }
      onClose()
    }, [dispatch, onClose, onError, onSuccess, slug, viewOnly]
  )

  const selectedTerm = useMemo(
    () => find(terms, { termType: { slug } }), [slug, terms]
  )

  const isAccepted = useMemo(
    () => accepted || viewOnly,
    [accepted, viewOnly]
  )

  const formatHTML = useCallback(
    (text) => (
      `
      <html>
      <head>
        <style>
          body { overflow: auto }
        </style>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
      </head>
      <body>
      <p>${ text }</p>
        </body>
      </html>`
    ), []
  )

  return (
    <Modal
      id={ id }
      open={ open }
      escape={ false }
      title={ selectedTerm?.termType?.type }
    >
      {isLoading ? <Loader /> :
        <Container>
          <Content>
            <Item>
              <I18n as={ Title }>
              please read the terms below with attention
              </I18n>
            </Item>
            <WebViewContainer>
              <WebView
                originWhitelist={ ['*'] }
                source={ { html: formatHTML(selectedTerm?.description) } }
              />
            </WebViewContainer>
            <Item>
              <CheckboxButton
                label={ t('i declare that i read and im aware of the norms that are in the term above') }
                value={ accepted }
                checked={ isAccepted }
                style={ { backgroundColor: colors.white } }
                onChange={ (value) => {setAccepted(!value)} }
              />
            </Item>
          </Content>
          <Buttons>
            {!viewOnly &&
            <>
              <Button
                id="cancel-button"
                onPress={ cancel }
                style={ { flex: 1 } }
                variant="outlined"
                title={ t('cancel') }
              />
              <View style={ { width: '10%' } } />
            </>
            }
            <Button
              id="confirm-button"
              onPress={ onSubmit }
              style={ { flex: 1 } }
              disabled={ !isAccepted }
              title={ t('confirm') }
            />
          </Buttons>
        </Container>}
    </Modal>
  )
}

AcceptTermModal.propTypes = {
  id: PropTypes.string.isRequired,
  open: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  onWillClose: PropTypes.func,
  onCancel: PropTypes.func,
  viewOnly: PropTypes.bool,
  slug: PropTypes.oneOf([
    'privacy-term',
    'social-network-term',
    'commercialization-term',
    'supplier-term',
    'organization-term',
    'use-term'
  ]).isRequired
}

AcceptTermModal.defaultProps = {
  onWillClose: () => {},
  onCancel: () => {},
  viewOnly: false
}

export default AcceptTermModal
