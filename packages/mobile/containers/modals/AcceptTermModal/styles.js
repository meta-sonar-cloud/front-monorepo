import { Subheading } from 'react-native-paper'

import styled from 'styled-components/native'

import colors from '@smartcoop/styles/colors'

export const Container = styled.View`
  flex-direction: column;
  justify-content: space-between;
  align-items: center;
  width: 100%;
  padding: 0%;
`

export const Title = styled(Subheading)`
  font-family: 'Montserrat';
  letter-spacing: 0;
  margin: 0;
  color: ${ colors.black };
`

export const Content = styled.View`
  display: flex;
  flex-direction: column;
  justify-content: center;
`

export const WebViewContainer = styled.View`
  height: 200px;
`

export const Item = styled.View`
  padding: 5px;
`

export const Buttons = styled.View`
  display: flex;
  flex-direction: row;
`


export const Term = styled.ScrollView`
  max-height: 200px;
  height: 33%;
  padding: 20px 10px;
`
