import { useEffect, useCallback, useMemo } from 'react'
import { useSelector, useDispatch } from 'react-redux'

import { camelCase, isEmpty } from 'lodash'
import find from 'lodash/find'

import { useDialog } from '@smartcoop/dialog'
import AcceptTermModal from '@smartcoop/mobile-containers/modals/AcceptTermModal'
import { AuthenticationActions } from '@smartcoop/stores/authentication'
import {
  selectTerms,
  selectUserIsSupplier
} from '@smartcoop/stores/authentication/selectorAuthentication'


const useTerm = (slug = 'use-term', startOpened = true, onCancel = () => {}, onError = () => {}) => {
  const { createDialog } = useDialog()
  const dispatch = useCallback(useDispatch(), [])

  const terms = useSelector(selectTerms)
  const isSupplier = useSelector(selectUserIsSupplier)

  const selectedTerm = useMemo(
    () => find(terms, { termType: { slug } }) ?? { userTerm: 'mock' }, [slug, terms]
  )


  const accept = useMemo(() => !isEmpty(selectedTerm?.userTerm), [selectedTerm])

  const showModal = useMemo(
    () => ({
      privacyTerm: !accept,
      socialNetworkTerm: !accept,
      commercializationTerm: !accept,
      supplierTerm: !accept && isSupplier,
      organizationTerm: !accept && !isSupplier,
      useTerm: !accept
    }[camelCase(slug)]), [accept, isSupplier, slug]
  )

  const modalTerm = useCallback(
    (onWillClose = () => {}, viewOnly = false) => {
      if(!accept || viewOnly) {
        createDialog({
          id: `term-modal-${  slug }`,
          Component: AcceptTermModal,
          props: {
            viewOnly,
            slug,
            onWillClose,
            onCancel
          }
        })
      }
    }, [accept, createDialog, onCancel, slug]
  )

  const openModal = useCallback(
    () => {
      if(startOpened && showModal) modalTerm()
    }, [modalTerm, showModal, startOpened]
  )

  useEffect(
    () => {
      dispatch(AuthenticationActions.loadTerms(
        openModal,
        onError
      ))
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []
  )

  return [
    modalTerm,
    selectedTerm,
    terms
  ]
}

export default useTerm
