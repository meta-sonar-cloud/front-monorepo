import React, { useCallback } from 'react'

import { createDrawerNavigator } from '@react-navigation/drawer'

import { useT } from '@smartcoop/i18n'
import CustomDrawer from '@smartcoop/mobile-containers/fragments/CustomDrawer'

const Drawer = createDrawerNavigator()

export const withDrawer = (WrapperComponent) => props => {
  const t = useT()

  const AppDrawer = useCallback(
    (p) => <CustomDrawer { ...p } t={ t } />,
    [t]
  )

  const renderScreen = useCallback(
    (screenProps) => <WrapperComponent { ...screenProps } { ...props } />,
    [props]
  )

  return (
    <Drawer.Navigator
      detachInactiveScreens
      drawerContent={ AppDrawer }
    >
      <Drawer.Screen name="Drawer">
        {renderScreen}
      </Drawer.Screen>
    </Drawer.Navigator>
  )
}
