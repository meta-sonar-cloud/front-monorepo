import { useState, useCallback, useMemo } from 'react'
import { Platform } from 'react-native'
import * as ImagePicker from 'react-native-image-picker'

import filter from 'lodash/filter'
import forEach from 'lodash/forEach'
import isEmpty from 'lodash/isEmpty'

import  { useT } from '@smartcoop/i18n'
import { useSnackbar } from '@smartcoop/snackbar'
import { convertFileSize, isValidSize } from '@smartcoop/utils/files'

const useFile = (initialSelectedFiles = [], initialReceivedFiles = []) => {
  const [selectedFiles, setSelectedFiles] = useState(initialSelectedFiles)
  const [receivedFiles, setReceivedFiles] = useState(initialReceivedFiles)
  const snackbar = useSnackbar()
  const t = useT()
  const maxFileSize = 5242880

  const isEmptyFiles = useMemo(
    () => isEmpty(receivedFiles) && isEmpty(selectedFiles),
    [receivedFiles, selectedFiles]
  )

  const handleAdd = useCallback(() => {
    const options = {
      noData: true
    }
    ImagePicker.launchImageLibrary(options, (response) => {
      if (response.uri) {
        if (isValidSize(response, maxFileSize)) {
          setSelectedFiles([...selectedFiles, response])
        } else {
          snackbar.error(
            t('the size of one or more files exceeds {this}', {
              this: convertFileSize(maxFileSize)
            })
          )
        }

      }
    })
  },
  [selectedFiles, snackbar, t]
  )

  const handleAddCamera = useCallback(() => {
    const options = {
      noData: true,
      mediaType: 'photo',
      quality: 0.5
    }
    ImagePicker.launchCamera(options, (response) => {
      if (response.uri) {
        if (isValidSize(response, maxFileSize)) {
          setSelectedFiles([...selectedFiles, response])
        } else {
          snackbar.error(
            t('the size of one or more files exceeds {this}', {
              this: convertFileSize(maxFileSize)
            })
          )
        }
      }
    })
  },
  [selectedFiles, snackbar, t]
  )

  const createFormData = useCallback(
    (fileKey = 'upload', body = {}) => {
      const data = new FormData()

      forEach(selectedFiles, (file) => {
        data.append(fileKey, {
          name: file.fileName,
          type: file.type,
          uri:
        Platform.OS === 'android' ? file.uri : file.uri.replace('file://', '')
        })
      })

      Object.keys(body).forEach((key) => {
        data.append(key, body[key])
      })

      return data
    },
    [selectedFiles]
  )

  const handleRemove = useCallback(
    (list, removedFile) => {
      if (list === 'selectedFiles') {
        setSelectedFiles((state) => filter(state, (file) => ( file !== removedFile )))
      } else {
        setReceivedFiles((state) => filter(state, (file) => ( file.id !== removedFile.id )))
      }
    },
    []
  )

  return {
    selectedFiles,
    receivedFiles,
    isEmpty: isEmptyFiles,
    handleAdd,
    handleAddCamera,
    createFormData,
    handleRemove
  }
}

export default useFile
