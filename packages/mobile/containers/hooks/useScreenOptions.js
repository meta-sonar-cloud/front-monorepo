import React, {
  useMemo,
  useState,
  useCallback,
  createContext,
  useContext,
  useLayoutEffect
} from 'react'
import { Platform } from 'react-native'

import { useFocusEffect } from '@react-navigation/native'

import { arrowLeft } from '@smartcoop/icons'
import Icon from '@smartcoop/mobile-components/Icon'
import { colors } from '@smartcoop/styles'

const ScreenOptionsContext = createContext()

const useScreenOptions = (parentNavigation) => {
  const hook = useMemo(
    () => parentNavigation ? useFocusEffect : useLayoutEffect,
    [parentNavigation]
  )

  // hide parent header parentNavigation if it exist
  const effect = useCallback(
    () => {
      if (parentNavigation) {
        parentNavigation.setOptions({ headerShown: false })
      }
      return () => {
        if (parentNavigation) {
          parentNavigation.setOptions({ headerShown: true })
        }
      }
    },
    [parentNavigation]
  )

  hook(effect, [])

  return useContext(ScreenOptionsContext)
}

// eslint-disable-next-line react/prop-types
const ScreenOptionsProvider = ({ initialOptions: externalOptions, ...props } = {}) => {
  const initialOptions = useMemo(
    () => ({
      cardStyle: { backgroundColor: colors.backgroundHtml },
      headerStyle: {
        backgroundColor: colors.primary
      },
      headerTintColor: colors.white,
      headerBackTitle: ' ',
      headerMode: 'float',
      headerBackImage: Platform.OS === 'android'
        ? ({ tintColor }) => <Icon icon={ arrowLeft } color={ tintColor } size={ 20 } />
        : undefined,
      // eslint-disable-next-line react/prop-types
      headerTitle: null,
      ...(externalOptions || {})
    }),
    [externalOptions]
  )

  const [options, setOptionsState] = useState(initialOptions)

  const setOptions = useCallback(
    (opts = {}) => setOptionsState({
      ...initialOptions,
      ...opts
    }),
    [initialOptions]
  )

  const tabBarOptions = useMemo(
    () =>({
      activeTintColor: colors.primary,
      inactiveTintColor: colors.grey,
      style: {
        backgroundColor: colors.backgroundHtml
      }
    }),
    []
  )

  const value = useMemo(
    () => ({
      options,
      setOptions,
      tabBarOptions
    }),
    [options, setOptions, tabBarOptions]
  )

  return (
    <ScreenOptionsContext.Provider value={ value } { ...props } />
  )
}

const withScreenOptions = WrapperComponent => props => (
  <ScreenOptionsProvider>
    <WrapperComponent { ...props } />
  </ScreenOptionsProvider>
)

export {
  withScreenOptions,
  useScreenOptions
}
