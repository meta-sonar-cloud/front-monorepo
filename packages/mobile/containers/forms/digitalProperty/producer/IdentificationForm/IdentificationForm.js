import React, { useCallback, forwardRef } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import PropTypes from 'prop-types'

import identification from '@smartcoop/forms/schemas/producer/identification.schema'
import { useT } from '@smartcoop/i18n'
import Button from '@smartcoop/mobile-components/Button'
import Form from '@smartcoop/mobile-components/Form'
import InputEmail from '@smartcoop/mobile-components/InputEmail'
import InputText from '@smartcoop/mobile-components/InputText'
import { UserActions } from '@smartcoop/stores/user'
import { selectUser } from '@smartcoop/stores/user/selectorUser'

import { Container, ButtonContainer } from './styles'

const IdentificationForm = forwardRef((props, formRef) => {
  const { withoutSubmitButton, onSubmit } = props

  const dispatch = useCallback(useDispatch(), [])
  const t = useT()

  const user = useSelector(selectUser)

  const handleSubmit = useCallback(
    async (data) => {
      dispatch(UserActions.updateUser(
        data,
        onSubmit
      ))
    },
    [dispatch, onSubmit]
  )

  return (
    <Container>
      <Form
        ref={ formRef }
        schemaConstructor={ identification }
        onSubmit={ handleSubmit }
      >
        <InputText
          name="name"
          label={ t('full name') }
          defaultValue={ user.name }
          disabled
        />

        <InputText
          name="cellPhone"
          label={ t('cellphone') }
          defaultValue={ user.cellPhone }
          disabled
        />

        <InputEmail
          name="email"
          label={ t('email') }
          defaultValue={ user.email }
          disabled
        />

        {!withoutSubmitButton && (
          <ButtonContainer>
            <Button
              style={ { width: '48%' } }
              title={ t('next') }
              onPress={ () => formRef.current.submit() }
            />
          </ButtonContainer>
        )}
      </Form>
    </Container>
  )
})

IdentificationForm.propTypes = {
  onSubmit: PropTypes.func,
  withoutSubmitButton: PropTypes.bool
}

IdentificationForm.defaultProps = {
  onSubmit: () => {},
  withoutSubmitButton: false
}

export default IdentificationForm
