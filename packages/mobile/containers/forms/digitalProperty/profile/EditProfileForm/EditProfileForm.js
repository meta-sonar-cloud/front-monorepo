import React, { useCallback, forwardRef } from 'react'

import PropTypes from 'prop-types'

import profileSchema from '@smartcoop/forms/schemas/profile/profileEdit.schema'
import { useT } from '@smartcoop/i18n'
import Button from '@smartcoop/mobile-components/Button'
import Form from '@smartcoop/mobile-components/Form'
import InputEmail from '@smartcoop/mobile-components/InputEmail'
import InputPhone from '@smartcoop/mobile-components/InputPhone'
import InputText from '@smartcoop/mobile-components/InputText'

import { FormContainer, ButtonsContainer, ButtonContainer } from './styles'

const EditProfileForm = forwardRef((props, formRef) => {
  const {
    defaultValues,
    onSubmit,
    onCancel
  } = props

  const t = useT()

  const handleSubmit = useCallback(
    (data) => {

      onSubmit(data)
    },
    [onSubmit]
  )
  return (
    <Form
      style={ { display: 'flex', flexDirection: 'column', width: '100%' } }
      ref={ formRef }
      schemaConstructor={ profileSchema }
      onSubmit={ handleSubmit }
    >
      <FormContainer>

        <InputText
          name="name"
          label={ t('name') }
          defaultValue={ defaultValues.name }
          style={ { width: '100%' } }
        />

        <InputEmail
          name="email"
          label={ t('email') }
          defaultValue={ defaultValues.email }
          style={ { width: '100%' } }
        />

        <InputPhone
          label={ t('phone') }
          name="cellPhone"
          defaultValue={ defaultValues.cellPhone }
          style={ { width: '100%' } }
        />
      </FormContainer>

      <ButtonsContainer>
        <ButtonContainer>
          <Button
            onPress={ onCancel }
            variant="outlined"
            title={ t('cancel') }
          />
        </ButtonContainer>
        <ButtonContainer>
          <Button
            onPress={ () => formRef.current.submit() }
            title={ t('save') }
          />
        </ButtonContainer>
      </ButtonsContainer>
    </Form>
  )
})

EditProfileForm.propTypes = {
  onSubmit: PropTypes.func,
  onCancel: PropTypes.func,
  defaultValues: PropTypes.object.isRequired
}

EditProfileForm.defaultProps = {
  onSubmit: () => {},
  onCancel: () => {}
}

export default EditProfileForm
