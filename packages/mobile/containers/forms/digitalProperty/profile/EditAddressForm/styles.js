import styled from 'styled-components'

export const Container = styled.View`
  display: flex;
  justify-content: center;
  align-items: center;
  flex: 1 1 auto;
  flex-direction: column;
  width: 100%;
  min-width: 500px;
`

export const FormContainer = styled.View`
    width: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
    align-content: center;
    padding: 20px;
    padding-bottom: 80px;
    flex-direction: column;
`

export const ButtonsContainer = styled.View`
  display: flex;
  flex-direction: row;
  justify-content: space-around;
  width: 100%;
  position: absolute;
  bottom: 0px;
  left: 00px;
`

export const ButtonContainer = styled.View`
  width: 150px;
  height: 50px;

`
export const InputContainer = styled.View`
  display: flex;
  justify-content: space-between;
`
