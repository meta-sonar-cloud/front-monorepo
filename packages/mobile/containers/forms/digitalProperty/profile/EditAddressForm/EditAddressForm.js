import React, { useState, useEffect, useCallback, forwardRef } from 'react'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

import PropTypes from 'prop-types'

import { map } from 'lodash'


import addressSchema from '@smartcoop/forms/schemas/profile/addressEdit.schema'
import { useT } from '@smartcoop/i18n'
import Button from '@smartcoop/mobile-components/Button'
import Form from '@smartcoop/mobile-components/Form'
import InputCep from '@smartcoop/mobile-components/InputCep'
import InputSelect from '@smartcoop/mobile-components/InputSelect'
import InputText from '@smartcoop/mobile-components/InputText'
import { searchCep } from '@smartcoop/services/apis/brasilApi'
import { searchCities, searchStates } from '@smartcoop/services/apis/ibgeApi'

import { FormContainer, ButtonsContainer, ButtonContainer } from './styles'

const EditAddressForm = forwardRef((props, formRef) => {
  const {
    defaultValues,
    onSubmit,
    onCancel
  } = props

  const t = useT()

  const [stateOptions, setStateOptions] = useState([])
  const [cityOptions, setCityOptions] = useState([])



  const handleSubmit = useCallback(
    (data) => {
      onSubmit({ ...data, district: data.neighborhood })
    },
    [onSubmit]
  )

  const receiveAddress = useCallback(({ ...address }) => {
    formRef.current.setData({
      ...address
    })
  }, [formRef])

  const handleStateChange = useCallback(
    async (value) => {
      if (value) {
        let data = await searchCities(value)
        data = map(data, ({ nome }) => ({ label: nome, value: nome }))
        setCityOptions(data)

      } else {
        setCityOptions([])
        formRef.current.clearField('city')
      }
    },
    [formRef]
  )

  const handleEdit = useCallback(
    async (value) => {
      await handleStateChange(value.state)
      const address = await searchCep(value.postalCode)
      formRef.current && formRef.current.setData({
        ...{ city: address.city }
      })
    }, [formRef, handleStateChange]
  )


  useEffect(() => {
    async function findStates() {
      let data = await searchStates()
      data = map(data, ({ sigla }) => ({ label: sigla, value: sigla }))
      setStateOptions(data)
    }
    findStates()
  }, [])


  useEffect(() => {
    if (defaultValues.state) {
      handleEdit(defaultValues)

    } else {
      setCityOptions([])
      formRef.current.clearField('city')
    }
  }, [defaultValues, formRef, handleEdit])


  return (
    <Form
      style={ { display: 'flex', flexDirection: 'column', width: '100%' } }
      ref={ formRef }
      schemaConstructor={ addressSchema }
      onSubmit={ handleSubmit }
    >
      <FormContainer>
        <KeyboardAwareScrollView contentContainerStyle={ { paddingTop: 5 } }>
          <InputCep
            name="postalCode"
            label={ t('cep') }
            fullWidth
            onAddressChange={ receiveAddress }
            defaultValue={ defaultValues.postalCode }
          />

          <InputSelect
            label={ t('uf') }
            name="state"
            onChange={ (e) => handleStateChange(e.target.value) }
            options={ stateOptions }
            fullWidth
            defaultValue={ defaultValues.state }
            style={
              {
                width: '100%'
              }
            }
          />

          <InputSelect
            label={ t('city', { howMany: 1 }) }
            name="city"
            options={ cityOptions }
            fullWidth
            defaultValue={ defaultValues.city }
          />

          <InputText
            name="neighborhood"
            label={ t('neighborhood') }
            fullWidth
            defaultValue={ defaultValues.district }
          />

          <InputText
            name="street"
            label={ t('street') }
            fullWidth
            defaultValue={ defaultValues.street }
          />

          <InputText
            name="number"
            label={ t('number') }
            fullWidth
            defaultValue={ defaultValues.number }
          />
        </KeyboardAwareScrollView>
      </FormContainer>

      <ButtonsContainer>
        <ButtonContainer>
          <Button
            onPress={ onCancel }
            style={ { flex: 1 } }
            variant="outlined"
            title={ t('cancel') }
          />
        </ButtonContainer>
        <ButtonContainer>
          <Button
            onPress={ () => formRef.current.submit() }
            style={ { flex: 1 } }
            title={ t('save') }
          />
        </ButtonContainer>
      </ButtonsContainer>
    </Form>
  )
})

EditAddressForm.propTypes = {
  onSubmit: PropTypes.func,
  onCancel: PropTypes.func,
  defaultValues: PropTypes.object.isRequired
}

EditAddressForm.defaultProps = {
  onSubmit: () => {},
  onCancel: () => {}
}

export default EditAddressForm
