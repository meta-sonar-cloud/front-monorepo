import React, { useMemo, useCallback, forwardRef } from 'react'

import PropTypes from 'prop-types'


import filterAnimalBirthSchema from '@smartcoop/forms/schemas/dairyFarm/filterAnimaBirth.schema'
import { useT } from '@smartcoop/i18n'
import Form from '@smartcoop/mobile-components/Form'
import InputDate from '@smartcoop/mobile-components/InputDate'
import InputSelect from '@smartcoop/mobile-components/InputSelect'
import InputUnit from '@smartcoop/mobile-components/InputUnit'
import RadioGroup from '@smartcoop/mobile-components/RadioGroup'

import { Container, Item } from './styles'


const FilterAnimalBirthForm = forwardRef((props, formRef) => {
  const { onSubmit, filters } = props

  const t = useT()
  const handleSubmit = useCallback(
    (data) => onSubmit(data),
    [onSubmit]
  )

  const defaultOptions = useMemo(() => (
    [
      {
        label: 'Assistido',
        value: 'assistido'
      },
      {
        label: 'Normal',
        value: 'normal'
      },
      {
        label: 'Distocico',
        value: 'distocico'
      }
    ]
  ), [])

  const situationOptions = useMemo(
    () => ([
      {
        label: 'Vivo',
        value: true
      },
      {
        label: 'Morto',
        value: false
      }
    ]), []
  )

  const genderOptions = useMemo(
    () => ([
      {
        label: 'Macho',
        value: 'male'
      },
      {
        label: 'Fêmea',
        value: 'female'
      }
    ]), []
  )

  return (
    <Form
      style={ { display: 'flex', flexDirection: 'column', width: '100%' } }
      ref={ formRef }
      schemaConstructor={ filterAnimalBirthSchema }
      onSubmit={ handleSubmit }
    >
      <Container>
        <Item>
          <InputSelect
            label={ t('type') }
            placeholder={ t('type') }
            name="birthType"
            options={ defaultOptions }
            fullWidth
            defaultValue={ filters.birthType }
          />
        </Item>
        <Item>
          <InputDate
            label={ t('expected date') }
            placeholder={ t('expected date') }
            name="expectedDate"
            fullWidth
            defaultValue={ filters.expectedDate }
          />
        </Item>
        <Item>
          <InputDate
            label={ t('occurrence date') }
            placeholder={ t('occurrence date') }
            name="occurrenceDate"
            fullWidth
            defaultValue={ filters.occurrenceDate }
          />
        </Item>
        <Item>
          <InputUnit
            label={ t('weight') }
            placeholder={ t('weight') }
            name="weight"
            fullWidth
            defaultValue={ filters.weight }
            unit="Kg"
          />
        </Item>
        <Item>
          <RadioGroup
            label={ t('gender') }
            placeholder={ t('gender') }
            name="gender"
            options={ genderOptions }
            fullWidth
            variant="row"
            defaultValue={ filters.gender }
          />
        </Item>
        <Item>
          <RadioGroup
            label={ t('situation') }
            placeholder={ t('situation') }
            name="isAlive"
            variant="row"
            options={ situationOptions }
            fullWidth
            defaultValue={ filters.isAlive }
          />
        </Item>
      </Container>
    </Form>
  )
})

FilterAnimalBirthForm.propTypes = {
  onSubmit: PropTypes.func,
  filters: PropTypes.object
}

FilterAnimalBirthForm.defaultProps = {
  filters: {},
  onSubmit: () => {}
}

export default FilterAnimalBirthForm
