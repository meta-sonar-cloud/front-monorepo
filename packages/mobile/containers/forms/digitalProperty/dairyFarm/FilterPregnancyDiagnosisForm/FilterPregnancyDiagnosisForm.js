import React, { useCallback, forwardRef } from 'react'
import { useSelector } from 'react-redux'

import PropTypes from 'prop-types'


import filterDiagnosisSchema from '@smartcoop/forms/schemas/dairyFarm/filterDiagnosis.schema'
import { useT } from '@smartcoop/i18n'
import Form from '@smartcoop/mobile-components/Form'
import InputDate from '@smartcoop/mobile-components/InputDate'
import InputSelect from '@smartcoop/mobile-components/InputSelect'
import { getAnimalPregnancyDiagnosticsTypes } from '@smartcoop/services/apis/smartcoopApi/resources/animalPregnancyDiagnostics'
import { selectCurrentAnimal } from '@smartcoop/stores/animal/selectorAnimal'

import { Container } from './styles'


const FilterPregnancyDiagnosisForm = forwardRef((props, formRef) => {
  const { onSubmit, filters } = props

  const t = useT()
  const currentAnimal = useSelector(selectCurrentAnimal)
  const handleSubmit = useCallback(
    (data) => {onSubmit(data)},
    [onSubmit]
  )

  return (
    <Form
      style={ { display: 'flex', flexDirection: 'column', width: '100%' } }
      ref={ formRef }
      schemaConstructor={ filterDiagnosisSchema }
      onSubmit={ handleSubmit }
    >
      <Container>
        <InputDate
          label={ t('accomplished date') }
          name="date"
          defaultValue={ filters?.date }
        />

        <InputSelect
          label={ t('type of diagnosis') }
          name="diagnosisType"
          defaultValue={ filters?.diagnosisType }
          options={ getAnimalPregnancyDiagnosticsTypes }
          urlParams={ { animalId: currentAnimal.id } }
          asyncOptionLabelField="typeName"
          asyncOptionValueField="id"
        />

        <InputSelect
          label={ t('result') }
          name="result"
          defaultValue={ filters.result }
          options={ [
            {
              label: t('pregnant'),
              value: 'Positivo'
            },
            {
              label: t('empty'),
              value: 'Vazia'
            },
            {
              label: t('inconclusive'),
              value: 'Inconclusivo'
            }
          ] }
        />
      </Container>
    </Form>
  )
})

FilterPregnancyDiagnosisForm.propTypes = {
  onSubmit: PropTypes.func,
  filters: PropTypes.object
}

FilterPregnancyDiagnosisForm.defaultProps = {
  filters: {},
  onSubmit: () => {}
}

export default FilterPregnancyDiagnosisForm
