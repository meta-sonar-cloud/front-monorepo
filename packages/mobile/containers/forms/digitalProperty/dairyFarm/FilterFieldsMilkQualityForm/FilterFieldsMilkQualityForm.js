import React, { useCallback, forwardRef } from 'react'

import PropTypes from 'prop-types'

import filterMilkQualitySchema from '@smartcoop/forms/schemas/dairyFarm/filterMilkQuality.schema'
import { useT } from '@smartcoop/i18n'
import Button from '@smartcoop/mobile-components/Button'
import Form from '@smartcoop/mobile-components/Form'
import InputDate from '@smartcoop/mobile-components/InputDate'

const FilterFieldsMilkQualityForm = forwardRef((props, formRef) => {
  const { withoutSubmitButton, onSubmit, filters } = props

  const t = useT()
  const handleSubmit = useCallback(
    (data) => onSubmit(data),
    [onSubmit]
  )

  return (
    <Form
      ref={ formRef }
      schemaConstructor={ filterMilkQualitySchema }
      onSubmit={ handleSubmit }
    >
      <InputDate
        label={ t('gathering date') }
        name="date"
        fullWidth
        defaultValue={ filters.date }
      />
      {!withoutSubmitButton && (
        <Button
          title={ t('login') }
          onPress={ () => formRef.current.submit() }
        />
      )}
    </Form>
  )
})

FilterFieldsMilkQualityForm.propTypes = {
  onSubmit: PropTypes.func,
  withoutSubmitButton: PropTypes.bool,
  filters: PropTypes.object
}

FilterFieldsMilkQualityForm.defaultProps = {
  filters: {
    date: ''
  },
  onSubmit: () => {},
  withoutSubmitButton: false
}

export default FilterFieldsMilkQualityForm
