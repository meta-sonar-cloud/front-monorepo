import React, { useCallback, forwardRef } from 'react'

import moment from 'moment'
import PropTypes from 'prop-types'

import registerMilkDeliverySchema from '@smartcoop/forms/schemas/dairyFarm/registerMilkDelivery.schema'
import I18n, { useT } from '@smartcoop/i18n'
import Button from '@smartcoop/mobile-components/Button'
import Form from '@smartcoop/mobile-components/Form'
import InputDate from '@smartcoop/mobile-components/InputDate'
import InputFloat from '@smartcoop/mobile-components/InputFloat'
import InputNumber from '@smartcoop/mobile-components/InputNumber'
import InputText from '@smartcoop/mobile-components/InputText'
import { colors } from '@smartcoop/styles'
import { formatCurrencyIntl } from '@smartcoop/utils/formatters'

import {
  ButtonsContainer,
  ButtonContainer,
  Item
} from './styles'

const RegisterMilkDeliveryForm = forwardRef((props, formRef) => {
  const {
    withoutSubmitButton,
    defaultValues,
    loading,
    onSubmit,
    onCancel
  } = props

  const t = useT()

  const handleSubmit = useCallback(
    (data) => {
      onSubmit({ ...data })
    },
    [onSubmit]
  )

  const formatValue = useCallback(
    (value) => formatCurrencyIntl(value),
    []
  )

  return (
    <Form
      style={ { display: 'flex', flexDirection: 'column', width: '100%' } }
      ref={ formRef }
      schemaConstructor={ registerMilkDeliverySchema }
      onSubmit={ handleSubmit }
    >
      <Item>
        <InputDate
          label={ t('volume date') }
          name="volumeDate"
          fullWidth
          defaultValue={ defaultValues.volumeDate }
          pickerProps={
            {
              maxDate: moment().format(),
              title: t('select one date')
            }
          }
        />
      </Item>
      <Item>
        <InputNumber
          label={ t('volume') }
          name="volume"
          fullWidth
          defaultValue={ defaultValues.volume }
        />
      </Item>
      <Item>
        <InputNumber
          label={ t('condemned volume') }
          name="condemnedVolume"
          fullWidth
          defaultValue={ defaultValues.condemnedVolume }
        />
      </Item>
      <Item>
        <InputFloat
          label={ t('temperature') }
          name="temperature"
          fullWidth
          maxLength={ 4 }
          decimalScale={ 1 }
          defaultValue={ defaultValues.temperature ? formatValue(defaultValues.temperature) : '' }
        />
      </Item>
      <Item>
        <InputText
          label={ t('type here the reason of the condemnation') }
          name="observation"
          multiline
          rows={ 4 }
          fullWidth
          defaultValue={ defaultValues.observation }
        />
      </Item>

      {!withoutSubmitButton && (
        <ButtonsContainer style={ { paddingTop: 10 } }>
          <ButtonContainer>
            <Button
              id="mobile-cancel-form-button"
              onClick={ onCancel }
              color={ colors.white }
              disabled={ loading }
            >
              <I18n>cancel</I18n>
            </Button>
          </ButtonContainer>
          <ButtonContainer>
            <Button
              id="mobile-save-form-button"
              onClick={ () => formRef.current.submit() }
              disabled={ loading }
            >
              <I18n>save</I18n>
            </Button>
          </ButtonContainer>
        </ButtonsContainer>
      )}
    </Form>
  )
})

RegisterMilkDeliveryForm.propTypes = {
  loading: PropTypes.bool,
  onSubmit: PropTypes.func,
  onCancel: PropTypes.func,
  withoutSubmitButton: PropTypes.bool,
  defaultValues: PropTypes.object.isRequired
}

RegisterMilkDeliveryForm.defaultProps = {
  loading: false,
  onSubmit: () => {},
  onCancel: () => {},
  withoutSubmitButton: false
}

export default RegisterMilkDeliveryForm
