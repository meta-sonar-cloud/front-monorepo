import styled from 'styled-components/native'

export const Item = styled.View``

export const ButtonsContainer = styled.View`
  display: flex;
  flex: 1 1 auto;
  flex-direction: row;
  justify-content: space-around;
  width: 100%;
  padding-bottom: 20px;
`

export const ButtonContainer = styled.View`
  width: 80px;
`
