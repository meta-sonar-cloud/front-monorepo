import React, {
  useState,
  useMemo,
  useEffect,
  useCallback,
  forwardRef
} from 'react'
// import { useDispatch } from 'react-redux'
import { Text } from 'react-native'
import { useSelector } from 'react-redux'

import moment from 'moment'
import PropTypes from 'prop-types'

import { filter, includes, isEmpty, map } from 'lodash'

import registerInseminationSchema from '@smartcoop/forms/schemas/dairyFarm/registerInsemination.schema'
import I18n, { useT } from '@smartcoop/i18n'
import CheckboxButton from '@smartcoop/mobile-components/CheckboxGroup/CheckboxButton'
import Form from '@smartcoop/mobile-components/Form'
import InputDate from '@smartcoop/mobile-components/InputDate'
import InputSelect from '@smartcoop/mobile-components/InputSelect'
import InputText from '@smartcoop/mobile-components/InputText'
import { getAnimals as getAnimalsService } from '@smartcoop/services/apis/smartcoopApi/resources/animal'
import { getBulls as getBullsService } from '@smartcoop/services/apis/smartcoopApi/resources/animalBulls'
import { selectCurrentProperty } from '@smartcoop/stores/property/selectorProperty'
import { colors } from '@smartcoop/styles'

import { Container, FormContainer, Item, Label } from './styles'

const RegisterInseminationForm = forwardRef((props, formRef) => {
  const { onSubmit, defaultValues, setDisabledButton } = props

  const t = useT()
  // const dispatch = useCallback(useDispatch(), [])
  const currentProperty = useSelector(selectCurrentProperty)

  const [checkValues, setCheckValues] = useState([])

  const handleSubmit = useCallback(
    (data) => {
      onSubmit({
        ...data,
        inseminationType: map(checkValues, (v) => ({ name: v, id: 0 })),
        bullId: !isEmpty(data.bullId) ? data.bullId : null,
        embryoBull: !isEmpty(data.embryoBull) ? data.embryoBull : null,
        embryoMother: !isEmpty(data.embryoMother) ? data.embryoMother : null,
        internal: includes(checkValues, 'Monta')
      })
    },
    [checkValues, onSubmit]
  )

  const handleCheckValues = useCallback(
    (value) => {
      if (includes(checkValues, value)) {
        setCheckValues(filter(checkValues, (item) => item !== value))
      } else {
        setCheckValues([...checkValues, value])
      }
    },
    [checkValues]
  )

  const service = useMemo(
    () =>
      includes(checkValues, 'Monta') ? getAnimalsService : getBullsService,
    [checkValues]
  )

  const isMonta = useMemo(() => includes(checkValues, 'Monta'), [checkValues])

  const isTypeTe = useMemo(() => includes(checkValues, 'TE'), [checkValues])

  const urlParams = useMemo(
    () => ({
      propertyId: currentProperty.id
    }),
    [currentProperty.id]
  )

  const queryParams = useMemo(() => {
    if (isMonta) {
      return { category: 'touro' }
    }
    return {}
  }, [isMonta])

  const selectBull = useMemo(
    () => (
      <InputSelect
        name="bullId"
        label={ t('bull\'s name') }
        options={ service }
        urlParams={ urlParams }
        queryParams={ queryParams }
        asyncOptionLabelField="nameAndCode"
        defaultValue={ defaultValues?.bullId || defaultValues?.internalBullId }
        key={ checkValues }
      />
    ),
    [checkValues, defaultValues.bullId, defaultValues.internalBullId, queryParams, service, t, urlParams]
  )

  useEffect(() => {
    if (!isEmpty(defaultValues?.inseminationType)) {
      setCheckValues(map(defaultValues.inseminationType, (i) => i.name))
    }
  }, [defaultValues])

  useEffect(() => {
    setDisabledButton(isEmpty(checkValues))
  }, [checkValues, setDisabledButton])

  return (
    <Container>
      <Form
        style={ { display: 'flex', flexDirection: 'column', width: '100%' } }
        ref={ formRef }
        schemaConstructor={ registerInseminationSchema }
        onSubmit={ handleSubmit }
      >
        <FormContainer>
          <Item>
            <InputDate
              label={ t('insemination date') }
              name="inseminationDate"
              fullWidth
              pickerProps={ {
                maxDate: moment().format()
              } }
              defaultValue={ defaultValues?.inseminationDate }
            />
          </Item>
          <Item>
            <I18n as={ Text }>type</I18n>
            <CheckboxButton
              label="IATF"
              value={ includes(checkValues, 'IATF') }
              checked={ includes(checkValues, 'IATF') }
              style={ { backgroundColor: colors.white, marginBottom: 0 } }
              onChange={ () => {
                handleCheckValues('IATF')
              } }
              disabled={
                includes(checkValues, 'Monta') || includes(checkValues, 'TE')
              }
            />
            <CheckboxButton
              label="CIO Visual"
              value={ includes(checkValues, 'CIO Visual') }
              checked={ includes(checkValues, 'CIO Visual') }
              style={ { backgroundColor: colors.white, marginBottom: 0 } }
              onChange={ () => {
                handleCheckValues('CIO Visual')
              } }
              disabled={
                includes(checkValues, 'Monta') || includes(checkValues, 'TE')
              }
            />
            <CheckboxButton
              label="CIO Monitorado"
              value={ includes(checkValues, 'CIO Monitorado') }
              checked={ includes(checkValues, 'CIO Monitorado') }
              style={ { backgroundColor: colors.white, marginBottom: 0 } }
              onChange={ () => {
                handleCheckValues('CIO Monitorado')
              } }
              disabled={
                includes(checkValues, 'Monta') || includes(checkValues, 'TE')
              }
            />
            <CheckboxButton
              label="TE"
              value={ includes(checkValues, 'TE') }
              checked={ includes(checkValues, 'TE') }
              style={ { backgroundColor: colors.white, marginBottom: 0 } }
              onChange={ () => {
                handleCheckValues('TE')
              } }
              disabled={ !includes(checkValues, 'TE') && checkValues.length > 0 }
            />
            <CheckboxButton
              label="Monta"
              value={ includes(checkValues, 'Monta') }
              checked={ includes(checkValues, 'Monta') }
              style={ { backgroundColor: colors.white, marginBottom: 0 } }
              onChange={ () => {
                handleCheckValues('Monta')
              } }
              disabled={
                !includes(checkValues, 'Monta') && checkValues.length > 0
              }
            />
          </Item>
          <Item>{!isTypeTe && selectBull}</Item>
          {!isMonta && (
            <Item>
              <InputText
                label={ t('inseminator') }
                name="inseminator"
                placeholder={ t('inseminator') }
                defaultValue={ defaultValues?.inseminator }
                fullWidth
              />
            </Item>
          )}
          {isTypeTe && (
            <>
              <I18n as={ Label }>embryo&apos;s data</I18n>
              <Item>
                <InputText
                  label={ t('father') }
                  name="embryoBull"
                  fullWidth
                  placeholder={ t('father') }
                  defaultValue={ defaultValues?.embryoBull }
                />
              </Item>
              <Item>
                <InputText
                  label={ t('mother') }
                  name="embryoMother"
                  fullWidth
                  placeholder={ t('mother') }
                  defaultValue={ defaultValues?.embryoMother }
                />
              </Item>
            </>
          )}
        </FormContainer>
      </Form>
    </Container>
  )
})

RegisterInseminationForm.propTypes = {
  onSubmit: PropTypes.func,
  defaultValues: PropTypes.object.isRequired,
  setDisabledButton: PropTypes.func
}

RegisterInseminationForm.defaultProps = {
  onSubmit: () => {},
  setDisabledButton: () => {}
}

export default RegisterInseminationForm
