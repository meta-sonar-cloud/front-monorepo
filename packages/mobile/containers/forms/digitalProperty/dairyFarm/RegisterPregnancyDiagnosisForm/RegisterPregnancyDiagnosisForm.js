import React, { useCallback, forwardRef } from 'react'
// import { Text } from 'react-native'
import { useSelector } from 'react-redux'

import moment from 'moment/moment'
import PropTypes from 'prop-types'

import toString from 'lodash/toString'

import registerDiagnosisActionsSchema from '@smartcoop/forms/schemas/dairyFarm/registerDiagnosisActions.schema'
import { useT } from '@smartcoop/i18n'
import Form from '@smartcoop/mobile-components/Form'
import InputDate from '@smartcoop/mobile-components/InputDate'
import InputSelect from '@smartcoop/mobile-components/InputSelect'
import InputText from '@smartcoop/mobile-components/InputText'
import { getAnimalPregnancyDiagnosticsTypes } from '@smartcoop/services/apis/smartcoopApi/resources/animalPregnancyDiagnostics'
import { selectCurrentAnimal } from '@smartcoop/stores/animal/selectorAnimal'
import { selectCurrentInsemination } from '@smartcoop/stores/insemination/selectorInsemination'
import { momentBackDateFormat } from '@smartcoop/utils/dates'

import {
  Container,
  FormContainer,
  Item
} from './styles'

const RegisterPregnancyDiagnosisForm = forwardRef((props, formRef) => {
  const {
    onSubmit,
    defaultValues
  } = props

  const t = useT()
  const currentAnimal = useSelector(selectCurrentAnimal)
  const currentInsemination = useSelector(selectCurrentInsemination)


  const handleSubmit = useCallback(
    (data) => {
      const { earring, ...rest } = data
      onSubmit({ ...rest })
    },
    [onSubmit]
  )

  return (
    <Container>
      <Form
        style={ { display: 'flex', flexDirection: 'column', width: '100%' } }
        ref={ formRef }
        schemaConstructor={ registerDiagnosisActionsSchema }
        onSubmit={ handleSubmit }
      >
        <FormContainer>
          <Item>
            <InputText
              style={ { display: 'none' } }
              name="animalId"
              fullWidth
              defaultValue={ defaultValues?.animalId }
              disabled
            />
            <InputText
              label={ t('earring') }
              name="earring"
              fullWidth
              defaultValue={ defaultValues?.earring }
              disabled
            />
          </Item>
          <Item>
            <InputSelect
              label={ t('type of diagnosis') }
              name="diagnosisTypeId"
              options={ getAnimalPregnancyDiagnosticsTypes }
              urlParams={ { animalId: currentAnimal.id } }
              asyncOptionLabelField="typeName"
              asyncOptionValueField="id"
              defaultValue={ toString(defaultValues?.diagnosisTypeId ) }
            />
          </Item>
          <Item>
            <InputDate
              label={ t('accomplished date') }
              name="realizationDate"
              pickerProps={ {
                maxDate: moment().format(),
                minDate: new Date(currentInsemination?.inseminationDate)
              } }
              fullWidth
              defaultValue={ moment(defaultValues?.realizationDate, momentBackDateFormat) || moment() }
            />
          </Item>
          <Item>
            <InputSelect
              label={ t('result') }
              name="result"
              defaultValue={ defaultValues.result }
              options={ [
                {
                  label: t('pregnant'),
                  value: 'Positivo'
                },
                {
                  label: t('empty'),
                  value: 'Vazia'
                },
                {
                  label: t('inconclusive'),
                  value: 'Inconclusivo'
                }
              ] }
            />
          </Item>
        </FormContainer>
      </Form>
    </Container>
  )
})

RegisterPregnancyDiagnosisForm.propTypes = {
  onSubmit: PropTypes.func,
  defaultValues: PropTypes.object.isRequired
}

RegisterPregnancyDiagnosisForm.defaultProps = {
  onSubmit: () => {}
}

export default RegisterPregnancyDiagnosisForm
