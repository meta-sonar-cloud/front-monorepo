import styled from 'styled-components/native'

import fonts from '@smartcoop/styles/fonts'

export const Container = styled.View`
  display: flex;
  justify-content: center;
  align-items: center;
  flex: 1 1 auto;
  flex-direction: column;
`

export const FormContainer = styled.View`
    width: 100%;
    display: flex;
`

export const Item = styled.View`
    /* justify-content: center; */
  /* align-items: center; */
  /* align-content: center; */
`

export const ButtonsContainer = styled.View`
  display: flex;
  flex: 1 1 auto;
  flex-direction: row;
  justify-content: space-around;
  width: 100%;
  padding-bottom: 20px;
`

export const ButtonContainer = styled.View`
  width: 80px;
`
export const InputContainer = styled.View`
  display: flex;
  justify-content: space-between;
`

export const Label = styled.Text`
  font-size: ${ `${ fonts.fontSize.S }px` };
  font-family: ${ fonts.fontFamilyMontserrat };
  margin-bottom: 10px;
  font-weight: ${ fonts.fontWeight.semiBold };
`
