import React, {
  useState,
  useCallback,
  forwardRef,
  useEffect,
  useMemo
} from 'react'
import { useSelector } from 'react-redux'

import moment from 'moment/moment'
import PropTypes from 'prop-types'

import { isEmpty } from 'lodash'

import registerPregnancyActionsSchema from '@smartcoop/forms/schemas/dairyFarm/registerPregnancyActions.schema'
import I18n, { useT } from '@smartcoop/i18n'
import Form from '@smartcoop/mobile-components/Form'
import InputDate from '@smartcoop/mobile-components/InputDate'
import InputSelect from '@smartcoop/mobile-components/InputSelect'
import InputText from '@smartcoop/mobile-components/InputText'
import { selectCurrentAnimal } from '@smartcoop/stores/animal/selectorAnimal'
import {
  selectLotsOptions,
  selectPreDefinedLots
} from '@smartcoop/stores/lot/selectorLot'
import { colors } from '@smartcoop/styles'
import { momentBackDateFormat } from '@smartcoop/utils/dates'

import { Container, FormContainer, Item } from './styles'

const RegisterPregnancyActionsForm = forwardRef((props, formRef) => {
  const { onSubmit, defaultValues } = props

  const t = useT()

  const [disableLot, setDisableLot] = useState(false)
  const [statusDoesNotMatchWithType, setStatusDoesNotMatchWithType] = useState(
    false
  )
  const currentAnimal = useSelector(selectCurrentAnimal)
  const lotsOptions = useSelector(selectLotsOptions)
  const preDefinedLots = useSelector(selectPreDefinedLots)

  const options = useMemo(() => (isEmpty(lotsOptions) ? [] : lotsOptions), [
    lotsOptions
  ])
  const preDefinedOptions = useMemo(
    () => (isEmpty(preDefinedLots) ? {} : preDefinedLots),
    [preDefinedLots]
  )

  const handleSubmit = useCallback(
    (data) => {
      onSubmit({
        ...data
      })
    },
    [onSubmit]
  )

  const handleTypeChange = useCallback(
    (value) => {
      switch (value) {
        case 'aborto':
          setDisableLot(false)
          formRef.current.getFieldRef('lotId').setValue('')
          setStatusDoesNotMatchWithType(
            currentAnimal?.animalStatus.name !== 'Prenha'
          )
          break
        case 'pre-parto':
          setDisableLot(true)
          formRef.current
            .getFieldRef('lotId')
            .setValue(preDefinedOptions?.prepartum?.value)
          setStatusDoesNotMatchWithType(
            currentAnimal?.animalStatus.name !== 'Prenha'
          )
          break
        case 'secagem':
        default:
          setDisableLot(true)
          formRef.current
            .getFieldRef('lotId')
            .setValue(preDefinedOptions?.drying?.value)
          setStatusDoesNotMatchWithType(
            currentAnimal?.animalStatus.name !== 'Prenha' ||
              currentAnimal?.category !== 'vaca'
          )
          break
      }
    },
    [currentAnimal, formRef, preDefinedOptions]
  )

  useEffect(() => {
    const { type } = defaultValues
    if (type !== 'aborto') {
      setDisableLot(true)
    } else {
      setDisableLot(false)
    }
  }, [defaultValues, handleTypeChange])

  return (
    <Container>
      <Form
        style={ { display: 'flex', flexDirection: 'column', width: '100%' } }
        ref={ formRef }
        schemaConstructor={ registerPregnancyActionsSchema }
        onSubmit={ handleSubmit }
      >
        <FormContainer>
          <Item>
            <InputText
              style={ { display: 'none' } }
              name="animalId"
              fullWidth
              defaultValue={ defaultValues?.animalId }
              disabled
            />
            <InputText
              label={ t('earring') }
              name="earring"
              fullWidth
              defaultValue={ defaultValues?.earring }
              disabled
            />
          </Item>
          <Item>
            <InputSelect
              label={ t('type') }
              name="type"
              options={ [
                {
                  label: t('drying'),
                  value: 'secagem'
                },
                {
                  label: t('prepartum'),
                  value: 'pre-parto'
                },
                {
                  label: t('abortion'),
                  value: 'aborto'
                }
              ] }
              onChange={ ({ target: { value } }) => handleTypeChange(value) }
              defaultValue={ defaultValues?.type }
            />
          </Item>
          <Item>
            <InputSelect
              label={ t('lot') }
              name="lotId"
              options={ options }
              defaultValue={ defaultValues?.animal?.lot?.id }
              disabled={ disableLot }
            />
          </Item>
          <Item>
            <InputDate
              label={ t('accomplished date') }
              name="accomplishedDate"
              pickerProps={ {
                maxDate: moment().format()
              } }
              fullWidth
              defaultValue={
                moment(defaultValues?.accomplishedDate, momentBackDateFormat) ||
                moment()
              }
            />
          </Item>
          {statusDoesNotMatchWithType && (
            <Item style={ { marginBottom: 15 } }>
              <I18n style={ { color: colors.red } }>
                invalid animal status or category
              </I18n>
            </Item>
          )}
        </FormContainer>
      </Form>
    </Container>
  )
})

RegisterPregnancyActionsForm.propTypes = {
  onSubmit: PropTypes.func,
  defaultValues: PropTypes.object.isRequired
}

RegisterPregnancyActionsForm.defaultProps = {
  onSubmit: () => {}
}

export default RegisterPregnancyActionsForm
