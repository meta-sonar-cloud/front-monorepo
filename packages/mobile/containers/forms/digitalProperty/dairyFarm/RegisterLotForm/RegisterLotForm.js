import React, { useCallback, forwardRef } from 'react'

import PropTypes from 'prop-types'

import registerLotSchema from '@smartcoop/forms/schemas/dairyFarm/registerLot.schema'
import { useT } from '@smartcoop/i18n'
import Form from '@smartcoop/mobile-components/Form'
import InputText from '@smartcoop/mobile-components/InputText'

import {
  Container,
  FormContainer,
  Item
} from './styles'

const RegisterLotForm = forwardRef((props, formRef) => {
  const {
    defaultValues,
    onSubmit
  } = props

  const t = useT()

  const handleSubmit = useCallback(
    (data) => {
      onSubmit({
        ...data
      })
    },
    [onSubmit]
  )

  return (
    <Container>
      <Form
        style={ { display: 'flex', flexDirection: 'column', width: '100%' } }
        ref={ formRef }
        schemaConstructor={ registerLotSchema }
        onSubmit={ handleSubmit }
      >
        <FormContainer>
          <Item>
            <InputText
              label={ t('lot name') }
              name="name"
              fullWidth
              defaultValue={ defaultValues.name }
            />
          </Item>
        </FormContainer>
      </Form>
    </Container>
  )
})

RegisterLotForm.propTypes = {
  onSubmit: PropTypes.func,
  defaultValues: PropTypes.object.isRequired
}

RegisterLotForm.defaultProps = {
  onSubmit: () => {}
}

export default RegisterLotForm
