import React, { useMemo, useCallback, forwardRef } from 'react'

import moment from 'moment/moment'
import PropTypes from 'prop-types'

import registerPevSchema from '@smartcoop/forms/schemas/dairyFarm/registerPev.schema'
import { useT } from '@smartcoop/i18n'
import Form from '@smartcoop/mobile-components/Form'
import InputDate from '@smartcoop/mobile-components/InputDate'
import InputText from '@smartcoop/mobile-components/InputText'
import RadioGroup from '@smartcoop/mobile-components/RadioGroup'

import {
  Container,
  FormContainer,
  Item
} from './styles'

const RegisterAnimalPevForm = forwardRef((props, formRef) => {
  const {
    onSubmit,
    defaultValues
  } = props

  const t = useT()

  const handleSubmit = useCallback(
    (data) => {
      onSubmit({
        ...data
      })
    },
    [onSubmit]
  )

  const ableOptions = useMemo(
    () => ([
      { label: t('yes'), value: true },
      { label: t('no'), value: false }
    ]),
    [t]
  )

  return (
    <Container>
      <Form
        style={ { display: 'flex', flexDirection: 'column', width: '100%' } }
        ref={ formRef }
        schemaConstructor={ registerPevSchema }
        onSubmit={ handleSubmit }
      >
        <FormContainer>
          <Item>
            <InputDate
              label={ t('registry date') }
              name="registryDate"
              fullWidth
              defaultValue={ defaultValues.registryDate }
              pickerProps={ {
                maxDate: moment().format()
              } }
            />
          </Item>

          <Item>
            <InputText
              name="reason"
              label={ t('reason') }
              fullWidth
              defaultValue={ defaultValues.reason }
              multiline
            />
          </Item>

          <Item>
            <RadioGroup
              label={ t('able?') }
              name="able"
              options={ ableOptions }
              variant="row"
              defaultValue={ defaultValues.able }
              disabled={ !!defaultValues.id }
            />
          </Item>
        </FormContainer>
      </Form>
    </Container>
  )
})

RegisterAnimalPevForm.propTypes = {
  onSubmit: PropTypes.func,
  defaultValues: PropTypes.object.isRequired
}

RegisterAnimalPevForm.defaultProps = {
  onSubmit: () => {}
}

export default RegisterAnimalPevForm
