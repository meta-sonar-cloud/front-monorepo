import React, { useCallback, forwardRef } from 'react'
import { useSelector } from 'react-redux'

import PropTypes from 'prop-types'


import filterLotSchema from '@smartcoop/forms/schemas/dairyFarm/filterLot.schema'
import { useT } from '@smartcoop/i18n'
import Form from '@smartcoop/mobile-components/Form'
import InputSelect from '@smartcoop/mobile-components/InputSelect'
import { getLots as getLotsService } from '@smartcoop/services/apis/smartcoopApi/resources/lot'
import { selectCurrentProperty } from '@smartcoop/stores/property/selectorProperty'

import { Container } from './styles'


const FilterLotForm = forwardRef((props, formRef) => {
  const { onSubmit, filters } = props

  const t = useT()
  const { id: propertyId } = useSelector(selectCurrentProperty)
  const handleSubmit = useCallback(
    (data) => onSubmit(data),
    [onSubmit]
  )

  return (
    <Form
      style={ { display: 'flex', flexDirection: 'column', width: '100%' } }
      ref={ formRef }
      schemaConstructor={ filterLotSchema }
      onSubmit={ handleSubmit }
    >
      <Container>
        <InputSelect
          label={ t('lot code') }
          name="code"
          options={ getLotsService }
          urlParams={ { propertyId } }
          asyncOptionLabelField="code"
          asyncOptionValueField="code"
          defaultValue={ filters.code }
        />

        <InputSelect
          label={ t('name of lot') }
          name="name"
          options={ getLotsService }
          urlParams={ { propertyId } }
          asyncOptionLabelField="name"
          asyncOptionValueField="name"
          defaultValue={ filters.name }
        />

      </Container>
    </Form>
  )
})

FilterLotForm.propTypes = {
  onSubmit: PropTypes.func,
  filters: PropTypes.object
}

FilterLotForm.defaultProps = {
  filters: {},
  onSubmit: () => {}
}

export default FilterLotForm
