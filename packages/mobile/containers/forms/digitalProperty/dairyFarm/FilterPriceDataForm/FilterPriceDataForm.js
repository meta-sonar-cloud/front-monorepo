import React, { useCallback, forwardRef } from 'react'

import PropTypes from 'prop-types'

import filterPriceDataSchema from '@smartcoop/forms/schemas/dairyFarm/filterPriceData.schema'
import { useT } from '@smartcoop/i18n'
import Button from '@smartcoop/mobile-components/Button'
import Form from '@smartcoop/mobile-components/Form'
import InputMonthYear from '@smartcoop/mobile-components/InputMonthYear'
import InputUnit from '@smartcoop/mobile-components/InputUnit'

import { Container } from './styles'

const FilterPriceDataForm = forwardRef((props, formRef) => {
  const { withoutSubmitButton, onSubmit, filters } = props

  const t = useT()

  const handleSubmit = useCallback(
    (data) => onSubmit({
      ...data,
      initialPrice: data.initialPrice.replace(',', '.'),
      finalPrice: data.finalPrice.replace(',', '.')
    }),
    [onSubmit]
  )

  return (
    <Form
      ref={ formRef }
      schemaConstructor={ filterPriceDataSchema }
      onSubmit={ handleSubmit }
    >
      <Container>
        <InputMonthYear
          label={ t('initial month/year') }
          name="from"
          fullWidth
          defaultValue={ filters.from }
        />

        <InputMonthYear
          label={ t('final month/year') }
          name="to"
          fullWidth
          defaultValue={ filters.to }
        />

        <InputUnit
          name="initialPrice"
          label={ t('initial price') }
          unit="R$"
          type="float"
          fullWidth
          defaultValue={ filters.initialPrice }
        />

        <InputUnit
          name="finalPrice"
          label={ t('final price') }
          unit="R$"
          type="float"
          fullWidth
          defaultValue={ filters.finalPrice }
        />

        {!withoutSubmitButton && (
          <Button
            title={ t('login') }
            onPress={ () => formRef.current.submit() }
          />
        )}
      </Container>
    </Form>
  )
})

FilterPriceDataForm.propTypes = {
  onSubmit: PropTypes.func,
  withoutSubmitButton: PropTypes.bool,
  filters: PropTypes.object
}

FilterPriceDataForm.defaultProps = {
  filters: {},
  onSubmit: () => {},
  withoutSubmitButton: false
}

export default FilterPriceDataForm
