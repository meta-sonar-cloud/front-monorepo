import React, { useCallback, forwardRef, useState, useEffect } from 'react'
import { useSelector } from 'react-redux'

import moment from 'moment/moment'
import PropTypes from 'prop-types'

import filter from 'lodash/filter'
import includes from 'lodash/includes'


import filterCattleManagementSchema from '@smartcoop/forms/schemas/dairyFarm/filterCattleManagement.schema'
import { useT } from '@smartcoop/i18n'
import CheckboxButton from '@smartcoop/mobile-components/CheckboxGroup/CheckboxButton'
import Form from '@smartcoop/mobile-components/Form'
import InputDateRange from '@smartcoop/mobile-components/InputDateRange'
import InputSelect from '@smartcoop/mobile-components/InputSelect'
import { getAnimalBreeds as getAnimalBreedsService } from '@smartcoop/services/apis/smartcoopApi/resources/animalBreed'
import { getAnimalStatus as getAnimalStatusService } from '@smartcoop/services/apis/smartcoopApi/resources/animalStatus'
import { getLots as getLotsService } from '@smartcoop/services/apis/smartcoopApi/resources/lot'
import { selectCurrentProperty } from '@smartcoop/stores/property/selectorProperty'
import { momentBackDateTimeFormat } from '@smartcoop/utils/dates'

import { Container } from './styles'


const FilterCattleManagementForm = forwardRef((props, formRef) => {
  const { onSubmit, filters } = props

  const t = useT()
  const { id: propertyId } = useSelector(selectCurrentProperty)
  const [checkValues, setCheckValues] = useState([])
  const handleSubmit = useCallback(
    (data) => onSubmit({
      ...data,
      startBirthDate: data.birthDate.from,
      endBirthDate: data.birthDate.to,
      birthDate: undefined,
      dead: includes(checkValues, 'dead')
    }),
    [checkValues, onSubmit]
  )

  const handleChangeCheckBox = useCallback((value) => {
    if(includes(checkValues, value)) {
      setCheckValues(filter(checkValues, (item) => item !== value))
    } else {
      setCheckValues([...checkValues, value])
    }
  }, [checkValues])

  useEffect(() => {
    if(filters.dead) {
      setCheckValues([filters.dead ? 'dead' : ''])
    }
  }, [filters])

  return (
    <Form
      style={ { display: 'flex', flexDirection: 'column', width: '100%' } }
      ref={ formRef }
      schemaConstructor={ filterCattleManagementSchema }
      onSubmit={ handleSubmit }
    >
      <Container>

        <InputDateRange
          label={ t('date of birth') }
          name="birthDate"
          defaultValue={
            filters.startBirthDate ?
              {
                from: moment(filters.startBirthDate, momentBackDateTimeFormat), to: moment(filters.endBirthDate, momentBackDateTimeFormat)
              } : {
                from: '', to: ''
              }
          }
          fullWidth
        />

        <InputSelect
          label={ t('predominant race') }
          name="predominantBreedId"
          options={ getAnimalBreedsService }
          defaultValue={ filters?.predominantBreedId }
        />

        <InputSelect
          label={ t('status') }
          name="statusId"
          options={ getAnimalStatusService }
          defaultValue={ filters?.animalStatus?.id }
        />
        <InputSelect
          label={ t('name of lot') }
          name="lotId"
          options={ getLotsService }
          urlParams={ { propertyId } }
          asyncOptionLabelField="name"
          defaultValue={ filters?.lotId }
        />
        <CheckboxButton
          label={ t('display animals already slaughtered') }
          onChange={ () => handleChangeCheckBox('dead') }
          checked={ includes(checkValues, 'dead') }
          value={ includes(checkValues, 'dead') }
        />

      </Container>
    </Form>
  )
})

FilterCattleManagementForm.propTypes = {
  onSubmit: PropTypes.func,
  filters: PropTypes.object
}

FilterCattleManagementForm.defaultProps = {
  filters: {},
  onSubmit: () => {}
}

export default FilterCattleManagementForm
