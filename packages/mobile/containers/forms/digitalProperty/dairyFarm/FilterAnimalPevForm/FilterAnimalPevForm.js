import React, { useMemo, useCallback, forwardRef } from 'react'

import moment from 'moment/moment'
import PropTypes from 'prop-types'

import isBoolean from 'lodash/isBoolean'
import isEmpty from 'lodash/isEmpty'
import omitBy from 'lodash/omitBy'

import filterPevSchema from '@smartcoop/forms/schemas/dairyFarm/filterPev.schema'
import { useT } from '@smartcoop/i18n'
import Form from '@smartcoop/mobile-components/Form'
import InputDate from '@smartcoop/mobile-components/InputDate'
import InputText from '@smartcoop/mobile-components/InputText'
import RadioGroup from '@smartcoop/mobile-components/RadioGroup'

import {
  Container,
  FormContainer,
  Item
} from './styles'

const FilterAnimalPevForm = forwardRef((props, formRef) => {
  const {
    onSubmit,
    filters
  } = props

  const t = useT()

  const handleSubmit = useCallback(
    (data) => {
      onSubmit(omitBy(data, val => isBoolean(val) ? false : isEmpty(val)))
    },
    [onSubmit]
  )

  const ableOptions = useMemo(
    () => ([
      { label: t('yes'), value: true },
      { label: t('no'), value: false }
    ]),
    [t]
  )

  return (
    <Container>
      <Form
        style={ { display: 'flex', flexDirection: 'column', width: '100%' } }
        ref={ formRef }
        schemaConstructor={ filterPevSchema }
        onSubmit={ handleSubmit }
      >
        <FormContainer>
          <Item>
            <InputDate
              label={ t('registry date') }
              name="registryDate"
              defaultValue={ filters.registryDate }
              pickerProps={ {
                minDate: moment().format()
              } }
            />
          </Item>

          <Item>
            <InputText
              name="reason"
              label={ t('reason') }
              defaultValue={ filters.reason }
              multiline
            />
          </Item>

          <Item>
            <RadioGroup
              label={ t('able?') }
              name="able"
              options={ ableOptions }
              variant="row"
              defaultValue={ filters.able }
              clearable
            />
          </Item>
        </FormContainer>
      </Form>
    </Container>
  )
})

FilterAnimalPevForm.propTypes = {
  onSubmit: PropTypes.func,
  filters: PropTypes.object.isRequired
}

FilterAnimalPevForm.defaultProps = {
  onSubmit: () => {}
}

export default FilterAnimalPevForm
