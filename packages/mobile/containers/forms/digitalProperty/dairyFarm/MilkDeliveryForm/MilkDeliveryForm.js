import React, {
  useMemo,
  useCallback,
  forwardRef,
  useState,
  useEffect
} from 'react'
import { useDispatch } from 'react-redux'

import PropTypes from 'prop-types'

import isEmpty from 'lodash/isEmpty'

import milkDeliverySchema from '@smartcoop/forms/schemas/dairyFarm/milkDelivery.schema'
import { useT } from '@smartcoop/i18n'
import Button from '@smartcoop/mobile-components/Button'
import Form from '@smartcoop/mobile-components/Form'
import InputSelect from '@smartcoop/mobile-components/InputSelect'
import InputText from '@smartcoop/mobile-components/InputText'
import RadioGroup from '@smartcoop/mobile-components/RadioGroup'
import { getOrganizationsByUser as getOrganizationsByUserService } from '@smartcoop/services/apis/smartcoopApi/resources/organization'
import { useSnackbar } from '@smartcoop/snackbar'
import { DairyFarmActions } from '@smartcoop/stores/dairyFarm'
import colors from '@smartcoop/styles/colors'

import { Container, ButtonGroup } from './styles'

const MilkDeliveryForm = forwardRef((props, formRef) => {
  const t = useT()

  const dispatch = useCallback(useDispatch(), [])

  const { handleClose, dairyFarm, clearForm } = props
  const [textInputValue, setTextInputValue] = useState('')
  const [selectInputValue, setSelectInputValue] = useState('')

  const isCooperative = useMemo(() => {
    if (isEmpty(dairyFarm)) {
      return true
    }
    return !isEmpty(dairyFarm?.organizationId)
  }, [dairyFarm])

  const [tamboType, setTamboType] = useState(
    isCooperative ? 'cooperative' : 'third party'
  )

  const snackbar = useSnackbar()

  const onSuccess = useCallback(() => {
    snackbar.success(
      t('your {this} was registered', {
        howMany: 1,
        gender: 'male',
        this: t('dairy farm', { howMany: 1 })
      })
    )
    handleClose()
  }, [handleClose, snackbar, t])

  const handleSubmit = useCallback(
    (data) => {
      dispatch(DairyFarmActions.saveTamboType(data, () => onSuccess()))
    },
    [dispatch, onSuccess]
  )

  const modalOptions = useMemo(
    () => [
      {
        label: t('cooperative', { howMany: 1 }),
        value: 'cooperative'
      },
      {
        label: t('others'),
        value: 'third party'
      }
    ],
    [t]
  )

  const isDisabled = useMemo(
    () =>
      tamboType === 'third party'
        ? isEmpty(textInputValue)
        : isEmpty(selectInputValue),
    [selectInputValue, tamboType, textInputValue]
  )

  const stateRadioGroup = useMemo(() => {
    if (tamboType === 'third party') {
      return (
        <InputText
          name="companyName"
          label={ t('enter the company name') }
          style={ { paddingBottom: 10, width: 300 } }
          defaultValue={ dairyFarm?.companyName || '' }
          onChange={ ({ target }) => setTextInputValue(target.value) }
        />
      )
    }
    return (
      <InputSelect
        label="Selecione a Cooperativa "
        name="organizationId"
        asyncOptionValueField="id"
        asyncOptionLabelField="tradeName"
        options={ getOrganizationsByUserService }
        queryParams={ { type: 1, isSubsidiary: false } }
        defaultValue={ dairyFarm?.organizationId || {} }
        onChange={ ({ target }) => setSelectInputValue(target.value) }
        style={ { paddingBottom: 10 } }
        clearable
      />
    )
  }, [dairyFarm, t, tamboType])

  useEffect(() => {
    setSelectInputValue(dairyFarm?.organizationId)
    setTextInputValue(dairyFarm?.companyName)
  }, [dairyFarm])

  return (
    <Container>
      <Form
        style={ { display: 'flex', flexDirection: 'column', width: '100%' } }
        ref={ formRef }
        schemaConstructor={ milkDeliverySchema }
        onSubmit={ handleSubmit }
      >
        <RadioGroup
          onChange={ (e) => setTamboType(e.target?.value) }
          label=""
          name="tamboType"
          options={ modalOptions }
          style={ { paddingBottom: 10, width: 200, fontWeight: 'bold' } }
          defaultValue={ tamboType }
        />
        {stateRadioGroup}
        <ButtonGroup>
          <Button
            id="clear"
            onPress={ () => clearForm() }
            variant="outlined"
            style={ { marginRight: 7 } }
            title={ t('cancel') }
          />
          <Button
            id="save"
            onPress={ () => formRef.current.submit() }
            style={ { marginLeft: 7, fontColor: colors.white } }
            title={ t('save') }
            disabled={ isDisabled }
          />
        </ButtonGroup>
      </Form>
    </Container>
  )
})

MilkDeliveryForm.propTypes = {
  handleClose: PropTypes.func.isRequired,
  dairyFarm: PropTypes.object,
  clearForm: PropTypes.func
}

MilkDeliveryForm.defaultProps = {
  dairyFarm: {},
  clearForm: () => {}
}

export default MilkDeliveryForm
