import styled from 'styled-components'

export const Container = styled.View`
  display: flex;
  justify-content: center;
  align-items: center;
  flex: 1 1 auto;
  flex-direction: column;
  width: 100%;
`
export const InputContainer = styled.View`
  display: flex;
  justify-content: space-between;
`

export const ButtonGroup = styled.View`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`
