import React, {
  forwardRef,
  useRef,
  useCallback,
  useMemo,
  useState
} from 'react'
import { View } from 'react-native'
import { useSelector, useDispatch } from 'react-redux'

import { useNavigation } from '@react-navigation/native'
import moment from 'moment/moment'

import { find, isEmpty } from 'lodash'
import lowerCase from 'lodash/lowerCase'
import omitBy from 'lodash/omitBy'
import toString from 'lodash/toString'

import { useDialog } from '@smartcoop/dialog'
import registerAnimal from '@smartcoop/forms/schemas/dairyFarm/registerAnimal.schema'
import registerNewAnimal from '@smartcoop/forms/schemas/dairyFarm/registerNewAnimal.schema'
import { useT } from '@smartcoop/i18n'
import Button from '@smartcoop/mobile-components/Button'
import Form from '@smartcoop/mobile-components/Form'
import InputDate from '@smartcoop/mobile-components/InputDate'
import InputSelect from '@smartcoop/mobile-components/InputSelect'
import InputText from '@smartcoop/mobile-components/InputText'
import {
  Scroll,
  ButtonsContainer
} from '@smartcoop/mobile-containers/layouts/AuthenticatedLayout/theme'
import LoadingModal from '@smartcoop/mobile-containers/modals/LoadingModal'
import { getAnimals as getAnimalsService } from '@smartcoop/services/apis/smartcoopApi/resources/animal'
import { getAnimalBreeds as getAnimalBreedsService } from '@smartcoop/services/apis/smartcoopApi/resources/animalBreed'
import { getBulls as getBullsService } from '@smartcoop/services/apis/smartcoopApi/resources/animalBulls'
import { getAnimalReasons as getAnimalReasonsService } from '@smartcoop/services/apis/smartcoopApi/resources/animalReason'
import { getAnimalStatus as getAnimalStatusService } from '@smartcoop/services/apis/smartcoopApi/resources/animalStatus'
import { getLots as getLotsService } from '@smartcoop/services/apis/smartcoopApi/resources/lot'
import { useSnackbar } from '@smartcoop/snackbar'
import { AnimalActions } from '@smartcoop/stores/animal'
import {
  selectCurrentAnimal,
  selectIsDetail
} from '@smartcoop/stores/animal/selectorAnimal'
import { selectLotsOptions } from '@smartcoop/stores/lot/selectorLot'
import { selectCurrentProperty } from '@smartcoop/stores/property/selectorProperty'
import {
  SLUG_ANIMALS_STATUS,
  TYPE_ANIMALS,
  ANIMALS_CATEGORY
} from '@smartcoop/utils/constants'
import { momentBackDateFormat } from '@smartcoop/utils/dates'

import { FormContainer, Item, Container } from './styles'

const RegisterAnimalForm = forwardRef(() => {
  const t = useT()
  const formRef = useRef(null)
  const dispatch = useCallback(useDispatch(), [])
  const navigation = useNavigation()
  const snackbar = useSnackbar()
  const currentProperty = useSelector(selectCurrentProperty)
  const currentAnimal = useSelector(selectCurrentAnimal)
  const isDetail = useSelector(selectIsDetail)
  const lotsOptions = useSelector(selectLotsOptions)
  const [slaughterDate, setSlaughterDate] = useState(
    currentAnimal?.slaughterDate || ''
  )
  const { createDialog, removeDialog } = useDialog()

  const isNewAnimal = useMemo(() => {
    if (currentAnimal.isNewAnimal) {
      return true
    }
    return false
  }, [currentAnimal.isNewAnimal])

  const handleClose = useCallback(() => {
    navigation.navigate('DigitalProperty', { screen: 'DairyFarmDashboard' })
  }, [navigation])

  const onSuccess = useCallback(() => {
    snackbar.success(
      t(
        currentAnimal?.id
          ? 'your {this} was edited'
          : 'your {this} was registered',
        {
          howMany: 1,
          gender: 'male',
          this: t('animal', { howMany: 1 })
        }
      )
    )
    handleClose()
  }, [currentAnimal, handleClose, snackbar, t])

  const handleSubmit = useCallback(() => {
    createDialog({
      id: 'loading',
      Component: LoadingModal,
      props: {
        message: t('saving your {this}', {
          howMany: 1,
          gender: 'male',
          this: t('animal', { howMany: 1 })
        })
      }
    })

    const removeLoader = () => {
      removeDialog({ id: 'loading' })
    }

    const newData = omitBy(formRef.current.getData(), (item) => item === '')
    dispatch(
      AnimalActions.saveAnimal(
        {
          fatherTable: TYPE_ANIMALS.bulls,
          maternalGrandfatherTable: TYPE_ANIMALS.bulls,
          maternalGreatGrandfatherTable: TYPE_ANIMALS.bulls,
          ...currentAnimal,
          ...newData
        },
        () => {
          onSuccess()
          removeLoader()
        },
        removeLoader
      )
    )
  }, [createDialog, currentAnimal, dispatch, onSuccess, removeDialog, t])

  const optionsCategory = useMemo(
    () => [
      {
        label: 'Touro',
        value: 'touro'
      },
      {
        label: 'Boi',
        value: 'boi'
      },
      {
        label: 'Terneira',
        value: 'terneira'
      },
      {
        label: 'Novilha',
        value: 'novilha'
      },
      {
        label: 'Vaca',
        value: 'vaca'
      }
    ],
    []
  )

  const optionsMotivation = useMemo(
    () => [
      {
        label: 'Descarte',
        value: 'descarte'
      },
      {
        label: 'Morte',
        value: 'morte'
      },
      {
        label: 'Venda',
        value: 'venda'
      }
    ],
    []
  )

  const queryParamsStatus = useMemo(
    () =>
      !isEmpty(currentAnimal)
        ? {}
        : {
          slug: [
            SLUG_ANIMALS_STATUS.nenhum,
            SLUG_ANIMALS_STATUS.aptas,
            SLUG_ANIMALS_STATUS.vazia
          ]
        },
    [currentAnimal]
  )

  const defaultLot = useMemo(() => {
    if (currentAnimal.isNewAnimal) {
      const terneiraLot = find(lotsOptions, (item) => item.label === 'Terneira')

      return terneiraLot.value
    }
    return currentAnimal?.lot?.id
  }, [currentAnimal, lotsOptions])

  return (
    <Container>
      <Scroll>
        <Form
          ref={ formRef }
          schemaConstructor={ isNewAnimal ? registerNewAnimal : registerAnimal }
          onSubmit={ handleSubmit }
        >
          <FormContainer>
            <InputText
              label={ t('{this} name', {
                gender: 'male',
                this: t('animal', { howMany: 1 })
              }) }
              name="name"
              defaultValue={ currentAnimal?.name }
              disabled={ isDetail }
            />

            <InputText
              label={ t('earring') }
              name="earringCode"
              defaultValue={ currentAnimal?.earring?.earringCode }
              disabled={ isDetail }
            />

            <InputSelect
              label={ t('category') }
              name="category"
              options={ optionsCategory }
              defaultValue={ lowerCase(currentAnimal?.category) }
              disabled={ isDetail }
            />

            <InputDate
              label={ t('date of birth') }
              name="birthDate"
              maxDate={ new Date() }
              defaultValue={
                currentAnimal?.birthDate
                  ? moment(
                    currentAnimal?.birthDate,
                    momentBackDateFormat
                  ).format(momentBackDateFormat)
                  : ''
              }
              disabled={ isDetail || currentAnimal.isNewAnimal }
              fullWidth
            />

            <InputSelect
              label={ t('lot') }
              name="lotId"
              options={ getLotsService }
              urlParams={ { propertyId: currentProperty?.id } }
              defaultValue={ defaultLot }
              disabled={ isDetail }
            />

            <InputSelect
              label={ t('predominant race') }
              name="predominantBreedId"
              options={ getAnimalBreedsService }
              defaultValue={ toString(currentAnimal?.predominantBreed?.id) }
              disabled={ isDetail }
            />

            <InputSelect
              label={ t('status') }
              name="statusId"
              options={ getAnimalStatusService }
              queryParams={ queryParamsStatus }
              defaultValue={ currentAnimal?.animalStatus?.id }
              disabled={
                (!currentAnimal.isNewAnimal &&
                  (isDetail || !isEmpty(currentAnimal))) ||
                currentAnimal.isNewAnimal
              }
            />

            {!currentAnimal.isNewAnimal && (
              <InputDate
                label={ t('date of discharge') }
                name="slaughterDate"
                maxDate={ new Date() }
                defaultValue={
                  currentAnimal?.slaughterDate
                    ? moment(currentAnimal?.slaughterDate, momentBackDateFormat)
                    : ''
                }
                onChangeForm={ (value) => setSlaughterDate(value) }
                disabled={ isDetail }
                fullWidth
              />
            )}

            {!isEmpty(slaughterDate) && (
              <>
                <InputSelect
                  name="motivation"
                  label={ t('select the reason for the discharge') }
                  options={ optionsMotivation }
                  defaultValue={ currentAnimal?.motivation }
                  disabled={ isDetail }
                  fullWidth
                />

                <InputSelect
                  label={ t('reason') }
                  name="slaughterReasonId"
                  options={ getAnimalReasonsService }
                  defaultValue={ currentAnimal?.slaughterReason?.id }
                  asyncOptionLabelField="description"
                  disabled={ isDetail }
                />
              </>
            )}

            {!currentAnimal.isNewAnimal && (
              <>
                <InputSelect
                  label={ t('{this} code', {
                    this: t('father')
                  }) }
                  name="fatherCode"
                  options={ getBullsService }
                  urlParams={ { propertyId: currentProperty?.id } }
                  asyncOptionLabelField="nameAndCode"
                  disabled={ isDetail }
                  defaultValue={ currentAnimal?.fatherBull?.id }
                />

                <InputSelect
                  label={ t('{this} code', {
                    this: t('mother')
                  }) }
                  name="motherEarringId"
                  options={ getAnimalsService }
                  asyncOptionLabelField="nameAndEarring"
                  asyncOptionValueField="earring.id"
                  urlParams={ { propertyId: currentProperty?.id } }
                  queryParams={ { category: ANIMALS_CATEGORY.vaca } }
                  disabled={ isDetail }
                  defaultValue={ currentAnimal?.motherEarring?.id }
                />

                <InputSelect
                  label={ t('{this} code', {
                    this: t('maternal grandfather')
                  }) }
                  options={ getBullsService }
                  name="maternalGrandfatherCode"
                  asyncOptionLabelField="nameAndCode"
                  urlParams={ { propertyId: currentProperty?.id } }
                  disabled={ isDetail }
                  defaultValue={ currentAnimal?.grandfatherBull?.id }
                />

                <InputSelect
                  label={ t('{this} code', {
                    this: t('great grandmother')
                  }) }
                  options={ getBullsService }
                  name="maternalGreatGrandfatherCode"
                  asyncOptionLabelField="nameAndCode"
                  urlParams={ { propertyId: currentProperty?.id } }
                  disabled={ isDetail }
                  defaultValue={ currentAnimal?.greatGrandfatherBull?.id }
                />
              </>
            )}

            <Item>
              <ButtonsContainer style={ { paddingTop: 10 } }>
                <Button
                  onPress={ handleClose }
                  style={ { flex: 1 } }
                  variant="outlined"
                  title={ t('cancel') }
                />
                <View style={ { width: '10%' } } />
                {isDetail ? (
                  <Button
                    onPress={ () =>
                      dispatch(AnimalActions.setIsDetail(!isDetail))
                    }
                    style={ { flex: 1 } }
                    variant="outlined"
                    title={ t('edit') }
                  />
                ) : (
                  <Button
                    onPress={ () => formRef.current.submit() }
                    style={ { flex: 1 } }
                    title={ t('confirm') }
                  />
                )}
              </ButtonsContainer>
            </Item>
          </FormContainer>
        </Form>
      </Scroll>
    </Container>
  )
})

export default RegisterAnimalForm
