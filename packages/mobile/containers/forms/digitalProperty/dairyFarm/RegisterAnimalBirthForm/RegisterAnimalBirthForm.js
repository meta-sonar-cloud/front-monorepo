import React, { useEffect, useMemo, useCallback, forwardRef } from 'react'
import { useSelector, useDispatch } from 'react-redux'

import moment from 'moment'
import PropTypes from 'prop-types'

import { find, isEmpty, toNumber } from 'lodash'

import registerAnimalBirthSchema from '@smartcoop/forms/schemas/dairyFarm/registerAnimalBirth.schema'
import { useT } from '@smartcoop/i18n'
import Form from '@smartcoop/mobile-components/Form'
import InputDate from '@smartcoop/mobile-components/InputDate'
import InputSelect from '@smartcoop/mobile-components/InputSelect'
import InputText from '@smartcoop/mobile-components/InputText'
import InputUnit from '@smartcoop/mobile-components/InputUnit'
import RadioGroup from '@smartcoop/mobile-components/RadioGroup'
import { selectCurrentAnimal } from '@smartcoop/stores/animal/selectorAnimal'
import { InseminationActions } from '@smartcoop/stores/insemination'
import { selectCurrentInsemination } from '@smartcoop/stores/insemination/selectorInsemination'
import { LotActions } from '@smartcoop/stores/lot'
import { selectLots } from '@smartcoop/stores/lot/selectorLot'
import { momentFriendlyDateFormat } from '@smartcoop/utils/dates'

import { Container, FormContainer, Item } from './styles'

const RegisterAnimalBirthForm = forwardRef((props, formRef) => {
  const { onSubmit, defaultValues } = props

  const t = useT()

  const dispatch = useCallback(useDispatch(), [])

  const lots = useSelector(selectLots)
  const currentAnimal = useSelector(selectCurrentAnimal)
  const currentInsemination = useSelector(selectCurrentInsemination)

  const defaultLot = useMemo(() => find(lots, { code: 'LAC7072' }), [lots])

  const options = useMemo(() => {
    if (defaultLot) return { label: defaultLot?.name, value: defaultLot?.id }
    return {}
  }, [defaultLot])

  const animalId = useMemo(() => {
    if (!isEmpty(currentAnimal)) {
      const { id: currentAnimalId } = currentAnimal
      return currentAnimalId
    }
    return ''
  }, [currentAnimal])

  const defaultOptions = useMemo(
    () => [
      {
        label: 'Assistido',
        value: 'assistido'
      },
      {
        label: 'Normal',
        value: 'normal'
      },
      {
        label: 'Distocico',
        value: 'distocico'
      }
    ],
    []
  )

  const situationOptions = useMemo(
    () => [
      {
        label: 'Vivo',
        value: true
      },
      {
        label: 'Morto',
        value: false
      }
    ],
    []
  )
  const genderOptions = useMemo(
    () => [
      {
        label: 'Macho',
        value: 'masculino'
      },
      {
        label: 'Fêmea',
        value: 'feminino'
      }
    ],
    []
  )

  const gestationDays = 283
  const expectedDate = useMemo(
    () =>
      moment(defaultValues.expectedDate, 'DD/MM/YYYY').isValid()
        ? defaultValues.expectedDate
        : moment(currentInsemination?.inseminationDate, 'YYYY-MM-DD')
          .add(gestationDays, 'days')
          .format(momentFriendlyDateFormat),
    [currentInsemination, defaultValues]
  )

  const handleSubmit = useCallback(
    (data) => {
      onSubmit({
        ...data,
        weight: toNumber(data.weight),
        expectedDate: moment(expectedDate, 'DD/MM/YYYY').format('YYYY-MM-DD'),
        animalId
      })
    },
    [animalId, expectedDate, onSubmit]
  )

  useEffect(() => {
    if (isEmpty(lots)) {
      dispatch(LotActions.loadLots({}))
    }
  }, [dispatch, lots])

  useEffect(() => {
    dispatch(InseminationActions.loadCurrentInsemination())
  }, [dispatch])

  return (
    <Container>
      <Form
        style={ { display: 'flex', flexDirection: 'column', width: '100%' } }
        ref={ formRef }
        schemaConstructor={ registerAnimalBirthSchema }
        onSubmit={ handleSubmit }
      >
        <FormContainer>
          <Item>
            <InputSelect
              label={ t('type') }
              placeholder={ t('type') }
              name="birthType"
              options={ defaultOptions }
              fullWidth
              defaultValue={ defaultValues.birthType }
            />
          </Item>

          <Item style={ { marginBottom: 15 } }>
            <RadioGroup
              label={ t('situation') }
              placeholder={ t('situation') }
              name="isAlive"
              variant="row"
              options={ situationOptions }
              fullWidth
              defaultValue={ defaultValues.isAlive }
            />
          </Item>
          <Item>
            <InputText
              label={ t('expected date') }
              placeholder={ t('expected date') }
              name="expectedDate"
              options={ defaultOptions }
              fullWidth
              disabled
              defaultValue={ expectedDate }
            />
          </Item>
          <Item>
            <InputDate
              label={ t('occurrence date') }
              placeholder={ t('occurrence date') }
              name="occurrenceDate"
              options={ defaultOptions }
              fullWidth
              defaultValue={ defaultValues.occurrenceDate }
            />
          </Item>
          <Item>
            {!isEmpty(options) && (
              <InputSelect
                label={ t('lot') }
                placeholder={ t('lot') }
                name="lotId"
                options={ [options] ?? [] }
                fullWidth
                defaultValue={ defaultLot.id }
                disabled
              />
            )}
          </Item>
          <Item>
            <InputUnit
              label={ t('weight') }
              placeholder={ t('weight') }
              name="weight"
              options={ defaultOptions }
              fullWidth
              defaultValue={ defaultValues.weight }
              unit="Kg"
            />
          </Item>
          <Item>
            <RadioGroup
              label={ t('gender') }
              placeholder={ t('gender') }
              name="gender"
              options={ genderOptions }
              fullWidth
              variant="row"
              defaultValue={ defaultValues.gender }
            />
          </Item>
        </FormContainer>
      </Form>
    </Container>
  )
})

RegisterAnimalBirthForm.propTypes = {
  onSubmit: PropTypes.func,
  defaultValues: PropTypes.object.isRequired
}

RegisterAnimalBirthForm.defaultProps = {
  onSubmit: () => {}
}

export default RegisterAnimalBirthForm
