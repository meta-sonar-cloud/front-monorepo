import React, { useCallback, forwardRef } from 'react'

import moment from 'moment'
import PropTypes from 'prop-types'

import toString from 'lodash/toString'

import registerPriceDataSchema from '@smartcoop/forms/schemas/dairyFarm/registerPriceData.schema'
import I18n, { useT } from '@smartcoop/i18n'
import Button from '@smartcoop/mobile-components/Button'
import Form from '@smartcoop/mobile-components/Form'
import InputMonthYear from '@smartcoop/mobile-components/InputMonthYear'
import InputText from '@smartcoop/mobile-components/InputText'
import InputUnit from '@smartcoop/mobile-components/InputUnit'

import {
  Container,
  ButtonsContainer,
  Label
} from './styles'

const RegisterPriceDataForm = forwardRef((props, formRef) => {
  const {
    withoutSubmitButton,
    defaultValues,
    onSubmit,
    onCancel
  } = props

  const t = useT()

  const handleSubmit = useCallback(
    (data) => onSubmit({
      ...data,
      price: toString(data.price).replace(',', '.')
    }),
    [onSubmit]
  )
  return (
    <Container>
      <Form
        style={ { display: 'flex', flexDirection: 'column', width: '100%' } }
        ref={ formRef }
        schemaConstructor={ registerPriceDataSchema }
        onSubmit={ handleSubmit }
      >
        <InputMonthYear
          label={ `${ t('month', { howMany: 1 }) }/${ t('year', { howMany: 1 }) }` }
          name="priceDate"
          defaultValue={ defaultValues.priceDate }
          pickerProps={
            {
              maxDate: moment().format(),
              title: t('select one {this}', { gender: 'male', this: t('month', { howMany: 1 }) })
            }
          }
          fullWidth
        />
        <InputUnit
          name="price"
          label={ t('price', { howMany: 1 }) }
          unit="R$"
          type="float"
          fullWidth
          defaultValue={ defaultValues.price }
        />
        <InputText
          name="companyName"
          label={ t('third company', { howMany: 1 }) }
          fullWidth
          disabled
          detached
          value={ defaultValues.companyName }
        />
        <Label><I18n>to change the commercialization company name, return to the inital panel</I18n></Label>

        {!withoutSubmitButton && (
          <ButtonsContainer>
            <Button
              onPress={ onCancel }
              style={ { flex: 1 } }
              variant="outlined"
              title={ t('cancel') }
            />

            <Button
              onPress={ () => formRef.current.submit() }
              style={ { flex: 1 } }
              title={ t('save') }
            />
          </ButtonsContainer>
        )}
      </Form>
    </Container>
  )
})

RegisterPriceDataForm.propTypes = {
  onSubmit: PropTypes.func,
  onCancel: PropTypes.func,
  withoutSubmitButton: PropTypes.bool,
  defaultValues: PropTypes.object.isRequired
}

RegisterPriceDataForm.defaultProps = {
  onSubmit: () => {},
  onCancel: () => {},
  withoutSubmitButton: false
}

export default RegisterPriceDataForm
