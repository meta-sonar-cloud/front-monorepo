import React, { useMemo, useCallback, forwardRef } from 'react'

import PropTypes from 'prop-types'

import filterMilkDeliverySchema from '@smartcoop/forms/schemas/dairyFarm/filterMilkDelivery.schema'
import { useT } from '@smartcoop/i18n'
import Button from '@smartcoop/mobile-components/Button'
import Form from '@smartcoop/mobile-components/Form'
import InputDate from '@smartcoop/mobile-components/InputDate'
import InputSelect from '@smartcoop/mobile-components/InputSelect'

import { Container } from './styles'

const FilterFieldsMilkDeliveryForm = forwardRef((props, formRef) => {
  const { withoutSubmitButton, onSubmit, filters } = props

  const t = useT()

  const handleSubmit = useCallback(
    (data) => onSubmit(data),
    [onSubmit]
  )

  const statusOptions = useMemo(
    () => [
      {
        label: t('manual'),
        value: 'manual'
      },
      {
        label: t('definitive'),
        value: 'definitive'
      },
      {
        label: t('depending on approval'),
        value: 'depending-on-approval'
      }
    ],
    [t]
  )

  return (
    <Form
      ref={ formRef }
      schemaConstructor={ filterMilkDeliverySchema }
      onSubmit={ handleSubmit }
    >
      <Container>
        <InputDate
          label="Data Volume"
          name="volumeDate"
          fullWidth
          defaultValue={ filters.volumeDate }
        />
        <InputSelect
          label="Status"
          name="status"
          options={ statusOptions }
          variant="row"
          style={ { paddingBottom: 10 } }
          defaultValue={ filters.status }
          clearable
        />
        {!withoutSubmitButton && (
          <Button
            title={ t('login') }
            onPress={ () => formRef.current.submit() }
          />
        )}
      </Container>
    </Form>
  )
})

FilterFieldsMilkDeliveryForm.propTypes = {
  onSubmit: PropTypes.func,
  withoutSubmitButton: PropTypes.bool,
  filters: PropTypes.object
}

FilterFieldsMilkDeliveryForm.defaultProps = {
  filters: {
    cropId: '',
    cultivationGoalId: '',
    sowingYear: '',
    closed: false
  },
  onSubmit: () => {},
  withoutSubmitButton: false
}

export default FilterFieldsMilkDeliveryForm
