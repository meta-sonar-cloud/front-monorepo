import styled from 'styled-components/native'

export const ItemContainer = styled.View`
  display: flex;
  flex-direction: column;
`

export const HalfGroup = styled.View`
  display: flex;
  flex-direction: row;
  padding-bottom: 10px;
  justify-content: space-between;
`

export const HalfItem = styled.View`
  width: 48%;
`

export const ButtonsContainer = styled.View`
  display: flex;
  flex: 1 1 auto;
  flex-direction: row;
  justify-content: space-around;
  width: 100%;
  padding-bottom: 20px;
`

export const ButtonContainer = styled.View`
  width: 80px;
`
