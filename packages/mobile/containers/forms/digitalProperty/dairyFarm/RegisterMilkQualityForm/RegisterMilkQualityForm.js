import React, { useMemo, forwardRef } from 'react'

import moment from 'moment'
import PropTypes from 'prop-types'

import registerMilkQualitySchema from '@smartcoop/forms/schemas/dairyFarm/registerMilkQuality.schema'
import I18n, { useT } from '@smartcoop/i18n'
import Button from '@smartcoop/mobile-components/Button'
import Form from '@smartcoop/mobile-components/Form'
import InputDate from '@smartcoop/mobile-components/InputDate'
import InputText from '@smartcoop/mobile-components/InputText'
import InputUnit from '@smartcoop/mobile-components/InputUnit'
import { colors } from '@smartcoop/styles'

import {
  ButtonsContainer,
  ButtonContainer,
  ItemContainer,
  HalfGroup,
  HalfItem
} from './styles'

const RegisterMilkQualityForm = forwardRef((props, formRef) => {
  const {
    withoutSubmitButton,
    defaultValues,
    loading,
    onSubmit,
    onCancel
  } = props

  const t = useT()

  const options = useMemo(
    () => ([
      {
        label: 'Proteína',
        value: 'protein'
      },
      {
        label: 'Gordura',
        value: 'fat'
      },
      {
        label: 'CCS',
        value: 'ccs'
      },
      {
        label: 'CTB',
        value: 'ctb'
      }
    ]
    ), []
  )


  return (
    <Form
      style={ { display: 'flex', flexDirection: 'column', width: '100%' } }
      ref={ formRef }
      schemaConstructor={ registerMilkQualitySchema }
      onSubmit={ onSubmit }
    >
      <InputDate
        label={ t('date', { howMany: 1 }) }
        name="date"
        fullWidth
        defaultValue={ defaultValues.date }
        pickerProps={
          {
            maxDate: moment().format(),
            title: t('select one date')
          }
        }
      />
      <ItemContainer>
        <HalfGroup>
          <HalfItem>
            <InputText
              label="Tipo"
              name='type-protein'
              options={ options }
              clearable="true"
              value={ t('protein') }
              disabled
              detached
            />
          </HalfItem>
          <HalfItem>
            <InputUnit
              name="protein"
              placeholder='Resultado'
              unit="%"
              type="float"
              fullWidth
              defaultValue={ defaultValues.protein }
            />
          </HalfItem>
        </HalfGroup>
        <HalfGroup>
          <HalfItem>
            <InputText
              label="Tipo"
              name='type-fat'
              options={ options }
              clearable="true"
              value={ t('fat') }
              disabled
              detached
            />
          </HalfItem>
          <HalfItem>
            <InputUnit
              name="fat"
              placeholder='Resultado'
              unit="%"
              type="float"
              fullWidth
              defaultValue={ defaultValues.fat }
            />
          </HalfItem>
        </HalfGroup>
        <HalfGroup>
          <HalfItem>
            <InputText
              label="Tipo"
              name='type-ccs'
              options={ options }
              clearable="true"
              value={ t('ccs') }
              disabled
              detached
            />
          </HalfItem>
          <HalfItem>
            <InputUnit
              name="ccs"
              placeholder='Resultado'
              unit="células/ml"
              type="integer"
              fullWidth
              defaultValue={ defaultValues.ccs }
            />
          </HalfItem>
        </HalfGroup>
        <HalfGroup>
          <HalfItem>
            <InputText
              label="Tipo"
              name='type-ctb'
              options={ options }
              clearable="true"
              value={ t('ctb') }
              disabled
              detached
            />
          </HalfItem>
          <HalfItem>
            <InputUnit
              name="ctb"
              placeholder='Resultado'
              unit="UFC/ml"
              type="integer"
              defaultValue={ defaultValues.ctb }
              fullWidth
            />
          </HalfItem>
        </HalfGroup>
      </ItemContainer>

      {!withoutSubmitButton && (
        <ButtonsContainer>
          <ButtonContainer>
            <Button
              id="mobile-cancel-form-button"
              onClick={ onCancel }
              color={ colors.white }
              disabled={ loading }
            >
              <I18n>cancel</I18n>
            </Button>
          </ButtonContainer>
          <ButtonContainer>
            <Button
              id="mobile-save-form-button"
              onClick={ () => formRef.current.submit() }
              disabled={ loading }
            >
              <I18n>save</I18n>
            </Button>
          </ButtonContainer>
        </ButtonsContainer>
      )}
    </Form>
  )
})

RegisterMilkQualityForm.propTypes = {
  loading: PropTypes.bool,
  onSubmit: PropTypes.func,
  onCancel: PropTypes.func,
  withoutSubmitButton: PropTypes.bool,
  defaultValues: PropTypes.object.isRequired
}

RegisterMilkQualityForm.defaultProps = {
  loading: false,
  onSubmit: () => {},
  onCancel: () => {},
  withoutSubmitButton: false
}

export default RegisterMilkQualityForm
