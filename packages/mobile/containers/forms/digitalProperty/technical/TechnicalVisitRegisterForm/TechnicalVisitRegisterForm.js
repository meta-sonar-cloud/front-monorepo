import React, { useCallback, forwardRef, useMemo, useState, useEffect } from 'react'
import { Text, View } from 'react-native'
import { useDispatch } from 'react-redux'

import Geolocation from '@react-native-community/geolocation'
import moment from 'moment/moment'
import PropTypes from 'prop-types'

import omitBy from 'lodash/omitBy'

import { useDialog } from '@smartcoop/dialog'
import TechnicalVisit from '@smartcoop/forms/schemas/property/technicalVisit.schema'
import I18n, { useT } from '@smartcoop/i18n'
import Button from '@smartcoop/mobile-components/Button'
import Form from '@smartcoop/mobile-components/Form'
import InputDate from '@smartcoop/mobile-components/InputDate'
import InputHour from '@smartcoop/mobile-components/InputHour'
import InputNumber from '@smartcoop/mobile-components/InputNumber'
import InputSelect from '@smartcoop/mobile-components/InputSelect'
import InputText from '@smartcoop/mobile-components/InputText'
import ConfirmModal from '@smartcoop/mobile-components/Modal/ConfirmModal'
import { TechnicalActions } from '@smartcoop/stores/technical'
import { momentBackDateFormat } from '@smartcoop/utils/dates'

import { Container, TextLabel, ButtonGroup, ContainerButtons } from './styles'

const TechnicalVisitRegisterForm = forwardRef((props, formRef) => {
  const { withoutSubmitButton, cultivateName, fieldName, technicalVisit, onSuccess, handleClose, style } = props
  const t = useT()
  const dispatch = useCallback(useDispatch(), [])
  const { createDialog } = useDialog()

  const [region, setRegion] = useState({})

  const onChangeRegion = useCallback(
    (newRegion) => {
      setRegion(old => {
        if (
          newRegion.latitude !== old.latitude
          && newRegion.longitude !== old.longitude
        ) {
          return { ...old, ...newRegion }
        }
        return old
      })
    },
    []
  )

  const dateAndTime = useMemo(
    () => ({
      'hour': moment(`${ technicalVisit.visitDateTime }`, 'YYYY-MM-DD HH:mm').format('HH:mm'),
      'date': moment(`${ technicalVisit.visitDateTime }`, 'YYYY-MM-DD HH:mm').format('YYYY-MM-DD')
    }),
    [technicalVisit.visitDateTime]
  )

  const currentLocationTechnical = useMemo(
    () => region.latitude && region.longitude ?
      [ region.latitude, region.longitude ] : []
    ,
    [region.latitude, region.longitude]
  )

  const handleSubmitRegisterTechnicalVisit = useCallback(
    (data) => {
      const items = omitBy(data, (item) => item === '')
      if(!items.productivityEstimate || !items.estimateUnity) {
        createDialog({
          id: 'confirm-register-technical-visit',
          Component: ConfirmModal,
          props: {
            onConfirm: () => {
              dispatch(TechnicalActions.saveTechnicalVisit(
                {
                  ...items,
                  id: technicalVisit.id || undefined,
                  fieldId: technicalVisit.fieldId || '',
                  cropId: technicalVisit.cropId,
                  growingSeasonId: technicalVisit.growingSeasonId,
                  visitDateTime: moment(`${ items.dateVisit } ${ items.hour }`, 'YYYY-MM-DD HH:mm').format('YYYY-MM-DD HH:mm'),
                  geoLocation: currentLocationTechnical
                },
                onSuccess()
              ))
            },
            message: t('you have empty fields, do you want to register like this?', {
              howMany: 1,
              gender: 'male',
              this: t('access', { howMany: 1 })
            })
          }
        })
      } else {
        dispatch(TechnicalActions.saveTechnicalVisit(
          {
            ...items,
            id: technicalVisit.id || undefined,
            cropId: technicalVisit.cropId,
            fieldId: technicalVisit.fieldId,
            growingSeasonId: technicalVisit.growingSeasonId,
            visitDateTime: moment(`${ items.dateVisit } ${ items.hour }`, 'YYYY-MM-DD HH:mm').format('YYYY-MM-DD HH:mm'),
            geoLocation: currentLocationTechnical
          },
          onSuccess()
        ))
      }
    },
    [createDialog, currentLocationTechnical, dispatch, onSuccess, t, technicalVisit]
  )

  const unitProductivity = useMemo(
    () => (
      [
        {
          label: 'kg/ha',
          value: 'kg/ha'
        },
        {
          label: 'sacas/ha',
          value: 'sacas/ha'
        }
      ]
    ),
    []
  )

  useEffect(() => {
    Geolocation.getCurrentPosition(
      location => onChangeRegion(location.coords),
      // eslint-disable-next-line no-console
      error => console.error(error),
      { enableHighAccuracy: true }
    )
  }, [onChangeRegion])

  return (
    <Container>
      <Form
        style={ { ...style } }
        ref={ formRef }
        schemaConstructor={ TechnicalVisit }
        onSubmit={ handleSubmitRegisterTechnicalVisit }
      >
        <View style={ { displayDirection: 'row' } }>
          <View style={ { marginRight: 10, flex: 1 } }>
            <InputText
              name='cultivate'
              label='Talhão'
              defaultValue={ fieldName || technicalVisit?.field?.fieldName }
              disabled
              fullWidth
            />
            <InputText
              name='cultivate'
              label='cultivo'
              defaultValue={ cultivateName || technicalVisit.crop?.description }
              disabled
              fullWidth
            />
            <View style={ { flexDirection: 'row' } }>
              <InputDate
                name="dateVisit"
                label="Data"
                defaultValue={ dateAndTime.date || moment().format(momentBackDateFormat) }
                style={ { marginRight: 5, flex: 1 } }
              />
              <InputHour
                name="hour"
                style={ { flex: 1 } }
                label={ t('hour') }
                defaultValue={ dateAndTime ? dateAndTime?.hour : '' }
              />
            </View>
          </View>
          <View style={ { flex: 1 } }>
            <TextLabel>
              <Text style={ { fontWeight: 'bold', fontSize: 16 } }>
                <I18n>productivity estimate</I18n>

              </Text>
            </TextLabel>
            <InputSelect
              label={ t('unit') }
              name="estimateUnity"
              options={ unitProductivity }
              defaultValue={ technicalVisit.estimateUnity }
              fullWidth
            />
            <InputNumber
              maxLength={ 5 }
              label={ t('productivity') }
              name="productivityEstimate"
              defaultValue={ `${ technicalVisit.productivityEstimate }` }
              fullWidth
            />
          </View>
        </View>
        <InputText
          name="observations"
          label={ t('observation', { howMany: 2 }) }
          fullWidth
          multiline
          defaultValue={ technicalVisit.observations }
          rows={ 5 }
        />

        {!withoutSubmitButton && (
          <ContainerButtons>
            <ButtonGroup>
              <Button
                title={ t('cancel') }
                variant='outlined'
                onPress={ handleClose }
                style={ { marginRight: '5%', textTransform: 'none' } }
              />
              <Button
                title={ t('confirm') }
                onPress={ () => formRef.current?.submit() }
              />
            </ButtonGroup>
          </ContainerButtons>
        )}
      </Form>
    </Container>
  )
})

TechnicalVisitRegisterForm.propTypes = {
  withoutSubmitButton: PropTypes.bool,
  style: PropTypes.object,
  cultivateName: PropTypes.string,
  fieldName: PropTypes.string,
  handleClose: PropTypes.func,
  technicalVisit: PropTypes.object,
  onSuccess: PropTypes.func
}

TechnicalVisitRegisterForm.defaultProps = {
  withoutSubmitButton: false,
  style: {},
  cultivateName: '',
  fieldName: '',
  handleClose: () => {},
  onSuccess: () => {},
  technicalVisit: {}
}

export default TechnicalVisitRegisterForm
