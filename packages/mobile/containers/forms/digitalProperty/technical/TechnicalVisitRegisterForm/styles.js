import styled from 'styled-components/native'

export const Container = styled.View`
  justify-content: center;
  align-items: center;
  flex: 1 1 auto;
  flex-direction: column;
  width: 100%;
`

export const ButtonGroup = styled.View`
  flex-direction: row;
  align-items: center;
  justify-content: center;
`

export const TextLabel = styled.View`
  padding-bottom: 15px;
`

export const ContainerButtons = styled.View`
  width: 100%;
  margin-top: 20px;
`
