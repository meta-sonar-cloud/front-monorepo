import React, { useCallback, forwardRef, useState, useMemo, useEffect } from 'react'

import PropTypes from 'prop-types'

import filterTechnicalVisit from '@smartcoop/forms/schemas/property/filterTechnicalVisit.schema'
import { useT } from '@smartcoop/i18n'
import Button from '@smartcoop/mobile-components/Button'
import Form from '@smartcoop/mobile-components/Form'
import InputDateRange from '@smartcoop/mobile-components/InputDateRange'
import InputSelect from '@smartcoop/mobile-components/InputSelect'
import {
  getFieldsByPropertyId as getFieldsByPropertyIdService
} from '@smartcoop/services/apis/smartcoopApi/resources/field'
import {
  getPropertiesByOwner as getPropertiesByOwnerService
} from '@smartcoop/services/apis/smartcoopApi/resources/property'
import {
  getTechnicalVisitUsersList as getTechnicalVisitUsersListService
} from '@smartcoop/services/apis/smartcoopApi/resources/technical'

import { Container } from './styles'

const FilterTechnicalVisitForm = forwardRef((props, formRef) => {
  const { withoutSubmitButton, onSubmit, filters } = props
  const [proprietaryId, setProprietaryId] = useState('')
  const [propertyId, setPropertyId] = useState('')
  const [fieldId, setFieldId] = useState('')
  const t = useT()

  const handleSubmit = useCallback(
    (value) => onSubmit({
      ...value,
      startDate: value.rangeDate.from,
      endDate: value.rangeDate.to,
      rangeDate: undefined
    }),
    [onSubmit]
  )

  const loadFieldOptions = useCallback(
    async (...params) => {
      const result = await getFieldsByPropertyIdService(...params)
      return {
        data: {
          data: result
        }
      }
    },
    []
  )

  const depsProperty = useMemo(
    () => [proprietaryId],
    [proprietaryId]
  )

  const depsField = useMemo(
    () => [propertyId],
    [propertyId]
  )

  const urlParamsProperty = useMemo(
    () => ({ ownerId: proprietaryId }),
    [proprietaryId]
  )

  const urlParamsField = useMemo(
    () => ({ propertyId }),
    [propertyId]
  )

  const propertySelectField = useMemo(
    () => formRef.current?.getFieldRef('propertyId'),
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [formRef, propertyId]
  )

  const fieldSelectField = useMemo(
    () => formRef.current?.getFieldRef('fieldId'),
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [formRef, fieldId]
  )

  useEffect(() => {
    if(!proprietaryId){
      formRef.current.reset()
    }
  }, [formRef, proprietaryId])

  useEffect(() => {
    if(!propertyId && propertySelectField && fieldSelectField){
      propertySelectField.resetField()
      fieldSelectField.resetField()
    }
  }, [fieldSelectField, propertyId, formRef, propertySelectField])

  useEffect(() => {
    if((!fieldId || !propertyId) && fieldSelectField){
      fieldSelectField.resetField()
    }
  }, [fieldId, fieldSelectField, formRef, propertyId])

  useEffect(() => {
    if(filters.proprietary){
      setProprietaryId(filters.proprietary)
    }

    if(filters.propertyId){
      setPropertyId(filters.propertyId)
    }

    if(filters.fieldId){
      setFieldId(filters.fieldId)
    }
  }, [filters])


  return (
    <Form
      ref={ formRef }
      schemaConstructor={ filterTechnicalVisit }
      onSubmit={ handleSubmit }
    >
      <Container>
        <InputSelect
          label={ t('producer', { gender: 'male', howMany: 1 }) }
          name="proprietary"
          options={ getTechnicalVisitUsersListService }
          onChange={ ({ target }) => setProprietaryId(target.value) }
          defaultValue={ filters.proprietary }
          asyncOptionLabelField="name"
        />

        <InputSelect
          label={ t('property', { howMany: 1 }) }
          name="propertyId"
          options={ getPropertiesByOwnerService }
          onChange={ ({ target }) => setPropertyId(target.value) }
          urlParams={ urlParamsProperty }
          asyncOptionLabelField="name"
          defaultValue={ filters.propertyId }
          deps={ depsProperty }
          loaderEnable={ !!proprietaryId }
          disabled={ !proprietaryId }
        />
        <InputSelect
          label={ t('field', { howMany: 1 }) }
          name="fieldId"
          options={ loadFieldOptions }
          urlParams={ urlParamsField }
          asyncOptionLabelField="fieldName"
          onChange={ ({ target }) => setFieldId(target.value) }
          deps={ depsField }
          defaultValue={ filters.fieldId }
          loaderEnable={ !!propertyId }
          disabled={ !propertyId }
        />

        <InputDateRange
          label={ t('period') }
          name="rangeDate"
          defaultValue={ { from: filters.startDate, to: filters.endDate } || { from: '', to: '' } }
        />

        {!withoutSubmitButton && (
          <Button
            title={ t('login') }
            onPress={ () => formRef.current.submit() }
          />
        )}
      </Container>
    </Form>
  )
})

FilterTechnicalVisitForm.propTypes = {
  onSubmit: PropTypes.func,
  withoutSubmitButton: PropTypes.bool,
  filters: PropTypes.object
}

FilterTechnicalVisitForm.defaultProps = {
  filters: {
    cropId: '',
    cultivationGoalId: '',
    sowingYear: '',
    closed: false
  },
  onSubmit: () => {},
  withoutSubmitButton: false
}

export default FilterTechnicalVisitForm
