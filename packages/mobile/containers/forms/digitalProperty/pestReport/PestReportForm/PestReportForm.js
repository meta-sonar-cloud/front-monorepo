import React, { useState, useCallback, forwardRef, useMemo } from 'react'

import PropTypes from 'prop-types'

import pestReportSchema from '@smartcoop/forms/schemas/pestReport/pestReport.schema'
import I18n, { useT } from '@smartcoop/i18n'
import Button from '@smartcoop/mobile-components/Button'
import Form from '@smartcoop/mobile-components/Form'
import InputDate from '@smartcoop/mobile-components/InputDate'
import InputSelect from '@smartcoop/mobile-components/InputSelect'
import InputText from '@smartcoop/mobile-components/InputText'
import Slider from '@smartcoop/mobile-components/Slider'
import { getPestReportTypes, getDiseases, getWeeds, getPests } from '@smartcoop/services/apis/smartcoopApi/resources/pestReport'

import { Container, ButtonContainer } from './styles'

const PestReportForm = forwardRef((props, formRef) => {
  const {
    withoutSubmitButton,
    defaultValues,
    isEditing,
    loading,
    onSubmit
  } = props

  const t = useT()
  const [sliderValue, setSliderValue] = useState(defaultValues.damage || 50)
  const [ocurrenceType, setOcurrenceType] = useState(defaultValues.reportTypes.slug || '')
  const sliderProps = useMemo(
    () => (
      {
        step: 1,
        valueLabelDisplay: 'on',
        onSlidingComplete: (value) => setSliderValue( ...value ),
        marks: [
          {
            value: 0,
            label: '0%'
          },
          {
            value: 100,
            label: '100%'
          }
        ]
      }
    ), []
  )
  const handleSubmit = useCallback(
    (data) => onSubmit({
      ...data,
      damage: sliderValue
    })
    ,
    [onSubmit, sliderValue]
  )

  const handleOcorrenceTypeChange = useCallback(
    (selectedItem) => {
      setOcurrenceType(selectedItem.target.value)
    },
    []
  )

  const specificationService = useMemo(
    () => ({
      'pest': getPests,
      'disease': getDiseases,
      'weed': getWeeds
    }[ocurrenceType]),
    [ocurrenceType]
  )


  const handleOcorrenceType = useMemo(
    () => {
      if(ocurrenceType === 'other') {
        return (
          <InputText
            placeholder={ t('description') }
            name="description"
            fullWidth
            multiline
            rows={ 4 }
            defaultValue={ defaultValues.description }
          />)
      }

      if(ocurrenceType !== 'emergency') {
        return (
          <InputSelect
            key={ ocurrenceType }
            label={ t('specification') }
            name='plagueId'
            options={ specificationService }
            asyncOptionLabelField="name"
            defaultValue={ defaultValues.specification }
          />
        )
      }

      return <></>
    },
    [defaultValues, ocurrenceType, specificationService, t]
  )

  return (
    <Container>
      <Form
        style={ { display: 'flex', flexDirection: 'column', width: '100%' } }
        ref={ formRef }
        schemaConstructor={ pestReportSchema }
        onSubmit={ handleSubmit }
      >
        <InputDate
          label={ t('select one date') }
          name="occurenceDate"
          placeholder={ t('date', { howMany: 1 }) }
          fullWidth
          maxDate={ new Date() }
          disabled={ loading || isEditing }
          defaultValue={ defaultValues.occurenceDate }
        />

        <InputSelect
          label={ t('occurrence type') }
          name="typeSlug"
          options={ getPestReportTypes }
          onChange={ handleOcorrenceTypeChange }
          asyncOptionLabelField="name"
          asyncOptionValueField="slug"
          defaultValue={ defaultValues.reportTypes.slug }
        />

        {handleOcorrenceType}

        <InputText
          placeholder={ t('observations') }
          name="observations"
          fullWidth
          multiline
          rows={ 4 }
          defaultValue={ defaultValues.observations }
        />

        <Slider
          label={ t('damage done') }
          value={ sliderValue }
          sliderProps={ sliderProps }
        />

        {!withoutSubmitButton && (
          <ButtonContainer>
            <Button
              id="mobile-field-form-button"
              style={ { width: '48%' } }
              onClick={ () => formRef.current.submit() }
              disabled={ loading }
            >
              <I18n>next</I18n>
            </Button>
          </ButtonContainer>
        )}
      </Form>
    </Container>
  )
})

PestReportForm.propTypes = {
  loading: PropTypes.bool,
  isEditing: PropTypes.bool,
  onSubmit: PropTypes.func,
  withoutSubmitButton: PropTypes.bool,
  defaultValues: PropTypes.object
}

PestReportForm.defaultProps = {
  isEditing: false,
  loading: false,
  onSubmit: () => {},
  withoutSubmitButton: false,
  defaultValues: {}
}

export default PestReportForm
