import styled from 'styled-components/native'

export const Container = styled.View`
  display: flex;
  flex: 1 1 auto;
  flex-direction: column;
  width: 100%;
  margin-bottom: 20px;
`

export const ButtonContainer = styled.View`
  display: flex;
  flex: 1 1 auto;
  justify-content: center;
  align-items: flex-end;
  flex-direction: column;
  width: 100%;
`
