import React, { useCallback, useEffect, forwardRef, useMemo, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import PropTypes from 'prop-types'

import filter from 'lodash/filter'
import find from 'lodash/find'
import forEach from 'lodash/forEach'
import forOwn from 'lodash/forOwn'
import isArray from 'lodash/isArray'
import isBoolean from 'lodash/isBoolean'
import isEmpty from 'lodash/isEmpty'
import isNull from 'lodash/isNull'
import isObject from 'lodash/isObject'
import isString from 'lodash/isString'
import mapValues from 'lodash/mapValues'
import toString from 'lodash/toString'

import { useT } from '@smartcoop/i18n'
import Button from '@smartcoop/mobile-components/Button'
import DynamicForm from '@smartcoop/mobile-components/DynamicForm'
import useSmartcoopApi from '@smartcoop/services/hooks/useSmartcoopApi'
import { PropertyActions } from '@smartcoop/stores/property'
import { selectOfflineProperty, selectActivities } from '@smartcoop/stores/property/selectorProperty'

import { Container, ButtonContainer } from './styles'

const PropertyActivityDetailsForm = forwardRef((props, formRef) => {
  const { withoutSubmitButton, onSubmit } = props
  const t = useT()

  const dispatch = useCallback(useDispatch(), [])
  const property = useSelector(selectOfflineProperty)
  const activities = useSelector(selectActivities)
  const [loading, setLoading] = useState(false)

  const { data: activitiesDetail } = useSmartcoopApi(!isEmpty(property?.data?.id) ? `/property/${ property?.data?.id }/activities` : '')

  const fields = useMemo(
    () => filter(activities, (activity) => property.activities.indexOf(activity.id) >= 0),
    [activities, property.activities]
  )

  const handleSubmit = useCallback(
    async (data) => {
      dispatch(PropertyActions.updateOfflineProperty({
        activitiesDetails: data
      }))
      onSubmit()
    },
    [dispatch, onSubmit]
  )

  const mapValuesDeep = useCallback(
    (obj, cb) => {
      if (isArray(obj)) {
        return obj.map(innerObj => mapValuesDeep(innerObj, cb))
      } if (isObject(obj)) {
        return mapValues(obj, val => mapValuesDeep(val, cb))
      } if(isString(obj) || isBoolean(obj)) {
        return obj
      }
      return cb(obj)
    },
    []
  )

  useEffect(() => {
    if (!isEmpty(property.activitiesDetails) && loading) {
      const validateProperty = mapValuesDeep(property.activitiesDetails, toString)
      formRef.current.setData(validateProperty)
    }
  }, [formRef, loading, mapValuesDeep, property.activitiesDetails])

  useEffect(() => {
    if (!isEmpty(property.activitiesDetails && formRef.current)) {
      formRef.current.setData(mapValuesDeep(property.activitiesDetails, toString))
    }
  }, [formRef, mapValuesDeep, property])




  useEffect(() => {
    if(activitiesDetail) {
      forEach(fields, item => {
        const activity = find(activitiesDetail?.data, { activityId: item.id })
        forOwn(activity?.details, (value, key) => {
          const fieldActivity = find(item.formFields, { name: key })
          fieldActivity.defaultValue = (fieldActivity.type === 'float') && !isNull(value) ? (value.toFixed(2).toString().replace('.', ',') ?? '') : toString(value)
        })
      })
      setLoading(true)
    }
  }, [activitiesDetail, fields, formRef, property])

  return (
    <Container>
      { (loading || isNull(property.data.id)) && (
        <DynamicForm
          onSubmit={ handleSubmit }
          ref={ formRef }
          fields={ fields }
          withoutSubmitButton
        />
      )}

      {!withoutSubmitButton && (
        <ButtonContainer>
          <Button
            title={ t('next') }
            style={ { width: '48%' } }
            onPress={ () => formRef.current.submit() }
          />
        </ButtonContainer>
      )}
    </Container>
  )
})

PropertyActivityDetailsForm.propTypes = {
  onSubmit: PropTypes.func,
  withoutSubmitButton: PropTypes.bool
}

PropertyActivityDetailsForm.defaultProps = {
  onSubmit: () => {},
  withoutSubmitButton: false
}

export default PropertyActivityDetailsForm
