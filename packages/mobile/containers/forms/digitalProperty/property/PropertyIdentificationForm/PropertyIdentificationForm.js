import React, { useCallback, forwardRef, useState, useEffect, useMemo } from 'react'
import { Platform, PermissionsAndroid, KeyboardAvoidingView, Keyboard, TouchableWithoutFeedback } from 'react-native'
import { useDispatch, useSelector } from 'react-redux'

import Geolocation from '@react-native-community/geolocation'
import PropTypes from 'prop-types'

import map from 'lodash/map'

import propertyIdentificationSchema from '@smartcoop/forms/schemas/property/propertyIdentification.schema'
import I18n, { useT } from '@smartcoop/i18n'
import Button from '@smartcoop/mobile-components/Button'
import Form from '@smartcoop/mobile-components/Form'
import InputSelect from '@smartcoop/mobile-components/InputSelect'
import InputText from '@smartcoop/mobile-components/InputText'
import InputUnit from '@smartcoop/mobile-components/InputUnit'
import { FormLabel } from '@smartcoop/mobile-containers/layouts/AuthenticatedLayout/theme'
import { searchStates, searchCities } from '@smartcoop/services/apis/ibgeApi'
import { PropertyActions } from '@smartcoop/stores/property'
import { selectOfflineProperty } from '@smartcoop/stores/property/selectorProperty'
import { selectUser } from '@smartcoop/stores/user/selectorUser'


import { Container, ButtonContainer } from './styles'

const PropertyIdentificationForm = forwardRef((props, formRef) => {
  // eslint-disable-next-line react/prop-types
  const { withoutSubmitButton, onSubmit, style, isEditing } = props

  const [stateOptions, setStateOptions] = useState([])
  const [cityOptions, setCityOptions] = useState([])


  const user = useSelector(selectUser)
  const property = useSelector(selectOfflineProperty)

  const propertyEditing = useMemo(
    () => {
      if(isEditing){
        return property
      }
      return {}
    },
    [isEditing, property]
  )

  const dispatch = useCallback(useDispatch(), [])
  const t = useT()

  const requestPermissions = async () => {
    if (Platform.OS === 'android') {
      await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION
      )
    } else {
      Geolocation.requestAuthorization()
    }
  }

  useEffect(() => {
    requestPermissions()
  }, [])

  const handleSubmit = useCallback(
    async (data) => {
      dispatch(PropertyActions.updateOfflineProperty({
        data
      }))
      onSubmit()
    },
    [dispatch, onSubmit]
  )

  const handleStateChange = useCallback(
    async ({ target: { value } }) => {
      if (value) {
        let data = await searchCities(value)
        data = map(data, ({ nome }) => ({ label: nome, value: nome }))
        setCityOptions(data)
      } else {
        setCityOptions([])
        formRef.current.clearField('city')
      }
    },
    [formRef]
  )

  useEffect(() => {
    async function findStates() {
      let data = await searchStates()
      data = map(data, ({ sigla, nome }) => ({ label: nome, value: sigla }))
      setStateOptions(data)
    }
    findStates()
  }, [])

  useEffect(() => {
    if (property.data.state || user.state) {
      handleStateChange({ target: { value: property.data.state || user.state } })
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  return (
    <KeyboardAvoidingView
      behavior={ Platform.OS === 'ios' ? 'padding' : 'height' }
      style={ { flex: 1 } }
    >
      <TouchableWithoutFeedback onPress={ Keyboard.dismiss }>
        <Container>
          <Form
            style={ { ...style } }
            ref={ formRef }
            schemaConstructor={ propertyIdentificationSchema }
            onSubmit={ handleSubmit }
          >
            <FormLabel>
              <I18n>identification</I18n>
            </FormLabel>
            <InputText
              name="name"
              label={ t('property name') }
              defaultValue={ propertyEditing?.data?.name }
              autoFocus
            />

            <InputUnit
              name="totalArea"
              label={ t('total area') }
              type="float"
              unit="ha"
              defaultValue={ (propertyEditing?.data?.totalArea?.toFixed(2) || '').toString() }
            />

            <FormLabel>
              <I18n params={ { howMany: 1 } }>address</I18n>
            </FormLabel>
            <InputSelect
              label={ t('state', { howMany: 1 }) }
              name="state"
              onChange={ handleStateChange }
              options={ stateOptions }
              defaultValue={ propertyEditing?.data?.state || user.state }
            />

            <InputSelect
              label={ t('city', { howMany: 1 }) }
              name="city"
              options={ cityOptions }
              defaultValue={ propertyEditing?.data?.city || user.city }
            />

            <InputText
              name="addressComplement"
              label={ t('complement') }
              helperText={ t('example: line, community, attachment, etc.') }
              defaultValue={ propertyEditing?.data?.addressComplement }
            />

            {!withoutSubmitButton && (
              <ButtonContainer>
                <Button
                  title={ t('next') }
                  onPress={ () => formRef.current.submit() }
                  style={ { width: '48%' } }
                />
              </ButtonContainer>
            )}
          </Form>
        </Container>

      </TouchableWithoutFeedback>
    </KeyboardAvoidingView>
  )
})

PropertyIdentificationForm.propTypes = {
  onSubmit: PropTypes.func,
  withoutSubmitButton: PropTypes.bool,
  style: PropTypes.object
}

PropertyIdentificationForm.defaultProps = {
  onSubmit: () => {},
  withoutSubmitButton: false,
  style: {}
}

export default PropertyIdentificationForm
