import { Paragraph } from 'react-native-paper'

import styled from 'styled-components/native'

import Button from '@smartcoop/mobile-components/Button'
import colors from '@smartcoop/styles/colors'


export const Container = styled.View`
  justify-content: center;
  align-items: center;
  flex: 1 1 auto;
  flex-direction: column;
  width: 100%;
`

export const ButtonContainer = styled.View`
  flex: 1 1 auto;
  justify-content: center;
  align-items: flex-end;
  flex-direction: column;
  width: 100%;
`

export const TitleDrawField = styled(Paragraph)`
  color: ${ colors.black };
  margin-bottom: 5px;
`


export const ButtonDrawField = styled(Button)`
  justify-content: center;
  border-color: ${ colors.lightGrey };
  padding: 0;
`

export const ContainerOpenMap = styled.View`
  flex: 1;
  width: 100%;
  flex-direction: row;
  align-items: center;
  margin-bottom: 30px;
`
