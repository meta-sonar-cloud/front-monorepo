import React, { useState, useCallback, forwardRef, useMemo, useEffect } from 'react'
import { View } from 'react-native'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { useSelector , useDispatch } from 'react-redux'

import { useRoute } from '@react-navigation/native'
import PropTypes from 'prop-types'

import isEmpty from 'lodash/isEmpty'
import map from 'lodash/map'

import fieldSchema from '@smartcoop/forms/schemas/producer/field.schema'
import I18n, { useT } from '@smartcoop/i18n'
import { editField } from '@smartcoop/icons'
import Button from '@smartcoop/mobile-components/Button'
import Form from '@smartcoop/mobile-components/Form'
import Icon from '@smartcoop/mobile-components/Icon'
import InputText from '@smartcoop/mobile-components/InputText'
import InputUnit from '@smartcoop/mobile-components/InputUnit'
import Loader from '@smartcoop/mobile-components/Loader'
import PolygonToSvg from '@smartcoop/mobile-components/PolygonToSvg'
import RadioGroup from '@smartcoop/mobile-components/RadioGroup'
import { FieldActions } from '@smartcoop/stores/field'
import { selectCurrentProperty } from '@smartcoop/stores/property/selectorProperty'

import {
  Container,
  ButtonContainer,
  ContainerOpenMap,
  ButtonDrawField,
  TitleDrawField
} from './styles'

const FieldForm = forwardRef((props, formRef) => {
  const {
    withoutSubmitButton,
    loading,
    handleLoading,
    polygonCoordinates,
    area,
    onSuccess,
    handleGoDrawField
  } = props

  const currentProperty = useSelector(selectCurrentProperty)
  const { params } = useRoute()
  const t = useT()

  const [defaultValue, setDefaultValue] = useState({})
  const dispatch = useCallback(useDispatch(), [])
  const [isLoading, setIsLoading] = useState(false)

  const setLoading = useCallback(
    (value) => {
      setIsLoading(value)
      handleLoading(value)
    },
    [handleLoading]
  )

  const handleSubmit = useCallback(
    (data) => {
      setLoading(true)
      dispatch(FieldActions.saveOfflineField(
        {
          ...defaultValue,
          ...data,
          polygonCoordinates
        },
        (payload) => {
          setLoading(false)
          onSuccess(payload)
        },
        () => {
          setLoading(false)
        }
      ))

      formRef.current.reset()
    },
    [defaultValue, dispatch, formRef, onSuccess, polygonCoordinates, setLoading]
  )

  const selectOptions = useMemo(
    () => (
      [
        {
          label: t('own'),
          value: 'owned'
        },
        {
          label: t('leased'),
          value: 'rented'
        }
      ]
    ),
    [t]
  )

  const irrigationOptions = useMemo(
    () => (
      [
        {
          label: t('yes'),
          value: true
        },
        {
          label: t('no'),
          value: false
        }
      ]
    ),
    [t]
  )

  useEffect(
    () => {
      formRef.current.setData({ area })
    },
    [area, formRef]
  )


  useEffect(
    () => {
      if(!isEmpty(params)) {
        setDefaultValue({
          irrigated: params.field?.irrigated ?? null,
          fieldName: params.field?.fieldName || '',
          fieldMode: params.field?.fieldMode || false,
          id: params.field?.id
        })
      }
    }, [params]
  )

  return (isLoading || loading) ? <Loader /> : (
    <Container>
      <Form
        ref={ formRef }
        schemaConstructor={ fieldSchema }
        onSubmit={ handleSubmit }
      >
        <KeyboardAwareScrollView contentContainerStyle={ { paddingTop: 5 } }>
          <ContainerOpenMap>
            <PolygonToSvg
              shape={ map(polygonCoordinates, ([ lat, lng ]) => ({ lat, lng })) }
              width={ 100 }
              style={ { marginRight: 20 } }
            />
            <View >
              <TitleDrawField><I18n>draw field</I18n></TitleDrawField>
              <ButtonDrawField
                onPress={ handleGoDrawField }
                variant="outlined"
                icon={ <Icon style={ { marginRight: 5 } } icon={ editField } size={ 18 }/> }
                title={ t('edit') }
                textStyle={ { fontWeight: 'normal' } }
              />
            </View>
          </ContainerOpenMap>
          <InputText
            name="property"
            label={ t('property', { howMany: 1 }) }
            value={ currentProperty.name }
            detached
            disabled
          />

          <InputUnit
            name="area"
            label={ t('area') }
            maxLength={ 18 }
            unit="ha"
            type="float"
            defaultValue={ area }
            disabled
          />

          <InputText
            name="fieldName"
            label={ t('field name') }
            disabled={ loading }
            defaultValue={ defaultValue.fieldName }
          />

          <RadioGroup
            label={ t('situation') }
            name="fieldMode"
            options={ selectOptions }
            variant="row"
            style={ { paddingBottom: 10 } }
            defaultValue={ defaultValue.fieldMode }
          />

          <RadioGroup
            label={ t('irrigated?') }
            name="irrigated"
            options={ irrigationOptions }
            variant="row"
            defaultValue={ defaultValue.irrigated }
          />


          {!withoutSubmitButton && (
            <ButtonContainer>
              <Button
                style={ { width: '48%' } }
                title={ t('next') }
                onPress={ () => formRef.current.submit() }
                disabled={ loading }
              />
            </ButtonContainer>
          )}
        </KeyboardAwareScrollView>
      </Form>
    </Container>
  )
})

FieldForm.propTypes = {
  loading: PropTypes.bool,
  onSuccess: PropTypes.func,
  handleLoading: PropTypes.func,
  handleGoDrawField: PropTypes.func,
  withoutSubmitButton: PropTypes.bool,
  polygonCoordinates: PropTypes.arrayOf(PropTypes.array),
  area: PropTypes.string
}

FieldForm.defaultProps = {
  loading: false,
  handleGoDrawField: () => {},
  onSuccess: () => {},
  handleLoading: () => {},
  withoutSubmitButton: false,
  polygonCoordinates: [],
  area: ''
}

export default FieldForm
