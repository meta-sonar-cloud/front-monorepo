import React, { useCallback, forwardRef } from 'react'

import PropTypes from 'prop-types'

import filterFields from '@smartcoop/forms/schemas/property/fields/filterFields.schema'
import { useT } from '@smartcoop/i18n'
import Button from '@smartcoop/mobile-components/Button'
import Form from '@smartcoop/mobile-components/Form'
import InputNumber from '@smartcoop/mobile-components/InputNumber'
import InputSelect from '@smartcoop/mobile-components/InputSelect'
import { getCrops as getCropsService } from '@smartcoop/services/apis/smartcoopApi/resources/crop'
import { getCultivationsGoal as getCultivationsGoalService } from '@smartcoop/services/apis/smartcoopApi/resources/cultivationsGoal'

import { Container } from './styles'

const FilterFieldsForm = forwardRef((props, formRef) => {
  const { withoutSubmitButton, onSubmit, filters } = props

  const t = useT()

  const handleSubmit = useCallback(
    (data) => onSubmit(data),
    [onSubmit]
  )

  return (
    <Form
      ref={ formRef }
      schemaConstructor={ filterFields }
      onSubmit={ handleSubmit }
    >
      <Container>
        <InputSelect
          label={ t('crop', { howMany: 1 }) }
          name="cropId"
          options={ getCropsService }
          defaultValue={ filters.cropId }
          clearable
          asyncOptionLabelField="description"
        />

        <InputSelect
          label={ t('goal') }
          name="cultivationGoalId"
          options={ getCultivationsGoalService }
          defaultValue={ filters.cultivationGoalId }
          asyncOptionLabelField="description"
          clearable
        />

        <InputNumber
          maxLength={ 4 }
          name="sowingYear"
          label={ t('sowing year') }
          value={ filters.sowingYear }
          defaultValue={ filters.sowingYear }
        />

        {!withoutSubmitButton && (
          <Button
            title={ t('login') }
            onPress={ () => formRef.current.submit() }
          />
        )}
      </Container>
    </Form>
  )
})

FilterFieldsForm.propTypes = {
  onSubmit: PropTypes.func,
  withoutSubmitButton: PropTypes.bool,
  filters: PropTypes.object
}

FilterFieldsForm.defaultProps = {
  filters: {
    cropId: '',
    cultivationGoalId: '',
    sowingYear: '',
    closed: false
  },
  onSubmit: () => {},
  withoutSubmitButton: false
}

export default FilterFieldsForm
