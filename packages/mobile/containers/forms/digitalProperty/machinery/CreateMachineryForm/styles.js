import { Subheading } from 'react-native-paper'

import styled from 'styled-components'

export const Container = styled.View`
  justify-content: center;
  align-items: center;
  flex: 1 1 auto;
  flex-direction: column;
  width: 100%;
`

export const ButtonContainer = styled.View`
  flex: 1 1 auto;
  justify-content: center;
  align-items: flex-end;
  flex-direction: column;
  width: 100%;
`

export const ColumnContainer = styled.View`
  justify-content: space-between;
  flex-direction: row;
`

export const Column = styled.View`
  width: 48%;
`

export const Subtitle = styled(Subheading)`
  margin-bottom: 15px;
  font-weight: 500;
`
