import React, { useCallback, forwardRef, useMemo } from 'react'

import PropTypes from 'prop-types'

import machinerySchema from '@smartcoop/forms/schemas/machinery/machineryRegister.schema'
import I18n, { useT } from '@smartcoop/i18n'
import Button from '@smartcoop/mobile-components/Button'
import Form from '@smartcoop/mobile-components/Form'
import InputNumber from '@smartcoop/mobile-components/InputNumber'
import InputPhone from '@smartcoop/mobile-components/InputPhone'
import InputSelect from '@smartcoop/mobile-components/InputSelect'
import InputText from '@smartcoop/mobile-components/InputText'
import RadioGroup from '@smartcoop/mobile-components/RadioGroup'
import {
  getMachinesTypes as getMachinesTypesService,
  getMachinesBrands as getMachinesBrandsService
} from '@smartcoop/services/apis/smartcoopApi/resources/machine'
import { selectGetProperties as getPropertiesService } from '@smartcoop/services/apis/smartcoopApi/resources/property'
import { colors } from '@smartcoop/styles'

import {
  Container,
  Column,
  Subtitle,
  ButtonContainer,
  ColumnContainer
} from './styles'

const CreateMachineryForm = forwardRef((props, formRef) => {
  const { withoutSubmitButton, defaultValues, loading, onSubmit } = props
  const t = useT()

  const handleSubmit = useCallback(
    (data) => {
      onSubmit(data)
    },
    [onSubmit]
  )

  const availableOptions = useMemo(
    () => [
      {
        label: t('available'),
        value: true
      },
      {
        label: t('unavailable'),
        value: false
      }
    ],
    [t]
  )
  return (
    <Container>
      <Form
        style={ { display: 'flex', flexDirection: 'column', width: '100%' } }
        ref={ formRef }
        schemaConstructor={ machinerySchema }
        onSubmit={ handleSubmit }
      >
        <RadioGroup
          label={ t('rental availability') }
          labelStyle={ {
            marginTop: 0,
            fontSize: 16,
            color: colors.text,
            fontWeight: 'bold'
          } }
          name="available"
          options={ availableOptions }
          variant="row"
          style={ { paddingBottom: 20 } }
          itemStyle={ { padding: 20 } }
          defaultValue={ defaultValues.available }
        />

        <InputText
          name="description"
          label={ t('description') }
          defaultValue={ defaultValues.description }
        />

        <InputSelect
          label={ t('property', { howMany: 1 }) }
          name="propertyId"
          options={ getPropertiesService }
          defaultValue={ defaultValues.property.id }
        />

        <InputSelect
          label={ t('type', { howMany: 1 }) }
          name="machineTypeId"
          options={ getMachinesTypesService }
          asyncOptionLabelField="description"
          defaultValue={ defaultValues.machineType.id }
        />

        <InputSelect
          label={ t('brand', { howMany: 1 }) }
          name="machineBrandId"
          options={ getMachinesBrandsService }
          asyncOptionLabelField="description"
          defaultValue={ defaultValues.machineBrand.id }
        />

        <InputText
          name="model"
          label={ t('model') }
          disabled={ loading }
          defaultValue={ defaultValues.model }
        />

        <ColumnContainer>
          <Column>
            <InputNumber
              name="year"
              label={ t('year', { howMany: 1 }) }
              maxLength={ 4 }
              defaultValue={ defaultValues.year }
            />
          </Column>
          <Column>
            <InputNumber
              name="availableHours"
              label={ t('hours') }
              defaultValue={ defaultValues.availableHours }
            />
          </Column>
        </ColumnContainer>

        <I18n as={ Subtitle }>contact informations</I18n>

        <InputPhone
          name="phone"
          label={ t('phone') }
          defaultValue={ defaultValues.phone }
        />

        <InputText
          name="contactInformation"
          label={ t('observations') }
          defaultValue={ defaultValues.contactInformation }
        />

        {!withoutSubmitButton && (
          <ButtonContainer>
            <Button
              id="mobile-machine-form-button"
              style={ { width: '48%' } }
              onClick={ () => formRef.current.submit() }
              disabled={ loading }
            >
              <I18n>next</I18n>
            </Button>
          </ButtonContainer>
        )}
      </Form>
    </Container>
  )
})

CreateMachineryForm.propTypes = {
  loading: PropTypes.bool,
  onSubmit: PropTypes.func,
  withoutSubmitButton: PropTypes.bool,
  defaultValues: PropTypes.object.isRequired
}

CreateMachineryForm.defaultProps = {
  loading: false,
  onSubmit: () => {},
  withoutSubmitButton: false
}

export default CreateMachineryForm
