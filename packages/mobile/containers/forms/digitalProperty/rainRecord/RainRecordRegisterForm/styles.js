import styled from 'styled-components/native'

import { colors } from '@smartcoop/styles'

export const Container = styled.View`
  justify-content: center;
  align-items: center;
  flex: 1 1 auto;
  flex-direction: column;
  width: 100%;
`

export const ButtonContainer = styled.View`
  flex: 1 1 auto;
  justify-content: center;
  align-items: flex-end;
  flex-direction: column;
  width: 100%;
`

export const InfoView = styled.View`
  display: flex;
  flex-direction: row;
  align-items: flex-start;
`

export const InfoText = styled.Text`
  margin-left: 5px;
  color: ${ colors.mediumGrey };
  margin-top: -2px;
`
