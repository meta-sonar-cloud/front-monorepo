import React, { useCallback, forwardRef, useMemo, useState } from 'react'
import { View } from 'react-native'
import { useDispatch, useSelector } from 'react-redux'

import { useNavigation } from '@react-navigation/native'
import PropTypes from 'prop-types'

import isEmpty from 'lodash/isEmpty'
import map from 'lodash/map'
import toNumber from 'lodash/toNumber'
import toString from 'lodash/toString'


import rainRecordPropertyFieldsSchema from '@smartcoop/forms/schemas/property/fields/rainRecordPropertyFields.schema'
import I18n, { useT } from '@smartcoop/i18n'
import { info } from '@smartcoop/icons'
import Button from '@smartcoop/mobile-components/Button'
import CheckboxGroup from '@smartcoop/mobile-components/CheckboxGroup'
import Form from '@smartcoop/mobile-components/Form'
import Icon from '@smartcoop/mobile-components/Icon'
import InputDate from '@smartcoop/mobile-components/InputDate'
import InputSelect from '@smartcoop/mobile-components/InputSelect'
import InputUnit from '@smartcoop/mobile-components/InputUnit'
import { Item, ButtonsContainer, Scroll } from '@smartcoop/mobile-containers/layouts/AuthenticatedLayout/theme'
import { getFieldsByPropertyId as getFieldsByPropertyIdService } from '@smartcoop/services/apis/smartcoopApi/resources/field'
import { FieldsMonitorationActions } from '@smartcoop/stores/fieldsMonitoration'
import { selectCurrentProperty } from '@smartcoop/stores/property/selectorProperty'
import { colors } from '@smartcoop/styles'

import { Container, InfoView, InfoText } from './styles'

const RainRecordRegisterForm = forwardRef((props, formRef) => {
  const { onSuccess, style, rainRecordData } = props
  const dispatch = useCallback(useDispatch(), [])
  const [rainMm, setRainMm] = useState('')
  const [events, setEvents] = useState([])
  const t = useT()

  const currentProperty = useSelector(selectCurrentProperty)

  const navigation = useNavigation()

  const checkboxOptions = useMemo(
    () => [
      { label: 'Geada', value: 'frost' },
      { label: 'Granizo', value: 'hail' }
    ],
    []
  )

  const handleClose = useCallback(
    () => {
      navigation.goBack()
    },
    [navigation]
  )

  const handleSubmit = useCallback(
    (data) => {
      dispatch(FieldsMonitorationActions.saveFieldsMonitoration(
        {
          occurrenceDate: data.occurrenceDate,
          fieldsId: data.fieldsId,
          rainMm: toNumber(data.rainMm),
          frost: data.events.includes('frost'),
          hail: data.events.includes('hail')
        },
        rainRecordData.id,
        () => {
          navigation.navigate('Property', { screen: 'RainRecord' })
          onSuccess(rainRecordData.id)
        }
      ))
    },
    [dispatch, navigation, onSuccess, rainRecordData.id]
  )

  const urlParamsFields = useMemo(
    () => ({ propertyId: currentProperty.id }),
    [currentProperty]
  )

  const loadFieldOptions = useCallback(
    async (...params) => {
      const result = await getFieldsByPropertyIdService(...params)
      return {
        data: {
          data: result
        }
      }
    },
    []
  )

  const defaultValueCheckbox = useMemo(
    () => [ rainRecordData?.frost && 'frost', rainRecordData?.hail && 'hail'],
    [rainRecordData]
  )

  const defaultValueFields = useMemo(
    () => map(rainRecordData.fields, field => field.id),
    [rainRecordData.fields]
  )

  const disabled = useMemo(
    () => isEmpty(rainMm) && isEmpty(events.filter(item => item !== undefined)),
    [events, rainMm]
  )

  return (
    <Container>
      <Scroll padding={ 0 }>
        <Form
          style={ { ...style } }
          ref={ formRef }
          schemaConstructor={ rainRecordPropertyFieldsSchema }
          onSubmit={ handleSubmit }
        >

          <InputDate
            name="occurrenceDate"
            label={ t('register date') }
            maxDate={ new Date() }
            defaultValue={ rainRecordData.occurrenceDate }
          />

          <InputUnit
            style={ { marginBottom: 0 } }
            name="rainMm"
            label={ t('rain quantity') }
            onChange={ (e) => setRainMm(e.target.value) }
            defaultValue={ toString(rainRecordData.rainMm) }
            unit="mm"
            type="integer"
            fullWidth
          />

          <CheckboxGroup
            name="events"
            label=""
            options={ checkboxOptions }
            defaultValue={ defaultValueCheckbox }
            handleChange={ (e) => setEvents(e) }
            variant="row"
            style={ { marginBottom: 0, marginTop: 0 } }
            styleContainer={ { justifyContent: 'space-around' } }
          />

          <InputSelect
            multiple
            label={ t('apply to fields') }
            name="fieldsId"
            options={ loadFieldOptions }
            urlParams={ urlParamsFields }
            style= { { marginBottom: 20 } }
            defaultValue={ defaultValueFields }
            asyncOptionLabelField="fieldName"
            checkBoxSelectAll
          />

          { loadFieldOptions.length === 0 && (
            <InfoView>
              <Icon icon={ info } color={ colors.grey } size={ 16 } />
              <InfoText>
                <I18n>you need to have a registered field to register rain</I18n>
              </InfoText>
            </InfoView>
          ) }

        </Form>

      </Scroll>
      <Item style={ { bottom: 0 } }>
        <ButtonsContainer style={ { paddingTop: 10 } }>
          <Button
            onPress={ handleClose }
            style={ { flex: 1 } }
            variant="outlined"
            title={ t('cancel') }
          />

          <View style={ { width: '10%' } } />

          <Button
            onPress={ () => formRef.current.submit() }
            style={ { flex: 1 } }
            title={ t('save') }
            disabled={ disabled }
          />
        </ButtonsContainer>
      </Item>
    </Container>
  )
})

RainRecordRegisterForm.propTypes = {
  onSuccess: PropTypes.func,
  style: PropTypes.object,
  rainRecordData: PropTypes.object
}

RainRecordRegisterForm.defaultProps = {
  onSuccess: () => {},
  style: {},
  rainRecordData: {}
}

export default RainRecordRegisterForm
