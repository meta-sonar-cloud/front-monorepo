import React, { useCallback, forwardRef, useMemo } from 'react'
import { useSelector } from 'react-redux'

import PropTypes from 'prop-types'

import filterRainRecord from '@smartcoop/forms/schemas/property/filterRainRecord.schema'
import { useT } from '@smartcoop/i18n'
import Button from '@smartcoop/mobile-components/Button'
import Form from '@smartcoop/mobile-components/Form'
import InputDateRange from '@smartcoop/mobile-components/InputDateRange'
import InputSelect from '@smartcoop/mobile-components/InputSelect'
import {
  getFieldsByPropertyId as getFieldsByPropertyIdService
} from '@smartcoop/services/apis/smartcoopApi/resources/field'
import { selectCurrentProperty } from '@smartcoop/stores/property/selectorProperty'

import { Container } from './styles'

const FilterRainRecordForm = forwardRef((props, formRef) => {
  const { withoutSubmitButton, onSubmit, filters } = props
  const currentProperty = useSelector(selectCurrentProperty)
  const t = useT()

  const handleSubmit = useCallback(
    (value) => onSubmit({
      ...value,
      startDate: value.rangeDate.from,
      endDate: value.rangeDate.to,
      rangeDate: undefined
    }),
    [onSubmit]
  )

  const loadFieldOptions = useCallback(
    async (...params) => {
      const result = await getFieldsByPropertyIdService(...params)
      return {
        data: {
          data: result
        }
      }
    },
    []
  )

  const urlParams = useMemo(
    () => ({ propertyId: currentProperty.id }),
    [currentProperty]
  )


  return (
    <Container>
      <Form
        ref={ formRef }
        schemaConstructor={ filterRainRecord }
        onSubmit={ handleSubmit }
      >
        <InputSelect
          label={ t('field', { howMany: 1 }) }
          name="fieldId"
          options={ loadFieldOptions }
          urlParams={ urlParams }
          asyncOptionLabelField="fieldName"
          defaultValue={ filters.fieldId }
        />

        <InputDateRange
          label={ t('period') }
          name="rangeDate"
          defaultValue={ { from: filters.startDate, to: filters.endDate } || { from: '', to: '' } }
        />

        {!withoutSubmitButton && (
          <Button
            title={ t('login') }
            onPress={ () => formRef.current.submit() }
          />
        )}
      </Form>
    </Container>
  )
})

FilterRainRecordForm.propTypes = {
  onSubmit: PropTypes.func,
  withoutSubmitButton: PropTypes.bool,
  filters: PropTypes.object
}

FilterRainRecordForm.defaultProps = {
  filters: {
    cropId: '',
    cultivationGoalId: '',
    sowingYear: '',
    closed: false
  },
  onSubmit: () => {},
  withoutSubmitButton: false
}

export default FilterRainRecordForm
