import React, { forwardRef, useMemo, useState, useCallback } from 'react'
import { useSelector } from 'react-redux'

import isEmpty from 'lodash/isEmpty'

import { useT } from '@smartcoop/i18n'
import InputPercentage from '@smartcoop/mobile-components/InputPercentage'
import InputSelect from '@smartcoop/mobile-components/InputSelect'
import InputUnit from '@smartcoop/mobile-components/InputUnit'
import RadioGroup from '@smartcoop/mobile-components/RadioGroup'
import { getProducts as getProductsService } from '@smartcoop/services/apis/smartcoopApi/resources/product'
import { getProductGroups as getProductGroupsService } from '@smartcoop/services/apis/smartcoopApi/resources/productGroup'
import { selectCurrentField } from '@smartcoop/stores/field/selectorField'

const SeedingForm = forwardRef(() => {
  const t = useT()

  const [productGroupSlug, setProductGroupSlug] = useState('')


  const field = useSelector(selectCurrentField)

  const { growingSeason } = field

  const selectOptions = useMemo(
    () => (
      [
        {
          label: t('yes'),
          value: 'Sim'
        },
        {
          label: t('no'),
          value: 'Não'
        }
      ]
    ),
    [t]
  )

  const handleProductGroup = useCallback(
    (selectedItem) => {
      setProductGroupSlug(selectedItem.target.value)
    },
    []
  )


  const productQueryParams = useMemo(
    () => (!isEmpty(productGroupSlug) ? { productGroupId: productGroupSlug } : {}),
    [productGroupSlug]
  )

  return (
    <>
      <InputSelect
        label={ t('productGroup') }
        name="productGroupSeeding"
        options={ getProductGroupsService }
        queryParams={ { cropId: growingSeason.cropId } }
        onChange={ handleProductGroup }
      />

      <InputSelect
        label={ t('cultivate') }
        name="cultivate"
        options={ getProductsService }
        queryParams={ productQueryParams }
        asyncOptionLabelField="shortDescription"
        asyncOptionValueField="shortDescription"
        disabled={ !productGroupSlug }
      />


      <InputUnit
        maxLength={ 5 }
        label={ t('number of plants per linear meter') }
        name="numberOfPlants"
        decimalScale={ 1 }
        type="float"
        fullWidth
      />

      <InputUnit
        name="lineSpacing"
        label={ t('line spacing') }
        maxLength={ 2 }
        unit="cm"
        type="integer"
        fullWidth
      />

      <RadioGroup
        style={ { marginBottom: 10 } }
        label={ t('replanting?') }
        name="replanting"
        options={ selectOptions }
        variant="row"
      />

      <InputPercentage
        name="germination"
        label={ t('germination') }
        maxLength={ 3 }
        maxValue={ 100 }
        type="integer"
        fullWidth
      />

      <InputPercentage
        name="vigor"
        label={ t('vigor') }
        maxLength={ 3 }
        maxValue={ 100 }
        type="integer"
        fullWidth
      />
    </>
  )
})

export default SeedingForm
