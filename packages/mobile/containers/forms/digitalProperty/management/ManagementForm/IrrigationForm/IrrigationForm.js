import React, { forwardRef, useMemo } from 'react'

import { useT } from '@smartcoop/i18n'
import InputUnit from '@smartcoop/mobile-components/InputUnit'
import RadioGroup from '@smartcoop/mobile-components/RadioGroup'

const IrrigationForm = forwardRef(() => {
  const t = useT()

  const typesOfIrrigation = useMemo(
    () => (
      [
        {
          label: t('superficial'),
          value: 'Superficial'
        },
        {
          label: t('sprinkler'),
          value: 'Aspersor'
        },
        {
          label: t('center pivot'),
          value: 'Pivô central'
        }
      ]
    ),
    [t]
  )

  return (
    <>
      <InputUnit
        name="irrigationPlates"
        label={ t('irrigation plates') }
        maxLength={ 3 }
        maxValue={ 999 }
        unit="mm"
        type="integer"
        fullWidth
      />

      <RadioGroup
        style={ { marginBottom: 10 } }
        label={ t('type') }
        name="typeOfIrrigation"
        options={ typesOfIrrigation }
        variant="row"
      />
    </>
  )
})

export default IrrigationForm
