import React, { forwardRef } from 'react'

import { useT } from '@smartcoop/i18n'
import InputUnit from '@smartcoop/mobile-components/InputUnit'

const SilageProductionForm = forwardRef(() => {
  const t = useT()

  return (
    <>
      <InputUnit
        name="productionSilage"
        label={ t('production') }
        maxLength={ 4 }
        decimalScale={ 1 }
        unit="t/ha"
        type="float"
      />
    </>
  )
})

export default SilageProductionForm
