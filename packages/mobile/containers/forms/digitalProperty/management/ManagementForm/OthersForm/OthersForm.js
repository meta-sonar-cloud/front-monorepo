import React, { forwardRef } from 'react'

import { useT } from '@smartcoop/i18n'
import InputText from '@smartcoop/mobile-components/InputText'

const OthersForm = forwardRef(() => {
  const t = useT()

  return (
    <>
      <InputText
        name="description"
        label={ t('description') }
        fullWidth
        multiline
        rows={ 10 }
      />
    </>
  )
})

export default OthersForm
