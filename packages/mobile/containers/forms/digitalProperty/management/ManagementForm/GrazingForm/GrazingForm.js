import React, { forwardRef } from 'react'

import { useT } from '@smartcoop/i18n'
import InputNumber from '@smartcoop/mobile-components/InputNumber'
import InputUnit from '@smartcoop/mobile-components/InputUnit'

const GrazingForm = forwardRef(() => {
  const t = useT()

  return (
    <>
      <InputNumber
        name="numberOfAnimals"
        label={ t('number of animals') }
        maxLength={ 3 }
        maxValue={ 999 }
        type="integer"
        fullWidth
      />

      <InputUnit
        name="grazingTimePeriod"
        label={ t('grazing time period') }
        maxLength={ 2 }
        maxValue={ 99 }
        unit="dia(s)"
        type="integer"
        fullWidth
      />
    </>
  )
})

export default GrazingForm
