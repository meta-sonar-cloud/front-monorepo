import React, { useCallback, forwardRef, useState, useMemo, useEffect } from 'react'
import { useDispatch } from 'react-redux'

import PropTypes from 'prop-types'

import omitBy from 'lodash/omitBy'

import managementSchemaRegister from '@smartcoop/forms/schemas/management/managementRegister.schema'
import I18n, { useT } from '@smartcoop/i18n'
import Button from '@smartcoop/mobile-components/Button'
import Form from '@smartcoop/mobile-components/Form'
import InputDate from '@smartcoop/mobile-components/InputDate'
import InputSelect from '@smartcoop/mobile-components/InputSelect'
import InputText from '@smartcoop/mobile-components/InputText'
import FertilizationForm from '@smartcoop/mobile-containers/forms/digitalProperty/management/ManagementForm/FertilizationForm'
import GrazingForm from '@smartcoop/mobile-containers/forms/digitalProperty/management/ManagementForm/GrazingForm'
import HarvestForm from '@smartcoop/mobile-containers/forms/digitalProperty/management/ManagementForm/HarvestForm'
import HayProductionForm from '@smartcoop/mobile-containers/forms/digitalProperty/management/ManagementForm/HayProductionForm'
import IrrigationForm from '@smartcoop/mobile-containers/forms/digitalProperty/management/ManagementForm/IrrigationForm'
import OthersForm from '@smartcoop/mobile-containers/forms/digitalProperty/management/ManagementForm/OthersForm'
import PhytosanitaryApplicationForm from '@smartcoop/mobile-containers/forms/digitalProperty/management/ManagementForm/PhytosanitaryApplicationForm/PhytosanitaryApplicationForm'
import SeedingForm from '@smartcoop/mobile-containers/forms/digitalProperty/management/ManagementForm/SeedingForm'
import SilageProductionForm from '@smartcoop/mobile-containers/forms/digitalProperty/management/ManagementForm/SilageProductionForm'
import { getOperations, getOperation } from '@smartcoop/services/apis/smartcoopApi/resources/operations'
import { FieldActions } from '@smartcoop/stores/field'

import { Container, ButtonContainer } from './styles'

const ManagementForm = forwardRef((props, formRef) => {
  const { withoutSubmitButton, onSuccess, style } = props
  const dispatch = useCallback(useDispatch(), [])
  const t = useT()
  const [operationId, setOperationId] = useState('')
  const [currentOperation, setcurrentOperation] = useState({})

  const handleSubmit = useCallback(
    (data) => {
      const keysDontHaveInItems = ['observations', 'operationId', 'predictedDate']
      const items = omitBy(data, (item, key) => keysDontHaveInItems.includes(key))
      dispatch(FieldActions.saveOfflineCropManagement(
        {
          operationSlug: currentOperation.slug,
          predictedDate: data.predictedDate,
          observations: data.observations,
          items
        }
      ))
      onSuccess()
    },
    [currentOperation, dispatch, onSuccess]
  )

  const handleOperationChange = useCallback(
    (selectedItem) => {
      setOperationId(selectedItem.target.value)
    },
    []
  )

  useEffect(() => {
    (async () => {
      setcurrentOperation(await getOperation(operationId))
    })()
  }, [operationId])


  const operationSelected = useMemo(
    () => ({
      'Semeadura': <SeedingForm />,
      'Fertilização': <FertilizationForm />,
      'Aplicação fitossanitária': <PhytosanitaryApplicationForm />,
      'Colheita': <HarvestForm />,
      'Irrigação': <IrrigationForm />,
      'Produção de feno': <HayProductionForm />,
      'Pastejo': <GrazingForm />,
      'Outros': <OthersForm />,
      'Produção de silagem':<SilageProductionForm />
    }[currentOperation.name]),
    [currentOperation.name]
  )

  return (
    <Container>
      <Form
        style={ { ...style } }
        ref={ formRef }
        schemaConstructor={ managementSchemaRegister }
        onSubmit={ handleSubmit }
      >

        <InputDate
          name="predictedDate"
          label={ t('select one date') }
          fullWidth
        />

        <InputSelect
          label={ t('operation') }
          name="operationId"
          options={ getOperations }
          onChange={ handleOperationChange }
          asyncOptionLabelField="name"
        />

        {operationSelected}

        <InputText
          name="observations"
          label={ t('observation', { howMany: 2 }) }
          fullWidth
          multiline
          rows={ 5 }
        />

        {!withoutSubmitButton && (
          <ButtonContainer>
            <Button
              id="web-property-identification-form-button"
              style={ { width: '48%' } }
              onClick={ () => formRef.current.submit() }
            >
              <I18n>next</I18n>
            </Button>
          </ButtonContainer>
        )}
      </Form>
    </Container>
  )
})

ManagementForm.propTypes = {
  onSuccess: PropTypes.func,
  withoutSubmitButton: PropTypes.bool,
  style: PropTypes.object
}

ManagementForm.defaultProps = {
  onSuccess: () => {},
  withoutSubmitButton: false,
  style: {}
}

export default ManagementForm
