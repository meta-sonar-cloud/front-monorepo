import React, { forwardRef, useMemo, useState, useCallback } from 'react'
import { useSelector } from 'react-redux'

import isEmpty from 'lodash/isEmpty'
import lowerCase from 'lodash/lowerCase'

import { useT } from '@smartcoop/i18n'
import InputNumber from '@smartcoop/mobile-components/InputNumber'
import InputPercentage from '@smartcoop/mobile-components/InputPercentage'
import InputSelect from '@smartcoop/mobile-components/InputSelect'
import InputUnit from '@smartcoop/mobile-components/InputUnit'
import { selectCurrentField } from '@smartcoop/stores/field/selectorField'

const HarvestForm = forwardRef(() => {
  const t = useT()
  const [unit, setUnit] = useState('kg/ha')

  const currentField = useSelector(selectCurrentField)

  const { crop } = currentField.growingSeason

  const handleUnitsChange = useCallback(
    (selectedItem) => {
      setUnit(selectedItem.target.value)
    },
    []
  )

  const unitProductivity = useMemo(
    () => (
      [
        {
          label: t('kg/ha'),
          value: 'kg/ha'
        },
        {
          label: t('bags/ha'),
          value: 'sacas/ha'
        }
      ]
    ),
    [t]
  )

  const inputProductivity = useMemo(
    () => {
      const inputTypeProductivity = {
        'kg/ha':
          <InputNumber
            maxLength={ 5 }
            label={ t('productivity') }
            name="productivityKg"
            fullWidth
          />,
        'sacas/ha':
          <InputUnit
            maxLength={ 5 }
            label={ t('productivity') }
            name="productivityBags"
            decimalScale={ 1 }
            type="float"
            fullWidth
          />
      }
      return inputTypeProductivity[unit]

    },
    [t, unit]
  )

  const complementaryForm = useMemo(
    () => {
      const cropSelectForm = {
        'trigo':
          <InputPercentage
            name="ph"
            label={ t('ph') }
            maxLength={ 2 }
            maxValue={ 99 }
            type="integer"
            fullWidth
          />,
        'cevada':
          <>
            <InputPercentage
              name="ph"
              label={ t('ph') }
              maxLength={ 2 }
              maxValue={ 99 }
              type="integer"
              fullWidth
            />
            <InputPercentage
              name="germinationProductivity"
              label={ t('germination') }
              type="integer"
              fullWidth
            />
          </>,
        'aveia':
          <InputPercentage
            name="ph"
            label={ t('ph') }
            maxLength={ 2 }
            maxValue={ 99 }
            type="integer"
            fullWidth
          />,
        'arroz':
          <>
            <InputPercentage
              name="germinationProductivity"
              label={ t('germination') }
              type="integer"
              fullWidth
            />
            <InputPercentage
              name="wholeGrain"
              label={ t('whole grain') }
              type="integer"
              fullWidth
            />
            <InputPercentage
              name="shellAndBran"
              label={ t('shell and bran') }
              type="integer"
              fullWidth
            />
          </>
      }

      return cropSelectForm[lowerCase(crop.description)]
    },
    [crop.description, t]
  )

  return (
    <>
      <InputSelect
        label={ t('unit') }
        name="unitProductivity"
        options={ unitProductivity }
        onChange={ handleUnitsChange }
        defaultValue="kg/ha"
      />

      { !isEmpty(unit) && (
        inputProductivity
      )}

      { !isEmpty(crop.description) && (
        complementaryForm
      )}

    </>
  )
})

export default HarvestForm
