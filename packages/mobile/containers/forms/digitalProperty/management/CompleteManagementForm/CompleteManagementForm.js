import React, { useCallback, forwardRef, useState, useMemo, useEffect } from 'react'
import { Text, View } from 'react-native'
import { useDispatch } from 'react-redux'

import moment from 'moment/moment'
import PropTypes from 'prop-types'

import { useDialog } from '@smartcoop/dialog'
import completeSchemaManagement from '@smartcoop/forms/schemas/management/completeManagement.schema'
import I18n, { useT } from '@smartcoop/i18n'
import { arrowLeftSimple, arrowRightSimple } from '@smartcoop/icons'
import Button from '@smartcoop/mobile-components/Button'
import DatePicker from '@smartcoop/mobile-components/DatePicker'
import Form from '@smartcoop/mobile-components/Form'
import Icon from '@smartcoop/mobile-components/Icon'
import ConfirmModal from '@smartcoop/mobile-components/Modal/ConfirmModal'
import RadioGroup from '@smartcoop/mobile-components/RadioGroup'
import { useSnackbar } from '@smartcoop/snackbar'
import { FieldActions } from '@smartcoop/stores/field'
import colors from '@smartcoop/styles/colors'
import { momentBackDateFormat } from '@smartcoop/utils/dates'

import { Container, ContentRadioGroup, ButtonGroup, ContainerButtons } from './styles'

const CompleteManagementForm = forwardRef((props, formRef) => {
  const { withoutSubmitButton, currentCropManagement, canConcludeGrowingSeason, onSuccess, handleClose, style } = props
  const dispatch = useCallback(useDispatch(), [])
  const [valueDate, setValueDate] = useState(moment().isAfter(moment(currentCropManagement.predictedDate)) ? currentCropManagement.predictedDate : '')
  const t = useT()
  const { createDialog } = useDialog()
  const snackbar = useSnackbar()

  const [isRadioChecked, setIsRadioChecked] = useState(false)

  const isDisabled = useMemo(
    () => (!valueDate || (!canConcludeGrowingSeason && isRadioChecked)), [canConcludeGrowingSeason, isRadioChecked, valueDate]
  )

  useEffect(() => {
    if(!canConcludeGrowingSeason && isRadioChecked){
      snackbar.warning(t('still hasnt concluded all the crop management'))
    }
  }, [canConcludeGrowingSeason, isRadioChecked, snackbar, t])


  const handleSubmit = useCallback(
    (data) => {
      if(currentCropManagement.operation.concludeCultivation) {
        if(data.closeGrowingSeason){
          createDialog({
            id: 'confirm-delete-cropManagement-growingSeason',
            Component: ConfirmModal,
            props: {
              onConfirm: () => {
                const realizationDate = moment(valueDate).format(momentBackDateFormat) || moment()
                dispatch(FieldActions.saveOfflineCropManagement(
                  {
                    ...currentCropManagement,
                    operationSlug: currentCropManagement.operation.slug,
                    realizationDate
                  }
                ))

                dispatch(FieldActions.saveOfflineGrowingSeason(
                  {
                    closed: true
                  },
                  () => {
                    handleClose()
                    onSuccess()
                    snackbar.success(
                      t('its crop management and cultivation have been completed')
                    )}
                ))
              },
              message: t('are you sure you want to finalize your crop management and cultivation?')
            }
          })
        } else{
          createDialog({
            id: 'confirm-delete-cropManagement-growinwewegSeason',
            Component: ConfirmModal,
            props: {
              onConfirm: () => {
                const realizationDate = moment(valueDate).format(momentBackDateFormat) || moment()
                dispatch(FieldActions.saveOfflineCropManagement(
                  {
                    ...currentCropManagement,
                    operationSlug: currentCropManagement.operation.slug,
                    realizationDate
                  },
                  () => {
                    handleClose()
                    onSuccess()
                    snackbar.success(
                      t('its management has been completed')
                    )}
                ))
              },
              message: t('are you sure you want to complete your {this}?', {
                howMany: 1,
                this: t('cropManagement', { howMany: 1 })
              })
            }
          })
        }
      }

      if(!currentCropManagement.operation.concludeCultivation) {
        createDialog({
          id: 'confirm-complete-cropManagement',
          Component: ConfirmModal,
          props: {
            onConfirm: () => {
              const realizationDate = moment(valueDate).format(momentBackDateFormat) || moment()
              dispatch(FieldActions.saveOfflineCropManagement(
                {
                  ...currentCropManagement,
                  operationSlug: currentCropManagement.operation.slug,
                  realizationDate
                },
                () => {
                  handleClose()
                  onSuccess()
                  snackbar.success(
                    t('its management has been completed')
                  )
                }
              ))
            },
            message: t('are you sure you want to complete your {this}?', {
              howMany: 1,
              this: t('cropManagement', { howMany: 1 })
            })
          }
        })
      }
    },
    [createDialog, currentCropManagement, dispatch, handleClose, onSuccess, snackbar, t, valueDate]
  )

  const selectOptions = useMemo(
    () => (
      [
        {
          label: t('yes'),
          value: true
        },
        {
          label: t('no'),
          value: false
        }
      ]
    ),
    [t]
  )

  return (
    <Container>
      <Form
        style={ { ...style } }
        ref={ formRef }
        schemaConstructor={ completeSchemaManagement }
        onSubmit={ handleSubmit }
      >

        <Text
          style={ {
            fontWeight: 'bold',
            fontSize: 18,
            textAlign: 'center',
            color: colors.black,
            paddingBottom: 15
          } }
        >
          <I18n>what was the date of realization?</I18n>
        </Text>
        <View>
          <DatePicker
            value={ valueDate }
            onChange={ setValueDate }
            maxDate={ new Date() }
            previousTitle={
              <Icon icon={ arrowLeftSimple } color={ colors.secondary } size={ 18 } />
            }
            nextTitle={
              <Icon icon={ arrowRightSimple } color={ colors.secondary } size={ 18 } />

            }
            previousTitleStyle={ { marginLeft: 30 } }
            nextTitleStyle={ { marginRight: 30 } }
          />
        </View>
        {currentCropManagement.operation.concludeCultivation && (
          <ContentRadioGroup>
            <Text
              style={ {
                fontWeight: 'bold',
                fontSize: 18,
                paddingTop: 15,
                paddingBottom: 15
              } }
            >
              <I18n>want to end cultivation?</I18n>
            </Text>
            <RadioGroup
              name="closeGrowingSeason"
              options={ selectOptions }
              variant="row"
              defaultValue={ false }
              onChange={ value => {setIsRadioChecked(value)} }
            />
          </ContentRadioGroup>
        )}

        {!withoutSubmitButton && (
          <ContainerButtons>
            <ButtonGroup>
              <Button
                title={ t('cancel') }
                variant='outlined'
                onPress={ handleClose }
                style={ { marginRight: '5%', textTransform: 'none' } }
              />
              <Button
                title={ t('confirm') }
                onPress={ () => formRef.current?.submit() }
                disabled={ isDisabled }
              />
            </ButtonGroup>
          </ContainerButtons>
        )}
      </Form>
    </Container>
  )
})

CompleteManagementForm.propTypes = {
  onSuccess: PropTypes.func,
  withoutSubmitButton: PropTypes.bool,
  style: PropTypes.object,
  currentCropManagement: PropTypes.object,
  handleClose: PropTypes.func,
  canConcludeGrowingSeason: PropTypes.bool
}

CompleteManagementForm.defaultProps = {
  onSuccess: () => {},
  withoutSubmitButton: false,
  style: {},
  currentCropManagement: {},
  handleClose: () => {},
  canConcludeGrowingSeason: false
}

export default CompleteManagementForm
