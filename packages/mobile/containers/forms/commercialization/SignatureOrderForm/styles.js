import styled from 'styled-components/native'

export const Container = styled.View`
  justify-content: center;
  align-items: center;
  flex: 1 1 auto;
  flex-direction: column;
  width: 100%;
`

export const TextDate = styled.Text`
  font-weight: 400;
  font-size: 16px;
  padding-bottom: 20px;
`

export const TextPassword = styled.Text`
  font-size: 16px;
  padding-bottom: 10px;
`
