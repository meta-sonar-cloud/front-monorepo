import styled from 'styled-components/native'

export const Container = styled.View`
  justify-content: center;
  align-items: center;
  flex: 1 1 auto;
  flex-direction: column;
  width: 100%;
`
