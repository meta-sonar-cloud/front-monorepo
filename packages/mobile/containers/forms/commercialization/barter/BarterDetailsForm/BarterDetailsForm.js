import React, { forwardRef, useMemo } from 'react'
import { useSelector } from 'react-redux'

import PropTypes from 'prop-types'

import barterDetailsSchema from '@smartcoop/forms/schemas/commercialization/barter/barterDetails.schema'
import I18n, { useT } from '@smartcoop/i18n'
import Button from '@smartcoop/mobile-components/Button'
import Form from '@smartcoop/mobile-components/Form'
import InputSelect from '@smartcoop/mobile-components/InputSelect'
import InputText from '@smartcoop/mobile-components/InputText'
import { ButtonsContainer } from '@smartcoop/mobile-containers/layouts/AuthenticatedLayout/theme'
import { getCrops as getCropsService } from '@smartcoop/services/apis/smartcoopApi/resources/crop'
import { getUserStateRegistrations as getUserStateRegistrationsService } from '@smartcoop/services/apis/smartcoopApi/resources/stateRegistration'
import { selectCurrentOrganization } from '@smartcoop/stores/organization/selectorOrganization'

import { Container } from './styles'

const BarterDetailsForm = forwardRef((props, formRef) => {
  const { onSubmit, withoutSubmitButton, barterName, cropInternalCode, stateRegistrationId } = props
  const t = useT()

  const currentOrganization = useSelector(selectCurrentOrganization)

  const urlParams = useMemo(
    () => ({ organizationId: currentOrganization.id }),
    [currentOrganization.id]
  )

  return (
    <Container>
      <Form
        style={ { display: 'flex', flexDirection: 'column', width: '100%' } }
        ref={ formRef }
        schemaConstructor={ barterDetailsSchema }
        onSubmit={ (data) => onSubmit(data) }
      >
        <InputText
          name="barterName"
          label={ t('name') }
          defaultValue={ barterName || 'Meu pacote' }
          fullWidth
        />
        <InputSelect
          name="cropInternalCode"
          label={ t('crop used in trade') }
          defaultValue={ cropInternalCode }
          options={ getCropsService }
          asyncOptionLabelField="description"
          asyncOptionValueField="internalCode"
        />
        <InputSelect
          name="stateRegistrationId"
          label={ t('state registration') }
          urlParams={ urlParams }
          defaultValue={ stateRegistrationId }
          options={ getUserStateRegistrationsService }
          asyncOptionLabelField="stateRegistration"
          asyncOptionValueField="id"
        />
        {!withoutSubmitButton && (
          <ButtonsContainer>
            <Button
              id="mobile-barter-details-form-button"
              style={ { width: '48%' } }
              onClick={ () => formRef.current.submit() }
            >
              <I18n>save</I18n>
            </Button>
          </ButtonsContainer>
        )}
      </Form>
    </Container>
  )
})

BarterDetailsForm.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  withoutSubmitButton: PropTypes.bool,
  barterName: PropTypes.string,
  cropInternalCode: PropTypes.string,
  stateRegistrationId: PropTypes.string
}

BarterDetailsForm.defaultProps = {
  withoutSubmitButton: false,
  barterName: '',
  cropInternalCode: '',
  stateRegistrationId: ''
}

export default BarterDetailsForm
