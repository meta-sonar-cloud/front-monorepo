import React, { useCallback, forwardRef, useMemo, useState } from 'react'
import { useSelector } from 'react-redux'

import PropTypes from 'prop-types'

import ceil from 'lodash/ceil'
import toNumber from 'lodash/toNumber'

import { reloadSchema } from '@smartcoop/forms'
import createInCashOrderSchema from '@smartcoop/forms/schemas/commercialization/createInCashOrder.schema'
import { useT } from '@smartcoop/i18n'
import Button from '@smartcoop/mobile-components/Button'
import Form from '@smartcoop/mobile-components/Form'
import InputSelect from '@smartcoop/mobile-components/InputSelect'
import InputText from '@smartcoop/mobile-components/InputText'
import InputUnit from '@smartcoop/mobile-components/InputUnit'
import { getUserProductsBalanceWithQuotation } from '@smartcoop/services/apis/smartcoopApi/resources/productBalance'
import { getPropertiesWithData } from '@smartcoop/services/apis/smartcoopApi/resources/property'
import { getSettlementDateByOrg } from '@smartcoop/services/apis/smartcoopApi/resources/salesOrders'
import { getUserStateRegistrations as getUserStateRegistrationsService } from '@smartcoop/services/apis/smartcoopApi/resources/stateRegistration'
import { selectCurrentOrganization } from '@smartcoop/stores/organization/selectorOrganization'
import { formatCurrency, formatNumber } from '@smartcoop/utils/formatters'

import { Container, ButtonContainer, Row } from './styles'

const InCashForm = forwardRef((props, formRef) => {
  const { withoutSubmitButton, onSubmit, loading } = props

  const t = useT()

  const currentOrganization = useSelector(selectCurrentOrganization)

  const [productUnit, setProductUnit] = useState('kg')
  const [quantityUnit, setQuantityUnit] = useState('kg')

  const urlParams = useMemo(
    () => ({ organizationId: currentOrganization.id }),
    [currentOrganization.id]
  )

  const getQuotationUnit = useCallback((asyncItem) => {
    // eslint-disable-next-line eqeqeq
    const isConversionFactorEmpty = !asyncItem.conversionFactor || asyncItem.conversionFactor == 0
    return isConversionFactorEmpty
      ? asyncItem.measureUnit
      : `${
        t('{this} of', { this: asyncItem.unitOfMeasuresForConversion } )
      } ${
        formatNumber(ceil(Number(asyncItem.conversionFactor), 2))
      } ${
        asyncItem.measureUnit
      }`
  }, [t])

  const getQuantityInSacks = useCallback((asyncItem) => {
    // eslint-disable-next-line eqeqeq
    const isConversionFactorEmpty = !asyncItem.conversionFactor || asyncItem.conversionFactor == 0
    return isConversionFactorEmpty
      ? asyncItem.currentBalance
      :  Math.floor(Number(asyncItem.currentBalance)/Number(asyncItem.conversionFactor))
  }, [])

  const onProductChange = useCallback(
    (_, asyncItem) => {
      setQuantityUnit(getQuotationUnit(asyncItem))
      setProductUnit(asyncItem.measureUnit)

      formRef.current.getFieldRef('exibitionBalance').setValue(`${ formatNumber(getQuantityInSacks(asyncItem)) } ${ getQuotationUnit(asyncItem) }`)
      formRef.current.getFieldRef('exibitionBalanceInUnit').setValue((formatNumber(ceil(asyncItem.currentBalance, 2))).toString())
      formRef.current.getFieldRef('conversionFactor').setValue((asyncItem.conversionFactor || 0).toString())

      formRef.current.getFieldRef('productBalance').setValue(asyncItem.currentBalance.toString())

      formRef.current.getFieldRef('quotation').setValue(asyncItem.quotation.toString())
      formRef.current.getFieldRef('exibitionQuotation').setValue(`${ formatCurrency(asyncItem.quotation * asyncItem.conversionFactor) }/${ getQuotationUnit(asyncItem) }`)
      formRef.current.getFieldRef('quotationExpirationDate').setValue(asyncItem.expirationDate)

      formRef.current.getFieldRef('totalValue').setValue(
        (toNumber(formRef.current.getFieldRef('quantity').value * toNumber(asyncItem.quotation * asyncItem.conversionFactor))).toString()
      )
      formRef.current.getFieldRef('totalValueExibition').setValue(
        formatCurrency(toNumber(formRef.current.getFieldRef('quantity').value) * toNumber(asyncItem.quotation * asyncItem.conversionFactor))
      )

      reloadSchema(formRef, true)
    },
    [formRef, getQuantityInSacks, getQuotationUnit]
  )

  const onQuantityChange = useCallback(({ target: { value } }) => {
    formRef.current.getFieldRef('totalValue').setValue(
      toNumber(value) * toNumber(formRef.current.getFieldRef('quotation').value) * toNumber(formRef.current.getFieldRef('conversionFactor').value )
    )
    formRef.current.getFieldRef('totalValueExibition').setValue(
      formatCurrency(toNumber(value) * toNumber(formRef.current.getFieldRef('quotation').value) * toNumber(formRef.current.getFieldRef('conversionFactor').value))
    )
  }, [formRef])

  return (
    <Container>
      <Form
        style={ { display: 'flex', flexDirection: 'column', width: '100%' } }
        ref={ formRef }
        schemaConstructor={ createInCashOrderSchema }
        onSubmit={ (data) => onSubmit(data) }
      >
        <Row>
          <InputSelect
            label={ t('product') }
            name="productInternalCode"
            options={ getUserProductsBalanceWithQuotation }
            urlParams={ urlParams }
            asyncOptionLabelField="productName"
            asyncOptionValueField="productInternalCode"
            onChange={ onProductChange }
            disabled={ loading }
          />
        </Row>

        <Row>
          <InputText
            name="exibitionQuotation"
            label={ t('quotation', { howMany: 1 }) }
            fullWidth
            disabled
          />
          <InputText
            style={ { display: 'none' } }
            name="quotation"
            fullWidth
            disabled
          />
          <InputText
            style={ { display: 'none' } }
            name="quotationExpirationDate"
            fullWidth
            disabled
          />
        </Row>
        <Row>
          <InputText
            name="exibitionBalance"
            label={ t('balance') }
            fullWidth
            disabled
          />
          <InputText
            style={ { display: 'none' } }
            name="productBalance"
            fullWidth
            disabled
          />
          <InputText
            style={ { display: 'none' } }
            name="conversionFactor"
            fullWidth
            disabled
          />
        </Row>
        <Row>
          <InputText
            name="exibitionBalanceInUnit"
            label={ t('{this} in {that}', { this: t('balance'), that: productUnit }) }
            fullWidth
            disabled
          />
        </Row>
        <Row>
          <InputUnit
            name="quantity"
            label={ t('quantity') }
            unit={ quantityUnit }
            type="integer"
            fullWidth
            onChange={ onQuantityChange }
            disabled={ loading }
          />
        </Row>
        <Row>
          <InputText
            style={ { display: 'none' } }
            name="totalValue"
            fullWidth
            disabled
          />
          <InputText
            name="totalValueExibition"
            label={ t('total value') }
            fullWidth
            disabled
          />
        </Row>
        <Row>
          <InputSelect
            name="settlementDate"
            label={ t('settlement date') }
            options={ getSettlementDateByOrg }
            asyncOptionLabelField="settlementDate"
            asyncOptionValueField="settlementDate"
            urlParams={ urlParams }
            disabled={ loading }
          />
        </Row>
        <Row>
          <InputSelect
            label={ t('property', { howMany: 1 }) }
            name="propertyId"
            options={ getPropertiesWithData }
            asyncOptionLabelField="name"
            disabled={ loading }
          />
        </Row>
        <Row>
          <InputSelect
            name="stateRegistrationId"
            label={ t('state registration') }
            urlParams={ urlParams }
            options={ getUserStateRegistrationsService }
            asyncOptionLabelField="stateRegistration"
            asyncOptionValueField="id"
          />
        </Row>
        {!withoutSubmitButton && (
          <ButtonContainer>
            <Button
              id="mobile-in-cash-form-button"
              onPress={ () => formRef.current.submit() }
              title={ t('save') }
            />
          </ButtonContainer>
        )}
      </Form>
    </Container>
  )
})

InCashForm.propTypes = {
  onSubmit: PropTypes.func,
  withoutSubmitButton: PropTypes.bool,
  loading: PropTypes.bool
}

InCashForm.defaultProps = {
  onSubmit: () => {},
  withoutSubmitButton: false,
  loading: false
}

export default InCashForm
