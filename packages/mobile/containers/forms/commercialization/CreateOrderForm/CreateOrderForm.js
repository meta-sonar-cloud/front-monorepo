import React, { useCallback, forwardRef, useMemo, useState } from 'react'
import { useDispatch } from 'react-redux'

import PropTypes from 'prop-types'

import capitalize from 'lodash/capitalize'

import { useDialog } from '@smartcoop/dialog'
import { useT } from '@smartcoop/i18n'
import RadioGroup from '@smartcoop/mobile-components/RadioGroup'
import { Scroll } from '@smartcoop/mobile-containers/layouts/AuthenticatedLayout/theme'
import SignatureOrderModal from '@smartcoop/mobile-containers/modals/Commercialization/SignatureOrderModal'
import { useSnackbar } from '@smartcoop/snackbar'
import { SalesOrdersActions } from '@smartcoop/stores/salesOrders'
import { colors } from '@smartcoop/styles'


import FutureSaleForm from './FutureSaleForm'
import InCashForm from './InCashForm'
import { Container, Row } from './styles'
import TriggerForm from './TriggerForm'

const CreateOrderForm = forwardRef((props, formRef) => {
  const { withoutSubmitButton, onSubmit } = props

  const t = useT()
  const dispatch = useCallback(useDispatch(), [])
  const snackbar = useSnackbar()
  const [loading, setLoading] = useState(false)
  const [type, setType] = useState('in_cash')
  const { createDialog } = useDialog()


  const handleSubmit = useCallback(
    (incomingData) => {
      const { exibitionBalance, exibitionBalanceInUnit, exibitionQuotation, totalValueExibition, ...data } = incomingData
      const quantity = incomingData.quantity * incomingData.conversionFactor

      createDialog({
        id: 'signature-order-modal',
        Component: SignatureOrderModal,
        props: {
          onSuccess: () => {
            setLoading(true)
            dispatch(SalesOrdersActions.saveOfflineSalesOrder({ ...data, type, quantity },
              () => {
                snackbar.success(t('your {this} was registered', {
                  howMany: 1,
                  this: t('demand', { howMany: 1 }),
                  gender: 'female'
                }))
                setLoading(false)
              },
              () => setLoading(false)
            ))
            onSubmit(data)
          }
        }
      })
    },
    [createDialog, dispatch, onSubmit, snackbar, t, type]
  )

  const statusOptions = useMemo(
    () => [
      {
        label: capitalize(t('in cash')),
        value: 'in_cash'
      },
      {
        label: capitalize(t('upcoming', { gender: 'female' })),
        value: 'future_sale'
      },
      {
        label: capitalize(t('trigger')),
        value: 'trigger'
      }
    ],
    [t]
  )

  const OrderForm = useMemo(() => {
    switch (type) {
      case 'future_sale':
        return FutureSaleForm
      case 'trigger':
        return TriggerForm
      case 'in_cash':
      default:
        return InCashForm
    }
  }, [type])

  return (
    <Container>
      <Row>
        <RadioGroup
          options={ statusOptions }
          variant="row"
          value={ type }
          onChange={ ({ value }) => setType(value) }
          style={ { marginBottom: 10 } }
          itemStyle={ { paddingTop: 9, paddingBottom: 9, paddingLeft: 11, paddingRight: 11 } }
          itemFontStyle={ { fontSize: 16, color: colors.black } }
          detached
        />
      </Row>
      <Scroll padding={ 0 }>
        <OrderForm
          ref={ formRef }
          onSubmit={ handleSubmit }
          withoutSubmitButton={ withoutSubmitButton }
          loading={ loading }
        />
      </Scroll>
    </Container>
  )
})

CreateOrderForm.propTypes = {
  onSubmit: PropTypes.func,
  withoutSubmitButton: PropTypes.bool
}

CreateOrderForm.defaultProps = {
  onSubmit: () => {},
  withoutSubmitButton: false
}

export default CreateOrderForm
