import React, { useCallback, forwardRef } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import PropTypes from 'prop-types'

import { useDialog } from '@smartcoop/dialog'
import { reloadSchema } from '@smartcoop/forms'
import createPasswordSchema from '@smartcoop/forms/schemas/auth/createPassword.schema'
import { useT } from '@smartcoop/i18n'
import Button from '@smartcoop/mobile-components/Button'
import Form from '@smartcoop/mobile-components/Form'
import InputPassword from '@smartcoop/mobile-components/InputPassword'
import LoadingModal from '@smartcoop/mobile-containers/modals/LoadingModal'
import { AuthenticationActions } from '@smartcoop/stores/authentication'
import { selectUser } from '@smartcoop/stores/user/selectorUser'

import { Container, ButtonContainer } from './styles'

const CreatePasswordForm = forwardRef((props, formRef) => {
  const { withoutSubmitButton, loading, onSubmit } = props

  const dispatch = useCallback(useDispatch(), [])

  const user = useSelector(selectUser)

  const t = useT()
  const { createDialog, removeDialog } = useDialog()

  const handleKeyPressSubmit = useCallback(
    () => {
      formRef.current.submit()
    },
    [formRef]
  )

  const handleSubmit = useCallback(
    ({ newPassword }) => {
      createDialog({
        id: 'loading',
        Component: LoadingModal
      })

      const removeLoader = () => {
        setTimeout(() => removeDialog({ id: 'loading' }), 150)
      }

      dispatch(AuthenticationActions.changePassword(
        newPassword,
        () => {
          onSubmit()
          removeLoader()
        },
        removeLoader
      ))
    },
    [createDialog, dispatch, onSubmit, removeDialog]
  )

  const validateSamePassword = useCallback(
    () => reloadSchema(formRef, true),
    [formRef]
  )

  const schemaConstructor = useCallback(
    (opts) => createPasswordSchema({ ...opts, user }),
    [user]
  )

  return (
    <Container>
      <Form
        ref={ formRef }
        schemaConstructor={ schemaConstructor }
        onSubmit={ handleSubmit }
      >
        <InputPassword
          name="newPassword"
          label={ t('password') }
          onChange={ validateSamePassword }
          disabled={ loading }
        />

        <InputPassword
          name="confirmPassword"
          label={ t('confirm password') }
          disabled={ loading }
          onSubmitEditing={ handleKeyPressSubmit }
        />

        {!withoutSubmitButton && (
          <ButtonContainer>
            <Button
              style={ { width: '48%' } }
              title={ t('next') }
              onPress={ () => formRef.current.submit() }
              disabled={ loading }
            />
          </ButtonContainer>
        )}
      </Form>
    </Container>
  )
})

CreatePasswordForm.propTypes = {
  loading: PropTypes.bool,
  onSubmit: PropTypes.func,
  withoutSubmitButton: PropTypes.bool
}

CreatePasswordForm.defaultProps = {
  loading: false,
  onSubmit: () => {},
  withoutSubmitButton: false
}

export default CreatePasswordForm
