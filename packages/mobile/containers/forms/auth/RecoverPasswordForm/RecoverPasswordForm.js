import React, { useCallback, forwardRef } from 'react'
import { useDispatch } from 'react-redux'

import PropTypes from 'prop-types'

import { useDialog } from '@smartcoop/dialog'
import recoverPassword from '@smartcoop/forms/schemas/auth/recoverPassword.schema'
import { useT } from '@smartcoop/i18n'
import Button from '@smartcoop/mobile-components/Button'
import Form from '@smartcoop/mobile-components/Form'
import InputCpfCnpj from '@smartcoop/mobile-components/InputCpfCnpj'
import ErrorModal from '@smartcoop/mobile-containers/modals/ErrorModal'
import LoadingModal from '@smartcoop/mobile-containers/modals/LoadingModal'
import { AuthenticationActions } from '@smartcoop/stores/authentication'

import { Container, ButtonContainer } from './styles'

const RecoverPasswordForm = forwardRef((props, formRef) => {
  const { withoutSubmitButton, onSubmit } = props

  const dispatch = useCallback(useDispatch(), [])
  const { createDialog, removeDialog } = useDialog()

  const t = useT()

  const handleKeyPressSubmit = useCallback(
    () => {
      formRef.current.submit()
    },
    [formRef]
  )

  const handleSubmit = useCallback(
    async (data) => {
      createDialog({
        id: 'loading',
        Component: LoadingModal
      })

      const removeLoader = () => {
        setTimeout(() => removeDialog({ id: 'loading' }), 150)
      }

      dispatch(AuthenticationActions.recoverPassword(
        data.document,
        () => {
          removeLoader()
          onSubmit()
        },
        (error) => {
          createDialog({
            id: 'user-not-found',
            Component: ErrorModal,
            props: {
              message: t(error)
            }
          })
          removeLoader()
        }
      ))
    },
    [createDialog, dispatch, onSubmit, removeDialog, t]
  )

  return (
    <Container>
      <Form
        ref={ formRef }
        schemaConstructor={ recoverPassword }
        onSubmit={ handleSubmit }
      >
        <InputCpfCnpj
          name="document"
          label="CPF"
          onlyCpf
          onSubmitEditing={ handleKeyPressSubmit }
        />

        {!withoutSubmitButton && (
          <ButtonContainer>
            <Button
              style={ { width: '48%' } }
              title={ t('next') }
              onPress={ () => formRef.current.submit() }
            />
          </ButtonContainer>
        )}
      </Form>
    </Container>
  )
})

RecoverPasswordForm.propTypes = {
  onSubmit: PropTypes.func,
  withoutSubmitButton: PropTypes.bool
}

RecoverPasswordForm.defaultProps = {
  onSubmit: () => {},
  withoutSubmitButton: false
}

export default RecoverPasswordForm
