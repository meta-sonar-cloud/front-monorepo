import React, { useCallback, forwardRef } from 'react'
import { useDispatch } from 'react-redux'

import PropTypes from 'prop-types'

import { useDialog } from '@smartcoop/dialog'
import resetCode from '@smartcoop/forms/schemas/auth/resetcode.schema'
import { useT } from '@smartcoop/i18n'
import Button from '@smartcoop/mobile-components/Button'
import Form from '@smartcoop/mobile-components/Form'
import InputCode from '@smartcoop/mobile-components/InputCode'
import ErrorModal from '@smartcoop/mobile-containers/modals/ErrorModal'
import LoadingModal from '@smartcoop/mobile-containers/modals/LoadingModal'
import { AuthenticationActions } from '@smartcoop/stores/authentication'

import { Container, ButtonContainer } from './styles'

const ResetCodeForm = forwardRef((props, formRef) => {
  const { withoutSubmitButton, onSubmit } = props

  const dispatch = useCallback(useDispatch(), [])
  const { createDialog } = useDialog()

  const t = useT()

  const handleKeyPressSubmit = useCallback(
    () => {
      formRef.current.submit()
    },
    [formRef]
  )

  const handleResetCodeSubmit = useCallback(
    (data) => {

      createDialog({
        id: 'loading',
        Component: LoadingModal
      })

      dispatch(AuthenticationActions.validateVerificationCode(
        data.code,
        () => {
          onSubmit()
        },
        () => {
          createDialog({
            id: 'user-not-found',
            Component: ErrorModal,
            props: {
              message: t('invalid verification code')
            }
          })
        }
      ))
    },
    [createDialog, dispatch, onSubmit, t]
  )

  return (
    <Container>
      <Form
        ref={ formRef }
        schemaConstructor={ resetCode }
        onSubmit={ handleResetCodeSubmit }
      >
        <InputCode
          name="code"
          label={ t('code') }
          onSubmitEditing={ handleKeyPressSubmit }
        />

        {!withoutSubmitButton && (
          <ButtonContainer>
            <Button
              style={ { width: '48%' } }
              title={ t('next') }
              onPress={ () => formRef.current.submit() }
            />
          </ButtonContainer>
        )}
      </Form>
    </Container>
  )
})

ResetCodeForm.propTypes = {
  onSubmit: PropTypes.func,
  withoutSubmitButton: PropTypes.bool
}

ResetCodeForm.defaultProps = {
  onSubmit: () => {},
  withoutSubmitButton: false
}

export default ResetCodeForm
