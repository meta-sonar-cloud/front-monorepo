import React, { useCallback, forwardRef } from 'react'
import { useDispatch } from 'react-redux'

import PropTypes from 'prop-types'

import { useDialog } from '@smartcoop/dialog'
import loginSchema from '@smartcoop/forms/schemas/auth/login.schema'
import { useT } from '@smartcoop/i18n'
import Button from '@smartcoop/mobile-components/Button'
import Form from '@smartcoop/mobile-components/Form'
import InputCpfCnpj from '@smartcoop/mobile-components/InputCpfCnpj'
import InputPassword from '@smartcoop/mobile-components/InputPassword'
import ErrorModal from '@smartcoop/mobile-containers/modals/ErrorModal'
import LoadingModal from '@smartcoop/mobile-containers/modals/LoadingModal'
import { AuthenticationActions } from '@smartcoop/stores/authentication'

import { Container } from './styles'

const LoginForm = forwardRef((props, formRef) => {
  const { withoutSubmitButton, loading, onSubmit } = props

  const dispatch = useCallback(useDispatch(), [])
  const { createDialog, removeDialog } = useDialog()
  const t = useT()

  const handleKeyPressSubmit = useCallback(
    () => {
      formRef.current.submit()
    },
    [formRef]
  )

  const handleSubmit = useCallback(
    ({ document: doc, password }) => {
      createDialog({
        id: 'loading',
        Component: LoadingModal
      })

      const removeLoader = () => {
        setTimeout(() => removeDialog({ id: 'loading' }), 150)
      }

      dispatch(AuthenticationActions.login(
        doc,
        password,
        () => {
          onSubmit()
          removeLoader()
        },
        (error) => {
          createDialog({
            id: 'user-not-found',
            Component: ErrorModal,
            props: {
              message: t(error)
            }
          })
          removeLoader()
        }
      ))
    },
    [createDialog, dispatch, onSubmit, removeDialog, t]
  )

  return (
    <Form
      ref={ formRef }
      schemaConstructor={ loginSchema }
      onSubmit={ handleSubmit }
    >
      <Container>
        <InputCpfCnpj
          name="document"
          label="CPF/CNPJ"
          disabled={ loading }
        />

        <InputPassword
          name="password"
          label={ t('password') }
          disabled={ loading }
          onSubmitEditing={ handleKeyPressSubmit }
        />

        {!withoutSubmitButton && (
          <Button
            title={ t('login') }
            onPress={ () => formRef.current.submit() }
            disabled={ loading }
          />
        )}
      </Container>
    </Form>
  )
})

LoginForm.propTypes = {
  loading: PropTypes.bool,
  onSubmit: PropTypes.func,
  withoutSubmitButton: PropTypes.bool
}

LoginForm.defaultProps = {
  loading: false,
  onSubmit: () => {},
  withoutSubmitButton: false
}

export default LoginForm
