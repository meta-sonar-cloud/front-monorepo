import styled from 'styled-components/native'

export const Container = styled.View`
  flex: 1 1 auto;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  width: 100%;
`
