import React, { useCallback, forwardRef } from 'react'
import { useDispatch } from 'react-redux'

import PropTypes from 'prop-types'

import { useDialog } from '@smartcoop/dialog'
import signUp from '@smartcoop/forms/schemas/auth/signup.schema'
import { useT } from '@smartcoop/i18n'
import Button from '@smartcoop/mobile-components/Button'
import Form from '@smartcoop/mobile-components/Form'
import InputCpfCnpj from '@smartcoop/mobile-components/InputCpfCnpj'
import ErrorModal from '@smartcoop/mobile-containers/modals/ErrorModal'
import LoadingModal from '@smartcoop/mobile-containers/modals/LoadingModal'
import { AuthenticationActions } from '@smartcoop/stores/authentication'

import { Container, ButtonContainer } from './styles'

const SignUpForm = forwardRef((props, formRef) => {
  const { withoutSubmitButton, onSubmit } = props

  const dispatch = useCallback(useDispatch(), [])
  const { createDialog, removeDialog } = useDialog()
  const t = useT()

  const handleKeyPressSubmit = useCallback(
    () => {
      formRef.current.submit()
    },
    [formRef]
  )

  const handleSubmit = useCallback(
    ({ document: doc }) => {
      createDialog({
        id: 'loading',
        Component: LoadingModal
      })
      const removeLoader = () => {
        setTimeout(() => removeDialog({ id: 'loading' }), 150)
      }

      dispatch(AuthenticationActions.validateSignUp(
        doc,
        () => {
          onSubmit()
          removeLoader()
        },
        (error) => {
          createDialog({
            id: 'user-not-found',
            Component: ErrorModal,
            props: {
              message: t(error)
            }
          })
          removeLoader()
        },
        true
      ))
    },
    [createDialog, dispatch, onSubmit, removeDialog, t]
  )

  return (
    <Container>
      <Form
        ref={ formRef }
        schemaConstructor={ signUp }
        onSubmit={ handleSubmit }
      >
        <InputCpfCnpj
          name="document"
          label="CPF/CNPJ"
          onSubmitEditing={ handleKeyPressSubmit }
        />
        {!withoutSubmitButton && (
          <ButtonContainer>
            <Button
              style={ { width: '48%' } }
              title={ t('signup') }
              onPress={ () => formRef.current.submit() }
            />
          </ButtonContainer>
        )}
      </Form>
    </Container>
  )
})

SignUpForm.propTypes = {
  onSubmit: PropTypes.func,
  withoutSubmitButton: PropTypes.bool
}

SignUpForm.defaultProps = {
  onSubmit: () => {},
  withoutSubmitButton: false
}

export default SignUpForm
