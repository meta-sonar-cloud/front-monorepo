import React, { useEffect, useRef } from 'react'
import { Dimensions, Platform, Animated } from 'react-native'
import { getStatusBarHeight } from 'react-native-iphone-x-helper'

import styled from 'styled-components/native'

import Icon from '@smartcoop/mobile-components/Icon'
import { colors } from '@smartcoop/styles'
import mobileBackground from '@smartcoop/styles/assets/images/mobile-guest-background'


export const Container = styled.View`
  flex: 1;
  width: ${ Dimensions.get('window').width }px;
  height: ${ Dimensions.get('window').height }px;
  justify-content: space-between;
  background-color: ${ colors.backgroundHtml };
`

export const Background = styled.View`
  justify-content: flex-start;
  position: absolute;
  left: 0;
  width: ${ Dimensions.get('window').width }px;
  height: ${ Dimensions.get('window').height }px;
`

export const ImageBackgroundContainer = styled.View`
  flex: 1;
`

export const ImageBackground = styled(Icon).attrs({
  icon: mobileBackground,
  width: Dimensions.get('window').width
})``

export const Content = styled.View`
  flex: 1;
  background-color: transparent;
  justify-content: flex-end;
  padding-top: ${ (Platform.OS === 'ios' ? getStatusBarHeight() : 0) + 150 }px;
`

export const KeyboardAvoidingView = styled.KeyboardAvoidingView.attrs({
  behavior: Platform.OS === 'ios' ? 'padding' : 'height'
})`
  flex: 1;
  align-items: center;
  justify-content: center;
`

export const Header = styled.View`
  width: 100%;
  flex-direction: row;
  align-items: center;
  justify-content: space-around;
  margin-bottom: 10px;
  margin-top: ${ Platform.OS === 'ios' ? getStatusBarHeight() : 0 }px;
  height: 150px;
`

export const Title = styled.Text`
  color: ${ colors.text };
  font-size: 20px;
  font-family: Montserrat-Bold;
`


export const Scroll = styled.ScrollView.attrs({
  keyboardShouldPersistTaps: 'always'
})`
  width: 100%;
`

export const ScrollContainer = styled.View`
  position: absolute;
  left: 0;
  width: ${ Dimensions.get('window').width }px;
  height: ${ Dimensions.get('window').height }px;
`

const ContentContainerStyled = styled(Animated.View).attrs({
  elevation: 20,
  onStartShouldSetResponder: () => true
})`
  flex: 1;
  width: 100%;
  height: 100%;
  min-height: ${ Dimensions.get('window').height - ((Platform.OS === 'ios' ? getStatusBarHeight() : 0) + 150) }px;
  margin-top: 0px;
  padding-top: 25px;
  padding-bottom: 25px;
  background-color: ${ colors.white };
  border-top-left-radius: 10px;
  border-top-right-radius: 10px;
  shadow-color: #000;
  shadow-offset: 0px -1px;
  shadow-opacity: 0.4;
  shadow-radius: 3px;
`

// eslint-disable-next-line react/prop-types
export const ContentContainer = ({ focused, ...props } = {}) => {
  const mounted = useRef(false)
  const animationOpacity = useRef(new Animated.Value(0))

  useEffect(() => {
    setTimeout(
      () => {
        Animated.timing(animationOpacity.current, {
          toValue: focused ? 1 : 0,
          duration: 200,
          useNativeDriver: true
        }).start()
        mounted.current = true
      },
      mounted.current ? 1 : 100
    )
  }, [focused])


  return (
    <ContentContainerStyled
      { ...props }
      style={ { opacity: animationOpacity.current } }
    />
  )
}
