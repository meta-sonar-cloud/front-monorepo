import styled from 'styled-components/native'

import colors from '@smartcoop/styles/colors'

export const Item = styled.View`
  width: 100%;
  padding: 20px 30px;
`

export const Title = styled.Text`
  color: ${ colors.text };
  font-size: 20px;
  font-family: Montserrat-Bold;
`

export const Subtitle = styled.Text`
  color: ${ colors.text };
  font-size: 16px;
  margin-bottom: 10px;
  font-family: Montserrat-Bold;
`

export const Text = styled.Text`
  color: ${ colors.text };
  font-size: 14px;
  margin-bottom: 20px;
`

export const TextUnderlined = styled(Text)`
  text-decoration: underline;
`

export const ButtonsContainer = styled.View`
  width: 100%;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`

export const ListItem = styled.View`
  flex-direction: row;
  align-items: flex-start;
`
