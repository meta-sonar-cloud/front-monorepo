import React from 'react'
import { Keyboard, TouchableWithoutFeedback } from 'react-native'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

import PropTypes from 'prop-types'

import I18n from '@smartcoop/i18n'
import Logo from '@smartcoop/styles/assets/images/smartcoop-logo.svg'

import {
  Container,
  Background,
  ImageBackgroundContainer,
  ImageBackground,
  KeyboardAvoidingView,
  Header,
  Title,
  ContentContainer
} from './styles'

const GuestLayout = ({ children, focused }) => (
  <KeyboardAvoidingView>
    <TouchableWithoutFeedback
      onPress={ Keyboard.dismiss }
      accessible={ false }
    >
      <Container>
        <Background>
          <ImageBackgroundContainer>
            <ImageBackground />
          </ImageBackgroundContainer>
        </Background>

        <Header>
          <I18n as={ Title } numberOfLines={ 2 }>
              welcome to smartcoop
          </I18n>

          <Logo width={ 120 } />
        </Header>

        <KeyboardAwareScrollView keyboardShouldPersistTaps="always">
          <ContentContainer focused={ focused }>
            {children}
          </ContentContainer>
        </KeyboardAwareScrollView>

      </Container>
    </TouchableWithoutFeedback>
  </KeyboardAvoidingView>
)

GuestLayout.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.arrayOf(PropTypes.element)
  ]),
  focused: PropTypes.bool
}

GuestLayout.defaultProps = {
  children: null,
  focused: false
}

export default GuestLayout
