import { Dimensions, Platform } from 'react-native'

import styled from 'styled-components/native'

import colors from '@smartcoop/styles/colors'

export const Container = styled.View`
  flex: 1;
  width: ${ Dimensions.get('window').width }px;
  height: ${ Dimensions.get('window').height }px;
  background-color: ${ colors.white };
`

export const Content = styled.View`
  flex: 1;
`

export const KeyboardAvoidingView = styled.KeyboardAvoidingView.attrs({
  behavior: Platform.OS === 'ios' ? 'padding' : 'height'
})`
  flex: 1;
  align-items: center;
  justify-content: center;
`

export const IconContainer = styled.View`
  margin-top: 2px;
`

export const Title = styled.Text`
  color: ${ colors.white };
  font-size: 18px;
`
