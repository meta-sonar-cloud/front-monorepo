import React, { useState, useEffect } from 'react'
import { Keyboard, TouchableWithoutFeedback } from 'react-native'

import PropTypes from 'prop-types'

import useTerm from '@smartcoop/mobile-containers/hooks/useTerm'

import {
  Container,
  Content,
  KeyboardAvoidingView
} from './styles'

const AuthenticatedLayout = ({ children, ...rest }) => {
  const [privacyModal, selectedPrivacyTerm] = useTerm('privacy-term', false)
  const [termModal] = useTerm('use-term', false)
  const [mounted, setMounted] = useState(false)

  useEffect(() => {
    if(mounted) {
      privacyModal(termModal)
    } else {
      setMounted(true)
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [selectedPrivacyTerm, mounted])

  return (
    <KeyboardAvoidingView>
      <TouchableWithoutFeedback
        onPress={ Keyboard.dismiss }
        accessible={ false }
      >
        <Container { ...rest }>
          <Content>
            {children}
          </Content>
        </Container>
      </TouchableWithoutFeedback>
    </KeyboardAvoidingView>
  )
}

AuthenticatedLayout.propTypes = {
  children: PropTypes.element
}

AuthenticatedLayout.defaultProps = {
  children: null
}

export default AuthenticatedLayout
