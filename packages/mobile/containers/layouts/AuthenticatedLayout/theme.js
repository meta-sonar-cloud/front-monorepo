import React from 'react'
import { Platform, RefreshControl, Dimensions } from 'react-native'
import { getStatusBarHeight } from 'react-native-iphone-x-helper'

import { HeaderTitle as RNHeaderTitle } from '@react-navigation/stack'
import PropTypes from 'prop-types'
import styled from 'styled-components/native'

import colors from '@smartcoop/styles/colors'

export const Item = styled.View`
  width: 100%;
  padding: 10px;
`

export const Header = styled.View`
  width: 100%;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  margin-bottom: 10px;
`

export const Title = styled.Text`
  color: ${ colors.text };
  font-size: 20px;
  font-family: Montserrat-Bold;
`

export const Subtitle = styled.Text`
  color: ${ colors.text };
  font-size: 16px;
  margin-bottom: 10px;
  font-family: Montserrat-Bold;
`

export const FormLabel = styled.Text`
  color: ${ colors.text };
  font-size: 14px;
  padding-bottom: 12px;
  font-weight: 700;
`

export const Text = styled.Text`
  color: ${ colors.text };
  font-size: 14px;
  margin-bottom: 20px;
`

export const TextUnderlined = styled(Text)`
  text-decoration: underline;
`

export const ButtonsContainer = styled.View`
  width: 100%;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  padding-bottom: 10px;
`

export const ListItem = styled.View`
  flex-direction: row;
  align-items: flex-start;
`

export const Container = styled.View`
  flex: 1;
  width: 100%;
`

export const LandscapeContainer = styled.View`
  transform: rotate(90deg);
  width: ${ Dimensions.get('window').height - 120 }px;
  height: ${ Dimensions.get('window').width }px;
  margin-top: -20px;
`

export const ScreenHeaderTitleContainer = styled.View`
  flex-direction: row;
  align-items: center;
`

export const HeaderTitle = styled(RNHeaderTitle)`
  color: ${ colors.white };
`

export const ScreenHeaderTitleIcon = styled.View`
  padding-right: 10px;
  padding-top: 0;
`

export const ContentWithoutHeader = styled.View`
  padding-top: ${ Platform.OS === 'ios' ? getStatusBarHeight() : 0 }px;
  flex: 2;
`

const ScrollStyled = styled.ScrollView.attrs({
  keyboardShouldPersistTaps: 'always'
})`
  width: 100%;
  flex: ${ ({ horizontal }) => horizontal ? '0 0 auto' : 1 };
`

const ScrollView = styled.View.attrs({
  onStartShouldSetResponder: () => true
})`
  padding: ${ ({ pad }) => pad ? 10 : 0 }px;
  flex-direction: ${ ({ horizontal }) => horizontal ? 'row' : 'column' };
`

export const Scroll = (props = {}) => {
  const { refreshControl, refreshing, onRefresh, children, horizontal, ...rest } = props
  return (
    <ScrollStyled
      horizontal={ horizontal }
      refreshControl={
        refreshControl
          ? (
            <RefreshControl
              refreshing={ refreshing }
              onRefresh={ onRefresh }
            />
          )
          : undefined
      }
      { ...rest }
    >
      <ScrollView horizontal={ horizontal }>
        {children}
      </ScrollView>
    </ScrollStyled>
  )
}

Scroll.propTypes = {
  children: PropTypes.any,
  refreshControl: PropTypes.bool,
  horizontal: PropTypes.bool,
  refreshing: PropTypes.bool,
  onRefresh: PropTypes.func
}
Scroll.defaultProps = {
  children: null,
  refreshControl: false,
  horizontal: false,
  refreshing: false,
  onRefresh: () => {}
}
