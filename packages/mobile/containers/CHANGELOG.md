# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [0.18.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.17.0...v0.18.0) (2021-04-19)


### Bug Fixes

* **#1748:** fix error eslint ([d442777](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d442777ee2757fe79ab6423304c8437d2774875f)), closes [#581](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/581)
* **adjust value normal part:** adjust value of option normal part ([6ba9c8e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/6ba9c8e666a3dab06e3e39d3b1ddc700be0dad4c))
* **commercialization:** fixed change organization modal ([eedbfa9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/eedbfa9be1e142c2b9c1146165b2f25abbf4b967))
* **commercialization:** selecting same organization with diff registry ([22d73cb](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/22d73cbdbec59ca8bc6e998681e05e3bb8e46b0e))
* **develop:** fix in filters ([aabe6b6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/aabe6b651a4f983fc3b947151ef9c40b0d92b777))
* **docuemntos#1766:** ajustes mapa ([f2306ab](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f2306abda00fc359d42696faeafb74263d437be1)), closes [docuemntos#1766](http://gitlab.meta.com.br/docuemntos/issues/1766)
* **documentos#1702:** request permission ([34df4d0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/34df4d0c45500e58224ce62fc50ad28aa17715f0)), closes [documentos#1702](http://gitlab.meta.com.br/documentos/issues/1702)
* **documentos#1750:** adjustments ([28966d7](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/28966d704546ed37e849552d6fddf196ce808568)), closes [documentos#1750](http://gitlab.meta.com.br/documentos/issues/1750)
* **documentos#1755:** adjustments ([096f90b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/096f90b47500de13e083e959f99be6f044dec0b6)), closes [documentos#1755](http://gitlab.meta.com.br/documentos/issues/1755)
* **navigation:** added stacks as modal ([0b5f808](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0b5f808d1911884eb547d6bb16314a6c5b8ad7e4))
* **navigation:** technical module navigation ([4333c69](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4333c693c2a865f66817fa5b1f4e970d42bd9478))
* **onboarding:** fixed loading ([ef459af](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ef459af658772588809bb93242bf858050f44bad))
* **rain map:** disabe=le rain map ([d844428](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d84442868f35be97df179f8c1782464cf797df05))
* **resize images:** resize images from rain map ([71462ad](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/71462ad6857a3fe04a6b68f2e35194fb6c137381))


### Features

* resolve issues ([7aa198f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7aa198f2c59d6d5720f3ca3e38f4062fa7451787))
* **develop:** fix on submit in other actions ([163b0ab](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/163b0abea7af14120cecc37f2daaa3d476b9e9cf))
* **develop:** set tabs as initial date ([0cc3d90](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0cc3d904b5466879a391f504da4f2edd7686ebb0))
* **documentos#1767:** omit inputs when its calving ([94aa075](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/94aa0753d92fe5f6ed3c83a31bdb71695d646440)), closes [documentos#1767](http://gitlab.meta.com.br/documentos/issues/1767)
* **documentos#1767:** removes clg ([8110892](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/81108929ceed1925fd358a663db034972bb5d85b)), closes [documentos#1767](http://gitlab.meta.com.br/documentos/issues/1767)
* merge develop ([65962b2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/65962b2d2d1752343c77861b94a416495ea3cb77))
* resolve [#1714](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1714) [#1756](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1756) [#1757](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1757) [#1758](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1758) ([ecfb39f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ecfb39f2f1e64696f55ed5a93f3db260fbf062b0))
* resolve [#1739](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1739) ([574a891](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/574a891a16f98cff15008981d31ba12bc04952ed))
* resolve [#1763](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1763) ([12f2121](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/12f212194e6a294e6c7b2b3f77a499f3147d65d2))
* resolve issue ([a8a64e2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a8a64e26d60cd4da0a6654efa924ab8449661974))
* resolve issue ([1525d8e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1525d8eba60317edfb1d7b6215dda063c3ace556))
* resolve issues ([ed338b0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ed338b0fedfa0d76b85c1a053b16589ddf1302ae))
* resolve some issues ([0e7d7ce](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0e7d7ce1738f55d75ddd3907cb5ca197b5885e1a))
* **#1737:** adjustment layout animalbirth and pev mobile ([00f6c1d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/00f6c1d90f8804fb2334a3623223f3c7ec0c3824)), closes [#1737](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1737) [#572](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/572)
* **documentos#1737:** disables status if editing ([6c6cdf8](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/6c6cdf861d2c0cbd328d7dbc89e93f3ae1c4396e)), closes [documentos#1737](http://gitlab.meta.com.br/documentos/issues/1737)
* **documentos#1737:** inseminationItem ([f657827](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f657827f375e615cf882e13c5bac6085faaa5fd0)), closes [documentos#1737](http://gitlab.meta.com.br/documentos/issues/1737)
* **documentos#1737:** reload currentanimal each step of cycle ([a801d42](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a801d427590d143f9bc7d4ff12be66c1fd0e2b01)), closes [documentos#1737](http://gitlab.meta.com.br/documentos/issues/1737)
* **documentos#1743:** fixes undefined and infinite loading ([a6d7aa1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a6d7aa1e343c369e4991e16c5df7f04730a695eb))
* **documentos#1743:** shwo expected date in trigger ([8e57886](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8e57886df3c1c77b12439aabd35bf90a8b6a199a)), closes [documentos#1743](http://gitlab.meta.com.br/documentos/issues/1743)
* wip ([a20c949](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a20c949da809ca74018f6f2ff36d63d6fa877bcc))
* **documentos#1744:** changes get of calving ([60d0cd3](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/60d0cd351cf40aff67d38cf064d7309a50329193)), closes [documentos#1744](http://gitlab.meta.com.br/documentos/issues/1744)
* **documentos#1744:** changes in order list ([d9a3960](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d9a3960d89e1268b1e55363199ee02d526110d71)), closes [documentos#1744](http://gitlab.meta.com.br/documentos/issues/1744)
* wip ([ed88867](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ed88867dbcb9af5b51eff1164ff708db209a3b95))





# [0.17.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.16.0...v0.17.0) (2021-04-15)


### Bug Fixes

* **#1684:** adjustment filter cattle management with lotid ([90a24c5](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/90a24c5c66b22cc94ed54aa2380d3dd3eddfae72)), closes [#1684](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1684) [#549](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/549)
* **#1684:** adjustment rules create insemination ([11514e6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/11514e62e7d4197881d70430ff01f83b6d969c19)), closes [#1684](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1684) [#549](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/549)
* **#1684:** validation register insemination, adjustment order ([7fcca06](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7fcca068a04b500c7469ca78f2c383555a03b4b8)), closes [#1684](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1684) [#549](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/549)
* **#1686:** adjustment filter status register animal ([b4f2a7a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b4f2a7aef3c9a0a17701a1bfaa5b86e0d75da2f4)), closes [#1686](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1686) [#548](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/548)
* **#1701:** adjustment filter vaca in register animal ([7b723f4](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7b723f48a10d605d10e745d83c79d7f8ed02bcb8)), closes [#1701](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1701) [#556](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/556)
* **#1717 #1718 #1719 #1720:** adjustment animalbirth edit ([fc7ef48](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/fc7ef48abf3ac9a056c80ad4884218779c2a2f11)), closes [#1717](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1717) [#1718](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1718) [#1719](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1719) [#1720](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1720) [#563](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/563)
* **#1717 #1718 #1719 #1720:** adjustment filter cattle management ([7507cb0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7507cb0034375db11f744af68acaad9a778a44e9)), closes [#1717](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1717) [#1718](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1718) [#1719](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1719) [#1720](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1720) [#563](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/563)
* **#1730:** block button when register animal ([321f83f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/321f83f2180aacf086175d79f7270e6163c008b4)), closes [#1730](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1730) [#568](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/568)
* **develop:** fix change organization modal in dairyfarm ([5677ec0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/5677ec08c6fd8b9db7884be5231923490e672d20))
* **documentos#1704:** map adjustments ([9dcd718](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9dcd718ee90e9e0e5c55a29f652c2ccf3f820d5f)), closes [documentos#1704](http://gitlab.meta.com.br/documentos/issues/1704)
* **documentos#1715:** hotifx dates ([4241254](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4241254c6d1032194c058641e486735218d513ff)), closes [documentos#1715](http://gitlab.meta.com.br/documentos/issues/1715)


### Features

* resolve [#1735](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1735) ([5cfe034](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/5cfe034b5732998c159c39c75d32d3d13f3a74c0))
* **#1693:** create filter dead animals ([beb6666](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/beb6666f99385a127c4a39c4d3fd20bc33f1fe54)), closes [#1693](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1693) [#551](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/551)
* **#1724:** remove filter earring in list lot ([ac68ec0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ac68ec0aa1a3c3b4afe9bd79dcb4b7b640eaed37)), closes [#1724](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1724) [#565](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/565)
* **#1725:** remove buttons delete in all list ([6fab45b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/6fab45b06ff79eee6e454fe476a6febafffa9da4)), closes [#1725](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1725) [#565](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/565)
* **#1727:** adjustment text registry date ([5b7e1f2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/5b7e1f2ba34154e2f14960b9bd957d890a881be6)), closes [#1727](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1727) [#565](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/565)
* **#1733:** input date range in filter cattle managemnt ([d75e195](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d75e1952dd83c8560fa863efb10d23d33a2436f0)), closes [#1733](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1733) [#569](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/569)
* **develop:** adds status filter in cattle management ([a712e97](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a712e979b9373ae7aa8b9ad16afbf478e6b1de64))
* **documenots#1674:** lint fix ([4bc20ed](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4bc20edc567147bf7aa552f31fac608e51ce898d)), closes [documenots#1674](http://gitlab.meta.com.br/documenots/issues/1674)
* **documentos#1321:** fix typos ([abaeee1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/abaeee120680b43281c94f08ccb3cb7811786598))
* **documentos#1515:** removes earring from filters ([6cf3dea](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/6cf3dea088f4f85fea435ffd5f138e888d95082a)), closes [documentos#1515](http://gitlab.meta.com.br/documentos/issues/1515)
* **documentos#1674:** animals active tab ([cdb9bd2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/cdb9bd265fd8f827f7c1d4f2248887a52d534daf)), closes [documentos#1674](http://gitlab.meta.com.br/documentos/issues/1674)
* **documentos#1674:** fix lint ([7205386](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/72053860bc39a85b39ed8a27d331ce7bfd9b3fd5))
* **documentos#1674:** fixes inanimalbirth ([a399dcc](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a399dcca4c8046db12a8f3dceb25ce591a3af90f))
* **documentos#1674:** insemination max date ([da6f739](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/da6f73970dc41d706ca6978440e4d250052b1032)), closes [documentos#1674](http://gitlab.meta.com.br/documentos/issues/1674)
* **documentos#1674:** option to register animal when theres a calving ([13d3343](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/13d3343db13ea4b062a3558800673c9edfb4e50c)), closes [documentos#1674](http://gitlab.meta.com.br/documentos/issues/1674)
* **documentos#1674:** removes select from lot ([95556de](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/95556ded8b1c169e14381027ef18241053ffdc79)), closes [documentos#1674](http://gitlab.meta.com.br/documentos/issues/1674)
* **documentos#1679:** mindate based on insemination date ([d6cf2b5](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d6cf2b5b95af2203814c035af009c3f35210ebfb)), closes [documentos#1679](http://gitlab.meta.com.br/documentos/issues/1679)
* **documentos#1679:** mindate mobile ([d6bf868](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d6bf868e719e5c4b2fafb685df95f6f009c2ce54)), closes [documentos#1679](http://gitlab.meta.com.br/documentos/issues/1679)
* **documentos#1715:** fix internal/external in mobile ([203baa6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/203baa688c8f675dbc3bbc21dc2b2f711917f85f))
* **documentos#1715:** renaming things and logic changes ([7b0ad3c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7b0ad3c5f4fd006fd458cfb31d4817270f4d561b)), closes [documentos#1715](http://gitlab.meta.com.br/documentos/issues/1715)





# [0.16.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.15.0...v0.16.0) (2021-04-13)


### Bug Fixes

* **#1484:** remove console log and change battery name ([8dfe9ea](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8dfe9ea3ffc23dfb83b47ccb1165aadb92a7030b)), closes [#1484](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1484)
* **#1601:** change icon stations. Apply for technical ([5bc5205](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/5bc52050ccbf45943b38f429387552af9b88e3ab)), closes [#1601](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1601) [#510](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/510)
* **#1649:** change form register animal ([0a65145](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0a6514522fd9f9decc650c73545448a69ce912c9)), closes [#1649](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1649)
* **#1666:** add information is animal or bull ([9611ee8](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9611ee8d1baf68454112ddd1f7dea9f7c866fb97)), closes [#1666](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1666) [#540](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/540)
* **#1668:** remove action get insemination in frontend ([cc7fb75](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/cc7fb752dfd74df0f9893ffcb33c7790c828c6f4)), closes [#1668](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1668) [#542](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/542)
* **#1686:** add filter status animal in register animal ([18c5404](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/18c5404c5e9685fd54103864161317151bc70a52)), closes [#1686](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1686) [#548](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/548)
* **#1686:** add validation form register animal ([8007e1d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8007e1d294c51117a4091e542fac47bf073d7bb8)), closes [#1686](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1686) [#548](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/548)
* **ajustes finos:** ajustes finos da aplicação ([eefa46f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/eefa46fe613632b216df51736217149a8027dd16))
* **ajustes ordem menus:** ajustes nas ordens dos menus no mobile ([8ca6d60](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8ca6d6030f736feb35c426d3853bb1342b5fd804))
* **ajustes tambo:** ajustes em fluxos do tambo e ativacao de usuarios ([8fe2e56](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8fe2e5628f91a6d809e81142daba2dded80c38c4))
* **auth:** fixed first refresh token and added free actions ([fc2eea9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/fc2eea987612d3f4d4c783bb59cb3df82cc6a4d1))
* **documentos#1424:** informar inclusão de dados tambo pela cooperativa ([da53b2c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/da53b2cdaeb87e4cbf8c987b054a867644ec1b54)), closes [documentos#1424](http://gitlab.meta.com.br/documentos/issues/1424)
* **documentos#1560:** ajustes aparencia ([d7f810f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d7f810f0ddf2f083da431d974a709092033aac17)), closes [documentos#1560](http://gitlab.meta.com.br/documentos/issues/1560)
* **documentos#1576:** ajustes conversao numerica ([035a7c2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/035a7c29bcaef1da263db55cd90cc61ebf959d98)), closes [documentos#1576](http://gitlab.meta.com.br/documentos/issues/1576)
* **documentos#1586:** correções e ajustes finos ([e6dbcc0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e6dbcc0d7966935a77519dbb4d8077d3bafcf3c5)), closes [documentos#1586](http://gitlab.meta.com.br/documentos/issues/1586)
* **documentos#1615:** loading mapa de chuva ([3b68621](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3b68621bbac6be21a209b8da9bc3c2439d3f1a05)), closes [documentos#1615](http://gitlab.meta.com.br/documentos/issues/1615)
* **documentos#1632:** ajustes finos app ([0e0ffdb](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0e0ffdbce9e4c4ab1c84e30d092d1cd16f83967d)), closes [documentos#1632](http://gitlab.meta.com.br/documentos/issues/1632)
* **documentos#1642:** correção ativação ([2b25b08](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2b25b088c76585b50f59c0be3abe893d31bf6310)), closes [documentos#1642](http://gitlab.meta.com.br/documentos/issues/1642)
* **documentos#1645:** ajustes modulo tecnico ([442cf1b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/442cf1b0bd4849104d4b6bd93416510a9340b150)), closes [documentos#1645](http://gitlab.meta.com.br/documentos/issues/1645)
* **documentos#1661:** label adjustments ([127efad](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/127efadd69c6afe28c674c9cf351b2adfc0e6283)), closes [documentos#1661](http://gitlab.meta.com.br/documentos/issues/1661)


### Features

* **#1484:** create screens stations and administration module mobile ([becfd36](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/becfd36d9d7c1dceda69af09426beba688a84127)), closes [#1484](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1484) [#484](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/484)
* **#1484:** integration with backend weather stations details ([c776b4e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c776b4eb5720ad698dc40ce90bc003c42b18027a)), closes [#1484](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1484) [#484](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/484)
* **#1484:** map station list  integration api, web and mobile ([828c5e9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/828c5e9871594d5a6ad6623348cfcaf4cebea8c2)), closes [#1484](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1484) [#484](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/484)
* **#1531 #1532:** adjustment register animal and pregnancy actions ([2d30fba](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2d30fba15fd95fcd4af012e26fdfdafba480d3e0)), closes [#1531](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1531) [#1532](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1532) [#499](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/499)
* **#1531 #1532:** create list animals, create and edit ([03bc1a2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/03bc1a23a48639d11f58f59faa7868a05a34984f)), closes [#1531](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1531) [#1532](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1532) [#499](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/499)
* **#1531 #1532:** rebase with develop ([5ca5628](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/5ca56284767d383497f3f75059af87b466694c5b)), closes [#1531](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1531) [#1532](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1532) [#499](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/499)
* **#1601:** adjustment weather stations and validation show stations ([6d17d60](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/6d17d60b76cee650c066156cd3568e92a6331e4f)), closes [#1601](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1601) [#510](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/510)
* **#1630 #1631:** create filter pregnancy diagnostic mobile ([07e46cd](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/07e46cdeb592363bbfd708db6ac94d79ab3ab979)), closes [#1630](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1630) [#1631](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1631) [#521](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/521)
* **#1630 #1631:** create list pregnancy diagnostics ([d46d5d7](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d46d5d767b1ca9fd6cfb48775ebf9a10ce55a274)), closes [#1630](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1630) [#1631](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1631) [#521](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/521)
* **#1630 #1631:** create register pregnancy diagnostic mobile ([25e5733](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/25e57334d132724a4ad8ca1dd9d96550bbb4b745)), closes [#1630](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1630) [#1631](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1631) [#521](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/521)
* **#1649:** create new message prop in loading modal ([e5c231e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e5c231e75ecc6f2f4a2ba3f4f8e48e8d12085141)), closes [#1649](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1649)
* **#1675 #1676:** adjustment mobile, web, and add new feature ([94afa5b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/94afa5bafdb99f6de2d5f704b875d560ef384be3)), closes [#1675](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1675) [#1676](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1676) [#546](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/546)
* **docuemntos#1558:** ajustes visual datas ([d563dd4](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d563dd4971e826303f748cc9371207b27d89a6d9)), closes [docuemntos#1558](http://gitlab.meta.com.br/docuemntos/issues/1558)
* **documentos#1325:** created pev crud for mobile ([f0ef8c0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f0ef8c00ffd2a45ad9b3a34ce5fee71fc1eced50)), closes [documentos#1325](http://gitlab.meta.com.br/documentos/issues/1325)
* **documentos#1490:** wip LotList ([ebcbf71](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ebcbf71bdfaa03e8f18c82bb005479bba921932d)), closes [documentos#1490](http://gitlab.meta.com.br/documentos/issues/1490)
* **documentos#1495:** lint fixes ([541b231](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/541b2314942c268a0b058bfe5c5e565adce51d4b)), closes [documentos#1495](http://gitlab.meta.com.br/documentos/issues/1495)
* **documentos#1495:** lots in mobile ([3fdcfbe](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3fdcfbeb9b601a1672a8d10cb1dabfdbccec2022)), closes [documentos#1495](http://gitlab.meta.com.br/documentos/issues/1495)
* **documentos#1495:** style tweaks ([343d541](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/343d541b3945004fa43e7136f28e5476913511e6)), closes [documentos#1495](http://gitlab.meta.com.br/documentos/issues/1495)
* **documentos#1547:** added new technical visualization validation ([0eca514](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0eca5149d7092ccd7c3f82146fd9d2fda54c4722)), closes [documentos#1547](http://gitlab.meta.com.br/documentos/issues/1547)
* **documentos#1579:** adds verticaldots ([bbad462](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/bbad462cc8b7e408eb01d5c4984d391a4b6b8b2a)), closes [documentos#1579](http://gitlab.meta.com.br/documentos/issues/1579)
* **documentos#1579:** bugfixes ([ac3d888](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ac3d88889c11a1a384c1d79bd0636dca547be8bc)), closes [documentos#1579](http://gitlab.meta.com.br/documentos/issues/1579)
* **documentos#1579:** changes input position ([010d148](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/010d1484a84e131399b66178dbf2105267b07f91)), closes [documentos#1579](http://gitlab.meta.com.br/documentos/issues/1579)
* **documentos#1579:** filter modal mobile ([833f4e9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/833f4e9e150117e9b6a949dc96d2539639f720bb)), closes [documentos#1579](http://gitlab.meta.com.br/documentos/issues/1579)
* **documentos#1579:** insemination mobile ([767d69b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/767d69b03b2e183e6a70f74b4bf648f21bedea84)), closes [documentos#1579](http://gitlab.meta.com.br/documentos/issues/1579)
* **documentos#1579:** lot changes ([f63dc6d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f63dc6dca603cdd367d6f6497f14e8abe3031ca4)), closes [documentos#1579](http://gitlab.meta.com.br/documentos/issues/1579)
* **documentos#1579:** mobile insemination ([1bf225d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1bf225d6f5ffbafb10226bf7c5c77b13a03ed58e)), closes [documentos#1579](http://gitlab.meta.com.br/documentos/issues/1579)
* **documentos#1579:** register calving in web ([2f1c94f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2f1c94f1e4936fcbc1662726eb25e216a1b54949)), closes [documentos#1579](http://gitlab.meta.com.br/documentos/issues/1579)
* **documentos#1650:** calving in mobile ([f1218c7](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f1218c7a11e6444eee6c4210889ba2eba91c1629)), closes [documentos#1650](http://gitlab.meta.com.br/documentos/issues/1650)
* **documentos#1650:** lint fixes ([fc5d7a8](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/fc5d7a8968c5dc2f03afcbd9c307872a6df20d70)), closes [documentos#1650](http://gitlab.meta.com.br/documentos/issues/1650)
* **documentos#1650:** register animalbirth screen ([5d093c8](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/5d093c823d050a358e0639a6f07bd62e8a70c2cd)), closes [documentos#1650](http://gitlab.meta.com.br/documentos/issues/1650)
* resolve [#1566](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1566) ([2fbc2ca](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2fbc2ca04a5a635ab63a491388503bbcb495cd1b))


### BREAKING CHANGES

* **ajustes tambo:** \







**Note:** Version bump only for package @smartcoop/mobile-containers





# [0.15.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.12.0-alpha.1...v0.15.0) (2021-03-23)


### Bug Fixes

* **#1185:** adjustment isvalidsize and quality photo camera ([e4b6202](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e4b6202e8d810ce30888e7a7930595ee5c649895)), closes [#1185](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1185)
* **#1217:** fix loading list fields ([ba533da](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ba533da79ed9712289376fc6e67132a71353cc6e))
* **#1280:** fix dispatch register milk quality ([2015255](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/20152551c1f50561a6b41777111f8f9ab21b9072))
* **#1341 #1366:** fix show pest report in field detail screen ([03148d0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/03148d00998fd978b633edef94425e9d75e4957a))
* **#1406:** hidden button register cropmanagement ([d1398b1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d1398b1b3ca387206c973de1751286dafa672cbc)), closes [#1406](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1406) [#452](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/452)
* **#1461:** calling function ([e070f5c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e070f5c306e55064b86537f3fc3d4c7196023a2b)), closes [#1461](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1461)
* **#1461:** validation fields milk delivery form ([d6df4f2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d6df4f2f52d89c06eed86f36fb70ed7ee4db3dec)), closes [#1461](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1461)
* **#1485:** fix z-index buttons edit/delete field ([392ff56](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/392ff56eac6d5bf2b770a613973fa9bd466c4ec0)), closes [#498](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/498)
* **graphs update:** graphs update ([4fd1adc](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4fd1adc06320b82633523915627073f0a5c58d1b))
* fixed field list refresh ([b5152b7](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b5152b79d10db5fb7c90f48818f159bcfe4e93fe))
* **#1280:** fix register milk quality mobile ([79e8429](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/79e8429ecfdf191d8f66e97004c844a6bbb76b2d))
* **#1280:** remove code never user ([e80746d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e80746d48decc3f901c0d347c9b9e45fde4c2814)), closes [#1280](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1280)
* **#1311:** fix change property modal ([0b5d5d9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0b5d5d97a29f47cdd82f475eae3c0d59d5fd4f21)), closes [#400](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/400)
* **#1312:** remove rain record button in field detail mobile ([1e3ac1c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1e3ac1cd09d7b7de3c2b31bb5879f7aae06b5886)), closes [#1312](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1312)
* **#1331:** fix open field like technical ([f6f2806](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f6f2806822f42e8e4a4e3028cabe612691dc9e6d))
* **#1370:** adjustment default value in register milk delivery ([9846cde](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9846cde351f7334cb7b3814fd3fdcf8565b89d82)), closes [#1370](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1370)
* **#1373:** set loading false when delete crop management ([14c6510](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/14c6510349321b25f1f7f0ea7dd414c6d5c83a14)), closes [#1373](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1373) [#429](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/429)
* **#1476:** show correct terms when technical ([7f3e258](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7f3e2589db293cb4546066922e38bed81482b9a1)), closes [#1476](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1476) [#477](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/477)
* **auto merging:** auto merging ([16d2158](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/16d21584dee6b2855e847b30d910d2c538684d1f))
* **click on screen when not loaded:** click on screen when not loaded ([90ebcea](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/90ebcea6f45e427367b7e0f0b22ff18e77c35133))
* **click twice on button:** click twice on button ([3cb8778](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3cb8778ad019e0ad4600116e0717615e4ba3d842))
* **documentos#1292:** fixed securitie movement item ([e343ae9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e343ae932b04d967685d0c41319e159990c5d507))
* **documentos#1328:** fixed property registration mobile map ([cdbfc66](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/cdbfc66c77e4289d53fb22ec6ef8328471f56c5c))
* **documentos#1441:** fixes quotation in mobile ([f3990d7](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f3990d7b573a514735be1282697a53e178771db9))
* **documentos#1445:** ajustes layout indicadores ([232c194](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/232c1943208214ef4317dabec8b70d391a1e08e1)), closes [documentos#1445](http://gitlab.meta.com.br/documentos/issues/1445)
* **documentos#1447:** ajustes mapa de chuva e relatorios ([9cd51a9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9cd51a9037291d437f5b847da66e8b7bf8a67998)), closes [documentos#1447](http://gitlab.meta.com.br/documentos/issues/1447)
* **documentos#1458:** fix typo in input select ([0235284](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/023528495cc2085e29f60d42737be0ee86ae19ce))
* **documentos#1459:** fixed barter package price ([360ce03](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/360ce03ce958f09f5683bc5d73ca5845d142b8ba))
* **documentos#1463:** ajustes dimensao mapa ([ce3a059](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ce3a05955ec6516f19f049b9c4eacaa360b23cf0)), closes [documentos#1463](http://gitlab.meta.com.br/documentos/issues/1463)
* **documentos#1501:** fixed field register navigation ([e0dd818](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e0dd8186e57c6ff5813d64a59cf05244700e6703))
* **update graph when property edit:** update graph when property edit ([e46940a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e46940a77f4dc2ee66babb06628a012f898aed9a))
* fixed input date label and props ([40c0431](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/40c04313374dea7a9508518d301ddfe186937cae))
* **add loader while organization is empty:** add loader ([8b92d3e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8b92d3eb15bb51db849a40ffe9386018059729f8))
* **field indicators layout:** field indicators layout ([dd6d6c7](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/dd6d6c734ef7d398b70042bf7dc5e391abc30dbc))
* **home:** set property as homescreen ([79a33c2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/79a33c282a8d289a91a2f4bbde0986733239b11e))
* **keyboard hiding inputs:** keyboard hiding inputs ([ca76f6e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ca76f6ebd147717813ba4ee336b955c600796059))
* **prevent click twice on button:** click twice button ([065cda2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/065cda2ec4912a9afdfa728a8e3de9a018a41c8a))
* fixed mobile order forms ([3f58d77](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3f58d778861f89f05551a3f17145f52cc0364ac0))
* **keyboard on address form:** keyboard on address form ([131eab8](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/131eab8b01685afd30b602cc752c1a5c5db0729d))


### Features

* **#1031:** add color when have filter active ([b1d1c3f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b1d1c3f0368c28509761a8987c260d1249b0e1ca)), closes [#1031](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1031) [#449](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/449)
* **#1182:** adjustment text pest report location and pin ([21c7da1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/21c7da14eb242f378890d6947cabbda961edff37)), closes [#1182](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1182) [#430](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/430)
* **#1185:** using camera for add new images in pest report ([4437125](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/44371252a7ad104654d5373fcef2f572814cf4e7)), closes [#1185](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1185)
* **#1221:** change report icon marker ([e11875f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e11875f51a8e396928442496842a1e38720487b6)), closes [#1221](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1221) [#444](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/444)
* **#1389:** change leave modal in empty property ([a747d84](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a747d8460e233bc1a39f67086ca8a86a9ca52be3)), closes [#1389](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1389) [#441](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/441)
* **#1414:** remove console log and adjustment editing property ([b83b5db](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b83b5dbbc6715b4ceb0ba8d90fd25d7904a4bca9)), closes [#1414](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1414) [#454](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/454)
* **documenots#1423:** adds useeffect ([5d1c192](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/5d1c19229a1c97e3ba149a6ccd2dbe5695dc8500)), closes [documenots#1423](http://gitlab.meta.com.br/documenots/issues/1423)
* **documentos#1292:** added filter reload ([852192c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/852192c9dc25d2d00042d51b8fe6196a04915d64)), closes [documentos#1292](http://gitlab.meta.com.br/documentos/issues/1292)
* **documentos#1292:** added filter to securities movement list screen ([64fa4af](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/64fa4af8b95417cb508469cdf613ff26a33f329c)), closes [documentos#1292](http://gitlab.meta.com.br/documentos/issues/1292)
* **documentos#1292:** integrated securities movement filter ([b5ca39a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b5ca39afd455fecbe30245a2497c4d446453650d)), closes [documentos#1292](http://gitlab.meta.com.br/documentos/issues/1292)
* **documentos#1305:** removes birthday from onboarding ([5eda681](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/5eda6815388bc7289d1d33d1e6896faca4e4aa68)), closes [documentos#1305](http://gitlab.meta.com.br/documentos/issues/1305)
* **documentos#1323:** fixes technician in web ([3ea9b64](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3ea9b6409460f5af500e67cccc1c50e11ab543b7))
* **documentos#1323:** lint ifxes ([1adaeb9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1adaeb9d03041af2d81434374f9f9f0cd700e6db)), closes [documentos#1323](http://gitlab.meta.com.br/documentos/issues/1323)
* **documentos#1323:** new icon + new login image background ([54cf57d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/54cf57d3c61ec328aba99391ea836eba09fce9dd)), closes [documentos#1323](http://gitlab.meta.com.br/documentos/issues/1323)
* **documentos#1323:** now acceptterm modal in mobile renders HTML ([3df0efc](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3df0efc569b88ecc21861769a164f8796a9c7c7f)), closes [documentos#1323](http://gitlab.meta.com.br/documentos/issues/1323)
* **documentos#1323:** removes console.log ([005a2c1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/005a2c1c08ac6cb7649419c54168a5b8d8c0f629)), closes [documentos#1323](http://gitlab.meta.com.br/documentos/issues/1323)
* **documentos#1332:** removes clg ([86a7569](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/86a75694cd95d753c62fbcf526d4e60084af1709)), closes [documentos#1332](http://gitlab.meta.com.br/documentos/issues/1332)
* **documentos#1357:** changes in field monitoration ([2508a09](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2508a09a73f04732609b124c0c08aeaa59bd7668)), closes [documentos#1357](http://gitlab.meta.com.br/documentos/issues/1357)
* **documentos#1357:** filters and field timeline in web ([2351d72](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2351d72c4e29ce32fcb6d45f2e8f7f1f63927ae2)), closes [documentos#1357](http://gitlab.meta.com.br/documentos/issues/1357)
* **documentos#1357:** fixes permission error ([caed226](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/caed22667aa0259d611894310d4afa266634c2d5))
* **documentos#1357:** modals ([1307ba9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1307ba9b9939cc649f3eb6a44f31bf70434d170b)), closes [documentos#1357](http://gitlab.meta.com.br/documentos/issues/1357)
* **documentos#1357:** removes clg ([8d7fc3c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8d7fc3ca07edcf60e94337637ca6aa094ddf186d)), closes [documentos#1357](http://gitlab.meta.com.br/documentos/issues/1357)
* **documentos#1357:** removes unnused var ([5afd808](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/5afd8083b3cab487cdd5ef48dafcddbd5dcb3db2)), closes [documentos#1357](http://gitlab.meta.com.br/documentos/issues/1357)
* **documentos#1357:** technical modal ([8808db1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8808db1072f9efafa56b3eec0ceb3868b21512cf)), closes [documentos#1357](http://gitlab.meta.com.br/documentos/issues/1357)
* **documentos#1374:** firebase changes ([df6a585](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/df6a585a34570777c468c98c134cd8fbd855601c)), closes [documentos#1374](http://gitlab.meta.com.br/documentos/issues/1374)
* **documentos#1400:** accept terms modal mobile ([7244e0d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7244e0d161d81982fbecaa13a08148f342a94527)), closes [documentos#1400](http://gitlab.meta.com.br/documentos/issues/1400)
* **documentos#1400:** style tweaks ([8485d10](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8485d108afa5099ea63ed59598082424cff5ba3d)), closes [documentos#1400](http://gitlab.meta.com.br/documentos/issues/1400)
* **documentos#1405:** changes in resetcodescreen ([d17b405](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d17b4054b52b1fbc095604fd73bcaaf089325da9)), closes [documentos#1405](http://gitlab.meta.com.br/documentos/issues/1405)
* **documentos#1405:** loaduser ([d2a8537](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d2a85372840f1cec1fc8081efb33bffc72ba578e)), closes [documentos#1405](http://gitlab.meta.com.br/documentos/issues/1405)
* **documentos#1409:** logic to disable button ([70efbf4](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/70efbf4c317e1bf0a9b58a643a34a7a14e3acb50)), closes [documentos#1409](http://gitlab.meta.com.br/documentos/issues/1409)
* **documentos#1423:** adds warning snackbar if mngmt isn conc. ([13f4553](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/13f455348b48a86f2648c3d876244e7bca22cecb)), closes [documentos#1423](http://gitlab.meta.com.br/documentos/issues/1423)
* **documentos#1436:** changes in commercialization ([f4edf6b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f4edf6b977ef4c7a8fe514f844097ab2703e1572)), closes [documentos#1436](http://gitlab.meta.com.br/documentos/issues/1436)
* **documentos#1437:** changes to make onboarding works without email ([f91732d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f91732d32c6437b9ca3c407c470238fa9507c76c)), closes [documentos#1437](http://gitlab.meta.com.br/documentos/issues/1437)
* **documentos#1438:** ajustes mapa ([97b7c63](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/97b7c638d12a5abd12cffe38cc78553d4457b6f8)), closes [documentos#1438](http://gitlab.meta.com.br/documentos/issues/1438)
* **documentos#1441:** barter fixes ([8575fa4](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8575fa4c8a98e6b61e167c14c4ef19e7a6a2f91b)), closes [documentos#1441](http://gitlab.meta.com.br/documentos/issues/1441)
* **documentos#1441:** field timeline chart ([a1bf429](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a1bf4292b9c7bf249e62efcba59aabbd681d79ca)), closes [documentos#1441](http://gitlab.meta.com.br/documentos/issues/1441)
* **documentos#1441:** lint ([4839a2b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4839a2be20eb8a0d51086b78991d44c71eb07f8a)), closes [documentos#1441](http://gitlab.meta.com.br/documentos/issues/1441)
* **documentos#1463:** ajustes finos mapa de chuva ([b25fc96](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b25fc9669fad05fb451c83ffb378bcd72038d3c5)), closes [documentos#1463](http://gitlab.meta.com.br/documentos/issues/1463)
* **documentos#1487:** added loader to machinery register screen ([fabf005](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/fabf005c6fae6ff382ef34be060df4623e462e76)), closes [documentos#1487](http://gitlab.meta.com.br/documentos/issues/1487)
* **documentos#1503:** added loader to field ([9fd282a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9fd282a115c39d954e2e0725226fe023b012d8fb)), closes [documentos#1503](http://gitlab.meta.com.br/documentos/issues/1503)
* changed barter price exibition ([88722b7](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/88722b7e41dceac3f418aedbf24e986b65de69e2))
* fixed tambo forms ([5e1990c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/5e1990c6fb377039bf80f06e47933fa7c490b7a4))
* resolve [#1439](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1439) ([3600986](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3600986a3d59851cbc0a4a75160eb6ab23497167))
* **documentos#1554:** alterada exibição de matricula ([a21246b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a21246b5122acd568b4b0168d61321bb485e65d8)), closes [documentos#1554](http://gitlab.meta.com.br/documentos/issues/1554)
* **doucmentos#1232:** shows all user modules in choosemodulescreen ([8405ea3](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8405ea36080c4bed643150f5632ec666fe0e51e5)), closes [doucmentos#1232](http://gitlab.meta.com.br/doucmentos/issues/1232)
* changed securities movement list filter param name ([d9d5fcc](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d9d5fcc204275b8f0b233c401a8ea11ddec0636a))
* **#1116:** link cards milk dashboard in mobile ([a52742b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a52742b959b64f4db45d3e6f72022b401f330267)), closes [#1116](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1116)
* **#1288:** adjustment select organization ([2dbbc9c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2dbbc9c4234ad1a408da856e1c2deeccc814f993)), closes [#1288](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1288) [#395](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/395)
* **documentos#1116:** adds indicators in dashbboard ([a5110e4](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a5110e4975ab40c4fee20f8d001b927d9615c678)), closes [documentos#1116](http://gitlab.meta.com.br/documentos/issues/1116)
* **documentos#1116:** chart card ([ec6da44](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ec6da446df3e0d09e1ca049ba3313e4e5a615bf5)), closes [documentos#1116](http://gitlab.meta.com.br/documentos/issues/1116)
* **documentos#1116:** chart card on mobile ([9048c80](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9048c8056cfb418569836d307dfa162a38152cb2)), closes [documentos#1116](http://gitlab.meta.com.br/documentos/issues/1116)
* **documentos#1116:** edit activities send to Create Property ([8008bb0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8008bb04b4a2427fb4284c5dc13de12dd9bfb102)), closes [documentos#1116](http://gitlab.meta.com.br/documentos/issues/1116)
* **documentos#1116:** lint fixexs ([88c18b2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/88c18b24b2cc021af41facd68bb1502df53d8c54)), closes [documentos#1116](http://gitlab.meta.com.br/documentos/issues/1116)
* **documentos#1116:** price chart ([1f7805d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1f7805d0509714b108e594591bf33ea59d12a692)), closes [documentos#1116](http://gitlab.meta.com.br/documentos/issues/1116)
* **documentos#1116:** quality chart ([47196f4](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/47196f487a10664e7a60d322e1616ec34df205de)), closes [documentos#1116](http://gitlab.meta.com.br/documentos/issues/1116)
* **documentos#1116:** removes console.log ([dd23fe6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/dd23fe698eddfcf4c1883988920e18c1a5f881af)), closes [documentos#1116](http://gitlab.meta.com.br/documentos/issues/1116)
* **documentos#1116:** tambo for mobile ([f4d650f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f4d650f18602702f4cfccdf32758336da2041ca0)), closes [documentos#1116](http://gitlab.meta.com.br/documentos/issues/1116)
* **documentos#1240:** address card fragment ([8829216](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8829216a6457b17382ae3457a328a44839ad1162)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** change signature modal in mobile ([aefa82a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/aefa82a23f9aaff77a8aba7d4aa643c22db85745)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** creates changesignaturemodal in web ([77eab34](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/77eab3453e87f5839cc64e0a0bbbdf02377c5b08)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** edit address form in mobile ([4ecfa57](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4ecfa5762c22d48faff3c5d4bba3abe8b2effc1a)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** edit address route ([547c709](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/547c709f5c982a36b557a153cbf266bec679db33)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** edit profile form in mobile ([cd71092](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/cd71092d86f0186644fa22d819040f35528220cf)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** edit profile screen ([7316eff](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7316eff0acaa631fa1d4fa12eba9741b38097238)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** init avatar input ([d018ec3](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d018ec371fb33f01a39e91f29bbc853227b6ff34)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** init change signature modal ([e2744a4](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e2744a4e804a998a1c22c67abe213c1f7b339592)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** lint ([123448f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/123448fef6cd259436ecf24bec2b50d0467077c7)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** list fragment in mobile ([f7b674e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f7b674ededa43bfb6bbd13ee2513e31cf05689f2)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** profile screen and addresscard fragment in mobile ([9b0c045](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9b0c04571437a84335178c6da80dd7a207c65dc7)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** removes log ([09066f1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/09066f1fb409cd3ef05210f39813c808788631b7)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** style tweaks ([861ea3e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/861ea3ef96a33c80d02370d8b230f056b4bd9387)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** updates service which select get orgs ([d8e67ce](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d8e67ce0dbaef707383a7a45f096247f361074ba)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** user additional info in mobile ([a450779](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a450779682ac6e103174ef5b7a072524f5b1322d)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** user info fragment in mobile ([310e058](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/310e0585dc6623572e229029aef79d32dff9a121)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1267:** added eletronic signature to barter ([3222712](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/32227120c5422d03f94ce9869ee58c5026e6abec)), closes [documentos#1267](http://gitlab.meta.com.br/documentos/issues/1267)
* **documentos#1267:** added new fields to barter list/create ([1cbbf4e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1cbbf4e36f780f717e7d1012d42294241b632ee7)), closes [documentos#1267](http://gitlab.meta.com.br/documentos/issues/1267)
* **documentos#1282:** added filter to barter products ([c42e044](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c42e04450907fcde3f76db7cf7b4bba77cbcd61e)), closes [documentos#1282](http://gitlab.meta.com.br/documentos/issues/1282)
* **documentos#1282:** added ie field to barter package join ([63b6be4](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/63b6be4de967544edeebb2305f87ccb4c67a0ce7)), closes [documentos#1282](http://gitlab.meta.com.br/documentos/issues/1282)
* **documentos#1282:** added IE field to sale order form ([d171dac](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d171dac06719c0a06505988e94322852c55ef538)), closes [documentos#1282](http://gitlab.meta.com.br/documentos/issues/1282)
* **documentos#1290:** accept term modal in mobile ([58cf9e5](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/58cf9e5bcb7f5ad6b0be8c332b2da9b893396fbf)), closes [documentos#1290](http://gitlab.meta.com.br/documentos/issues/1290)
* **documentos#1290:** adds use-term to accept modals ([1fbbe35](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1fbbe35bc9d57ead097a7ab594e63987a6cd3850)), closes [documentos#1290](http://gitlab.meta.com.br/documentos/issues/1290)
* **documentos#1290:** adds validation in useterm ([64a52aa](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/64a52aa54d4a2f03c47a887ece1295284b4d825b)), closes [documentos#1290](http://gitlab.meta.com.br/documentos/issues/1290)
* **documentos#1290:** changes in api call ([e6834d7](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e6834d78aec2923209c1de7d59f521b49486d62d)), closes [documentos#1290](http://gitlab.meta.com.br/documentos/issues/1290)
* **documentos#1290:** fixes ([c573fe6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c573fe6aeb22c90b95f4c84447a0d9eea8592a2f)), closes [documentos#1290](http://gitlab.meta.com.br/documentos/issues/1290)
* **documentos#1290:** removes mock onSuccess ([6086dee](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/6086dee40bcfcb9c21965942b5acb4a9f31d2373)), closes [documentos#1290](http://gitlab.meta.com.br/documentos/issues/1290)
* **documentos#1290:** tweaks to fix onboarding ([a7d1d3d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a7d1d3d0b5c4e28e49a2ddd938277ce92ee87569))
* **documentos#1303:** barter fixes ([173fc91](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/173fc91f32d548c88b49361e175eebeda60bb68a)), closes [documentos#1303](http://gitlab.meta.com.br/documentos/issues/1303)
* **documentos#1305:** added new columns to product withdraw list ([e6e2051](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e6e2051f788625b9e83a908466ec358405580057)), closes [documentos#1305](http://gitlab.meta.com.br/documentos/issues/1305)
* **documentos#1305:** changed tab name ([cbc6241](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/cbc624159fc1bb99b8e3b3a5524a8a36b582aab2)), closes [documentos#1305](http://gitlab.meta.com.br/documentos/issues/1305)
* **documentos#885:** changed field screen ([4299f89](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4299f898298c9641e4e63098dfdcf85a5633c11d)), closes [documentos#885](http://gitlab.meta.com.br/documentos/issues/885)
* **mobile:** changed menu order ([92fad8b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/92fad8b4ba42ecca452d8f0ab047553b6f2a3f10))


### BREAKING CHANGES

* **click twice on button:** [





# [0.15.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.12.0-alpha.1...v0.15.0) (2021-03-23)


### Bug Fixes

* **#1185:** adjustment isvalidsize and quality photo camera ([e4b6202](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e4b6202e8d810ce30888e7a7930595ee5c649895)), closes [#1185](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1185)
* **#1217:** fix loading list fields ([ba533da](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ba533da79ed9712289376fc6e67132a71353cc6e))
* **#1280:** fix dispatch register milk quality ([2015255](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/20152551c1f50561a6b41777111f8f9ab21b9072))
* **#1341 #1366:** fix show pest report in field detail screen ([03148d0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/03148d00998fd978b633edef94425e9d75e4957a))
* **#1406:** hidden button register cropmanagement ([d1398b1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d1398b1b3ca387206c973de1751286dafa672cbc)), closes [#1406](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1406) [#452](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/452)
* **#1461:** calling function ([e070f5c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e070f5c306e55064b86537f3fc3d4c7196023a2b)), closes [#1461](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1461)
* **#1461:** validation fields milk delivery form ([d6df4f2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d6df4f2f52d89c06eed86f36fb70ed7ee4db3dec)), closes [#1461](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1461)
* **#1485:** fix z-index buttons edit/delete field ([392ff56](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/392ff56eac6d5bf2b770a613973fa9bd466c4ec0)), closes [#498](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/498)
* **graphs update:** graphs update ([4fd1adc](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4fd1adc06320b82633523915627073f0a5c58d1b))
* fixed field list refresh ([b5152b7](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b5152b79d10db5fb7c90f48818f159bcfe4e93fe))
* **#1280:** fix register milk quality mobile ([79e8429](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/79e8429ecfdf191d8f66e97004c844a6bbb76b2d))
* **#1280:** remove code never user ([e80746d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e80746d48decc3f901c0d347c9b9e45fde4c2814)), closes [#1280](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1280)
* **#1311:** fix change property modal ([0b5d5d9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0b5d5d97a29f47cdd82f475eae3c0d59d5fd4f21)), closes [#400](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/400)
* **#1312:** remove rain record button in field detail mobile ([1e3ac1c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1e3ac1cd09d7b7de3c2b31bb5879f7aae06b5886)), closes [#1312](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1312)
* **#1331:** fix open field like technical ([f6f2806](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f6f2806822f42e8e4a4e3028cabe612691dc9e6d))
* **#1370:** adjustment default value in register milk delivery ([9846cde](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9846cde351f7334cb7b3814fd3fdcf8565b89d82)), closes [#1370](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1370)
* **#1373:** set loading false when delete crop management ([14c6510](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/14c6510349321b25f1f7f0ea7dd414c6d5c83a14)), closes [#1373](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1373) [#429](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/429)
* **#1476:** show correct terms when technical ([7f3e258](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7f3e2589db293cb4546066922e38bed81482b9a1)), closes [#1476](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1476) [#477](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/477)
* **auto merging:** auto merging ([16d2158](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/16d21584dee6b2855e847b30d910d2c538684d1f))
* **click on screen when not loaded:** click on screen when not loaded ([90ebcea](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/90ebcea6f45e427367b7e0f0b22ff18e77c35133))
* **click twice on button:** click twice on button ([3cb8778](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3cb8778ad019e0ad4600116e0717615e4ba3d842))
* **documentos#1292:** fixed securitie movement item ([e343ae9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e343ae932b04d967685d0c41319e159990c5d507))
* **documentos#1328:** fixed property registration mobile map ([cdbfc66](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/cdbfc66c77e4289d53fb22ec6ef8328471f56c5c))
* **documentos#1441:** fixes quotation in mobile ([f3990d7](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f3990d7b573a514735be1282697a53e178771db9))
* **documentos#1445:** ajustes layout indicadores ([232c194](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/232c1943208214ef4317dabec8b70d391a1e08e1)), closes [documentos#1445](http://gitlab.meta.com.br/documentos/issues/1445)
* **documentos#1447:** ajustes mapa de chuva e relatorios ([9cd51a9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9cd51a9037291d437f5b847da66e8b7bf8a67998)), closes [documentos#1447](http://gitlab.meta.com.br/documentos/issues/1447)
* **documentos#1458:** fix typo in input select ([0235284](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/023528495cc2085e29f60d42737be0ee86ae19ce))
* **documentos#1459:** fixed barter package price ([360ce03](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/360ce03ce958f09f5683bc5d73ca5845d142b8ba))
* **documentos#1463:** ajustes dimensao mapa ([ce3a059](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ce3a05955ec6516f19f049b9c4eacaa360b23cf0)), closes [documentos#1463](http://gitlab.meta.com.br/documentos/issues/1463)
* **documentos#1501:** fixed field register navigation ([e0dd818](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e0dd8186e57c6ff5813d64a59cf05244700e6703))
* **update graph when property edit:** update graph when property edit ([e46940a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e46940a77f4dc2ee66babb06628a012f898aed9a))
* fixed input date label and props ([40c0431](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/40c04313374dea7a9508518d301ddfe186937cae))
* **add loader while organization is empty:** add loader ([8b92d3e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8b92d3eb15bb51db849a40ffe9386018059729f8))
* **field indicators layout:** field indicators layout ([dd6d6c7](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/dd6d6c734ef7d398b70042bf7dc5e391abc30dbc))
* **home:** set property as homescreen ([79a33c2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/79a33c282a8d289a91a2f4bbde0986733239b11e))
* **keyboard hiding inputs:** keyboard hiding inputs ([ca76f6e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ca76f6ebd147717813ba4ee336b955c600796059))
* **prevent click twice on button:** click twice button ([065cda2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/065cda2ec4912a9afdfa728a8e3de9a018a41c8a))
* fixed mobile order forms ([3f58d77](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3f58d778861f89f05551a3f17145f52cc0364ac0))
* **keyboard on address form:** keyboard on address form ([131eab8](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/131eab8b01685afd30b602cc752c1a5c5db0729d))


### Features

* **#1031:** add color when have filter active ([b1d1c3f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b1d1c3f0368c28509761a8987c260d1249b0e1ca)), closes [#1031](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1031) [#449](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/449)
* **#1182:** adjustment text pest report location and pin ([21c7da1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/21c7da14eb242f378890d6947cabbda961edff37)), closes [#1182](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1182) [#430](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/430)
* **#1185:** using camera for add new images in pest report ([4437125](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/44371252a7ad104654d5373fcef2f572814cf4e7)), closes [#1185](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1185)
* **#1221:** change report icon marker ([e11875f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e11875f51a8e396928442496842a1e38720487b6)), closes [#1221](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1221) [#444](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/444)
* **#1389:** change leave modal in empty property ([a747d84](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a747d8460e233bc1a39f67086ca8a86a9ca52be3)), closes [#1389](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1389) [#441](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/441)
* **#1414:** remove console log and adjustment editing property ([b83b5db](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b83b5dbbc6715b4ceb0ba8d90fd25d7904a4bca9)), closes [#1414](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1414) [#454](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/454)
* **documenots#1423:** adds useeffect ([5d1c192](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/5d1c19229a1c97e3ba149a6ccd2dbe5695dc8500)), closes [documenots#1423](http://gitlab.meta.com.br/documenots/issues/1423)
* **documentos#1292:** added filter reload ([852192c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/852192c9dc25d2d00042d51b8fe6196a04915d64)), closes [documentos#1292](http://gitlab.meta.com.br/documentos/issues/1292)
* **documentos#1292:** added filter to securities movement list screen ([64fa4af](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/64fa4af8b95417cb508469cdf613ff26a33f329c)), closes [documentos#1292](http://gitlab.meta.com.br/documentos/issues/1292)
* **documentos#1292:** integrated securities movement filter ([b5ca39a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b5ca39afd455fecbe30245a2497c4d446453650d)), closes [documentos#1292](http://gitlab.meta.com.br/documentos/issues/1292)
* **documentos#1305:** removes birthday from onboarding ([5eda681](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/5eda6815388bc7289d1d33d1e6896faca4e4aa68)), closes [documentos#1305](http://gitlab.meta.com.br/documentos/issues/1305)
* **documentos#1323:** fixes technician in web ([3ea9b64](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3ea9b6409460f5af500e67cccc1c50e11ab543b7))
* **documentos#1323:** lint ifxes ([1adaeb9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1adaeb9d03041af2d81434374f9f9f0cd700e6db)), closes [documentos#1323](http://gitlab.meta.com.br/documentos/issues/1323)
* **documentos#1323:** new icon + new login image background ([54cf57d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/54cf57d3c61ec328aba99391ea836eba09fce9dd)), closes [documentos#1323](http://gitlab.meta.com.br/documentos/issues/1323)
* **documentos#1323:** now acceptterm modal in mobile renders HTML ([3df0efc](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3df0efc569b88ecc21861769a164f8796a9c7c7f)), closes [documentos#1323](http://gitlab.meta.com.br/documentos/issues/1323)
* **documentos#1323:** removes console.log ([005a2c1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/005a2c1c08ac6cb7649419c54168a5b8d8c0f629)), closes [documentos#1323](http://gitlab.meta.com.br/documentos/issues/1323)
* **documentos#1332:** removes clg ([86a7569](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/86a75694cd95d753c62fbcf526d4e60084af1709)), closes [documentos#1332](http://gitlab.meta.com.br/documentos/issues/1332)
* **documentos#1357:** changes in field monitoration ([2508a09](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2508a09a73f04732609b124c0c08aeaa59bd7668)), closes [documentos#1357](http://gitlab.meta.com.br/documentos/issues/1357)
* **documentos#1357:** filters and field timeline in web ([2351d72](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2351d72c4e29ce32fcb6d45f2e8f7f1f63927ae2)), closes [documentos#1357](http://gitlab.meta.com.br/documentos/issues/1357)
* **documentos#1357:** fixes permission error ([caed226](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/caed22667aa0259d611894310d4afa266634c2d5))
* **documentos#1357:** modals ([1307ba9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1307ba9b9939cc649f3eb6a44f31bf70434d170b)), closes [documentos#1357](http://gitlab.meta.com.br/documentos/issues/1357)
* **documentos#1357:** removes clg ([8d7fc3c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8d7fc3ca07edcf60e94337637ca6aa094ddf186d)), closes [documentos#1357](http://gitlab.meta.com.br/documentos/issues/1357)
* **documentos#1357:** removes unnused var ([5afd808](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/5afd8083b3cab487cdd5ef48dafcddbd5dcb3db2)), closes [documentos#1357](http://gitlab.meta.com.br/documentos/issues/1357)
* **documentos#1357:** technical modal ([8808db1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8808db1072f9efafa56b3eec0ceb3868b21512cf)), closes [documentos#1357](http://gitlab.meta.com.br/documentos/issues/1357)
* **documentos#1374:** firebase changes ([df6a585](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/df6a585a34570777c468c98c134cd8fbd855601c)), closes [documentos#1374](http://gitlab.meta.com.br/documentos/issues/1374)
* **documentos#1400:** accept terms modal mobile ([7244e0d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7244e0d161d81982fbecaa13a08148f342a94527)), closes [documentos#1400](http://gitlab.meta.com.br/documentos/issues/1400)
* **documentos#1400:** style tweaks ([8485d10](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8485d108afa5099ea63ed59598082424cff5ba3d)), closes [documentos#1400](http://gitlab.meta.com.br/documentos/issues/1400)
* **documentos#1405:** changes in resetcodescreen ([d17b405](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d17b4054b52b1fbc095604fd73bcaaf089325da9)), closes [documentos#1405](http://gitlab.meta.com.br/documentos/issues/1405)
* **documentos#1405:** loaduser ([d2a8537](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d2a85372840f1cec1fc8081efb33bffc72ba578e)), closes [documentos#1405](http://gitlab.meta.com.br/documentos/issues/1405)
* **documentos#1409:** logic to disable button ([70efbf4](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/70efbf4c317e1bf0a9b58a643a34a7a14e3acb50)), closes [documentos#1409](http://gitlab.meta.com.br/documentos/issues/1409)
* **documentos#1423:** adds warning snackbar if mngmt isn conc. ([13f4553](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/13f455348b48a86f2648c3d876244e7bca22cecb)), closes [documentos#1423](http://gitlab.meta.com.br/documentos/issues/1423)
* **documentos#1436:** changes in commercialization ([f4edf6b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f4edf6b977ef4c7a8fe514f844097ab2703e1572)), closes [documentos#1436](http://gitlab.meta.com.br/documentos/issues/1436)
* **documentos#1437:** changes to make onboarding works without email ([f91732d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f91732d32c6437b9ca3c407c470238fa9507c76c)), closes [documentos#1437](http://gitlab.meta.com.br/documentos/issues/1437)
* **documentos#1438:** ajustes mapa ([97b7c63](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/97b7c638d12a5abd12cffe38cc78553d4457b6f8)), closes [documentos#1438](http://gitlab.meta.com.br/documentos/issues/1438)
* **documentos#1441:** barter fixes ([8575fa4](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8575fa4c8a98e6b61e167c14c4ef19e7a6a2f91b)), closes [documentos#1441](http://gitlab.meta.com.br/documentos/issues/1441)
* **documentos#1441:** field timeline chart ([a1bf429](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a1bf4292b9c7bf249e62efcba59aabbd681d79ca)), closes [documentos#1441](http://gitlab.meta.com.br/documentos/issues/1441)
* **documentos#1441:** lint ([4839a2b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4839a2be20eb8a0d51086b78991d44c71eb07f8a)), closes [documentos#1441](http://gitlab.meta.com.br/documentos/issues/1441)
* **documentos#1463:** ajustes finos mapa de chuva ([b25fc96](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b25fc9669fad05fb451c83ffb378bcd72038d3c5)), closes [documentos#1463](http://gitlab.meta.com.br/documentos/issues/1463)
* **documentos#1487:** added loader to machinery register screen ([fabf005](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/fabf005c6fae6ff382ef34be060df4623e462e76)), closes [documentos#1487](http://gitlab.meta.com.br/documentos/issues/1487)
* **documentos#1503:** added loader to field ([9fd282a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9fd282a115c39d954e2e0725226fe023b012d8fb)), closes [documentos#1503](http://gitlab.meta.com.br/documentos/issues/1503)
* changed barter price exibition ([88722b7](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/88722b7e41dceac3f418aedbf24e986b65de69e2))
* fixed tambo forms ([5e1990c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/5e1990c6fb377039bf80f06e47933fa7c490b7a4))
* resolve [#1439](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1439) ([3600986](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3600986a3d59851cbc0a4a75160eb6ab23497167))
* **documentos#1554:** alterada exibição de matricula ([a21246b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a21246b5122acd568b4b0168d61321bb485e65d8)), closes [documentos#1554](http://gitlab.meta.com.br/documentos/issues/1554)
* **doucmentos#1232:** shows all user modules in choosemodulescreen ([8405ea3](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8405ea36080c4bed643150f5632ec666fe0e51e5)), closes [doucmentos#1232](http://gitlab.meta.com.br/doucmentos/issues/1232)
* changed securities movement list filter param name ([d9d5fcc](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d9d5fcc204275b8f0b233c401a8ea11ddec0636a))
* **#1116:** link cards milk dashboard in mobile ([a52742b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a52742b959b64f4db45d3e6f72022b401f330267)), closes [#1116](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1116)
* **#1288:** adjustment select organization ([2dbbc9c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2dbbc9c4234ad1a408da856e1c2deeccc814f993)), closes [#1288](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1288) [#395](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/395)
* **documentos#1116:** adds indicators in dashbboard ([a5110e4](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a5110e4975ab40c4fee20f8d001b927d9615c678)), closes [documentos#1116](http://gitlab.meta.com.br/documentos/issues/1116)
* **documentos#1116:** chart card ([ec6da44](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ec6da446df3e0d09e1ca049ba3313e4e5a615bf5)), closes [documentos#1116](http://gitlab.meta.com.br/documentos/issues/1116)
* **documentos#1116:** chart card on mobile ([9048c80](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9048c8056cfb418569836d307dfa162a38152cb2)), closes [documentos#1116](http://gitlab.meta.com.br/documentos/issues/1116)
* **documentos#1116:** edit activities send to Create Property ([8008bb0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8008bb04b4a2427fb4284c5dc13de12dd9bfb102)), closes [documentos#1116](http://gitlab.meta.com.br/documentos/issues/1116)
* **documentos#1116:** lint fixexs ([88c18b2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/88c18b24b2cc021af41facd68bb1502df53d8c54)), closes [documentos#1116](http://gitlab.meta.com.br/documentos/issues/1116)
* **documentos#1116:** price chart ([1f7805d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1f7805d0509714b108e594591bf33ea59d12a692)), closes [documentos#1116](http://gitlab.meta.com.br/documentos/issues/1116)
* **documentos#1116:** quality chart ([47196f4](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/47196f487a10664e7a60d322e1616ec34df205de)), closes [documentos#1116](http://gitlab.meta.com.br/documentos/issues/1116)
* **documentos#1116:** removes console.log ([dd23fe6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/dd23fe698eddfcf4c1883988920e18c1a5f881af)), closes [documentos#1116](http://gitlab.meta.com.br/documentos/issues/1116)
* **documentos#1116:** tambo for mobile ([f4d650f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f4d650f18602702f4cfccdf32758336da2041ca0)), closes [documentos#1116](http://gitlab.meta.com.br/documentos/issues/1116)
* **documentos#1240:** address card fragment ([8829216](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8829216a6457b17382ae3457a328a44839ad1162)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** change signature modal in mobile ([aefa82a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/aefa82a23f9aaff77a8aba7d4aa643c22db85745)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** creates changesignaturemodal in web ([77eab34](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/77eab3453e87f5839cc64e0a0bbbdf02377c5b08)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** edit address form in mobile ([4ecfa57](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4ecfa5762c22d48faff3c5d4bba3abe8b2effc1a)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** edit address route ([547c709](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/547c709f5c982a36b557a153cbf266bec679db33)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** edit profile form in mobile ([cd71092](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/cd71092d86f0186644fa22d819040f35528220cf)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** edit profile screen ([7316eff](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7316eff0acaa631fa1d4fa12eba9741b38097238)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** init avatar input ([d018ec3](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d018ec371fb33f01a39e91f29bbc853227b6ff34)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** init change signature modal ([e2744a4](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e2744a4e804a998a1c22c67abe213c1f7b339592)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** lint ([123448f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/123448fef6cd259436ecf24bec2b50d0467077c7)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** list fragment in mobile ([f7b674e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f7b674ededa43bfb6bbd13ee2513e31cf05689f2)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** profile screen and addresscard fragment in mobile ([9b0c045](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9b0c04571437a84335178c6da80dd7a207c65dc7)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** removes log ([09066f1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/09066f1fb409cd3ef05210f39813c808788631b7)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** style tweaks ([861ea3e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/861ea3ef96a33c80d02370d8b230f056b4bd9387)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** updates service which select get orgs ([d8e67ce](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d8e67ce0dbaef707383a7a45f096247f361074ba)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** user additional info in mobile ([a450779](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a450779682ac6e103174ef5b7a072524f5b1322d)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** user info fragment in mobile ([310e058](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/310e0585dc6623572e229029aef79d32dff9a121)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1267:** added eletronic signature to barter ([3222712](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/32227120c5422d03f94ce9869ee58c5026e6abec)), closes [documentos#1267](http://gitlab.meta.com.br/documentos/issues/1267)
* **documentos#1267:** added new fields to barter list/create ([1cbbf4e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1cbbf4e36f780f717e7d1012d42294241b632ee7)), closes [documentos#1267](http://gitlab.meta.com.br/documentos/issues/1267)
* **documentos#1282:** added filter to barter products ([c42e044](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c42e04450907fcde3f76db7cf7b4bba77cbcd61e)), closes [documentos#1282](http://gitlab.meta.com.br/documentos/issues/1282)
* **documentos#1282:** added ie field to barter package join ([63b6be4](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/63b6be4de967544edeebb2305f87ccb4c67a0ce7)), closes [documentos#1282](http://gitlab.meta.com.br/documentos/issues/1282)
* **documentos#1282:** added IE field to sale order form ([d171dac](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d171dac06719c0a06505988e94322852c55ef538)), closes [documentos#1282](http://gitlab.meta.com.br/documentos/issues/1282)
* **documentos#1290:** accept term modal in mobile ([58cf9e5](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/58cf9e5bcb7f5ad6b0be8c332b2da9b893396fbf)), closes [documentos#1290](http://gitlab.meta.com.br/documentos/issues/1290)
* **documentos#1290:** adds use-term to accept modals ([1fbbe35](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1fbbe35bc9d57ead097a7ab594e63987a6cd3850)), closes [documentos#1290](http://gitlab.meta.com.br/documentos/issues/1290)
* **documentos#1290:** adds validation in useterm ([64a52aa](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/64a52aa54d4a2f03c47a887ece1295284b4d825b)), closes [documentos#1290](http://gitlab.meta.com.br/documentos/issues/1290)
* **documentos#1290:** changes in api call ([e6834d7](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e6834d78aec2923209c1de7d59f521b49486d62d)), closes [documentos#1290](http://gitlab.meta.com.br/documentos/issues/1290)
* **documentos#1290:** fixes ([c573fe6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c573fe6aeb22c90b95f4c84447a0d9eea8592a2f)), closes [documentos#1290](http://gitlab.meta.com.br/documentos/issues/1290)
* **documentos#1290:** removes mock onSuccess ([6086dee](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/6086dee40bcfcb9c21965942b5acb4a9f31d2373)), closes [documentos#1290](http://gitlab.meta.com.br/documentos/issues/1290)
* **documentos#1290:** tweaks to fix onboarding ([a7d1d3d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a7d1d3d0b5c4e28e49a2ddd938277ce92ee87569))
* **documentos#1303:** barter fixes ([173fc91](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/173fc91f32d548c88b49361e175eebeda60bb68a)), closes [documentos#1303](http://gitlab.meta.com.br/documentos/issues/1303)
* **documentos#1305:** added new columns to product withdraw list ([e6e2051](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e6e2051f788625b9e83a908466ec358405580057)), closes [documentos#1305](http://gitlab.meta.com.br/documentos/issues/1305)
* **documentos#1305:** changed tab name ([cbc6241](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/cbc624159fc1bb99b8e3b3a5524a8a36b582aab2)), closes [documentos#1305](http://gitlab.meta.com.br/documentos/issues/1305)
* **documentos#885:** changed field screen ([4299f89](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4299f898298c9641e4e63098dfdcf85a5633c11d)), closes [documentos#885](http://gitlab.meta.com.br/documentos/issues/885)
* **mobile:** changed menu order ([92fad8b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/92fad8b4ba42ecca452d8f0ab047553b6f2a3f10))


### BREAKING CHANGES

* **click twice on button:** [





# [0.14.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.12.0-alpha.1...v0.14.0) (2021-03-23)


### Bug Fixes

* **#1185:** adjustment isvalidsize and quality photo camera ([e4b6202](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e4b6202e8d810ce30888e7a7930595ee5c649895)), closes [#1185](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1185)
* **#1217:** fix loading list fields ([ba533da](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ba533da79ed9712289376fc6e67132a71353cc6e))
* **#1280:** fix dispatch register milk quality ([2015255](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/20152551c1f50561a6b41777111f8f9ab21b9072))
* **#1341 #1366:** fix show pest report in field detail screen ([03148d0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/03148d00998fd978b633edef94425e9d75e4957a))
* **#1406:** hidden button register cropmanagement ([d1398b1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d1398b1b3ca387206c973de1751286dafa672cbc)), closes [#1406](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1406) [#452](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/452)
* **#1461:** calling function ([e070f5c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e070f5c306e55064b86537f3fc3d4c7196023a2b)), closes [#1461](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1461)
* **#1461:** validation fields milk delivery form ([d6df4f2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d6df4f2f52d89c06eed86f36fb70ed7ee4db3dec)), closes [#1461](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1461)
* **#1485:** fix z-index buttons edit/delete field ([392ff56](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/392ff56eac6d5bf2b770a613973fa9bd466c4ec0)), closes [#498](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/498)
* **graphs update:** graphs update ([4fd1adc](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4fd1adc06320b82633523915627073f0a5c58d1b))
* fixed field list refresh ([b5152b7](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b5152b79d10db5fb7c90f48818f159bcfe4e93fe))
* **#1280:** fix register milk quality mobile ([79e8429](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/79e8429ecfdf191d8f66e97004c844a6bbb76b2d))
* **#1280:** remove code never user ([e80746d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e80746d48decc3f901c0d347c9b9e45fde4c2814)), closes [#1280](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1280)
* **#1311:** fix change property modal ([0b5d5d9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0b5d5d97a29f47cdd82f475eae3c0d59d5fd4f21)), closes [#400](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/400)
* **#1312:** remove rain record button in field detail mobile ([1e3ac1c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1e3ac1cd09d7b7de3c2b31bb5879f7aae06b5886)), closes [#1312](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1312)
* **#1331:** fix open field like technical ([f6f2806](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f6f2806822f42e8e4a4e3028cabe612691dc9e6d))
* **#1370:** adjustment default value in register milk delivery ([9846cde](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9846cde351f7334cb7b3814fd3fdcf8565b89d82)), closes [#1370](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1370)
* **#1373:** set loading false when delete crop management ([14c6510](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/14c6510349321b25f1f7f0ea7dd414c6d5c83a14)), closes [#1373](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1373) [#429](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/429)
* **#1476:** show correct terms when technical ([7f3e258](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7f3e2589db293cb4546066922e38bed81482b9a1)), closes [#1476](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1476) [#477](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/477)
* **auto merging:** auto merging ([16d2158](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/16d21584dee6b2855e847b30d910d2c538684d1f))
* **click on screen when not loaded:** click on screen when not loaded ([90ebcea](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/90ebcea6f45e427367b7e0f0b22ff18e77c35133))
* **click twice on button:** click twice on button ([3cb8778](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3cb8778ad019e0ad4600116e0717615e4ba3d842))
* **documentos#1292:** fixed securitie movement item ([e343ae9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e343ae932b04d967685d0c41319e159990c5d507))
* **documentos#1328:** fixed property registration mobile map ([cdbfc66](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/cdbfc66c77e4289d53fb22ec6ef8328471f56c5c))
* **documentos#1441:** fixes quotation in mobile ([f3990d7](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f3990d7b573a514735be1282697a53e178771db9))
* **documentos#1445:** ajustes layout indicadores ([232c194](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/232c1943208214ef4317dabec8b70d391a1e08e1)), closes [documentos#1445](http://gitlab.meta.com.br/documentos/issues/1445)
* **documentos#1447:** ajustes mapa de chuva e relatorios ([9cd51a9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9cd51a9037291d437f5b847da66e8b7bf8a67998)), closes [documentos#1447](http://gitlab.meta.com.br/documentos/issues/1447)
* **documentos#1458:** fix typo in input select ([0235284](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/023528495cc2085e29f60d42737be0ee86ae19ce))
* **documentos#1459:** fixed barter package price ([360ce03](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/360ce03ce958f09f5683bc5d73ca5845d142b8ba))
* **documentos#1463:** ajustes dimensao mapa ([ce3a059](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ce3a05955ec6516f19f049b9c4eacaa360b23cf0)), closes [documentos#1463](http://gitlab.meta.com.br/documentos/issues/1463)
* **documentos#1501:** fixed field register navigation ([e0dd818](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e0dd8186e57c6ff5813d64a59cf05244700e6703))
* **update graph when property edit:** update graph when property edit ([e46940a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e46940a77f4dc2ee66babb06628a012f898aed9a))
* fixed input date label and props ([40c0431](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/40c04313374dea7a9508518d301ddfe186937cae))
* **add loader while organization is empty:** add loader ([8b92d3e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8b92d3eb15bb51db849a40ffe9386018059729f8))
* **field indicators layout:** field indicators layout ([dd6d6c7](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/dd6d6c734ef7d398b70042bf7dc5e391abc30dbc))
* **home:** set property as homescreen ([79a33c2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/79a33c282a8d289a91a2f4bbde0986733239b11e))
* **keyboard hiding inputs:** keyboard hiding inputs ([ca76f6e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ca76f6ebd147717813ba4ee336b955c600796059))
* **prevent click twice on button:** click twice button ([065cda2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/065cda2ec4912a9afdfa728a8e3de9a018a41c8a))
* fixed mobile order forms ([3f58d77](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3f58d778861f89f05551a3f17145f52cc0364ac0))
* **keyboard on address form:** keyboard on address form ([131eab8](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/131eab8b01685afd30b602cc752c1a5c5db0729d))


### Features

* **#1031:** add color when have filter active ([b1d1c3f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b1d1c3f0368c28509761a8987c260d1249b0e1ca)), closes [#1031](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1031) [#449](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/449)
* **#1182:** adjustment text pest report location and pin ([21c7da1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/21c7da14eb242f378890d6947cabbda961edff37)), closes [#1182](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1182) [#430](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/430)
* **#1185:** using camera for add new images in pest report ([4437125](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/44371252a7ad104654d5373fcef2f572814cf4e7)), closes [#1185](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1185)
* **#1221:** change report icon marker ([e11875f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e11875f51a8e396928442496842a1e38720487b6)), closes [#1221](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1221) [#444](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/444)
* **#1389:** change leave modal in empty property ([a747d84](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a747d8460e233bc1a39f67086ca8a86a9ca52be3)), closes [#1389](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1389) [#441](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/441)
* **#1414:** remove console log and adjustment editing property ([b83b5db](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b83b5dbbc6715b4ceb0ba8d90fd25d7904a4bca9)), closes [#1414](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1414) [#454](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/454)
* **documenots#1423:** adds useeffect ([5d1c192](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/5d1c19229a1c97e3ba149a6ccd2dbe5695dc8500)), closes [documenots#1423](http://gitlab.meta.com.br/documenots/issues/1423)
* **documentos#1292:** added filter reload ([852192c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/852192c9dc25d2d00042d51b8fe6196a04915d64)), closes [documentos#1292](http://gitlab.meta.com.br/documentos/issues/1292)
* **documentos#1292:** added filter to securities movement list screen ([64fa4af](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/64fa4af8b95417cb508469cdf613ff26a33f329c)), closes [documentos#1292](http://gitlab.meta.com.br/documentos/issues/1292)
* **documentos#1292:** integrated securities movement filter ([b5ca39a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b5ca39afd455fecbe30245a2497c4d446453650d)), closes [documentos#1292](http://gitlab.meta.com.br/documentos/issues/1292)
* **documentos#1305:** removes birthday from onboarding ([5eda681](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/5eda6815388bc7289d1d33d1e6896faca4e4aa68)), closes [documentos#1305](http://gitlab.meta.com.br/documentos/issues/1305)
* **documentos#1323:** fixes technician in web ([3ea9b64](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3ea9b6409460f5af500e67cccc1c50e11ab543b7))
* **documentos#1323:** lint ifxes ([1adaeb9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1adaeb9d03041af2d81434374f9f9f0cd700e6db)), closes [documentos#1323](http://gitlab.meta.com.br/documentos/issues/1323)
* **documentos#1323:** new icon + new login image background ([54cf57d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/54cf57d3c61ec328aba99391ea836eba09fce9dd)), closes [documentos#1323](http://gitlab.meta.com.br/documentos/issues/1323)
* **documentos#1323:** now acceptterm modal in mobile renders HTML ([3df0efc](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3df0efc569b88ecc21861769a164f8796a9c7c7f)), closes [documentos#1323](http://gitlab.meta.com.br/documentos/issues/1323)
* **documentos#1323:** removes console.log ([005a2c1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/005a2c1c08ac6cb7649419c54168a5b8d8c0f629)), closes [documentos#1323](http://gitlab.meta.com.br/documentos/issues/1323)
* **documentos#1332:** removes clg ([86a7569](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/86a75694cd95d753c62fbcf526d4e60084af1709)), closes [documentos#1332](http://gitlab.meta.com.br/documentos/issues/1332)
* **documentos#1357:** changes in field monitoration ([2508a09](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2508a09a73f04732609b124c0c08aeaa59bd7668)), closes [documentos#1357](http://gitlab.meta.com.br/documentos/issues/1357)
* **documentos#1357:** filters and field timeline in web ([2351d72](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2351d72c4e29ce32fcb6d45f2e8f7f1f63927ae2)), closes [documentos#1357](http://gitlab.meta.com.br/documentos/issues/1357)
* **documentos#1357:** fixes permission error ([caed226](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/caed22667aa0259d611894310d4afa266634c2d5))
* **documentos#1357:** modals ([1307ba9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1307ba9b9939cc649f3eb6a44f31bf70434d170b)), closes [documentos#1357](http://gitlab.meta.com.br/documentos/issues/1357)
* **documentos#1357:** removes clg ([8d7fc3c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8d7fc3ca07edcf60e94337637ca6aa094ddf186d)), closes [documentos#1357](http://gitlab.meta.com.br/documentos/issues/1357)
* **documentos#1357:** removes unnused var ([5afd808](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/5afd8083b3cab487cdd5ef48dafcddbd5dcb3db2)), closes [documentos#1357](http://gitlab.meta.com.br/documentos/issues/1357)
* **documentos#1357:** technical modal ([8808db1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8808db1072f9efafa56b3eec0ceb3868b21512cf)), closes [documentos#1357](http://gitlab.meta.com.br/documentos/issues/1357)
* **documentos#1374:** firebase changes ([df6a585](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/df6a585a34570777c468c98c134cd8fbd855601c)), closes [documentos#1374](http://gitlab.meta.com.br/documentos/issues/1374)
* **documentos#1400:** accept terms modal mobile ([7244e0d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7244e0d161d81982fbecaa13a08148f342a94527)), closes [documentos#1400](http://gitlab.meta.com.br/documentos/issues/1400)
* **documentos#1400:** style tweaks ([8485d10](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8485d108afa5099ea63ed59598082424cff5ba3d)), closes [documentos#1400](http://gitlab.meta.com.br/documentos/issues/1400)
* **documentos#1405:** changes in resetcodescreen ([d17b405](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d17b4054b52b1fbc095604fd73bcaaf089325da9)), closes [documentos#1405](http://gitlab.meta.com.br/documentos/issues/1405)
* **documentos#1405:** loaduser ([d2a8537](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d2a85372840f1cec1fc8081efb33bffc72ba578e)), closes [documentos#1405](http://gitlab.meta.com.br/documentos/issues/1405)
* **documentos#1409:** logic to disable button ([70efbf4](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/70efbf4c317e1bf0a9b58a643a34a7a14e3acb50)), closes [documentos#1409](http://gitlab.meta.com.br/documentos/issues/1409)
* **documentos#1423:** adds warning snackbar if mngmt isn conc. ([13f4553](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/13f455348b48a86f2648c3d876244e7bca22cecb)), closes [documentos#1423](http://gitlab.meta.com.br/documentos/issues/1423)
* **documentos#1436:** changes in commercialization ([f4edf6b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f4edf6b977ef4c7a8fe514f844097ab2703e1572)), closes [documentos#1436](http://gitlab.meta.com.br/documentos/issues/1436)
* **documentos#1437:** changes to make onboarding works without email ([f91732d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f91732d32c6437b9ca3c407c470238fa9507c76c)), closes [documentos#1437](http://gitlab.meta.com.br/documentos/issues/1437)
* **documentos#1438:** ajustes mapa ([97b7c63](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/97b7c638d12a5abd12cffe38cc78553d4457b6f8)), closes [documentos#1438](http://gitlab.meta.com.br/documentos/issues/1438)
* **documentos#1441:** barter fixes ([8575fa4](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8575fa4c8a98e6b61e167c14c4ef19e7a6a2f91b)), closes [documentos#1441](http://gitlab.meta.com.br/documentos/issues/1441)
* **documentos#1441:** field timeline chart ([a1bf429](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a1bf4292b9c7bf249e62efcba59aabbd681d79ca)), closes [documentos#1441](http://gitlab.meta.com.br/documentos/issues/1441)
* **documentos#1441:** lint ([4839a2b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4839a2be20eb8a0d51086b78991d44c71eb07f8a)), closes [documentos#1441](http://gitlab.meta.com.br/documentos/issues/1441)
* **documentos#1463:** ajustes finos mapa de chuva ([b25fc96](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b25fc9669fad05fb451c83ffb378bcd72038d3c5)), closes [documentos#1463](http://gitlab.meta.com.br/documentos/issues/1463)
* **documentos#1487:** added loader to machinery register screen ([fabf005](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/fabf005c6fae6ff382ef34be060df4623e462e76)), closes [documentos#1487](http://gitlab.meta.com.br/documentos/issues/1487)
* **documentos#1503:** added loader to field ([9fd282a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9fd282a115c39d954e2e0725226fe023b012d8fb)), closes [documentos#1503](http://gitlab.meta.com.br/documentos/issues/1503)
* changed barter price exibition ([88722b7](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/88722b7e41dceac3f418aedbf24e986b65de69e2))
* fixed tambo forms ([5e1990c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/5e1990c6fb377039bf80f06e47933fa7c490b7a4))
* resolve [#1439](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1439) ([3600986](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3600986a3d59851cbc0a4a75160eb6ab23497167))
* **documentos#1554:** alterada exibição de matricula ([a21246b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a21246b5122acd568b4b0168d61321bb485e65d8)), closes [documentos#1554](http://gitlab.meta.com.br/documentos/issues/1554)
* **doucmentos#1232:** shows all user modules in choosemodulescreen ([8405ea3](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8405ea36080c4bed643150f5632ec666fe0e51e5)), closes [doucmentos#1232](http://gitlab.meta.com.br/doucmentos/issues/1232)
* changed securities movement list filter param name ([d9d5fcc](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d9d5fcc204275b8f0b233c401a8ea11ddec0636a))
* **#1116:** link cards milk dashboard in mobile ([a52742b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a52742b959b64f4db45d3e6f72022b401f330267)), closes [#1116](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1116)
* **#1288:** adjustment select organization ([2dbbc9c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2dbbc9c4234ad1a408da856e1c2deeccc814f993)), closes [#1288](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1288) [#395](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/395)
* **documentos#1116:** adds indicators in dashbboard ([a5110e4](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a5110e4975ab40c4fee20f8d001b927d9615c678)), closes [documentos#1116](http://gitlab.meta.com.br/documentos/issues/1116)
* **documentos#1116:** chart card ([ec6da44](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ec6da446df3e0d09e1ca049ba3313e4e5a615bf5)), closes [documentos#1116](http://gitlab.meta.com.br/documentos/issues/1116)
* **documentos#1116:** chart card on mobile ([9048c80](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9048c8056cfb418569836d307dfa162a38152cb2)), closes [documentos#1116](http://gitlab.meta.com.br/documentos/issues/1116)
* **documentos#1116:** edit activities send to Create Property ([8008bb0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8008bb04b4a2427fb4284c5dc13de12dd9bfb102)), closes [documentos#1116](http://gitlab.meta.com.br/documentos/issues/1116)
* **documentos#1116:** lint fixexs ([88c18b2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/88c18b24b2cc021af41facd68bb1502df53d8c54)), closes [documentos#1116](http://gitlab.meta.com.br/documentos/issues/1116)
* **documentos#1116:** price chart ([1f7805d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1f7805d0509714b108e594591bf33ea59d12a692)), closes [documentos#1116](http://gitlab.meta.com.br/documentos/issues/1116)
* **documentos#1116:** quality chart ([47196f4](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/47196f487a10664e7a60d322e1616ec34df205de)), closes [documentos#1116](http://gitlab.meta.com.br/documentos/issues/1116)
* **documentos#1116:** removes console.log ([dd23fe6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/dd23fe698eddfcf4c1883988920e18c1a5f881af)), closes [documentos#1116](http://gitlab.meta.com.br/documentos/issues/1116)
* **documentos#1116:** tambo for mobile ([f4d650f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f4d650f18602702f4cfccdf32758336da2041ca0)), closes [documentos#1116](http://gitlab.meta.com.br/documentos/issues/1116)
* **documentos#1240:** address card fragment ([8829216](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8829216a6457b17382ae3457a328a44839ad1162)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** change signature modal in mobile ([aefa82a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/aefa82a23f9aaff77a8aba7d4aa643c22db85745)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** creates changesignaturemodal in web ([77eab34](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/77eab3453e87f5839cc64e0a0bbbdf02377c5b08)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** edit address form in mobile ([4ecfa57](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4ecfa5762c22d48faff3c5d4bba3abe8b2effc1a)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** edit address route ([547c709](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/547c709f5c982a36b557a153cbf266bec679db33)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** edit profile form in mobile ([cd71092](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/cd71092d86f0186644fa22d819040f35528220cf)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** edit profile screen ([7316eff](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7316eff0acaa631fa1d4fa12eba9741b38097238)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** init avatar input ([d018ec3](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d018ec371fb33f01a39e91f29bbc853227b6ff34)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** init change signature modal ([e2744a4](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e2744a4e804a998a1c22c67abe213c1f7b339592)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** lint ([123448f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/123448fef6cd259436ecf24bec2b50d0467077c7)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** list fragment in mobile ([f7b674e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f7b674ededa43bfb6bbd13ee2513e31cf05689f2)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** profile screen and addresscard fragment in mobile ([9b0c045](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9b0c04571437a84335178c6da80dd7a207c65dc7)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** removes log ([09066f1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/09066f1fb409cd3ef05210f39813c808788631b7)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** style tweaks ([861ea3e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/861ea3ef96a33c80d02370d8b230f056b4bd9387)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** updates service which select get orgs ([d8e67ce](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d8e67ce0dbaef707383a7a45f096247f361074ba)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** user additional info in mobile ([a450779](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a450779682ac6e103174ef5b7a072524f5b1322d)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** user info fragment in mobile ([310e058](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/310e0585dc6623572e229029aef79d32dff9a121)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1267:** added eletronic signature to barter ([3222712](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/32227120c5422d03f94ce9869ee58c5026e6abec)), closes [documentos#1267](http://gitlab.meta.com.br/documentos/issues/1267)
* **documentos#1267:** added new fields to barter list/create ([1cbbf4e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1cbbf4e36f780f717e7d1012d42294241b632ee7)), closes [documentos#1267](http://gitlab.meta.com.br/documentos/issues/1267)
* **documentos#1282:** added filter to barter products ([c42e044](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c42e04450907fcde3f76db7cf7b4bba77cbcd61e)), closes [documentos#1282](http://gitlab.meta.com.br/documentos/issues/1282)
* **documentos#1282:** added ie field to barter package join ([63b6be4](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/63b6be4de967544edeebb2305f87ccb4c67a0ce7)), closes [documentos#1282](http://gitlab.meta.com.br/documentos/issues/1282)
* **documentos#1282:** added IE field to sale order form ([d171dac](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d171dac06719c0a06505988e94322852c55ef538)), closes [documentos#1282](http://gitlab.meta.com.br/documentos/issues/1282)
* **documentos#1290:** accept term modal in mobile ([58cf9e5](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/58cf9e5bcb7f5ad6b0be8c332b2da9b893396fbf)), closes [documentos#1290](http://gitlab.meta.com.br/documentos/issues/1290)
* **documentos#1290:** adds use-term to accept modals ([1fbbe35](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1fbbe35bc9d57ead097a7ab594e63987a6cd3850)), closes [documentos#1290](http://gitlab.meta.com.br/documentos/issues/1290)
* **documentos#1290:** adds validation in useterm ([64a52aa](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/64a52aa54d4a2f03c47a887ece1295284b4d825b)), closes [documentos#1290](http://gitlab.meta.com.br/documentos/issues/1290)
* **documentos#1290:** changes in api call ([e6834d7](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e6834d78aec2923209c1de7d59f521b49486d62d)), closes [documentos#1290](http://gitlab.meta.com.br/documentos/issues/1290)
* **documentos#1290:** fixes ([c573fe6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c573fe6aeb22c90b95f4c84447a0d9eea8592a2f)), closes [documentos#1290](http://gitlab.meta.com.br/documentos/issues/1290)
* **documentos#1290:** removes mock onSuccess ([6086dee](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/6086dee40bcfcb9c21965942b5acb4a9f31d2373)), closes [documentos#1290](http://gitlab.meta.com.br/documentos/issues/1290)
* **documentos#1290:** tweaks to fix onboarding ([a7d1d3d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a7d1d3d0b5c4e28e49a2ddd938277ce92ee87569))
* **documentos#1303:** barter fixes ([173fc91](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/173fc91f32d548c88b49361e175eebeda60bb68a)), closes [documentos#1303](http://gitlab.meta.com.br/documentos/issues/1303)
* **documentos#1305:** added new columns to product withdraw list ([e6e2051](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e6e2051f788625b9e83a908466ec358405580057)), closes [documentos#1305](http://gitlab.meta.com.br/documentos/issues/1305)
* **documentos#1305:** changed tab name ([cbc6241](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/cbc624159fc1bb99b8e3b3a5524a8a36b582aab2)), closes [documentos#1305](http://gitlab.meta.com.br/documentos/issues/1305)
* **documentos#885:** changed field screen ([4299f89](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4299f898298c9641e4e63098dfdcf85a5633c11d)), closes [documentos#885](http://gitlab.meta.com.br/documentos/issues/885)
* **mobile:** changed menu order ([92fad8b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/92fad8b4ba42ecca452d8f0ab047553b6f2a3f10))


### BREAKING CHANGES

* **click twice on button:** [





# [0.13.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.12.0-alpha.1...v0.13.0) (2021-03-23)


### Bug Fixes

* **#1185:** adjustment isvalidsize and quality photo camera ([e4b6202](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e4b6202e8d810ce30888e7a7930595ee5c649895)), closes [#1185](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1185)
* **#1217:** fix loading list fields ([ba533da](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ba533da79ed9712289376fc6e67132a71353cc6e))
* **#1280:** fix dispatch register milk quality ([2015255](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/20152551c1f50561a6b41777111f8f9ab21b9072))
* **#1341 #1366:** fix show pest report in field detail screen ([03148d0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/03148d00998fd978b633edef94425e9d75e4957a))
* **#1406:** hidden button register cropmanagement ([d1398b1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d1398b1b3ca387206c973de1751286dafa672cbc)), closes [#1406](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1406) [#452](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/452)
* **#1461:** calling function ([e070f5c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e070f5c306e55064b86537f3fc3d4c7196023a2b)), closes [#1461](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1461)
* **#1461:** validation fields milk delivery form ([d6df4f2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d6df4f2f52d89c06eed86f36fb70ed7ee4db3dec)), closes [#1461](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1461)
* **#1485:** fix z-index buttons edit/delete field ([392ff56](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/392ff56eac6d5bf2b770a613973fa9bd466c4ec0)), closes [#498](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/498)
* **graphs update:** graphs update ([4fd1adc](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4fd1adc06320b82633523915627073f0a5c58d1b))
* fixed field list refresh ([b5152b7](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b5152b79d10db5fb7c90f48818f159bcfe4e93fe))
* **#1280:** fix register milk quality mobile ([79e8429](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/79e8429ecfdf191d8f66e97004c844a6bbb76b2d))
* **#1280:** remove code never user ([e80746d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e80746d48decc3f901c0d347c9b9e45fde4c2814)), closes [#1280](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1280)
* **#1311:** fix change property modal ([0b5d5d9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0b5d5d97a29f47cdd82f475eae3c0d59d5fd4f21)), closes [#400](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/400)
* **#1312:** remove rain record button in field detail mobile ([1e3ac1c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1e3ac1cd09d7b7de3c2b31bb5879f7aae06b5886)), closes [#1312](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1312)
* **#1331:** fix open field like technical ([f6f2806](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f6f2806822f42e8e4a4e3028cabe612691dc9e6d))
* **#1370:** adjustment default value in register milk delivery ([9846cde](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9846cde351f7334cb7b3814fd3fdcf8565b89d82)), closes [#1370](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1370)
* **#1373:** set loading false when delete crop management ([14c6510](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/14c6510349321b25f1f7f0ea7dd414c6d5c83a14)), closes [#1373](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1373) [#429](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/429)
* **#1476:** show correct terms when technical ([7f3e258](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7f3e2589db293cb4546066922e38bed81482b9a1)), closes [#1476](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1476) [#477](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/477)
* **auto merging:** auto merging ([16d2158](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/16d21584dee6b2855e847b30d910d2c538684d1f))
* **click on screen when not loaded:** click on screen when not loaded ([90ebcea](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/90ebcea6f45e427367b7e0f0b22ff18e77c35133))
* **click twice on button:** click twice on button ([3cb8778](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3cb8778ad019e0ad4600116e0717615e4ba3d842))
* **documentos#1292:** fixed securitie movement item ([e343ae9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e343ae932b04d967685d0c41319e159990c5d507))
* **documentos#1328:** fixed property registration mobile map ([cdbfc66](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/cdbfc66c77e4289d53fb22ec6ef8328471f56c5c))
* **documentos#1441:** fixes quotation in mobile ([f3990d7](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f3990d7b573a514735be1282697a53e178771db9))
* **documentos#1445:** ajustes layout indicadores ([232c194](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/232c1943208214ef4317dabec8b70d391a1e08e1)), closes [documentos#1445](http://gitlab.meta.com.br/documentos/issues/1445)
* **documentos#1447:** ajustes mapa de chuva e relatorios ([9cd51a9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9cd51a9037291d437f5b847da66e8b7bf8a67998)), closes [documentos#1447](http://gitlab.meta.com.br/documentos/issues/1447)
* **documentos#1458:** fix typo in input select ([0235284](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/023528495cc2085e29f60d42737be0ee86ae19ce))
* **documentos#1459:** fixed barter package price ([360ce03](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/360ce03ce958f09f5683bc5d73ca5845d142b8ba))
* **documentos#1463:** ajustes dimensao mapa ([ce3a059](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ce3a05955ec6516f19f049b9c4eacaa360b23cf0)), closes [documentos#1463](http://gitlab.meta.com.br/documentos/issues/1463)
* **documentos#1501:** fixed field register navigation ([e0dd818](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e0dd8186e57c6ff5813d64a59cf05244700e6703))
* **update graph when property edit:** update graph when property edit ([e46940a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e46940a77f4dc2ee66babb06628a012f898aed9a))
* fixed input date label and props ([40c0431](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/40c04313374dea7a9508518d301ddfe186937cae))
* **add loader while organization is empty:** add loader ([8b92d3e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8b92d3eb15bb51db849a40ffe9386018059729f8))
* **field indicators layout:** field indicators layout ([dd6d6c7](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/dd6d6c734ef7d398b70042bf7dc5e391abc30dbc))
* **home:** set property as homescreen ([79a33c2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/79a33c282a8d289a91a2f4bbde0986733239b11e))
* **keyboard hiding inputs:** keyboard hiding inputs ([ca76f6e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ca76f6ebd147717813ba4ee336b955c600796059))
* **prevent click twice on button:** click twice button ([065cda2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/065cda2ec4912a9afdfa728a8e3de9a018a41c8a))
* fixed mobile order forms ([3f58d77](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3f58d778861f89f05551a3f17145f52cc0364ac0))
* **keyboard on address form:** keyboard on address form ([131eab8](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/131eab8b01685afd30b602cc752c1a5c5db0729d))


### Features

* **#1031:** add color when have filter active ([b1d1c3f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b1d1c3f0368c28509761a8987c260d1249b0e1ca)), closes [#1031](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1031) [#449](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/449)
* **#1182:** adjustment text pest report location and pin ([21c7da1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/21c7da14eb242f378890d6947cabbda961edff37)), closes [#1182](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1182) [#430](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/430)
* **#1185:** using camera for add new images in pest report ([4437125](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/44371252a7ad104654d5373fcef2f572814cf4e7)), closes [#1185](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1185)
* **#1221:** change report icon marker ([e11875f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e11875f51a8e396928442496842a1e38720487b6)), closes [#1221](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1221) [#444](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/444)
* **#1389:** change leave modal in empty property ([a747d84](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a747d8460e233bc1a39f67086ca8a86a9ca52be3)), closes [#1389](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1389) [#441](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/441)
* **#1414:** remove console log and adjustment editing property ([b83b5db](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b83b5dbbc6715b4ceb0ba8d90fd25d7904a4bca9)), closes [#1414](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1414) [#454](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/454)
* **documenots#1423:** adds useeffect ([5d1c192](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/5d1c19229a1c97e3ba149a6ccd2dbe5695dc8500)), closes [documenots#1423](http://gitlab.meta.com.br/documenots/issues/1423)
* **documentos#1292:** added filter reload ([852192c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/852192c9dc25d2d00042d51b8fe6196a04915d64)), closes [documentos#1292](http://gitlab.meta.com.br/documentos/issues/1292)
* **documentos#1292:** added filter to securities movement list screen ([64fa4af](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/64fa4af8b95417cb508469cdf613ff26a33f329c)), closes [documentos#1292](http://gitlab.meta.com.br/documentos/issues/1292)
* **documentos#1292:** integrated securities movement filter ([b5ca39a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b5ca39afd455fecbe30245a2497c4d446453650d)), closes [documentos#1292](http://gitlab.meta.com.br/documentos/issues/1292)
* **documentos#1305:** removes birthday from onboarding ([5eda681](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/5eda6815388bc7289d1d33d1e6896faca4e4aa68)), closes [documentos#1305](http://gitlab.meta.com.br/documentos/issues/1305)
* **documentos#1323:** fixes technician in web ([3ea9b64](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3ea9b6409460f5af500e67cccc1c50e11ab543b7))
* **documentos#1323:** lint ifxes ([1adaeb9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1adaeb9d03041af2d81434374f9f9f0cd700e6db)), closes [documentos#1323](http://gitlab.meta.com.br/documentos/issues/1323)
* **documentos#1323:** new icon + new login image background ([54cf57d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/54cf57d3c61ec328aba99391ea836eba09fce9dd)), closes [documentos#1323](http://gitlab.meta.com.br/documentos/issues/1323)
* **documentos#1323:** now acceptterm modal in mobile renders HTML ([3df0efc](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3df0efc569b88ecc21861769a164f8796a9c7c7f)), closes [documentos#1323](http://gitlab.meta.com.br/documentos/issues/1323)
* **documentos#1323:** removes console.log ([005a2c1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/005a2c1c08ac6cb7649419c54168a5b8d8c0f629)), closes [documentos#1323](http://gitlab.meta.com.br/documentos/issues/1323)
* **documentos#1332:** removes clg ([86a7569](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/86a75694cd95d753c62fbcf526d4e60084af1709)), closes [documentos#1332](http://gitlab.meta.com.br/documentos/issues/1332)
* **documentos#1357:** changes in field monitoration ([2508a09](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2508a09a73f04732609b124c0c08aeaa59bd7668)), closes [documentos#1357](http://gitlab.meta.com.br/documentos/issues/1357)
* **documentos#1357:** filters and field timeline in web ([2351d72](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2351d72c4e29ce32fcb6d45f2e8f7f1f63927ae2)), closes [documentos#1357](http://gitlab.meta.com.br/documentos/issues/1357)
* **documentos#1357:** fixes permission error ([caed226](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/caed22667aa0259d611894310d4afa266634c2d5))
* **documentos#1357:** modals ([1307ba9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1307ba9b9939cc649f3eb6a44f31bf70434d170b)), closes [documentos#1357](http://gitlab.meta.com.br/documentos/issues/1357)
* **documentos#1357:** removes clg ([8d7fc3c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8d7fc3ca07edcf60e94337637ca6aa094ddf186d)), closes [documentos#1357](http://gitlab.meta.com.br/documentos/issues/1357)
* **documentos#1357:** removes unnused var ([5afd808](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/5afd8083b3cab487cdd5ef48dafcddbd5dcb3db2)), closes [documentos#1357](http://gitlab.meta.com.br/documentos/issues/1357)
* **documentos#1357:** technical modal ([8808db1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8808db1072f9efafa56b3eec0ceb3868b21512cf)), closes [documentos#1357](http://gitlab.meta.com.br/documentos/issues/1357)
* **documentos#1374:** firebase changes ([df6a585](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/df6a585a34570777c468c98c134cd8fbd855601c)), closes [documentos#1374](http://gitlab.meta.com.br/documentos/issues/1374)
* **documentos#1400:** accept terms modal mobile ([7244e0d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7244e0d161d81982fbecaa13a08148f342a94527)), closes [documentos#1400](http://gitlab.meta.com.br/documentos/issues/1400)
* **documentos#1400:** style tweaks ([8485d10](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8485d108afa5099ea63ed59598082424cff5ba3d)), closes [documentos#1400](http://gitlab.meta.com.br/documentos/issues/1400)
* **documentos#1405:** changes in resetcodescreen ([d17b405](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d17b4054b52b1fbc095604fd73bcaaf089325da9)), closes [documentos#1405](http://gitlab.meta.com.br/documentos/issues/1405)
* **documentos#1405:** loaduser ([d2a8537](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d2a85372840f1cec1fc8081efb33bffc72ba578e)), closes [documentos#1405](http://gitlab.meta.com.br/documentos/issues/1405)
* **documentos#1409:** logic to disable button ([70efbf4](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/70efbf4c317e1bf0a9b58a643a34a7a14e3acb50)), closes [documentos#1409](http://gitlab.meta.com.br/documentos/issues/1409)
* **documentos#1423:** adds warning snackbar if mngmt isn conc. ([13f4553](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/13f455348b48a86f2648c3d876244e7bca22cecb)), closes [documentos#1423](http://gitlab.meta.com.br/documentos/issues/1423)
* **documentos#1436:** changes in commercialization ([f4edf6b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f4edf6b977ef4c7a8fe514f844097ab2703e1572)), closes [documentos#1436](http://gitlab.meta.com.br/documentos/issues/1436)
* **documentos#1437:** changes to make onboarding works without email ([f91732d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f91732d32c6437b9ca3c407c470238fa9507c76c)), closes [documentos#1437](http://gitlab.meta.com.br/documentos/issues/1437)
* **documentos#1438:** ajustes mapa ([97b7c63](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/97b7c638d12a5abd12cffe38cc78553d4457b6f8)), closes [documentos#1438](http://gitlab.meta.com.br/documentos/issues/1438)
* **documentos#1441:** barter fixes ([8575fa4](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8575fa4c8a98e6b61e167c14c4ef19e7a6a2f91b)), closes [documentos#1441](http://gitlab.meta.com.br/documentos/issues/1441)
* **documentos#1441:** field timeline chart ([a1bf429](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a1bf4292b9c7bf249e62efcba59aabbd681d79ca)), closes [documentos#1441](http://gitlab.meta.com.br/documentos/issues/1441)
* **documentos#1441:** lint ([4839a2b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4839a2be20eb8a0d51086b78991d44c71eb07f8a)), closes [documentos#1441](http://gitlab.meta.com.br/documentos/issues/1441)
* **documentos#1463:** ajustes finos mapa de chuva ([b25fc96](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b25fc9669fad05fb451c83ffb378bcd72038d3c5)), closes [documentos#1463](http://gitlab.meta.com.br/documentos/issues/1463)
* **documentos#1487:** added loader to machinery register screen ([fabf005](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/fabf005c6fae6ff382ef34be060df4623e462e76)), closes [documentos#1487](http://gitlab.meta.com.br/documentos/issues/1487)
* **documentos#1503:** added loader to field ([9fd282a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9fd282a115c39d954e2e0725226fe023b012d8fb)), closes [documentos#1503](http://gitlab.meta.com.br/documentos/issues/1503)
* changed barter price exibition ([88722b7](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/88722b7e41dceac3f418aedbf24e986b65de69e2))
* fixed tambo forms ([5e1990c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/5e1990c6fb377039bf80f06e47933fa7c490b7a4))
* resolve [#1439](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1439) ([3600986](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3600986a3d59851cbc0a4a75160eb6ab23497167))
* **documentos#1554:** alterada exibição de matricula ([a21246b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a21246b5122acd568b4b0168d61321bb485e65d8)), closes [documentos#1554](http://gitlab.meta.com.br/documentos/issues/1554)
* **doucmentos#1232:** shows all user modules in choosemodulescreen ([8405ea3](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8405ea36080c4bed643150f5632ec666fe0e51e5)), closes [doucmentos#1232](http://gitlab.meta.com.br/doucmentos/issues/1232)
* changed securities movement list filter param name ([d9d5fcc](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d9d5fcc204275b8f0b233c401a8ea11ddec0636a))
* **#1116:** link cards milk dashboard in mobile ([a52742b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a52742b959b64f4db45d3e6f72022b401f330267)), closes [#1116](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1116)
* **#1288:** adjustment select organization ([2dbbc9c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2dbbc9c4234ad1a408da856e1c2deeccc814f993)), closes [#1288](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1288) [#395](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/395)
* **documentos#1116:** adds indicators in dashbboard ([a5110e4](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a5110e4975ab40c4fee20f8d001b927d9615c678)), closes [documentos#1116](http://gitlab.meta.com.br/documentos/issues/1116)
* **documentos#1116:** chart card ([ec6da44](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ec6da446df3e0d09e1ca049ba3313e4e5a615bf5)), closes [documentos#1116](http://gitlab.meta.com.br/documentos/issues/1116)
* **documentos#1116:** chart card on mobile ([9048c80](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9048c8056cfb418569836d307dfa162a38152cb2)), closes [documentos#1116](http://gitlab.meta.com.br/documentos/issues/1116)
* **documentos#1116:** edit activities send to Create Property ([8008bb0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8008bb04b4a2427fb4284c5dc13de12dd9bfb102)), closes [documentos#1116](http://gitlab.meta.com.br/documentos/issues/1116)
* **documentos#1116:** lint fixexs ([88c18b2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/88c18b24b2cc021af41facd68bb1502df53d8c54)), closes [documentos#1116](http://gitlab.meta.com.br/documentos/issues/1116)
* **documentos#1116:** price chart ([1f7805d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1f7805d0509714b108e594591bf33ea59d12a692)), closes [documentos#1116](http://gitlab.meta.com.br/documentos/issues/1116)
* **documentos#1116:** quality chart ([47196f4](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/47196f487a10664e7a60d322e1616ec34df205de)), closes [documentos#1116](http://gitlab.meta.com.br/documentos/issues/1116)
* **documentos#1116:** removes console.log ([dd23fe6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/dd23fe698eddfcf4c1883988920e18c1a5f881af)), closes [documentos#1116](http://gitlab.meta.com.br/documentos/issues/1116)
* **documentos#1116:** tambo for mobile ([f4d650f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f4d650f18602702f4cfccdf32758336da2041ca0)), closes [documentos#1116](http://gitlab.meta.com.br/documentos/issues/1116)
* **documentos#1240:** address card fragment ([8829216](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8829216a6457b17382ae3457a328a44839ad1162)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** change signature modal in mobile ([aefa82a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/aefa82a23f9aaff77a8aba7d4aa643c22db85745)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** creates changesignaturemodal in web ([77eab34](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/77eab3453e87f5839cc64e0a0bbbdf02377c5b08)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** edit address form in mobile ([4ecfa57](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4ecfa5762c22d48faff3c5d4bba3abe8b2effc1a)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** edit address route ([547c709](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/547c709f5c982a36b557a153cbf266bec679db33)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** edit profile form in mobile ([cd71092](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/cd71092d86f0186644fa22d819040f35528220cf)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** edit profile screen ([7316eff](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7316eff0acaa631fa1d4fa12eba9741b38097238)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** init avatar input ([d018ec3](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d018ec371fb33f01a39e91f29bbc853227b6ff34)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** init change signature modal ([e2744a4](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e2744a4e804a998a1c22c67abe213c1f7b339592)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** lint ([123448f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/123448fef6cd259436ecf24bec2b50d0467077c7)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** list fragment in mobile ([f7b674e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f7b674ededa43bfb6bbd13ee2513e31cf05689f2)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** profile screen and addresscard fragment in mobile ([9b0c045](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9b0c04571437a84335178c6da80dd7a207c65dc7)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** removes log ([09066f1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/09066f1fb409cd3ef05210f39813c808788631b7)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** style tweaks ([861ea3e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/861ea3ef96a33c80d02370d8b230f056b4bd9387)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** updates service which select get orgs ([d8e67ce](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d8e67ce0dbaef707383a7a45f096247f361074ba)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** user additional info in mobile ([a450779](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a450779682ac6e103174ef5b7a072524f5b1322d)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** user info fragment in mobile ([310e058](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/310e0585dc6623572e229029aef79d32dff9a121)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1267:** added eletronic signature to barter ([3222712](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/32227120c5422d03f94ce9869ee58c5026e6abec)), closes [documentos#1267](http://gitlab.meta.com.br/documentos/issues/1267)
* **documentos#1267:** added new fields to barter list/create ([1cbbf4e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1cbbf4e36f780f717e7d1012d42294241b632ee7)), closes [documentos#1267](http://gitlab.meta.com.br/documentos/issues/1267)
* **documentos#1282:** added filter to barter products ([c42e044](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c42e04450907fcde3f76db7cf7b4bba77cbcd61e)), closes [documentos#1282](http://gitlab.meta.com.br/documentos/issues/1282)
* **documentos#1282:** added ie field to barter package join ([63b6be4](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/63b6be4de967544edeebb2305f87ccb4c67a0ce7)), closes [documentos#1282](http://gitlab.meta.com.br/documentos/issues/1282)
* **documentos#1282:** added IE field to sale order form ([d171dac](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d171dac06719c0a06505988e94322852c55ef538)), closes [documentos#1282](http://gitlab.meta.com.br/documentos/issues/1282)
* **documentos#1290:** accept term modal in mobile ([58cf9e5](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/58cf9e5bcb7f5ad6b0be8c332b2da9b893396fbf)), closes [documentos#1290](http://gitlab.meta.com.br/documentos/issues/1290)
* **documentos#1290:** adds use-term to accept modals ([1fbbe35](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1fbbe35bc9d57ead097a7ab594e63987a6cd3850)), closes [documentos#1290](http://gitlab.meta.com.br/documentos/issues/1290)
* **documentos#1290:** adds validation in useterm ([64a52aa](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/64a52aa54d4a2f03c47a887ece1295284b4d825b)), closes [documentos#1290](http://gitlab.meta.com.br/documentos/issues/1290)
* **documentos#1290:** changes in api call ([e6834d7](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e6834d78aec2923209c1de7d59f521b49486d62d)), closes [documentos#1290](http://gitlab.meta.com.br/documentos/issues/1290)
* **documentos#1290:** fixes ([c573fe6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c573fe6aeb22c90b95f4c84447a0d9eea8592a2f)), closes [documentos#1290](http://gitlab.meta.com.br/documentos/issues/1290)
* **documentos#1290:** removes mock onSuccess ([6086dee](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/6086dee40bcfcb9c21965942b5acb4a9f31d2373)), closes [documentos#1290](http://gitlab.meta.com.br/documentos/issues/1290)
* **documentos#1290:** tweaks to fix onboarding ([a7d1d3d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a7d1d3d0b5c4e28e49a2ddd938277ce92ee87569))
* **documentos#1303:** barter fixes ([173fc91](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/173fc91f32d548c88b49361e175eebeda60bb68a)), closes [documentos#1303](http://gitlab.meta.com.br/documentos/issues/1303)
* **documentos#1305:** added new columns to product withdraw list ([e6e2051](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e6e2051f788625b9e83a908466ec358405580057)), closes [documentos#1305](http://gitlab.meta.com.br/documentos/issues/1305)
* **documentos#1305:** changed tab name ([cbc6241](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/cbc624159fc1bb99b8e3b3a5524a8a36b582aab2)), closes [documentos#1305](http://gitlab.meta.com.br/documentos/issues/1305)
* **documentos#885:** changed field screen ([4299f89](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4299f898298c9641e4e63098dfdcf85a5633c11d)), closes [documentos#885](http://gitlab.meta.com.br/documentos/issues/885)
* **mobile:** changed menu order ([92fad8b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/92fad8b4ba42ecca452d8f0ab047553b6f2a3f10))


### BREAKING CHANGES

* **click twice on button:** [





# [0.12.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.12.0-alpha.1...v0.12.0) (2021-03-23)


### Bug Fixes

* **#1185:** adjustment isvalidsize and quality photo camera ([e4b6202](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e4b6202e8d810ce30888e7a7930595ee5c649895)), closes [#1185](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1185)
* **#1217:** fix loading list fields ([ba533da](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ba533da79ed9712289376fc6e67132a71353cc6e))
* **#1280:** fix dispatch register milk quality ([2015255](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/20152551c1f50561a6b41777111f8f9ab21b9072))
* **#1341 #1366:** fix show pest report in field detail screen ([03148d0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/03148d00998fd978b633edef94425e9d75e4957a))
* **#1406:** hidden button register cropmanagement ([d1398b1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d1398b1b3ca387206c973de1751286dafa672cbc)), closes [#1406](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1406) [#452](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/452)
* **#1461:** calling function ([e070f5c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e070f5c306e55064b86537f3fc3d4c7196023a2b)), closes [#1461](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1461)
* **#1461:** validation fields milk delivery form ([d6df4f2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d6df4f2f52d89c06eed86f36fb70ed7ee4db3dec)), closes [#1461](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1461)
* **#1485:** fix z-index buttons edit/delete field ([392ff56](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/392ff56eac6d5bf2b770a613973fa9bd466c4ec0)), closes [#498](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/498)
* **graphs update:** graphs update ([4fd1adc](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4fd1adc06320b82633523915627073f0a5c58d1b))
* fixed field list refresh ([b5152b7](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b5152b79d10db5fb7c90f48818f159bcfe4e93fe))
* **#1280:** fix register milk quality mobile ([79e8429](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/79e8429ecfdf191d8f66e97004c844a6bbb76b2d))
* **#1280:** remove code never user ([e80746d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e80746d48decc3f901c0d347c9b9e45fde4c2814)), closes [#1280](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1280)
* **#1311:** fix change property modal ([0b5d5d9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0b5d5d97a29f47cdd82f475eae3c0d59d5fd4f21)), closes [#400](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/400)
* **#1312:** remove rain record button in field detail mobile ([1e3ac1c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1e3ac1cd09d7b7de3c2b31bb5879f7aae06b5886)), closes [#1312](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1312)
* **#1331:** fix open field like technical ([f6f2806](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f6f2806822f42e8e4a4e3028cabe612691dc9e6d))
* **#1370:** adjustment default value in register milk delivery ([9846cde](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9846cde351f7334cb7b3814fd3fdcf8565b89d82)), closes [#1370](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1370)
* **#1373:** set loading false when delete crop management ([14c6510](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/14c6510349321b25f1f7f0ea7dd414c6d5c83a14)), closes [#1373](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1373) [#429](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/429)
* **#1476:** show correct terms when technical ([7f3e258](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7f3e2589db293cb4546066922e38bed81482b9a1)), closes [#1476](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1476) [#477](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/477)
* **auto merging:** auto merging ([16d2158](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/16d21584dee6b2855e847b30d910d2c538684d1f))
* **click on screen when not loaded:** click on screen when not loaded ([90ebcea](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/90ebcea6f45e427367b7e0f0b22ff18e77c35133))
* **click twice on button:** click twice on button ([3cb8778](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3cb8778ad019e0ad4600116e0717615e4ba3d842))
* **documentos#1292:** fixed securitie movement item ([e343ae9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e343ae932b04d967685d0c41319e159990c5d507))
* **documentos#1328:** fixed property registration mobile map ([cdbfc66](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/cdbfc66c77e4289d53fb22ec6ef8328471f56c5c))
* **documentos#1441:** fixes quotation in mobile ([f3990d7](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f3990d7b573a514735be1282697a53e178771db9))
* **documentos#1445:** ajustes layout indicadores ([232c194](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/232c1943208214ef4317dabec8b70d391a1e08e1)), closes [documentos#1445](http://gitlab.meta.com.br/documentos/issues/1445)
* **documentos#1447:** ajustes mapa de chuva e relatorios ([9cd51a9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9cd51a9037291d437f5b847da66e8b7bf8a67998)), closes [documentos#1447](http://gitlab.meta.com.br/documentos/issues/1447)
* **documentos#1458:** fix typo in input select ([0235284](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/023528495cc2085e29f60d42737be0ee86ae19ce))
* **documentos#1459:** fixed barter package price ([360ce03](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/360ce03ce958f09f5683bc5d73ca5845d142b8ba))
* **documentos#1463:** ajustes dimensao mapa ([ce3a059](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ce3a05955ec6516f19f049b9c4eacaa360b23cf0)), closes [documentos#1463](http://gitlab.meta.com.br/documentos/issues/1463)
* **documentos#1501:** fixed field register navigation ([e0dd818](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e0dd8186e57c6ff5813d64a59cf05244700e6703))
* **update graph when property edit:** update graph when property edit ([e46940a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e46940a77f4dc2ee66babb06628a012f898aed9a))
* fixed input date label and props ([40c0431](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/40c04313374dea7a9508518d301ddfe186937cae))
* **add loader while organization is empty:** add loader ([8b92d3e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8b92d3eb15bb51db849a40ffe9386018059729f8))
* **field indicators layout:** field indicators layout ([dd6d6c7](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/dd6d6c734ef7d398b70042bf7dc5e391abc30dbc))
* **home:** set property as homescreen ([79a33c2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/79a33c282a8d289a91a2f4bbde0986733239b11e))
* **keyboard hiding inputs:** keyboard hiding inputs ([ca76f6e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ca76f6ebd147717813ba4ee336b955c600796059))
* **prevent click twice on button:** click twice button ([065cda2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/065cda2ec4912a9afdfa728a8e3de9a018a41c8a))
* fixed mobile order forms ([3f58d77](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3f58d778861f89f05551a3f17145f52cc0364ac0))
* **keyboard on address form:** keyboard on address form ([131eab8](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/131eab8b01685afd30b602cc752c1a5c5db0729d))


### Features

* **#1031:** add color when have filter active ([b1d1c3f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b1d1c3f0368c28509761a8987c260d1249b0e1ca)), closes [#1031](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1031) [#449](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/449)
* **#1182:** adjustment text pest report location and pin ([21c7da1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/21c7da14eb242f378890d6947cabbda961edff37)), closes [#1182](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1182) [#430](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/430)
* **#1185:** using camera for add new images in pest report ([4437125](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/44371252a7ad104654d5373fcef2f572814cf4e7)), closes [#1185](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1185)
* **#1221:** change report icon marker ([e11875f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e11875f51a8e396928442496842a1e38720487b6)), closes [#1221](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1221) [#444](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/444)
* **#1389:** change leave modal in empty property ([a747d84](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a747d8460e233bc1a39f67086ca8a86a9ca52be3)), closes [#1389](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1389) [#441](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/441)
* **#1414:** remove console log and adjustment editing property ([b83b5db](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b83b5dbbc6715b4ceb0ba8d90fd25d7904a4bca9)), closes [#1414](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1414) [#454](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/454)
* **documenots#1423:** adds useeffect ([5d1c192](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/5d1c19229a1c97e3ba149a6ccd2dbe5695dc8500)), closes [documenots#1423](http://gitlab.meta.com.br/documenots/issues/1423)
* **documentos#1292:** added filter reload ([852192c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/852192c9dc25d2d00042d51b8fe6196a04915d64)), closes [documentos#1292](http://gitlab.meta.com.br/documentos/issues/1292)
* **documentos#1292:** added filter to securities movement list screen ([64fa4af](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/64fa4af8b95417cb508469cdf613ff26a33f329c)), closes [documentos#1292](http://gitlab.meta.com.br/documentos/issues/1292)
* **documentos#1292:** integrated securities movement filter ([b5ca39a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b5ca39afd455fecbe30245a2497c4d446453650d)), closes [documentos#1292](http://gitlab.meta.com.br/documentos/issues/1292)
* **documentos#1305:** removes birthday from onboarding ([5eda681](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/5eda6815388bc7289d1d33d1e6896faca4e4aa68)), closes [documentos#1305](http://gitlab.meta.com.br/documentos/issues/1305)
* **documentos#1323:** fixes technician in web ([3ea9b64](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3ea9b6409460f5af500e67cccc1c50e11ab543b7))
* **documentos#1323:** lint ifxes ([1adaeb9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1adaeb9d03041af2d81434374f9f9f0cd700e6db)), closes [documentos#1323](http://gitlab.meta.com.br/documentos/issues/1323)
* **documentos#1323:** new icon + new login image background ([54cf57d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/54cf57d3c61ec328aba99391ea836eba09fce9dd)), closes [documentos#1323](http://gitlab.meta.com.br/documentos/issues/1323)
* **documentos#1323:** now acceptterm modal in mobile renders HTML ([3df0efc](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3df0efc569b88ecc21861769a164f8796a9c7c7f)), closes [documentos#1323](http://gitlab.meta.com.br/documentos/issues/1323)
* **documentos#1323:** removes console.log ([005a2c1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/005a2c1c08ac6cb7649419c54168a5b8d8c0f629)), closes [documentos#1323](http://gitlab.meta.com.br/documentos/issues/1323)
* **documentos#1332:** removes clg ([86a7569](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/86a75694cd95d753c62fbcf526d4e60084af1709)), closes [documentos#1332](http://gitlab.meta.com.br/documentos/issues/1332)
* **documentos#1357:** changes in field monitoration ([2508a09](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2508a09a73f04732609b124c0c08aeaa59bd7668)), closes [documentos#1357](http://gitlab.meta.com.br/documentos/issues/1357)
* **documentos#1357:** filters and field timeline in web ([2351d72](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2351d72c4e29ce32fcb6d45f2e8f7f1f63927ae2)), closes [documentos#1357](http://gitlab.meta.com.br/documentos/issues/1357)
* **documentos#1357:** fixes permission error ([caed226](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/caed22667aa0259d611894310d4afa266634c2d5))
* **documentos#1357:** modals ([1307ba9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1307ba9b9939cc649f3eb6a44f31bf70434d170b)), closes [documentos#1357](http://gitlab.meta.com.br/documentos/issues/1357)
* **documentos#1357:** removes clg ([8d7fc3c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8d7fc3ca07edcf60e94337637ca6aa094ddf186d)), closes [documentos#1357](http://gitlab.meta.com.br/documentos/issues/1357)
* **documentos#1357:** removes unnused var ([5afd808](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/5afd8083b3cab487cdd5ef48dafcddbd5dcb3db2)), closes [documentos#1357](http://gitlab.meta.com.br/documentos/issues/1357)
* **documentos#1357:** technical modal ([8808db1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8808db1072f9efafa56b3eec0ceb3868b21512cf)), closes [documentos#1357](http://gitlab.meta.com.br/documentos/issues/1357)
* **documentos#1374:** firebase changes ([df6a585](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/df6a585a34570777c468c98c134cd8fbd855601c)), closes [documentos#1374](http://gitlab.meta.com.br/documentos/issues/1374)
* **documentos#1400:** accept terms modal mobile ([7244e0d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7244e0d161d81982fbecaa13a08148f342a94527)), closes [documentos#1400](http://gitlab.meta.com.br/documentos/issues/1400)
* **documentos#1400:** style tweaks ([8485d10](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8485d108afa5099ea63ed59598082424cff5ba3d)), closes [documentos#1400](http://gitlab.meta.com.br/documentos/issues/1400)
* **documentos#1405:** changes in resetcodescreen ([d17b405](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d17b4054b52b1fbc095604fd73bcaaf089325da9)), closes [documentos#1405](http://gitlab.meta.com.br/documentos/issues/1405)
* **documentos#1405:** loaduser ([d2a8537](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d2a85372840f1cec1fc8081efb33bffc72ba578e)), closes [documentos#1405](http://gitlab.meta.com.br/documentos/issues/1405)
* **documentos#1409:** logic to disable button ([70efbf4](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/70efbf4c317e1bf0a9b58a643a34a7a14e3acb50)), closes [documentos#1409](http://gitlab.meta.com.br/documentos/issues/1409)
* **documentos#1423:** adds warning snackbar if mngmt isn conc. ([13f4553](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/13f455348b48a86f2648c3d876244e7bca22cecb)), closes [documentos#1423](http://gitlab.meta.com.br/documentos/issues/1423)
* **documentos#1436:** changes in commercialization ([f4edf6b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f4edf6b977ef4c7a8fe514f844097ab2703e1572)), closes [documentos#1436](http://gitlab.meta.com.br/documentos/issues/1436)
* **documentos#1437:** changes to make onboarding works without email ([f91732d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f91732d32c6437b9ca3c407c470238fa9507c76c)), closes [documentos#1437](http://gitlab.meta.com.br/documentos/issues/1437)
* **documentos#1438:** ajustes mapa ([97b7c63](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/97b7c638d12a5abd12cffe38cc78553d4457b6f8)), closes [documentos#1438](http://gitlab.meta.com.br/documentos/issues/1438)
* **documentos#1441:** barter fixes ([8575fa4](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8575fa4c8a98e6b61e167c14c4ef19e7a6a2f91b)), closes [documentos#1441](http://gitlab.meta.com.br/documentos/issues/1441)
* **documentos#1441:** field timeline chart ([a1bf429](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a1bf4292b9c7bf249e62efcba59aabbd681d79ca)), closes [documentos#1441](http://gitlab.meta.com.br/documentos/issues/1441)
* **documentos#1441:** lint ([4839a2b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4839a2be20eb8a0d51086b78991d44c71eb07f8a)), closes [documentos#1441](http://gitlab.meta.com.br/documentos/issues/1441)
* **documentos#1463:** ajustes finos mapa de chuva ([b25fc96](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b25fc9669fad05fb451c83ffb378bcd72038d3c5)), closes [documentos#1463](http://gitlab.meta.com.br/documentos/issues/1463)
* **documentos#1487:** added loader to machinery register screen ([fabf005](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/fabf005c6fae6ff382ef34be060df4623e462e76)), closes [documentos#1487](http://gitlab.meta.com.br/documentos/issues/1487)
* **documentos#1503:** added loader to field ([9fd282a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9fd282a115c39d954e2e0725226fe023b012d8fb)), closes [documentos#1503](http://gitlab.meta.com.br/documentos/issues/1503)
* changed barter price exibition ([88722b7](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/88722b7e41dceac3f418aedbf24e986b65de69e2))
* fixed tambo forms ([5e1990c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/5e1990c6fb377039bf80f06e47933fa7c490b7a4))
* resolve [#1439](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1439) ([3600986](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3600986a3d59851cbc0a4a75160eb6ab23497167))
* **documentos#1554:** alterada exibição de matricula ([a21246b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a21246b5122acd568b4b0168d61321bb485e65d8)), closes [documentos#1554](http://gitlab.meta.com.br/documentos/issues/1554)
* **doucmentos#1232:** shows all user modules in choosemodulescreen ([8405ea3](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8405ea36080c4bed643150f5632ec666fe0e51e5)), closes [doucmentos#1232](http://gitlab.meta.com.br/doucmentos/issues/1232)
* changed securities movement list filter param name ([d9d5fcc](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d9d5fcc204275b8f0b233c401a8ea11ddec0636a))
* **#1116:** link cards milk dashboard in mobile ([a52742b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a52742b959b64f4db45d3e6f72022b401f330267)), closes [#1116](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1116)
* **#1288:** adjustment select organization ([2dbbc9c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2dbbc9c4234ad1a408da856e1c2deeccc814f993)), closes [#1288](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1288) [#395](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/395)
* **documentos#1116:** adds indicators in dashbboard ([a5110e4](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a5110e4975ab40c4fee20f8d001b927d9615c678)), closes [documentos#1116](http://gitlab.meta.com.br/documentos/issues/1116)
* **documentos#1116:** chart card ([ec6da44](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ec6da446df3e0d09e1ca049ba3313e4e5a615bf5)), closes [documentos#1116](http://gitlab.meta.com.br/documentos/issues/1116)
* **documentos#1116:** chart card on mobile ([9048c80](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9048c8056cfb418569836d307dfa162a38152cb2)), closes [documentos#1116](http://gitlab.meta.com.br/documentos/issues/1116)
* **documentos#1116:** edit activities send to Create Property ([8008bb0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8008bb04b4a2427fb4284c5dc13de12dd9bfb102)), closes [documentos#1116](http://gitlab.meta.com.br/documentos/issues/1116)
* **documentos#1116:** lint fixexs ([88c18b2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/88c18b24b2cc021af41facd68bb1502df53d8c54)), closes [documentos#1116](http://gitlab.meta.com.br/documentos/issues/1116)
* **documentos#1116:** price chart ([1f7805d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1f7805d0509714b108e594591bf33ea59d12a692)), closes [documentos#1116](http://gitlab.meta.com.br/documentos/issues/1116)
* **documentos#1116:** quality chart ([47196f4](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/47196f487a10664e7a60d322e1616ec34df205de)), closes [documentos#1116](http://gitlab.meta.com.br/documentos/issues/1116)
* **documentos#1116:** removes console.log ([dd23fe6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/dd23fe698eddfcf4c1883988920e18c1a5f881af)), closes [documentos#1116](http://gitlab.meta.com.br/documentos/issues/1116)
* **documentos#1116:** tambo for mobile ([f4d650f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f4d650f18602702f4cfccdf32758336da2041ca0)), closes [documentos#1116](http://gitlab.meta.com.br/documentos/issues/1116)
* **documentos#1240:** address card fragment ([8829216](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8829216a6457b17382ae3457a328a44839ad1162)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** change signature modal in mobile ([aefa82a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/aefa82a23f9aaff77a8aba7d4aa643c22db85745)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** creates changesignaturemodal in web ([77eab34](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/77eab3453e87f5839cc64e0a0bbbdf02377c5b08)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** edit address form in mobile ([4ecfa57](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4ecfa5762c22d48faff3c5d4bba3abe8b2effc1a)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** edit address route ([547c709](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/547c709f5c982a36b557a153cbf266bec679db33)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** edit profile form in mobile ([cd71092](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/cd71092d86f0186644fa22d819040f35528220cf)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** edit profile screen ([7316eff](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7316eff0acaa631fa1d4fa12eba9741b38097238)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** init avatar input ([d018ec3](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d018ec371fb33f01a39e91f29bbc853227b6ff34)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** init change signature modal ([e2744a4](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e2744a4e804a998a1c22c67abe213c1f7b339592)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** lint ([123448f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/123448fef6cd259436ecf24bec2b50d0467077c7)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** list fragment in mobile ([f7b674e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f7b674ededa43bfb6bbd13ee2513e31cf05689f2)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** profile screen and addresscard fragment in mobile ([9b0c045](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9b0c04571437a84335178c6da80dd7a207c65dc7)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** removes log ([09066f1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/09066f1fb409cd3ef05210f39813c808788631b7)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** style tweaks ([861ea3e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/861ea3ef96a33c80d02370d8b230f056b4bd9387)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** updates service which select get orgs ([d8e67ce](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d8e67ce0dbaef707383a7a45f096247f361074ba)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** user additional info in mobile ([a450779](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a450779682ac6e103174ef5b7a072524f5b1322d)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** user info fragment in mobile ([310e058](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/310e0585dc6623572e229029aef79d32dff9a121)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1267:** added eletronic signature to barter ([3222712](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/32227120c5422d03f94ce9869ee58c5026e6abec)), closes [documentos#1267](http://gitlab.meta.com.br/documentos/issues/1267)
* **documentos#1267:** added new fields to barter list/create ([1cbbf4e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1cbbf4e36f780f717e7d1012d42294241b632ee7)), closes [documentos#1267](http://gitlab.meta.com.br/documentos/issues/1267)
* **documentos#1282:** added filter to barter products ([c42e044](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c42e04450907fcde3f76db7cf7b4bba77cbcd61e)), closes [documentos#1282](http://gitlab.meta.com.br/documentos/issues/1282)
* **documentos#1282:** added ie field to barter package join ([63b6be4](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/63b6be4de967544edeebb2305f87ccb4c67a0ce7)), closes [documentos#1282](http://gitlab.meta.com.br/documentos/issues/1282)
* **documentos#1282:** added IE field to sale order form ([d171dac](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d171dac06719c0a06505988e94322852c55ef538)), closes [documentos#1282](http://gitlab.meta.com.br/documentos/issues/1282)
* **documentos#1290:** accept term modal in mobile ([58cf9e5](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/58cf9e5bcb7f5ad6b0be8c332b2da9b893396fbf)), closes [documentos#1290](http://gitlab.meta.com.br/documentos/issues/1290)
* **documentos#1290:** adds use-term to accept modals ([1fbbe35](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1fbbe35bc9d57ead097a7ab594e63987a6cd3850)), closes [documentos#1290](http://gitlab.meta.com.br/documentos/issues/1290)
* **documentos#1290:** adds validation in useterm ([64a52aa](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/64a52aa54d4a2f03c47a887ece1295284b4d825b)), closes [documentos#1290](http://gitlab.meta.com.br/documentos/issues/1290)
* **documentos#1290:** changes in api call ([e6834d7](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e6834d78aec2923209c1de7d59f521b49486d62d)), closes [documentos#1290](http://gitlab.meta.com.br/documentos/issues/1290)
* **documentos#1290:** fixes ([c573fe6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c573fe6aeb22c90b95f4c84447a0d9eea8592a2f)), closes [documentos#1290](http://gitlab.meta.com.br/documentos/issues/1290)
* **documentos#1290:** removes mock onSuccess ([6086dee](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/6086dee40bcfcb9c21965942b5acb4a9f31d2373)), closes [documentos#1290](http://gitlab.meta.com.br/documentos/issues/1290)
* **documentos#1290:** tweaks to fix onboarding ([a7d1d3d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a7d1d3d0b5c4e28e49a2ddd938277ce92ee87569))
* **documentos#1303:** barter fixes ([173fc91](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/173fc91f32d548c88b49361e175eebeda60bb68a)), closes [documentos#1303](http://gitlab.meta.com.br/documentos/issues/1303)
* **documentos#1305:** added new columns to product withdraw list ([e6e2051](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e6e2051f788625b9e83a908466ec358405580057)), closes [documentos#1305](http://gitlab.meta.com.br/documentos/issues/1305)
* **documentos#1305:** changed tab name ([cbc6241](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/cbc624159fc1bb99b8e3b3a5524a8a36b582aab2)), closes [documentos#1305](http://gitlab.meta.com.br/documentos/issues/1305)
* **documentos#885:** changed field screen ([4299f89](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4299f898298c9641e4e63098dfdcf85a5633c11d)), closes [documentos#885](http://gitlab.meta.com.br/documentos/issues/885)
* **mobile:** changed menu order ([92fad8b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/92fad8b4ba42ecca452d8f0ab047553b6f2a3f10))


### BREAKING CHANGES

* **click twice on button:** [





# [0.12.0-alpha.1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.12.0-alpha.0...v0.12.0-alpha.1) (2021-02-22)


### Bug Fixes

* **#1173:** remove button technical visit register mobile ([612cd91](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/612cd913d3a6d391702fdf604418b6eb361c536f)), closes [#1173](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1173)
* **#1173:** resolve problems of rebase ([9a26af1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9a26af16f3b717545a0dce0d52275b5b820c2056)), closes [#1173](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1173)
* **auth:** fixed multiples refresh token ([b976597](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b976597b48bfce6639d77453f295a8a3437d836a))
* **documentos#1240:** bug fixes ([bb4562a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/bb4562aa03fb046c9ee90103777f114de1d30b8d)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* fixed mobile barter list item ([7454b6b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7454b6bbd2168aacf037f49bca1da33e05d9ca7a))
* **#1215:** start input of type unit with value ([59e952e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/59e952e4cf968126f492d634c9539a9638113930)), closes [#1215](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1215) [#357](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/357)
* **documentos#1209:** fixed firebase lifecycle ([831d393](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/831d3934bad4dd29e794559504a2c3b87dd63b20))


### Features

* **#1173:** submit rain record with raimm or events complete ([4a7b6b1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4a7b6b1501867917da7de21249710610f3a972f7)), closes [#1173](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1173)
* **1220:** ajuste paleta de cores ([1af336d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1af336d4b9c5def20ca58cc26ac6a43ad218f45a))
* **documentos#1196:** added date validation to trigger ([44d093e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/44d093e4bb70da9020200f68a150149ccbd5e8f4)), closes [documentos#1196](http://gitlab.meta.com.br/documentos/issues/1196)
* **documentos#535:** finished home screen for mobile ([b595eee](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b595eeed9efc2c6371bb04276a22abfd74a1d9db)), closes [documentos#535](http://gitlab.meta.com.br/documentos/issues/535)
* **documentos#947:** added edit to barter mobile ([2016788](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2016788b1f875bc60c1f22de551abe7e50e2ce4e)), closes [documentos#947](http://gitlab.meta.com.br/documentos/issues/947)
* **documentos#947:** created barter details screen ([d6bd889](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d6bd88933da46ccb44ed66ba846711252b9a42ff)), closes [documentos#947](http://gitlab.meta.com.br/documentos/issues/947)
* **documentos#947:** created create barter screen ([692f6fb](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/692f6fb3289b3e4ab3b3eb1409b366a56d210365)), closes [documentos#947](http://gitlab.meta.com.br/documentos/issues/947)
* **documentos#947:** created mobile barter details screen ([097e9cd](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/097e9cd4eb734ba0abc9fa17548f2ae16c2a1ec1)), closes [documentos#947](http://gitlab.meta.com.br/documentos/issues/947)
* **documentos#947:** created mobile barter package details ([e3dac6b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e3dac6b145d4b8d13033dbab1ce89a4423e1713f)), closes [documentos#947](http://gitlab.meta.com.br/documentos/issues/947)
* **documentos#987:** crud milk quality ([49b127e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/49b127e01fa9061496aadce437988874d0b60af5)), closes [documentos#987](http://gitlab.meta.com.br/documentos/issues/987)
* **documentos#987:** form in mobile ([cec1cb9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/cec1cb947a9b3abdb775d590048150a328655359)), closes [documentos#987](http://gitlab.meta.com.br/documentos/issues/987)
* **documentos#987:** list, edit and delete in web m. quality ([b0acdd7](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b0acdd789e9c35d8b58c366e357e7e9756435ec5)), closes [documentos#987](http://gitlab.meta.com.br/documentos/issues/987)
* **documentos#987:** register milk quality mobile ([22d7467](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/22d746704abb6694ccefdd93559ac598932d472a)), closes [documentos#987](http://gitlab.meta.com.br/documentos/issues/987)
* **documnentos#1115:** created mobile barter list screen ([60c1e4c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/60c1e4cfdad2d498c11e55a3797de7d950946071)), closes [documnentos#1115](http://gitlab.meta.com.br/documnentos/issues/1115)
* **landscape:** opening chart on landscape left ([636fe6d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/636fe6d36a6aebb6f812ce00fcbdf6889cea4cc5))
* changes in barter list ([9f9e407](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9f9e407127fce7731080024d75b4cdd067868255))





# [0.12.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.11.0...v0.12.0-alpha.0) (2021-02-12)

**Note:** Version bump only for package @smartcoop/mobile-containers





# [0.11.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.11.0-alpha.0...v0.11.0) (2021-02-12)


### Features

* **#1142:** adjustment register technical visit ([6b11511](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/6b11511de1d069510ea4532532aa095e665f677e)), closes [#1142](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1142) [#319](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/319)
* **#1142:** resolve conflicts ([3abbaa9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3abbaa9ec9eaaa849f653fa5c073f89fabd56167)), closes [#1142](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1142)
* **#1146:** create user access area in mobile ([63fa182](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/63fa182f721a275a0755dd562af346475849ea42)), closes [#1146](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1146) [#320](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/320)





# [0.11.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.10.0...v0.11.0-alpha.0) (2021-02-05)


### Bug Fixes

* **#1128#1062:** adjustment button technical register ([b731bcf](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b731bcfed05af9270e068e7d72b48517c96eaba6)), closes [#1128](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1128) [#1062](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1062)
* **onboarding:** fixed save user workflow ([a361e15](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a361e15d4dc30efa0d9d1c964174f3a52ef11975))
* general sales orders fixes ([0b06c10](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0b06c10a0c82fd30d2ab34dd2132a95e6f5ee563))


### Features

* **technical:** turn on permissions ([5f75712](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/5f75712c9ebd3e83ec4cb86762ac20ddb0696720))





# [0.10.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.10.0-alpha.0...v0.10.0) (2021-02-01)


### Bug Fixes

* **documentos#1116:** catching error on formatNumber ([2d7baed](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2d7baed8129b630b7b2f1733925b504c72638d95)), closes [documentos#1116](http://gitlab.meta.com.br/documentos/issues/1116)
* fixed commercilization produts lists ([1304705](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1304705ed6ff849c8de25fd656ef07a7634aaf19))
* **#1003:** adjustment title technical visit screen mobile ([f3aa7a8](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f3aa7a8bc0143235b9f06bd5a6eb8a32284da92a)), closes [#1003](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1003)
* **#1003:** eslint add obligatorily this broken line for commit ([14120a4](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/14120a4943c09c3d6caa97729883da1508e73d00)), closes [#1003](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1003)
* **#1003:** fix import, it's broken application mobile ([e630a2a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e630a2a00765e8c30948f887469673589e1a054c))
* **commecialization:** renaming folder ([01c0626](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/01c062621416018c18a065bcdd527fa6494013df))
* **commercialization:** renamed form folder ([716eaa9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/716eaa9e3968d8776e8345484c017dadc9513209))
* **dirname:** fixed ([8e7e8aa](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8e7e8aa77a9e0c49cda73a15c20b703fc86bf277))
* **dirname:** fixed dirname ([b600af8](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b600af83f7684a97514832ecb9e4e98f08585d5a))
* **documentos#1116:** fixed mobile list loads without org ([4c27567](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4c27567e57bd1f8abfcf744fcab8d0b00abe4fc0))
* **documentos#445:** ajustes na posicao de indicador android ([83e7628](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/83e7628f3f82fe341a935ad90719b79f5add9eea)), closes [documentos#445](http://gitlab.meta.com.br/documentos/issues/445)
* **documentos#994:** securities movement fixes ([8fb505d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8fb505db23743fa82f5674363b022c8a225047d4)), closes [documentos#994](http://gitlab.meta.com.br/documentos/issues/994)
* **lint:** fixed lint ([2ff2c20](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2ff2c20f4c63a8b554fc10c408d14d8657544323))
* **login:** fixed firebase login and logout ([aee9d4d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/aee9d4d2f4ac03cf74338cbb8699fdf061cda307))
* **mobile:** fixed import ([da00e5a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/da00e5ab6bea89e44577eaa90d58c3bc37746e37))


### Features

* **#1003:** adjustment for mr ([3aa716f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3aa716fb9283f3697aea633f00aa5e1198582c43)), closes [#1003](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1003)
* **#1003:** remove useEffect don't usually ([d1d4897](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d1d4897feffb42d0872fb9a8399e60f1501ef164)), closes [#1003](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1003)
* **#1004:** adjustment and create filters for technical visits ([53623e5](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/53623e5c296bcfecc245a9820631d8303645abb5)), closes [#1004](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1004)
* **#1004:** adjustment show technical visits and register ([7c06744](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7c06744d7fc13b44151f1b9354a2ab9d5f247426)), closes [#1004](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1004)
* **#981:** adjustment form in mobile and web ([12c2b7e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/12c2b7e933117e6f21f5f42b00b9815b54f40aab)), closes [#981](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/981)
* **#981:** return object ([5ad9e4b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/5ad9e4b504eb3c61918e234875fe734f5563a00f)), closes [#981](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/981)
* **auth:** added login by cpf/cnpj ([3fb8616](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3fb8616f1773e6686c4be2a5a4893884ab86b84d))
* **documentos#1006:** created technical portfolio screen ([8f964b7](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8f964b73604e5520e9b956cca6855770abc6a89c)), closes [documentos#1006](http://gitlab.meta.com.br/documentos/issues/1006)
* **documentos#1080 documentos#1081:** added number format ([ca0f69f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ca0f69f87e8e004f9de34d83a046931f5a21e1e0)), closes [documentos#1080](http://gitlab.meta.com.br/documentos/issues/1080) [documentos#1081](http://gitlab.meta.com.br/documentos/issues/1081)
* **documentos#534:** added technical module to mobile ([007789a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/007789a8ca387d04401095875ba867d35388cbc9)), closes [documentos#534](http://gitlab.meta.com.br/documentos/issues/534)
* **documentos#974:** added products withdraw list in technical ([d241e8a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d241e8aa6106e4a1fa0b9cc9a2ea367729129e8c)), closes [documentos#974](http://gitlab.meta.com.br/documentos/issues/974)
* **documentos#975:** changed securities movement list ([d19da7d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d19da7d67372b7acb79fdb68e14412883d8e5a8c)), closes [documentos#975](http://gitlab.meta.com.br/documentos/issues/975)
* **documentos#987:** action to open filter modal and some tweaks ([3229d2c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3229d2c31edeee3b77136e480bb6718d122c3044)), closes [documentos#987](http://gitlab.meta.com.br/documentos/issues/987)
* **documentos#987:** action to open filter modal and some tweaks ([fb4002e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/fb4002e34d6ca034e96856c7e2898ce5382f0b1c)), closes [documentos#987](http://gitlab.meta.com.br/documentos/issues/987)
* **documentos#987:** changes to correct icon in web and mobile ([270f211](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/270f211354b2965f202027c72e499124bf658b83)), closes [documentos#987](http://gitlab.meta.com.br/documentos/issues/987)
* **documentos#987:** changes to correct icon in web and mobile ([c53d5b4](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c53d5b44736b72bbdb3cbc7001c8b73058522d05)), closes [documentos#987](http://gitlab.meta.com.br/documentos/issues/987)
* **documentos#987:** creates barebones dashboard in mobile ([ca15622](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ca156225b39a0009babc01839156e757405c20cd)), closes [documentos#987](http://gitlab.meta.com.br/documentos/issues/987)
* **documentos#987:** creates barebones dashboard in mobile ([72b6040](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/72b6040366cb8d8dc4a0149cb3f4ef398e594e09)), closes [documentos#987](http://gitlab.meta.com.br/documentos/issues/987)
* **documentos#987:** creates filter form milk quality ([fd24417](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/fd2441790211b8bc499e1002ae4afa276df42777)), closes [documentos#987](http://gitlab.meta.com.br/documentos/issues/987)
* **documentos#987:** creates filter in mobile ([8683d2f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8683d2fcb62b2336b6181753972f419aa7956f6d)), closes [documentos#987](http://gitlab.meta.com.br/documentos/issues/987)
* **documentos#987:** creates filter in mobile ([0e8d05e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0e8d05ea3a4dd8c818c37b560946db641dfe7a13)), closes [documentos#987](http://gitlab.meta.com.br/documentos/issues/987)
* **documentos#987:** creates filter milk modal ([dfa4170](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/dfa417022506e6e73d77b8266b4c9f784d15c04a)), closes [documentos#987](http://gitlab.meta.com.br/documentos/issues/987)
* **documentos#987:** creates milk quality item ([04e70da](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/04e70da83d40fed2e58ca746fb2895a89a0ab564)), closes [documentos#987](http://gitlab.meta.com.br/documentos/issues/987)
* **documentos#987:** creates milk quality list screen ([3b961da](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3b961dac2777434e74e5e4eb0045b76b65218ab1)), closes [documentos#987](http://gitlab.meta.com.br/documentos/issues/987)
* **documentos#987:** dairy farm routes ([9ff1ffe](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9ff1ffe28cd5e9314798e201cf53161dab1da6c9)), closes [documentos#987](http://gitlab.meta.com.br/documentos/issues/987)
* **documentos#987:** dairy farm routes ([e152850](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e1528506db0c7896b9b654adff63c1490f7573d5)), closes [documentos#987](http://gitlab.meta.com.br/documentos/issues/987)
* **documentos#987:** filter milk modal in mobile ([a7b861f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a7b861f520b724c521f40cb163fc200775177235)), closes [documentos#987](http://gitlab.meta.com.br/documentos/issues/987)
* **documentos#987:** filter milk modal in mobile ([25a1f39](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/25a1f393376d16e98eb90cf86ce04b8ba8ca2eb8)), closes [documentos#987](http://gitlab.meta.com.br/documentos/issues/987)
* **documentos#987:** integrates lists ([9ef2d40](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9ef2d4024888ab1d86d9ecc45e0e79274378fb10)), closes [documentos#987](http://gitlab.meta.com.br/documentos/issues/987)
* **documentos#987:** register in mobile and web ([31b638d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/31b638d34d2f86182ba5cbe0a37c7d8def9f03d0)), closes [documentos#987](http://gitlab.meta.com.br/documentos/issues/987)
* files forgotten ([c10f65b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c10f65b81279280c8f251bc1901eb1da6f042bd6))
* resolve [#990](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/990) ([0a9226e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0a9226e45797c9e3c129b2919235c48ea23ebd51))
* resolve merge conflicts ([0646a07](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0646a07c03edcc03fe7ad4c3b58200a3fdaf5369))
* resolve merge conflicts ([d76333d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d76333db93b61f9897987ee033d050dcdc48d8fe))
* techical products withdraw list ([9ca01d8](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9ca01d8e858f2bfe9458e29f1f670f5de8be7fe1))
* **documentos#987:** register milk in mobile ([a4434d8](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a4434d8a294bb4f57b2c402da67e61a82bd92b3f)), closes [documentos#987](http://gitlab.meta.com.br/documentos/issues/987)
* **documentos#987:** register milk in mobile ([66b641d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/66b641d650133a6449e8afe63a95e82beb5fd150)), closes [documentos#987](http://gitlab.meta.com.br/documentos/issues/987)
* **documentos#987:** reworks on filters ([c1adafb](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c1adafb901f8ce20afeb2698126cba238ffa9231)), closes [documentos#987](http://gitlab.meta.com.br/documentos/issues/987)





# [0.10.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.9.0...v0.10.0-alpha.0) (2021-01-19)

**Note:** Version bump only for package @smartcoop/mobile-containers





# [0.9.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.8.2...v0.9.0) (2021-01-19)


### Bug Fixes

* **#873:** remove comment ([ddc4cd1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ddc4cd1cc4a8d8fcfb93be24b22f9a88cd6beb32)), closes [#873](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/873)
* fixed proptypes ([0089726](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0089726aa4e6ef389eecc513075f7a3e3667cc6c))
* **#882:** add unit dose in fertilization form ([0b2d800](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0b2d80047bb9c9bef1f40dc4acdbcdc74c7a652f)), closes [#882](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/882)
* **#892 #893:** adjustment in mobile ([5064601](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/5064601109ad835d74b8056d099ac963f8bdeca1)), closes [#892](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/892) [#893](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/893) [#237](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/237)
* **documentos#869:** also fixed documentos[#942](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/942) ([35481fb](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/35481fb256a5fc5a8b9a659b8acc86befbd04c3f))
* **documentos#939:** added ?. operator ([8ef96e3](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8ef96e37f64979fbef5edbf528a86b7ef3b68f52)), closes [documentos#939](http://gitlab.meta.com.br/documentos/issues/939)
* **documentos#939:** added shiny operator to organizationProduct ([ee101c9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ee101c9d4bcbee3b6155a495de5b1de043a62464)), closes [documentos#939](http://gitlab.meta.com.br/documentos/issues/939)
* object read fixes ([c6d3424](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c6d34248fbbc8a97399e48783a9c40e76676d332))


### Features

* **#862:** adjustment register and edit field ([4aa85af](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4aa85af740e380188f806588a53dbe96b006fb55)), closes [#862](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/862)
* **#862:** implemented onboarding for supplier ([a639b79](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a639b79882d5c01fd04dd43a363c5580039992a2)), closes [#862](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/862)
* **#873:** adjustments ([23a0c52](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/23a0c5202e48d1f08c3bd87930e47328084cde68)), closes [#873](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/873)
* **#873:** create signature modal and form in mobile ([3dea9b7](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3dea9b742cf7aa0f955e8f510c201992740a6d40)), closes [#873](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/873) [#260](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/260)
* **#873:** enable onsuccess ([4480d4a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4480d4a1882ddedfaa1b147d0672add8715c6233)), closes [#873](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/873)
* **#873:** integration order create and signature electronic ([1f72cb3](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1f72cb3e3cc5d70f80d31c8e4fe48b8658a4ba2f)), closes [#873](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/873) [#260](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/260)
* added sales order delete ([ff4f588](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ff4f588585eb4c76f9a544ee4c650d5551eae85b))
* dynamic validation in sales orders form ([8a7a60a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8a7a60ac7a8eb8f39098ac6ded762aaadacb6692))
* **#924:** comment edited button ([d1cc90e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d1cc90ec808e3131d39083467f6eef49b03b61ff)), closes [#924](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/924) [#247](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/247)
* **ajustada label hour:** ajustada label hour ([cb630bc](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/cb630bc0c4781e2c3b0800bf7d0fe6796fde5fce))
* **documentos#299:** adapts mobile to new form to post ([9f56270](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9f56270af1d5c2b606857b6e3ceabcf48d1b990a)), closes [documentos#299](http://gitlab.meta.com.br/documentos/issues/299)
* **documentos#299:** adds handleclose to plague modal ([ed85b8b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ed85b8b6da87dc2f239f5c06bc9027d056121275))
* **documentos#299:** adds images and fixes in web ([105f7d9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/105f7d995db2ca4e67879e013af2403147cad4ac))
* **documentos#299:** adds label to slider in mobile ([f80fbb9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f80fbb93a436f6f532bfac01fbd886eb78dc6ed9)), closes [documentos#299](http://gitlab.meta.com.br/documentos/issues/299)
* **documentos#299:** adds onedit prop on modal ([a031e0b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a031e0bce97b3650b0e7c7d308ca89f95bc06bda)), closes [documentos#299](http://gitlab.meta.com.br/documentos/issues/299)
* **documentos#299:** button to access pest report mobile ([1ea802f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1ea802f6baad9e1bfbe9fe059a507cf30b36a2d3)), closes [documentos#299](http://gitlab.meta.com.br/documentos/issues/299)
* **documentos#299:** creates pest report register screen ([c78dec2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c78dec207700bece623b8ec300511edcfbfec7f2)), closes [documentos#299](http://gitlab.meta.com.br/documentos/issues/299)
* **documentos#299:** default id ([79476da](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/79476dadb857ecfd46d1b9f80c63efc4786f53f7)), closes [documentos#299](http://gitlab.meta.com.br/documentos/issues/299)
* **documentos#299:** defaultvalues mobile ([87c2b6b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/87c2b6b7eb87b307ddc4fd1edb27cc26d97e7a48)), closes [documentos#299](http://gitlab.meta.com.br/documentos/issues/299)
* **documentos#299:** edit on mobile ([1ee0264](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1ee02644bc75f4546aa76e82d61a219c7179c7c2)), closes [documentos#299](http://gitlab.meta.com.br/documentos/issues/299)
* **documentos#299:** edit pest report on mobile ([08594f2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/08594f2634039b4d1e6fc8822c1db343d77a660a)), closes [documentos#299](http://gitlab.meta.com.br/documentos/issues/299)
* **documentos#299:** moves logic to usememo ([c9261da](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c9261da496ece3474e91fadf5597727be38fbe91)), closes [documentos#299](http://gitlab.meta.com.br/documentos/issues/299)
* **documentos#299:** pest report delete button on mobile ([45c0440](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/45c044008159ee5c5010d0e087316401c29044a7)), closes [documentos#299](http://gitlab.meta.com.br/documentos/issues/299)
* **documentos#299:** pest report map screen ([15944e4](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/15944e40d12dac74db61d7c83be21a1a60cd4f10)), closes [documentos#299](http://gitlab.meta.com.br/documentos/issues/299)
* **documentos#299:** pest Report routes ([7476746](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/74767469b018436802b1e532766fb82e89796c9b)), closes [documentos#299](http://gitlab.meta.com.br/documentos/issues/299)
* **documentos#299:** removes onpress without fnc ([e1c6539](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e1c65399013bcb3053641972355ba97742b0cfa5)), closes [documentos#299](http://gitlab.meta.com.br/documentos/issues/299)
* **documentos#299:** render dom files ([6c239da](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/6c239dab1193653de33f3cef37af315edb1ec8c9)), closes [documentos#299](http://gitlab.meta.com.br/documentos/issues/299)
* **documentos#299:** usememo props ([dfc1150](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/dfc1150f485ca02a39864ebc01460d5b326830e2)), closes [documentos#299](http://gitlab.meta.com.br/documentos/issues/299)
* **documentos#455:** fixed indicators modal title style ([a0514c9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a0514c9e47a4da9f236b9462ebcbaa66ce669957))
* added module selection rules in mobile ([cb56581](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/cb565811f4d57ab809c984beb13b524b5f08fff4))
* added sales orders forms ([b1e3fe8](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b1e3fe88414dba234ec6c9deff8d6cb7c3616f5a))
* changed sales order forms ([a4dccbe](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a4dccbe39baa24fcd5462a81c56f7ebdffe55e3b))
* changed sales orders forms ([16f9223](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/16f92232da24dcafc412441e8533c44d8d244c3e))
* created sales orders screen and form ([32fe5b8](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/32fe5b8e4cd6bbc8f6ec6f9fa78b12e415b0bbdc))
* **documentos#491:** sales orders api integration ([db53258](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/db532586160a539d34d858d40742b63b55c0ffbe)), closes [documentos#491](http://gitlab.meta.com.br/documentos/issues/491)
* **documentos#507:** adds edit and delete buttons in my machines ([09c0698](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/09c06988c454959784e9ba7e188b22b3e130f9c0)), closes [documentos#507](http://gitlab.meta.com.br/documentos/issues/507)
* **documentos#507:** delete on mobile ([b5707b2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b5707b2932381d29f7f65a4aa58a9a612bc3d8de)), closes [documentos#507](http://gitlab.meta.com.br/documentos/issues/507)
* **documentos#507:** edit validations ([0dc1082](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0dc1082c36c5f2e0592bdc123b6eb66e07127e55)), closes [documentos#507](http://gitlab.meta.com.br/documentos/issues/507)
* **documentos#507:** rm clg ([26858eb](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/26858eb6954526f17db05ffd942ee2af996daaac)), closes [documentos#507](http://gitlab.meta.com.br/documentos/issues/507)
* **documentos#820:** added credit limit in mobile ([4104279](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4104279ffaa7567830875765b1efdc3bddaa54dc)), closes [documentos#820](http://gitlab.meta.com.br/documentos/issues/820)
* **documentos#836:** finished mobile list ([6cbf8b3](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/6cbf8b38ee2ef54367d0503f607dc38386ca56e8)), closes [documentos#836](http://gitlab.meta.com.br/documentos/issues/836)





## [0.8.2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.8.1...v0.8.2) (2021-01-06)


### Bug Fixes

* commented error line ([b053d8b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b053d8bb2691cdd1563e029977175c21ccbb9f92))
* fixed use of i18n in reset code screen ([c13a5bd](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c13a5bdbdaa8c316bb0c0a48a3bc3d7f434ffe85))


### Features

* **documentos#506:** adds input phone mobile machinery ([0799907](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0799907129b356f0523926b25f6b4b8f44d94707)), closes [documentos#506](http://gitlab.meta.com.br/documentos/issues/506)
* **documentos#506:** image gallery on mobile ([0c26416](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0c264165820f39575cb198d12f4e6885a935b81b)), closes [documentos#506](http://gitlab.meta.com.br/documentos/issues/506)
* **documentos#725:** action buttons in temporary position ([71c31a0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/71c31a0df7d229c19d5df376908f25b8c5dd1b0f)), closes [documentos#725](http://gitlab.meta.com.br/documentos/issues/725)
* **documentos#725:** adds defaultValue prop to form ([83f249b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/83f249b54815722f27c88a73e897e8d99ede6071)), closes [documentos#725](http://gitlab.meta.com.br/documentos/issues/725)
* **documentos#725:** adds defaultvalues to edit form ([3c3309a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3c3309a6ec99e85c1066c9923a82806827d4fbef)), closes [documentos#725](http://gitlab.meta.com.br/documentos/issues/725)
* **documentos#725:** better buttons placement ([3b075d4](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3b075d4d136449d763d73e9c75ed50ae099447c2)), closes [documentos#725](http://gitlab.meta.com.br/documentos/issues/725)
* **documentos#725:** buttons placement in details ([923dcd7](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/923dcd7670a3831ab388cf43dcf39de9c1629005)), closes [documentos#725](http://gitlab.meta.com.br/documentos/issues/725)
* **documentos#725:** default values treatment on mobile ([0c069c3](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0c069c3c5df57e03f2f581ab6cac3d2655e69308)), closes [documentos#725](http://gitlab.meta.com.br/documentos/issues/725)
* **documentos#725:** isUsermachine validation ([ba69ceb](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ba69cebd6f581458806674730ecd8455d30dcd78)), closes [documentos#725](http://gitlab.meta.com.br/documentos/issues/725)
* **documentos#725:** rename id of button ([903c7c6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/903c7c60d53f7ca9731c12331913786edbfc6d5c)), closes [documentos#725](http://gitlab.meta.com.br/documentos/issues/725)
* **documentos#725:** uncomment phone input mobile ([09481ec](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/09481ecf2090f557ea080713189315c04495bfa3)), closes [documentos#725](http://gitlab.meta.com.br/documentos/issues/725)





## [0.8.1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.9.0-alpha.0...v0.8.1) (2021-01-06)


### Features

* **#629:** adjustment complete crop management ([6cb1770](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/6cb17706af72bb4599cc421a9862041727e83189)), closes [#629](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/629)
* **#629:** adjustment forms management register ([552cec6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/552cec63a9cc7c7fdbe553f22efc980098648c65)), closes [#629](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/629)
* **#629:** adjustment name screen for operation name ([1e0be1e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1e0be1e372d6ed5d23d5495d0e9fe33f230799ce)), closes [#629](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/629)





# [0.9.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.8.0...v0.9.0-alpha.0) (2021-01-05)


### Features

* **social:** social network enabled ([d987ac7](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d987ac71897fd85f83a0e45f260d4868230afbe8))





# [0.8.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.8.0-alpha.0...v0.8.0) (2021-01-05)


### Bug Fixes

* **ajuste tamanho coluna:** ajuste tamanho coluna ([d98cdfa](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d98cdfa5c7376bdd811e1639a35c9671768dcffb))
* **documentos#321:** implementada satelite mobile ([7a6f77e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7a6f77e9aeb52482b60c4524c85dd47019a192db)), closes [documentos#321](http://gitlab.meta.com.br/documentos/issues/321)
* **documentos#69:** get token by organization ([f478fab](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f478fab50847279b60e38491ed90d67fcdd3caae)), closes [documentos#69](http://gitlab.meta.com.br/documentos/issues/69)


### Features

* **#321:** conflicts resolved ([429f7fc](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/429f7fc6d3248e2a041739fef5230336643ff3f9)), closes [#321](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/321)
* **#629:** add conection weather in mobile ([a2b481f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a2b481f855cfbf63a582122911c4a8010cf70330)), closes [#629](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/629)
* **321:** previsao do tempo mobile ([0db0a4b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0db0a4b8761cd1deb3c498313fcd0de1e3409fd7))
* **445:** adicionada paleta de cores ([7e25bc2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7e25bc2d7327aeee86d534ca51ecc820b7dc6bca))
* **ajustes sms e direcionamento:** ajustes sms e direcionamento ([a7f7a74](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a7f7a742a8a6f4c977ca06d48f7063c459b567a1))
* **delete addres page on guest:** delete addres page on guest ([95b1f87](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/95b1f87b64feb0ea84358f8fe047375802ac817d))
* **docuemtnos#491:** created mobile sales order list item ([0e86d02](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0e86d02fd3afc087224d5aa25e527129f38c8d45)), closes [docuemtnos#491](http://gitlab.meta.com.br/docuemtnos/issues/491)
* **documentos#143:** creates product balance item ([bb93ab6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/bb93ab6bc2d3fa2bb2ddf993fa255b5d4814b442)), closes [documentos#143](http://gitlab.meta.com.br/documentos/issues/143)
* **documentos#176 documentos#143:** product balance and withdraw list ([076dae2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/076dae26e34aecb1a671e86e4d40eb574521c4db)), closes [documentos#176](http://gitlab.meta.com.br/documentos/issues/176) [documentos#143](http://gitlab.meta.com.br/documentos/issues/143)
* **documentos#27:** slugs validation ([d387a04](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d387a04cdbb1bcb4d8560340cf46398da6dac50f)), closes [documentos#27](http://gitlab.meta.com.br/documentos/issues/27)
* **documentos#300:** created refresh token mechanism ([54ee2f1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/54ee2f1626aee3e6c60d7a1412305fce7fef5d01)), closes [documentos#300](http://gitlab.meta.com.br/documentos/issues/300)
* **documentos#491:** created mobile sales orders list screen ([f3ace22](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f3ace22d4428c097f32646c58e915f76490950e0)), closes [documentos#491](http://gitlab.meta.com.br/documentos/issues/491)
* **documentos#501:** fix missing translation field register ([d80e025](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d80e025f757372d1e5a06a358f9b63ff086e9c96))
* **documentos#506:** creates details screen ofr machine ([9e7f19f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9e7f19f8853a3951976751539dc503eb98103300)), closes [documentos#506](http://gitlab.meta.com.br/documentos/issues/506)
* **documentos#506:** creates machinery list in mobile ([bb0bd87](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/bb0bd871d7519b3a36c9f1e6ed992b4bfd9ffc2f)), closes [documentos#506](http://gitlab.meta.com.br/documentos/issues/506)
* **documentos#506:** fixing mobile navigation ([508ca89](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/508ca89dad43d2903db28d1c06e91fe5a11dae56))
* **documentos#506:** machinery form mobile ([ec6d055](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ec6d0551091beec6acbbd82bc0007ca3d3364112)), closes [documentos#506](http://gitlab.meta.com.br/documentos/issues/506)
* **documentos#506:** machinery list ([ff842b6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ff842b68c3605fa5bd9211bb801c4a4393b0971d)), closes [documentos#506](http://gitlab.meta.com.br/documentos/issues/506)
* **documentos#506:** machinery register screen at mobile ([9b4add6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9b4add66464cf5a1991390924569e6854c249006)), closes [documentos#506](http://gitlab.meta.com.br/documentos/issues/506)
* **documentos#506:** machinery routes mobile ([92e83a3](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/92e83a3870f0b8acd96ba2364cf741489f84a5e5)), closes [documentos#506](http://gitlab.meta.com.br/documentos/issues/506)
* **documentos#506:** post to create new machine ([cf046a8](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/cf046a8a40b68ea94a6d3092ead942b7e5d78eb9)), closes [documentos#506](http://gitlab.meta.com.br/documentos/issues/506)
* **documentos#506:** removes detached ([8585aa8](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8585aa80826ca7a79608231ad8d5db59d3b8d5a8)), closes [documentos#506](http://gitlab.meta.com.br/documentos/issues/506)
* **documentos#506:** removes maxlen ([bd897bd](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/bd897bd2b73155a0e78405dcbe9efb6a810dc9f6)), closes [documentos#506](http://gitlab.meta.com.br/documentos/issues/506)
* **documentos#507:** adds property_id to query params ([05f77c6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/05f77c61dbe80143ce8406b7bfdc49f6224d1528)), closes [documentos#507](http://gitlab.meta.com.br/documentos/issues/507)
* **documentos#51:** wip ([45c81cc](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/45c81cc46f96334ae37160677e1babe63c5de358)), closes [documentos#51](http://gitlab.meta.com.br/documentos/issues/51)
* **documentos#71:** added mobile securities movement screen ([7bb7fe1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7bb7fe121a83cfb4c448e2d9d845e4204224167b)), closes [documentos#71](http://gitlab.meta.com.br/documentos/issues/71)
* **documentos#71:** created mobile securities movement list ([23ac60c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/23ac60c772f788218d2b60a0cfc1006d380c1aab)), closes [documentos#71](http://gitlab.meta.com.br/documentos/issues/71)
* **documentos#79:** finished charts ([2b089cd](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2b089cdf380708c35b8fe4a47f65fd8e20b7523f)), closes [documentos#79](http://gitlab.meta.com.br/documentos/issues/79)
* **documentos#79:** merged with develop ([7538e6f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7538e6fd5b6870ade8c87eb3c9f62e1295990fb3)), closes [documentos#79](http://gitlab.meta.com.br/documentos/issues/79)
* **staging:** creating staging version ([108ea4f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/108ea4f683fc25e4ab6adcabaadd89a5ab7c60f4))
* added filters to product lists ([a5544d5](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a5544d50d9d0f0e4f21b05eeebba2de2a5732da3))
* **social-network:** hidden social network module ([cf4b3f1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/cf4b3f17bf5350f178198b26d1c2ff07ba97bb72))





# [0.8.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.7.0...v0.8.0-alpha.0) (2020-12-11)

**Note:** Version bump only for package @smartcoop/mobile-containers





# [0.7.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.7.0-alpha.0...v0.7.0) (2020-12-11)


### Bug Fixes

* **#407:** adjustment input number ([e912a33](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e912a335a37fb6daf8cf2ab6afa1d61ad5f626a6)), closes [#407](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/407) [#164](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/164)
* **#521:** fix bug button register field ([f8eaa55](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f8eaa55434c3376d608871adcb095a5b773113ae)), closes [#169](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/169)
* **documentos#398:** fixes field navigation ([bf45b12](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/bf45b1202b927f577e5c42b733d6fb1313e99c5f))
* **documentos#436:** fixed scroll and width ([a59569e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a59569e9ef6e7e319d706c5445ad5d39a58c66d5))
* **documentos#436:** fixed scroll inside product quotations list ([340a84a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/340a84a61b71b974263bf86cad24ded2b4ae6c65))
* **documentos#450:** adds conversion factor in mobile ([c045f89](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c045f892138f0ee13a98071abd745b44fbe41e4d)), closes [documentos#450](http://gitlab.meta.com.br/documentos/issues/450)
* **documentos#452:** adds backdate parse in moment ([ad3b1da](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ad3b1da60af6a2f649d665c81cd01ed98880da70)), closes [documentos#452](http://gitlab.meta.com.br/documentos/issues/452)
* **documentos#452:** backdate -> backdatetime ([63fcf9f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/63fcf9f149ddff2b26cd98c569d5622db75f8283)), closes [documentos#452](http://gitlab.meta.com.br/documentos/issues/452)
* **documentos#518:** disabled delete field when it have growing season ([b1412d7](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b1412d78721f5a3c1adabf38dd5ce9847e9f36b8)), closes [documentos#518](http://gitlab.meta.com.br/documentos/issues/518)
* **documentos#519:** fixed fields list ([d49125e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d49125e26cc24c734367924a5080869368fb55d7))
* **fields:** styles ([a3545ab](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a3545ab046b6fd6ade53dbcf07771657161fde0b))


### Features

* **#407:** adjustment filter fields mobile ([2523e03](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2523e03081c0290f5994dc6df4e08ecfc06b81bb)), closes [#407](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/407) [#164](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/164)
* **#407:** adjustment reset debounce text when change property ([1bad664](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1bad6640dbddfde11bab573735024e6500ad1bea)), closes [#407](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/407) [#164](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/164)
* **#407:** ajustment layout and show ([944559d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/944559de8338f3887ca16808289a9c4924efdf37)), closes [#407](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/407) [#159](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/159)
* **#407:** show detail growingSeason ([a9fa5ae](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a9fa5ae097d0e0617054b9c3ffa187accf3f3149)), closes [#407](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/407) [#159](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/159)
* **#412:** adjustment button for delete field ([3b52f61](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3b52f613c726ef9babf0d774ee1040aa97fc09e7)), closes [#412](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/412) [#141](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/141)
* **#412:** use modal confirm mobile ([8e692cb](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8e692cb524d2a1f3faf6b00efcda546ca3d27b5a)), closes [#412](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/412) [#141](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/141)
* **#518:** fix remove field with cultivation ([8156e3a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8156e3a1d157261d559367df71b2b3a74087044d))
* **documentos#103:** add translations to edit fields ([05a0f1e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/05a0f1e080e31771045ad27c759a28fbf4975e21)), closes [documentos#103](http://gitlab.meta.com.br/documentos/issues/103)
* **documentos#103:** added dynamic zoom to field details ([40a2ea8](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/40a2ea878773472acb9edd75512accabfb961a9d)), closes [documentos#103](http://gitlab.meta.com.br/documentos/issues/103)
* **documentos#108:** edit field mobile ([c1b36db](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c1b36db3eca91b960354470bc6ff6ae5e289c199)), closes [documentos#108](http://gitlab.meta.com.br/documentos/issues/108)
* **documentos#108:** edit field route ([9238092](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9238092525f6047ed023d1e927c4cf8859045193)), closes [documentos#108](http://gitlab.meta.com.br/documentos/issues/108)
* **documentos#108:** go to field location when you open it ([70bfe2b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/70bfe2b20a4575e13081d2f32d09ceab9cbb61fe)), closes [documentos#108](http://gitlab.meta.com.br/documentos/issues/108)
* **documentos#108:** styles tweaks ([546fba1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/546fba1c6f09b80f11fada86dadff5aab8224d2d)), closes [documentos#108](http://gitlab.meta.com.br/documentos/issues/108)
* **documentos#308:** changed mobile navigation ([6a06eee](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/6a06eee9034387bee975cf9eab8c20bfe960f2df)), closes [documentos#308](http://gitlab.meta.com.br/documentos/issues/308)
* **documentos#360:** created delivery locations ([295d06d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/295d06d08a8bd875612743c0a181eff07350ef85)), closes [documentos#360](http://gitlab.meta.com.br/documentos/issues/360)
* **documentos#385:** adds empty state fields ([34498ba](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/34498ba9e3f4055595fbbaacb51080c5f4e6e470)), closes [documentos#385](http://gitlab.meta.com.br/documentos/issues/385)
* **documentos#385:** fixes height of list ([488d112](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/488d1123c49bf31b76c57ea1dcc9fc3d69546ce7))
* **documentos#385:** fixes size ([7ef96d3](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7ef96d327a9b404e746683eee7318b72994fe535))
* **documentos#385:** function fix ([9239ee6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9239ee6bd77b79c57fc744c4f21b786051430434)), closes [documentos#385](http://gitlab.meta.com.br/documentos/issues/385)
* **documentos#393:** social network finished on mobile devices ([e838996](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e838996f44d0b26d857c47b062b6b1ecf0bdb809)), closes [documentos#393](http://gitlab.meta.com.br/documentos/issues/393)
* **documentos#400:** added empty states ([ec28dc9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ec28dc91cc111d229e4d0a981c7acfaf378b984c)), closes [documentos#400](http://gitlab.meta.com.br/documentos/issues/400)
* **documentos#402:** editing growing season ([321da9c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/321da9cb75b830d048c43afa491b4f2911e58b03)), closes [documentos#402](http://gitlab.meta.com.br/documentos/issues/402)
* **documentos#403:** removing crop from field ([69e1fc4](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/69e1fc43d2b83ecf235e7ce9a9cb5b7e5facc74a)), closes [documentos#403](http://gitlab.meta.com.br/documentos/issues/403)
* **documentos#426:** integrated with List component ([d35f7b3](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d35f7b33291afd1c4ae1f806b45bf3f52414315e)), closes [documentos#426](http://gitlab.meta.com.br/documentos/issues/426)
* **documentos#451:** created feed on mobile ([90668fe](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/90668fe9e1f1a0924d9e36729b427167cff8ba42)), closes [documentos#451](http://gitlab.meta.com.br/documentos/issues/451)
* **documentos#451:** created publish screen for mobile ([626b575](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/626b575f4021c206b0900c884d2f943d5238c47f)), closes [documentos#451](http://gitlab.meta.com.br/documentos/issues/451)
* **documentos#452:** adds defaultProps ([8f1e957](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8f1e9571a2e4489c3dea503443b9743f228006a2)), closes [documentos#452](http://gitlab.meta.com.br/documentos/issues/452)
* **documentos#452:** localization of moment string ([f3a4d14](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f3a4d1441f94ac8fb57387f28257eae37ada8165)), closes [documentos#452](http://gitlab.meta.com.br/documentos/issues/452)
* **documentos#455:** changed sowing year from monthYear to year ([e563242](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e56324297b37ed0eb3a20a36ee7aa7af808d831b)), closes [documentos#455](http://gitlab.meta.com.br/documentos/issues/455)
* **documentos#459:** created social and notifications screens ([98e93e7](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/98e93e7ae3c6e4c8f6cda5ace7518ee36cc86495)), closes [documentos#459](http://gitlab.meta.com.br/documentos/issues/459)
* **documentos#459:** created social network screen ([862b5e9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/862b5e9024c0da5c96e54f83583c7f8a6bb9e926)), closes [documentos#459](http://gitlab.meta.com.br/documentos/issues/459)
* **doucmenots#453:** removes duplicated T ([c9d2e29](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c9d2e29a12c59f04b11c06b265495c8bc94bf6c2)), closes [doucmenots#453](http://gitlab.meta.com.br/doucmenots/issues/453)





# [0.7.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.6.0...v0.7.0-alpha.0) (2020-11-27)

**Note:** Version bump only for package @smartcoop/mobile-containers





# [0.6.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.5.3...v0.6.0) (2020-11-27)


### Bug Fixes

* **#307:** adjustment to validate if there are items in the array ([fc26fa3](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/fc26fa33ab0df6a99db821804e985279b66b9416)), closes [#307](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/307) [#92](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/92)
* **documentos#107:** changes name of function (property->organization) ([8413567](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8413567826dbfcdb397546813b500b9b5efee945)), closes [documentos#107](http://gitlab.meta.com.br/documentos/issues/107)
* **documentos#107:** fragmented quotes screen ([ba3ba93](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ba3ba93d6731c0832b89353250c6eb7ba132637a)), closes [documentos#107](http://gitlab.meta.com.br/documentos/issues/107)
* **documentos#324:** removed field property from schema for mobile ([b137514](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b13751462dd0a257447cc89b0c1edd7596214c30)), closes [documentos#324](http://gitlab.meta.com.br/documentos/issues/324)
* **documentos#344:** fixed mobile change organization modal ([0a2c0c7](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0a2c0c77e689648c078b66191fc62ee1aa0eb7da))
* **documentos#353:** removed all others "to start" ([a65f078](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a65f078e1b41b2274c1f3e67ea25894d945ec912)), closes [documentos#353](http://gitlab.meta.com.br/documentos/issues/353) [#120](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/120)
* **doumentos#371:** remove unused alert ([d1d8756](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d1d8756c9321a7983637491206ef3b95c01aa805)), closes [doumentos#371](http://gitlab.meta.com.br/doumentos/issues/371)


### Features

* **#282:** add route the new screen, and add new screen ([c06ce09](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c06ce0927b0ae17edf5740095a0d1c4ac16e6641)), closes [#282](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/282) [#103](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/103)
* **#282:** adds new button and function ([bbaa514](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/bbaa5143834c002657c331b0fa2d4ae2509e5686)), closes [#282](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/282) [#103](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/103)
* **#282:** ajustment, remove handleSubmit ([3f13e0f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3f13e0f5a54c1eaaebf9462d47805769c77f0c66)), closes [#282](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/282) [#103](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/103)
* **#320:** uncomment code ([cb7563c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/cb7563c87d49970c5cd454941171a12fc44e061e)), closes [#320](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/320) [#115](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/115)
* **#332:** submit on click enter ([b6897da](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b6897dac4b7eb1f97ca2bf3e16e7c72135050a4e)), closes [#332](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/332) [#107](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/107)
* **documentos#107:** adds button to commercialization ([1d2e39a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1d2e39a0408ad7ec58134c13c565d1bda3cd2b6c)), closes [documentos#107](http://gitlab.meta.com.br/documentos/issues/107)
* **documentos#107:** adds debounce in search inputs ([1c1abef](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1c1abef032e0df481abdcaaa338455ea04ca717b)), closes [documentos#107](http://gitlab.meta.com.br/documentos/issues/107)
* **documentos#107:** adds fragment ([43a0d01](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/43a0d01dbaa797ee8174f7d7fa09ebdb2c8df817)), closes [documentos#107](http://gitlab.meta.com.br/documentos/issues/107)
* **documentos#107:** adds home screen to commercialization ([9ccfa5f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9ccfa5f094a482dba5cd14626dcc3bf486c5e2ab)), closes [documentos#107](http://gitlab.meta.com.br/documentos/issues/107)
* **documentos#107:** adds Home screen to commercialization ([84f31b7](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/84f31b7c001c38c10c6e92a14643d41506878b2e)), closes [documentos#107](http://gitlab.meta.com.br/documentos/issues/107)
* **documentos#107:** hide search in account balance ([0afa0ad](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0afa0ad3eb855da843361ed753fa150d0da89e13)), closes [documentos#107](http://gitlab.meta.com.br/documentos/issues/107)
* **documentos#107:** margin in search ([8889dae](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8889daedc6dfbc8a4471b5004b0645983594063f)), closes [documentos#107](http://gitlab.meta.com.br/documentos/issues/107)
* **documentos#107:** populates list ([5dadeeb](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/5dadeeb5ce4132191ef29a9fe73b3667ba4b2ca6)), closes [documentos#107](http://gitlab.meta.com.br/documentos/issues/107)
* **documentos#108:** adds list in mobile ([40bff51](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/40bff512d1eadd16fac0b511570842b17ead4530)), closes [documentos#108](http://gitlab.meta.com.br/documentos/issues/108)
* **documentos#108:** initialization of account balance list in mobile ([12a2b73](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/12a2b73e03d13bb4b6b243a89b0f5fcbae94bcfc)), closes [documentos#108](http://gitlab.meta.com.br/documentos/issues/108)
* **documentos#156:** changed button ([0747dbb](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0747dbb37e79c548b8db90eb860139c35e93ca06)), closes [documentos#156](http://gitlab.meta.com.br/documentos/issues/156)
* **documentos#16:** added current field reset and field details ([7d9e830](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7d9e830c0157bf163712c66c86403eaded7efdf9)), closes [documentos#16](http://gitlab.meta.com.br/documentos/issues/16)
* **documentos#16:** added other to crop in growing season form ([6bcab63](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/6bcab63b36637f9bc7d4f355756912d173b964d9)), closes [documentos#16](http://gitlab.meta.com.br/documentos/issues/16)
* **documentos#16:** adds flatlist to fieldlistscreen ([1136995](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1136995441d1791088d9aa56784447cd33597ed9)), closes [documentos#16](http://gitlab.meta.com.br/documentos/issues/16)
* **documentos#16:** adds onRefresh ([34e37c2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/34e37c219f50bc90ef587abc7c16b9f1f8d64f22)), closes [documentos#16](http://gitlab.meta.com.br/documentos/issues/16)
* **documentos#16:** created growing season form ([3f963e2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3f963e2f60fabec572979f7fb2b03c1afb84b41a)), closes [documentos#16](http://gitlab.meta.com.br/documentos/issues/16)
* **documentos#16:** created growing season register screen ([34698f6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/34698f65b54fd72b4efb5acc76ad3f3dc5816697)), closes [documentos#16](http://gitlab.meta.com.br/documentos/issues/16)
* **documentos#16:** creates fieldlist ([7a9074e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7a9074ef32c1fa3e4f3647fce3ff1a242b8f0c5c)), closes [documentos#16](http://gitlab.meta.com.br/documentos/issues/16)
* **documentos#16:** style tweaks ([bb7c6b6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/bb7c6b602a8821e7a3cff17872031503ed23b286)), closes [documentos#16](http://gitlab.meta.com.br/documentos/issues/16)
* **documentos#194:** added CompanyName as an alternative to TradeName ([b2d0a3d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b2d0a3dea027a639117c34f0f050e228bd6c06a6)), closes [documentos#194](http://gitlab.meta.com.br/documentos/issues/194)
* **documentos#194:** improved quotation translation ([f5c7550](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f5c7550fb08c68c7fb4132691a66e233431f5483)), closes [documentos#194](http://gitlab.meta.com.br/documentos/issues/194)
* **documentos#273:** added picker into date input ([e4cf2c7](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e4cf2c7048e68037962dbe0948ae241c78582114)), closes [documentos#273](http://gitlab.meta.com.br/documentos/issues/273)
* **documentos#286:** drawing field on mobile device ([a057a07](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a057a070ddbf50c4f633c65f2c20f68caea519ff)), closes [documentos#286](http://gitlab.meta.com.br/documentos/issues/286)
* **documentos#370:** craeted polygon to svg for mobile ([20de6d0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/20de6d0b22399c622b9a5ff06d4b3837344e8585)), closes [documentos#370](http://gitlab.meta.com.br/documentos/issues/370)
* **documentos#371:** adds snackbar to field feedback ([3a5d4df](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3a5d4df92576fe3e6c6e14bf8d844b9607974401)), closes [documentos#371](http://gitlab.meta.com.br/documentos/issues/371)
* **documentos#371:** redirects to field list when a field is registered ([5b301d1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/5b301d1bc3c13d20795a81a888c10a6c02f575a2)), closes [documentos#371](http://gitlab.meta.com.br/documentos/issues/371)



# [0.6.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.5.0...v0.6.0-alpha.0) (2020-11-10)





# [0.6.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.5.0...v0.6.0-alpha.0) (2020-11-10)
## [0.5.3](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.5.2...v0.5.3) (2020-11-11)


### Bug Fixes

* **staging:** fixed bugs from release 5 ([aa1cae0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/aa1cae08a05c9653ccaf08d49b3bd630d3c4cd5a))





## [0.5.2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.5.1...v0.5.2) (2020-11-11)


### Bug Fixes

* **staging:** fixed bugs for release 5 ([f9e8b8d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f9e8b8d6a8e0f9154b2b0ab11bcebda699d322d5))





## [0.5.1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.5.0...v0.5.1) (2020-11-10)

**Note:** Version bump only for package @smartcoop/mobile-containers





# [0.5.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.5.0-alpha.0...v0.5.0) (2020-11-10)



### Bug Fixes

* **ci-cd:** release pipeline ([57ca450](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/57ca45049725fb6d8fb2721b0b478c959047b41c))
* **documentos#236:** fixed eslint warning ([b1fcd39](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b1fcd39cd6c067886527ac1ae3831a6f9221f887))


### Features

* **documentos#101:** created basic layout in dashboard screen ([04e857d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/04e857dba4ed20752af94a05a6d6dcf189774a27)), closes [documentos#101](http://gitlab.meta.com.br/documentos/issues/101)
* **documentos#101:** created mobile navigation ([61b6e26](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/61b6e26770a454f968a7c314008f9af448c69752)), closes [documentos#101](http://gitlab.meta.com.br/documentos/issues/101)
* **documentos#101:** created profile screen for mobile ([3fd414d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3fd414d5d112b6bfc6a473d09249a04530da6cb0)), closes [documentos#101](http://gitlab.meta.com.br/documentos/issues/101)
* **documentos#236:** disabled send code by mail ([0f0b7ea](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0f0b7ea5846c0613c51110dc457c244907cd0bf7)), closes [documentos#236](http://gitlab.meta.com.br/documentos/issues/236)
* **documentos#28:** adds RS as default location in property's maps ([09d8432](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/09d8432f4464e82275b3f43014545c78992735a8)), closes [documentos#28](http://gitlab.meta.com.br/documentos/issues/28)
* **documentos#28:** adds RS as default location in web ([20841eb](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/20841eb1ff0b5cd950b134f68ae544d793064498)), closes [documentos#28](http://gitlab.meta.com.br/documentos/issues/28)
* **documentos#72:** adds functionality to mobile button ([54218de](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/54218de971c3f4db0c384540d55ff845a2f3db81)), closes [documentos#72](http://gitlab.meta.com.br/documentos/issues/72)
* **documentos#72:** creates organization modal in mobile ([7dc7635](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7dc76355bd89583d23c0a7fd0ae86b107d968e39)), closes [documentos#72](http://gitlab.meta.com.br/documentos/issues/72)
* **documentos#72:** footer validation in mobile modal ([e6e9139](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e6e9139dd40aa21e4c70aa6f13f4e1fc7422dbae)), closes [documentos#72](http://gitlab.meta.com.br/documentos/issues/72)
* **documentos#72:** wip: added footer to mobile modal ([12becf1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/12becf1b0e824401c7e031f5369c8e2f4eb32f4d)), closes [documentos#72](http://gitlab.meta.com.br/documentos/issues/72)





# [0.5.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.4.0...v0.5.0-alpha.0) (2020-10-27)

**Note:** Version bump only for package @smartcoop/mobile-containers





# [0.4.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.3.0-alpha.0...v0.4.0) (2020-10-27)



### Bug Fixes

* **documentos#10:** changed update user to user reducer ([5d941f6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/5d941f61e06d9356b6165fefeda4739d8691df38)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#10:** fixing redux ([ae9e830](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ae9e8309a9840b467c9fbbb6eb71408b0617a192))
* **documentos#10:** passing mode params to reset code screen ([7f93272](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7f93272abe1be7b67bc471b18389b7b7bcc21592)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#10:** showing organization registry for user ([432b577](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/432b5776217e739bfb25793856950b6fbc9894eb)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#132:** fix fonts ([86411d9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/86411d952258466480657462de9c6d8c0c55f059))
* **documentos#135:** removed fonts from react native ([663f986](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/663f986d2ef5133008e999b1164e57140b530201)), closes [documentos#135](http://gitlab.meta.com.br/documentos/issues/135)
* **documentos#139:** asking for permissions ([0c28ead](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0c28ead1ba34822b5383b3046b9bf6946456a14f)), closes [documentos#139](http://gitlab.meta.com.br/documentos/issues/139)
* **documentos#139:** requesting android permission ([c12f4e8](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c12f4e82115ebc9d9514c75d849f053590eb1245)), closes [documentos#139](http://gitlab.meta.com.br/documentos/issues/139)
* **documentos#185:** fixed ios layout ([a408f62](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a408f621302bdabcc2f38e6284762a3aa6cbed39))
* **documentos#25:** fixed mobile Field container to field ([f5caf52](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f5caf52aa45eed8d53091b9ca66c317e17d177ad))
* **documentos#25:** fixed mobile field form ([6fb00e3](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/6fb00e3038d99daea2714ba11345316452f60162))
* **documentos#25:** fixed save button text ([3638b9a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3638b9a5ef4655ecf2e017485a79ba311ce3fd41))
* **documentos#28:** adds more padding in labels ([470c5c9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/470c5c9a6bf4eb9097eb2549c1287225c12b14d8)), closes [documentos#28](http://gitlab.meta.com.br/documentos/issues/28)
* **documentos#28:** changed address property fields names ([7258879](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/72588794f72a4cd5967ed2942325a5b00592da40)), closes [documentos#28](http://gitlab.meta.com.br/documentos/issues/28) [#34](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/34)
* **documentos#28:** fixed android localization issue ([4343be3](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4343be34f8ec03358bc4e93fd19de47629cc7862))


### Features

* **documentos#10:** validating password for mobile ([bfa3dae](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/bfa3dae9d19784c36ffd899ce4d08d765da01ce7)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#132:** added fonts to android ([653a067](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/653a067f14359e9d2603dfe60314b23ff81c82ab)), closes [documentos#132](http://gitlab.meta.com.br/documentos/issues/132) [#31](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/31)
* **documentos#132:** wip: Added fonts to mobile ([a1212ba](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a1212bac57fda5018518af4049d8bac2aff21fbe)), closes [documentos#132](http://gitlab.meta.com.br/documentos/issues/132) [#31](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/31)
* **documentos#132:** wIP: Applicated fonts to mobile ([193cd20](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/193cd20899b60aa36eb7d12d3c12121413b0cea6)), closes [documentos#132](http://gitlab.meta.com.br/documentos/issues/132) [#31](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/31)
* **documentos#135:** redux offline queue ([7deba4f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7deba4f419b4104188247143901b1d7ea38de210)), closes [documentos#135](http://gitlab.meta.com.br/documentos/issues/135)
* **documentos#135:** sorting out user reducer ([f49641f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f49641f9fe65c0e080a2d5750ec281fd72f52843)), closes [documentos#135](http://gitlab.meta.com.br/documentos/issues/135)
* **documentos#18:** wIP:Created mobile datepicker ([be26632](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/be266326f898865711616fe260d7ecf1a6dafaaa)), closes [documentos#18](http://gitlab.meta.com.br/documentos/issues/18) [#10](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/10)
* **documentos#185:** created basic navigation for web and mobile ([4b144ed](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4b144edcd2c7d7338100f5da5105df4ccec87969)), closes [documentos#185](http://gitlab.meta.com.br/documentos/issues/185)
* **documentos#25:** added fieldRegister formReset and successMessage ([0158132](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0158132012370c29dc2db5bae52ca8c3abc32189)), closes [documentos#25](http://gitlab.meta.com.br/documentos/issues/25)
* **documentos#25:** created mobile field initial screen ([e5e9d07](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e5e9d07ced3a4a925fa81b30da3422ec36297650)), closes [documentos#25](http://gitlab.meta.com.br/documentos/issues/25)
* **documentos#28:** added initial location to property localization ([db9656e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/db9656e0a7d042b0c0fdc59a9339a7dc31a39ab1)), closes [documentos#28](http://gitlab.meta.com.br/documentos/issues/28)
* **documentos#28:** added resetSchema to propertyIdentificationForm ([6d60450](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/6d6045085ef363ef29ddf7225bef58361c0dbfd4)), closes [documentos#28](http://gitlab.meta.com.br/documentos/issues/28)
* **documentos#28:** fixed property identification form revalidation ([3f03852](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3f0385262fdef9685b8603ce719e114bb4361bf0))
* **documentos#28:** wIP: start map in mobile ([f70c2c7](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f70c2c7d6254a7d839434f5c39ac585c51127785)), closes [documentos#28](http://gitlab.meta.com.br/documentos/issues/28)
* **documentos#53:** added unit to area in field register ([d9b3f83](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d9b3f83870c3acb96458901ce94a6ddb8a9905b5)), closes [documentos#53](http://gitlab.meta.com.br/documentos/issues/53)





# [0.3.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.2.0...v0.3.0-alpha.0) (2020-09-28)

**Note:** Version bump only for package @smartcoop/mobile-containers





# [0.2.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.2.0-alpha.0...v0.2.0) (2020-09-28)


### Bug Fixes

* **documentos#10:** header title modal for mobile ([be8c4e0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/be8c4e053858df25b186a264fdcea24c1b3800d1)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#10:** scroll view area ([2370ff1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2370ff1560c793985b0f27a1a20501bbffa1e685)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#25:** removed mobile fieldform mdx documentos[#60](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/60) ([d01807e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d01807e4f3061d23101de92eb32597e1a94b7588)), closes [documentos#25](http://gitlab.meta.com.br/documentos/issues/25)
* **documentos#28:** removed asd ([82bd041](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/82bd0416c911f53e3292eaa07b0fd5215a54f989)), closes [documentos#28](http://gitlab.meta.com.br/documentos/issues/28)
* **documentos#33:** rm loader changes made in login screen ([5edf939](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/5edf93948007f72435c48a3216fda4cf82297e1f)), closes [documentos#33](http://gitlab.meta.com.br/documentos/issues/33)


### Features

* **#57:** created SuccessModal container on mobile ([53ecb6c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/53ecb6c16fd43613a7b3705ca136c43218cdc214)), closes [#57](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/57) [#18](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/18)
* **#57:** fixed Success Modal on web ([300c218](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/300c218da74a526f869128424764504de0bb0ffa)), closes [#18](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/18)
* **documentos#10:** added loader after submit signup form ([f73783e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f73783e063374954f51d4d1193cac555ec5bbb05)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#10:** auto fill forms with user api data ([9909ac6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9909ac6498d857e0f5ecb587d0334fab4b064036)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#10:** changing reset code because api changed ([42d201d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/42d201d983b0fd1be00356119205b6ad56034d97)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#10:** craeted loading and error modal ([34bc091](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/34bc091a4766d8b07fda331faf0cbec7c81a7497)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#10:** created identification and address screen on web ([57b0f74](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/57b0f7432fdabf9ac59cdfa52760d1a56a1e49f2)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#10:** created web login screen ([34ed647](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/34ed647f1ab16e0c1dc45db8455239335ed6286d)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#10:** created web screens ([496ac64](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/496ac645455e7c7e127a68385e9d8fa8b3b4b9f6)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#10:** finished onboarding w/ recover password for mobile ([d8d7623](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d8d7623f801f871a0473a9d7f1334a975a379207)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#10:** finished productor onboarding ([4eb4a89](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4eb4a89ed42ba9a8161999c29bf941a95c5c1119)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#10:** hidding header from error and loader modals mobile ([1b73559](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1b735596b4e1454c5e2b18fa314c20ea82565827)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#10:** login for mobile ([39b6072](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/39b60720db516f0b8e669d8691d4f61e404144e2)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#25:** changed mobile field form style ([a4fc141](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a4fc1414a535a3e9120f1f51ee837054e27d559e)), closes [documentos#25](http://gitlab.meta.com.br/documentos/issues/25)
* **documentos#25:** created field form documentos[#60](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/60) ([3ea4e41](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3ea4e412579de3801b4c94168212192098cda2da)), closes [documentos#25](http://gitlab.meta.com.br/documentos/issues/25)
* **documentos#28:** added form label to authenticated layout theme ([ece1889](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ece1889d8353472e4ec1be1d842f5bfeb914963b)), closes [documentos#28](http://gitlab.meta.com.br/documentos/issues/28)
* **documentos#28:** added mobile property activity details form ([132d4cb](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/132d4cba949c80064d257dc27eaeb4ef4e3d7004)), closes [documentos#28](http://gitlab.meta.com.br/documentos/issues/28)
* **documentos#28:** created dynamic form documentos[#67](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/67) ([5173d0b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/5173d0b5880863112ab156fba61035de810134b9)), closes [documentos#28](http://gitlab.meta.com.br/documentos/issues/28)
* **documentos#28:** created property screens for mobile ([10abfa0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/10abfa083e93e992b3fac4c4ec49966b44e28c2c)), closes [documentos#28](http://gitlab.meta.com.br/documentos/issues/28)
* **documentos#28:** created web/mobile property identification form ([0882def](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0882def6d575f26dd51bfffabf4af7f61fb8d36d)), closes [documentos#28](http://gitlab.meta.com.br/documentos/issues/28)
* **documentos#28:** created web/mobile property identification form ([476f89f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/476f89f70d2d16e44512a823bbd1cde7724c3440)), closes [documentos#28](http://gitlab.meta.com.br/documentos/issues/28)
* **documentos#28:** fixed mobile property identification form ([1b04b3e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1b04b3ea87c08f50017f91937695584a9f4f8ba7))
* **documentos#28:** navigating from activity to activity details ([27e88bd](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/27e88bddfce7df3b5d6574660f761c5e21d95b85)), closes [documentos#28](http://gitlab.meta.com.br/documentos/issues/28)
* **documentos#28:** turn property activity details on for mobile ([309fbe3](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/309fbe3b4378eea8dc21b317f5bd4449b75a568b)), closes [documentos#28](http://gitlab.meta.com.br/documentos/issues/28)
* **documentos#32:** added database into web ([1f6ad73](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1f6ad73cc2f1de976bce6aab7835337f1f8d2e05)), closes [documentos#32](http://gitlab.meta.com.br/documentos/issues/32)
* **documentos#32:** removed watermelon. added redux saga ([cd97772](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/cd977725490d8654afc2dad1a0e7bc3d6b16b4dd)), closes [documentos#32](http://gitlab.meta.com.br/documentos/issues/32)
* **documentos#40:** removed Smartcoop text from GuestCard ([cef7dcf](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/cef7dcf30f65da670d1433da7fa4969da75c0066)), closes [documentos#40](http://gitlab.meta.com.br/documentos/issues/40) [#14](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/14)





# [0.2.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.1.0...v0.2.0-alpha.0) (2020-09-13)

**Note:** Version bump only for package @smartcoop/mobile-containers





# [0.1.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.1.0-alpha.0...v0.1.0) (2020-09-13)


### Bug Fixes

* **documentos#10:** fixed import useImperativeHandle from react ([286e6d2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/286e6d28252485f333d99f725429d6fd93b16761))
* **documentos#10:** fixed select \o/ documentos[#30](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/30) ([56ab086](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/56ab08628ed8b0859112cc76f6b2da989098d5a0))
* **documentos#10:** fixed select \o/ documentos[#30](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/30) ([c043c42](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c043c4211b46564f7bf0a50b323a8043d1dfbccc))
* **documentos#10:** removed space in resetcodeform folder name ([b340554](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b3405545e50036d87b459ed4ff129e321e3cf915)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)


### Features

* **documentos#10:** added address form documentos[#51](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/51) ([a062106](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a06210666826d66582caff88756428ca770a2621)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#10:** added createpassword screen ([fed3b11](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/fed3b11499d134768ab2b77e3da0130e1c5ec184)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#10:** added number validation addressForm documentos[#51](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/51) ([3f416bc](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3f416bcee40bf11bf7586659a5aa53da59a86f85)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#10:** added producer identification form documentos[#51](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/51) ([c49234c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c49234cb768879b3a56da13bcfff3b511772bec2)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#10:** added signup form documentos[#51](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/51) ([cb1c455](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/cb1c4555593c10e3f11a613e32a4060454e9046c)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#10:** added textunderlined styled component ([30b18dd](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/30b18dde68b27ebc57ea23d96da41a8f2da3adb7)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#10:** created cooperatives screen ([64bbac6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/64bbac6146a39901b28e2c406f50675757eab107)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#10:** created create password form ([a181c58](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a181c5891bfa34a21b64e338699d61c524d6c77a)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#10:** created identification screen ([02919dc](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/02919dcd19bd38e94b40047c9766523e53cd7d86)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#10:** created identification screen ([5342ee6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/5342ee65eb7129e62b3de0367b3ef086c8e05a4f)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#10:** created reset code form ([8f53188](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8f53188f6eaa28929e6034c0cc7700b5b96ad45b)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#10:** created reset code screen ([50c1030](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/50c10306ebc726aa901d804c675b2c822fda1b85)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#10:** created resetcode screen ([a179d84](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a179d84abf0a6c03f197ce635896509f74f39b56)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#10:** detach apps screens ([b7d9d50](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b7d9d507c89bd74a89c9addb28fc3e9bdd810e91)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#10:** detach apps screens ([52623d9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/52623d9f4c2236f345f2315faab7c76541e18fc1)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#10:** dynamically form button ([7d98394](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7d98394e345b16c24abcd0e05e610f9f58bc3e0f)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#10:** finished onboard mocked ([b20abfa](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b20abfaf5a51d791934136a03c7c4714e1cda6b3)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#10:** login and signup screens ([e4b9d02](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e4b9d0278f4785aa31928f9ec3bc6e5de4ff98e2)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#10:** login and signup screens ([4558694](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4558694308db08a04873004ca9e0db6dce6bbba7)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#10:** rebased with develop ([13780a6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/13780a6bd6fe1ff605aeda52dc096d94d93e7a8d)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#10:** selects ok \o/ ([7f691f4](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7f691f4b648911711a598bb30c447533bf29ad88)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
