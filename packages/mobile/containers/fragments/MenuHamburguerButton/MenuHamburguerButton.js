import React from 'react'
import { TouchableOpacity } from 'react-native'

import { DrawerActions } from '@react-navigation/native'
import PropTypes from 'prop-types'

import { hamburguerMenu } from '@smartcoop/icons'
import Icon from '@smartcoop/mobile-components/Icon'
import colors from '@smartcoop/styles/colors'

const MenuHamburguerButton = ({ navigation }) => (
  <TouchableOpacity
    style={ { paddingLeft: 20 } }
    onPress={ () => navigation.dispatch(DrawerActions.toggleDrawer) }
  >
    <Icon icon={ hamburguerMenu } color={ colors.white } size={ 22 } />
  </TouchableOpacity>
)

MenuHamburguerButton.propTypes = {
  navigation: PropTypes.object.isRequired
}

export default MenuHamburguerButton
