/* eslint-disable react/no-this-in-sfc */
import React, { useCallback, useState, useEffect, useRef } from 'react'
import { View } from 'react-native'
import { useSelector } from 'react-redux'

import PropTypes from 'prop-types'

import { useT } from '@smartcoop/i18n'
import Chart from '@smartcoop/mobile-components/Chart'
import Loader from '@smartcoop/mobile-components/Loader'
import { selectReloadData } from '@smartcoop/stores/property/selectorProperty'
import { createPropertyGrowingSeasonsChartOptions } from '@smartcoop/utils/charts'


const PropertyGrowingSeasonsChart = (props) => {
  const { property, forceRefresh, onLoaded } = props

  const t = useT()

  const mounted = useRef(false)

  const [loading, setLoading] = useState(false)
  const reloadData = useSelector(selectReloadData)
  const [chartOptions, setChartOptions] = useState({})

  const getChartOptions = useCallback(
    async () => {
      try {
        setLoading(true)
        const options = await createPropertyGrowingSeasonsChartOptions({
          propertyId: property.id,
          t
        })
        if (mounted.current) {
          setChartOptions({
            ...options,
            legend: {
              ...options.legend,
              align: 'left',
              verticalAlign: 'top',
              layout: 'horizontal'
            }
          })
        }
      } finally {
        setLoading(false)
        onLoaded(`growing-seasons-chart-${  property.id }`)
      }
    },
    [onLoaded, property.id, t]
  )

  useEffect(
    () => {
      if (property.id || reloadData) {
        getChartOptions()
      }
    },
    [getChartOptions, property.id, forceRefresh, reloadData]
  )

  useEffect(() => {
    mounted.current = true
    return () => {
      mounted.current = false
    }
  }, [])

  return (
    <View style={ { flex: 1, minHeight: 300, justifyContent: 'center' } }>
      {loading
        ? <Loader width={ 50 } />
        : (
          <Chart
            options={ chartOptions }
            containerProps={ { style: { height: 250 } } }
          />
        )
      }
    </View>
  )
}

PropertyGrowingSeasonsChart.propTypes = {
  property: PropTypes.object.isRequired,
  forceRefresh: PropTypes.bool,
  onLoaded: PropTypes.func
}
PropertyGrowingSeasonsChart.defaultProps = {
  forceRefresh: false,
  onLoaded: () => {}
}

export default PropertyGrowingSeasonsChart
