export default [
  { color: '#D70000', text: '< 26' },
  { color: '#FF7A00', text: '26 - 57' },
  { color: '#FFE600', text: '58 - 96' },
  { color: '#0C9300', text: '97 - 145' },
  { color: '#00B0D7', text: '146 - 203' },
  { color: '#0070D7', text: '204 - 271' },
  { color: '#0022D7', text: '272 - 352' },
  { color: '#9E00FF', text: '> 252' }
]
