import React from 'react'

import map from 'lodash/map'

import scale from './scale'
import { Container, Item, Description } from './styles'

const RainScaleFragment = () => (
  <Container>
    { map(scale, item => (
      <Item key={ item?.color } color={ item?.color }>
        <Description>
          {item?.text}
        </Description>
      </Item>
    )) }
  </Container>
)

export default RainScaleFragment
