import styled from 'styled-components'

export const Container = styled.View`
  width: 100%;
  display: flex;
  flex-direction: row;
`

export const Item = styled.View`
  flex: 1;
  background-color: ${ ({ color }) => color };
  padding: 2px;
  font-size: 10px;
  align-items: center;
  justify-content: center;
`

export const Description = styled.Text`
  text-align: center;
  color: white;
  font-weight: bold;
`
