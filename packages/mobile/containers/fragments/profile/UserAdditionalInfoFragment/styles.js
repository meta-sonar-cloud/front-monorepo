import styled from 'styled-components'

export const Container = styled.View`
  display: flex;
  /* background: tomato; */
  flex-direction: column;
  flex: 1;
  margin-top: 20px;
`

export const RowContainer = styled.View`
  display: flex;
  flex-direction: row;
`

export const Item = styled.View`
`

export const AvatarNameContainer = styled.View`
`

export const ButtonContainer = styled.View`
`

export const FirstRowContainer = styled(RowContainer)`
  justify-content: space-between;
`

export const Text = styled.Text`
  font-size: 16px;
`

export const Name = styled(Text)`
  font-weight: 700;
  font-size: 17px;
`
