import React, { useMemo, useCallback } from 'react'
import { useSelector } from 'react-redux'

import PropTypes from 'prop-types'

import { useDialog } from '@smartcoop/dialog'
import { useT } from '@smartcoop/i18n'
import { organizationRounded, signature } from '@smartcoop/icons'
import Button from '@smartcoop/mobile-components/Button'
import Divider from '@smartcoop/mobile-components/Divider'
import Icon from '@smartcoop/mobile-components/Icon'
import ChangeSignatureModal from '@smartcoop/mobile-containers/modals/ChangeSignatureModal/ChangeSignatureModal'
import { selectAllModules } from '@smartcoop/stores/authentication/selectorAuthentication'
import { colors } from '@smartcoop/styles'

import AddressCardFragment from '../AddressCardFragment'
import ListFragment from '../ListFragment'
import { Container, Item } from './styles'

const UserAdditionalInfoFragment = (props) => {
  const { user, userOrganizations } = props

  const t = useT()
  const { createDialog } = useDialog()
  const allModules = useSelector(selectAllModules)

  const openChangeSignatureModal = useCallback(() => {
    createDialog({
      id: 'change-signature',
      Component: ChangeSignatureModal
    })
  }, [createDialog])

  const showEletronicSignatureButton = useMemo(
    () => allModules.some((item) => item.slug === 'commercialization'),
    [allModules]
  )

  const address = useMemo(
    () => ({
      state: user.state ?? null,
      city: user.city ?? null,
      district: user.district ?? null,
      street: user.street ?? null,
      number: user.number ?? null,
      postalCode: user.postalCode ?? null
    }),
    [user]
  )

  return (
    <Container>
      <Divider />
      <Item>
        <ListFragment
          listId="id"
          listKey="companyName"
          title={ t('associated organizations') }
          icon={ organizationRounded }
          list={ userOrganizations }
        />
        {showEletronicSignatureButton && (
          <Button
            variant="outlined"
            style={ { margin: 20, padding: 8, borderColor: colors.lightGrey } }
            icon={ <Icon size={ 20 } icon={ signature } color={ colors.black } /> }
            backgroundColor={ colors.white }
            onPress={ openChangeSignatureModal }
            title={ t('eletronic signature') }
          />
        )}
      </Item>
      <Divider />
      <Item>
        <AddressCardFragment address={ address } />
      </Item>
    </Container>
  )
}

UserAdditionalInfoFragment.propTypes = {
  user: PropTypes.object.isRequired,
  userOrganizations: PropTypes.array.isRequired
}

export default UserAdditionalInfoFragment
