import React, { useMemo, useCallback, useState } from 'react'
import { Text , Platform } from 'react-native'
import { useDispatch } from 'react-redux'

import { useNavigation } from '@react-navigation/native'
import moment from 'moment/moment'
import PropTypes from 'prop-types'

import { forEach, isEmpty } from 'lodash'


import { useT } from '@smartcoop/i18n'
import {
  pencil
} from '@smartcoop/icons'
import AvatarInput from '@smartcoop/mobile-components/AvatarInput'
import Button from '@smartcoop/mobile-components/Button'
import Icon from '@smartcoop/mobile-components/Icon'
import Loader from '@smartcoop/mobile-components/Loader'
import { useSnackbar } from '@smartcoop/snackbar'
import { UserActions } from '@smartcoop/stores/user'
import colors from '@smartcoop/styles/colors'
import {
  momentBackDateFormat,
  momentFriendlyDateFormat
} from '@smartcoop/utils/dates'
import { formatCpfCnpj , formatPhone } from '@smartcoop/utils/formatters'

import {
  Container,
  FirstRowContainer,
  Item,
  AvatarContainer,
  Top,
  InfoContainer,
  AvatarAndContent,
  Name,
  ButtonContainer
} from './styles'

const UserInfoFragment = (props) => {
  const {
    user: {
      name,
      dateOfBirth,
      email,
      cellPhone,
      document: doc,
      profilePhoto
    }
  } = props


  const [isAvatarLoading, setIsAvatarLoading] = useState(false)
  const dispatch = useCallback(useDispatch(), [])
  const snackbar = useSnackbar()
  const t = useT()
  const navigation = useNavigation()

  const formatted = useMemo(
    () => ({
      phone: formatPhone(cellPhone),
      document: formatCpfCnpj(doc),
      birthday: moment(dateOfBirth, momentBackDateFormat).format(momentFriendlyDateFormat)
    }),
    [cellPhone, doc, dateOfBirth]
  )

  const source = useMemo(
    () => (profilePhoto?.fileUrl ?? null), [profilePhoto]
  )

  const openEditProfileModal = useCallback(
    () => {
      navigation.navigate('EditProfile')
    },
    [navigation]
  )

  const onSuccessAvatarChange = useCallback(
    () => {
      dispatch(UserActions.loadUser(
        () => {
          setIsAvatarLoading(false)
          snackbar.success(
            t('your {this} was updated', {
              gender: 'female',
              this: t('profile picture', { howMany: 1 })
            })
          )
        },
        () => {
          setIsAvatarLoading(false)
        }
      ))
    }, [dispatch, snackbar, t]
  )

  const createFormData = useCallback(
    (selectedFiles = [], fileKey = 'upload', body = {}) => {
      const data = new FormData()

      forEach(selectedFiles, (file) => {
        data.append(fileKey, {
          name: file.fileName,
          type: file.type,
          uri:
            Platform.OS === 'android' ?
              file.uri : file.uri.replace('file://', '')
        })
      })

      Object.keys(body).forEach((key) => {
        data.append(key, body[key])
      })

      return data
    },
    []
  )

  const handleAvatarChange = useCallback(
    (file) => {
      if(!isEmpty(file)) {
        setIsAvatarLoading(true)
        dispatch(UserActions.saveUserPicture(
          createFormData([file]),
          () => onSuccessAvatarChange(),
          () => setIsAvatarLoading(false)
        ))
      }
    }
    , [createFormData, dispatch, onSuccessAvatarChange]
  )

  const renderAvatar = useMemo(
    () => (isAvatarLoading ?
      <Loader width={ 120 } style={ { padding: 0 } }/> :
      <AvatarInput src={ source } onChange={ handleAvatarChange } />)
    , [handleAvatarChange, isAvatarLoading, source]
  )

  return (
    <Container>
      <FirstRowContainer>
        <AvatarAndContent>
          <AvatarContainer>
            {renderAvatar}
          </AvatarContainer>
          <InfoContainer>
            <Top>
              <Name>
                {name}
              </Name>
              <ButtonContainer>
                <Button
                  variant="outlined"
                  style={ { marginRight: 5, padding: 8, borderColor: colors.lightGrey } }
                  icon={ <Icon size={ 14 } icon={ pencil } color={ colors.black } /> }
                  backgroundColor={ colors.white }
                  onPress={ openEditProfileModal }
                />
              </ButtonContainer>
            </Top>
            <Item>
              <Text>{formatted.birthday}   {formatted.document}</Text>
            </Item>
            <Item>
              <Text>{email}</Text>
            </Item>
            <Item>
              <Text>{formatted.phone}</Text>
            </Item>
          </InfoContainer>
        </AvatarAndContent>

      </FirstRowContainer>
    </Container>
  )
}

UserInfoFragment.propTypes = {
  user: PropTypes.object.isRequired
}

export default UserInfoFragment
