import { Dimensions } from 'react-native'

import styled from 'styled-components'

import colors from '@smartcoop/styles/colors'

export const Container = styled.View`
  display: flex;
  flex-direction: column;
  margin-top: 10px;
`

export const RowContainer = styled.View`
`

export const Item = styled.View`
  margin: 5px;
  margin-left: 0;
  margin-bottom: 0;
  flex: 1;
`

export const Top = styled.View`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`

export const InfoContainer = styled.View`
  display: flex;
  flex-direction: column;
  flex: 1;
  padding: 10px;
`

export const AvatarContainer = styled.View`
  padding-top: 0;
  display: flex;
  justify-content: center;
  align-items: center;
`

export const AvatarAndContent = styled.View`
  flex-direction: row;
  padding: 10px;
  max-width: ${ Dimensions.get('screen').width };
`

export const ButtonContainer = styled.View`
  /* width: fit-content; */
`

export const FirstRowContainer = styled(RowContainer)`
  justify-content: flex-start;
`

export const Text = styled.Text`
  font-size: 16px;
`

export const Name = styled(Text)`
  font-weight: 700;
  font-size: 17px;
  color: ${ colors.black };
  width: ${ Dimensions.get('screen').width / 2 };
  padding-right: 10px;
`
