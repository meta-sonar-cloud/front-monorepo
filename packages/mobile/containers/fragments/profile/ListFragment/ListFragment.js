import React from 'react'

import PropTypes from 'prop-types'

import map from 'lodash/map'

import Icon from '@smartcoop/mobile-components/Icon'

import { Container, Title, Label, List, Item } from './styles'

const ListFragment = ({ title, icon, list, listKey, listId }) => (
  <Container>
    <Title>{title}</Title>
    <List>
      {
        map(list, (item) => (
          <Item key={ item[listId] }>
            {icon && <Icon icon={ icon } />}
            <Label>{item[listKey]}</Label>
          </Item>
        ))
      }
    </List>
  </Container>
)

ListFragment.propTypes = {
  icon: PropTypes.any,
  list: PropTypes.array.isRequired,
  listKey: PropTypes.string.isRequired,
  listId: PropTypes.string.isRequired,
  title: PropTypes.string
}

ListFragment.defaultProps = {
  icon: null,
  title: ''
}


export default ListFragment
