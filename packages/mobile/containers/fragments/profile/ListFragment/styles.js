import styled from 'styled-components'

import { colors } from '@smartcoop/styles'


export const Container = styled.View`

`

export const Title = styled.Text`
  font-weight: 700;
  font-size: 17px;
  margin-left: 40px;
  color: ${ colors.black };
`

export const Label = styled.Text`
  margin-left: 8px;
  color: ${ colors.black };
`

export const List = styled.View`
  display: flex;
  flex-direction: column;
`

export const Item = styled.View`
  margin-top: 10px;
  display: flex;
  flex-direction: row;
  font-weight: 600;
  align-items: center;
  margin-left: 40px;
`
