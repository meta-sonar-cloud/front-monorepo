import React,
{ useMemo, useCallback, useState, useEffect } from 'react'

import { useNavigation } from '@react-navigation/native'
import PropTypes from 'prop-types'

import every from 'lodash/every'
import isEmpty from 'lodash/isEmpty'

import I18n, { useT } from '@smartcoop/i18n'
import {
  noAddress
} from '@smartcoop/icons'
import Address from '@smartcoop/mobile-components/Address'
import EmptyState from '@smartcoop/mobile-components/EmptyState'
import Loader from '@smartcoop/mobile-components/Loader'

import { Container, Label, AddressContainer } from './styles'

const AddressCardFragment = (props) => {
  const { address } = props
  const t = useT()
  const [addressState, setAddressState] = useState('loading')
  const navigation = useNavigation()

  const isAddressEmpty = useMemo(
    () => (
      every(address, isEmpty)
    ), [address]
  )

  const editAddress = useCallback(
    () => {
      navigation.navigate('EditAddress')
    }, [navigation]
  )


  useEffect(() => {
    isAddressEmpty ? setAddressState('empty') : setAddressState('loaded')
  }, [address, isAddressEmpty])

  const renderAddressCard = useMemo(
    () => (
      {
        loading: <Loader width={ 80 } />,
        empty: (
          <EmptyState
            text={ t('no address registered') }
            icon={ noAddress }
            iconProps={ {
              size: 120,
              style: { marginRight: 10 }
            } }
            action={ {
              text: t('register address'),
              onClick: editAddress
            } }
            style={ { alignContent: 'center' } }
          />),
        loaded: (
          <AddressContainer>
            <Address
              fields={ address }
              isUserAddress
              onEdit={  editAddress }
            />
          </AddressContainer>)

      }[addressState]
    ), [address, addressState, editAddress, t]
  )


  return (
    <Container>
      <Label>
        <I18n params={ { howMany: 2 } }>address</I18n>
      </Label>
      {renderAddressCard}
    </Container>
  )
}

AddressCardFragment.propTypes = {
  address: PropTypes.object.isRequired
}

export default AddressCardFragment
