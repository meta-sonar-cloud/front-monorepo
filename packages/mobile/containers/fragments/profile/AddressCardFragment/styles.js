import styled from 'styled-components'

import colors from '@smartcoop/styles/colors'

export const Container = styled.View`
  margin-top: 10px;
`

export const AddressContainer = styled.View`
  background-color: ${ colors.white };
  margin: 0 20px;
  border-radius: 5px;
`

export const Label = styled.Text`
  font-weight: 700;
  font-size: 17px;
  margin-left: 40px;
  color: ${ colors.black }
`
