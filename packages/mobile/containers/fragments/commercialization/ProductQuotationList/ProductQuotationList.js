import React, { useCallback, useEffect, useMemo } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import PropTypes from 'prop-types'

import { toNumber } from 'lodash'
import isEmpty from 'lodash/isEmpty'

import { useT } from '@smartcoop/i18n'
import List from '@smartcoop/mobile-components/List'
import { selectCurrentOrganization } from '@smartcoop/stores/organization/selectorOrganization'
import { ProductsQuotationActions } from '@smartcoop/stores/productsQuotation'
import { selectProductsQuotation } from '@smartcoop/stores/productsQuotation/selectorProductsQuotation'

import ProductQuotationItem from './ProductQuotationItem'

const ProductQuotationList = ({ params }) => {
  const dispatch = useCallback(useDispatch(), [])
  const t = useT()

  const productsQuotation = useSelector(selectProductsQuotation)
  const currentOrganization = useSelector(selectCurrentOrganization)

  const deps = useMemo(
    () => [currentOrganization, params],
    [currentOrganization, params]
  )

  const calcUnitValue = useCallback(
    (item) => (
      item.organizationProduct?.conversionFactor !== 1 &&
      item.organizationProduct?.conversionFactor !== undefined ?
        toNumber(item.unitValue) * toNumber(item.organizationProduct?.conversionFactor)
        : item.unitValue
    ), []
  )

  const renderItem = useCallback(
    ({ item }) => (
      <ProductQuotationItem
        productName={ item.organizationProduct?.productName }
        measureUnit={ item.organizationProduct?.measureUnit }
        expirationDate={ item.expirationDate }
        unitValue={ calcUnitValue(item) }
        conversionFactor={ item.organizationProduct?.conversionFactor }
        unitOfMeasuresForConversion={ item.organizationProduct?.unitOfMeasuresForConversion }
      />
    ),
    [calcUnitValue]
  )

  const loadProductsQuotation = useCallback(
    (page, onSuccess, onError) => {
      if (!isEmpty(currentOrganization)) {
        dispatch(ProductsQuotationActions.loadProductsQuotation(
          { ...params, page },
          onSuccess,
          onError
        ))
      } else {
        onError(new Error ('current organization is required'))
      }
    },
    [currentOrganization, dispatch, params]
  )

  useEffect(() => () => {
    dispatch(ProductsQuotationActions.resetProductsQuotation())
  }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  , [currentOrganization, dispatch])

  return (
    <List
      data={ productsQuotation }
      renderItem={ renderItem }
      onListLoad={ loadProductsQuotation }
      obs={ `* ${ t('valid until') }` }
      deps={ deps }
    />
  )
}

ProductQuotationList.propTypes = {
  params: PropTypes.object
}

ProductQuotationList.defaultProps = {
  params: {}
}

export default ProductQuotationList
