import { Caption, Subheading } from 'react-native-paper'

import styled from 'styled-components/native'

import colors from '@smartcoop/styles/colors'

export const ItemInformation = styled.View`
  display: flex;
  flex-shrink: 1;
  flex-direction: column;
`

export const ItemContainer = styled.View`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-around;
`

export const LeftContainer= styled.View`
  display: flex;
  flex: 1;
  flex-direction: row;
  align-items: center;
`

export const RightContainer= styled.View`
  display: flex;
  flex-direction: column;
  margin-right: 5px;
`

export const ItemValue =  styled(Subheading)`
  color: ${ colors.black };
`

export const ItemDate = styled(Caption)`
  color: ${ colors.darkGrey };
`

export const ItemTitle = styled(Subheading)`
  color: ${ colors.black };
  font-weight: 700;
`

export const ItemSubtitle = styled(Subheading)`
  color: ${ colors.black };
`
