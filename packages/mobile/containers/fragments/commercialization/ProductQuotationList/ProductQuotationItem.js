import React, { useMemo } from 'react'

import moment from 'moment/moment'
import PropTypes from 'prop-types'

import ceil from 'lodash/ceil'

import { useT } from '@smartcoop/i18n'
import { momentBackDateTimeFormat } from '@smartcoop/utils/dates'
import { formatCurrency, formatNumber } from '@smartcoop/utils/formatters'

import {
  ItemContainer,
  LeftContainer,
  RightContainer,
  ItemValue,
  ItemInformation,
  ItemDate,
  ItemTitle,
  ItemSubtitle
} from './styles'

const ProductQuotationItem = (props) => {
  const {
    unitValue: externalUnitValue,
    expirationDate: externalExpirationDate,
    productName,
    conversionFactor,
    unitOfMeasuresForConversion,
    measureUnit: externalMeasureUnit
  } = props

  const t = useT()

  const measureUnit = useMemo(
    () => (
      conversionFactor > 1
        ? `${ unitOfMeasuresForConversion } ${ t('of') } ${ formatNumber(ceil(conversionFactor, 2)) } ${ externalMeasureUnit }`
        : `${ externalMeasureUnit }`
    ),
    [conversionFactor, externalMeasureUnit, t, unitOfMeasuresForConversion]
  )

  const unitValue = useMemo(
    () => (
      formatCurrency(parseFloat(externalUnitValue))
    ),
    [externalUnitValue]
  )

  const expirationDate = useMemo(
    () => (
      moment(externalExpirationDate, momentBackDateTimeFormat).format(`DD/MM/YYYY[ ${ t('until') }] HH[h]mm[*]`)
    ),
    [externalExpirationDate, t]
  )

  return (
    <ItemContainer>
      <LeftContainer>
        <ItemInformation>
          <ItemTitle>{productName}</ItemTitle>
          <ItemSubtitle>{measureUnit}</ItemSubtitle>
        </ItemInformation>
      </LeftContainer>
      <RightContainer>
        <ItemValue>
          {unitValue}
        </ItemValue>
        <ItemDate>
          {expirationDate}
        </ItemDate>
      </RightContainer>
    </ItemContainer>
  )
}

ProductQuotationItem.propTypes = {
  unitValue: PropTypes.number.isRequired,
  expirationDate: PropTypes.string.isRequired,
  productName: PropTypes.string.isRequired,
  measureUnit: PropTypes.string.isRequired,
  unitOfMeasuresForConversion: PropTypes.string,
  conversionFactor: PropTypes.number
}

ProductQuotationItem.defaultProps = {
  unitOfMeasuresForConversion: null,
  conversionFactor: null
}

export default ProductQuotationItem
