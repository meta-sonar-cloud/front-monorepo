import styled from 'styled-components/native'

import colors from '@smartcoop/styles/colors'

export const ItemContainer = styled.View`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`

export const ItemInformation = styled.View`
  display: flex;
  flex-shrink: 1;
  flex-direction: column;
`

export const ItemRow = styled.View`
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  flex: 1;
  padding: 5px 0;
`

export const LeftContainer= styled.View`
  display: flex;
  flex: 1;
  flex-direction: column;
`

export const RightContainer= styled.View`
  display: flex;
  flex-direction: column;
  flex: 1;
  align-items: flex-end;
  justify-content: space-between;
`

export const ItemValue = styled.Text`
  color: ${ colors.black };
  font-size: 16px;
`

export const ItemTitle = styled.Text`
  color: ${ colors.black };
  font-weight: 700;
  font-size: 16px;
`

export const FloatButtonContainer = styled.View`
  position: absolute;
  bottom: 0;
  z-index: 2;
  width: 50%;
  left: 25%;
  padding-bottom: 20px;
`
