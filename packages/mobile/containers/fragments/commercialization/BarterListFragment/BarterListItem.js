import React, { useMemo } from 'react'
import { TouchableWithoutFeedback, View } from 'react-native'

import moment from 'moment/moment'
import PropTypes from 'prop-types'

import size from 'lodash/size'

import { useT } from '@smartcoop/i18n'
import Badge from '@smartcoop/mobile-components/Badge'
import { momentBackDateTimeFormat } from '@smartcoop/utils/dates'

import {
  ItemContainer,
  LeftContainer,
  RightContainer,
  ItemValue,
  ItemInformation,
  ItemTitle,
  ItemRow
} from './styles'

const BarterListItem = (props) => {
  const {
    name,
    id,
    createdAt,
    cropName,
    status,
    statusColor,
    packageType,
    onClick
  } = props

  const t = useT()

  const data = useMemo(
    () => ({
      name: name ? `${ name } ${ packageType ? '(coop)' : '' }` : t('my package'),
      cropName,
      id: `${ t('code') } ${ id }`,
      createdAt: moment(createdAt, momentBackDateTimeFormat).format('DD/MM/YYYY'),
      status:
        <Badge
          textStyle={ { fontSize:14 } }
          style={ { paddingTop: 8, paddingBottom: 8, paddingLeft: 14, paddingRight: 14 } }
          color={ statusColor }
        >
          {status}
        </Badge>
    }),
    [name, packageType, t, cropName, id, createdAt, statusColor, status]
  )

  return (
    <TouchableWithoutFeedback onPress={ onClick }>
      <View>
        <ItemContainer>
          <LeftContainer>
            <ItemInformation>
              {size(data.name) > 0 && (
                <ItemRow>
                  <ItemTitle>{data.name}</ItemTitle>
                </ItemRow>
              )}
              <ItemRow>
                <ItemValue>{data.id}</ItemValue>
              </ItemRow>
              {size(data.cropName) > 0 && (
                <ItemRow>
                  <ItemValue>{data.cropName}</ItemValue>
                </ItemRow>
              )}
            </ItemInformation>
          </LeftContainer>

          <RightContainer>
            <ItemRow>
              <ItemValue>{data.createdAt}</ItemValue>
            </ItemRow>
            <ItemRow>
              <ItemValue>{data.status}</ItemValue>
            </ItemRow>
          </RightContainer>
        </ItemContainer>
      </View>
    </TouchableWithoutFeedback>
  )
}

BarterListItem.propTypes = {
  name: PropTypes.string,
  cropName: PropTypes.string,
  packageType: PropTypes.bool,
  id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  createdAt: PropTypes.string.isRequired,
  status: PropTypes.string.isRequired,
  statusColor: PropTypes.string.isRequired,
  onClick: PropTypes.func
}

BarterListItem.defaultProps = {
  name: '',
  cropName: '',
  packageType: false,
  onClick: () => {}
}

export default BarterListItem
