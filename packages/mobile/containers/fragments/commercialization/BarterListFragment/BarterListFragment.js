import React, { useCallback, useMemo, useState, useRef } from 'react'
import { View } from 'react-native'
import { useDispatch, useSelector } from 'react-redux'

import { useNavigation, useFocusEffect } from '@react-navigation/native'
import PropTypes from 'prop-types'

import isEmpty from 'lodash/isEmpty'

import { useT } from '@smartcoop/i18n'
import Button from '@smartcoop/mobile-components/Button'
import List from '@smartcoop/mobile-components/List'
import { BarterActions } from '@smartcoop/stores/barter'
import { selectBarters } from '@smartcoop/stores/barter/selectorBarter'
import { selectCurrentOrganization } from '@smartcoop/stores/organization/selectorOrganization'
import colors from '@smartcoop/styles/colors'

import BarterListItem from './BarterListItem'
import {
  FloatButtonContainer
} from './styles'


const BarterListFragment = ({ params }) => {
  const dispatch = useCallback(useDispatch(), [])

  const t = useT()
  const navigation = useNavigation()

  const barters = useSelector(selectBarters)
  const currentOrganization = useSelector(selectCurrentOrganization)

  const [refresh, setRefresh] = useState(true)

  const mounted = useRef(false)

  const deps = useMemo(
    () => [currentOrganization, params, refresh],
    [currentOrganization, params, refresh]
  )

  const handleBarterRegisterClick = useCallback(
    () => {
      navigation.navigate('Barter', { screen: 'CreateBarterScreen' })
    },
    [navigation]
  )

  const handleBarterDetailsClick = useCallback(
    (item) => {
      navigation.navigate('Barter', { screen: 'BarterDetailsScreen', params: { barterOrderNumber: item.barterOrderNumber } })
    },
    [navigation]
  )

  const renderItem = useCallback(
    ({ item }) => (
      <BarterListItem
        id={ item.barterOrderNumber }
        name={ item?.barterName }
        cropName={ item?.cropName }
        packageType={ item?.packageType }
        createdAt={ item.releaseDate }
        status={ item.status.statusName }
        statusColor={ item.status.statusColor }
        onClick={ () => handleBarterDetailsClick(item) }
      />
    ),
    [handleBarterDetailsClick]
  )

  const loadBarters = useCallback(
    (page, onSuccess, onError) => {
      if (!isEmpty(currentOrganization)) {
        dispatch(BarterActions.loadBarters(
          { ...params, page },
          onSuccess,
          onError
        ))
      } else {
        onError(new Error (t('current organization is required')))
      }
    },
    [currentOrganization, dispatch, params, t]
  )

  useFocusEffect(
    useCallback(() => {
      if(!mounted.current) {
        mounted.current = true
      } else {
        setRefresh(old => !old)
      }
    }, [])
  )

  return (
    <>
      <FloatButtonContainer>
        <View>
          <Button
            onPress={ handleBarterRegisterClick }
            color={ colors.text }
            backgroundColor={ colors.secondary }
            title={ t('new {this}', { gender: 'male', howMany: 1, this: t('barter') }) }
          />
        </View>
      </FloatButtonContainer>
      <List
        itemKey="barterOrderNumber"
        data={ barters }
        renderItem={ renderItem }
        onListLoad={ loadBarters }
        deps={ deps }
      />
    </>
  )
}

BarterListFragment.propTypes = {
  params: PropTypes.object
}

BarterListFragment.defaultProps = {
  params: {}
}

export default BarterListFragment
