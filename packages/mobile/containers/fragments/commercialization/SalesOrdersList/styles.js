import { Subheading, Caption } from 'react-native-paper'

import styled from 'styled-components/native'

import colors from '@smartcoop/styles/colors'


export const ItemInformation = styled.View`
  display: flex;
  flex-shrink: 1;
  flex-direction: column;
`

export const ItemContainer = styled.View`
  display: flex;
  justify-content: space-between;
  flex-direction: row;
`

export const ItemRow = styled.View`
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  margin: 3px 0;
  flex: 1;
`

export const LeftContainer= styled.View`
  display: flex;
  flex: 1;
  flex-direction: column;
  padding-right: 40px;
`

export const RightContainer= styled.View`
  display: flex;
  flex-direction: column;
  align-items: flex-end;
  justify-content: space-between;
`

export const ItemQuotation = styled(Caption)`
  color: ${ colors.black };
`

export const ItemValue = styled(Subheading)`
  color: ${ colors.black };
`

export const ItemDate = styled(Caption)`
  color: ${ colors.darkGrey };
`

export const ItemTitle = styled(Subheading)`
  color: ${ colors.black };
  font-weight: 700;
`

export const ButtonsContainer = styled.View`
  flex-direction: row;
  justify-content: space-around;
`

export const FloatButtonContainer = styled.View`
  position: absolute;
  bottom: 0;
  z-index: 2;
  width: 50%;
  left: 25%;
  padding-bottom: 20px;
`
