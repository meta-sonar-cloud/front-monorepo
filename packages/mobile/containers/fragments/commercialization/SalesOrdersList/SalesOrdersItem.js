import React, { useMemo, useCallback } from 'react'
import { View } from 'react-native'
import { useDispatch } from 'react-redux'

import moment from 'moment/moment'
import PropTypes from 'prop-types'

import { useDialog } from '@smartcoop/dialog'
import { useT } from '@smartcoop/i18n'
import { trash } from '@smartcoop/icons'
import Badge from '@smartcoop/mobile-components/Badge'
import Button from '@smartcoop/mobile-components/Button'
import Icon from '@smartcoop/mobile-components/Icon'
import ConfirmModal from '@smartcoop/mobile-components/Modal/ConfirmModal'
import SignatureOrderModal from '@smartcoop/mobile-containers/modals/Commercialization/SignatureOrderModal'
import { useSnackbar } from '@smartcoop/snackbar'
import { SalesOrdersActions } from '@smartcoop/stores/salesOrders'
import { colors } from '@smartcoop/styles'
import { momentBackDateTimeFormat } from '@smartcoop/utils/dates'
import { formatCurrency } from '@smartcoop/utils/formatters'

import {
  ItemContainer,
  LeftContainer,
  RightContainer,
  ItemValue,
  ItemInformation,
  ItemTitle,
  ItemDate,
  ItemRow,
  ButtonsContainer
} from './styles'

const SalesOrdersItem = (props) => {
  const {
    productName,
    id,
    releaseDate,
    quotation,
    type,
    totalValue,
    status,
    statusColor
  } = props

  const t = useT()
  const { createDialog } = useDialog()
  const dispatch = useCallback(useDispatch(), [])
  const snackbar = useSnackbar()

  const data = useMemo(
    () => ({
      productName,
      id: `ID ${ id }`,
      releaseDate: moment(releaseDate, momentBackDateTimeFormat).format('DD/MM/YYYY'),
      quotation: `${ t('quotation', { howMany: 1 }) } ${ quotation }`,
      type,
      totalValue: formatCurrency(totalValue),
      status:
        <Badge
          textStyle={ { fontSize:14 } }
          style={ { paddingTop: 8, paddingBottom: 8, paddingLeft: 14, paddingRight: 14 } }
          color={ statusColor }
        >
          {status}
        </Badge>
    }),
    [id, productName, quotation, releaseDate, status, statusColor, t, totalValue, type]
  )

  const handleDelete = useCallback(
    () => {
      createDialog({
        id: 'confirm-delete',
        Component: ConfirmModal,
        props: {
          onConfirm: () => {
            createDialog({
              id: 'signature-order-modal',
              Component: SignatureOrderModal,
              props: {
                onSuccess: () => {
                  dispatch(SalesOrdersActions.deleteSalesOrders(id))
                  snackbar.success(
                    t('your {this} was deleted', {
                      howMany: 1,
                      gender: 'female',
                      this: t('order', { howMany: 1 })
                    })
                  )
                }
              }
            })
          },
          message: t('are you sure you want to delete the {this}?', {
            howMany: 1,
            gender: 'female',
            this: t('order', { howMany: 1 })
          })
        }
      })
    },
    [createDialog, dispatch, id, snackbar, t]
  )

  return (
    <ItemContainer>

      <LeftContainer>
        <ItemInformation>
          <ItemRow>
            <ItemTitle>{data.productName}</ItemTitle>
          </ItemRow>
          <ItemRow>
            <ItemValue>{data.quotation}</ItemValue>
          </ItemRow>
          <ItemRow>
            <ItemValue>{data.totalValue}</ItemValue>
          </ItemRow>
        </ItemInformation>
      </LeftContainer>

      <RightContainer>
        <ItemRow>
          <ItemDate>{data.releaseDate}</ItemDate>
        </ItemRow>
        <ItemRow>
          <ItemValue>{data.type}</ItemValue>
        </ItemRow>
        <ItemRow>
          <ItemValue>{data.status}</ItemValue>
        </ItemRow>
      </RightContainer>

      <RightContainer style={ { marginLeft: 10 } }>
        <ItemRow>
          <ButtonsContainer>
            <Button
              variant='outlined'
              icon={ (
                <View style={ { paddingTop: 10, paddingBottom: 10 } }>
                  <Icon
                    size={ 16 }
                    icon={ trash }
                    color={ colors.red }
                  />
                </View>
              ) }
              onPress={ handleDelete }
            />
          </ButtonsContainer>
        </ItemRow>
      </RightContainer>

    </ItemContainer>
  )
}

SalesOrdersItem.propTypes = {
  productName: PropTypes.string.isRequired,
  id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  releaseDate: PropTypes.string.isRequired,
  quotation: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  type: PropTypes.string.isRequired,
  totalValue: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  status: PropTypes.string.isRequired,
  statusColor: PropTypes.string.isRequired
}

export default SalesOrdersItem
