import React, { useCallback, useEffect, useMemo, useState, useRef } from 'react'
import { View } from 'react-native'
import { useDispatch, useSelector } from 'react-redux'

import { useNavigation, useFocusEffect } from '@react-navigation/native'
import PropTypes from 'prop-types'

import ceil from 'lodash/ceil'
import isEmpty from 'lodash/isEmpty'

import { useT } from '@smartcoop/i18n'
import Button from '@smartcoop/mobile-components/Button'
import List from '@smartcoop/mobile-components/List'
import { selectCurrentOrganization } from '@smartcoop/stores/organization/selectorOrganization'
import { SalesOrdersActions } from '@smartcoop/stores/salesOrders'
import { selectSalesOrders } from '@smartcoop/stores/salesOrders/selectorSalesOrders'
import colors from '@smartcoop/styles/colors'
import { formatCurrency, formatNumber } from '@smartcoop/utils/formatters'

import SalesOrdersItem from './SalesOrdersItem'
import {
  FloatButtonContainer
} from './styles'


const SalesOrdersList = ({ params }) => {
  const dispatch = useCallback(useDispatch(), [])

  const t = useT()
  const navigation = useNavigation()

  const salesOrders = useSelector(selectSalesOrders)
  const currentOrganization = useSelector(selectCurrentOrganization)

  const [refresh, setRefresh] = useState(true)

  const mounted = useRef(false)

  const deps = useMemo(
    () => [currentOrganization, params, refresh],
    [currentOrganization, params, refresh]
  )

  const getQuotationUnit = useCallback((product) => {
    // eslint-disable-next-line eqeqeq
    const isConversionFactorEmpty = !product.conversionFactor || product.conversionFactor == 0
    return isConversionFactorEmpty
      ? product.measureUnit
      : `${
        t('{this} of', { this: product.unitOfMeasuresForConversion } )
      } ${
        formatNumber(ceil(Number(product.conversionFactor), 2))
      } ${
        product.measureUnit
      }`
  }, [t])

  const renderItem = useCallback(
    ({ item }) => (
      <SalesOrdersItem
        productName={ item.organizationProduct.productName }
        id={ item.id }
        releaseDate={ item.salesType === 'Gatilho' ? item.scheduledSale.expirationDate : item.releaseDate }
        quotation={ item.salesType === 'Gatilho' ? `${ formatCurrency(item.scheduledSale.targetPrice * item.organizationProduct.conversionFactor)  }/${  getQuotationUnit(item.organizationProduct) }` :
          `${ formatCurrency(item.quotation * item.organizationProduct.conversionFactor) }/${ getQuotationUnit(item.organizationProduct) }` }
        type={ item.salesType }
        totalValue={ item.totalValue }
        status={ item.salesOrdersStatus.statusName }
        statusColor={ item.salesOrdersStatus.statusColor }
      />
    ),
    [getQuotationUnit]
  )

  const loadSalesOrders = useCallback(
    (page, onSuccess, onError) => {
      if (!isEmpty(currentOrganization)) {
        dispatch(SalesOrdersActions.loadSalesOrders(
          { ...params, page },
          onSuccess,
          onError
        ))
      } else {
        onError(new Error ('current organization is required'))
      }
    },
    [currentOrganization, dispatch, params]
  )

  const handleRegisterFieldClick = useCallback(
    () => {
      navigation.navigate('Quotation', { screen: 'CreateSalesOrders' })
    },
    [navigation]
  )

  useFocusEffect(
    useCallback(() => {
      if(!mounted.current) {
        mounted.current = true
      } else {
        setRefresh(old => !old)
      }
    }, [])
  )

  useEffect(() => () => {
    dispatch(SalesOrdersActions.resetSalesOrders())
  }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  , [currentOrganization, dispatch])

  return (
    <>
      <FloatButtonContainer>
        <View>
          <Button
            onPress={ handleRegisterFieldClick }
            color={ colors.text }
            backgroundColor={ colors.secondary }
            title={ t('new order') }
          />
        </View>
      </FloatButtonContainer>
      <List
        data={ salesOrders }
        renderItem={ renderItem }
        onListLoad={ loadSalesOrders }
        deps={ deps }
      />
    </>
  )
}

SalesOrdersList.propTypes = {
  params: PropTypes.object
}

SalesOrdersList.defaultProps = {
  params: {}
}

export default SalesOrdersList
