import React, { useCallback, useEffect, useMemo } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import PropTypes from 'prop-types'

import isEmpty from 'lodash/isEmpty'

import List from '@smartcoop/mobile-components/List'
import { selectCurrentOrganization } from '@smartcoop/stores/organization/selectorOrganization'
import { ProductsBalanceActions } from '@smartcoop/stores/productsBalance'
import { selectProductsBalance } from '@smartcoop/stores/productsBalance/selectorProductsBalance'

import ProductBalanceItem from './ProductBalanceItem'

const ProductBalanceList = ({ params }) => {
  const dispatch = useCallback(useDispatch(), [])

  const productsBalance = useSelector(selectProductsBalance)
  const currentOrganization = useSelector(selectCurrentOrganization)

  const deps = useMemo(
    () => [currentOrganization, params],
    [currentOrganization, params]
  )

  const renderItem = useCallback(
    ({ item }) => (
      <ProductBalanceItem
        currentBalance={ item.currentBalance }
        productName={ item.organizationProduct?.productName }
        measureUnit={ item.organizationProduct?.measureUnit }
        conversionFactor={ item.organizationProduct?.conversionFactor }
        unitOfMeasuresForConversion={ item.organizationProduct?.unitOfMeasuresForConversion }
      />
    ),
    []
  )

  const loadProductsBalance = useCallback(
    (page, onSuccess, onError) => {
      if (!isEmpty(currentOrganization)) {
        dispatch(ProductsBalanceActions.loadProductsBalance(
          { ...params, page },
          onSuccess,
          onError
        ))
      } else {
        onError(new Error ('current organization is required'))
      }
    },
    [currentOrganization, dispatch, params]
  )

  useEffect(() => () => {
    dispatch(ProductsBalanceActions.resetProductsBalance())
  }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  , [currentOrganization, dispatch])

  return (
    <List
      data={ productsBalance }
      renderItem={ renderItem }
      onListLoad={ loadProductsBalance }
      deps={ deps }
    />
  )
}

ProductBalanceList.propTypes = {
  params: PropTypes.object
}

ProductBalanceList.defaultProps = {
  params: {}
}

export default ProductBalanceList
