import React, { useMemo } from 'react'

import PropTypes from 'prop-types'

import ceil from 'lodash/ceil'

import { useT } from '@smartcoop/i18n'
import { formatNumber } from '@smartcoop/utils/formatters'

import {
  ItemContainer,
  ItemValue,
  ItemHeader,
  ItemFooter,
  ItemTitle
} from './styles'

const ProductBalanceItem = (props) => {
  const {
    currentBalance: externalCurrentBalance,
    productName,
    measureUnit,
    conversionFactor,
    unitOfMeasuresForConversion
  } = props

  const t = useT()

  const currentBalance = useMemo(
    () => `${ formatNumber(ceil(externalCurrentBalance, 2)) } ${ measureUnit }`,
    [externalCurrentBalance, measureUnit]
  )

  const amountInBags = useMemo(
    () => // eslint-disable-next-line eqeqeq
      !conversionFactor || conversionFactor == 0
        ? t('does not apply - short version')
        : `${ formatNumber(Math.floor(Number(externalCurrentBalance)/Number(conversionFactor))) } ${ t('{this} of', { this: unitOfMeasuresForConversion }) } ${ formatNumber(ceil(conversionFactor, 2)) } ${ measureUnit }`,
    [conversionFactor, externalCurrentBalance, measureUnit, t, unitOfMeasuresForConversion]
  )

  return (
    <ItemContainer>
      <ItemHeader>
        <ItemTitle>{productName}</ItemTitle>
      </ItemHeader>

      <ItemFooter>
        <ItemValue>{currentBalance}</ItemValue>
        <ItemValue>{amountInBags}</ItemValue>
      </ItemFooter>
    </ItemContainer>
  )
}

ProductBalanceItem.propTypes = {
  currentBalance: PropTypes.number.isRequired,
  productName: PropTypes.string.isRequired,
  measureUnit: PropTypes.string.isRequired,
  conversionFactor: PropTypes.number,
  unitOfMeasuresForConversion: PropTypes.string
}

ProductBalanceItem.defaultProps = {
  conversionFactor: 0,
  unitOfMeasuresForConversion: null
}

export default ProductBalanceItem
