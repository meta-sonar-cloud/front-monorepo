import React, { useCallback, useEffect, useMemo } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import PropTypes from 'prop-types'

import isEmpty from 'lodash/isEmpty'

import List from '@smartcoop/mobile-components/List'
import { selectCurrentOrganization } from '@smartcoop/stores/organization/selectorOrganization'
import { ProductsWithdrawActions } from '@smartcoop/stores/productsWithdraw'
import { selectProductsWithdraw } from '@smartcoop/stores/productsWithdraw/selectorProductsWithdraw'

import ProductWithdrawItem from './ProductWithdrawItem'

const ProductWithdrawList = ({ params, technical }) => {
  const dispatch = useCallback(useDispatch(), [])

  const productsWithdraw = useSelector(selectProductsWithdraw)
  const currentOrganization = useSelector(selectCurrentOrganization)

  const deps = useMemo(
    () => [currentOrganization, params],
    [currentOrganization, params]
  )

  const renderItem = useCallback(
    ({ item }) => (
      <ProductWithdrawItem
        currentBalance={ item.currentBalance }
        productName={ item.organizationProducts?.productName }
        measureUnit={ item.organizationProducts?.measureUnit }
        conversionFactor={ item.organizationProducts?.conversionFactor }
        unitOfMeasuresForConversion={ item.organizationProducts?.unitOfMeasuresForConversion }
      />
    ),
    []
  )

  const loadProductsWithdraw = useCallback(
    (page, onSuccess, onError) => {
      if (!isEmpty(currentOrganization)) {
        dispatch(ProductsWithdrawActions.loadProductsWithdraw(
          { ...params, technical, page },
          onSuccess,
          onError
        ))
      } else {
        onError(new Error ('current organization is required'))
      }
    },
    [currentOrganization, dispatch, params, technical]
  )

  useEffect(() => () => {
    dispatch(ProductsWithdrawActions.resetProductsWithdraw())
  }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  , [currentOrganization, dispatch])

  return (
    <List
      data={ productsWithdraw }
      renderItem={ renderItem }
      onListLoad={ loadProductsWithdraw }
      deps={ deps }
    />
  )
}

ProductWithdrawList.propTypes = {
  params: PropTypes.object,
  technical: PropTypes.bool
}

ProductWithdrawList.defaultProps = {
  params: {},
  technical: false
}

export default ProductWithdrawList
