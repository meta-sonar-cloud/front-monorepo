import { Subheading } from 'react-native-paper'

import styled from 'styled-components/native'

import colors from '@smartcoop/styles/colors'

export const ItemHeader = styled.View`
`

export const ItemFooter = styled.View`
  flex-direction: row;
  justify-content: space-between;
  width: 100%;
`

export const ItemContainer = styled.View`
  display: flex;
  justify-content: space-between;
`

export const ItemValue = styled(Subheading)`
  color: ${ colors.black };
`

export const ItemTitle = styled(Subheading)`
  color: ${ colors.black };
  font-weight: 700;
`
