import React, { useCallback, useMemo } from 'react'

import PropTypes from 'prop-types'

import { ceil, filter } from 'lodash'
import map from 'lodash/map'

import I18n, { useT } from '@smartcoop/i18n'
import { plus, trash } from '@smartcoop/icons'
import Button from '@smartcoop/mobile-components/Button'
import Icon from '@smartcoop/mobile-components/Icon'
import InputNumber from '@smartcoop/mobile-components/InputNumber'
import InputSelect from '@smartcoop/mobile-components/InputSelect'
import InputText from '@smartcoop/mobile-components/InputText'
import useSmartcoopApi from '@smartcoop/services/hooks/useSmartcoopApi'
import { colors } from '@smartcoop/styles'
import { formatNumber } from '@smartcoop/utils/formatters'

import {
  InputsContainer,
  Item,
  IconText,
  IconContainer,
  Container,
  Body,
  DeleteContainer,
  Title
} from './styles'

const CreateBarterFragment = ({
  orgId,
  title,
  type,
  onResultChange,
  data
}) => {
  const t = useT()

  const { data: requestProducts } = useSmartcoopApi(`/barters/products/organization/${ orgId }?page=1&limit=${ process.env.REACT_APP_FAKE_PAGINATION_SIZE }&q=${ type }`)

  const products = useMemo(
    () => map(requestProducts?.data, (item) => ({ ...item, value: item.productInternalCode, label: item.productName })),
    [requestProducts]
  )

  const handleItemChange = useCallback(
    (externalIndex, externalItem) => {
      onResultChange(
        (old) => ({
          ...old,
          [type]: map(old[type], (item, index) =>
            index === externalIndex
              ? {
                ...item,
                ...externalItem
              }
              : item
          )
        })
      )
    },
    [onResultChange, type]
  )

  const handleAddMoreItems = useCallback(
    () => {
      onResultChange(
        (old) => ({
          ...old,
          [type]: [
            ...old[type],
            []
          ]
        })
      )
    },
    [onResultChange, type]
  )

  const removeItem = useCallback(
    (externalIndex) => {
      onResultChange(
        (old) => ({
          ...old,
          [type]: filter(old[type], (_, index) => index !== externalIndex)
        })
      )
    },
    [onResultChange, type]
  )

  const handleOnChange = useCallback( (id, value, index) => {
    if(value === undefined){
      handleItemChange(index, { productInternalCode: '', unit: '', quantity: '' })
    }else{
      const { unitOfMeasuresForConversion, conversionFactor, measureUnit } = value
      handleItemChange(index, { productInternalCode: id })
      handleItemChange(index, {
        unit: conversionFactor === 1
          ?
          measureUnit
          :
          `${ unitOfMeasuresForConversion === undefined ? '' : unitOfMeasuresForConversion } ${ t('of') } ${ formatNumber(ceil(conversionFactor, 2)) } ${ measureUnit }` }
      )
    }
  },[handleItemChange, t])

  return (
    <Container>
      <Item>
        <Title>{title}</Title>
      </Item>
      {map(data, ({ productInternalCode, unit, quantity }, index) => (
        <Body key={ index }>
          <InputsContainer>
            <InputSelect
              style={ { backgroundColor: 'white' } }
              options={ products }
              value={ (productInternalCode || '').toString() }
              onChange={
                (id, value) => handleOnChange(id, value, index)
              }
              label={ t('product') }
              detached
            />

            <InputText
              value={ (unit || '').toString() }
              onChange={ (value) => handleItemChange(index, { unit: value }) }
              label={ t('unit') }
              style={ { backgroundColor: 'white' } }
              fullWidth
              disabled
              detached
            />

            <InputNumber
              value={ (quantity || '').toString() }
              onChange={ (value) => handleItemChange(index, { quantity: value }) }
              label={ t('quantity') }
              style={ { backgroundColor: 'white' } }
              detached
            />
          </InputsContainer>

          <DeleteContainer>
            <Button
              onPress={ () => removeItem(index) }
              icon={ <Icon size={ 16 } icon={ trash } color={ colors.red } /> }
              backgroundColor={ colors.backgroundHtml }
            />
          </DeleteContainer>
        </Body>

      ))}

      <IconContainer style={ { cursor: 'pointer' } } onPress={ handleAddMoreItems }>
        <Icon icon={ plus } size={ 13 }/>
        <I18n as={ IconText }>add product</I18n>
      </IconContainer>
    </Container>
  )
}

CreateBarterFragment.propTypes = {
  orgId: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  onResultChange: PropTypes.func.isRequired,
  data: PropTypes.array.isRequired
}

export default CreateBarterFragment
