import { Subheading, Paragraph } from 'react-native-paper'

import styled from 'styled-components/native'

import colors from '@smartcoop/styles/colors'

export const Container = styled.View`
  padding: 20px;
`

export const IconContainer = styled.TouchableOpacity`
  flex-direction: row;
  align-items: center;
`

export const IconText = styled(Paragraph)`
  font-weight: 600;
  padding-left: 6px;
`

export const Title = styled(Subheading)`
  font-weight: 600;
  font-family: 'Montserrat';
`

export const Body = styled.View`
  flex-direction: row;
  align-items: center;
  background-color: ${ colors.backgroundHtml };
  padding: 13px 5px 0px;
  border-radius: 4px;
  margin-bottom: 5px;
`

export const InputsContainer = styled.View`
  flex: 1;
  padding-left: 5px;
`

export const Item = styled.View`
  flex-direction: row;
`

export const DeleteContainer = styled.View`
  justify-content: center;
  padding: 5px;
`