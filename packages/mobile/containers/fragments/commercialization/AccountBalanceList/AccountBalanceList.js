import React, { useCallback, useEffect, useMemo } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import { useNavigation } from '@react-navigation/native'

import isEmpty from 'lodash/isEmpty'

import AccountBalance from '@smartcoop/mobile-components/AccountBalance'
import List from '@smartcoop/mobile-components/List'
import { AccountActions } from '@smartcoop/stores/account'
import { selectAccounts } from '@smartcoop/stores/account/selectorAccount'
import { selectHasOrganizationToken } from '@smartcoop/stores/authentication/selectorAuthentication'
import { selectCurrentOrganization } from '@smartcoop/stores/organization/selectorOrganization'

const AccountBalanceList = () => {
  const dispatch = useCallback(useDispatch(), [])
  const navigation = useNavigation()

  const accounts = useSelector(selectAccounts)
  const currentOrganization = useSelector(selectCurrentOrganization)
  const hasOrganizationToken = useSelector(selectHasOrganizationToken)

  const deps = useMemo(() => [currentOrganization], [currentOrganization])

  const onPress = useCallback(
    (account) => {
      dispatch(AccountActions.setCurrentAccount(account))
      navigation.navigate('Quotation', { screen: 'SecuritiesMovement' })
    },
    [dispatch, navigation]
  )

  const renderItem = useCallback(
    ({ item }) => (
      <AccountBalance
        accountName={ item.accountName }
        balance={ item.currentBalance }
        onPress={ () => onPress(item) }
      />
    ),
    [onPress]
  )

  const loadAccounts = useCallback(
    (_, onSuccess, onError) => {
      if (!isEmpty(currentOrganization) && hasOrganizationToken) {
        dispatch(AccountActions.loadAccounts(onSuccess, onError))
      } else {
        onError(new Error('current organization is required'))
      }
    },
    [currentOrganization, dispatch, hasOrganizationToken]
  )

  useEffect(
    () => () => {
      dispatch(AccountActions.resetAccounts())
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [currentOrganization, dispatch]
  )

  return (
    <List
      data={ accounts }
      renderItem={ renderItem }
      onListLoad={ loadAccounts }
      deps={ deps }
    />
  )
}

export default AccountBalanceList
