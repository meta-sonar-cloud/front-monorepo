import React, { useMemo } from 'react'
import { TouchableWithoutFeedback, View } from 'react-native'

import moment from 'moment/moment'
import PropTypes from 'prop-types'

import { useT } from '@smartcoop/i18n'
import { momentBackDateTimeFormat } from '@smartcoop/utils/dates'

import {
  ItemContainer,
  LeftContainer,
  RightContainer,
  ItemValue,
  ItemInformation,
  ItemTitle,
  ItemRow
} from './styles'

const BarterPackageListItem = (props) => {
  const {
    barterName,
    id,
    createdAt,
    cropName,
    price,
    measureUnit,
    onClick
  } = props

  const t = useT()

  const data = useMemo(
    () => ({
      price,
      barterName,
      cropName,
      id: `${ t('code') } ${ id }`,
      createdAt: moment(createdAt, momentBackDateTimeFormat).format('DD/MM/YYYY'),
      measureUnit
    }),
    [price, barterName, cropName, t, id, createdAt, measureUnit]
  )

  return (
    <TouchableWithoutFeedback onPress={ onClick }>
      <View>
        <ItemContainer>
          <LeftContainer>
            <ItemInformation>
              <ItemRow>
                <ItemTitle>{data.barterName}</ItemTitle>
              </ItemRow>
              <ItemRow>
                <ItemValue>{data.id}</ItemValue>
              </ItemRow>
              <ItemRow>
                <ItemValue>{data.cropName}</ItemValue>
              </ItemRow>
            </ItemInformation>
          </LeftContainer>

          <RightContainer>
            <ItemRow>
              <ItemValue>{data.createdAt}</ItemValue>
            </ItemRow>
            <ItemRow>
              <ItemValue>{data.price} {data.measureUnit}</ItemValue>
            </ItemRow>
          </RightContainer>
        </ItemContainer>
      </View>
    </TouchableWithoutFeedback>
  )
}

BarterPackageListItem.propTypes = {
  barterName: PropTypes.string.isRequired,
  id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  price: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  createdAt: PropTypes.string.isRequired,
  cropName: PropTypes.string.isRequired,
  measureUnit: PropTypes.string,
  onClick: PropTypes.func
}

BarterPackageListItem.defaultProps = {
  onClick: () => {},
  price: '',
  measureUnit: ''
}

export default BarterPackageListItem
