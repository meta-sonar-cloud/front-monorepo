import styled from 'styled-components/native'

import colors from '@smartcoop/styles/colors'

export const ItemInformation = styled.View`
  display: flex;
  flex-shrink: 1;
  flex-direction: column;
`

export const ItemContainer = styled.View`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`

export const ItemRow = styled.View`
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  flex: 1;
  padding: 5px 0;
`

export const LeftContainer= styled.View`
  display: flex;
  flex: 1;
  flex-direction: column;
`

export const RightContainer= styled.View`
  display: flex;
  flex-direction: column;
  flex: 1;
  align-items: flex-end;
  justify-content: space-between;
`

export const ItemValue = styled.Text`
  color: ${ colors.black };
  font-size: 16px;
`

export const ItemDate = styled.Text`
  color: ${ colors.darkGrey };
  font-size: 12px;
`

export const ItemTitle = styled.Text`
  color: ${ colors.black };
  font-weight: 700;
  font-size: 16px;
`

export const Container = styled.View`
  flex: 1;
  flex-direction: row;
  padding: 20px 10px;
`

export const LeftItem = styled.View`
  flex: 1;
  flex-direction: row;
  justify-content: flex-start;
`

export const RightItem = styled.View`
  flex: 1;
  flex-direction: row;
  justify-content: flex-end;
`