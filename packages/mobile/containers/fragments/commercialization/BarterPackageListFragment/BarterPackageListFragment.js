import React, { useCallback, useMemo, useState, useRef } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import { useNavigation, useFocusEffect } from '@react-navigation/native'
import PropTypes from 'prop-types'

import isEmpty from 'lodash/isEmpty'

import { useT } from '@smartcoop/i18n'
import List from '@smartcoop/mobile-components/List'
import { BarterActions } from '@smartcoop/stores/barter'
import { selectBarterPackages } from '@smartcoop/stores/barter/selectorBarter'
import { selectCurrentOrganization } from '@smartcoop/stores/organization/selectorOrganization'

import BarterPackageListItem from './BarterPackageListItem'

const BarterPackageListFragment = ({ params }) => {
  const dispatch = useCallback(useDispatch(), [])
  const navigation = useNavigation()

  const t = useT()

  const barterPackages = useSelector(selectBarterPackages)
  const currentOrganization = useSelector(selectCurrentOrganization)

  const [refresh, setRefresh] = useState(true)

  const mounted = useRef(false)

  const deps = useMemo(
    () => [currentOrganization, params, refresh],
    [currentOrganization, params, refresh]
  )

  const handleBarterDetailsClick = useCallback(
    (item) => {
      navigation.navigate('Barter', { screen: 'BarterPackageDetailsScreen', params: {
        barterId: item.id,
        exibitionCode: item.internalCode
      } })
    },
    [navigation]
  )

  const renderItem = useCallback(
    ({ item }) => (
      <BarterPackageListItem
        id={ item.internalCode }
        barterName={ item.barterName }
        cropName={ item.cropName }
        createdAt={ item.createdAt }
        price={ item.price }
        measureUnit={ item.measureUnit }
        onClick={ () => handleBarterDetailsClick(item) }
      />
    ),
    [handleBarterDetailsClick]
  )

  const loadBarters = useCallback(
    (page, onSuccess, onError) => {
      if (!isEmpty(currentOrganization)) {
        dispatch(BarterActions.loadPackages(
          { ...params, page },
          onSuccess,
          onError
        ))
      } else {
        onError(new Error (t('current organization is required')))
      }
    },
    [currentOrganization, dispatch, params, t]
  )

  useFocusEffect(
    useCallback(() => {
      if(!mounted.current) {
        mounted.current = true
      } else {
        setRefresh(old => !old)
      }
    }, [])
  )

  return (
    <List
      data={ barterPackages }
      renderItem={ renderItem }
      onListLoad={ loadBarters }
      deps={ deps }
    />
  )
}

BarterPackageListFragment.propTypes = {
  params: PropTypes.object
}

BarterPackageListFragment.defaultProps = {
  params: {}
}

export default BarterPackageListFragment
