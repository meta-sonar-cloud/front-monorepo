import React, { useCallback, useMemo, useState, useRef } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import { useFocusEffect } from '@react-navigation/native'
import PropTypes from 'prop-types'

import { useDialog } from '@smartcoop/dialog'
import List from '@smartcoop/mobile-components/List'
import ConfigurePermissionModal from '@smartcoop/mobile-containers/modals/ConfigurePermissionModal'
import { TechnicalActions } from '@smartcoop/stores/technical'
import { selectRequestAccessList } from '@smartcoop/stores/technical/selectorTechnical'

import UserAccessListItem from './UserAccessListItem'

const UserAccessList = ({ params }) => {
  const dispatch = useCallback(useDispatch(), [])
  const { createDialog } = useDialog()

  const requestAccessList = useSelector(selectRequestAccessList)

  const [refresh, setRefresh] = useState(true)

  const mounted = useRef(false)

  const deps = useMemo(
    () => [params, refresh],
    [params, refresh]
  )

  const configurePermissionModal = useCallback(
    (item) => {
      createDialog({
        id: 'configure-permission',
        Component: ConfigurePermissionModal,
        props: {
          item,
          onSuccess: () => setRefresh(old => !old)
        }
      })
    },
    [createDialog]
  )

  const renderItem = useCallback(
    ({ item, index }) => (
      <UserAccessListItem
        accessRequest={ item }
        index={ index }
        configurePermissionModal={ () => configurePermissionModal(item) }
      />
    ),
    [configurePermissionModal]
  )

  const loadRequestAccessList = useCallback(
    (page, onSuccess, onError) => {
      dispatch(TechnicalActions.loadRequestAccessList(
        { ...params, page },
        onSuccess,
        onError
      ))
    },
    [dispatch, params]
  )

  useFocusEffect(
    useCallback(() => {
      if(!mounted.current) {
        mounted.current = true
      } else {
        setRefresh(old => !old)
      }
    }, [])
  )

  return (
    <List
      data={ requestAccessList }
      renderItem={ renderItem }
      onListLoad={ loadRequestAccessList }
      deps={ deps }
      hideUpdatedAt
    />
  )
}

UserAccessList.propTypes = {
  params: PropTypes.object
}

UserAccessList.defaultProps = {
  params: {}
}

export default UserAccessList
