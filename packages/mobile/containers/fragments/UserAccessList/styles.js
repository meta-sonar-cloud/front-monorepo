import { Subheading, Paragraph } from 'react-native-paper'

import styled from 'styled-components/native'

import colors from '@smartcoop/styles/colors'

export const ItemInformation = styled.View`
  flex-shrink: 1;
  flex-direction: column;
`

export const ItemContainer = styled.View`
  justify-content: space-around;
`

export const InfoContainer = styled.View`
  flex-direction: row;
  justify-content: space-around;
`

export const BottomContainer = styled.View`
`

export const ItemRow = styled.View`
  flex-direction: row;
  align-items: flex-start;
  flex: 1;
`

export const LeftContainer= styled.View`
  flex: 1;
  flex-direction: column;
`

export const RightContainer= styled.View`
  flex-direction: column;
  align-items: flex-end;
  justify-content: space-between;
`

export const ItemValue = styled(Paragraph)`
  color: ${ colors.black };
`

export const ItemTitle = styled(Subheading)`
  color: ${ colors.black };
  font-weight: 700;
`

export const ButtonsContainer = styled.View`
  flex-direction: row;
  justify-content: space-around;
`
