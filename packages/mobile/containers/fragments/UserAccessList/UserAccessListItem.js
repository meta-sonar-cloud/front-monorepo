import React, { useMemo } from 'react'
import { View } from 'react-native'

import PropTypes from 'prop-types'

import { useT } from '@smartcoop/i18n'
import { shield } from '@smartcoop/icons'
import Button from '@smartcoop/mobile-components/Button'
import Icon from '@smartcoop/mobile-components/Icon'
import TechnicalPortfolioStatus from '@smartcoop/mobile-components/TechnicalPortfolioStatus'
import { colors } from '@smartcoop/styles'
import { formatPhone } from '@smartcoop/utils/formatters'

import {
  ItemContainer,
  LeftContainer,
  RightContainer,
  ItemValue,
  ItemInformation,
  ItemTitle,
  ItemRow,
  InfoContainer,
  BottomContainer,
  ButtonsContainer
} from './styles'

const TechnicalPortfolioItem = (props) => {
  const { accessRequest, index, configurePermissionModal } = props

  const t = useT()

  const disableRequestAccess = useMemo(
    () => (
      accessRequest?.technicianProprietary?.status
      && (accessRequest?.technicianProprietary?.status === 2
      || accessRequest?.technicianProprietary?.status === 3)
    ),
    [accessRequest]
  )

  return (
    <>
      <View>
        <ItemContainer first={ index === 0 }>
          <InfoContainer>
            <LeftContainer>
              <ItemInformation>
                <ItemRow>
                  <ItemTitle>{accessRequest.name}</ItemTitle>
                </ItemRow>
                <ItemRow>
                  <ItemValue>{formatPhone(accessRequest.document)}</ItemValue>
                </ItemRow>
                <ItemRow>
                  <ItemValue>{accessRequest.cellPhone}</ItemValue>
                </ItemRow>
              </ItemInformation>
            </LeftContainer>
            <RightContainer>
              <ItemRow>
                <ButtonsContainer>
                  <ItemValue>{accessRequest.technicianProprietary ? 'Técnico' : ''}</ItemValue>
                </ButtonsContainer>
              </ItemRow>
              <ItemRow>
                <TechnicalPortfolioStatus statusNumber={ accessRequest.technicianProprietary?.status } />
              </ItemRow>
            </RightContainer>
          </InfoContainer>
          <BottomContainer>
            <Button
              title={ t('configure permission') }
              variant="outlined"
              icon={ <Icon icon={ shield } size={ 20 } color={ disableRequestAccess ? colors.muted : undefined } /> }
              style={ { marginTop: 10 } }
              onPress={ configurePermissionModal }
              disabled={ disableRequestAccess }
              color={ disableRequestAccess ? colors.muted : undefined }
            />
          </BottomContainer>
        </ItemContainer>
      </View>
    </>
  )
}

TechnicalPortfolioItem.propTypes = {
  accessRequest: PropTypes.object.isRequired,
  configurePermissionModal: PropTypes.func.isRequired,
  index: PropTypes.number.isRequired
}

export default TechnicalPortfolioItem
