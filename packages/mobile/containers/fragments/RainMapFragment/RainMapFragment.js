import React, { useEffect, useMemo, useState } from 'react'
import { Overlay } from 'react-native-maps'
import { useSelector, useDispatch } from 'react-redux'

import moment from 'moment'
import PropTypes from 'prop-types'

import filter from 'lodash/filter'
import map from 'lodash/map'

import Loader from '@smartcoop/mobile-components/Loader'
import Maps from '@smartcoop/mobile-components/Maps'
import { WeatherForecastActions } from '@smartcoop/stores/weatherForecast'
import { selectAggregatedRain } from '@smartcoop/stores/weatherForecast/selectorWeatherForecast'
import { momentBackDateFormat } from '@smartcoop/utils/dates'
import { getPolygonCenter } from '@smartcoop/utils/maps'

import * as images from './images'

const RainMapFragment = ({ property }) => {
  const dispatch = useDispatch()
  const [mapReady, setMapReady] = useState(false)

  const rain = useSelector(selectAggregatedRain)

  useEffect(() => {
    dispatch(WeatherForecastActions.searchAggregatedRain(
      moment().subtract(7, 'days').format(momentBackDateFormat),
      moment().format(momentBackDateFormat)
    ))
  }, [dispatch])

  const rainArea = useMemo(
    () => filter(rain, item => item.aggregatedPrecipitation > 0),
    [rain]
  )

  const getImage = rainValue => {
    if (rainValue < 26) return images.red
    if (rainValue >= 26 && rainValue <= 57) return images.orange
    if (rainValue >= 58 && rainValue <= 96) return images.yellow
    if (rainValue >= 97 && rainValue <= 145) return images.green
    if (rainValue >= 146 && rainValue <= 203) return images.blueSky
    if (rainValue >= 204 && rainValue <= 271) return images.blue
    if (rainValue >= 272 && rainValue <= 352) return images.blueOcean
    return images.purple
  }

  const getBounds = (coordinates) => {
    const coord = getPolygonCenter(JSON.parse(coordinates))
    const { latitude, longitude } = coord

    return [
      [latitude - 0.15, longitude - 0.17],
      [latitude + 0.15, longitude + 0.17]
    ]
  }

  return (
    property?.geolocalization ? (
      <>
        <Maps
          zoom={ 8 }
          maxZoom={ 9 }
          mapType="hybrid"
          region={ { ...property?.geolocalization, latitudeDelta: .20, longitudeDelta: 1.50 } }
          onMapReady={ () => setMapReady(true) }
        >
          { map(rainArea, item => (
            <Overlay
              key={ item?.id }
              image={ getImage(item?.aggregatedPrecipitation) }
              bounds={ getBounds(item?.polygonCoordinates) }
            />
          )) }
        </Maps>
        { !mapReady && (
          <Loader />
        ) }
      </>
    ) : (
      null
    )
  )
}

RainMapFragment.propTypes = {
  property: PropTypes.object.isRequired
}

export default RainMapFragment
