import React from 'react'
import { View } from 'react-native'

import Chart from '@smartcoop/mobile-components/Chart'
import { useDairyFarmDeliveryChartOptions } from '@smartcoop/utils/charts'

const DairyFarmDeliveryChart = () => {

  const chartOptions = useDairyFarmDeliveryChartOptions()

  return (
    <View style={ { flex: 1, minHeight: 300, justifyContent: 'center' } }>
      <Chart
        options={ chartOptions }
        containerProps={ { style: { height: 250 } } }
      />
    </View>
  )
}

export default DairyFarmDeliveryChart
