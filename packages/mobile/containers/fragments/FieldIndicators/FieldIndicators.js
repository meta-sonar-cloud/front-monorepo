import React, { useCallback, useEffect, useState, useMemo } from 'react'
import { ActivityIndicator } from 'react-native'
import { useDispatch, useSelector } from 'react-redux'

import moment from 'moment'
import PropTypes from 'prop-types'

import find from 'lodash/find'
import isEmpty from 'lodash/isEmpty'
import map from 'lodash/map'
import sortBy from 'lodash/sortBy'
import uniqBy from 'lodash/uniqBy'

import { useDialog } from '@smartcoop/dialog'
import I18n, { useT } from '@smartcoop/i18n'
import { calendar } from '@smartcoop/icons'
import ColorPalette from '@smartcoop/mobile-components/ColorPalette'
import DatePickerModal from '@smartcoop/mobile-components/DatePicker/DatePickerModal'
import Icon from '@smartcoop/mobile-components/Icon'
import { Scroll } from '@smartcoop/mobile-containers/layouts/AuthenticatedLayout/theme'
import { useSnackbar } from '@smartcoop/snackbar'
import { selectCurrentField } from '@smartcoop/stores/field/selectorField'
import { SateliteSvcActions } from '@smartcoop/stores/sateliteSvc'
import { selectHasMissingImages, selectIndicators, selectIndicatorsLoaded } from '@smartcoop/stores/sateliteSvc/selectorSateliteSvc'
import { colors } from '@smartcoop/styles'
import { momentBackDateFormat } from '@smartcoop/utils/dates'

import DateTabs from './DateTabs'
import {
  Container,
  Row,
  IndicatorButton,
  LoadingContainer,
  LoadingText,
  NoResultContainer,
  NoResultText,
  IconButton
} from './styles'

const FieldIndicators = (props) => {
  const {
    setImageIndicator
  } = props

  const dispatch = useCallback(useDispatch(), [])
  const t = useT()
  const { createDialog, removeDialog } = useDialog()
  const snackbar = useSnackbar()

  const maxCloudsPercent = useMemo(() => 50, [])

  const [period, setPeriod] = useState({
    from: moment().subtract(3, 'months').format(momentBackDateFormat),
    to: moment().format(momentBackDateFormat)
  })

  const [selectedDate, setSelectedDate] = useState(null)
  const [indicatorType, setIndicatorType] = useState()
  const [isFirstRender, setIsFirstRender] = useState(true)

  const indicators = useSelector(selectIndicators)
  const field = useSelector(selectCurrentField)
  const loaded = useSelector(selectIndicatorsLoaded)
  const hasMissingImages = useSelector(selectHasMissingImages)

  const indicatorOptions = useMemo(
    () => [
      { label: t('base map'), value: 'trueColor' },
      { label: 'NDVI', value: 'NDVI' },
      { label: 'NDRE', value: 'NDRE' }
    ],
    [t]
  )

  const indicatorsDates = useMemo(
    () => {
      if (isEmpty(indicators)) return []

      let formattedIndicators = indicators
        .filter(item => item.fieldId === field.id)
        .map(item => ({
          date: item.date,
          isImageTooCloudy: !item.url || item.cloudPercent > maxCloudsPercent,
          cloudPercent: item.cloudPercent
        }))

      formattedIndicators = sortBy(formattedIndicators, item => new Date(item.date))

      return uniqBy(formattedIndicators, item => item.date)
    }, [indicators, field.id, maxCloudsPercent]
  )

  const imageIndicator = useMemo(
    () => find(indicators, { date: selectedDate, indicator: indicatorType })?.url,
    [indicatorType, indicators, selectedDate]
  )

  const searchIndicators = useCallback(
    () => dispatch(SateliteSvcActions.searchIndicatorsByDate({
      period,
      fieldId: field.id
    })),
    [dispatch, field.id, period]
  )

  const handleChangeIndicatorType = useCallback(
    (newIndicatorType) => setIndicatorType(
      old => newIndicatorType !== old ? newIndicatorType : null
    ),
    []
  )

  const handleOpenCalendar = useCallback(
    () => {
      createDialog({
        id: 'indicator-period',
        Component: DatePickerModal,
        props: {
          mode: 'range',
          value: period,
          onChange: (newPeriod) => {
            if (newPeriod.to) {
              setPeriod(newPeriod)
              removeDialog({ id: 'indicator-period' })
            }
          }
        }
      })
    },
    [createDialog, period, removeDialog]
  )

  useEffect(() => {
    if (indicatorsDates[0]) {
      setSelectedDate(indicatorsDates[0].value)
    }
  }, [indicatorsDates, setSelectedDate])

  useEffect(() => {
    if (period.to) {
      searchIndicators()
    }
  }, [period, searchIndicators])

  useEffect(() => {
    setImageIndicator(imageIndicator)
  }, [imageIndicator, setImageIndicator])

  useEffect(() => () => {
    dispatch(SateliteSvcActions.resetSateliteSvc())
  }, [dispatch])

  useEffect(() => {
    setIsFirstRender(false)
  }, [])

  useEffect(() => {
    if (!isFirstRender && hasMissingImages) {
      snackbar.warning(t('the satellite images will be shown soon'))
    }
  }, [t, snackbar, hasMissingImages, isFirstRender])

  return (
    <Container>
      <Row style={ { paddingTop: 0 } }>
        {!!indicatorType && indicatorType !== 'trueColor' && (
          <ColorPalette type={ indicatorType } />
        )}
      </Row>
      <Row
        style={ {
          paddingRight: 0,
          paddingLeft: 0
        } }
      >
        <Scroll horizontal>
          <IconButton onPress={ handleOpenCalendar }>
            <Icon icon={ calendar } color={ colors.white } />
          </IconButton>

          {map(indicatorOptions, indicatorOpt => (
            <IndicatorButton
              key={ indicatorOpt.value }
              title={ indicatorOpt.label }
              active={ indicatorOpt.value === indicatorType }
              onPress={ () => handleChangeIndicatorType(indicatorOpt.value) }
            />
          ))}
        </Scroll>
      </Row>
      { !loaded && (
        <LoadingContainer>
          <ActivityIndicator size={ 24 } />
          <LoadingText>
            <I18n>the satellite images will be shown soon</I18n>
          </LoadingText>
        </LoadingContainer>
      ) }

      { loaded && indicatorsDates?.length > 0 && (
        <Row
          style={ {
            paddingRight: 0,
            paddingLeft: 0
          } }
        >
          <DateTabs
            indicators={ indicatorsDates }
            maxCloudsPercent={ maxCloudsPercent }
            onChange={ value => setSelectedDate(value) }
            value={ selectedDate }
          />
        </Row>
      ) }

      { loaded && !indicatorsDates?.length > 0 && (
        <NoResultContainer>
          <NoResultText>
            <I18n>
            no data found
            </I18n>
          </NoResultText>
        </NoResultContainer>
      ) }

    </Container>
  )
}

FieldIndicators.propTypes = {
  setImageIndicator: PropTypes.func.isRequired
}

export default FieldIndicators
