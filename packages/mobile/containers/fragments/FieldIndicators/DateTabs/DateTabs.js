import React, { useCallback } from 'react'

import moment from 'moment'
import PropTypes from 'prop-types'

import map from 'lodash/map'

import { icon26 } from '@smartcoop/icons/weatherIcons'
import Icon from '@smartcoop/mobile-components/Icon'
import { Scroll } from '@smartcoop/mobile-containers/layouts/AuthenticatedLayout/theme'
import { momentFriendlyDateFormat } from '@smartcoop/utils/dates'

import { DateButton, CloudPercentContainer, WeatherPercentText } from './styles'

const DateTabs = ({
  onChange,
  value,
  indicators,
  maxCloudsPercent
}) => {
  const getIconPercent = useCallback((percent) => {
    if (percent > maxCloudsPercent) {
      return (
        <CloudPercentContainer>
          <WeatherPercentText>{ maxCloudsPercent }%+ &nbsp;</WeatherPercentText>
          <Icon size={ 16 } icon={ icon26 } />
        </CloudPercentContainer>
      )
    }

    if (percent > 0.1) {
      return (
        <CloudPercentContainer>
          <WeatherPercentText>{ Number(percent).toFixed(1) }% &nbsp;</WeatherPercentText>
          <Icon size={ 16 } icon={ icon26 } />
        </CloudPercentContainer>
      )
    }

    return null
  }, [maxCloudsPercent])

  return (
    <Scroll horizontal style={ { minHeight: 42 } }>
      {map(indicators, item => (
        <DateButton
          key={ item.date }
          title={ moment(item.date).format(momentFriendlyDateFormat) }
          active={ item.date === value }
          onPress={ () => onChange(item.date) }
          disabled={ item.isImageTooCloudy }
          style={ { flexDirection: 'row-reverse' } }
          icon={ getIconPercent(item.cloudPercent) }
        />
      ))}
    </Scroll>
  )
}

DateTabs.propTypes = {
  onChange: PropTypes.func.isRequired,
  value: PropTypes.string.isRequired,
  indicators: PropTypes.array.isRequired,
  maxCloudsPercent: PropTypes.number.isRequired
}

export default DateTabs
