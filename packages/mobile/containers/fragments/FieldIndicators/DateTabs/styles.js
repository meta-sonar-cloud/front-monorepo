import styled from 'styled-components/native'

import Button from '@smartcoop/mobile-components/Button'
import colors from '@smartcoop/styles/colors'

export const DateButton = styled(Button).attrs(props => ({
  borderBottomColor: props.active ? colors.secondary : 'transparent',
  ...props
}))`
  color: ${ colors.white };
  margin-left: 10px;
  border-bottom-width: 2px;
  border-bottom-color: ${ ({ active }) => active ? colors.secondary : 'transparent' };
`


export const CloudPercentContainer = styled.View`
  display: flex;
`

export const WeatherPercentText = styled.Text`
  font-size: 8;
  font-weight: bold;
  color: ${ colors.white };
`
