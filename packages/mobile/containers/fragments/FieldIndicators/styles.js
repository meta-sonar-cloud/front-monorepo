import { Dimensions } from 'react-native'

import hexToRgba from 'hex-to-rgba'
import styled from 'styled-components/native'

import Button from '@smartcoop/mobile-components/Button'
import colors from '@smartcoop/styles/colors'

export const Container = styled.View`
  position: absolute;
  bottom: 40px;
  width: ${ Dimensions.get('window').width - 30 }px;
  margin: 0 15px;
  background-color: ${ hexToRgba(colors.black, 0.7) };
  border-radius: 10px;
`

export const Row = styled.View`
  flex-direction: row;
  align-items: center;
  padding: 10px 20px;
`

export const IconButton = styled.TouchableOpacity`
  margin-left: 10px;
  margin-right: 5px;
  margin-top: 8px;
`

export const IndicatorButton = styled(Button).attrs(props => ({
  variant: props.active ? 'contained' : 'outlined',
  color: colors.white,
  ...props
}))`
  background-color: ${ ({ active }) => active ? colors.green : colors.transparent };
  border-color: ${ colors.white };
  margin-left: 5px;
  margin-right: 5px;
`

export const LoadingContainer = styled.View`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  padding-top: 5px;
  padding-bottom: 20px;
`

export const LoadingText = styled.Text`
  color: ${ colors.white };
  text-align: center;
`

export const NoResultContainer = styled.View`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  padding-bottom: 30px;
  padding-top: 15px;
`

export const NoResultText = styled.Text`
  color: ${ colors.white };
  text-align: center;
`
