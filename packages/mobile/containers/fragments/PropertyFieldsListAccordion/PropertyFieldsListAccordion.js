import React, { useCallback, useMemo, useState, useEffect, useRef } from 'react'
import { View , RefreshControl } from 'react-native'
import { useSelector, useDispatch } from 'react-redux'

import { useNavigation, useFocusEffect } from '@react-navigation/native'

import debounce from 'lodash/debounce'
import isEmpty from 'lodash/isEmpty'
import size from 'lodash/size'

import { useDialog } from '@smartcoop/dialog'
import { useT } from '@smartcoop/i18n'
import { noFieldRegistered, filter, emptyFilter } from '@smartcoop/icons'
import Accordion from '@smartcoop/mobile-components/Accordion'
import Button from '@smartcoop/mobile-components/Button'
import EmptyState from '@smartcoop/mobile-components/EmptyState'
import FieldItem from '@smartcoop/mobile-components/FieldItem'
import Icon from '@smartcoop/mobile-components/Icon'
import InputSearch from '@smartcoop/mobile-components/InputSearch'
import FilterFieldsModal from '@smartcoop/mobile-containers/modals/FilterFieldsModal'
import { FieldActions } from '@smartcoop/stores/field'
import { selectFields } from '@smartcoop/stores/field/selectorField'
import { selectCurrentProperty } from '@smartcoop/stores/property/selectorProperty'
import { selectUserCanWrite } from '@smartcoop/stores/user/selectorUser'
import colors from '@smartcoop/styles/colors'

import { ButtonContainer, SearchContainer, FilterFields, ItemRow } from './styles'

const PropertyFieldsListAccordion = () => {
  const fields = useSelector(selectFields)
  const currentProperty = useSelector(selectCurrentProperty)
  const userWrite = useSelector(selectUserCanWrite)

  const mounted = useRef(false)

  const { createDialog } = useDialog()
  const [filters, setFilters] = useState({})
  const [fetching, setFetching] = useState(true)
  const [refreshing, setRefreshing] = useState(false)
  const [filterText, setFilterText] = useState('')
  const [refresh, setRefresh] = useState(true)
  const [debouncedFilterText, setDebouncedFilterText] = useState('')

  const dispatch = useCallback(useDispatch(), [])
  const navigation = useNavigation()
  const t = useT()

  const handleParams = useCallback(
    (values) => (
      Object.keys(values)
        .filter(
          (key) => typeof values[key] === 'boolean' || values[key]
        )
        .reduce(
          (prev, curr) => ({ ...prev, [curr]: values[curr] }), {}
        )
    ),
    []
  )

  const params = useMemo(
    () => {
      const filterParams = {
        ...filters,
        q: debouncedFilterText
      }
      return handleParams(filterParams)
    },
    [debouncedFilterText, filters, handleParams]
  )


  const lastIndex = useMemo(
    () => size(fields) - 1,
    [fields]
  )

  const handleRegisterFieldClick = useCallback(
    () => {
      navigation.navigate('Field', { screen: 'DrawField' })
    },
    [navigation]
  )

  const handleHistoryFieldClick = useCallback(
    () => {
      navigation.navigate('Field', { screen: 'History' })
    },
    [navigation]
  )

  const handleFieldClick = useCallback(
    ({ field }) => {
      dispatch(FieldActions.setCurrentField(field))
      navigation.navigate('Field', { screen: 'Details', params: { fieldId: field.id } })
    },
    [dispatch, navigation]
  )

  const loadFields = useCallback(
    () => {
      if (!isEmpty(currentProperty)) {
        if (isEmpty(fields)) {
          setRefreshing(true)
        }
        setFetching(true)
        dispatch(FieldActions.loadFields(
          params,
          () => {
            mounted.current && setRefreshing(false)
            setFetching(false)
          },
          () => {
            mounted.current && setRefreshing(false)
            setFetching(false)
          }
        ))
      }
    },
    [currentProperty, dispatch, fields, params]
  )

  const loadFieldsLoading = useCallback(
    () => {
      if (!isEmpty(currentProperty)) {
        setRefreshing(true)
        loadFields()
      }
    },
    [currentProperty, loadFields]
  )

  const debouncedChangeSearchFilter = useCallback(
    debounce((value) => {
      setDebouncedFilterText(value)
    }, 300), []
  )

  const onChangeSearchFilter = useCallback(
    (value) => {
      setFilterText(value)
      debouncedChangeSearchFilter(value)
    },
    [debouncedChangeSearchFilter]
  )

  const handleFilter = useCallback(
    (values) => {
      setFilters(values)
    },
    []
  )

  const handleFilterFields = useCallback(
    () => {
      createDialog({
        id: 'filter-fields',
        Component: FilterFieldsModal,
        props: {
          onConfirm: handleFilter,
          filters
        }
      })
    },
    [createDialog, filters, handleFilter]
  )

  const renderItem = useCallback(
    ({ item: field, index }) => (
      <ItemRow
        first={ index === 0 }
        last={ index === lastIndex }
      >
        <FieldItem
          field={ field }
          onPress={ handleFieldClick }
        />
      </ItemRow>
    ),
    [handleFieldClick, lastIndex]
  )

  const emptyFiltersAndData = useMemo(
    () => isEmpty(fields) && isEmpty(params)
    ,
    [fields, params]
  )

  const HeaderComponent = useCallback(
    () => !emptyFiltersAndData && (
      <SearchContainer>
        <View style={ { flex: 1 } }>
          <InputSearch
            detached
            name="field-search-input"
            label={ t('search') }
            value={ filterText }
            onChange={ onChangeSearchFilter }
            style={ { marginBottom: 0 } }
          />
        </View>
        <FilterFields>
          <Button
            variant="outlined"
            style={ {
              borderColor: colors.lightGrey,
              backgroundColor: isEmpty(filters) ? colors.white : colors.secondary
            } }
            onPress={ handleFilterFields }
            icon={
              <Icon icon={ filter } size={ 14 } />
            }
            color={ colors.text }
            backgroundColor={ colors.secondary }
            title={ t('filtrate') }
          />
        </FilterFields>
      </SearchContainer>
    ),
    [emptyFiltersAndData, filterText, filters, handleFilterFields, onChangeSearchFilter, t]
  )

  const FooterComponent = useCallback(
    () => userWrite && isEmpty(params) && (
      <ButtonContainer>
        <View>
          <Button
            onPress={ handleRegisterFieldClick }
            color={ colors.text }
            backgroundColor={ colors.secondary }
            title={ t('register field') }
            disabled={ isEmpty(currentProperty) }
          />
          <Button
            onPress={ handleHistoryFieldClick }
            color={ colors.white }
            backgroundColor={ colors.darkGrey }
            title={ t('field history') }
            disabled={ isEmpty(currentProperty) }
            style={ { marginTop: 10 } }
          />
        </View>
      </ButtonContainer>
    ),
    [currentProperty, handleHistoryFieldClick, handleRegisterFieldClick, params, t, userWrite]
  )

  const flatListProps = useMemo(
    () => ({
      renderItem,
      data: fields,
      keyExtractor: ({ id }) => id,
      contentContainerStyle: {
        paddingTop: 10,
        paddingBottom: 80,
        minHeight: '100%'
      },
      refreshControl: (
        <RefreshControl
          refreshing={ refreshing }
          onRefresh={ loadFields }
        />
      ),
      // eslint-disable-next-line no-nested-ternary
      ListEmptyComponent: fetching
        ? undefined
        : (emptyFiltersAndData
          ? (
            <EmptyState
              text={ t('no field registered') }
              icon={ noFieldRegistered }
            />
          ) : (
            <EmptyState
              text={ t('no results found') }
              icon={ emptyFilter }
            />
          )
        )
    }),
    [emptyFiltersAndData, fetching, fields, loadFields, refreshing, renderItem, t]
  )

  useEffect(() => {
    setFilters({})
    setFilterText('')
    setDebouncedFilterText('')
  }, [currentProperty])

  useEffect(() => {
    if (mounted.current) {
      loadFieldsLoading()
    } else {
      mounted.current = true
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [params])

  useEffect(() => () => {
    mounted.current = false
  }, [])

  useEffect(
    () => {
      loadFields()
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [refresh]
  )

  useFocusEffect(
    useCallback(() => {
      if(!mounted.current) {
        mounted.current = true
      } else {
        setRefresh(old => !old)
      }
    }, [])
  )

  return (
    <Accordion
      insideTab
      HeaderComponent={ HeaderComponent }
      FooterComponent={ FooterComponent }
      flatListProps={ flatListProps }
      onOpened={ loadFields }
      title={ t('field', { howMany: 2 }) }
      textColor={ colors.white }
      closedColor={ colors.secondary }
    />
  )
}

export default PropertyFieldsListAccordion
