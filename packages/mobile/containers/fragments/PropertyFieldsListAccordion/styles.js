import styled from 'styled-components/native'

import colors from '@smartcoop/styles/colors'

export const ButtonContainer = styled.View`
  position: absolute;
  bottom: 0;
  z-index: 2;
  width: 50%;
  left: 25%;
  padding-bottom: 20px;
`

export const Container = styled.SafeAreaView`
  flex: 1;
`

export const SearchContainer = styled.View`
  flex-direction: row;
  margin: 10px 10px 0px;
`

export const FilterFields = styled.View`
  margin-left: 10px;
`

export const ItemRow = styled.View`
  border-color: ${ colors.lightGrey };
  border-top-width: 1px;
  border-bottom-width: ${ ({ last }) => last ? 2 : 0 }px;
  padding: 10px 15px;
`
