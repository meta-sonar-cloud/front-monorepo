import { Subheading, Caption } from 'react-native-paper'

import styled from 'styled-components/native'

import Card from '@smartcoop/mobile-components/Card'
import colors from '@smartcoop/styles/colors'

export const TextCenter = styled.Text`
  text-align: center;
  font-weight: bold;
  color: ${ colors.darkGrey };
`

export const Row = styled.View`
  display: flex;
  flex-direction: row;
  justify-content: space-evenly;
  color: ${ colors.darkGrey };
  align-items: center;
  width: 100%;
  max-width: ${ ({ limitWidth }) => limitWidth ? '100%' : 'auto' };
  padding: 10px 30px 10px;
`

export const CardContainer = styled(Card)`
  margin: 0;
  padding: 0;
`

export const CardContent = styled.View`
`

export const CardButton = styled.TouchableOpacity`
  padding: 5px 15px 7px;
  font-weight: bold;
  width: 100%;
  justify-content: center;
  border-top-width: 1px;
  border-color: #E0E0E0;
`

export const Title = styled(Subheading)`
  font-family: 'Montserrat';
  font-weight: bold;
  letter-spacing: 0;
  margin: 0;
  text-align: center;
  padding-top: 10px;
`

export const AverageContainer = styled.View`
  flex-direction: row;
  align-items: center;
`

export const AverageImage = styled.View`
`

export const AverageNumberContainer = styled.View`
  flex-direction: row;
  align-items: center;
  margin-left: 20px;
`

export const AverageNumber = styled.Text`
  font-weight: bold;
  font-size: 42px;
  color: ${ colors.text };
`

export const AverageType = styled(Subheading)`
  font-weight: bold;
`

export const ViewMore = styled(Subheading)`
  width: 100%;
  text-align: center;
  font-weight: bold;
`

export const MinMaxText = styled(Caption)`
  color: ${ colors.text };
`

export const MinMaxContainer = styled.View`
`

export const MinMaxInfoContainer = styled.View`
  flex-direction: row;
  align-items: center;
`
