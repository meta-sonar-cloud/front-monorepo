
import React, { useEffect, useCallback, useMemo } from 'react'
import { useSelector, useDispatch } from 'react-redux'

import { useNavigation } from '@react-navigation/native'
import moment from 'moment/moment'
import PropTypes from 'prop-types'

import isEmpty from 'lodash/isEmpty'
import padStart from 'lodash/padStart'

import I18n from '@smartcoop/i18n'
import { arrowDownField, arrowUpField } from '@smartcoop/icons'
import * as weatherIcons  from '@smartcoop/icons/weatherIcons'
import Icon from '@smartcoop/mobile-components/Icon'
import { selectCurrentProperty } from '@smartcoop/stores/property/selectorProperty'
import { WeatherForecastActions } from '@smartcoop/stores/weatherForecast'
import { selectCurrent } from '@smartcoop/stores/weatherForecast/selectorWeatherForecast'
import colors from '@smartcoop/styles/colors'
import {
  momentBackDateTimeFormat,
  momentFriendlyExtendedDateFormat
} from '@smartcoop/utils/dates'

import {
  CardContainer,
  CardContent,
  CardButton,
  AverageContainer,
  AverageNumberContainer,
  AverageNumber,
  AverageType,
  Title,
  ViewMore,
  TextCenter,
  Row,
  MinMaxContainer,
  MinMaxInfoContainer,
  MinMaxText
} from './styles'

const WeatherForecastCard = (props) => {
  const { forceRefresh, onLoaded } = props

  const current = useSelector(selectCurrent)
  const currentProperty = useSelector(selectCurrentProperty)

  const navigation = useNavigation()

  const dispatch = useCallback(useDispatch(), [])

  const currentDate = useMemo(
    () => moment(current?.date, momentBackDateTimeFormat)
      .format(momentFriendlyExtendedDateFormat),
    [current]
  )

  const navigateWeatherForecast = useCallback(
    () => {
      navigation.navigate('Property', {
        screen: 'WeatherForecast',
        params: {
          coordinate: currentProperty.geolocalization
        }
      })
    },
    [currentProperty.geolocalization, navigation]
  )

  useEffect(() => {
    if (!isEmpty(currentProperty.geolocalization)) {
      dispatch(WeatherForecastActions.searchCurrentWeather(currentProperty.geolocalization.latitude, currentProperty.geolocalization.longitude))
      dispatch(
        WeatherForecastActions.searchWeatherForecast(
          currentProperty.geolocalization.latitude,
          currentProperty.geolocalization.longitude,
          () => onLoaded(`weather-forecast-${ currentProperty.id }`),
          () => onLoaded(`weather-forecast-${ currentProperty.id }`)
        )
      )
    }
  }, [dispatch, currentProperty.geolocalization, forceRefresh, currentProperty.id, onLoaded])

  if (!current) {
    return null
  }

  return (
    <CardContainer>
      <CardContent>
        <I18n as={ Title }>
          weather forecast
        </I18n>
        <TextCenter>{ currentDate }</TextCenter>

        <Row>

          <AverageContainer>
            <Icon
              icon={ weatherIcons[`icon${ padStart(current.iconCode, 2, '0') }`] }
              size={ 50 }
            />
            <AverageNumberContainer>
              <AverageNumber>{ Math.round(current.average) }</AverageNumber>
              <AverageType>°C</AverageType>
            </AverageNumberContainer>
          </AverageContainer>

          <MinMaxContainer>
            <MinMaxInfoContainer>
              <MinMaxText>{ current?.high }°C</MinMaxText>
              <Icon
                icon={ arrowUpField }
                size={ 10 }
                color={ colors.orange }
                style={ { marginLeft: 10, marginRight: 10 } }
              />
            </MinMaxInfoContainer>
            <MinMaxInfoContainer>
              <MinMaxText>{ current?.low }°C</MinMaxText>
              <Icon
                icon={ arrowDownField }
                size={ 10 }
                color={ colors.blue }
                style={ { marginLeft: 10 } }
              />
            </MinMaxInfoContainer>
          </MinMaxContainer>
        </Row>

      </CardContent>

      <CardButton onPress={ navigateWeatherForecast }>
        <I18n as={ ViewMore }>view more</I18n>
      </CardButton>

    </CardContainer>
  )
}

WeatherForecastCard.propTypes = {
  forceRefresh: PropTypes.bool,
  onLoaded: PropTypes.func
}

WeatherForecastCard.defaultProps = {
  forceRefresh: false,
  onLoaded: () => {}
}

export default WeatherForecastCard
