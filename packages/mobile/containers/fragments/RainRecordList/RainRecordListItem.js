import React, { useCallback, useMemo } from 'react'
import { View } from 'react-native'
import { useDispatch } from 'react-redux'

import {  useNavigation } from '@react-navigation/native'
import moment from 'moment/moment'
import PropTypes from 'prop-types'

import map from 'lodash/map'


import { useDialog } from '@smartcoop/dialog'
import { useT } from '@smartcoop/i18n'
import { pencil , trash, frost, hail } from '@smartcoop/icons'
import Button from '@smartcoop/mobile-components/Button'
import Icon from '@smartcoop/mobile-components/Icon'
import ConfirmModal from '@smartcoop/mobile-components/Modal/ConfirmModal'
import { useSnackbar } from '@smartcoop/snackbar'
import { FieldsMonitorationActions } from '@smartcoop/stores/fieldsMonitoration'
import { colors } from '@smartcoop/styles'

import {
  ItemContainer,
  LeftContainer,
  RightContainer,
  ItemValue,
  ItemInformation,
  ItemTitle,
  ItemRow,
  InfoContainer,
  ButtonsContainer
} from './styles'

const RainRecordListItem = (props) => {
  const { dataPrecipitation, index, onSuccess } = props

  const dispatch = useCallback(useDispatch(), [])
  const snackbar = useSnackbar()
  const { createDialog } = useDialog()
  const navigation = useNavigation()
  const t = useT()

  const navigateRainRecordEdit = useCallback(
    () => {
      navigation.navigate('Property', { screen: 'RainRecordRegister', params: { dataPrecipitation } })
    },
    [dataPrecipitation, navigation]
  )

  const handleDelete = useCallback(
    () => {
      createDialog({
        id: 'confirm-delete',
        Component: ConfirmModal,
        props: {
          onConfirm: () => {
            dispatch(FieldsMonitorationActions.deleteFieldsMonitoration(
              dataPrecipitation.id,
              () => {
                snackbar.success(
                  t('your {this} was deleted', {
                    howMany: 1,
                    gender: 'male',
                    this: t('rain record')
                  })
                )
                onSuccess()
              }
            ))
          },
          message: t('are you sure you want to delete the {this}?', {
            howMany: 1,
            gender: 'male',
            this: t('rain record')
          })
        }
      })
    },
    [createDialog, dataPrecipitation, dispatch, onSuccess, snackbar, t]
  )

  const showIconRegister = useMemo(
    () => {
      if(dataPrecipitation?.frost && dataPrecipitation?.hail) {
        return (
          <ItemRow style={ { alignItems: 'center' } }>
            <ItemValue>Geada e Granizo</ItemValue>
          </ItemRow>
        )
      }

      if(dataPrecipitation?.frost && !dataPrecipitation?.hail) {
        return (
          <ItemRow style={ { alignItems: 'center' } }>
            <Icon size={ 16 } icon={ frost } color={ colors.red } style={ { marginRight: 5 } }/>
            <ItemValue>Geada</ItemValue>
          </ItemRow>
        )
      }

      if(!dataPrecipitation?.frost && dataPrecipitation?.hail){
        return (
          <ItemRow style={ { alignItems: 'center' } }>
            <Icon size={ 16 } icon={ hail } color={ colors.red } style={ { marginRight: 5 } }/>
            <ItemValue>Granizo</ItemValue>
          </ItemRow>
        )
      }

      return <></>

    },
    [dataPrecipitation.frost, dataPrecipitation.hail]
  )

  return (
    <>
      <View>
        <ItemContainer first={ index === 0 }>
          <InfoContainer>
            <LeftContainer>
              <ItemInformation>
                <ItemRow>
                  <ItemTitle>{moment(dataPrecipitation?.occurrenceDate, 'YYYY-MM-DD').format('DD/MM/YYYY')}</ItemTitle>
                </ItemRow>
                <ItemRow>
                  <ItemValue>{`Precipitação: ${ dataPrecipitation.rainMm }mm`}</ItemValue>
                </ItemRow>
                <ItemRow>
                  <ItemValue>{`Aplicado ao: ${ map(dataPrecipitation.fields, field => ` ${ field.fieldName }`) }`}</ItemValue>
                </ItemRow>
              </ItemInformation>
            </LeftContainer>
            <RightContainer>
              <ItemRow>
                <ButtonsContainer>
                  <Button
                    variant='outlined'
                    style={ { width: 36, height: 36, borderColor: colors.lightGrey, marginRight: 10 } }
                    icon={ <Icon size={ 16 } icon={ pencil } color={ colors.black }/> }
                    onPress={ navigateRainRecordEdit }
                  />
                  <Button
                    variant='outlined'
                    style={ { width: 36, height: 36, borderColor: colors.lightGrey } }
                    icon={ <Icon size={ 16 } icon={ trash } color={ colors.red }/> }
                    onPress={ handleDelete }
                  />
                </ButtonsContainer>
              </ItemRow>
              {showIconRegister}
            </RightContainer>
          </InfoContainer>
        </ItemContainer>
      </View>
    </>
  )
}

RainRecordListItem.propTypes = {
  dataPrecipitation: PropTypes.object.isRequired,
  onSuccess: PropTypes.func.isRequired,
  index: PropTypes.number.isRequired
}

export default RainRecordListItem
