import React, { useCallback, useMemo, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import { useFocusEffect } from '@react-navigation/native'
import PropTypes from 'prop-types'

import List from '@smartcoop/mobile-components/List'
import { FieldsMonitorationActions } from '@smartcoop/stores/fieldsMonitoration'
import { selectFieldsMonitoration } from '@smartcoop/stores/fieldsMonitoration/selectorFieldsMonitoration'

import RainRecordListItem from './RainRecordListItem'

const RainRecordList = (props) => {
  const { params } = props
  const dispatch = useCallback(useDispatch(), [])

  const fieldsMonitoration = useSelector(selectFieldsMonitoration)

  const [refresh, setRefresh] = useState(true)

  const deps = useMemo(
    () => [params, refresh],
    [params, refresh]
  )

  const renderItem = useCallback(
    ({ item, index }) => (
      <RainRecordListItem
        dataPrecipitation={ item }
        index={ index }
        onSuccess={ () => setRefresh(old => !old) }
      />
    ),
    []
  )

  const loadFieldsMonitorarionList = useCallback(
    (page, onSuccess, onError) => {
      dispatch(FieldsMonitorationActions.loadFieldsMonitoration(
        { ...params, page },
        onSuccess,
        onError
      ))
    },
    [dispatch, params]
  )

  useFocusEffect(
    useCallback(() => {
      setRefresh(old => !old)
    }, [])
  )

  return (
    <>
      <List
        data={ fieldsMonitoration }
        renderItem={ renderItem }
        onListLoad={ loadFieldsMonitorarionList }
        deps={ deps }
        hideUpdatedAt
      />
    </>
  )
}

RainRecordList.propTypes = {
  params: PropTypes.object
}

RainRecordList.defaultProps = {
  params: {}
}

export default RainRecordList
