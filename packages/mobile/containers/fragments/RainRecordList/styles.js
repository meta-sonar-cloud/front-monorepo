import { Subheading } from 'react-native-paper'

import styled from 'styled-components/native'

import colors from '@smartcoop/styles/colors'

export const ItemInformation = styled.View`
  flex-shrink: 1;
  flex-direction: column;
`

export const ItemContainer = styled.View`
  border-color: ${ colors.lightGrey };
  border-top-width: ${ ({ first }) => first ? 0 : 1 }px;
  justify-content: space-around;
`

export const InfoContainer = styled.View`
  flex-direction: row;
  justify-content: space-around;
`

export const ItemRow = styled.View`
  flex-direction: row;
  align-items: flex-start;
  flex: 1;
  margin: 0 0 5px 0;
`

export const LeftContainer= styled.View`
  flex: 1;
  flex-direction: column;
  margin: 10px 0 5px 20px;
`

export const RightContainer= styled.View`
  flex-direction: column;
  align-items: flex-end;
  justify-content: space-between;
  margin: 10px 20px 5px 20px;
`

export const ItemValue = styled.Text`
  color: ${ colors.black };
  font-size: 16px;
`

export const ItemTitle = styled(Subheading)`
  font-weight: 700;
`

export const ButtonsContainer = styled.View`
  flex-direction: row;
  justify-content: space-around;
`
