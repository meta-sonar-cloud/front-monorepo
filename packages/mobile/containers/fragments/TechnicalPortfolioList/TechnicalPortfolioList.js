import React, { useCallback, useMemo, useState, useRef } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import { useFocusEffect } from '@react-navigation/native'
import PropTypes from 'prop-types'

import { useT } from '@smartcoop/i18n'
import List from '@smartcoop/mobile-components/List'
import { useSnackbar } from '@smartcoop/snackbar'
import {
  selectUserHaveTechnical,
  selectHasOrganizationToken
} from '@smartcoop/stores/authentication/selectorAuthentication'
import { selectCurrentOrganization } from '@smartcoop/stores/organization/selectorOrganization'
import { TechnicalActions } from '@smartcoop/stores/technical'
import { selectTechnicalOwners } from '@smartcoop/stores/technical/selectorTechnical'

import TechnicalPortfolioItem from './TechnicalPortfolioItem'

const TechnicalPortfolioList = ({ params }) => {
  const t = useT()
  const snackbar = useSnackbar()
  const dispatch = useCallback(useDispatch(), [])

  const portfolio = useSelector(selectTechnicalOwners)

  const [refresh, setRefresh] = useState(true)
  const userHaveTechnical = useSelector(selectUserHaveTechnical)
  const hasOrganizationToken = useSelector(selectHasOrganizationToken)
  const currentOrganization = useSelector(selectCurrentOrganization)

  const mounted = useRef(false)

  const deps = useMemo(() => [params, refresh, currentOrganization], [currentOrganization, params, refresh])

  const onRequestAccess = useCallback(
    (item) => {
      dispatch(
        TechnicalActions.requestAccessTechnicalForProprietary(
          { proprietaryId: item.id },
          () => {
            snackbar.success(t('your access request has been sent'))
            setRefresh((old) => !old)
          }
        )
      )
    },
    [dispatch, snackbar, t]
  )

  const onRevokeAccess = useCallback(
    (item) => {
      dispatch(
        TechnicalActions.revokeTechnicalAccess(
          item.proprietaryTechnician?.id,
          3,
          () => {
            snackbar.success(t('your access has been removed'))
            setRefresh((old) => !old)
          },
          (err) => {
            snackbar.error(t(err.message ? err.message : err))
          }
        )
      )
    },
    [dispatch, snackbar, t]
  )

  const onAccessOwner = useCallback(
    (item) => {
      dispatch(
        TechnicalActions.setTechnicalCurrentOwner(
          item,
          () => {},
          (err) => snackbar.error(t(err.message))
        )
      )
    },
    [dispatch, snackbar, t]
  )

  const renderItem = useCallback(
    ({ item, index }) => (
      <TechnicalPortfolioItem
        owner={ item }
        index={ index }
        onRequestAccess={ () => onRequestAccess(item) }
        onRevokeAccess={ () => onRevokeAccess(item) }
        onAccessOwner={ () => onAccessOwner(item) }
      />
    ),
    [onAccessOwner, onRequestAccess, onRevokeAccess]
  )

  const loadPortfolio = useCallback(
    (page, onSuccess, onError) => {
      dispatch(
        TechnicalActions.loadTechnicalOwners(
          { ...params, page },
          onSuccess,
          onError
        )
      )
    },
    [dispatch, params]
  )

  useFocusEffect(
    useCallback(() => {
      if (!mounted.current) {
        mounted.current = true
      } else {
        setRefresh((old) => !old)
      }
    }, [])
  )

  return (
    hasOrganizationToken &&
    userHaveTechnical && (
      <List
        data={ portfolio }
        renderItem={ renderItem }
        onListLoad={ loadPortfolio }
        deps={ deps }
        hideUpdatedAt
      />
    )
  )
}

TechnicalPortfolioList.propTypes = {
  params: PropTypes.object
}

TechnicalPortfolioList.defaultProps = {
  params: {}
}

export default TechnicalPortfolioList
