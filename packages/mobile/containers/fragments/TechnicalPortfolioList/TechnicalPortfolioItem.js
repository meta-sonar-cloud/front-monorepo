import React, { useMemo, useCallback } from 'react'
import { TouchableOpacity, TouchableWithoutFeedback, View } from 'react-native'
// import { useDispatch } from 'react-redux'

import PropTypes from 'prop-types'

import get from 'lodash/get'

import { useDialog } from '@smartcoop/dialog'
import { useT } from '@smartcoop/i18n'
import { shield , trash } from '@smartcoop/icons'
import Button from '@smartcoop/mobile-components/Button'
import Icon from '@smartcoop/mobile-components/Icon'
import ConfirmModal from '@smartcoop/mobile-components/Modal/ConfirmModal'
import TechnicalPortfolioStatus from '@smartcoop/mobile-components/TechnicalPortfolioStatus'
import { colors } from '@smartcoop/styles'
import { formatPhone } from '@smartcoop/utils/formatters'

import {
  ItemContainer,
  LeftContainer,
  RightContainer,
  ItemValue,
  ItemInformation,
  ItemTitle,
  ItemRow,
  InfoContainer,
  BottomContainer,
  ButtonsContainer
} from './styles'

const TechnicalPortfolioItem = (props) => {
  const { owner, index, onRequestAccess, onRevokeAccess, onAccessOwner } = props

  const t = useT()
  const { createDialog } = useDialog()

  const disableRequestAccess = useMemo(
    () => (
      owner?.proprietaryTechnician?.status
      && owner?.proprietaryTechnician?.status !== 2
      && owner?.proprietaryTechnician?.status !== 3
    ),
    [owner]
  )

  const canRevoke = useMemo(
    () => (
      owner.proprietaryTechnician?.status
      && owner.proprietaryTechnician?.status !== 3
    ),
    [owner]
  )

  const disabledAccess = useMemo(
    () => (
      owner.proprietaryTechnician?.status !== 4
      && owner.proprietaryTechnician?.status !== 5
    ),
    [owner]
  )

  const Touchable = useMemo(
    () => disabledAccess ? TouchableWithoutFeedback : TouchableOpacity,
    [disabledAccess]
  )

  const handleDelete = useCallback(
    () => {
      createDialog({
        id: 'confirm-delete',
        Component: ConfirmModal,
        props: {
          onConfirm: onRevokeAccess,
          message: t('are you sure you want to remove the {this}?', {
            howMany: 1,
            gender: 'male',
            this: t('access', { howMany: 1 })
          })
        }
      })
    },
    [createDialog, onRevokeAccess, t]
  )

  return (
    <Touchable onPress={ disabledAccess ? undefined : onAccessOwner }>
      <View>
        <ItemContainer first={ index === 0 }>
          <InfoContainer>
            <LeftContainer>
              <ItemInformation>
                <ItemRow>
                  <ItemTitle>{owner.name}</ItemTitle>
                </ItemRow>
                <ItemRow>
                  <ItemValue>{formatPhone(owner.cellPhone)}</ItemValue>
                </ItemRow>
                <ItemRow>
                  <ItemValue>{owner.email}</ItemValue>
                </ItemRow>
              </ItemInformation>
            </LeftContainer>
            <RightContainer>
              <ItemRow>
                <ItemValue>{get(owner, 'organizationUser[0].registry')}</ItemValue>
              </ItemRow>
              <ItemRow>
                <ButtonsContainer>
                  <Button
                    variant='outlined'
                    style={ { width: 36, height: 36, borderColor: colors.lightGrey } }
                    icon={ <Icon size={ 16 } icon={ trash } color={ canRevoke ? colors.red : colors.muted } /> }
                    onPress={ handleDelete }
                    disabled={ !canRevoke }
                  />
                </ButtonsContainer>
              </ItemRow>
            </RightContainer>
          </InfoContainer>
          <BottomContainer>
            <TechnicalPortfolioStatus statusNumber={ owner.proprietaryTechnician?.status } />
            <Button
              title={ t('request permission') }
              variant="outlined"
              icon={ <Icon icon={ shield } size={ 20 } color={ disableRequestAccess ? colors.muted : undefined } /> }
              style={ { marginTop: 10 } }
              onPress={ onRequestAccess }
              disabled={ disableRequestAccess }
              color={ disableRequestAccess ? colors.muted : undefined }
            />
          </BottomContainer>
        </ItemContainer>
      </View>
    </Touchable>
  )
}

TechnicalPortfolioItem.propTypes = {
  owner: PropTypes.object.isRequired,
  onRequestAccess: PropTypes.func.isRequired,
  onRevokeAccess: PropTypes.func.isRequired,
  onAccessOwner: PropTypes.func.isRequired,
  index: PropTypes.number.isRequired
}

export default TechnicalPortfolioItem
