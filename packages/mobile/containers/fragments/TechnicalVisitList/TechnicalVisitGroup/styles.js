
import styled from 'styled-components/native'

import colors from '@smartcoop/styles/colors'

export const ContainerData = styled.View`
  flex-direction: row;
  justify-content: space-between;
  padding: 7px 15px 10px;
  min-height: 100px;
`

export const ContentLeft = styled.View`
  justify-content: space-between;
  flex: 2.5;
`

export const PropertyName = styled.Text`
  font-size: 16px;
  font-weight: bold;
  color: ${ colors.text };
`

export const PropertyOwner = styled.Text`
  font-size: 16px;
`
export const FieldName = styled.Text`
  padding-right: 10px;
  font-size: 16px;
  color: ${ colors.text };
`

export const CropName = styled.Text`
  padding-right: 5px;
  font-size: 16px;
  color: ${ colors.text };
`

export const YearCrop = styled.Text`
  font-size: 16px;
  color: ${ colors.text };
`

export const Estimate = styled.Text`
  font-size: 16px;
  color: ${ colors.text };
`

export const Observations = styled.Text`
  font-size: 16px;
  color: ${ colors.text };
  width: auto;
`

export const ObservationsText = styled.Text`
  padding-left: 5px;
  font-size: 16px;
  color: ${ colors.text };
`

export const Production = styled.Text`
  font-size: 16px;
  font-weight: bold;
  color: ${ colors.text };
`

export const Hour = styled.Text`
  font-size: 16px;
  color: ${ colors.text };
`

export const ContainerHeader = styled.View`
  flex-direction: row;
  padding: 8px;
  justify-content: space-between;
  align-items: center;
`
export const DateHeaderTechnicalVisit = styled.Text`
  font-size: 16px;
  font-weight: bold;
  padding-left: 5px;
  color: ${ colors.text }
`
