import React, { useState, useMemo, useCallback } from 'react'
import { View } from 'react-native'
import Accordion from 'react-native-collapsible/Accordion'

import moment from 'moment/moment'
import PropTypes from 'prop-types'

import isEmpty from 'lodash/isEmpty'
import map from 'lodash/map'

import { pencil, trash, plantAndHand, arrowDown } from '@smartcoop/icons'
import Button from '@smartcoop/mobile-components/Button'
import Card from '@smartcoop/mobile-components/Card'
import Divider from '@smartcoop/mobile-components/Divider'
import Icon from '@smartcoop/mobile-components/Icon'
import colors from '@smartcoop/styles/colors'
import { momentBackDateTimeFormat } from '@smartcoop/utils/dates'

import {
  ContainerData,
  ContentLeft,
  PropertyName,
  PropertyOwner,
  FieldName,
  CropName,
  YearCrop,
  Estimate,
  Production,
  Hour,
  Observations,
  ContainerHeader,
  DateHeaderTechnicalVisit,
  ObservationsText
} from './styles'

const TechnicalVisitGroup = (props) => {
  const { group, items, handleEdit, handleDelete } = props

  const [currentSection, setCurrentSection] = useState([])

  const sections = useMemo(() => [{ group, items }], [group, items])

  const renderHeader = useCallback(
    (section, idx) => (
      <ContainerHeader>
        <View style={ { flexDirection: 'row' } }>
          <Icon
            icon={ plantAndHand }
            size={ 20 }
            color={ colors.yellow }
          />
          <DateHeaderTechnicalVisit>
            {section.group}
          </DateHeaderTechnicalVisit>
        </View>
        <Icon
          style={ { transform: [{ rotate: currentSection === idx ? '180deg' : '360deg' }] } }
          icon={ arrowDown }
          size={ 30 }
          color={ colors.darkGrey }
        />
      </ContainerHeader>
    ),
    [currentSection]
  )

  const renderContent = useCallback(
    (section) => (
      <>
        {map(section.items, item => (
          <View key={ item.id } style={ { paddingBottom: 5 } }>
            <Divider />
            <ContainerData>
              <ContentLeft>
                <PropertyName>
                  {item.property?.name}
                </PropertyName>
                {map(item?.users, user => (
                  <PropertyOwner>
                    {user.name}
                  </PropertyOwner>
                ))}
                <View style={ { flexDirection: 'row' } }>
                  {item.field?.fieldName && (
                    <FieldName>
                      {item.field?.fieldName}
                    </FieldName>
                  )}
                  {item.crop && (
                    <CropName>
                      {item.crop?.description}
                    </CropName>
                  )}
                  {item.growingSeason && (
                    <YearCrop>
                      {item.growingSeason?.sowingSeason}
                    </YearCrop>
                  )}
                </View>
                <View style={ { flexDirection: 'row' } }>
                  <Estimate>
                      Estimativa:
                  </Estimate>
                  <Production>
                    {` ${ item.productivityEstimate || '' } ${ item.estimateUnity || '-' }`}
                  </Production>
                </View>
                { !isEmpty(item.observations) && (
                  <View>
                    <Observations>
                      Observações:
                    </Observations>
                    <ObservationsText>
                      {item.observations}
                    </ObservationsText>
                  </View>
                )}
              </ContentLeft>
              <View style={ { justifyContent: 'space-between', alignItems: 'flex-end', flex: 1 } }>
                <Hour>
                  {moment(item.visitDateTime, momentBackDateTimeFormat).format('HH:mm')}
                </Hour>
                <View style={ { flexDirection: 'row' } }>
                  <View style={ { flexDirection: 'row' } }>
                    <Button
                      variant="outlined"
                      style={ { marginRight: 5, padding: 8, borderColor: colors.lightGrey } }
                      icon={ <Icon size={ 14 } icon={ pencil } color={ colors.black } /> }
                      onPress={ () => handleEdit(item) }
                      backgroundColor={ colors.white }
                    />
                    <Button
                      variant="outlined"
                      style={ { padding: 8, borderColor: colors.lightGrey } }
                      icon={ <Icon size={ 14 } icon={ trash } color={ colors.red } /> }
                      onPress={ () => handleDelete(item) }
                      backgroundColor={ colors.white }
                    />
                  </View>
                </View>
              </View>
            </ContainerData>
          </View>
        ))}
      </>
    ),
    [handleDelete, handleEdit]
  )

  return (
    <Card style={ { marginBottom: 10 } }>
      <Accordion
        activeSections={ currentSection }
        sections={ sections }
        renderHeader={ renderHeader }
        underlayColor={ colors.transparent }
        renderContent={ renderContent }
        onChange={ setCurrentSection }
      />
    </Card>
  )
}

TechnicalVisitGroup.propTypes = {
  group: PropTypes.string.isRequired,
  handleDelete: PropTypes.func.isRequired,
  handleEdit: PropTypes.func,
  items: PropTypes.array
}

TechnicalVisitGroup.defaultProps = {
  items: [],
  handleEdit: () => {}
}

export default TechnicalVisitGroup
