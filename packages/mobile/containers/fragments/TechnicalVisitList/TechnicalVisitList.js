import React, { useCallback, useMemo, useState, useRef } from 'react'
import { useDispatch, useSelector } from 'react-redux'


import { useFocusEffect , useNavigation } from '@react-navigation/native'
import PropTypes from 'prop-types'

import groupBy from 'lodash/groupBy'
import map from 'lodash/map'

import { useDialog } from '@smartcoop/dialog'
import { useT } from '@smartcoop/i18n'
import List from '@smartcoop/mobile-components/List'
import ConfirmModal from '@smartcoop/mobile-components/Modal/ConfirmModal'
import { useSnackbar } from '@smartcoop/snackbar'
import { TechnicalActions } from '@smartcoop/stores/technical'
import { selectTechnicalVisits } from '@smartcoop/stores/technical/selectorTechnical'

import TechnicalVisitGroup from './TechnicalVisitGroup'

const TechnicalVisitList = (props) => {
  const { params } = props
  const { createDialog } = useDialog()
  const dispatch = useCallback(useDispatch(), [])
  const snackbar = useSnackbar()
  const navigation = useNavigation()
  const t = useT()
  const technicalVisits = useSelector(selectTechnicalVisits)
  const [refresh, setRefresh] = useState(true)

  const mounted = useRef(false)

  const technicalVisitGroupedByDate = useMemo(
    () => {
      const result = groupBy(technicalVisits, 'groupDate')
      return map(result, (items, group) => ({ group, items }))
    }
    ,
    [technicalVisits]
  )

  const deps = useMemo(
    () => [params, refresh],
    [params, refresh]
  )

  const handleEdit = useCallback(
    (row) => {
      navigation.navigate('TechnicalRegisterVisit', { technicalVisit: row })
    },
    [navigation]
  )

  const handleDelete = useCallback(
    (row) => {
      createDialog({
        id: 'confirm-delete',
        Component: ConfirmModal,
        props: {
          onConfirm: () => {
            dispatch(TechnicalActions.deleteTechnicalVisit(
              row.id,
              () => {
                snackbar.success(
                  t('your {this} was deleted', {
                    howMany: 1,
                    gender: 'female',
                    this: t('technical visit')
                  })
                )
              }
            ))
          },
          message: t('are you sure you want to delete the {this}?', {
            howMany: 1,
            gender: 'female',
            this: t('technical visit')
          })
        }
      })
    },
    [createDialog, dispatch, snackbar, t]
  )

  const onListLoad = useCallback(
    (page, onSuccess, onError) => {
      dispatch(TechnicalActions.loadTechnicalVisits(
        { ...params, page },
        onSuccess,
        onError
      ))
    },
    [dispatch, params]
  )

  const renderItem = useCallback(
    ({ item }) => (
      <TechnicalVisitGroup
        group={ item.group }
        items={ item.items }
        handleDelete={ handleDelete }
        handleEdit={ handleEdit }
      />
    ),
    [handleDelete, handleEdit]
  )

  useFocusEffect(
    useCallback(() => {
      if(!mounted.current) {
        mounted.current = true
      } else {
        setRefresh(old => !old)
      }
    }, [])
  )

  return (
    <List
      data={ technicalVisitGroupedByDate }
      onListLoad={ onListLoad }
      renderItem={ renderItem }
      itemKey="group"
      deps={ deps }
      hideUpdatedAt
    />
  )
}

TechnicalVisitList.propTypes = {
  params: PropTypes.object
}

TechnicalVisitList.defaultProps = {
  params: {}
}

export default TechnicalVisitList
