import styled from 'styled-components/native'

import colors from '@smartcoop/styles/colors'


export const IconContainer = styled.View`
  background-color: ${ colors.backgroundHtml };
  height: 34px;
  width: 34px;
  margin-right: 8px;
  justify-content: center;
  align-items: center;
  border-radius: 5px;
`

export const ItemInformation = styled.View`
  flex-shrink: 1;
  flex-direction: column;
`

export const InputView = styled.View`
  margin-left: 10px;
  margin-right: 10px;
`

export const ItemContainer = styled.View`
  justify-content: space-around;
`

export const InfoContainer = styled.View`
  flex-direction: row;
  justify-content: space-around;
`

export const BottomContainer = styled.View`
  margin: 0 10px 10px;
`

export const ItemRow = styled.View`
  flex-direction: row;
  align-items: flex-start;
  flex: 1;
  margin: 0 0 5px 0;
`

export const LeftContainer= styled.View`
  flex: 1;
  flex-direction: column;
`

export const RightContainer= styled.View`
  flex-direction: column;
  align-items: flex-end;
  justify-content: space-between;
`

export const NoDataContainer = styled.View`
  justify-content: center;
  align-items: center;
  margin-top: 10px;
`

export const ItemValue = styled.Text`
  color: ${ colors.black };
  font-size: 16px;
`

export const ItemDate = styled.Text`
  color: ${ colors.darkGrey };
  font-size: 12px;
`

export const ItemTitle = styled.Text`
  color: ${ colors.black };
  font-weight: 700;
  font-size: 16px;
`

export const ItemSubtitle = styled.Text`
  color: ${ colors.black };
  font-size: 14px;
`

export const Infos = styled.View`
  margin-bottom: 7px;
  flex-direction: row;
  justify-content: space-around;
`

export const ButtonsContainer = styled.View`
  flex-direction: row;
  justify-content: space-around;
`

export const FlatListQuotation = styled.FlatList`
  width: 100%;
`

export const Italic = styled.Text`
  font-style: italic;
  font-size: 12px;
  color: ${ colors.blackLight };
`

export const FloatButtonContainer = styled.View`
  position: absolute;
  bottom: 0;
  z-index: 2;
  width: 50%;
  left: 25%;
  padding-bottom: 20px;
`

export const Actions = styled.View`
  display: flex;
  flex-direction: row;
  justify-content: flex-end;
`

export const ButtonContainer = styled.View`
  position: absolute;
  bottom: 0;
  z-index: 2;
  width: 50%;
  left: 25%;
  padding-bottom: 30px;
`

export const Top = styled.View`
  display: flex;
  flex-direction: row;
`
