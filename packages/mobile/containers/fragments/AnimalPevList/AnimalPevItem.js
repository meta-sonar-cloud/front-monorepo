import React, { useCallback } from 'react'
import { useSelector } from 'react-redux'

import { useNavigation } from '@react-navigation/native'
import moment from 'moment/moment'
import PropTypes from 'prop-types'

import {  pencil } from '@smartcoop/icons'
import Button from '@smartcoop/mobile-components/Button'
import Icon from '@smartcoop/mobile-components/Icon'
import { selectUserCanWrite } from '@smartcoop/stores/user/selectorUser'
import { colors } from '@smartcoop/styles'
import { momentBackDateFormat, momentFriendlyDateFormat } from '@smartcoop/utils/dates'

import {
  ItemContainer,
  LeftContainer,
  RightContainer,
  ItemValue,
  ItemInformation,
  ItemTitle,
  ItemRow,
  InfoContainer,
  Actions
} from './styles'

const AnimalPevItem = (props) => {
  const { pev } = props

  const navigation = useNavigation()

  const userWrite = useSelector(selectUserCanWrite)

  const handleEdit = useCallback(
    () => {
      navigation.navigate('DairyFarm', { screen: 'RegisterAnimalPev', params: { pev } })
    },
    [navigation, pev]
  )

  return (
    <ItemContainer>
      <InfoContainer>
        <LeftContainer>
          <ItemInformation>
            <ItemRow>
              <ItemValue>
                {pev?.able ? 'Apta' : 'Não apta'}
              </ItemValue>
            </ItemRow>
            <ItemRow>
              <ItemValue>{pev.reason}</ItemValue>
            </ItemRow>
          </ItemInformation>
        </LeftContainer>
        <RightContainer>
          <ItemRow>
            <ItemTitle>
              {moment(pev.registryDate, momentBackDateFormat).format(momentFriendlyDateFormat)}
            </ItemTitle>
          </ItemRow>

          {userWrite && (
            <Actions>
              <Button
                variant="outlined"
                style={ {
                  marginRight: 5,
                  minWidth: 0,
                  borderColor: colors.lightGrey
                } }
                onPress={ handleEdit }
                icon={ <Icon size={ 14 } icon={ pencil } color={ colors.black } /> }
                backgroundColor={ colors.white }
              />
            </Actions>
          )}
        </RightContainer>
      </InfoContainer>
    </ItemContainer>
  )
}

AnimalPevItem.propTypes = {
  pev: PropTypes.object.isRequired
}

export default AnimalPevItem
