import React, { useEffect, useCallback, useMemo, useState, useRef } from 'react'
import { View } from 'react-native'
import { useDispatch, useSelector } from 'react-redux'

import { useFocusEffect, useNavigation } from '@react-navigation/native'

import isEmpty from 'lodash/isEmpty'

import { useDialog } from '@smartcoop/dialog'
import { useT } from '@smartcoop/i18n'
import Button from '@smartcoop/mobile-components/Button'
import FilterButton from '@smartcoop/mobile-components/FilterButton'
import List from '@smartcoop/mobile-components/List'
import FilterAnimalPevModal from '@smartcoop/mobile-containers/modals/dairyFarm/FilterAnimalPevModal'
import { AnimalActions } from '@smartcoop/stores/animal'
import { selectCurrentAnimal , selectAnimalPevList } from '@smartcoop/stores/animal/selectorAnimal'
import { colors } from '@smartcoop/styles'

import AnimalPevItem from './AnimalPevItem'
import { Top, ButtonContainer } from './styles'

const AnimalPevList = () => {
  const navigation = useNavigation()
  const dispatch = useCallback(useDispatch(), [])
  const t = useT()
  const { createDialog } = useDialog()

  const pveList = useSelector(selectAnimalPevList)
  const currentAnimal = useSelector(selectCurrentAnimal)

  const [refresh, setRefresh] = useState(true)
  const [filters, setFilters] = useState({})

  const mounted = useRef(false)

  const deps = useMemo(
    () => [refresh, filters],
    [refresh, filters]
  )

  const registerDisabled = useMemo(
    () => (
      currentAnimal?.animalStatus?.name !== 'PEV'
    ),[currentAnimal]
  )

  const handleRefresh = useCallback(
    () => setRefresh(old => !old),
    []
  )

  const renderItem = useCallback(
    ({ item }) => (
      <AnimalPevItem
        pev={ item }
        handleRefresh={ handleRefresh }
      />
    ),
    [handleRefresh]
  )

  const loadPveList = useCallback(
    (page, onSuccess, onError) => {
      dispatch(AnimalActions.loadAnimalPevList(
        { ...filters, page, orderBy: '-registry_date' },
        onSuccess,
        onError
      ))
    },
    [dispatch, filters]
  )

  const openFilterModal = useCallback(
    () => {
      createDialog({
        id: 'filter-fields',
        Component: FilterAnimalPevModal,
        props: {
          onSubmit: setFilters,
          filters
        }
      })
    },
    [createDialog, filters]
  )

  useFocusEffect(
    useCallback(() => {
      if(!mounted.current) {
        mounted.current = true
      } else {
        handleRefresh()
      }
    }, [handleRefresh])
  )

  useEffect(
    () => {
      dispatch(AnimalActions.loadCurrentAnimal())
    },[dispatch]
  )

  return (
    <>
      <Top>
        <FilterButton
          id="insemination-filter-button"
          onPress={ openFilterModal }
          isActive={ !isEmpty(filters) }
          style={ { flex: 1 } }
        />
      </Top>
      <List
        data={ pveList }
        renderItem={ renderItem }
        onListLoad={ loadPveList }
        deps={ deps }
        hideUpdatedAt
      />
      <ButtonContainer>
        <View>
          <Button
            itemKey="code"
            id="register-animal"
            textStyle={ { fontSize: 16 } }
            backgroundColor={ colors.secondary }
            color={ colors.text }
            onPress={ () => navigation.navigate('DairyFarm', { screen: 'RegisterAnimalPev', params: { pev: {} } }) }
            title={ t('diagnostic pev') }
            disabled={ registerDisabled }
          />
        </View>
      </ButtonContainer>
    </>
  )
}

export default AnimalPevList
