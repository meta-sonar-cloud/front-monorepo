import React, { useCallback }  from 'react'
import { useSelector , useDispatch } from 'react-redux'

import { useNavigation } from '@react-navigation/native'
import PropTypes from 'prop-types'

import { useDialog } from '@smartcoop/dialog'
import I18n, { useT } from '@smartcoop/i18n'
import { barnYellow, pencil, trash } from '@smartcoop/icons'
import Button from '@smartcoop/mobile-components/Button'
import Icon from '@smartcoop/mobile-components/Icon'
import ConfirmModal from '@smartcoop/mobile-components/Modal/ConfirmModal'
import { PropertyActions } from '@smartcoop/stores/property'
import { selectUserCanWrite } from '@smartcoop/stores/user/selectorUser'
import { colors } from '@smartcoop/styles'
import { formatNumber } from '@smartcoop/utils/formatters'

import {
  Header,
  CardItem,
  FarmName,
  AreasContainer,
  AreaContainer,
  InfoContentTitle,
  InfoContent,
  DotsContainer
} from './styles'

const PropertyResumeCard = ({ property }) => {
  const { createDialog } = useDialog()
  const navigation = useNavigation()
  const dispatch = useDispatch()
  const t = useT()

  const userWrite = useSelector(selectUserCanWrite)

  const editProperty = useCallback(() => {
    dispatch(PropertyActions.setEditPropertyData(property.id, property))
    navigation.navigate('Property', { screen: 'Identification', params: { isEditing: true } })

  }, [dispatch, navigation, property])

  const deleteProperty = useCallback(propertyLocal => {
    createDialog({
      id: 'confirm-delete',
      Component: ConfirmModal,
      props: {
        onConfirm: () => {
          dispatch(PropertyActions.deleteProperty(propertyLocal.id))
          dispatch(PropertyActions.saveCurrentProperty({}))
        },
        message: t('are you sure you want to delete the {this}?', {
          howMany: 1,
          gender: 'female',
          this: t('property', {
            howMany: 1
          })
        })
      }
    })
  }, [createDialog, dispatch, t])

  return (
    <CardItem
      headerRight={ (
        <DotsContainer>
          <Button
            variant="outlined"
            style={ { borderColor: colors.lightGrey } }
            icon={ <Icon size={ 14 } icon={ pencil } color={ colors.black } /> }
            backgroundColor={ colors.white }
            onPress={ editProperty }
            disabled={ !userWrite }
          />
          <Button
            variant="outlined"
            style={ { padding: 8, borderColor: colors.lightGrey } }
            icon={ <Icon size={ 14 } icon={ trash } color={ colors.red } /> }
            backgroundColor={ colors.white }
            onPress={ () => deleteProperty(property) }
            disabled={ !userWrite }
          />
        </DotsContainer>
      ) }
    >
      <Header>
        <Icon
          icon={ barnYellow }
          size={ 55 }
        />

        <FarmName>
          {property.name}
        </FarmName>
      </Header>

      <AreasContainer>
        <AreaContainer>
          <I18n as={ InfoContentTitle }>
            total area
          </I18n>
          <InfoContent>
            {formatNumber(property.totalArea)} ha
          </InfoContent>
        </AreaContainer>
      </AreasContainer>

    </CardItem>
  )}

PropertyResumeCard.propTypes = {
  property: PropTypes.object.isRequired
}

export default PropertyResumeCard
