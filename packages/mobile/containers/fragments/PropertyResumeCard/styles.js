import { Paragraph, Subheading } from 'react-native-paper'

import styled from 'styled-components/native'

import Card from '@smartcoop/mobile-components/Card'
import colors from '@smartcoop/styles/colors'

export const CardItem = styled(Card).attrs({
  cardStyle: {
    width: '100%',
    padding: 0
  },
  childrenStyle: {
    padding: 20,
    paddingBottom: 10
  }
})`
  margin-top: 5px;
  margin-bottom: 0;
`

export const Header = styled.View`
  align-items: center;
  justify-content: center;
`

export const DotsContainer = styled.View`
  flex-direction: row;
  width: 100%;
  justify-content: flex-end;
  padding-top: 10px;
  padding-right: 3px;
`

export const FarmName = styled(Paragraph)`
  font-weight: 600;
  font-size: 16px;
  padding: 10px 10px 5px;
  text-align: center;
`

export const AreasContainer = styled.View`
  flex-direction: row;
  align-items: center;
  justify-content: center;
  margin-bottom: 10px;
`

export const AreaContainer = styled.View``

export const InfoContent = styled(Subheading)`
  margin: 0;
  text-align: center;
`

export const InfoContentTitle = styled(InfoContent)`
  color: ${ colors.darkGrey };
`
