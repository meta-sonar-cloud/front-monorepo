import React, { useState } from 'react'
import { View } from 'react-native'

import { useT } from '@smartcoop/i18n'
import Chart from '@smartcoop/mobile-components/Chart'
import { Scroll } from '@smartcoop/mobile-containers/layouts/AuthenticatedLayout/theme'
import { useDairyFarmQualityChartOptions } from '@smartcoop/utils/charts'

import { TabButton } from './styles'

const DairyFarmQualityChart = () => {
  const t = useT()

  const [type, setType] = useState('protein')

  const chartOptions = useDairyFarmQualityChartOptions(type)

  return (
    <View style={ { flex: 1, minHeight: 350, justifyContent: 'center' } }>
      <Scroll
        horizontal
        style={ {
          paddingTop: 5,
          paddingBottom: 5,
          paddingLeft: 15,
          paddingRight: 15
        } }
      >
        <TabButton
          id="set-ccs"
          activeTab={ type === 'ccs' }
          onPress={ () => setType('ccs') }
          title={ t('ccs') }
        />
        <TabButton
          id="set-ctb"
          activeTab={ type === 'ctb' }
          onPress={ () => setType('ctb') }
          title={ t('ctb') }
        />
        <TabButton
          id="set-protein"
          activeTab={ type === 'protein' }
          onPress={ () => setType('protein') }
          title={ t('protein') }
        />
        <TabButton
          id="set-fat"
          activeTab={ type === 'fat' }
          onPress={ () => setType('fat') }
          title={ t('fat') }
        />
      </Scroll>
      <Chart
        options={ chartOptions }
        containerProps={ { style: { height: 250 } } }
      />
    </View>
  )
}

export default DairyFarmQualityChart
