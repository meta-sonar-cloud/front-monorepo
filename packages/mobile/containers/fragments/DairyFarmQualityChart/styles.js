import styled from 'styled-components/native'

import Button from '@smartcoop/mobile-components/Button'
import colors from '@smartcoop/styles/colors'

export const TabButton = styled(Button).attrs(props => ({
  color: props.activeTab ? colors.white : colors.darkGrey,
  backgroundColor: props.activeTab ? colors.black : colors.backgroundHtml
}))`
  margin-right: 10px;
`
