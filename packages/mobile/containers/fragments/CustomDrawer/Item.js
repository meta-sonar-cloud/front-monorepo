import React from 'react'

import PropTypes from 'prop-types'

import BadgeCounter from '@smartcoop/mobile-components/BadgeCounter'
import Icon from '@smartcoop/mobile-components/Icon'
import colors from '@smartcoop/styles/colors'

import { ItemContainer, ItemText, ItemTextContainer, IconContainer, BadgeContainer } from './styles'

const Item = ({ icon, title, size, active, counter, ...rest }) => (
  <ItemContainer { ...rest }>
    <IconContainer active={ active } size={ size }>
      <Icon icon={ icon } size={ active ? size - (size/2) : size } color={ colors.text } />
    </IconContainer>
    <ItemTextContainer>
      <ItemText active={ active }>
        {title}
      </ItemText>
      <BadgeContainer>
        <BadgeCounter max={ 20 }>
          {counter}
        </BadgeCounter>
      </BadgeContainer>
    </ItemTextContainer>
  </ItemContainer>
)

Item.propTypes = {
  icon: PropTypes.func.isRequired,
  title: PropTypes.string.isRequired,
  size: PropTypes.number,
  active: PropTypes.bool,
  counter: PropTypes.number
}

Item.defaultProps = {
  size: 22,
  active: false,
  counter: 0
}

export default Item
