
import { getStatusBarHeight } from 'react-native-iphone-x-helper'

import styled from 'styled-components/native'

import colors from '@smartcoop/styles/colors'

export const Container = styled.View`
  flex: 1;
  margin: ${ getStatusBarHeight() + 20 }px 0 10px;
`

export const ScrollView = styled.ScrollView`
  padding-top: 10px;
  padding-left: 5px;
`

export const ItemContainer = styled.TouchableOpacity`
  flex-direction: row;
  padding: 15px;
  align-items: center;
`

export const ModuleContainer = styled.TouchableOpacity`
  flex-direction: row;
  width: 100%;
  align-items: center;
`
export const ItemTextContainer = styled.View`
  flex: 1;
  padding-left: 15px;
  justify-content: flex-start;
  flex-direction: row;
`

export const IconContainer = styled.View`
  background-color: ${ ({ active }) => active ? colors.secondary : colors.transparent };
  border-radius: ${ ({ active, size }) => active ? size/2 : 0 }px;
  padding: ${ ({ active, size }) => active ? size/2 : 0 }px;
  width: ${ ({ size }) => size }px;
  height: ${ ({ size }) => size }px;
  align-items: center;
  justify-content: center;
`

export const ItemText = styled.Text`
  font-size: 16px;
  font-weight: ${ ({ active }) => active ? 'bold' : 'normal' };
`

export const BadgeContainer = styled.View`
  margin-left: 10px;
`
