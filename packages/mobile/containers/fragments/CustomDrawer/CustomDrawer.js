/* eslint-disable react/prop-types */
import React, { useCallback, useMemo } from 'react'
import { TouchableOpacity } from 'react-native'
import { useSelector, useDispatch } from 'react-redux'

// import { useFocusEffect, DrawerActions } from '@react-navigation/native'
import { DrawerActions } from '@react-navigation/native'

import isEmpty from 'lodash/isEmpty'

import { bell, barn, change, currency, plantAndHand, userShield, dashboard } from '@smartcoop/icons'
import Avatar from '@smartcoop/mobile-components/Avatar'
import Divider from '@smartcoop/mobile-components/Divider'
import Icon from '@smartcoop/mobile-components/Icon'
import { selectUserHasMultipleModules } from '@smartcoop/stores/authentication/selectorAuthentication'
import { selectNewNotificationsCount } from '@smartcoop/stores/messaging/selectorMessaging'
import { AVAILABLE_MODULES , ModuleActions } from '@smartcoop/stores/module'
import { selectModuleIsTechnical , selectCurrentModule } from '@smartcoop/stores/module/selectorModule'
import { TechnicalActions } from '@smartcoop/stores/technical'
import { selectCurrentOwner } from '@smartcoop/stores/technical/selectorTechnical'
import { selectUser } from '@smartcoop/stores/user/selectorUser'
import colors from '@smartcoop/styles/colors'

import Item from './Item'
import { Container, ScrollView, ModuleContainer } from './styles'

const CustomDrawer = (props) => {
  const { navigation, t } = props

  const currentModule = useSelector(selectCurrentModule)
  const userHasMultipleModules = useSelector((state) => selectUserHasMultipleModules(state, true))
  const technicalCurrentOwner = useSelector(selectCurrentOwner)
  const technicalModules = useSelector(selectModuleIsTechnical)

  const user = useSelector(selectUser)
  const unreadCounter = useSelector(selectNewNotificationsCount)

  const dispatch = useCallback(useDispatch(), [])

  const currentModuleInfos = useMemo(
    () => {
      switch (currentModule) {
        case AVAILABLE_MODULES.digitalProperty:
          return { icon: barn, title: t('digital property') }
        case AVAILABLE_MODULES.commercialization:
          return { icon: currency, title: t('commercialization') }
        case AVAILABLE_MODULES.technical:
          return { icon: plantAndHand, title: t('technical area') }
        case AVAILABLE_MODULES.administration:
          return { icon: dashboard, title: t('administration') }
        default:
          return null
      }
    },
    [currentModule, t]
  )

  const closeDrawer = useCallback(
    () => () => navigation && navigation.dispatch(DrawerActions.closeDrawer),
    [navigation]
  )

  const openProfile = useCallback(
    () => navigation && navigation.navigate('Common', { screen: 'Profile' }),
    [navigation]
  )

  const exitModule = useCallback(
    () => dispatch(ModuleActions.exitCurrentModule()),
    [dispatch]
  )

  const technicalExitProperty = useCallback(
    () => {
      dispatch(TechnicalActions.resetTechnicalCurrentOwner())
      closeDrawer()()
    },
    [closeDrawer, dispatch]
  )

  const handleCurrentModulePress = useCallback(
    () => {
      if (!isEmpty(technicalCurrentOwner)) {
        technicalExitProperty()
      } else if (userHasMultipleModules) {
        exitModule()
      }
    },
    [
      exitModule,
      technicalCurrentOwner,
      technicalExitProperty,
      userHasMultipleModules
    ]
  )

  // useFocusEffect(closeDrawer)

  return (
    <Container>

      <TouchableOpacity onPress={ openProfile }>
        <Avatar
          size={ 55 }
          title={ user?.name }
          subTitle={ t('access profile') }
        />
      </TouchableOpacity>

      <Divider style={ { marginTop: 20 } } />

      {currentModuleInfos && (
        <>
          <ModuleContainer>
            <Item
              active
              size={ 30 }
              icon={ currentModuleInfos.icon }
              title={ currentModuleInfos.title }
              onPress={ handleCurrentModulePress }
              style={ {
                flex: 1,
                paddingTop: 7,
                paddingBottom: 7
              } }
            />
            {userHasMultipleModules && (
              <TouchableOpacity onPress={ exitModule } style={ { paddingRight: 20 } }>
                <Icon icon={ change } color={ colors.darkGrey } size={ 20 } />
              </TouchableOpacity>
            )}
          </ModuleContainer>
          <Divider />
        </>
      )}

      <ScrollView>
        <Item
          icon={ bell }
          title={ t('notification', { howMany: 2 }) }
          onPress={ () => navigation.navigate('Common', { screen: 'Notifications' }) }
          counter={ unreadCounter }
        />

        {!technicalModules && (
          <Item
            icon={ userShield }
            title={ t('user access') }
            onPress={ () => navigation.navigate('Common', { screen: 'UserAccess' }) }
          />
        )}

        {/* {currentModule === AVAILABLE_MODULES.digitalProperty && (
          <Item
            icon={ barn }
            title={ t('property', { howMany: 2 }) }
            onPress={ () => navigation.navigate('Property', { screen: 'Properties' }) }
          />
        )} */}
      </ScrollView>
    </Container>
  )
}

export default CustomDrawer
