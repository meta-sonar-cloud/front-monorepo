import React, { useMemo, useCallback } from 'react'
import { Platform , TouchableOpacity } from 'react-native'
import { useSelector } from 'react-redux'

import { useT } from '@meta-react/i18n'
import { useNavigation } from '@react-navigation/native'

import isEmpty from 'lodash/isEmpty'

import { newCropManagement } from '@smartcoop/icons'
import Accordion from '@smartcoop/mobile-components/Accordion'
import Divider from '@smartcoop/mobile-components/Divider'
import Icon from '@smartcoop/mobile-components/Icon'
import { selectCurrentField } from '@smartcoop/stores/field/selectorField'
import { selectUserCanWrite } from '@smartcoop/stores/user/selectorUser'
import { colors } from '@smartcoop/styles'

import CropManagement from './CropManagement'
import GrowingSeason from './GrowingSeason'
import { SectionContainer , ButtonRegisterManagement } from './styles'
import Weather from './Weather'

// const handleTechnicalVisitList = useCallback(
//   () => {
//     navigation.navigate('Technical', { screen: 'List', params: { cultivateName: growingSeason.crop.description, fieldName: field.fieldName } })
//   },
//   [field.fieldName, growingSeason.crop.description, navigation]
// )

const FieldDetailAccordion = () => {
  const navigation = useNavigation()
  const t = useT()

  const field = useSelector(selectCurrentField)
  const userWrite = useSelector(selectUserCanWrite)

  const growingSeason = useMemo(
    () => !isEmpty(field.growingSeason) ? field.growingSeason : {},
    [field.growingSeason]
  )

  const handleCropManagementRegisterClick = useCallback(
    () => {
      navigation.navigate('CropManagement', { screen: 'Register' })
    },
    [navigation]
  )

  const FooterComponent = useCallback(
    () => userWrite && !isEmpty(growingSeason.cropsManagements) && (
      <ButtonRegisterManagement>
        <TouchableOpacity
          onPress={ handleCropManagementRegisterClick }
        >
          <Icon size={ 70 } icon={ newCropManagement }/>
        </TouchableOpacity>
      </ButtonRegisterManagement>
    ),
    [growingSeason, handleCropManagementRegisterClick, userWrite]
  )

  return (
    <Accordion
      paddingIos={ Platform.OS === 'ios' }
      FooterComponent={ FooterComponent }
      title={ t('field details') }
      textColor={ colors.white }
      closedColor={ colors.secondary }
    >

      <SectionContainer>
        <GrowingSeason />
      </SectionContainer>

      <Divider />

      <SectionContainer>
        <Weather />
      </SectionContainer>

      <Divider />

      <SectionContainer>
        {!isEmpty(growingSeason) && <CropManagement />}
      </SectionContainer>
    </Accordion>
  )
}

export default FieldDetailAccordion
