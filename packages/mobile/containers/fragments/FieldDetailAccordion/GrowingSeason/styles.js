import { Title, Subheading } from 'react-native-paper'

import styled from 'styled-components/native'

export const Container = styled.View`
`

export const ContainerGrowing = styled.View`
margin-bottom: 10px;
`

export const CropText = styled(Title)`
  font-weight: 500;
`

export const CultivationText = styled(Subheading)`
`

export const CultivationDate = styled(Subheading)`
  /* font-weight: 600; */
`

export const Row = styled.View`
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`
