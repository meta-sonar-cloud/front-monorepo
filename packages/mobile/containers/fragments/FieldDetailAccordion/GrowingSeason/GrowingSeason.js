import React, { useCallback, useMemo } from 'react'
import { View } from 'react-native'
import { useDispatch, useSelector } from 'react-redux'

import { useNavigation } from '@react-navigation/native'

import isEmpty from 'lodash/isEmpty'
import upperFirst from 'lodash/upperFirst'

import { useDialog } from '@smartcoop/dialog'
import  I18n, { useT } from '@smartcoop/i18n'
import { bug, trash, pencil , emptyCrop } from '@smartcoop/icons'
import Button from '@smartcoop/mobile-components/Button'
import EmptyState from '@smartcoop/mobile-components/EmptyState'
import GrowingSeasonStatus from '@smartcoop/mobile-components/GrowingSeasonStatus'
import Icon from '@smartcoop/mobile-components/Icon'
import ConfirmModal from '@smartcoop/mobile-components/Modal/ConfirmModal'
import { useSnackbar } from '@smartcoop/snackbar'
import { FieldActions } from '@smartcoop/stores/field'
import { selectCurrentField } from '@smartcoop/stores/field/selectorField'
import { PropertyActions } from '@smartcoop/stores/property'
import { selectUserCanWrite } from '@smartcoop/stores/user/selectorUser'
import colors from '@smartcoop/styles/colors'

import { SectionTitle } from '../styles'
import {
  Container,
  ContainerGrowing,
  CultivationDate,
  Row,
  CropText,
  CultivationText
} from './styles'

const GrowingSeason = () => {
  const navigation = useNavigation()
  const t = useT()
  const snackbar = useSnackbar()
  const { createDialog } = useDialog()

  const dispatch = useCallback(useDispatch(), [])

  const field = useSelector(selectCurrentField)
  const userWrite = useSelector(selectUserCanWrite)

  const growingSeason = useMemo(
    () => !isEmpty(field.growingSeason) ? field.growingSeason : {},
    [field.growingSeason]
  )

  const handleGrowingSeasonRegisterClick = useCallback(
    () => {
      navigation.navigate('GrowingSeason', { screen: 'Register' })
    },
    [navigation]
  )

  const handlePestReportClick = useCallback(
    () => {
      navigation.navigate('PestReport', { screen: 'Map', params: { coordinates: field.polygonCoordinates } })
    },
    [field.polygonCoordinates, navigation]
  )

  const handleCropDelete = useCallback(
    () => {
      createDialog({
        id: 'confirm-delete-crop',
        Component: ConfirmModal,
        props: {
          onConfirm: () => {
            dispatch(PropertyActions.setReloadData(true))
            dispatch(FieldActions.deleteOfflineGrowingSeason(
              growingSeason.id,
              () => {
                snackbar.success(
                  t('your {this} was deleted', {
                    howMany: 1,
                    gender: 'male',
                    this: t('crops', { howMany: 1 })
                  })
                )
              }
            ))
          },
          message: t('are you sure you want to delete the {this}?', {
            howMany: 1,
            gender: 'male',
            this: t('crops', { howMany: 1 })
          })
        }
      })
    },
    [createDialog, dispatch, growingSeason.id, snackbar, t]
  )


  const statusGrowing = useMemo(
    () => {
      if (isEmpty(growingSeason.closed)) {
        return 'ativo'
      }
      return 'encerrado'
    },
    [growingSeason.closed]
  )

  return isEmpty(growingSeason) ? (
    <Container>
      <I18n as={ SectionTitle } params={ { howMany: 1 } }>
        crops
      </I18n>
      <EmptyState
        icon={ emptyCrop }
        text={ t('no crops registered') }
        action={ !userWrite ? undefined: {
          text: t('register crops'),
          onClick: handleGrowingSeasonRegisterClick
        } }
      />
    </Container>
  ) : (
    <Container>
      <ContainerGrowing>

        <Row>
          <I18n as={ SectionTitle } params={ { howMany: 1 } }>
            crops
          </I18n>
          {userWrite && (
            <Row style={ { justifyContent: 'flex-end' } }>
              <Button
                variant="outlined"
                style={ { marginRight: 5, borderColor: colors.lightGrey } }
                onPress={ handleGrowingSeasonRegisterClick }
                icon={ <Icon size={ 14 } icon={ pencil } color={ colors.black } /> }
                backgroundColor={ colors.white }
              />
              <Button
                variant="outlined"
                style={ { borderColor: colors.lightGrey } }
                onPress={ handleCropDelete }
                icon={ <Icon size={ 16 } icon={ trash } color={ colors.red } /> }
                backgroundColor={ colors.white }
              />
            </Row>
          )}
        </Row>

        <Row>
          {!isEmpty(growingSeason.crop?.description) ? (
            <CropText>
              {upperFirst(growingSeason.crop.description)}
            </CropText>
          ) : <View />}

          <CultivationDate>
            {growingSeason.sowingYear}
          </CultivationDate>
        </Row>

        <Row>
          {!isEmpty(growingSeason?.cultivation?.description) ? (
            <CultivationText>
              {growingSeason.cultivation.description}
            </CultivationText>
          ) : <View />}

          <GrowingSeasonStatus slug={ statusGrowing } />
        </Row>

      </ContainerGrowing>

      {userWrite && (
        <Button
          title={ t('report occurrence') }
          onPress={ handlePestReportClick }
          variant="outlined"
          icon={ <Icon size={ 20 } icon={ bug } color={ colors.black } /> }
        />
      )}
    </Container>
  )
}

export default GrowingSeason
