import { Headline } from 'react-native-paper'

import styled from 'styled-components/native'

export const SectionContainer = styled.View.attrs({
  onStartShouldSetResponder: () => true
})`
  padding: 15px 25px;
`

export const SectionTitle = styled(Headline)`
  font-weight: 700;
  font-family: Montserrat;
`

export const ButtonRegisterManagement = styled.View`
  position: absolute;
  bottom: 0;
  right: 0;
  z-index: 2;
  padding-bottom: 20px;
`
