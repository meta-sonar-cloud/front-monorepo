import React, { useCallback, useMemo, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import { useNavigation } from '@react-navigation/native'
import moment from 'moment/moment'

import isEmpty from 'lodash/isEmpty'
import padStart from 'lodash/padStart'

import I18n, { useT } from '@smartcoop/i18n'
import { chart , arrowDownField, arrowUpField } from '@smartcoop/icons'
import * as weatherIcons  from '@smartcoop/icons/weatherIcons'
import Button from '@smartcoop/mobile-components/Button'
import Icon from '@smartcoop/mobile-components/Icon'
import { selectCurrentField } from '@smartcoop/stores/field/selectorField'
import { WeatherForecastActions } from '@smartcoop/stores/weatherForecast'
import { selectCurrent } from '@smartcoop/stores/weatherForecast/selectorWeatherForecast'
import colors from '@smartcoop/styles/colors'
import {
  momentBackDateTimeFormat,
  momentFriendlyExtendedDateFormat
} from '@smartcoop/utils/dates'
import { getPolygonCenter } from '@smartcoop/utils/maps'

import { SectionTitle } from '../styles'
import {
  Row,
  Container,
  Date,
  AverageContainer,
  AverageNumberContainer,
  AverageNumber,
  AverageType,
  MinMaxContainer,
  MinMaxInfoContainer,
  MinMaxText
} from './styles'

const Weather = () => {
  const t = useT()
  const navigation = useNavigation()

  const current = useSelector(selectCurrent)
  const field = useSelector(selectCurrentField)

  const dispatch = useCallback(useDispatch(), [])

  const fieldCenter = useMemo(
    () => !isEmpty(field.polygonCoordinates) ? getPolygonCenter(field.polygonCoordinates) : {},
    [field]
  )

  const currentDate = useMemo(
    () => moment(current?.date, momentBackDateTimeFormat)
      .format(momentFriendlyExtendedDateFormat),
    [current]
  )

  const navigateWeatherForecast = useCallback(
    () => {
      navigation.navigate('Property', {
        screen: 'WeatherForecast',
        params: {
          coordinate: fieldCenter
        }
      })
    },
    [fieldCenter, navigation]
  )

  const handleTimelineChartClick = useCallback(
    () => {
      navigation.navigate('TimelineChart')
    },
    [navigation]
  )

  useEffect(() => {
    if (!isEmpty(fieldCenter)) {
      dispatch(WeatherForecastActions.searchCurrentWeather(fieldCenter.latitude, fieldCenter.longitude))
      dispatch(
        WeatherForecastActions.searchWeatherForecast(
          fieldCenter.latitude,
          fieldCenter.longitude
        )
      )
    }
  }, [dispatch, fieldCenter])

  return (
    <Container>
      <Row>
        <I18n as={ SectionTitle }>
          field weather
        </I18n>

        <Button
          variant="text"
          onPress={ navigateWeatherForecast }
          title={ t('view more') }
        />
      </Row>

      <Date>{ currentDate }</Date>

      <Row>
        <AverageContainer>
          <Icon
            icon={ weatherIcons[`icon${ padStart(current?.iconCode, 2, '0') }`] }
            size={ 50 }
          />
          <AverageNumberContainer>
            <AverageNumber>{ Math.round(current?.average) }</AverageNumber>
            <AverageType>°C</AverageType>
          </AverageNumberContainer>
        </AverageContainer>

        <MinMaxContainer>
          <MinMaxInfoContainer>
            <MinMaxText>{ current?.high }°C</MinMaxText>
            <Icon
              icon={ arrowUpField }
              size={ 10 }
              color={ colors.orange }
              style={ { marginLeft: 10, marginRight: 10 } }
            />
          </MinMaxInfoContainer>
          <MinMaxInfoContainer>
            <MinMaxText>{ current?.low }°C</MinMaxText>
            <Icon
              icon={ arrowDownField }
              size={ 10 }
              color={ colors.blue }
              style={ { marginLeft: 10 } }
            />
          </MinMaxInfoContainer>
        </MinMaxContainer>
      </Row>

      <Button
        title={ t('field timeline') }
        onPress={ handleTimelineChartClick }
        variant="outlined"
        style={ { marginTop: 10 } }
        icon={ <Icon size={ 22 } icon={ chart } color={ colors.black } /> }
      />
    </Container>
  )
}

export default Weather
