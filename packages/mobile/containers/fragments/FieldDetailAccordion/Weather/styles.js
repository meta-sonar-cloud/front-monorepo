import { Subheading, Caption } from 'react-native-paper'

import styled from 'styled-components/native'

import colors from '@smartcoop/styles/colors'

export const Container = styled.View`
`

export const Row = styled.View`
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`

export const Date = styled.Text`
  text-align: center;
  font-weight: bold;
  color: ${ colors.darkGrey };
`

export const AverageContainer = styled.View`
  flex-direction: row;
  align-items: center;
`

export const AverageNumberContainer = styled.View`
  flex-direction: row;
  align-items: center;
  margin-left: 20px;
`

export const AverageNumber = styled.Text`
  font-weight: bold;
  font-size: 42px;
  color: ${ colors.text };
`

export const AverageType = styled(Subheading)`
  font-weight: bold;
`

export const ViewMore = styled(Subheading)`
  width: 100%;
  text-align: center;
  font-weight: bold;
`

export const MinMaxText = styled(Caption)`
  color: ${ colors.text };
`

export const MinMaxContainer = styled.View`
`

export const MinMaxInfoContainer = styled.View`
  flex-direction: row;
  align-items: center;
`
