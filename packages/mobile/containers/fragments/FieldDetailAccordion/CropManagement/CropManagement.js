import React, { useCallback, useMemo } from 'react'
import { useSelector } from 'react-redux'

import { useNavigation } from '@react-navigation/native'

import { map, reduce, size } from 'lodash'
import isEmpty from 'lodash/isEmpty'

import  I18n, { useT } from '@smartcoop/i18n'
import { emptyManagement } from '@smartcoop/icons'
import EmptyState from '@smartcoop/mobile-components/EmptyState'
import Management from '@smartcoop/mobile-components/Management'
import { selectCurrentField } from '@smartcoop/stores/field/selectorField'
import { selectUserCanWrite } from '@smartcoop/stores/user/selectorUser'

import { SectionTitle } from '../styles'
import {
  Container,
  ManagementItem
} from './styles'

const CropManagement = () => {
  const navigation = useNavigation()
  const t = useT()

  const field = useSelector(selectCurrentField)
  const userWrite = useSelector(selectUserCanWrite)

  const growingSeason = useMemo(
    () => !isEmpty(field.growingSeason) ? field.growingSeason : {},
    [field.growingSeason]
  )

  const countClosedCropsManagement = useMemo(
    () => (reduce(
      map(growingSeason.cropsManagements, cm => cm.realizationDate ? 1 : 0),
      (acc, cm) => cm + acc
    )), [growingSeason]
  )

  const hasAllCropsManagementClosedButOne = useMemo(
    () => (!isEmpty(growingSeason.cropsManagements) &&
    countClosedCropsManagement === size(growingSeason.cropsManagements) - 1), [countClosedCropsManagement, growingSeason]
  )

  const handleCropManagementRegisterClick = useCallback(
    () => {
      navigation.navigate('CropManagement', { screen: 'Register' })
    },
    [navigation]
  )

  const handleCropManagementDetail = useCallback(
    (item) => {
      navigation.navigate('CropManagement',
        { screen: 'Detail', params: { canConcludeGrowingSeason: hasAllCropsManagementClosedButOne, currentCropManagement: item, coordinates: field.polygonCoordinates } })
    },
    [field.polygonCoordinates, hasAllCropsManagementClosedButOne, navigation]
  )

  return (
    <Container>
      <I18n as={ SectionTitle } params={ { howMany: 2 } }>
        cropManagement
      </I18n>

      {!isEmpty(growingSeason.cropsManagements) ? (
        growingSeason.cropsManagements.map((item, index) => (
          <ManagementItem
            key={ item.id }
            first={ index === 0 }
            last={ index === growingSeason.cropsManagements.length - 1 }
          >
            <Management
              key={ item.id }
              date={ item.predictedDate }
              managementType={ item.operation.name }
              done={ item.realizationDate }
              cropManagement={ item }
              onPress={ () => handleCropManagementDetail(item) }
            />
          </ManagementItem>
        )
        )
      ) : (
        <EmptyState
          icon={ emptyManagement }
          text={ t('no management registered') }
          action={ !userWrite ? undefined: {
            text: t('register management'),
            onClick: handleCropManagementRegisterClick
          } }
        />
      )}
    </Container>
  )
}

export default CropManagement
