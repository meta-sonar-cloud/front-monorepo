import styled from 'styled-components/native'

import colors from '@smartcoop/styles/colors'

export const Container = styled.View`
`

export const CropManagementTitle = styled.Text`
  padding-left: 10px;
  font-weight: bold;
  color: ${ colors.black };
`

export const ManagementItem = styled.View`
  padding: 7px 10px;
  border-color: ${ colors.lightGrey };
  border-top-width: ${ (first) => first ? 0 : 1 }px;
  border-bottom-width: ${ (last) => last ? 1 : 0 }px;
`
