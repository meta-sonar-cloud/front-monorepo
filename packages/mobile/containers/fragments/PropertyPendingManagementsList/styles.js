
import { Paragraph } from 'react-native-paper'

import styled from 'styled-components/native'

import { colors } from '@smartcoop/styles'


export const EmptyState = styled(Paragraph)`
  padding: 15px;
  text-align: center;
`

export const Item = styled.View`
  padding: 7px 10px;
  border-color: ${ colors.lightGrey };
  border-top-width: ${ (first) => first ? 0 : 1 }px;
  border-bottom-width: ${ (last) => last ? 1 : 0 }px;
`
