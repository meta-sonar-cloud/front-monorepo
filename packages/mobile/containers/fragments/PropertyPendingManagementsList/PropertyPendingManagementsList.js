import React, { useCallback, useEffect, useState, useRef } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import PropTypes from 'prop-types'

import isEmpty from 'lodash/isEmpty'
import map from 'lodash/map'

import I18n from '@smartcoop/i18n'
import Loader from '@smartcoop/mobile-components/Loader'
import Management from '@smartcoop/mobile-components/Management'
import { PropertyActions } from '@smartcoop/stores/property'
import { selectPropertyPendingManagements, selectReloadData } from '@smartcoop/stores/property/selectorProperty'

import { EmptyState, Item } from './styles'

const PropertyPendingManagementsList = props => {
  const { property, forceRefresh, onLoaded } = props

  const dispatch = useCallback(useDispatch(), [])
  const mounted = useRef(false)

  const [loading, setLoading] = useState(false)

  const propertyPendingManagements = useSelector(selectPropertyPendingManagements)
  const reloadData = useSelector(selectReloadData)

  useEffect(() => {
    if (property.id || reloadData) {
      setLoading(true)
      dispatch(PropertyActions.loadPropertyPendingManagements(
        property.id,
        () => {
          if (mounted.current) {
            setLoading(false)
            onLoaded(`pending-managements-${ property.id }`)
          }
        },
        () => {
          if (mounted.current) {
            setLoading(false)
            onLoaded(`pending-managements-${ property.id }`)
          }
        }
      ))
    }
  }, [dispatch, onLoaded, property, forceRefresh, reloadData])

  useEffect(
    () => {
      mounted.current = true
      return () => {
        mounted.current = false
      }
    },
    []
  )

  return loading && isEmpty(propertyPendingManagements)
    ? <Loader width={ 50 } />
    : (
      <>
        {isEmpty(propertyPendingManagements)
          ? (
            <I18n as={ EmptyState }>
              no found scheduled managements
            </I18n>
          )
          : map(propertyPendingManagements, (cropManagement, index) => (
            <Item
              key={ cropManagement.id }
              first={ index === 0 }
              last={ index === propertyPendingManagements.length - 1 }
            >
              <Management
                date={ cropManagement.predictedDate }
                managementType={ cropManagement.operation.name }
                done={ cropManagement.realizationDate }
                cropManagement={ cropManagement }
                fieldName={ cropManagement.growingSeason?.field?.fieldName }
              />
            </Item>
          ))
        }
      </>
    )
}

PropertyPendingManagementsList.propTypes = {
  property: PropTypes.object,
  forceRefresh: PropTypes.bool,
  onLoaded: PropTypes.func
}

PropertyPendingManagementsList.defaultProps = {
  property: {},
  forceRefresh: false,
  onLoaded: () => {}
}

export default PropertyPendingManagementsList
