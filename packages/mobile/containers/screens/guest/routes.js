import React, { useEffect } from 'react'

import { createStackNavigator } from '@react-navigation/stack'

import { useDialog } from '@smartcoop/dialog'

import CreatePasswordScreen from './CreatePasswordScreen'
import IdentificationScreen from './IdentificationScreen'
import LoginScreen from './LoginScreen'
import OrganizationsScreen from './OrganizationsScreen'
import RecoverPasswordScreen from './RecoverPasswordScreen'
import ResetCodeScreen from './ResetCodeScreen'

const Stack = createStackNavigator()

const GuestScreensRouter = () => {
  const { resetDialogs } = useDialog()

  useEffect(() => {
    resetDialogs()
  }, [resetDialogs])

  return (
    <Stack.Navigator
      screenOptions={ {
        headerShown: false,
        cardStyle: {
          backgroundColor: 'transparent'
        }
      } }
    >
      <Stack.Screen name="Login" component={ LoginScreen } />
      <Stack.Screen name="Identification" component={ IdentificationScreen } />
      <Stack.Screen name="Organizations" component={ OrganizationsScreen } />
      <Stack.Screen name="ResetCode" component={ ResetCodeScreen } />
      <Stack.Screen name="CreatePassword" component={ CreatePasswordScreen } />
      <Stack.Screen name="RecoverPassword" component={ RecoverPasswordScreen } />
    </Stack.Navigator>
  )
}

export default GuestScreensRouter
