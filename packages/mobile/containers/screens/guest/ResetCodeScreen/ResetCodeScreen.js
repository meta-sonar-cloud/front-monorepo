import React, { useRef, useEffect, useCallback } from 'react'
import { Alert } from 'react-native'
import { useDispatch , useSelector } from 'react-redux'

import { useIsFocused , useNavigation, useRoute, CommonActions } from '@react-navigation/native'

import { isEmpty, omit } from 'lodash'

import { useDialog } from '@smartcoop/dialog'
import I18n, { useT } from '@smartcoop/i18n'
import Button from '@smartcoop/mobile-components/Button'
import ResetCodeForm from '@smartcoop/mobile-containers/forms/auth/ResetCodeForm'
import GuestLayout from '@smartcoop/mobile-containers/layouts/GuestLayout'
import { Item, Text, TextUnderlined, ButtonsContainer } from '@smartcoop/mobile-containers/layouts/GuestLayout/theme'
import { AuthenticationActions } from '@smartcoop/stores/authentication'
import { UserActions } from '@smartcoop/stores/user'
import { selectUser } from '@smartcoop/stores/user/selectorUser'

const ResetCodeScreen = () => {
  const formRef = useRef(null)

  const dispatch = useCallback(useDispatch(), [])

  const { removeDialog } = useDialog()

  const t = useT()
  const focused = useIsFocused()
  const navigation = useNavigation()
  const { params: { mode } } = useRoute()
  const user = useSelector(selectUser)

  const { navigate } = navigation

  const handleSubmit = useCallback(
    () => {
      if (mode === 'signUp') {
        const removeLoader = () => {
          setTimeout(() => removeDialog({ id: 'loading' }), 150)
        }

        dispatch(UserActions.saveInitialUser(
          omit(user, ['email', 'cellPhone']),
          () => {
            removeLoader()
            navigate('CreatePassword', { mode })
            removeDialog({ id: 'loading' })
          },
          () => {
            removeLoader()
            removeDialog({ id: 'loading' })
          }
        ))
      } else {
        navigate('CreatePassword', { mode })
        removeDialog({ id: 'loading' })
      }
    },
    [dispatch, mode, navigate, removeDialog, user]
  )

  const sentCodeByEmail = useCallback(
    () => {
      dispatch(AuthenticationActions.requestVerificationCode(
        'email',
        () => Alert.alert(t('code send by email'), t('check your mail app'))
      ))
    },
    [dispatch, t]
  )

  useEffect(() => {
    dispatch(AuthenticationActions.requestVerificationCode('sms'))
  }, [dispatch])

  return (
    <GuestLayout focused={ focused }>
      <Item style={ { paddingTop: 0 } }>
        <Text>
          <I18n>we sent a password reset code via SMS to the final cell phone</I18n>
          <Text style={ { fontWeight: 'bold' } } >{ user.cellPhone.substr(-4)}</Text>.
        </Text>

        <Text>
          <I18n>enter it below</I18n>
        </Text>

        <ResetCodeForm
          ref={ formRef }
          onSubmit={ handleSubmit }
          withoutSubmitButton
        />
      </Item>

      <Item>
        {
          !isEmpty(user?.email) &&
          <TextUnderlined
            onPress={ sentCodeByEmail }
          >
            <I18n>send code by mail</I18n>
          </TextUnderlined>
        }
        <ButtonsContainer>
          <Button
            title={ t('go back') }
            onPress={ () => navigation.dispatch(CommonActions.goBack()) }
            style={ { width: '48%' } }
            variant="outlined"
          />
          <Button
            title={ t('next') }
            onPress={ () => formRef.current.submit() }
            style={ { width: '48%' } }
          />
        </ButtonsContainer>
      </Item>
    </GuestLayout>
  )
}

export default ResetCodeScreen
