import React, { useRef, useCallback } from 'react'
import { useDispatch } from 'react-redux'

import { useIsFocused, useNavigation, useRoute } from '@react-navigation/native'

import I18n, { useT } from '@smartcoop/i18n'
import { circle } from '@smartcoop/icons'
import Button from '@smartcoop/mobile-components/Button'
import Icon from '@smartcoop/mobile-components/Icon'
import CreatePasswordForm from '@smartcoop/mobile-containers/forms/auth/CreatePasswordForm'
import GuestLayout from '@smartcoop/mobile-containers/layouts/GuestLayout'
import { AuthenticationActions } from '@smartcoop/stores/authentication'

import { Item, Text, ButtonsContainer, ListItem } from '../../../layouts/GuestLayout/theme'

const CreatePasswordScreen = () => {
  const formRef = useRef(null)

  const dispatch = useCallback(useDispatch(), [])

  const t = useT()
  const focused = useIsFocused()
  const navigation = useNavigation()
  const { params: { mode } } = useRoute()

  // eslint-disable-next-line no-unused-vars
  const { navigate } = navigation

  const handleSubmit = useCallback(
    () => {
      if (mode === 'signUp') {
        dispatch(AuthenticationActions.loginSuccess())
      } else if (mode === 'recover') {
        navigate('Login')
      }
    },
    [dispatch, mode, navigate]
  )

  return (
    <GuestLayout focused={ focused }>
      <Item style={ { paddingTop: 0 } }>
        <Text>
          <I18n>your password must contain</I18n>
        </Text>

        <ListItem>
          <Icon icon={ circle } size={ 12 } />
          <Text style={ { paddingLeft: 10, marginTop: -3 } }>
            <I18n>at least length of five</I18n>
          </Text>
        </ListItem>

        <ListItem>
          <Icon icon={ circle } size={ 12 } />
          <Text style={ { paddingLeft: 10, marginTop: -3 } }>
            <I18n>at least one letter</I18n>
          </Text>
        </ListItem>

        <ListItem>
          <Icon icon={ circle } size={ 12 } />
          <Text style={ { paddingLeft: 10, marginTop: -3 } }>
            <I18n>at least one number</I18n>
          </Text>
        </ListItem>

        <ListItem>
          <Icon icon={ circle } size={ 12 } />
          <Text style={ { paddingLeft: 10, marginTop: -3 } }>
            <I18n>it must not contain your date of birth, social security number, registration number, telephone number, etc.</I18n>
          </Text>
        </ListItem>

        <CreatePasswordForm
          ref={ formRef }
          onSubmit={ handleSubmit }
          withoutSubmitButton
        />

      </Item>

      <Item>
        <ButtonsContainer style={ { flexDirection: 'row-reverse' } }>
          <Button
            title={ t('confirm') }
            onPress={ () => formRef.current.submit() }
            style={ { width: '48%' } }
          />
        </ButtonsContainer>
      </Item>
    </GuestLayout>
  )
}

export default CreatePasswordScreen
