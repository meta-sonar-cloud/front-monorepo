import React, { useRef, useCallback, useEffect } from 'react'

import { useIsFocused , useNavigation } from '@react-navigation/native'

import I18n, { useT } from '@smartcoop/i18n'
import Button from '@smartcoop/mobile-components/Button'
import Divider from '@smartcoop/mobile-components/Divider'
import LoginForm from '@smartcoop/mobile-containers/forms/auth/LoginForm'
import SignUpForm from '@smartcoop/mobile-containers/forms/auth/SignUpForm'
import GuestLayout from '@smartcoop/mobile-containers/layouts/GuestLayout'
import { Item, Subtitle, Text, TextUnderlined } from '@smartcoop/mobile-containers/layouts/GuestLayout/theme'
import { colors } from '@smartcoop/styles'


const LoginScreen = () => {
  const loginFormRef = useRef(null)
  const signUpFormRef = useRef(null)

  const t = useT()
  const focused = useIsFocused()
  const navigation = useNavigation()

  const { navigate } = navigation

  const handleSignUpSubmit = useCallback(
    () => navigate('Identification'),
    [navigate]
  )

  const handleForgotPasswordClick = useCallback(
    () => {
      navigate('RecoverPassword')
    },
    [navigate]
  )

  // prevent go back
  useEffect(
    () => {
      navigation.addListener('beforeRemove', (e) => e.preventDefault())
    },
    [navigation]
  )

  return (
    <GuestLayout focused={ focused }>
      <Item style={ { paddingTop: 0 } }>
        <Text>
          <I18n>to enter, insert your data</I18n>
        </Text>

        <LoginForm
          ref={ loginFormRef }
          withoutSubmitButton
        />

        <TextUnderlined onPress={ handleForgotPasswordClick }>
          <I18n>I forgot my password</I18n>
        </TextUnderlined>

        <Button
          title={ t('login') }
          onPress={ () => loginFormRef.current.submit() }
          backgroundColor={ colors.secondary }
          color={ colors.primary }
        />
      </Item>

      <Divider />

      <Item>
        <I18n as={ Subtitle }>
            first access?
        </I18n>

        <Text>
          <I18n>to us do your data validation, fill the fields below</I18n>
        </Text>

        <SignUpForm
          ref={ signUpFormRef }
          onSubmit={ handleSignUpSubmit }
        />

      </Item>
    </GuestLayout>
  )
}

export default LoginScreen
