import React, { useRef, useEffect, useCallback } from 'react'
import { useDispatch } from 'react-redux'

import { useIsFocused , useNavigation, CommonActions } from '@react-navigation/native'

import I18n, { useT } from '@smartcoop/i18n'
import Button from '@smartcoop/mobile-components/Button'
import RecoverPasswordForm from '@smartcoop/mobile-containers/forms/auth/RecoverPasswordForm'
import GuestLayout from '@smartcoop/mobile-containers/layouts/GuestLayout'
import { Item, Text, ButtonsContainer } from '@smartcoop/mobile-containers/layouts/GuestLayout/theme'
import { AuthenticationActions } from '@smartcoop/stores/authentication'

const RecoverPasswordScreen = () => {
  const formRef = useRef(null)

  const dispatch = useCallback(useDispatch(), [])

  const t = useT()
  const focused = useIsFocused()
  const navigation = useNavigation()

  const { navigate } = navigation

  const handleSubmit = useCallback(
    () => {
      navigate('ResetCode', { mode: 'recover' })
    },
    [navigate]
  )

  useEffect(
    () => {
      dispatch(AuthenticationActions.resetAuthentication())
    },
    [dispatch]
  )

  return (
    <GuestLayout focused={ focused }>
      <Item style={ { paddingTop: 0 } }>
        <Text>
          <I18n>to recover your password, confirm your data</I18n>
        </Text>

        <RecoverPasswordForm
          ref={ formRef }
          onSubmit={ handleSubmit }
          withoutSubmitButton
        />
      </Item>

      <Item>
        <ButtonsContainer>
          <Button
            title={ t('go back') }
            onPress={ () => navigation.dispatch(CommonActions.goBack()) }
            style={ { width: '48%' } }
            variant="outlined"
          />
          <Button
            title={ t('next') }
            onPress={ () => formRef.current.submit() }
            style={ { width: '48%' } }
          />
        </ButtonsContainer>
      </Item>
    </GuestLayout>
  )
}

export default RecoverPasswordScreen
