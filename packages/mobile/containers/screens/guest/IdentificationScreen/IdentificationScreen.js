import React, { useRef, useCallback } from 'react'

import { useIsFocused, useNavigation, CommonActions } from '@react-navigation/native'

import I18n, { useT } from '@smartcoop/i18n'
import Button from '@smartcoop/mobile-components/Button'
import IdentificationForm from '@smartcoop/mobile-containers/forms/digitalProperty/producer/IdentificationForm'
import GuestLayout from '@smartcoop/mobile-containers/layouts/GuestLayout'
import { Item, Subtitle, Text, ButtonsContainer } from '@smartcoop/mobile-containers/layouts/GuestLayout/theme'

const IdentificationScreen = () => {
  const formRef = useRef(null)

  const t = useT()
  const focused = useIsFocused()
  const navigation = useNavigation()

  const { navigate } = navigation

  const handleSubmit = useCallback(
    () => {
      navigate('Organizations')
    },
    [navigate]
  )

  return (
    <GuestLayout focused={ focused }>
      <Item style={ { paddingTop: 0 } }>
        <Text>
          <I18n>to continue your registration, confirm your data</I18n>
        </Text>

        <I18n as={ Subtitle }>
                identification
        </I18n>

        <IdentificationForm
          ref={ formRef }
          onSubmit={ handleSubmit }
          withoutSubmitButton
        />
      </Item>

      <Item>
        <ButtonsContainer>
          <Button
            title={ t('go back') }
            onPress={ () => navigation.dispatch(CommonActions.goBack()) }
            style={ { width: '48%' } }
            variant="outlined"
          />
          <Button
            title={ t('next') }
            onPress={ () => formRef.current.submit() }
            style={ { width: '48%' } }
          />
        </ButtonsContainer>
      </Item>
    </GuestLayout>
  )
}

export default IdentificationScreen
