import React, { useMemo } from 'react'
import { useSelector } from 'react-redux'

import { useIsFocused, useNavigation, CommonActions } from '@react-navigation/native'

import I18n, { useT } from '@smartcoop/i18n'
import Button from '@smartcoop/mobile-components/Button'
import DataTable from '@smartcoop/mobile-components/DataTable'
import GuestLayout from '@smartcoop/mobile-containers/layouts/GuestLayout'
import { Item, Text, ButtonsContainer } from '@smartcoop/mobile-containers/layouts/GuestLayout/theme'
import { selectUserOrganizations } from '@smartcoop/stores/organization/selectorOrganization'

const OrganizationsScreen = () => {
  const t = useT()
  const focused = useIsFocused()
  const navigation = useNavigation()

  const organizations = useSelector(selectUserOrganizations)

  const { navigate } = navigation

  const columns = useMemo(() => [
    {
      title: t('cooperative', { howMany: 1 }),
      field: 'companyName',
      style: { flex: 2 }
    },
    {
      title: t('register number'),
      field: 'registry',
      style: {
        justifyContent: 'center',
        width: '100%'
      },
      cellStyle: {
        textAlign: 'center'
      },
      headerStyle: {
        textAlign: 'center'
      }
    }
  ], [t])

  return (
    <GuestLayout focused={ focused }>
      <Item style={ { paddingTop: 0 } }>
        <Text>
          <I18n>by our data, we found the registers</I18n>
        </Text>
      </Item>

      <DataTable
        data={ organizations }
        columns={ columns }
      />

      <Item>
        <ButtonsContainer>
          <Button
            title={ t('go back') }
            onPress={ () => navigation.dispatch(CommonActions.goBack()) }
            style={ { width: '48%' } }
            variant="outlined"
          />
          <Button
            title={ t('next') }
            onPress={ () => navigate({ name: 'ResetCode', params: { mode: 'signUp' } }) }
            style={ { width: '48%' } }
          />
        </ButtonsContainer>
      </Item>
    </GuestLayout>
  )
}

export default OrganizationsScreen
