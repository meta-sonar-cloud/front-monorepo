import React, { useCallback, useMemo, useState, useEffect } from 'react'
import { View } from 'react-native'
import { useSelector, useDispatch } from 'react-redux'

import { useNavigation, useFocusEffect } from '@react-navigation/native'

import debounce from 'lodash/debounce'
import isEmpty from 'lodash/isEmpty'

import { useT } from '@smartcoop/i18n'
import { noFieldRegistered, emptyFilter } from '@smartcoop/icons'
import Button from '@smartcoop/mobile-components/Button'
import EmptyState from '@smartcoop/mobile-components/EmptyState'
import InputSearch from '@smartcoop/mobile-components/InputSearch'
import List from '@smartcoop/mobile-components/List'
import MachineItem from '@smartcoop/mobile-components/MachineItem'
import { MachineActions } from '@smartcoop/stores/machine'
import { selectMachinery } from '@smartcoop/stores/machine/selectorMachine'
import { selectCurrentProperty } from '@smartcoop/stores/property/selectorProperty'
import colors from '@smartcoop/styles/colors'

import { TabButton, ScrollViewButtons, InputView, SafeAreaViewList, ButtonContainer } from './styles'

const MachineryListScreen = () => {
  const machinery = useSelector(selectMachinery)
  const currentProperty = useSelector(selectCurrentProperty)
  const [activeTab, setActiveTab] = useState('all-machines')

  const [forceListUpdate, setForceListUpdate] = useState()
  const [filterText, setFilterText] = useState('')
  const [debouncedFilterText, setDebouncedFilterText] = useState('')

  const dispatch = useCallback(useDispatch(), [])
  const navigation = useNavigation()
  const t = useT()
  const filters = useMemo(
    () => ({
      my_machines: activeTab === 'my-machines',
      property_id: currentProperty.id
    }), [activeTab, currentProperty]
  )
  const handleParams = useCallback(
    (values) => (
      Object.keys(values)
        .filter(
          (key) => typeof values[key] === 'boolean' || values[key]
        )
        .reduce(
          (prev, curr) => ({ ...prev, [curr]: values[curr] }), {}
        )
    ),
    []
  )

  const params = useMemo(
    () => {
      const filterParams = {
        ...filters,
        q: debouncedFilterText
      }
      return handleParams(filterParams)
    },
    [debouncedFilterText, filters, handleParams]
  )

  const deps = useMemo(
    () => [currentProperty, forceListUpdate, params],
    [currentProperty, forceListUpdate, params]
  )

  const handleMachineryClick = useCallback(
    () => {
      navigation.navigate('Machinery', { screen: 'Register' })
    },
    [navigation]
  )

  const handleMachineClick = useCallback(
    (machine) => {
      dispatch(MachineActions.setCurrentMachine({ ...machine, isUserMachine: activeTab === 'my-machines' }))
      navigation.navigate('Machinery', { screen: 'Details' })
    },
    [activeTab, dispatch, navigation]
  )

  const loadMachinery = useCallback(
    (page, onSuccess, onError) => {
      if (!isEmpty(currentProperty)) {
        dispatch(MachineActions.loadMachinery(
          params,
          onSuccess,
          onError
        ))
      }
    },
    [currentProperty, dispatch, params]
  )

  const debouncedChangeSearchFilter = useCallback(
    debounce((value) => {
      setDebouncedFilterText(value)
    }, 300), []
  )

  const onChangeSearchFilter = useCallback(
    (value) => {
      setFilterText(value)
      debouncedChangeSearchFilter(value)
    },
    [debouncedChangeSearchFilter]
  )

  const renderItem = useCallback(
    ({ item, id }) => (
      <MachineItem
        machine={ item }
        key={ id }
        onPress={ handleMachineClick }
      />
    ),
    [handleMachineClick]
  )

  const emptyFiltersAndData = useMemo(
    () => isEmpty(machinery) && isEmpty(params)
    ,
    [machinery, params]
  )

  useFocusEffect(
    useCallback(() => {
      setForceListUpdate(old => !old)
    }, [])
  )

  useEffect(() => {
    setFilterText('')
    setDebouncedFilterText('')
  }, [currentProperty])

  return (
    <>
      <ScrollViewButtons horizontal>
        <TabButton
          onPress={ () => setActiveTab('all-machines') }
          activeTab={ activeTab === 'all-machines' }
          title={ t('rental machines') }
        />
        <TabButton
          onPress={ () => setActiveTab('my-machines') }
          activeTab={ activeTab === 'my-machines' }
          title={ t('my machines') }
        />
      </ScrollViewButtons>
      {activeTab !== 'account balance' && (
        <InputView>
          <InputSearch
            detached
            name="machinery-search-input"
            label={ t('search') }
            value={ filterText }
            onChange={ onChangeSearchFilter }
            style={ { marginBottom: 0 } }
          />
        </InputView>
      )}
      <SafeAreaViewList>
        <List
          hideUpdatedAt
          data={ machinery }
          renderItem={ renderItem }
          deps={ deps }
          onListLoad={ loadMachinery }
          ListEmptyComponent={ emptyFiltersAndData ? (
            <EmptyState
              text={ t('no field registered') }
              icon={ noFieldRegistered }
            />
          ) : (
            <EmptyState
              text={ t('no results found') }
              icon={ emptyFilter }
            />
          ) }
        />
      </SafeAreaViewList>

      <ButtonContainer>
        <View>
          <Button
            onPress={ handleMachineryClick }
            color={ colors.text }
            backgroundColor={ colors.secondary }
            title={ t('register machinery') }
          />
        </View>
      </ButtonContainer>
    </>
  )
}

export default MachineryListScreen

