import React, { useEffect, useRef, useCallback, useMemo, useState } from 'react'
import { View, Platform, PermissionsAndroid } from 'react-native'
import { useDispatch, useSelector } from 'react-redux'

import { useRoute, useNavigation , useFocusEffect } from '@react-navigation/native'

import isEmpty from 'lodash/isEmpty'
import map from 'lodash/map'
import toString from 'lodash/toString'

import { useDialog } from '@smartcoop/dialog'
import I18n, { useT } from '@smartcoop/i18n'
import { noImage } from '@smartcoop/icons'
import Button from '@smartcoop/mobile-components/Button'
import EmptyState from '@smartcoop/mobile-components/EmptyState'
import Loader from '@smartcoop/mobile-components/Loader'
import ConfirmModal from '@smartcoop/mobile-components/Modal/ConfirmModal'
import ThumbnailImage from '@smartcoop/mobile-components/ThumbnailImage'
import MachineryForm from '@smartcoop/mobile-containers/forms/digitalProperty/machinery/CreateMachineryForm'
import useFile from '@smartcoop/mobile-containers/hooks/useFile'
import { useScreenOptions } from '@smartcoop/mobile-containers/hooks/useScreenOptions'
import AuthenticatedLayout from '@smartcoop/mobile-containers/layouts/AuthenticatedLayout'
import { Container, Item, Scroll, ButtonsContainer, ScreenHeaderTitleContainer, HeaderTitle } from '@smartcoop/mobile-containers/layouts/AuthenticatedLayout/theme'
import { useSnackbar } from '@smartcoop/snackbar'
import { MachineActions } from '@smartcoop/stores/machine'
import { selectCurrentProperty } from '@smartcoop/stores/property/selectorProperty'
import { selectUser } from '@smartcoop/stores/user/selectorUser'
import colors from '@smartcoop/styles/colors'

import { Gallery, Subtitle } from './styles'

const MachineryRegisterScreen = () => {
  const machineryFormRef = useRef(null)
  const snackbar = useSnackbar()
  const navigation = useNavigation()
  const t = useT()
  const route = useRoute()
  const { createDialog } = useDialog()
  const { setOptions } = useScreenOptions()

  const dispatch = useCallback(useDispatch(), [])
  const { machine } = route?.params ?? {}

  const [loading, setLoading] = useState(false)

  const {
    selectedFiles,
    receivedFiles,
    isEmpty: isEmptyFiles,
    handleAdd,
    createFormData,
    handleRemove
  } = useFile([], machine?.machineFiles ?? [])
  const maxNumberFile = 20

  const user = useSelector(selectUser)
  const currentProperty = useSelector(selectCurrentProperty)

  const stateMachine = useMemo(
    () => machine ?? {}, [machine]
  )

  useEffect(() => {
    const requestCameraPermission = async () => {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.CAMERA,
          {
            title: 'Cool Photo App Camera Permission',
            message:
              'Cool Photo App needs access to your camera ' +
              'so you can take awesome pictures.',
            buttonNeutral: 'Ask Me Later',
            buttonNegative: 'Cancel',
            buttonPositive: 'OK'
          }
        )
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          console.log('You can use the camera')
        } else {
          console.log('Camera permission denied')
        }
      } catch (err) {
        console.warn(err)
      }
    }
    Platform.OS === 'android' && requestCameraPermission()
  }, [])

  const defaultValues = useMemo(
    () => ({
      ...stateMachine,
      year: toString(stateMachine?.year) ?? '',
      availableHours: toString(stateMachine?.availableHours) ?? '',
      property: {
        id: currentProperty.id,
        ...stateMachine?.property
      },
      machineBrand: {
        id: null,
        ...stateMachine?.machineBrand
      },
      machineType: {
        id: null,
        ...stateMachine?.machineType
      },
      phone: stateMachine?.phone ?? user.cellPhone,
      machineId: stateMachine?.id ?? null
    }), [currentProperty, stateMachine, user]
  )

  const isEditing = useMemo(
    () => (!isEmpty(defaultValues.machineId)), [defaultValues]
  )

  const submit = useCallback(
    () => {
      machineryFormRef.current.submit()
    },
    []
  )

  const handleClose = useCallback(
    () => {
      navigation.goBack()
    },
    [navigation]
  )

  const onSuccess = useCallback(
    () => {
      snackbar.success(
        t(`your {this} was ${ isEditing ? 'edited' :'registered' }`, {
          howMany: 1,
          gender: 'female',
          this: t('machine', { howMany: 1 })
        })
      )
      setLoading(false)
      handleClose()
    },
    [handleClose, isEditing, snackbar, t]
  )

  const submitNewFiles = useCallback(
    (updatedMachine) => {
      if (isEmpty(selectedFiles)) {
        onSuccess()
      } else {
        dispatch(MachineActions.addMachineFiles(createFormData(), updatedMachine.id,
          onSuccess
        ))
      }
    },
    [createFormData, dispatch, onSuccess, selectedFiles]
  )

  const submitOldFiles = useCallback(
    (updatedMachine) => {
      dispatch(MachineActions.editMachineFiles(receivedFiles, updatedMachine.id,
        () => submitNewFiles(updatedMachine)
      ))
    },
    [dispatch, receivedFiles, submitNewFiles]
  )

  const submitForms = useCallback(
    (data) => {
      setLoading(true)
      if(isEditing) {
        dispatch(MachineActions.updateMachine(
          data,
          defaultValues.machineId,
          (updatedMachine) => submitOldFiles(updatedMachine)
        ))
      } else {
        dispatch(MachineActions.saveMachine(
          data,
          (updatedMachine) => submitNewFiles(updatedMachine)
        ))
      }
    },
    [defaultValues, dispatch, isEditing, submitNewFiles, submitOldFiles]
  )

  const handleClickClose = useCallback(
    (list, removedFile) => {
      createDialog({
        id: 'confirm-delete',
        Component: ConfirmModal,
        props: {
          onConfirm: () => handleRemove(list, removedFile),
          message: t('are you sure you want to delete the {this}?', {
            howMany: 1,
            gender: 'female',
            this: t('image', { howMany: 1 })
          })
        }
      })
    },
    [createDialog, handleRemove, t]
  )

  const renderFiles = useCallback(
    () => {
      const elements = map(receivedFiles, (file) => (
        <ThumbnailImage
          key={ file.id }
          size={ 100 }
          src={ file.fileUrl }
          onClose={ () => handleClickClose('receivedFiles', file) }
        />
      )).concat(
        map(selectedFiles, (file, index) => (
          <ThumbnailImage
            key={ `${ index }-${ file.uri }` }
            size={ 100 }
            src={ file.uri }
            onClose={ () => handleClickClose('selectedFiles', file) }
          />
        ))
      )

      return elements
    },
    [handleClickClose, receivedFiles, selectedFiles]
  )

  const openHandleAdd = useCallback(
    () => {

      if(selectedFiles.length + receivedFiles.length < maxNumberFile) {
        handleAdd()
      } else {
        snackbar.error(
          t('the number of files exceeds the limit of {this}', {
            this: maxNumberFile
          })
        )
      }
    },
    [handleAdd, receivedFiles, selectedFiles, snackbar, t]
  )

  const renderDOMFiles = useMemo(
    () => isEmptyFiles
      ? (
        <EmptyState
          text={ t('no image added') }
          icon={ noImage }
          action={ {
            text: t('add images') ,
            onClick: openHandleAdd
          } }
        />
      ) : renderFiles(),
    [isEmptyFiles, openHandleAdd, renderFiles, t]
  )

  const renderButton = useMemo(
    () => !isEmptyFiles && (
      <Button
        onPress={ openHandleAdd }
        style={ { flex: 1 } }
        color={ colors.text }
        backgroundColor={ colors.secondary }
        title={ t('add images') }
      />
    ),
    [isEmptyFiles, openHandleAdd, t]
  )

  useFocusEffect(useCallback(
    () => {
      setOptions({
        headerTitle: () => (
          <ScreenHeaderTitleContainer>
            <I18n as={ HeaderTitle }>
              machinery register
            </I18n>
          </ScreenHeaderTitleContainer>
        )
      })
    },
    [setOptions]
  ))

  return (
    <AuthenticatedLayout>
      {loading ? <Loader width={ 100 } /> : (
        <Container>
          <Scroll padding={ 0 }>
            <Item
              style={ {
                flex: 1,
                paddingTop: 20,
                paddingBottom: 0
              } }
            >
              <MachineryForm
                ref={ machineryFormRef }
                onSuccess={ onSuccess }
                onSubmit={ submitForms }
                defaultValues={ defaultValues }
                withoutSubmitButton
              />

              <I18n as={ Subtitle } params={ { howMany: 2 } }>
                image
              </I18n>

              <Gallery isCenter={ isEmptyFiles }>
                {renderDOMFiles}
              </Gallery>

              {renderButton}

            </Item>
          </Scroll>

          <Item>
            <ButtonsContainer style={ { paddingTop: 10 } }>
              <Button
                onPress={ handleClose }
                style={ { flex: 1 } }
                variant="outlined"
                title={ t('cancel') }
              />

              <View style={ { width: '10%' } } />

              <Button
                onPress={ submit }
                style={ { flex: 1 } }
                title={ t('save') }
              />
            </ButtonsContainer>
          </Item>
        </Container>
      )}
    </AuthenticatedLayout>
  )
}

export default MachineryRegisterScreen
