import React from 'react'

import { useNavigation } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'

import { useScreenOptions, withScreenOptions } from '@smartcoop/mobile-containers/hooks/useScreenOptions'

import MachineryDetailsScreen from './MachineryDetailsScreen/MachineryDetailsScreen'
import MachineryRegisterScreen from './MachineryRegisterScreen/MachineryRegisterScreen'

const Stack = createStackNavigator()

const MachineryScreenRouter = () => {
  const navigation = useNavigation()
  const { options } = useScreenOptions(navigation)

  return (
    <Stack.Navigator screenOptions={ options } mode="modal">
      <Stack.Screen
        name="Register"
        component={ MachineryRegisterScreen }
      />

      <Stack.Screen
        name="Details"
        component={ MachineryDetailsScreen }
      />
    </Stack.Navigator>
  )
}


export default withScreenOptions(MachineryScreenRouter)
