import { Title as RNPTitle, Paragraph } from 'react-native-paper'

import styled from 'styled-components/native'

import { colors } from '@smartcoop/styles'

export const ButtonsContainer = styled.View`
  display: flex;
  flex-direction: row;
`

export const PaddingContainer = styled.View`
  padding: 15px 15px 0;
`

export const HorizontalContainer = styled.View`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
`

export const InfoContainer = styled.View`
  display: flex;
  flex-wrap: wrap;
  flex-direction: row;
  justify-content: flex-start;
  align-content: flex-start;
  align-self: center;
`

export const Title = styled(RNPTitle)`
  font-family: 'Montserrat';
  font-weight: bold;
  margin: 0;
`

export const Subtitle = styled(Title)`
  font-size: 20px;
  margin-bottom: 15px;
`

export const Text = styled(Paragraph)`
  font-size: 18px;
`
export const TextBox = styled.View`
  /* flex: 1 0 auto; */
  padding: 0 20px;
  margin-bottom: 10px;
  width: 50%;
`

export const TextInfo = styled(Text)`
  font-weight: bold;
`

export const DistanceText = styled(Title)`
  margin-top: 10px;
  color: ${ colors.green };
`
