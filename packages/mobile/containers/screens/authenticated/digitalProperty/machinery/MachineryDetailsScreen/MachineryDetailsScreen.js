import React, { useMemo, useCallback, useState } from 'react'
import { useSelector, useDispatch } from 'react-redux'

import { useNavigation, useFocusEffect } from '@react-navigation/native'

import isEmpty from 'lodash/isEmpty'

import { useDialog } from '@smartcoop/dialog'
import I18n, { useT } from '@smartcoop/i18n'
import { trash, pencil } from '@smartcoop/icons'
import Button from '@smartcoop/mobile-components/Button'
import Icon from '@smartcoop/mobile-components/Icon'
import Loader from '@smartcoop/mobile-components/Loader'
import ConfirmModal from '@smartcoop/mobile-components/Modal/ConfirmModal'
import { useScreenOptions } from '@smartcoop/mobile-containers/hooks/useScreenOptions'
import AuthenticatedLayout from '@smartcoop/mobile-containers/layouts/AuthenticatedLayout'
import { Scroll, ScreenHeaderTitleContainer, HeaderTitle } from '@smartcoop/mobile-containers/layouts/AuthenticatedLayout/theme'
import { useSnackbar } from '@smartcoop/snackbar'
import { MachineActions } from '@smartcoop/stores/machine'
import { selectCurrentMachine } from '@smartcoop/stores/machine/selectorMachine'
import colors from '@smartcoop/styles/colors'

import MachineDetailsImageGallery from './MachineDetailsImageGallery/MachineDetailsImageGallery'
import {
  Title,
  ButtonsContainer,
  HorizontalContainer,
  InfoContainer,
  DistanceText,
  Subtitle,
  Text,
  TextInfo,
  TextBox,
  PaddingContainer
} from './styles'

const MachineryDetailsScreen = () => {
  const machine = useSelector(selectCurrentMachine)
  const navigation = useNavigation()
  const { createDialog } = useDialog()
  const [loading, setLoading] = useState(false)
  const dispatch = useCallback(useDispatch(), [])
  const snackbar = useSnackbar()
  const t = useT()
  const { setOptions } = useScreenOptions()

  const machineData = useMemo(
    () => ({
      machineType: machine?.machineType?.description ?? null,
      machineBrand: machine?.machineBrand?.description ?? null,
      description: machine?.description ?? null,
      model: machine?.model ?? null,
      contactInformation: machine?.contactInformation ?? null,
      availableHours: machine?.availableHours ?? null,
      year: machine?.year ?? null,
      isUserMachine: machine?.isUserMachine ?? false
    }), [machine]
  )

  const images = useMemo(() => machine?.machineFiles ?? [], [machine])

  const distance = useMemo(
    () => (machine.property?.distance ? (`${ machine.property.distance  } km`) : ''),
    [machine]
  )

  const editMachine = useCallback(
    () => {
      navigation.navigate('Machinery',
        { screen: 'Register', params: { machine: { ...machine } } })
    },
    [machine, navigation]
  )

  const handleDeleteMachine = useCallback(
    (id) => {
      createDialog({
        id: 'confirm-delete',
        Component: ConfirmModal,
        props: {
          onConfirm: () => {
            setLoading(true)
            dispatch(MachineActions.deleteMachine(id,
              () => {
                snackbar.success(
                  t('your {this} was deleted', {
                    howMany: 1,
                    gender: 'female',
                    this: t('machine', { howMany: 1 })
                  })
                )
                navigation.goBack()
              },
              () => {
                setLoading(false)
              }
            ))
          },
          message: t('are you sure you want to delete the machine?')
        }
      })
    },
    [createDialog, dispatch, navigation, snackbar, t]
  )

  const deleteMachine = useCallback(
    () => {
      handleDeleteMachine(machine.id)
    },
    [handleDeleteMachine, machine]
  )


  useFocusEffect(useCallback(
    () => {
      setOptions({
        headerTitle: () => (
          <ScreenHeaderTitleContainer>
            <I18n as={ HeaderTitle }>
              machine details
            </I18n>
          </ScreenHeaderTitleContainer>
        )
      })
    },
    [setOptions]
  ))

  return (
    <AuthenticatedLayout>
      <Scroll>
        {loading
          ? <Loader />
          : (
            <>
              <PaddingContainer>
                <HorizontalContainer>
                  <Title>
                    {machineData.description}
                  </Title>
                  {machineData.isUserMachine && (
                    <ButtonsContainer>
                      <Button
                        style={ { width: 100, marginRight: 15 } }
                        textStyle={ { paddingTop: 5, paddingBottom: 5 } }
                        variant='outlined'
                        color={ colors.black }
                        onPress={ editMachine }
                        icon={ <Icon size={ 16 } icon={ pencil } color={ colors.black } /> }
                        title={ <I18n>edit</I18n> }
                        backgroundColor={ colors.white }
                      />
                      <Button
                        style={ { width: 36 } }
                        onPress={ deleteMachine }
                        variant='outlined'
                        textStyle={ { paddingTop: 5, paddingBottom: 5 } }

                        icon={ <Icon size={ 16 } icon={ trash } color={ colors.red } /> }
                        backgroundColor={ colors.white }
                      />
                    </ButtonsContainer>
                  )}
                </HorizontalContainer>

                <DistanceText>
                  {distance}
                </DistanceText>
              </PaddingContainer>


              {!isEmpty(images) && (
                <MachineDetailsImageGallery images={ images } />
              )}

              <PaddingContainer>

                <Subtitle>
                  <I18n>informations</I18n>
                </Subtitle>

                <InfoContainer>
                  <TextBox>
                    <TextInfo>
                      <I18n>type</I18n>
                    </TextInfo>
                    <Text>
                      {machine.machineType.description}
                    </Text>
                  </TextBox>

                  <TextBox>
                    <TextInfo>
                      <I18n>model</I18n>
                    </TextInfo>
                    <Text>
                      {machine.model}
                    </Text>
                  </TextBox>
                </InfoContainer>

                <InfoContainer>
                  <TextBox>
                    <TextInfo>
                      <I18n>brand</I18n>
                    </TextInfo>
                    <Text>
                      {machine.machineBrand.description}
                    </Text>
                  </TextBox>

                  <TextBox>
                    <TextInfo>
                      <I18n params={ { howMany: 1 } }>
                        year
                      </I18n>
                    </TextInfo>
                    <Text>
                      {machine.year}
                    </Text>
                  </TextBox>
                </InfoContainer>

                <InfoContainer>
                  <TextBox>
                    <TextInfo>
                      <I18n>hours</I18n>
                    </TextInfo>
                    <Text>
                      {machine.availableHours}
                    </Text>
                  </TextBox>

                  <TextBox>
                    <TextInfo>
                      <I18n>contact</I18n>
                    </TextInfo>
                    <Text>
                      {machine.contactInformation}
                    </Text>
                  </TextBox>
                </InfoContainer>
              </PaddingContainer>
            </>
          )}
      </Scroll>
    </AuthenticatedLayout>
  )
}

export default MachineryDetailsScreen
