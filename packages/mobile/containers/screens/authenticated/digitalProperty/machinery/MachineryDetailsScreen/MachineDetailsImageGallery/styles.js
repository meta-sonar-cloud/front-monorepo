import styled from 'styled-components'

import colors from '@smartcoop/styles/colors'

export const CurrentImage = styled.Image`
  width: 100%;
  height: 250px;
  resize-mode: cover;
  border-radius: 5px;
`

export const Container = styled.View`
  padding: 5px;
`

export const ImagesContainer = styled.View`
  display: flex;
  justify-content: center;
  flex-direction: column;
`

export const ThumbsContainer = styled.ScrollView.attrs({
  horizontal: true
})`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
`

export const CurrentImageContainer = styled.View`
  display: flex;
  justify-content: center;
  align-items: center;
  align-content: center;
`

export const Thumb = styled.View`
  border-radius: 8px;
  background: ${ colors.lightGrey };
  width: 88px;
  height: 88px;
  margin: 5px;
  border: 2px solid transparent;
  border-color: ${ props => props.isActive ? colors.yellow : 'transparent' };
`
