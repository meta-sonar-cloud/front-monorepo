import React, { useState, useMemo } from 'react'
import { TouchableOpacity } from 'react-native'

import PropTypes from 'prop-types'

import { isEmpty, map } from 'lodash'

import ThumbnailImage from '@smartcoop/mobile-components/ThumbnailImage'

import {
  Container,
  CurrentImageContainer,
  Thumb,
  ImagesContainer,
  CurrentImage,
  ThumbsContainer
} from './styles'

const MachineDetailsImageGallery = ({ images }) => {
  const [activeIndex, setActiveIndex] = useState(0)
  const activeImage = useMemo(() => !isEmpty(images) && !isEmpty(images[activeIndex]?.fileUrl) ? images[activeIndex]?.fileUrl : null, [activeIndex, images])
  return (
    <Container>
      <ImagesContainer>
        {!isEmpty(activeImage) && (
          <CurrentImageContainer>
            <CurrentImage source={ { uri: activeImage } } />
          </CurrentImageContainer>)
        }
        <ThumbsContainer>
          {!isEmpty(images) && map(images, (image, index) => (
            <TouchableOpacity
              onPress={ () => setActiveIndex(index) }
              key={ image.id }
            >
              <Thumb
                isActive={ activeIndex === index }
              >
                <ThumbnailImage
                  src={ images[index].fileUrl }
                  size={ 85 }
                />
              </Thumb>
            </TouchableOpacity>
          ))}
        </ThumbsContainer>
      </ImagesContainer>
    </Container>
  )
}

MachineDetailsImageGallery.propTypes = {
  images: PropTypes.array
}

MachineDetailsImageGallery.defaultProps = {
  images: []
}


export default MachineDetailsImageGallery
