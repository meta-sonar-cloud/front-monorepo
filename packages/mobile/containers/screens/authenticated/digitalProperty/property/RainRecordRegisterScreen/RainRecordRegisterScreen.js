import React, { useRef, useCallback } from 'react'

import { useRoute , useFocusEffect } from '@react-navigation/native'


import I18n, { useT } from '@smartcoop/i18n'
import RainRecordRegisterForm from '@smartcoop/mobile-containers/forms/digitalProperty/rainRecord/RainRecordRegisterForm'
import { useScreenOptions } from '@smartcoop/mobile-containers/hooks/useScreenOptions'
import { Container, Item , ScreenHeaderTitleContainer, HeaderTitle } from '@smartcoop/mobile-containers/layouts/AuthenticatedLayout/theme'
import { useSnackbar } from '@smartcoop/snackbar'


const RainRecordRegisterScreen = () => {
  const rainRecordFormRef = useRef(null)
  const routes = useRoute()
  const t = useT()
  const snackbar = useSnackbar()
  const { setOptions } = useScreenOptions()

  const { dataPrecipitation } = routes.params

  const snackbarSuccess = useCallback(
    (precipitationId) => (
      precipitationId ? (
        snackbar.success(
          t('your {this} was edited', {
            howMany: 1,
            gender: 'male',
            this: t('rain record')
          })
        )
      ) : (
        snackbar.success(
          t('your {this} was registered', {
            howMany: 1,
            gender: 'male',
            this: t('rain record')
          })
        )
      )
    ),
    [snackbar, t]
  )

  useFocusEffect(useCallback(
    () => {
      setOptions({
        headerTitle: () => (
          <ScreenHeaderTitleContainer>
            <I18n as={ HeaderTitle }>
              register rain
            </I18n>
          </ScreenHeaderTitleContainer>
        )
      })
    },
    [setOptions]
  ))

  return (
    <Container>
      <Item
        style={ {
          flex: 1,
          paddingTop: 20,
          paddingBottom: 0
        } }
      >
        <RainRecordRegisterForm
          ref={ rainRecordFormRef }
          onSuccess={ snackbarSuccess }
          withoutSubmitButton
          rainRecordData={ dataPrecipitation }
        />
      </Item>

    </Container>
  )}

export default RainRecordRegisterScreen
