import styled from 'styled-components/native'

import { Item } from '@smartcoop/mobile-containers/layouts/AuthenticatedLayout/theme'
import colors from '@smartcoop/styles/colors'

export const IconContainer = styled.View`
  margin-bottom: 15px;
`

export const Container = styled.View`
  flex: 2;
`

export const OverlayButtonContainer = styled.View`
  position: absolute;
  bottom: 25px;
  left: 0;
  right: 0;
  padding-left: 25%;
  padding-right: 25%;
`

export const ItemHeader = styled(Item).attrs({
  elevation: 0
})`
  shadow-color: #000;
  shadow-offset: 0px 5px;
  shadow-opacity: 0.3;
  shadow-radius: 3px;
  background-color: ${ colors.white };
  /* padding-top: 25px;
  padding-bottom: 5px; */
  align-items: center;
`

export const ItemFooter = styled(Item).attrs({
  elevation: 0
})`
  shadow-color: #000;
  shadow-offset: 0px -2px;
  shadow-opacity: 0.2;
  shadow-radius: 3px;
  padding-top: 15px;
  background-color: ${ colors.white };
`
