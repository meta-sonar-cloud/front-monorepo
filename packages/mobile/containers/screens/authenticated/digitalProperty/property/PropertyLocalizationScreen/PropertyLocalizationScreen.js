import React, { useState, useCallback, useMemo, useEffect } from 'react'
import { Platform } from 'react-native'
import { useDispatch, useSelector } from 'react-redux'

import Geolocation from '@react-native-community/geolocation'
import { useNavigation, useFocusEffect } from '@react-navigation/native'

import isEmpty from 'lodash/isEmpty'

import I18n, { useT } from '@smartcoop/i18n'
import { marker as markerIcon , markerClosed as markerClosedIcon } from '@smartcoop/icons'
import Button from '@smartcoop/mobile-components/Button'
import Maps from '@smartcoop/mobile-components/Maps'
import PinMarker from '@smartcoop/mobile-components/Maps/markers/PinMarker'
import Stepper from '@smartcoop/mobile-components/Stepper'
import { useScreenOptions } from '@smartcoop/mobile-containers/hooks/useScreenOptions'
import { Container, Item, ButtonsContainer, ScreenHeaderTitleContainer, HeaderTitle } from '@smartcoop/mobile-containers/layouts/AuthenticatedLayout/theme'
import { PropertyActions } from '@smartcoop/stores/property'
import { selectOfflineProperty } from '@smartcoop/stores/property/selectorProperty'

import { ItemHeader, ItemFooter } from './styles'

const PropertyLocalizationScreen = () => {
  const dispatch = useCallback(useDispatch(), [])

  const navigation = useNavigation()
  const t = useT()
  const { setOptions } = useScreenOptions()

  const property = useSelector(selectOfflineProperty)

  const [markerCoordinate, setMarkerCoordinate] = useState(property.data.geolocalization.latitude ? property.data.geolocalization : undefined )

  const [mounted, setMounted] = useState(false)

  const [selectingGeo, setSelectingGeo] = useState(!isEmpty(property.data.geolocalization))

  const disabled = useMemo(() => isEmpty(markerCoordinate), [markerCoordinate])

  const handlerMarkerMoved = useCallback((event) => {
    if(selectingGeo) {
      setMarkerCoordinate({
        latitude: event.nativeEvent.coordinate.latitude,
        longitude: event.nativeEvent.coordinate.longitude
      })
    }
  }, [selectingGeo])

  const savePropertyLocation = useCallback(
    () => {
      if (!disabled) {
        dispatch(PropertyActions.updateOfflineProperty({
          data: {
            geolocalization: markerCoordinate
          }
        }))
        setSelectingGeo(false)
      }
    },
    [disabled, dispatch, markerCoordinate]
  )

  const onConfirm = useCallback(() =>{
    savePropertyLocation()
    navigation.navigate('Activity')
  }, [navigation, savePropertyLocation])

  useEffect(() => {
    if (!mounted && !property.data.geolocalization.latitude) {
      Geolocation.getCurrentPosition(
        location => {
          setMarkerCoordinate({
            latitude: location.coords.latitude,
            longitude: location.coords.longitude
          })
        },
        // eslint-disable-next-line no-console
        error => console.error(error),
        {
          enableHighAccuracy: true
        }
      )
      setMounted(true)
    }
  }, [markerCoordinate, mounted, property.data, property.data.geolocalization.latitude])

  useFocusEffect(useCallback(
    () => {
      setOptions({
        headerTitle: () => (
          <ScreenHeaderTitleContainer>
            <I18n
              as={ HeaderTitle }
              style={ { fontSize: Platform.OS === 'android' ? 16 : 14 } }
            >
              mark your property on the map
            </I18n>
          </ScreenHeaderTitleContainer>
        )
      })
    },
    [setOptions]
  ))

  return (
    <Container>

      <ItemHeader>
        <Stepper
          style={ { width: '100%' } }
          step={ 2 }
          steps={ 4 }
        />
      </ItemHeader>


      <Item
        style={ {
          flex: 1,
          paddingTop: 0,
          paddingBottom: 0
        } }
      >
        <Maps
          autoUserPosition={ !property }
          region={ markerCoordinate }
          mapType="hybrid"
          onPress={ handlerMarkerMoved }
        >
          <PinMarker
            coordinate={ markerCoordinate }
            draggable
            onDragEnd={ handlerMarkerMoved }
            customIcon={ selectingGeo ? markerIcon : markerClosedIcon }
          />
        </Maps>
      </Item>

      <ItemFooter>
        <ButtonsContainer>
          <Button
            title={  t('skip') }
            onPress={ () => navigation.navigate('Activity') }
            style={ { width: '48%' } }
            variant="outlined"

          />
          <Button
            title={ t('confirm') }
            onPress={ () => onConfirm() }
            style={ { width: '48%' } }
          />
        </ButtonsContainer>
      </ItemFooter>
    </Container>

  )
}

export default PropertyLocalizationScreen
