import React, { useCallback, useRef } from 'react'
import { KeyboardAvoidingView, Platform, TouchableWithoutFeedback, Keyboard } from 'react-native'
import { useDispatch } from 'react-redux'

import { useNavigation, CommonActions, useFocusEffect } from '@react-navigation/native'

import { useDialog } from '@smartcoop/dialog'
import I18n, { useT } from '@smartcoop/i18n'
import Button from '@smartcoop/mobile-components/Button'
import Stepper from '@smartcoop/mobile-components/Stepper'
import PropertyActivityDetailsForm from '@smartcoop/mobile-containers/forms/digitalProperty/property/PropertyActivityDetailsForm'
import { useScreenOptions } from '@smartcoop/mobile-containers/hooks/useScreenOptions'
import { Scroll, Container, Item, ButtonsContainer , ScreenHeaderTitleContainer, HeaderTitle } from '@smartcoop/mobile-containers/layouts/AuthenticatedLayout/theme'
import SuccessModal from '@smartcoop/mobile-containers/modals/SuccessModal'
import { PropertyActions } from '@smartcoop/stores/property'

// import { IconContainer } from './styles'

const PropertyActivityDetailsScreen = () => {
  const formRef = useRef(null)

  const t = useT()
  const navigation = useNavigation()
  const dispatch = useCallback(useDispatch(), [])
  const { createDialog } = useDialog()
  const { setOptions } = useScreenOptions()


  const handleSubmit = useCallback(
    () => {
      dispatch(PropertyActions.saveOfflineProperty())
      navigation.navigate('DigitalProperty')
      createDialog({
        id: 'create-property-success',
        Component: SuccessModal,
        props: {
          message: t('your property was registered'),
          buttonText: t('go to start'),
          underlinedText: t('register another property'),
          onUnderlinedClick: () => navigation.navigate('Property', { screen: 'Identification' }),
          reloadDataAction: true
        }
      })
    },
    [createDialog, dispatch, navigation, t]
  )


  useFocusEffect(useCallback(
    () => {
      setOptions({
        headerTitle: () => (
          <ScreenHeaderTitleContainer>
            <I18n as={ HeaderTitle }>
              property activities
            </I18n>
          </ScreenHeaderTitleContainer>
        )
      })
    },
    [setOptions]
  ))

  return (
    <KeyboardAvoidingView
      behavior={ Platform.OS === 'ios' ? 'padding' : 'height' }
      style={ { flex: 1 } }
    >
      <TouchableWithoutFeedback onPress={ Keyboard.dismiss }>
        <Container>
          <Item style={ { paddingBottom: 15 } }>
            <Stepper
              style={ { width: '100%' } }
              step={ 4 }
              steps={ 4 }
            />
          </Item>

          <Item
            style={ {
              flex: 1,
              paddingTop: 0,
              paddingBottom: 0
            } }
          >
            <Scroll padding={ 0 }>
              <PropertyActivityDetailsForm
                ref={ formRef }
                onSubmit={ handleSubmit }
                withoutSubmitButton
              />
            </Scroll>
          </Item>

          <Item>
            <ButtonsContainer>
              <Button
                title={ t('go back') }
                onPress={ () => navigation.dispatch(CommonActions.goBack()) }
                style={ { width: '48%' } }
                variant="outlined"
              />
              <Button
                title={ t('next') }
                onPress={ () => formRef.current.submit() }
                style={ { width: '48%' } }
              />
            </ButtonsContainer>
          </Item>
        </Container>
      </TouchableWithoutFeedback>
    </KeyboardAvoidingView>
  )
}

export default PropertyActivityDetailsScreen
