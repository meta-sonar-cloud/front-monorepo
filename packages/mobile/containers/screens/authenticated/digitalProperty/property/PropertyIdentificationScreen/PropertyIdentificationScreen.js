import React, { useEffect, useRef, useCallback, useMemo } from 'react'
import { useSelector, useDispatch } from 'react-redux'

import {
  useRoute,
  useNavigation,
  CommonActions,
  useFocusEffect
} from '@react-navigation/native'

import I18n, { useT } from '@smartcoop/i18n'
import Button from '@smartcoop/mobile-components/Button'
import Stepper from '@smartcoop/mobile-components/Stepper'
import PropertyIdentificationForm from '@smartcoop/mobile-containers/forms/digitalProperty/property/PropertyIdentificationForm'
import { useScreenOptions } from '@smartcoop/mobile-containers/hooks/useScreenOptions'
import {
  Scroll,
  Container,
  Item,
  ButtonsContainer,
  HeaderTitle,
  ScreenHeaderTitleContainer
} from '@smartcoop/mobile-containers/layouts/AuthenticatedLayout/theme'
import { ModuleActions } from '@smartcoop/stores/module'
import { PropertyActions } from '@smartcoop/stores/property'
import { selectCurrentProperty } from '@smartcoop/stores/property/selectorProperty'

const PropertyIdentificationScreen = () => {
  const formRef = useRef(null)

  const t = useT()
  const navigation = useNavigation()
  const dispatch = useCallback(useDispatch(), [])
  const route = useRoute()
  const { setOptions } = useScreenOptions()

  const currentProperty = useSelector(selectCurrentProperty)

  const { isEditing } = route?.params ?? false

  const handleSubmit = useCallback(() => {
    navigation.navigate('Localization')
  }, [navigation])

  const editingData = useMemo(() => {
    if (isEditing) {
      return currentProperty
    }
    return {}
  }, [currentProperty, isEditing])

  useEffect(() => {
    if (isEditing) {
      dispatch(PropertyActions.updateOfflineProperty({ data: currentProperty }))
    }
  }, [currentProperty, dispatch, isEditing])

  useFocusEffect(
    useCallback(() => {
      setOptions({
        headerTitle: () => (
          <ScreenHeaderTitleContainer>
            <I18n as={ HeaderTitle }>property registration</I18n>
          </ScreenHeaderTitleContainer>
        )
      })
    }, [setOptions])
  )

  return (
    <Container>
      <Item style={ { paddingBottom: 15 } }>
        <Stepper style={ { width: '100%' } } step={ 1 } steps={ 4 } />
      </Item>

      <Scroll padding={ 0 }>
        <Item
          style={ {
            flex: 1,
            paddingTop: 0,
            paddingBottom: 0
          } }
        >
          <PropertyIdentificationForm
            ref={ formRef }
            currentProperty={ editingData }
            isEditing={ isEditing }
            onSubmit={ handleSubmit }
            withoutSubmitButton
          />
        </Item>
      </Scroll>

      <Item>
        <ButtonsContainer>
          <Button
            title={ t('cancel') }
            onPress={ () => {
              navigation.dispatch(CommonActions.goBack())
              dispatch(ModuleActions.initDigitalPropertyModule())
            } }
            style={ { width: '48%' } }
            variant="outlined"
          />
          <Button
            title={ t('next') }
            onPress={ () => formRef.current.submit() }
            style={ { width: '48%' } }
          />
        </ButtonsContainer>
      </Item>
    </Container>
  )
}

export default PropertyIdentificationScreen
