import React, { useCallback } from 'react'

import { useNavigation, useFocusEffect } from '@react-navigation/native'

import I18n, { useT } from '@smartcoop/i18n'
import Button from '@smartcoop/mobile-components/Button'
import { useScreenOptions } from '@smartcoop/mobile-containers/hooks/useScreenOptions'
import AuthenticatedLayout from '@smartcoop/mobile-containers/layouts/AuthenticatedLayout'
import { Scroll, ScreenHeaderTitleContainer, HeaderTitle } from '@smartcoop/mobile-containers/layouts/AuthenticatedLayout/theme'
import colors from '@smartcoop/styles/colors'

const PropertyListScreen = () => {
  const navigation = useNavigation()
  const t = useT()
  const { setOptions } = useScreenOptions()

  const handleRegisterPropertyClick = useCallback(
    () => {
      navigation.navigate('Identification')
    },
    [navigation]
  )

  useFocusEffect(useCallback(
    () => {
      setOptions({
        headerTitle: () => (
          <ScreenHeaderTitleContainer>
            <I18n
              as={ HeaderTitle }
              params={ { howMany: 2 } }
            >
              property
            </I18n>
          </ScreenHeaderTitleContainer>
        )
      })
    },
    [setOptions]
  ))

  return (
    <AuthenticatedLayout>
      <Scroll>
        <Button
          onPress={ handleRegisterPropertyClick }
          color={ colors.text }
          backgroundColor={ colors.secondary }
          title={ t('register property') }
        />
      </Scroll>
    </AuthenticatedLayout>
  )
}

export default PropertyListScreen
