import styled from 'styled-components/native'

import colors from '@smartcoop/styles/colors'

export const ModalFieldContainer = styled.View`
  padding: 15px;
`

export const InputView = styled.View`
  margin-left: 10px;
  margin-right: 10px;
`

export const ButtonContainer = styled.View`
  position: absolute;
  bottom: 0;
  z-index: 2;
  width: 50%;
  left: 25%;
  padding-bottom: 20px;
`

export const SafeAreaViewList = styled.SafeAreaView`
  flex: 100;
  padding-bottom: 70px;
  width: 100%;
`

export const Container = styled.View`
  padding-bottom: 25px;
`

export const ContainerAndroid = styled.View`
  padding: 25px;
  display: flex;
  align-items: center;
  justify-content: center;
  height: 100%;
`

export const Text = styled.Text`
margin-top: 25;
  font-size: 25;
  color: ${ colors.text };
  text-align: center;
`

export const LinkText = styled.Text`
  color: ${ colors.secondary };
  font-size: 25;
  font-weight: bold;
`

export const Circle = styled.View`
  background-color: ${ colors.secondary };
  padding: 25px;
  border-radius: 100px;
`
