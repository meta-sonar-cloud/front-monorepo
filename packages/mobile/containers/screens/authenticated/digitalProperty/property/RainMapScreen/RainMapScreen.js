import React, { useCallback, useRef } from 'react'
import { View, Dimensions, Platform, Linking } from 'react-native'
import { useSelector, useDispatch } from 'react-redux'

import { useFocusEffect } from '@react-navigation/native'
import moment from 'moment'

import filterRainMapSchema from '@smartcoop/forms/schemas/property/fields/filterRainMap.schema'
import I18n, { useT } from '@smartcoop/i18n'
import { rain } from '@smartcoop/icons'
import Button from '@smartcoop/mobile-components/Button'
import Form from '@smartcoop/mobile-components/Form'
import Icon from '@smartcoop/mobile-components/Icon'
import InputDate from '@smartcoop/mobile-components/InputDate'
import RainMapFragment from '@smartcoop/mobile-containers/fragments/RainMapFragment'
import RainScaleFragment from '@smartcoop/mobile-containers/fragments/RainScaleFragment/RainScaleFragment'
import { useScreenOptions } from '@smartcoop/mobile-containers/hooks/useScreenOptions'
import AuthenticatedLayout from '@smartcoop/mobile-containers/layouts/AuthenticatedLayout'
import { ScreenHeaderTitleContainer, HeaderTitle } from '@smartcoop/mobile-containers/layouts/AuthenticatedLayout/theme'
import { useSnackbar } from '@smartcoop/snackbar'
import { selectCurrentProperty } from '@smartcoop/stores/property/selectorProperty'
import { WeatherForecastActions } from '@smartcoop/stores/weatherForecast'
import colors from '@smartcoop/styles/colors'
import { momentBackDateFormat } from '@smartcoop/utils/dates'

import { ModalFieldContainer, Container, ContainerAndroid, Text, LinkText, Circle } from './styles'

const RainMapScreen = () => {
  const formRef = useRef(null)
  const dispatch = useDispatch()
  const property = useSelector(selectCurrentProperty)
  const snackbar = useSnackbar()
  const t = useT()
  const { setOptions } = useScreenOptions()

  const handleSubmit = useCallback((data?) => {
    if (moment(data?.finalDate).diff(moment(data?.initialDate), 'days') < 0) {
      return snackbar.warning(t('the start date must be less than end'))
    }

    if (
      moment(data?.finalDate).diff(moment(), 'days') > 0 ||
      moment(data?.initialDate).diff(moment(), 'days') > 0
    ) {
      return snackbar.warning(t('filtered dates must be less than or equal to current'))
    }

    return dispatch(WeatherForecastActions.searchAggregatedRain(
      moment(data?.initialDate).format(momentBackDateFormat),
      moment(data?.finalDate).format(momentBackDateFormat)
    ))
  }, [dispatch, snackbar, t])

  useFocusEffect(useCallback(
    () => {
      setOptions({
        headerTitle: () => (
          <ScreenHeaderTitleContainer>
            <I18n as={ HeaderTitle }>
              rain map
            </I18n>
          </ScreenHeaderTitleContainer>
        )
      })
    },
    [setOptions]
  ))

  return (
    <AuthenticatedLayout>
      { Platform.OS === 'android' ? (
        <ContainerAndroid>
          <Circle>
            <Icon icon={ rain } color={ colors.blackLight } size={ 90 } />
          </Circle>
          <Text>
            <I18n>rain map is not done</I18n>
          </Text>

          <LinkText onPress={ () => Linking.openURL('https://app.smart.coop.br') }>https://app.smart.coop.br</LinkText>
        </ContainerAndroid>
      ) : (
        <ModalFieldContainer>
          <Form
            ref={ formRef }
            schemaConstructor={ filterRainMapSchema }
            onSubmit={ handleSubmit }
          >
            <Container>
              <InputDate
                name="initialDate"
                label={ t('initial date') }
                defaultValue={ moment().subtract(7, 'days').format(momentBackDateFormat) }
                fullWidth
              />
              <InputDate
                name="finalDate"
                label={ t('final date') }
                defaultValue={ moment().format(momentBackDateFormat) }
                fullWidth
              />

              <Button
                color={ colors.text }
                backgroundColor={ colors.secondary }
                title={ t('search') }
                onPress={ () => formRef.current.submit() }
              />
            </Container>
          </Form>
          <View style={ {
            width: '100%',
            height: Dimensions.get('screen').height - 400
          } }
          >
            <RainMapFragment property={ property } />
          </View>
          <RainScaleFragment/>
        </ModalFieldContainer>
      ) }
    </AuthenticatedLayout>
  )
}

// eslint-disable-next-line react/prop-types
export default RainMapScreen
