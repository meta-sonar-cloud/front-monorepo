import React, { useRef, useCallback } from 'react'

import { useNavigation, CommonActions, useFocusEffect } from '@react-navigation/native'

import I18n, { useT } from '@smartcoop/i18n'
import Button from '@smartcoop/mobile-components/Button'
import Stepper from '@smartcoop/mobile-components/Stepper'
import PropertyActivityForm from '@smartcoop/mobile-containers/forms/digitalProperty/property/PropertyActivityForm'
import { useScreenOptions } from '@smartcoop/mobile-containers/hooks/useScreenOptions'
import { Scroll, Container, Item, ButtonsContainer, ScreenHeaderTitleContainer, HeaderTitle } from '@smartcoop/mobile-containers/layouts/AuthenticatedLayout/theme'

const PropertyActivityScreen = () => {
  const formRef = useRef(null)

  const t = useT()
  const navigation = useNavigation()
  const { setOptions } = useScreenOptions()

  const handleSubmit = useCallback(
    () => {
      navigation.navigate('ActivityDetails')
    },
    [navigation]
  )

  useFocusEffect(useCallback(
    () => {
      setOptions({
        headerTitle: () => (
          <ScreenHeaderTitleContainer>
            <I18n as={ HeaderTitle }>
              property activities
            </I18n>
          </ScreenHeaderTitleContainer>
        )
      })
    },
    [setOptions]
  ))

  return (
    <Container>
      <Item>
        <Stepper
          style={ { width: '100%' } }
          step={ 3 }
          steps={ 4 }
        />
      </Item>

      <Item
        style={ {
          flex: 1,
          paddingTop: 0,
          paddingBottom: 0
        } }
      >
        <Scroll padding={ 0 }>
          <PropertyActivityForm
            ref={ formRef }
            onSubmit={ handleSubmit }
            withoutSubmitButton
          />
        </Scroll>
      </Item>

      <Item>
        <ButtonsContainer>
          <Button
            title={ t('go back') }
            onPress={ () => navigation.dispatch(CommonActions.goBack()) }
            style={ { width: '48%' } }
            variant="outlined"
          />
          <Button
            title={ t('next') }
            onPress={ () => formRef.current.submit() }
            style={ { width: '48%' } }
          />
        </ButtonsContainer>
      </Item>
    </Container>

  )
}

export default PropertyActivityScreen
