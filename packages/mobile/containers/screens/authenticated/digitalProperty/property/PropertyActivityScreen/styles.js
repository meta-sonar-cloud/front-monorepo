import styled from 'styled-components/native'

export const IconContainer = styled.View`
  margin-bottom: 15px;
`

export const Container = styled.View`
  flex: 2;
`
