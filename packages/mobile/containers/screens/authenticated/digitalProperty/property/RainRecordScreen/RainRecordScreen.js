import React, { useCallback, useState } from 'react'
import { View } from 'react-native'
import { useSelector } from 'react-redux'

import { useNavigation, useFocusEffect } from '@react-navigation/native'

import isEmpty from 'lodash/isEmpty'

import { useDialog } from '@smartcoop/dialog'
import I18n, { useT } from '@smartcoop/i18n'
import { filter } from '@smartcoop/icons'
import Button from '@smartcoop/mobile-components/Button'
import Icon from '@smartcoop/mobile-components/Icon'
import RainRecordList from '@smartcoop/mobile-containers/fragments/RainRecordList'
import { useScreenOptions } from '@smartcoop/mobile-containers/hooks/useScreenOptions'
import AuthenticatedLayout from '@smartcoop/mobile-containers/layouts/AuthenticatedLayout'
import { ScreenHeaderTitleContainer, HeaderTitle } from '@smartcoop/mobile-containers/layouts/AuthenticatedLayout/theme'
import FilterRainRecordModal from '@smartcoop/mobile-containers/modals/FilterRainRecordModal'
import { selectUserCanWrite } from '@smartcoop/stores/user/selectorUser'
import colors from '@smartcoop/styles/colors'

import { ModalFieldContainer, SafeAreaViewList, ButtonContainer, InputView } from './styles'

const RainRecordScreen = () => {
  const { createDialog } = useDialog()
  const navigation = useNavigation()
  const [filters, setFilters] = useState({})
  const t = useT()
  const { setOptions } = useScreenOptions()

  const userWrite = useSelector(selectUserCanWrite)

  const handleFilter = useCallback(
    (values) => setFilters(values),
    []
  )

  const handleFilterFields = useCallback(
    () => {
      createDialog({
        id: 'filter-rain-record',
        Component: FilterRainRecordModal,
        props: {
          onSubmit: handleFilter,
          filters
        }

      })
    },
    [createDialog, filters, handleFilter]
  )

  const navigateRainRecordRegister = useCallback(
    () => {
      navigation.navigate('Property', { screen: 'RainRecordRegister', params: {} })
    },
    [navigation]
  )

  useFocusEffect(useCallback(
    () => {
      setOptions({
        headerTitle: () => (
          <ScreenHeaderTitleContainer>
            <I18n as={ HeaderTitle }>
              rain record
            </I18n>
          </ScreenHeaderTitleContainer>
        )
      })
    },
    [setOptions]
  ))

  return (
    <AuthenticatedLayout>
      <ButtonContainer>
        <View>
          <Button
            onPress={ navigateRainRecordRegister }
            color={ colors.text }
            backgroundColor={ colors.secondary }
            title={ t('register rain') }
            disabled={ !userWrite }
          />
        </View>
      </ButtonContainer>
      <ModalFieldContainer>
        <InputView>
          <Button
            title={ t('filtrate') }
            onPress={ handleFilterFields }
            variant='outlined'
            style={ {
              marginTop: 3,
              backgroundColor: isEmpty(filters) ? colors.white : colors.secondary
            } }
            icon={ <Icon size={ 22 } icon={ filter } color={ colors.black } /> }
          />
        </InputView>
      </ModalFieldContainer>
      <SafeAreaViewList>
        <RainRecordList params={ filters } />
      </SafeAreaViewList>
    </AuthenticatedLayout>
  )
}

// eslint-disable-next-line react/prop-types
export default RainRecordScreen
