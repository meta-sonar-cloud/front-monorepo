import styled from 'styled-components/native'


export const ModalFieldContainer = styled.View`
  padding: 15px;
`

export const InputView = styled.View`
  margin-left: 10px;
  margin-right: 10px;
`

export const ButtonContainer = styled.View`
  position: absolute;
  bottom: 0;
  z-index: 2;
  width: 50%;
  left: 25%;
  padding-bottom: 20px;
`

export const SafeAreaViewList = styled.SafeAreaView`
  flex: 100;
  padding-bottom: 70px;
  width: 100%;
`
