import { Paragraph, Subheading } from 'react-native-paper'

import styled from 'styled-components/native'

import { colors } from '@smartcoop/styles'


export const Container = styled.View`
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 20px;
`

export const TextCenter = styled(Subheading)`
  text-align: center;
  font-weight: bold;
  color: ${ colors.text };
`

export const Row = styled.View`
  display: flex;
  flex-direction: row;
  justify-content: ${ ({ center }) => center ? 'center' : 'space-around' };
  color: ${ colors.text };
  align-items: center;
  width: 100%;
  max-width: ${ ({ limitWidth }) => limitWidth ? '100%' : 'auto' };
  padding: 0px 10px;
`

export const Column = styled.View`
  display: flex;
  flex-direction: column;
  justify-content: ${ ({ center }) => center ? 'center' : 'space-around' };
  color: ${ colors.text };
`

export const Text = styled(Paragraph)`
  color: ${ colors.text };
  font-weight: ${ ({ bold }) => bold ? 600 : 300 };
`

export const TemperatureContainer = styled.View`
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  padding: 10px 0 15px;
`

export const TemperatureTitle = styled.Text`
  color: ${ colors.text };
  font-weight: 600;
  font-size: 60;
  margin-left: 10;
`

export const CardHours = styled.View`
  padding: 10px 15px;
  border-radius: 5px;
  border: 1px solid #eeeeee;
  background-color: ${ colors.white };
  margin-right: 10px;
  shadow-color: #000;
  shadow-offset: 0px 1px;
  shadow-opacity: 0.2;
  shadow-radius: 1px;
  elevation: 2;
`

export const DayCircle = styled.View`
  align-self: flex-start;
  border-radius: 100;
  border: 1px solid #eeeeee;
  height: 60px;
  width: 60px;
  display: flex;
  align-items: center;
  justify-content: center;
`
