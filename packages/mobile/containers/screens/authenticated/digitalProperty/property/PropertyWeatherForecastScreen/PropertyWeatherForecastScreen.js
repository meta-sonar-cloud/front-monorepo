/* eslint-disable global-require */
/* eslint-disable import/no-dynamic-require */

import React, { useEffect, useMemo, useCallback } from 'react'
import { ScrollView } from 'react-native'
import { useDispatch, useSelector } from 'react-redux'

import { useRoute, useFocusEffect } from '@react-navigation/native'
import moment from 'moment'

import map from 'lodash/map'
import padStart from 'lodash/padStart'

import I18n, { useT } from '@smartcoop/i18n'
import arrowDownField from '@smartcoop/icons/svgs/arrowDownField'
import arrowUpField from '@smartcoop/icons/svgs/arrowUpField'
import touchGesture from '@smartcoop/icons/svgs/touchGesture'
import * as weatherIcons  from '@smartcoop/icons/weatherIcons'
import Divider from '@smartcoop/mobile-components/Divider'
import Icon from '@smartcoop/mobile-components/Icon'
import List from '@smartcoop/mobile-components/List'
import { useScreenOptions } from '@smartcoop/mobile-containers/hooks/useScreenOptions'
import AuthenticatedLayout from '@smartcoop/mobile-containers/layouts/AuthenticatedLayout'
import { ScreenHeaderTitleContainer, HeaderTitle } from '@smartcoop/mobile-containers/layouts/AuthenticatedLayout/theme'
import { WeatherForecastActions } from '@smartcoop/stores/weatherForecast'
import { selectCurrent, selectNextDays, selectNextHour } from '@smartcoop/stores/weatherForecast/selectorWeatherForecast'
import { colors } from '@smartcoop/styles'
import {
  momentBackDateTimeFormat,
  momentFriendlyExtendedDateFormat,
  momentFriendlyTimeFormat,
  momentFriendlyDateFormat
} from '@smartcoop/utils/dates'

import {
  Container,
  TextCenter,
  Row,
  Column,
  Text,
  TemperatureContainer,
  TemperatureTitle,
  CardHours,
  DayCircle
} from './styles'

const PropertyWeatherForecastScreen = () => {
  const t = useT()
  const dispatch = useDispatch()
  const { params } = useRoute()
  const { setOptions } = useScreenOptions()

  const current = useSelector(selectCurrent)
  const nextHours = useSelector(selectNextHour)
  const nextDays = useSelector(selectNextDays)

  const currentDate = useMemo(
    () => moment(current?.date, momentBackDateTimeFormat)
      .format(momentFriendlyExtendedDateFormat),
    [current]
  )

  useEffect(() => {
    if (params.coordinate) {
      const { latitude, longitude } = params.coordinate

      dispatch(WeatherForecastActions.searchCurrentWeather(latitude, longitude))
      dispatch(WeatherForecastActions.searchWeatherForecast(latitude, longitude))
    }
  }, [dispatch, params.coordinate])

  useFocusEffect(useCallback(
    () => {
      setOptions({
        headerTitle: () => (
          <ScreenHeaderTitleContainer>
            <I18n as={ HeaderTitle }>
              climate
            </I18n>
          </ScreenHeaderTitleContainer>
        )
      })
    },
    [setOptions]
  ))

  const ListHeaderComponent = useMemo(
    () => (
      <>
        {current && (
          <>
            <Container>
              <TextCenter>{ currentDate }</TextCenter>

              <Row center style={ { marginTop: 10 } }>
                <Text>{ current?.high }°C</Text>
                <Icon
                  icon={ arrowUpField }
                  size={ 10 }
                  color={ colors.orange }
                  style={ { marginLeft: 10, marginRight: 10 } }
                />
                <Text>{ current?.low }°C</Text>
                <Icon
                  icon={ arrowDownField }
                  size={ 10 }
                  color={ colors.blue }
                  style={ { marginLeft: 10 } }
                />
              </Row>

              <Row center>
                <Icon
                  icon={ weatherIcons[`icon${ padStart(current?.iconCode, 2, '0') }`] }
                  size={ 60 }
                />
                <TemperatureContainer>
                  <TemperatureTitle>{ Math.round(current?.average) }</TemperatureTitle>
                  <Text bold style={ { marginBottom: 25 } }>°C</Text>
                </TemperatureContainer>
              </Row>

              <Row style={ { alignItems: 'space-around' } }>
                <Column>
                  <Text bold>{t('humidity')}</Text>
                  <Text>{current?.humidity}%</Text>
                </Column>
                <Column>
                  <Text bold>{t('wind')}</Text>
                  <Text>{current?.wind} km/h</Text>
                </Column>
                <Column>
                  <Text bold>{t('precipitation')}</Text>
                  <Text>{current?.precipitation} mm</Text>
                </Column>
              </Row>
            </Container>

            <ScrollView horizontal>
              <Row
                onStartShouldSetResponder ={ () => true }
                style={ { margin: 3 } }
              >
                {map(nextHours, item => (
                  <CardHours key={ item.date }>
                    <Text style={ { marginBottom: 5 } }>
                      { moment(item.date, momentBackDateTimeFormat).format(momentFriendlyTimeFormat) }
                    </Text>
                    <Icon
                      icon={ weatherIcons[`icon${ padStart(item.iconCode, 2, '0') }`] }
                      size={ 30 }
                      style={ { marginBottom: 5 } }
                    />
                    <Text bold>{ Math.round(item.temperature) }°C</Text>
                  </CardHours>
                ))}
              </Row>
            </ScrollView>

            <Row
              style={ {
                justifyContent: 'flex-end',
                marginTop: 5
              } }
            >
              <Icon icon={ touchGesture } />
              <Text>{t('swipe to see the rest')}</Text>
            </Row>

            <Divider style={ { marginTop: 0, marginBottom: 0 } } />
          </>
        )}
        <Container style={ { backgroundColor: colors.backgroundHtml } }>
          <Row style={ { justifyContent: 'flex-start', paddingTop: 0, paddingBottom: 0 } }>
            <Text bold>{t('forecast for the next 15 days')}</Text>
          </Row>
        </Container>
      </>
    ),
    [current, currentDate, nextHours, t]
  )

  const renderNextDay = useCallback(
    ({ item }) => (
      <Row style={ { paddingTop: 5 } }>
        <DayCircle>
          <Text
            bold
            style={ {
              marginBottom: 0,
              marginTop: 0
            } }
          >
            { moment(item.date, momentFriendlyDateFormat).format('DD') }
          </Text>
          <Text
            style={ {
              marginBottom: 0,
              marginTop: 0
            } }
          >
            { moment(item.date, momentFriendlyDateFormat).format('ddd') }
          </Text>
        </DayCircle>

        <Column style={ { flex: 1, marginBottom: 10 } }>
          <Row limitWidth style={ { justifyContent: 'space-between' } }>

            <Column center>
              <Row limitWidth center>
                <Text>{ item?.high }°C</Text>
                <Icon
                  icon={ arrowUpField }
                  size={ 10 }
                  color={ colors.orange }
                  style={ { marginLeft: 10 } }
                />
              </Row>

              <Row limitWidth center>
                <Text>{ item?.low }°C</Text>
                <Icon
                  icon={ arrowDownField }
                  size={ 10 }
                  color={ colors.blue }
                  style={ { marginLeft: 10 } }
                />
              </Row>
            </Column>

            <Icon
              icon={ weatherIcons[`icon${ padStart(item.iconCode, 2, '0') }`] }
              size={ 30 }
            />

            <Row style={ { flex: 1, justifyContent: 'flex-end' } }>
              <Text>{item?.description || '-'}</Text>
            </Row>
          </Row>

          <Row style={ { marginTop: 5 } }>
            <Column>
              <Text bold>{t('humidity')}</Text>
              <Text>{item.humidity}%</Text>
            </Column>

            <Column>
              <Text bold>{t('wind')}</Text>
              <Text>{item.wind} km/h</Text>
            </Column>

            <Column>
              <Text bold>{t('precipitation')}</Text>
              <Text>{item?.precipitation} mm</Text>
            </Column>
          </Row>

        </Column>
      </Row>
    ),
    [t]
  )

  return (
    <AuthenticatedLayout>
      <List
        ListHeaderComponent={ ListHeaderComponent }
        data={ nextDays }
        renderItem={ renderNextDay }
        itemKey="date"
      />
    </AuthenticatedLayout>
  )
}

export default PropertyWeatherForecastScreen
