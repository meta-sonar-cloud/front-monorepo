import React, { useState, useCallback, useRef, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'

import { useNavigation } from '@react-navigation/native'

import uniq from 'lodash/uniq'

import I18n, { useT } from '@smartcoop/i18n'
import { calendar, rain, heatmap } from '@smartcoop/icons'
import { Title } from '@smartcoop/mobile-components/CardTitle'
import Icon from '@smartcoop/mobile-components/Icon'
import PropertyActivitiesChart from '@smartcoop/mobile-containers/fragments/PropertyActivitiesChart'
import PropertyGrowingSeasonsChart from '@smartcoop/mobile-containers/fragments/PropertyGrowingSeasonsChart'
import PropertyPendingManagementsList from '@smartcoop/mobile-containers/fragments/PropertyPendingManagementsList'
import PropertyResumeCard from '@smartcoop/mobile-containers/fragments/PropertyResumeCard'
import WeatherForecastCard from '@smartcoop/mobile-containers/fragments/PropertyWeatherForecastCard'
import AuthenticatedLayout from '@smartcoop/mobile-containers/layouts/AuthenticatedLayout'
import { Scroll } from '@smartcoop/mobile-containers/layouts/AuthenticatedLayout/theme'
import { PropertyActions } from '@smartcoop/stores/property'
import { selectCurrentProperty, selectReloadData } from '@smartcoop/stores/property/selectorProperty'
import { selectUser } from '@smartcoop/stores/user/selectorUser'
import { colors } from '@smartcoop/styles'

import {
  Item,
  CardWelcome,
  CardItemTitle,
  WelcomeBackground,
  HeaderTitle,
  HeaderTitleBold,
  WelcomeBackgroundContainerAbsolute,
  WelcomeBackgroundContainer,
  WelcomeInformationContainer,
  ButtonCard
} from './styles'

const PropertyHomeScreen = () => {
  const t = useT()

  const reloadData = useSelector(selectReloadData)

  const dispatch = useCallback(useDispatch(), [])

  const user = useSelector(selectUser)
  const currentProperty = useSelector(selectCurrentProperty)

  const mounted = useRef(false)
  const navigation = useNavigation()

  const navigateRainRecordList = useCallback(
    () => {
      navigation.navigate('Property', { screen: 'RainRecord' })
    },
    [navigation]
  )

  const navigateRainMap = useCallback(
    () => {
      navigation.navigate('Property', { screen: 'RainMap' })
    },
    [navigation]
  )

  const [forceRefresh, setForceRefresh] = useState(false)
  const [refreshing, setRefreshing] = useState(false)
  const [cardsLoaded, setCardsLoaded] = useState([])

  const handleCardLoaded = useCallback(
    (cardName) => {
      if (mounted.current) {
        setCardsLoaded(old => uniq([...old, cardName]))
      }
    },
    []
  )

  const onRefresh = useCallback(
    () => {
      setCardsLoaded([])
      setForceRefresh(old => !old)
      setRefreshing(true)
    },
    []
  )

  useEffect(
    () => {
      if (mounted.current && cardsLoaded.length === 4) {
        setRefreshing(false)
      }
    },
    [cardsLoaded]
  )

  useEffect(
    () => {
      mounted.current = true
      return () => {
        mounted.current = false
      }
    },
    []
  )

  useEffect(() => {
    if (reloadData){
      dispatch(PropertyActions.setReloadData(false))
    }
  }, [dispatch, reloadData])

  return (
    <AuthenticatedLayout style={ { backgroundColor: colors.backgroundHtml } }>
      <>
        <Scroll
          padding={ 0 }
          refreshControl
          refreshing={ refreshing }
          onRefresh={ onRefresh }
          contentContainerStyle={ { flexGrow: 1 } }
        >
          <CardWelcome>
            <WelcomeBackgroundContainerAbsolute>
              <WelcomeBackgroundContainer>
                <WelcomeBackground />
              </WelcomeBackgroundContainer>
            </WelcomeBackgroundContainerAbsolute>

            <WelcomeInformationContainer>
              <I18n as={ HeaderTitle } params={ { user: user?.name } }>
                welcome
              </I18n>
              <HeaderTitleBold>
                {user?.name}!
              </HeaderTitleBold>
            </WelcomeInformationContainer>
          </CardWelcome>

          <Item>
            <WeatherForecastCard
              forceRefresh={ forceRefresh }
              onLoaded={ handleCardLoaded }
            />
          </Item>

          <ButtonCard
            title={ t('rain record') }
            icon={ <Icon icon={ rain } size={ 20 } /> }
            onPress={ navigateRainRecordList }
          />

          <ButtonCard
            title={ t('rain map') }
            icon={ <Icon icon={ heatmap } size={ 20 } /> }
            onPress={ navigateRainMap }
          />

          <PropertyResumeCard property={ currentProperty } />

          <CardItemTitle
            title={ <I18n as={ Title }>scheduled management</I18n> }
            titleLeft={ <Icon icon={ calendar } size={ 20 } /> }
            childrenStyle={ {
              paddingTop: 0,
              paddingBottom: 0,
              paddingRight: 0,
              paddingLeft: 0
            } }
          >
            <PropertyPendingManagementsList
              property={ currentProperty }
              forceRefresh={ forceRefresh }
              onLoaded={ handleCardLoaded }
            />
          </CardItemTitle>

          <CardItemTitle
            title={ <I18n as={ Title } params={ { howMany: 1 } }>activity</I18n> }
            childrenStyle={ { minHeight: 300 } }
          >
            <PropertyActivitiesChart
              property={ currentProperty }
              forceRefresh={ forceRefresh }
              onLoaded={ handleCardLoaded }
            />
          </CardItemTitle>

          <CardItemTitle
            title={ <I18n as={ Title }>finality and culture</I18n> }
            childrenStyle={ { minHeight: 300 } }
          >
            <PropertyGrowingSeasonsChart
              property={ currentProperty }
              forceRefresh={ forceRefresh }
              onLoaded={ handleCardLoaded }
            />
          </CardItemTitle>

        </Scroll>
      </>
    </AuthenticatedLayout>
  )
}

export default PropertyHomeScreen
