import { Title } from 'react-native-paper'

import styled from 'styled-components/native'

import Button from '@smartcoop/mobile-components/Button'
import Card from '@smartcoop/mobile-components/Card'
import CardTitle from '@smartcoop/mobile-components/CardTitle'
import { colors } from '@smartcoop/styles'
import Background from '@smartcoop/styles/assets/images/homeCardBackground.svg'


export const CardWelcome = styled(Card).attrs({
  childrenStyle: {
    padding: 0
  }
})`
  margin-bottom: 10px;
`

export const CardItemTitle = styled(CardTitle).attrs(props => ({
  childrenStyle: {
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 20,
    paddingRight: 20,
    margin: 5,
    backgroundColor: 'yellow',
    ...(props.childrenStyle || {})
  },
  ...props
}))`
  margin-bottom: 10px;
`

export const WelcomeBackgroundContainerAbsolute = styled.View`
  position: absolute;
  width: 100%;
  height: 100%;
`

export const WelcomeBackgroundContainer = styled.View`
  align-items: flex-end;
  justify-content: flex-end;
  flex: 1;
`

export const WelcomeInformationContainer = styled.View`
  padding: 14px 20px;
`

export const WelcomeBackground = styled(Background).attrs({
  height: '100%',
  width: '75%'
})`
  margin: 0 7px;
`

export const HeaderTitle = styled(Title).attrs({
})`
  font-family: 'Montserrat';
  margin: 0;
`

export const HeaderTitleBold = styled(HeaderTitle)`
  font-weight: bold;
`

export const ButtonCard = styled(Button).attrs({
  // variant: 'outlined',

  color: colors.text,
  backgroundColor: colors.white
})`
  margin: 5px 10px;
`

export const Item = styled.View`
  margin: 0px 10px 5px;
`
