import React, { useEffect, useCallback } from 'react'
import { useDispatch } from 'react-redux'

import { useNavigation, useRoute } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'

import { withScreenOptions, useScreenOptions } from '@smartcoop/mobile-containers/hooks/useScreenOptions'
import { PropertyActions } from '@smartcoop/stores/property'

import PropertyActivityDetailsScreen from './PropertyActivityDetailsScreen'
import PropertyActivityScreen from './PropertyActivityScreen'
import PropertyIdentificationScreen from './PropertyIdentificationScreen'
import PropertyListScreen from './PropertyListScreen'
import PropertyLocalizationScreen from './PropertyLocalizationScreen'
import PropertyWeatherForecastScreen from './PropertyWeatherForecastScreen'
import RainMapScreen from './RainMapScreen/RainMapScreen'
import RainRecordRegisterScreen from './RainRecordRegisterScreen'
import RainRecordScreen from './RainRecordScreen'

const Stack = createStackNavigator()

const PropertyScreenRouter = () => {
  const navigation = useNavigation()
  const dispatch = useCallback(useDispatch(), [])
  const route = useRoute()
  const isEditing = route?.params?.params?.isEditing ?? false
  const { options } = useScreenOptions(navigation)

  useEffect(() => {
    if(!isEditing) {
      dispatch(PropertyActions.resetOfflineProperty())
    }
  }, [dispatch, isEditing])

  return (
    <Stack.Navigator screenOptions={ options }>
      <Stack.Screen
        name="Properties"
        component={ PropertyListScreen }
      />
      <Stack.Screen
        name="Identification"
        component={ PropertyIdentificationScreen }
      />
      <Stack.Screen
        name="Localization"
        component={ PropertyLocalizationScreen }
      />
      <Stack.Screen
        name="Activity"
        component={ PropertyActivityScreen }
      />
      <Stack.Screen
        name="ActivityDetails"
        component={ PropertyActivityDetailsScreen }
      />
      <Stack.Screen
        name="WeatherForecast"
        component={ PropertyWeatherForecastScreen }
      />
      <Stack.Screen
        name="RainRecord"
        component={ RainRecordScreen }
      />
      <Stack.Screen
        name="RainRecordRegister"
        component={ RainRecordRegisterScreen }
      />
      <Stack.Screen
        name="RainMap"
        component={ RainMapScreen }
      />
    </Stack.Navigator>
  )
}

export default withScreenOptions(PropertyScreenRouter)
