/* eslint-disable react/prop-types */
import React, { useEffect, useCallback, useMemo } from 'react'
import { PermissionsAndroid, Platform } from 'react-native'
import { useSelector } from 'react-redux'

import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import { useNavigation, useFocusEffect } from '@react-navigation/native'
import styled from 'styled-components/native'

import isEmpty from 'lodash/isEmpty'

import { useDialog } from '@smartcoop/dialog'
import { useT } from '@smartcoop/i18n'
import {
  field,
  arrowDown,
  // socialNetwork,
  tractor,
  cattle,
  home
} from '@smartcoop/icons'
import Icon from '@smartcoop/mobile-components/Icon'
import Loader from '@smartcoop/mobile-components/Loader'
import MenuHamburguerButton from '@smartcoop/mobile-containers/fragments/MenuHamburguerButton/MenuHamburguerButton'
import { useScreenOptions } from '@smartcoop/mobile-containers/hooks/useScreenOptions'
import { withDrawer } from '@smartcoop/mobile-containers/hooks/withDrawer'
import {
  HeaderTitle,
  ScreenHeaderTitleContainer
} from '@smartcoop/mobile-containers/layouts/AuthenticatedLayout/theme'
import ChangePropertyModal from '@smartcoop/mobile-containers/modals/ChangePropertyModal'
import ZeroPropertiesModal from '@smartcoop/mobile-containers/modals/ZeroPropertiesModal'
import {
  selectSuggestPropertyRegister,
  selectCurrentProperty,
  selectProperties
} from '@smartcoop/stores/property/selectorProperty'
import colors from '@smartcoop/styles/colors'

// import SocialFeedScreen from '../social/SocialFeedScreen'

import DairyFarmDashboardScreen from './dairyFarm/DairyFarmDashboardScreen'
import FieldsMapScreen from './field/FieldsMapScreen'
import MachineryListScreen from './machinery/MachineryListScreen'
import PropertyHomeScreen from './property/PropertyHomeScreen'

export const Row = styled.View`
  flex-direction: row;
  align-items: center;
  justify-content: center;
`
export const IconContainer = styled.View`
  margin-top: 2px;
`
export const Backdrop = styled.View`
  position: absolute;
  height: 100%;
  width: 100%
  justify-content: center;
`

export const Title = styled.Text`
  color: ${ colors.white };
  font-size: 18px;
`

const Tab = createBottomTabNavigator()

const DigitalPropertyRouter = () => {
  const navigation = useNavigation()
  // const navigation = useMemo(() => ({ navigate(){} }), [])
  const { createDialog, removeDialog } = useDialog()
  const t = useT()
  const { setOptions, tabBarOptions } = useScreenOptions()

  const properties = useSelector(selectProperties)
  const currentProperty = useSelector(selectCurrentProperty)
  const suggestPropertyRegister = useSelector(selectSuggestPropertyRegister)

  const tabsOptions = useMemo(
    () => ({
      home: {
        title: t('property', { howMany: 1 }),
        tabBarIcon: ({ color }) => <Icon icon={ home } size={ 28 } color={ color } />
      },
      fields: {
        title: t('field', { howMany: 2 }),
        tabBarIcon: ({ color }) => <Icon icon={ field } size={ 23 } color={ color } />
      },
      dairyFarmDashboard: {
        title: t('dairy farm', { howMany: 2 }),
        tabBarIcon: ({ color }) => (
          <Icon icon={ cattle } size={ 28 } color={ color } />
        )
      },
      machineryList: {
        title: t('machinery', { howMany: 2 }),
        tabBarIcon: ({ color, size }) => (
          <Icon icon={ tractor } size={ size } color={ color } />
        )
      }
      // socialFeed: {
      //   tabBarLabel: t('network'),
      //   tabBarIcon: ({ color, size }) => (
      //     <Icon icon={ socialNetwork } size={ size } color={ color } />
      //   )
      // }
    }),
    [t]
  )

  const changePropertyDialog = useCallback(() => {
    const handleOnPressButton = () => {
      removeDialog({ id: 'property-onboarding' })
      navigation.navigate('Property', { screen: 'Identification' })
    }

    createDialog({
      id: 'property-onboarding',
      Component: ChangePropertyModal,
      props: {
        onPressButton: handleOnPressButton
      }
    })
  }, [createDialog, navigation, removeDialog])

  const handleSuggestPropertyRegister = useCallback(() => {
    if (suggestPropertyRegister) {
      createDialog({
        id: 'zero-properties',
        Component: ZeroPropertiesModal,
        props: {
          onConfirm: () => {
            navigation.navigate('Property', { screen: 'Identification' })
          }
        }
      })
    }
  }, [createDialog, navigation, suggestPropertyRegister])

  useEffect(() => {
    console.log('useEffect1', suggestPropertyRegister)
    if (
      !suggestPropertyRegister &&
      isEmpty(currentProperty) &&
      !isEmpty(properties)
    ) {
      changePropertyDialog()
    } else if (
      suggestPropertyRegister &&
      isEmpty(properties) &&
      isEmpty(currentProperty)
    ) {
      handleSuggestPropertyRegister()
    }
  }, [
    changePropertyDialog,
    currentProperty,
    handleSuggestPropertyRegister,
    properties,
    suggestPropertyRegister
  ])

  useEffect(() => {
    if (Platform.OS === 'android') {
      PermissionsAndroid.requestMultiple([
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION
      ])
    }
  }, [t])

  useFocusEffect(
    useCallback(() => {
      setOptions({
        headerLeft: () => <MenuHamburguerButton navigation={ navigation } />,
        headerTitle: (props) => (
          <ScreenHeaderTitleContainer>
            <HeaderTitle onPress={ changePropertyDialog } { ...props }>
              <Row>
                <Title { ...props }>
                  {currentProperty.name || t('select one property')}
                </Title>
                <IconContainer>
                  <Icon icon={ arrowDown } color={ colors.white } />
                </IconContainer>
              </Row>
            </HeaderTitle>
          </ScreenHeaderTitleContainer>
        )
      })
    }, [changePropertyDialog, currentProperty.name, navigation, setOptions, t])
  )

  return isEmpty(currentProperty) ? (
    <Backdrop>
      <Loader width={ 100 } />
    </Backdrop>
  ) : (
    <Tab.Navigator
      initialRouteName="Home"
      tabBarOptions={ tabBarOptions }
      lazy={ false }
    >
      <Tab.Screen
        name="Home"
        options={ tabsOptions.home }
        component={ PropertyHomeScreen }
      />
      <Tab.Screen
        name="Fields"
        options={ tabsOptions.fields }
        component={ FieldsMapScreen }
      />
      <Tab.Screen
        name="DairyFarmDashboard"
        options={ tabsOptions.dairyFarmDashboard }
        component={ DairyFarmDashboardScreen }
      />
      <Tab.Screen
        name="MachineryList"
        options={ tabsOptions.machineryList }
        component={ MachineryListScreen }
      />
      {/* <Tab.Screen
        name="SocialFeed"
        options={ tabsOptions.socialFeed }
        component={ SocialFeedScreen }
      /> */}
    </Tab.Navigator>
  )
}

export default withDrawer(DigitalPropertyRouter)
