import React, { useRef, useCallback } from 'react'
import { View } from 'react-native'
import { useSelector, useDispatch } from 'react-redux'

import { useFocusEffect , useNavigation } from '@react-navigation/native'

import I18n, { useT } from '@smartcoop/i18n'
import Button from '@smartcoop/mobile-components/Button'
import GrowingSeasonForm from '@smartcoop/mobile-containers/forms/digitalProperty/growingSeason/GrowingSeasonForm'
import { useScreenOptions } from '@smartcoop/mobile-containers/hooks/useScreenOptions'
import { Container, Item, Scroll, ButtonsContainer , ScreenHeaderTitleContainer, HeaderTitle } from '@smartcoop/mobile-containers/layouts/AuthenticatedLayout/theme'
import { useSnackbar } from '@smartcoop/snackbar'
import { selectCurrentField } from '@smartcoop/stores/field/selectorField'
import { PropertyActions } from '@smartcoop/stores/property'

const GrowingSeasonRegisterScreen = () => {
  const growingSeasonFormRef = useRef(null)
  const navigation = useNavigation()
  const t = useT()
  const snackbar = useSnackbar()
  const dispatch = useDispatch()
  const { setOptions } = useScreenOptions()

  const { growingSeason } = useSelector(selectCurrentField)

  const submitForms = useCallback(
    () => {
      growingSeasonFormRef.current.submit()
    },
    []
  )

  const handleClose = useCallback(
    () => {
      navigation.goBack()
    },
    [navigation]
  )

  const onSuccess = useCallback(
    () => {
      dispatch(PropertyActions.setReloadData(true))
      snackbar.success(t('your {this} was registered', {
        howMany: 1,
        gender: 'male',
        this: t('crops', { howMany: 1 })
      }))
      handleClose()
    },
    [dispatch, handleClose, snackbar, t]
  )

  useFocusEffect(useCallback(
    () => {
      setOptions({
        headerTitle: () => (
          <ScreenHeaderTitleContainer>
            <I18n as={ HeaderTitle }>
              crops register
            </I18n>
          </ScreenHeaderTitleContainer>
        )
      })
    },
    [setOptions]
  ))

  return (
    <Container>
      <Scroll padding={ 0 }>
        <Item
          style={ {
            flex: 1,
            paddingTop: 20,
            paddingBottom: 0
          } }
        >
          <GrowingSeasonForm
            ref={ growingSeasonFormRef }
            onSuccess={ onSuccess }
            withoutSubmitButton
            growingSeason={ growingSeason }
          />
        </Item>
      </Scroll>

      <Item>
        <ButtonsContainer style={ { paddingTop: 10 } }>
          <Button
            onPress={ handleClose }
            style={ { flex: 1 } }
            variant="outlined"
            title={ t('cancel') }
          />

          <View style={ { width: '10%' } } />

          <Button
            onPress={ submitForms }
            style={ { flex: 1 } }
            title={ t('save') }
          />
        </ButtonsContainer>
      </Item>
    </Container>
  )}

export default GrowingSeasonRegisterScreen
