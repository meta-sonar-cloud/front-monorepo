import React from 'react'

import { useNavigation } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'

import { useScreenOptions, withScreenOptions } from '@smartcoop/mobile-containers/hooks/useScreenOptions'

import GrowingSeasonRegisterScreen from './GrowingSeasonRegisterScreen'

const Stack = createStackNavigator()

const GrowingSeasonScreenRouter = () => {
  const navigation = useNavigation()
  const { options } = useScreenOptions(navigation)

  return (
    <Stack.Navigator screenOptions={ options } mode="modal">
      <Stack.Screen
        name="Register"
        component={ GrowingSeasonRegisterScreen }
      />
    </Stack.Navigator>
  )
}

export default withScreenOptions(GrowingSeasonScreenRouter)
