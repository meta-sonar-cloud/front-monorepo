import styled from 'styled-components'

import colors from '@smartcoop/styles/colors'

export const ContainerHeaderButtons = styled.View`
  padding-left: 25px;
  padding-right: 25px;
  flex-direction: row;
  justify-content: center;
  position: absolute;
  top: 20px;
  width: 100%;
`

export const ButtonsZoom = styled.View`
  border-radius: 4px;
  padding: 7px 0;
  justify-content: space-between;
  align-items: center;
  background-color: ${ colors.white };
  width: 36px;
  height: 72px;
`

export const Area = styled.View`
  border-radius: 4px;
  padding: 0;
  width: 120px;
  height: 36px;
  background-color: ${ colors.white };
`


export const ContainerFooterButtons = styled.View`
  padding-left: 25px;
  padding-right: 25px;
  flex-direction: row;
  justify-content: space-between;
  position: absolute;
  bottom: 8%;
  width: 100%;
`
