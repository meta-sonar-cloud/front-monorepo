import React, { useCallback, useEffect, useState, useRef, useMemo } from 'react'
import { View } from 'react-native'
import { useSelector } from 'react-redux'

import { useNavigation, useRoute , useFocusEffect } from '@react-navigation/native'


import isEmpty from 'lodash/isEmpty'
import size from 'lodash/size'

import I18n, { useT } from '@smartcoop/i18n'
// import { zoomIn, zoomOut } from '@smartcoop/icons'
import Button from '@smartcoop/mobile-components/Button'
// import Divider from '@smartcoop/mobile-components/Divider'
// import Icon from '@smartcoop/mobile-components/Icon'
import InputUnit from '@smartcoop/mobile-components/InputUnit'
import Maps from '@smartcoop/mobile-components/Maps'
import Polygon from '@smartcoop/mobile-components/Maps/components/Polygon'
import { useScreenOptions } from '@smartcoop/mobile-containers/hooks/useScreenOptions'
import AuthenticatedLayout from '@smartcoop/mobile-containers/layouts/AuthenticatedLayout'
import { ButtonsContainer , ScreenHeaderTitleContainer, HeaderTitle } from '@smartcoop/mobile-containers/layouts/AuthenticatedLayout/theme'
import { selectCurrentProperty } from '@smartcoop/stores/property/selectorProperty'
import colors from '@smartcoop/styles/colors'
import  { getPolygonCenter } from '@smartcoop/utils/maps'


import { ContainerHeaderButtons, ContainerFooterButtons, Area } from './styles'

const FieldDrawScreen = () => {
  const [polygonCoordinates, setPolygonCoordinates] = useState([])
  const [area, setArea] = useState()

  const polygonRef = useRef(null)

  const navigation = useNavigation()
  const { params } = useRoute()
  const t = useT()
  const { setOptions } = useScreenOptions()

  const currentProperty = useSelector(selectCurrentProperty)

  const region = useMemo(
    () => !isEmpty(params)
      ? getPolygonCenter(params.polygonCoordinates)
      : currentProperty.geolocalization,
    [currentProperty.geolocalization, params]
  )

  const handleRegisterFieldClick = useCallback(
    () => {
      navigation.navigate('Register', {
        polygonCoordinates,
        area
      })
    },
    [navigation, polygonCoordinates, area]
  )

  const createPoint = useCallback(
    (e) => {
      if (e.nativeEvent.coordinate && !e.nativeEvent.id) {
        const { latitude, longitude } = e.nativeEvent.coordinate
        const newCoordinates = polygonRef.current.sortNewPoint([latitude, longitude])
        setPolygonCoordinates(newCoordinates)
      }
    },
    []
  )

  const handleClose = useCallback(
    () => navigation.goBack(),
    [navigation]
  )

  useEffect(
    () => {
      if (!isEmpty(params)) {
        setPolygonCoordinates(params.polygonCoordinates)
        setArea(params.area)
      }
    },
    [navigation.field, params, polygonCoordinates]
  )

  useFocusEffect(useCallback(
    () => {
      setOptions({
        headerTitle: () => (
          <ScreenHeaderTitleContainer>
            <I18n as={ HeaderTitle }>
              draw the field on the map
            </I18n>
          </ScreenHeaderTitleContainer>
        )
      })
    },
    [setOptions]
  ))

  return (
    <AuthenticatedLayout>
      <Maps
        onPress={ createPoint }
        region={ region }
        mapType="hybrid"
        overlayButton={
          <>
            <ContainerHeaderButtons>
              <Area>
                <InputUnit
                  style={ { height: '100%', width: '100%' } }
                  detached
                  disabled
                  value={ area }
                  unit="ha"
                  name="text"
                />
              </Area>
              {/* <ButtonsZoom>
                <View>
                  <Icon icon={ zoomIn } size={ 20 }/>
                </View>
                <Divider />
                <View>
                  <Icon icon={ zoomOut } size={ 20 }/>
                </View>
              </ButtonsZoom> */}
            </ContainerHeaderButtons>
            <ContainerFooterButtons>
              <ButtonsContainer >
                <Button
                  onPress={ handleClose }
                  style={ { flex: 1 } }
                  variant="outlined"
                  title={ t('cancel') }
                />

                <View style={ { width: '10%' } } />

                <Button
                  style={ { flex: 1 } }
                  color={ colors.black }
                  title={ t('confirm') }
                  onPress={ handleRegisterFieldClick }
                  disabled={ size(polygonCoordinates) < 3 }
                  backgroundColor={ colors.secondary }
                />
              </ButtonsContainer>
            </ContainerFooterButtons>
          </>
        }
      >
        <Polygon
          ref={ polygonRef }
          points={ polygonCoordinates }
          onChange={ setPolygonCoordinates }
          onChangeArea={ setArea }
        />
      </Maps>
    </AuthenticatedLayout>
  )}

export default FieldDrawScreen
