import React, { useCallback, useEffect, useMemo, useState } from 'react'
import { Platform } from 'react-native'
import { Overlay } from 'react-native-maps'
import { useDispatch, useSelector } from 'react-redux'

import { useNavigation, useRoute, useFocusEffect } from '@react-navigation/native'

import isEmpty from 'lodash/isEmpty'
import map from 'lodash/map'
import max from 'lodash/max'
import min from 'lodash/min'

import { useDialog } from '@smartcoop/dialog'
import { useT } from '@smartcoop/i18n'
import { trash, edit, bugMarkerRound, plantAndHand } from '@smartcoop/icons'
import Button from '@smartcoop/mobile-components/Button'
import Icon from '@smartcoop/mobile-components/Icon'
import Maps from '@smartcoop/mobile-components/Maps'
import Polygon from '@smartcoop/mobile-components/Maps/components/Polygon'
import PinMarker from '@smartcoop/mobile-components/Maps/markers/PinMarker'
import ConfirmModal from '@smartcoop/mobile-components/Modal/ConfirmModal'
import FieldDetailAccordion from '@smartcoop/mobile-containers/fragments/FieldDetailAccordion/FieldDetailAccordion'
import FieldIndicators from '@smartcoop/mobile-containers/fragments/FieldIndicators'
import { useScreenOptions } from '@smartcoop/mobile-containers/hooks/useScreenOptions'
import { Container, Item , ScreenHeaderTitleContainer, HeaderTitle } from '@smartcoop/mobile-containers/layouts/AuthenticatedLayout/theme'
import PestReportModal from '@smartcoop/mobile-containers/modals/PestReportModal'
import { useSnackbar } from '@smartcoop/snackbar'
import { FieldActions } from '@smartcoop/stores/field'
import { selectCurrentField } from '@smartcoop/stores/field/selectorField'
import { selectModuleIsTechnical } from '@smartcoop/stores/module/selectorModule'
import { selectUserCanWrite } from '@smartcoop/stores/user/selectorUser'
import colors from '@smartcoop/styles/colors'
import  { getCenterCoordinates } from '@smartcoop/utils/maps'

import { ButtonsContainer } from './styles'

const FieldDetailsScreen = () => {
  const navigation = useNavigation()
  const route = useRoute()
  const t = useT()
  const snackbar = useSnackbar()
  const { createDialog } = useDialog()
  const { setOptions } = useScreenOptions()

  const dispatch = useCallback(useDispatch(), [])

  const [imageIndicator, setImageIndicator] = useState(null)

  const { fieldId } = route.params

  const field = useSelector(selectCurrentField)
  const technicalModule = useSelector(selectModuleIsTechnical)
  const userWrite = useSelector(selectUserCanWrite)

  const { growingSeasons } = field

  const plagueReports = useMemo(
    () => !isEmpty(growingSeasons) ? growingSeasons[0].plagueReports : [], [growingSeasons]
  )

  const growingSeason = useMemo(
    () => !isEmpty(field.growingSeason) ? field.growingSeason : {},
    [field.growingSeason]
  )

  const region = useMemo(
    () => field.polygonCoordinates ? getCenterCoordinates(field.polygonCoordinates) : {},
    [field.polygonCoordinates]
  )

  const indicatorsPoints = useMemo(() => {
    const lat = map(field?.polygonCoordinates, item => item[0])
    const long = map(field?.polygonCoordinates, item => item[1])

    if (Platform.OS === 'android') {
      return [
        [max(lat), min(long)],
        [min(lat), max(long)]
      ]
    }

    return [
      [min(lat), min(long)],
      [max(lat), max(long)]
    ]

  }, [field])

  const handleTechnicalVisitRegister = useCallback(
    () => {
      navigation.navigate('TechnicalRegisterVisit', {
        cultivateName: growingSeason.crop?.description,
        fieldName: field?.fieldName
      })
    },
    [field, growingSeason, navigation]
  )

  const handleDeleteField = useCallback(
    () => {
      createDialog({
        id: 'delete-field',
        Component: ConfirmModal,
        props: {
          onConfirm: () => {
            if (isEmpty(growingSeason)) {
              dispatch(FieldActions.deleteField(field.id))
              snackbar.success(
                t('your {this} was deleted', {
                  howMany: 1,
                  gender: 'male',
                  this: t('field', { howMany: 1 })
                })
              )
              navigation.navigate('DigitalProperty', { screen: 'Fields' })
            } else {
              snackbar.warning(
                t('it is not possible to exclude fields with registered cultivation')
              )
            }
          },
          message: t('are you sure you want to delete the {this}?', {
            howMany: 1,
            gender: 'male',
            this: t('field', { howMany: 1 })
          }),
          buttonText: t('yes'),
          underlinedText: t('register another property')
        }
      })
    },
    [createDialog, dispatch, field.id, growingSeason, navigation, snackbar, t]
  )

  const handleDeleteReport= useCallback(
    (reportId) => {
      createDialog({
        id: 'delete-report',
        Component: ConfirmModal,
        props: {
          onConfirm: () => {
            dispatch(FieldActions.deleteOfflinePestReport(reportId))
            snackbar.success(
              t('your {this} was deleted', {
                howMany: 1,
                gender: 'male',
                this: t('report', { howMany: 1 })
              })
            )
            navigation.navigate('DigitalProperty', { screen: 'Fields' })
          },
          message: t('are you sure you want to delete the {this}?', {
            howMany: 1,
            gender: 'male',
            this: t('report', { howMany: 1 })
          })
        }
      })
    },
    [createDialog, dispatch, navigation, snackbar, t]
  )

  const handleEditReport = useCallback(
    (pestReport) => {
      navigation.navigate('PestReport',
        {
          screen: 'Map',
          params: {
            coordinates: field.polygonCoordinates,
            pestReport
          }
        })
    },
    [field.polygonCoordinates, navigation]
  )

  const openReportDetails = useCallback(
    (pestReport) => (
      createDialog({
        id: 'pest-report',
        Component: PestReportModal,
        props: {
          pestReport: { ...pestReport },
          onDelete: handleDeleteReport,
          onEdit: handleEditReport
        }
      })
    ), [createDialog, handleEditReport, handleDeleteReport]
  )

  const handleEditField = useCallback(
    () => {
      navigation.navigate('Register', {
        field,
        polygonCoordinates: field.polygonCoordinates,
        area: (field.area).toFixed(2)
      })
    },
    [field, navigation]
  )

  useFocusEffect(useCallback(
    () => {
      setOptions({
        headerTitle: () => (
          <ScreenHeaderTitleContainer>
            <HeaderTitle>
              {`${ t('field', { howMany: 1 }) }: ${ field?.fieldName || '' }`}
            </HeaderTitle>
          </ScreenHeaderTitleContainer>
        )
      })
    },
    [field.fieldName, setOptions, t]
  ))

  useFocusEffect(
    useCallback(() => {
      dispatch(FieldActions.loadCurrentField(fieldId))
    }, [dispatch, fieldId])
  )

  useEffect(
    () => () => dispatch(FieldActions.resetCurrentField()),
    [dispatch]
  )

  return (
    <Container>
      <Item
        style={ {
          flex: 1,
          paddingTop: 20,
          paddingBottom: 0
        } }
      >
        <Maps
          region={ region }
          mapType="hybrid"
          overlayButton={
            <>
              {userWrite && (
                <ButtonsContainer>
                  {technicalModule && (
                    <Button
                      style={ { marginRight: 15 } }
                      color={ colors.black }
                      onPress={ handleTechnicalVisitRegister }
                      variant="null"
                      icon={ <Icon size={ 17 } icon={ plantAndHand } color={ colors.black } /> }
                      title={ t('technical visit') }
                      backgroundColor={ colors.white }
                    />
                  )}
                  <Button
                    style={ { marginRight: 15 } }
                    color={ colors.black }
                    variant="null"
                    onPress={ handleEditField }
                    icon={ <Icon size={ 17 } icon={ edit } color={ colors.black } /> }
                    title={ t('edit') }
                    backgroundColor={ colors.white }
                  />
                  <Button
                    onPress={ handleDeleteField }
                    variant="null"
                    icon={ <Icon size={ 17 } icon={ trash } color={ colors.red } /> }
                    backgroundColor={ colors.white }
                    color={ colors.black }
                  />
                </ButtonsContainer>
              )}

              <FieldIndicators setImageIndicator={ setImageIndicator } />
            </>
          }
        >
          {map(plagueReports, (pestReport) => (
            <PinMarker
              key={ pestReport.id }
              coordinate={ pestReport.pinLocation }
              customIcon={ bugMarkerRound }
              size={ 20 }
              onPress={ (e) => {e.stopPropagation(); openReportDetails(pestReport)} }
            />
          ))}

          {imageIndicator ? (
            <Overlay
              image={ imageIndicator }
              bounds={ indicatorsPoints }
            />
          ) : (
            <Polygon points={ field.polygonCoordinates } />
          )}
        </Maps>
      </Item>

      <FieldDetailAccordion />
    </Container>
  )}

export default FieldDetailsScreen
