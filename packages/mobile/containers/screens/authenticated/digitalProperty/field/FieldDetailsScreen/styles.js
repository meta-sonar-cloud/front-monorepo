import styled from 'styled-components/native'


export const ButtonsContainer = styled.View`
  flex-direction: row;
  justify-content: flex-end;
`
