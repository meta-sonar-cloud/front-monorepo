import React, { useRef, useCallback, useState } from 'react'
import { View } from 'react-native'

import { useNavigation, useRoute , useFocusEffect } from '@react-navigation/native'

import isEmpty from 'lodash/isEmpty'

import  I18n, { useT } from '@smartcoop/i18n'
import Button from '@smartcoop/mobile-components/Button'
import Loader from '@smartcoop/mobile-components/Loader'
import FieldForm from '@smartcoop/mobile-containers/forms/digitalProperty/field/FieldForm'
import { useScreenOptions } from '@smartcoop/mobile-containers/hooks/useScreenOptions'
import { Container, Item, Scroll, ButtonsContainer , ScreenHeaderTitleContainer, HeaderTitle } from '@smartcoop/mobile-containers/layouts/AuthenticatedLayout/theme'
import { useSnackbar } from '@smartcoop/snackbar'

const FieldRegisterScreen = () => {
  const activityFormRef = useRef(null)
  const navigation = useNavigation()
  const t = useT()
  const snackbar = useSnackbar()
  const { setOptions } = useScreenOptions()

  const [loading, setLoading] = useState(false)

  const { name: routeName, params: { polygonCoordinates, area, field } } = useRoute()
  const submitForms = useCallback(
    () => {
      // TODO obter valores dinamicamente
      activityFormRef.current.submit()
    },
    []
  )

  const handleGoDrawField = useCallback(
    () => {
      navigation.navigate('DrawField', {
        polygonCoordinates,
        area
      })
    },
    [area, navigation, polygonCoordinates]
  )

  const onSuccess = useCallback(
    (fieldId) => {
      const operationType = routeName === 'Edit' ? 'edited' : 'registered'
      navigation.replace('Field', { screen: 'Details', params: { fieldId } })
      snackbar.success(
        t(`your {this} was ${ operationType }`, {
          howMany: 1,
          gender: 'male',
          this: t('field', { howMany: 1 })
        })
      )
    },
    [navigation, routeName, snackbar, t]
  )

  const handleClose = useCallback(
    () => (
      navigation.goBack()
    ),
    [navigation]
  )

  useFocusEffect(useCallback(
    () => {
      setOptions({
        headerTitle: () => (
          <ScreenHeaderTitleContainer>
            <I18n as={ HeaderTitle }>
              {!isEmpty(field) ? 'field edit' : 'field registration'}
            </I18n>
          </ScreenHeaderTitleContainer>
        )
      })
    },
    [field, setOptions]
  ))

  return loading ? <Loader /> : (
    <Container>
      <Scroll padding={ 0 }>
        <Item
          style={ {
            flex: 1,
            paddingTop: 20,
            paddingBottom: 0
          } }
        >
          <FieldForm
            ref={ activityFormRef }
            onSuccess={ onSuccess }
            handleGoDrawField={ handleGoDrawField }
            polygonCoordinates={ polygonCoordinates }
            handleLoading={ (value) => setLoading(value) }
            area={ area }
            withoutSubmitButton
          />
        </Item>


        <Item>
          <ButtonsContainer>
            <Button
              onPress={ handleClose }
              style={ { flex: 1 } }
              variant="outlined"
              title={ t('cancel') }
            />

            <View style={ { width: '10%' } } />

            <Button
              onPress={ submitForms }
              style={ { flex: 1 } }
              title={ t('save') }
            />
          </ButtonsContainer>
        </Item>
      </Scroll>
    </Container>
  )}

export default FieldRegisterScreen
