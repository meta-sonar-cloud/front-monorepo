import styled from 'styled-components/native'

import colors from '@smartcoop/styles/colors'

export const ButtonsContainer = styled.View`
  flex-direction: row;
  justify-content: flex-end;
`

export const ContainerFooterButtons = styled.View`
  padding-left: 25px;
  padding-right: 25px;
  flex-direction: row;
  justify-content: space-between;
  position: absolute;
  bottom: 8%;
  width: 100%;
`

export const ContainerWrapper = styled.View`
  flex-direction: column;
  padding: 0 10px;
`

export const ContainerGrowing = styled.View`
  flex-direction: row;
  justify-content: space-between;
  margin-bottom: 10px;
  padding: 0 10px;
`

export const TitleGrowing = styled.Text`
  font-size: 18px;
  font-weight: 700;
  color: ${ colors.black };
  margin-bottom: 5%;
`

export const CropText = styled.Text`
  font-size: 18px;
  font-weight: 600;
  color: ${ colors.black };
  padding: 5px 0;
`

export const CultivationText = styled.Text`
  font-size: 16px;
  color: ${ colors.black };
  margin-bottom: 5%;
`

export const CultivationDate = styled.Text`
  font-size: 16px;
  font-weight: 600;
  color: ${ colors.black };
  padding: 5px 0;
`

export const LeftContent = styled.View`
`

export const RightContent = styled.View`
  align-items: flex-end;
`

export const CropManagementTitle = styled.Text`
  padding-left: 10px;
  font-weight: bold;
  color: ${ colors.black };
`

export const ButtonRegisterManagement = styled.View`
  position: absolute;
  bottom: 0;
  right: 0;
  z-index: 2;
  padding-bottom: 20px;
`

export const ManagementItem = styled.View`
  padding: 7px 10px;
  border-color: ${ colors.lightGrey };
  border-top-width: ${ (first) => first ? 0 : 1 }px;
  border-bottom-width: ${ (last) => last ? 1 : 0 }px;
`
