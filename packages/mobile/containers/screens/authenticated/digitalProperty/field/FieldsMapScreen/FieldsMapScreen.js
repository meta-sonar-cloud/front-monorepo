import React, { useCallback, useEffect, useMemo } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import { useNavigation } from '@react-navigation/native'

import debounce from 'lodash/debounce'
import isEmpty from 'lodash/isEmpty'
import map from 'lodash/map'
import reduce from 'lodash/reduce'

import Maps from '@smartcoop/mobile-components/Maps'
import Polygon from '@smartcoop/mobile-components/Maps/components/Polygon'
import PropertyFieldsListAccordion from '@smartcoop/mobile-containers/fragments/PropertyFieldsListAccordion'
import { Container, Item } from '@smartcoop/mobile-containers/layouts/AuthenticatedLayout/theme'
import { FieldActions } from '@smartcoop/stores/field'
import { selectFields } from '@smartcoop/stores/field/selectorField'
import { selectCurrentProperty } from '@smartcoop/stores/property/selectorProperty'
import colors from '@smartcoop/styles/colors'
import  { getCenterCoordinates } from '@smartcoop/utils/maps'

const FieldsMapScreen = () => {
  const navigation = useNavigation()

  const dispatch = useCallback(useDispatch(), [])

  const fields = useSelector(selectFields)
  const currentProperty = useSelector(selectCurrentProperty)

  const allPolygonCoordinates = useMemo(
    () => reduce(
      fields,
      (acc, field) => [
        ...acc,
        ...(field?.polygonCoordinates || [])
      ],
      []
    ),
    [fields]
  )

  const region = useMemo(
    () =>
      !isEmpty(allPolygonCoordinates)
        ? getCenterCoordinates(allPolygonCoordinates)
        : currentProperty.geolocalization,
    [allPolygonCoordinates, currentProperty.geolocalization]
  )

  const handleFieldNavitagion = useCallback(
    debounce(
      (field) => {
        dispatch(FieldActions.setCurrentField(field))
        navigation.navigate('Field', { screen: 'Details', params: { fieldId: field.id } })
      },
      50
    ),
    [dispatch, navigation]
  )

  const handleFieldPressed = useCallback(
    (field) => handleFieldNavitagion(field),
    [handleFieldNavitagion]
  )

  const loadFields = useCallback(
    () => {
      if (!isEmpty(currentProperty)) {
        dispatch(FieldActions.loadFields())
      } else {
        // snackbar.error(t('{this} is required'))
      }
    },
    [currentProperty, dispatch]
  )

  useEffect(() => {
    loadFields()
  }, [loadFields])

  return (
    <Container>
      <Item
        style={ {
          flex: 1,
          paddingTop: 20,
          paddingBottom: 0
        } }
      >
        <Maps region={ region } mapType="hybrid">
          {map(fields, (field) => (
            <Polygon
              key={ field.id }
              points={ field.polygonCoordinates }
              color={ colors.white }
              onPress={ () => handleFieldPressed(field) }
            />
          ))}
        </Maps>
      </Item>

      <PropertyFieldsListAccordion />
    </Container>
  )
}

export default FieldsMapScreen


