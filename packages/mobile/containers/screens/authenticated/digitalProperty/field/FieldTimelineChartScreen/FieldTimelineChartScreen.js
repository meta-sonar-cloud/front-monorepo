/* eslint-disable react/no-this-in-sfc */
import React, { useCallback, useState, useEffect } from 'react'
import { useSelector } from 'react-redux'

import { useFocusEffect } from '@react-navigation/native'
import moment from 'moment/moment'

import I18n, { useT } from '@smartcoop/i18n'
import Chart from '@smartcoop/mobile-components/Chart'
import InputDateRange from '@smartcoop/mobile-components/InputDateRange'
import { useScreenOptions } from '@smartcoop/mobile-containers/hooks/useScreenOptions'
import { LandscapeContainer, Container, Item , ScreenHeaderTitleContainer, HeaderTitle } from '@smartcoop/mobile-containers/layouts/AuthenticatedLayout/theme'
import { selectCurrentField } from '@smartcoop/stores/field/selectorField'
import { createFieldTimelineChartOptions } from '@smartcoop/utils/charts'
import { momentBackDateFormat, momentFriendlyShortDateFormat } from '@smartcoop/utils/dates'

const FieldTimelineChartScreen = () => {
  const t = useT()
  const { setOptions } = useScreenOptions()

  const field = useSelector(selectCurrentField)

  const [chartOptions, setChartOptions] = useState({})
  const [dates, setDates] = useState({
    from: moment().startOf('year').format(momentBackDateFormat),
    to: null
  })

  const getChartOptions = useCallback(
    async (firstTime = false) => {
      const options = await createFieldTimelineChartOptions({
        fieldId: field.id,
        dates: firstTime ? { from: null, to: null } : dates,
        t
      })
      setChartOptions(options)
      if(firstTime) {
        setDates({
          from: moment(options.xAxis.categories[0], momentFriendlyShortDateFormat).format(momentBackDateFormat),
          to: moment(options.xAxis.categories.slice(-1)[0], momentFriendlyShortDateFormat).format(momentBackDateFormat)
        })
      }
    },
    [dates, field.id, t]
  )

  useEffect(
    () => {
      if (dates.to) {
        getChartOptions()
      }
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [dates.to]
  )

  useEffect(
    () => {
      getChartOptions(true)
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    []
  )


  useFocusEffect(useCallback(
    () => {
      setOptions({
        headerTitle: () => (
          <ScreenHeaderTitleContainer>
            <I18n as={ HeaderTitle }>
              field timeline
            </I18n>
          </ScreenHeaderTitleContainer>
        )
      })
    },
    [setOptions]
  ))

  return (
    <Container style={ { alignItems: 'center', justifyContent: 'center' } }>
      <LandscapeContainer>
        <Item>
          <InputDateRange
            detached
            label={ t('period') }
            name="period"
            value={ dates }
            minWidth={ 250 }
            onChange={ setDates }
            style={ { marginBottom: 0 } }
            landscape
          />
        </Item>
        <Chart options={ chartOptions } />
      </LandscapeContainer>
    </Container>
  )
}

export default FieldTimelineChartScreen
