import styled from 'styled-components'

import colors from '@smartcoop/styles/colors'

export const Container = styled.View`
  padding: 25px;
  display: flex;
  align-items: center;
  justify-content: center;
  height: 100%;
`

export const Text = styled.Text`
margin-top: 25;
  font-size: 25;
  color: ${ colors.text };
  text-align: center;
`

export const LinkText = styled.Text`
  color: ${ colors.secondary };
  font-size: 25;
  font-weight: bold;
`

export const Circle = styled.View`
  background-color: ${ colors.secondary };
  padding: 25px;
  border-radius: 100px;
`
