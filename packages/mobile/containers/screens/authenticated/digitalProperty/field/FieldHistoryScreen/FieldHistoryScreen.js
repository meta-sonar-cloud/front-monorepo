import React, { useCallback } from 'react'
import { Linking } from 'react-native'

import { useFocusEffect } from '@react-navigation/native'

import I18n from '@smartcoop/i18n'
import { plantAndHand } from '@smartcoop/icons'
import Icon from '@smartcoop/mobile-components/Icon'
import { useScreenOptions } from '@smartcoop/mobile-containers/hooks/useScreenOptions'
import { ScreenHeaderTitleContainer, HeaderTitle } from '@smartcoop/mobile-containers/layouts/AuthenticatedLayout/theme'
import { colors } from '@smartcoop/styles'

import { Container, Text, LinkText, Circle } from './styles'

const FieldHistoryScreen = () => {
  const { setOptions } = useScreenOptions()

  useFocusEffect(useCallback(
    () => {
      setOptions({
        headerTitle: () => (
          <ScreenHeaderTitleContainer>
            <I18n as={ HeaderTitle }>
              field history
            </I18n>
          </ScreenHeaderTitleContainer>
        )
      })
    },
    [setOptions]
  ))

  return (
    <Container>
      <Circle>
        <Icon icon={ plantAndHand } color={ colors.blackLight } size={ 90 } />
      </Circle>
      <Text>
        <I18n>history is not done</I18n>
      </Text>

      <LinkText onPress={ () => Linking.openURL('https://app.smart.coop.br') }>https://app.smart.coop.br</LinkText>
    </Container>
  )}

export default FieldHistoryScreen
