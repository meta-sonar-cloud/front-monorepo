import React from 'react'

import { useNavigation } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'

import { useScreenOptions, withScreenOptions } from '@smartcoop/mobile-containers/hooks/useScreenOptions'

import FieldDetailsScreen from './FieldDetailsScreen'
import FieldDrawScreen from './FieldDrawScreen/FieldDrawScreen'
import FieldHistoryScreen from './FieldHistoryScreen'
import FieldRegisterScreen from './FieldRegisterScreen'
import FieldTimelineChartScreen from './FieldTimelineChartScreen'

const Stack = createStackNavigator()

const FieldScreenRouter = () => {
  const navigation = useNavigation()
  const { options } = useScreenOptions(navigation)

  return (
    <Stack.Navigator screenOptions={ options } mode="modal">
      <Stack.Screen
        name="Register"
        component={ FieldRegisterScreen }
      />

      <Stack.Screen
        name="DrawField"
        component={ FieldDrawScreen }
      />

      <Stack.Screen
        name="Details"
        component={ FieldDetailsScreen }
      />

      <Stack.Screen
        name="TimelineChart"
        component={ FieldTimelineChartScreen }
      />

      <Stack.Screen
        name="History"
        component={ FieldHistoryScreen }
      />
    </Stack.Navigator>
  )
}

export default withScreenOptions(FieldScreenRouter)
