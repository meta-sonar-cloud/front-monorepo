import React, { useEffect, useCallback, useState, useRef, useMemo } from 'react'
import { View } from 'react-native'
import { useDispatch, useSelector } from 'react-redux'

import { useT } from '@meta-react/i18n'
import { useNavigation, useFocusEffect } from '@react-navigation/native'
import moment from 'moment'

import { isEmpty, values } from 'lodash'

import { useDialog } from '@smartcoop/dialog'
import AnimalBirthItem from '@smartcoop/mobile-components/AnimalBirthItem'
import Button from '@smartcoop/mobile-components/Button'
import FilterButton from '@smartcoop/mobile-components/FilterButton'
import List from '@smartcoop/mobile-components/List'
import FilterAnimalBirthModal from '@smartcoop/mobile-containers/modals/dairyFarm/FilterAnimalBirthModal'
import { AnimalActions } from '@smartcoop/stores/animal'
import { selectCurrentAnimal } from '@smartcoop/stores/animal/selectorAnimal'
import { AnimalBirthActions } from '@smartcoop/stores/animalBirth'
import { selectAnimalBirths } from '@smartcoop/stores/animalBirth/selectorAnimalBirth'
import { InseminationActions } from '@smartcoop/stores/insemination'
import { LotActions } from '@smartcoop/stores/lot'
import { selectLots } from '@smartcoop/stores/lot/selectorLot'
import { colors } from '@smartcoop/styles'
import { momentBackDateFormat, momentFriendlyDateFormat } from '@smartcoop/utils/dates'

import { Top, ButtonContainer } from './styles'

const ListAnimalBirth = () => {
  const { createDialog } = useDialog()
  const t = useT()
  const dispatch = useCallback(useDispatch(), [])
  const [refresh, setRefresh] = useState(true)
  const animalBirths = useSelector(selectAnimalBirths)
  const [filters, setFilters] = useState({})
  const currentAnimal = useSelector(selectCurrentAnimal)
  const mounted = useRef(false)
  const navigation = useNavigation()
  const lots = useSelector(selectLots)

  const deps = useMemo(
    () => [filters, refresh],
    [filters, refresh]
  )

  const registerDisabled = useMemo(
    () => (
      currentAnimal?.animalStatus?.name !== 'PEV' &&
      currentAnimal?.animalStatus?.name !== 'Prenha'
    ),[currentAnimal]
  )

  const handleSetFilters = useCallback(
    (filterValues) => {
      setFilters({
        ...filterValues,
        q: filterValues?.name ?? null
      })
    }, []
  )

  const openFilterModal = useCallback(
    () => {
      createDialog({
        id: 'filter-fields',
        Component: FilterAnimalBirthModal,
        props: {
          onSubmit: handleSetFilters,
          filters
        }
      })
    },
    [createDialog, filters, handleSetFilters]
  )

  const loadAnimalBirths = useCallback(
    (page, onSuccess, onError) => {
      dispatch(AnimalBirthActions.loadAnimalBirths(
        { ...filters, page },
        onSuccess,
        onError
      ))
    },
    [dispatch, filters]
  )

  const renderItem = useCallback(
    ({ item }) => (
      <AnimalBirthItem
        animalBirth={ item }
        onEdit={ () => {navigation.navigate('DairyFarm', { screen: 'RegisterAnimalBirth', params: {
          animalBirth: {
            ...item,
            occurrenceDate: moment(item.occurrenceDate, momentBackDateFormat),
            expectedDate: moment(item.expectedDate, momentBackDateFormat).format(momentFriendlyDateFormat)
          }
        } })} }
      />
    ),
    [navigation]
  )

  useFocusEffect(
    useCallback(() => {
      if(!mounted.current) {
        mounted.current = true
      } else {
        setRefresh(old => !old)
      }
    }, [])
  )


  useEffect(() => {
    if(isEmpty(lots)) {
      dispatch(LotActions.loadLots(
        {}
      ))
    }
  }, [dispatch, lots])

  useEffect(
    () => {
      dispatch(AnimalActions.loadCurrentAnimal())
    },[dispatch]
  )

  useEffect(
    () => {
      dispatch(InseminationActions.loadCurrentInsemination())
    },[dispatch]
  )


  return (
    <>
      <Top>
        <FilterButton
          id="animalBirth-filter-button"
          onPress={ openFilterModal }
          isActive={ !values(filters).every(isEmpty) }
          style={ { flex: 1 } }
        />
      </Top>
      <List
        data={ animalBirths }
        renderItem={ renderItem }
        onListLoad={ loadAnimalBirths }
        deps={ deps }
        style={ { marginBottom: 100 } }
      />
      <ButtonContainer>
        <View>
          <Button
            itemKey="code"
            id="register-animalBirth"
            backgroundColor={ colors.secondary }
            color={ colors.text }
            textStyle={ { fontSize: 14 } }
            onPress={ () => {navigation.navigate('DairyFarm', { screen: 'RegisterAnimalBirth', params: { animalBirth: {} } })} }
            title={ t('register {this}', { this: t('calving') }) }
            disabled={ registerDisabled }
          />
        </View>
      </ButtonContainer>
    </>
  )
}

export default ListAnimalBirth
