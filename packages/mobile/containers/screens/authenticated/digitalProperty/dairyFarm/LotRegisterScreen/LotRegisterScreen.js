import React, { useRef, useCallback, useMemo } from 'react'
import { View } from 'react-native'
import { useSelector, useDispatch } from 'react-redux'

import { useFocusEffect , useRoute, useNavigation } from '@react-navigation/native'

import isEmpty from 'lodash/isEmpty'

import I18n, { useT } from '@smartcoop/i18n'
import Button from '@smartcoop/mobile-components/Button'
import RegisterLotForm from '@smartcoop/mobile-containers/forms/digitalProperty/dairyFarm/RegisterLotForm'
import { useScreenOptions } from '@smartcoop/mobile-containers/hooks/useScreenOptions'
import AuthenticatedLayout from '@smartcoop/mobile-containers/layouts/AuthenticatedLayout'
import { Container, Item, Scroll, ButtonsContainer , ScreenHeaderTitleContainer, HeaderTitle } from '@smartcoop/mobile-containers/layouts/AuthenticatedLayout/theme'
import { useSnackbar } from '@smartcoop/snackbar'
import { LotActions } from '@smartcoop/stores/lot'
import { selectCurrentProperty } from '@smartcoop/stores/property/selectorProperty'

const LotRegisterScreen = () => {
  const formRef = useRef(null)
  const snackbar = useSnackbar()
  const navigation = useNavigation()
  const t = useT()
  const dispatch = useCallback(useDispatch(), [])
  const route = useRoute()
  const { setOptions } = useScreenOptions()

  const { lot } = route?.params ?? {}

  const currentProperty = useSelector(selectCurrentProperty)
  const isEditing = useMemo(
    () => (!isEmpty(lot?.id)),[lot]
  )
  const defaultValues = useMemo(
    () => (
      {
        code: '',
        name: '',
        property: {
          id: currentProperty.id
        },
        ...lot
      }
    ), [currentProperty.id, lot]
  )
  const submit = useCallback(
    () => {
      formRef.current.submit()
    },
    []
  )

  const handleClose = useCallback(
    () => {
      navigation.goBack()
    },
    [navigation]
  )

  const onSuccess = useCallback(
    () => {
      snackbar.success(
        t(`your {this} was ${  isEditing ? 'edited' :'registered' }`, {
          howMany: 1,
          gender: 'male',
          this: t('lot')
        })
      )
      handleClose()
    },
    [handleClose, isEditing, snackbar, t]
  )

  const submitForms = useCallback(
    (data) => {
      dispatch(LotActions.saveLot(
        {
          ...lot,
          ...data
        },
        () => LotActions.loadLots()
      ))
      onSuccess()
    }, [dispatch, lot, onSuccess]
  )

  useFocusEffect(useCallback(
    () => {
      setOptions({
        headerTitle: () => (
          <ScreenHeaderTitleContainer>
            <I18n as={ HeaderTitle }>
              lot registration
            </I18n>
          </ScreenHeaderTitleContainer>
        )
      })
    },
    [setOptions]
  ))

  return (
    <AuthenticatedLayout>
      <>
        <Container>
          <Scroll padding={ 0 }>
            <Item>
              <RegisterLotForm
                ref={ formRef }
                onSuccess={ onSuccess }
                onSubmit={ submitForms }
                defaultValues={ defaultValues }
                withoutSubmitButton
              />
            </Item>
          </Scroll>
          <Item>
            <ButtonsContainer style={ { paddingTop: 10 } }>
              <Button
                onPress={ handleClose }
                style={ { flex: 1 } }
                variant="outlined"
                title={ t('cancel') }
              />
              <View style={ { width: '10%' } } />
              <Button
                onPress={ submit }
                style={ { flex: 1 } }
                title={ t('save') }
              />
            </ButtonsContainer>
          </Item>
        </Container>
      </>
    </AuthenticatedLayout>
  )
}

export default LotRegisterScreen
