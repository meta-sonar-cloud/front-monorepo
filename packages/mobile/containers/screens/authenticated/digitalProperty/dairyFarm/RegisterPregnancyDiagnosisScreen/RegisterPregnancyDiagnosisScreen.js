import React, { useRef, useCallback, useMemo, useState } from 'react'
import { View } from 'react-native'
import { useSelector, useDispatch } from 'react-redux'

import {
  useRoute,
  useNavigation,
  useFocusEffect
} from '@react-navigation/native'

import { isNil } from 'lodash'
import isEmpty from 'lodash/isEmpty'
import toString from 'lodash/toString'

import I18n, { useT } from '@smartcoop/i18n'
import Button from '@smartcoop/mobile-components/Button'
import Loader from '@smartcoop/mobile-components/Loader'
import RegisterPregnancyDiagnosisForm from '@smartcoop/mobile-containers/forms/digitalProperty/dairyFarm/RegisterPregnancyDiagnosisForm'
import { useScreenOptions } from '@smartcoop/mobile-containers/hooks/useScreenOptions'
import AuthenticatedLayout from '@smartcoop/mobile-containers/layouts/AuthenticatedLayout'
import {
  Container,
  Item,
  Scroll,
  ButtonsContainer,
  ScreenHeaderTitleContainer,
  HeaderTitle
} from '@smartcoop/mobile-containers/layouts/AuthenticatedLayout/theme'
import { useSnackbar } from '@smartcoop/snackbar'
import { selectCurrentAnimal } from '@smartcoop/stores/animal/selectorAnimal'
import { AnimalPregnancyDiagnosticsActions } from '@smartcoop/stores/animalPregnancyDiagnostics'

const RegisterPregnancyDiagnosisScreen = () => {
  const formRef = useRef(null)
  const snackbar = useSnackbar()
  const dispatch = useCallback(useDispatch(), [])
  const t = useT()
  const currentAnimal = useSelector(selectCurrentAnimal)
  const navigation = useNavigation()
  const { setOptions } = useScreenOptions()

  const route = useRoute()
  const { pregnancyDiagnosis } = route?.params ?? {}

  const [isLoading, setIsLoading] = useState(false)

  const isEditing = useMemo(() => !isEmpty(toString(pregnancyDiagnosis?.id)), [
    pregnancyDiagnosis
  ])

  const pregnancyDiagnosisId = useMemo(() => isEmpty(pregnancyDiagnosis) ? null : pregnancyDiagnosis.id, [pregnancyDiagnosis])

  const defaultValues = useMemo(
    () => ({
      earring: currentAnimal?.earring?.earringCode,
      animalId: currentAnimal?.id,
      ...pregnancyDiagnosis
    }),
    [currentAnimal.earring.earringCode, currentAnimal.id, pregnancyDiagnosis]
  )

  const handleClose = useCallback(() => {
    navigation.goBack()
  }, [navigation])

  const onSuccess = useCallback(() => {
    setIsLoading(false)
    snackbar.success(
      t(`your {this} was ${ isEditing ? 'edited' : 'registered' }`, {
        howMany: 1,
        gender: 'female',
        this: t('diagnosis', { howMany: 1 })
      })
    )
    handleClose()
  }, [handleClose, isEditing, snackbar, t])

  const handleSubmit = useCallback(
    (data) => {
      setIsLoading(true)
      const object = { ...data }

      if (!isNil(pregnancyDiagnosisId)) {
        object.id = pregnancyDiagnosisId
      }

      dispatch(
        AnimalPregnancyDiagnosticsActions.savePregnancyDiagnostic(
          object,
          (success) => onSuccess(success),
          (err) => {
            snackbar.error(err.message)

            setIsLoading(false)
          }
        )
      )
    },
    [dispatch, onSuccess, pregnancyDiagnosisId, snackbar]
  )

  const submit = useCallback(() => {
    formRef.current.submit()
  }, [])

  useFocusEffect(
    useCallback(() => {
      setOptions({
        headerTitle: () => (
          <ScreenHeaderTitleContainer>
            <I18n as={ HeaderTitle } params={ { howMany: 1 } }>
              diagnosis
            </I18n>
          </ScreenHeaderTitleContainer>
        )
      })
    }, [setOptions])
  )

  return (
    <AuthenticatedLayout>
      <>
        <Container>
          {isLoading ? (
            <Loader />
          ) : (
            <>
              <Scroll padding={ 0 }>
                <Item>
                  <RegisterPregnancyDiagnosisForm
                    ref={ formRef }
                    onSuccess={ onSuccess }
                    onSubmit={ handleSubmit }
                    defaultValues={ defaultValues }
                    withoutSubmitButton
                  />
                </Item>
              </Scroll>
              <Item>
                <ButtonsContainer style={ { paddingTop: 10 } }>
                  <Button
                    onPress={ handleClose }
                    style={ { flex: 1 } }
                    variant="outlined"
                    title={ t('cancel') }
                  />
                  <View style={ { width: '10%' } } />
                  <Button
                    onPress={ submit }
                    style={ { flex: 1 } }
                    title={ t('save') }
                  />
                </ButtonsContainer>
              </Item>
            </>
          )}
        </Container>
      </>
    </AuthenticatedLayout>
  )
}

export default RegisterPregnancyDiagnosisScreen
