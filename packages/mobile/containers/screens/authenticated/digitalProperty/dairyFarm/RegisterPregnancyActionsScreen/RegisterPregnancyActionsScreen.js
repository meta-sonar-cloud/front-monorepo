import React, { useState, useRef, useCallback, useMemo } from 'react'
import { View } from 'react-native'
import { useSelector, useDispatch } from 'react-redux'

import {
  useRoute,
  useNavigation,
  useFocusEffect
} from '@react-navigation/native'

import isEmpty from 'lodash/isEmpty'
import toString from 'lodash/toString'

import I18n, { useT } from '@smartcoop/i18n'
import Button from '@smartcoop/mobile-components/Button'
import Loader from '@smartcoop/mobile-components/Loader'
import RegisterPregnancyActionsForm from '@smartcoop/mobile-containers/forms/digitalProperty/dairyFarm/RegisterPregnancyActionsForm'
import { useScreenOptions } from '@smartcoop/mobile-containers/hooks/useScreenOptions'
import AuthenticatedLayout from '@smartcoop/mobile-containers/layouts/AuthenticatedLayout'
import {
  Container,
  Item,
  Scroll,
  ButtonsContainer,
  ScreenHeaderTitleContainer,
  HeaderTitle
} from '@smartcoop/mobile-containers/layouts/AuthenticatedLayout/theme'
import { useSnackbar } from '@smartcoop/snackbar'
import { selectCurrentAnimal } from '@smartcoop/stores/animal/selectorAnimal'
import { AnimalPregnancyActionsActions } from '@smartcoop/stores/animalPregnancyActions'

const RegisterPregnancyActionsScreen = () => {
  const formRef = useRef(null)
  const snackbar = useSnackbar()
  const dispatch = useCallback(useDispatch(), [])

  const t = useT()
  const route = useRoute()
  const navigation = useNavigation()
  const { setOptions } = useScreenOptions()

  const currentAnimal = useSelector(selectCurrentAnimal)

  const { pregnancyAction } = route?.params ?? {}

  const [isLoading, setIsLoading] = useState(false)

  const isEditing = useMemo(() => !isEmpty(toString(pregnancyAction?.id)), [
    pregnancyAction
  ])

  const defaultValues = useMemo(
    () => ({
      earring: currentAnimal?.earring?.earringCode,
      animalId: currentAnimal?.id,
      ...pregnancyAction
    }),
    [currentAnimal, pregnancyAction]
  )

  const handleClose = useCallback(() => {
    navigation.goBack()
  }, [navigation])

  const onSuccess = useCallback(() => {
    snackbar.success(
      t(`your {this} was ${ isEditing ? 'edited' : 'registered' }`, {
        howMany: 1,
        gender: 'female',
        this: t('action')
      })
    )
    handleClose()
  }, [handleClose, isEditing, snackbar, t])

  const handleSubmit = useCallback(
    (data) => {
      setIsLoading(true)
      dispatch(
        AnimalPregnancyActionsActions.saveDrying(
          {
            id: isEmpty(pregnancyAction) ? null : pregnancyAction.id,
            ...data
          },
          onSuccess,
          () => setIsLoading(false)
        )
      )
    },
    [dispatch, onSuccess, pregnancyAction]
  )

  const submit = useCallback(() => {
    formRef.current.submit()
  }, [])

  useFocusEffect(
    useCallback(() => {
      setOptions({
        headerTitle: () => (
          <ScreenHeaderTitleContainer>
            <I18n as={ HeaderTitle }>other actions</I18n>
          </ScreenHeaderTitleContainer>
        )
      })
    }, [setOptions])
  )

  return (
    <AuthenticatedLayout>
      <>
        <Container>
          {isLoading ? (
            <Loader />
          ) : (
            <>
              <Scroll padding={ 0 }>
                <Item>
                  <RegisterPregnancyActionsForm
                    ref={ formRef }
                    onSuccess={ onSuccess }
                    onSubmit={ handleSubmit }
                    defaultValues={ defaultValues }
                    withoutSubmitButton
                  />
                </Item>
              </Scroll>
              <Item>
                <ButtonsContainer style={ { paddingTop: 10 } }>
                  <Button
                    onPress={ handleClose }
                    style={ { flex: 1 } }
                    variant="outlined"
                    title={ t('cancel') }
                  />
                  <View style={ { width: '10%' } } />
                  <Button
                    onPress={ submit }
                    style={ { flex: 1 } }
                    title={ t('save') }
                  />
                </ButtonsContainer>
              </Item>
            </>
          )}
        </Container>
      </>
    </AuthenticatedLayout>
  )
}

export default RegisterPregnancyActionsScreen
