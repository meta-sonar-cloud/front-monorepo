import styled from 'styled-components'

export const Actions = styled.View`
  display: flex;
  flex-direction: row;
  justify-content: flex-end;
  margin-bottom: 10px;
`

export const Bold = styled.Text`
  margin: 0;
  font-weight: 700;
`

export const Date = styled(Bold)`
  font-size: 16px;
`

export const Line = styled.View`
  display: flex;
  flex-direction: row;
`

export const Value = styled.View`
  display: flex;
`

export const Top = styled(Line)`
  justify-content: space-between;
  margin-top: 4px;
`

export const Text = styled.Text`
  margin-right: 10px;
`
