import React, { useState, useMemo } from 'react'
import { TouchableOpacity } from 'react-native'
import Collapsible from 'react-native-collapsible'

import moment from 'moment/moment'
import PropTypes from 'prop-types'

import isEmpty from 'lodash/isEmpty'
import map from 'lodash/map'

import I18n from '@smartcoop/i18n'
import {  arrowDown, arrowUp, pencil, trash } from '@smartcoop/icons'
import Button from '@smartcoop/mobile-components/Button'
import Divider from '@smartcoop/mobile-components/Divider'
import Icon from '@smartcoop/mobile-components/Icon'
import { Item } from '@smartcoop/mobile-containers/layouts/AuthenticatedLayout/theme'
import { colors } from '@smartcoop/styles'
import { momentBackDateFormat, momentFriendlyDateFormat } from '@smartcoop/utils/dates'
import { formatNumber } from '@smartcoop/utils/formatters'

import {
  Text,
  Date,
  Value,
  Top,
  Line,
  Bold,
  Actions
} from './styles'

const MilkQualityItem = (props) => {
  const {
    data,
    onEdit,
    onDelete
  } = props
  const [isCollapsed, setIsCollapsed] = useState(true)
  const arrowIcon = useMemo(
    () => (isCollapsed? arrowDown : arrowUp),[isCollapsed]
  )

  const isCooperative = useMemo(
    () => !isEmpty(data.organizationId),
    [data]
  )

  return (
    <Item>
      <TouchableOpacity onPress={ () => setIsCollapsed(old => !old) }>
        <Top>
          <Date>{moment(data.date, momentBackDateFormat).format(momentFriendlyDateFormat)}</Date>
          <Icon icon={ arrowIcon } />
        </Top>
      </TouchableOpacity>
      <Collapsible collapsed={ isCollapsed }>
        <Divider />
        <Actions>
          <Button
            variant="outlined"
            style={ { marginRight: 5, padding: 8, borderColor: colors.lightGrey } }
            icon={ <Icon size={ 14 } icon={ pencil } color={ colors.black } /> }
            backgroundColor={ colors.white }
            onPress={ onEdit }
            disabled={ isCooperative }
          />
          <Button
            variant="outlined"
            style={ { padding: 8, borderColor: colors.lightGrey } }
            icon={ <Icon size={ 14 } icon={ trash } color={ colors.red } /> }
            backgroundColor={ colors.white }
            onPress={ onDelete }
            disabled={ isCooperative }
          />
        </Actions>
        {map(data.groupedItems, item => (
          <>
            <Value>
              <Top>
                <Bold><I18n>{item.type}</I18n></Bold>
              </Top>
              <Line>
                <Text>
                  <I18n>result</I18n>:
                </Text>
                <Bold>
                  {item.result ? formatNumber(item.result) : '-' }
                </Bold>
              </Line>
              <Line>
                <Text>
                  <I18n>limit</I18n>:
                </Text>
                <Bold>
                  {item.limit ?? '-'}
                </Bold>
              </Line>
            </Value>
            <Divider/>
          </>
        ))}
      </Collapsible>
    </Item>
  )
}

MilkQualityItem.propTypes = {
  data: PropTypes.object,
  onEdit: PropTypes.func,
  onDelete: PropTypes.func
}

MilkQualityItem.defaultProps = {
  data: {},
  onEdit: () => {},
  onDelete: () => {}
}

export default MilkQualityItem
