import React, { useCallback, useMemo, useState } from 'react'
import { SafeAreaView, View } from 'react-native'
import { useDispatch, useSelector } from 'react-redux'

import { useFocusEffect , useNavigation } from '@react-navigation/native'

import isEmpty from 'lodash/isEmpty'

import { useDialog } from '@smartcoop/dialog'
import I18n, { useT } from '@smartcoop/i18n'
import {  emptyFilter, filter } from '@smartcoop/icons'
import Button from '@smartcoop/mobile-components/Button'
import EmptyState from '@smartcoop/mobile-components/EmptyState'
import Icon from '@smartcoop/mobile-components/Icon'
import List from '@smartcoop/mobile-components/List'
import ConfirmModal from '@smartcoop/mobile-components/Modal/ConfirmModal'
import { useScreenOptions } from '@smartcoop/mobile-containers/hooks/useScreenOptions'
import AuthenticatedLayout from '@smartcoop/mobile-containers/layouts/AuthenticatedLayout'
import { Container , ScreenHeaderTitleContainer, HeaderTitle } from '@smartcoop/mobile-containers/layouts/AuthenticatedLayout/theme'
import FilterFieldsMilkQualityModal from '@smartcoop/mobile-containers/modals/FilterFieldsMilkQualityModal/FilterFieldsMilkQualityModal'
import { useSnackbar } from '@smartcoop/snackbar'
import { DairyFarmActions } from '@smartcoop/stores/dairyFarm'
import { selectMilkQualities } from '@smartcoop/stores/dairyFarm/selectorDairyFarm'
import { selectCurrentProperty } from '@smartcoop/stores/property/selectorProperty'
import { selectUserCanWrite } from '@smartcoop/stores/user/selectorUser'
import colors from '@smartcoop/styles/colors'

import MilkQualityItem from './MilkQualityItem/MilkQualityItem'
import {
  ButtonContainer,
  SearchContainer,
  WarningDiv
} from './styles'

const MilkQualityListScreen = () => {
  const navigation = useNavigation()
  const { createDialog } = useDialog()
  const snackbar = useSnackbar()
  const { setOptions } = useScreenOptions()

  const [forceListUpdate, setForceListUpdate] = useState()
  const [filters, setFilters] = useState({})

  const currentProperty = useSelector(selectCurrentProperty)
  const milkQualities = useSelector(selectMilkQualities)
  const userWrite = useSelector(selectUserCanWrite)

  const dairyFarm = useMemo(() => !isEmpty(currentProperty?.dairyFarm) && currentProperty?.dairyFarm[0], [currentProperty])

  const isCooperative = useMemo(
    () => !isEmpty(dairyFarm.organizationId),
    [dairyFarm]
  )

  const t = useT()
  const handleParams = useCallback(
    (values) => (
      Object.keys(values)
        .filter(
          (key) => typeof values[key] === 'boolean' || values[key]
        )
        .reduce(
          (prev, curr) => ({ ...prev, [curr]: values[curr] }), {}
        )
    ),
    []
  )
  const dispatch = useCallback(useDispatch(), [])

  const params = useMemo(
    () => {
      const filterParams = {
        ...filters
      }
      return handleParams(filterParams)
    },
    [filters, handleParams]
  )

  const deps = useMemo(
    () => [forceListUpdate, params],
    [forceListUpdate, params]
  )

  const onListLoad = useCallback(
    (page, onSuccess, onError) => {
      dispatch(DairyFarmActions.loadMilkQualities(
        { ...params, page },
        onSuccess,
        onError
      ))
    },
    [dispatch, params]
  )

  const onDelete = useCallback(
    ({ id }) => {
      createDialog({
        id: 'delete-field',
        Component: ConfirmModal,
        props: {
          onConfirm: () => {
            dispatch(
              DairyFarmActions.deleteMilkQuality(
                id,
                () => setForceListUpdate(old => !old),
                () => setForceListUpdate(old => !old)
              )
            )
            snackbar.success(
              t('your {this} was deleted', {
                howMany: 1,
                gender: 'female',
                this: t('analysis', { howMany: 1 })
              })
            )
          },
          message: t('are you sure you want to delete the {this}?', {
            howMany: 1,
            gender: 'female',
            this: t('analysis', { howMany: 1 })
          }),
          buttonText: t('yes'),
          underlinedText: t('no')
        }
      })
    },
    [createDialog, dispatch, snackbar, t]
  )

  const renderItem = useCallback(
    ({ item }) => (
      <MilkQualityItem
        data={ item }
        key={ item.id }
        onDelete={ () => onDelete(item) }
        onEdit={ () => navigation.navigate('DairyFarm', { screen: 'RegisterQuality', params: { milkQuality: { ...item } } }) }
      />
    ),
    [navigation, onDelete]
  )

  const handleFilter = useCallback(
    (values) => {
      setFilters(values)
    },
    []
  )

  const openFilterModal = useCallback(
    () => {
      createDialog({
        id: 'filter-fields',
        Component: FilterFieldsMilkQualityModal,
        props: {
          onConfirm: handleFilter,
          filters
        }
      })
    },
    [createDialog, filters, handleFilter]
  )

  useFocusEffect(
    useCallback(() => {
      setForceListUpdate(old => !old)
    }, [])
  )

  useFocusEffect(useCallback(
    () => {
      setOptions({
        headerTitle: () => (
          <ScreenHeaderTitleContainer>
            <I18n as={ HeaderTitle }>
              register analysis
            </I18n>
          </ScreenHeaderTitleContainer>
        )
      })
    },
    [setOptions]
  ))

  return (
    <AuthenticatedLayout>
      <>
        {userWrite && !isCooperative && !isEmpty(dairyFarm) && (
          <ButtonContainer>
            <View>
              <Button
                color={ colors.text }
                backgroundColor={ colors.secondary }
                title={ t('register analysis') }
                onPress={ () => navigation.navigate('DairyFarm', { screen: 'RegisterQuality' }) }
              />
            </View>
          </ButtonContainer>
        )}
        <Container>
          <SafeAreaView style={ { flex: 1 } }>
            <SearchContainer hasWarning={ isEmpty(dairyFarm) || isCooperative }>
              { (isEmpty(dairyFarm) || isCooperative) && (
                <WarningDiv>
                  <I18n>data inclusion will be done by the organization</I18n>
                </WarningDiv>
              )}
              <Button
                variant="outlined"
                style={ {
                  borderColor: colors.lightGrey,
                  backgroundColor: isEmpty(filters) ? colors.white : colors.secondary
                } }
                onPress={ openFilterModal }
                icon={
                  <Icon icon={ filter } size={ 14 } />
                }
                color={ colors.text }
                backgroundColor={ colors.secondary }
                title={ t('filtrate') }
              />
            </SearchContainer>
            <List
              hideUpdatedAt
              data={ milkQualities }
              renderItem={ renderItem }
              deps={ deps }
              onListLoad={ onListLoad }
              ListEmptyComponent={ (
                <EmptyState
                  text={ t('no results found') }
                  icon={ emptyFilter }
                />
              ) }
            />
          </SafeAreaView>
        </Container>
      </>
    </AuthenticatedLayout>
  )
}

export default MilkQualityListScreen
