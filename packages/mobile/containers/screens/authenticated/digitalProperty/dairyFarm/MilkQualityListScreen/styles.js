import styled from 'styled-components/native'

export const ButtonContainer = styled.View`
  position: absolute;
  bottom: 10px;
  z-index: 2;
  width: 50%;
  left: 25%;
  padding-bottom: 20px;
`

export const SearchContainer = styled.View`
  flex-direction: row;
  justify-content: ${ ({ hasWarning }) => hasWarning ? 'space-between' : 'flex-end' };
  align-items: center;
  flex-wrap: wrap;
  margin: 10px;
`

export const WarningDiv = styled.View`
  padding: 5px;
  background-color: rgba(254,241,182,0.8);
  flex: 1;
  margin-right: 10px;
`
