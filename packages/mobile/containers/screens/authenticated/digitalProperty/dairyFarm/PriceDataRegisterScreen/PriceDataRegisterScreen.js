import React, { useRef, useCallback, useMemo } from 'react'
import { View } from 'react-native'
import { useDispatch } from 'react-redux'

import { useRoute, useNavigation } from '@react-navigation/native'

import isEmpty from 'lodash/isEmpty'

import { useT } from '@smartcoop/i18n'
import Button from '@smartcoop/mobile-components/Button'
import RegisterPriceDataForm from '@smartcoop/mobile-containers/forms/digitalProperty/dairyFarm/RegisterPriceDataForm'
import AuthenticatedLayout from '@smartcoop/mobile-containers/layouts/AuthenticatedLayout'
import { Container, Item, Scroll, ButtonsContainer } from '@smartcoop/mobile-containers/layouts/AuthenticatedLayout/theme'
import { useSnackbar } from '@smartcoop/snackbar'
import { DairyFarmActions } from '@smartcoop/stores/dairyFarm'

const PriceDataRegisterScreen = () => {
  const formRef = useRef(null)
  const snackbar = useSnackbar()
  const navigation = useNavigation()
  const t = useT()
  const dispatch = useCallback(useDispatch(), [])
  const route = useRoute()
  const { priceData, dairyFarm } = route?.params ?? {}

  const isEditing = useMemo(
    () => (!isEmpty(priceData.id)), [priceData]
  )

  const defaultValues = useMemo(
    () => (
      {
        ...priceData,
        priceDate: priceData && priceData.priceDate ?
          priceData.priceDate.substring(0, priceData.priceDate.lastIndexOf('-'))
          : undefined,
        price: priceData && priceData.price ? priceData.price.toFixed(2).toString() : undefined,
        companyName: isEditing ? (priceData?.companyName ?? '') : (dairyFarm?.companyName ?? '')
      }
    ), [dairyFarm, isEditing, priceData]
  )

  const submit = useCallback(
    () => {
      formRef.current.submit()
    },
    []
  )

  const handleClose = useCallback(
    () => {
      navigation.navigate('ListPriceData')
    },
    [navigation]
  )

  const onSuccess = useCallback(
    () => {
      snackbar.success(
        t(`your {this} was ${  isEditing ? 'edited' :'registered' }`, {
          howMany: 2,
          gender: 'male',
          this: t('price data')
        })
      )
      handleClose()
    },
    [handleClose, isEditing, snackbar, t]
  )

  const submitForms = useCallback(
    (data) => {
      if(isEditing) {
        dispatch(DairyFarmActions.updatePriceData(
          data,
          defaultValues.id,
          onSuccess
        ))
      } else {
        dispatch(DairyFarmActions.savePriceData(
          data,
          onSuccess
        ))
      }
    }, [defaultValues, dispatch, isEditing, onSuccess]
  )


  return (
    <AuthenticatedLayout>
      <>
        <Container>
          <Scroll padding={ 0 }>
            <Item>
              <RegisterPriceDataForm
                ref={ formRef }
                onSuccess={ onSuccess }
                onSubmit={ submitForms }
                defaultValues={ defaultValues }
                withoutSubmitButton
              />
            </Item>
          </Scroll>
          <Item>
            <ButtonsContainer style={ { paddingTop: 10 } }>
              <Button
                onPress={ handleClose }
                style={ { flex: 1 } }
                variant="outlined"
                title={ t('cancel') }
              />
              <View style={ { width: '10%' } } />
              <Button
                onPress={ submit }
                style={ { flex: 1 } }
                title={ t('save') }
              />
            </ButtonsContainer>
          </Item>
        </Container>
      </>
    </AuthenticatedLayout>
  )
}

export default PriceDataRegisterScreen
