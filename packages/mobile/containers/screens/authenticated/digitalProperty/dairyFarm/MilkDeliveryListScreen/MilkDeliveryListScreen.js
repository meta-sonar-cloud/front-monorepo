import React, { useCallback, useMemo, useState } from 'react'
import { SafeAreaView, View } from 'react-native'
import { useDispatch, useSelector } from 'react-redux'

import { useFocusEffect , useNavigation } from '@react-navigation/native'

import isEmpty from 'lodash/isEmpty'

import { useDialog } from '@smartcoop/dialog'
import I18n, { useT } from '@smartcoop/i18n'
import {  emptyFilter, filter } from '@smartcoop/icons'
import Button from '@smartcoop/mobile-components/Button'
import EmptyState from '@smartcoop/mobile-components/EmptyState'
import Icon from '@smartcoop/mobile-components/Icon'
import List from '@smartcoop/mobile-components/List'
import MilkDeliveryItem from '@smartcoop/mobile-components/MilkDeliveryItem'
import ConfirmModal from '@smartcoop/mobile-components/Modal/ConfirmModal'
import { useScreenOptions } from '@smartcoop/mobile-containers/hooks/useScreenOptions'
import AuthenticatedLayout from '@smartcoop/mobile-containers/layouts/AuthenticatedLayout'
import { Container , ScreenHeaderTitleContainer, HeaderTitle } from '@smartcoop/mobile-containers/layouts/AuthenticatedLayout/theme'
import FilterFieldsMilkDeliveryModal from '@smartcoop/mobile-containers/modals/FilterFieldsMilkDeliveryModal/FilterFieldsMilkDeliveryModal'
import { useSnackbar } from '@smartcoop/snackbar'
import { DairyFarmActions } from '@smartcoop/stores/dairyFarm'
import { selectMilkDeliveries } from '@smartcoop/stores/dairyFarm/selectorDairyFarm'
import { selectCurrentProperty } from '@smartcoop/stores/property/selectorProperty'
import { selectUserCanWrite } from '@smartcoop/stores/user/selectorUser'
import colors from '@smartcoop/styles/colors'

import {
  ButtonContainer,
  SearchContainer,
  WarningDiv
} from './styles'

const MilkDeliveryListScreen = () => {
  const dispatch = useCallback(useDispatch(), [])
  const navigation = useNavigation()
  const snackbar = useSnackbar()
  const { createDialog } = useDialog()
  const t = useT()
  const { setOptions } = useScreenOptions()

  const [forceListUpdate, setForceListUpdate] = useState()
  const [filters, setFilters] = useState({})

  const currentProperty = useSelector(selectCurrentProperty)
  const milkDeliveries = useSelector(selectMilkDeliveries)
  const userWrite = useSelector(selectUserCanWrite)

  const dairyFarm = useMemo(() => currentProperty.dairyFarm[0], [currentProperty.dairyFarm])

  const isCooperative = useMemo(
    () => !isEmpty(dairyFarm.organizationId),
    [dairyFarm]
  )

  const handleParams = useCallback(
    (values) => (
      Object.keys(values)
        .filter(
          (key) => typeof values[key] === 'boolean' || values[key]
        )
        .reduce(
          (prev, curr) => ({ ...prev, [curr]: values[curr] }), {}
        )
    ),
    []
  )

  const params = useMemo(
    () => {
      const filterParams = {
        ...filters
      }
      return handleParams(filterParams)
    },
    [filters, handleParams]
  )

  const deps = useMemo(
    () => [forceListUpdate, params],
    [forceListUpdate, params]
  )

  const onListLoad = useCallback(
    (page, onSuccess, onError) => {
      dispatch(DairyFarmActions.loadMilkDeliveries(
        { ...params, page },
        onSuccess,
        onError
      ))
    },
    [dispatch, params]
  )

  const onDelete = useCallback(
    ({ id }) => {
      createDialog({
        id: 'delete-field',
        Component: ConfirmModal,
        props: {
          onConfirm: () => {
            dispatch(
              DairyFarmActions.deleteMilkDelivery(
                id,
                () => setForceListUpdate(old => !old),
                () => setForceListUpdate(old => !old)
              )
            )
            snackbar.success(
              t('your {this} was deleted', {
                howMany: 1,
                gender: 'female',
                this: t('delivery', { howMany: 1 })
              })
            )
          },
          message: t('are you sure you want to delete the {this}?', {
            howMany: 1,
            gender: 'female',
            this: t('delivery', { howMany: 1 })
          }),
          buttonText: t('yes'),
          underlinedText: t('no')
        }
      })
    },
    [createDialog, dispatch, snackbar, t]
  )

  const renderItem = useCallback(
    ({ item }) => (
      <MilkDeliveryItem
        data={ item }
        onDelete={ () => onDelete(item) }
        onEdit={ () => navigation.navigate('DairyFarm', { screen: 'RegisterDelivery', params: { milkDelivery: { ...item } } }) }
      />
    ),
    [navigation, onDelete]
  )

  const handleFilter = useCallback(
    (values) => {
      setFilters(values)
    },
    []
  )

  const openFilterModal = useCallback(
    () => {
      createDialog({
        id: 'filter-fields',
        Component: FilterFieldsMilkDeliveryModal,
        props: {
          onConfirm: handleFilter,
          filters
        }
      })
    },
    [createDialog, filters, handleFilter]
  )


  useFocusEffect(
    useCallback(() => {
      setForceListUpdate(old => !old)
    }, [])
  )

  useFocusEffect(useCallback(
    () => {
      setOptions({
        headerTitle: () => (
          <ScreenHeaderTitleContainer>
            <I18n as={ HeaderTitle }>
              milk deliveries
            </I18n>
          </ScreenHeaderTitleContainer>
        )
      })
    },
    [setOptions]
  ))

  return (
    <AuthenticatedLayout>
      <>
        {userWrite && !isCooperative && !isEmpty(dairyFarm) && (
          <ButtonContainer>
            <View>
              <Button
                color={ colors.text }
                backgroundColor={ colors.secondary }
                title={ t('register delivery') }
                onPress={ () => navigation.navigate('DairyFarm', { screen: 'RegisterDelivery', params: { milkDelivery: { } } }) }
              />
            </View>
          </ButtonContainer>
        )}
        <Container>
          <SafeAreaView style={ { flex: 1 } }>
            <SearchContainer hasWarning={ isEmpty(dairyFarm) || isCooperative }>
              { (isEmpty(dairyFarm) || isCooperative) && (
                <WarningDiv>
                  <I18n>data inclusion will be done by the organization</I18n>
                </WarningDiv>
              )}
              <Button
                variant="outlined"
                style={ {
                  borderColor: colors.lightGrey,
                  backgroundColor: isEmpty(filters) ? colors.white : colors.secondary
                } }
                onPress={ openFilterModal }
                icon={
                  <Icon icon={ filter } size={ 14 } />
                }
                color={ colors.text }
                backgroundColor={ colors.secondary }
                title={ t('filtrate') }
              />
            </SearchContainer>
            <List
              hideUpdatedAt
              data={ milkDeliveries }
              renderItem={ renderItem }
              deps={ deps }
              onListLoad={ onListLoad }
              ListEmptyComponent={ (
                <EmptyState
                  text={ t('no results found') }
                  icon={ emptyFilter }
                />
              ) }
            />
          </SafeAreaView>
        </Container>
      </>
    </AuthenticatedLayout>
  )
}

export default MilkDeliveryListScreen
