import React, { useEffect, useCallback, useState, useRef, useMemo } from 'react'
import { View } from 'react-native'
import { useDispatch, useSelector } from 'react-redux'

import { useT } from '@meta-react/i18n'
import { useNavigation, useFocusEffect } from '@react-navigation/native'

import { isEmpty, values } from 'lodash'

import { useDialog } from '@smartcoop/dialog'
import AnimalPregnancyActionsItem from '@smartcoop/mobile-components/AnimalPregnancyActionsItem'
import Button from '@smartcoop/mobile-components/Button'
import FilterButton from '@smartcoop/mobile-components/FilterButton'
import List from '@smartcoop/mobile-components/List'
import FilterInseminationModal from '@smartcoop/mobile-containers/modals/dairyFarm/FilterInseminationModal'
import { useSnackbar } from '@smartcoop/snackbar'
import { AnimalActions } from '@smartcoop/stores/animal'
import { selectCurrentAnimal } from '@smartcoop/stores/animal/selectorAnimal'
import { AnimalPregnancyActionsActions } from '@smartcoop/stores/animalPregnancyActions'
import { selectAnimalPregnancyActions } from '@smartcoop/stores/animalPregnancyActions/selectorAnimalPregnancyActions'
import { LotActions } from '@smartcoop/stores/lot'
import { selectCurrentProperty } from '@smartcoop/stores/property/selectorProperty'
import { colors } from '@smartcoop/styles'

import { Top, ButtonContainer } from './styles'

const ListDrying = () => {
  const { createDialog } = useDialog()
  const t = useT()
  const snackbar = useSnackbar()

  const dispatch = useCallback(useDispatch(), [])
  const [refresh, setRefresh] = useState(true)
  const animalPregnancyActions = useSelector(selectAnimalPregnancyActions)
  const { id: propertyId } = useSelector(selectCurrentProperty)
  const [filters, setFilters] = useState({})
  const mounted = useRef(false)
  const navigation = useNavigation()
  const currentAnimal = useSelector(selectCurrentAnimal)

  useEffect(() => {
    dispatch(
      LotActions.loadLots(
        { propertyId },
        () => {},
        (err) => {
          snackbar.error(t(err.message))
        }
      )
    )
  }, [dispatch, propertyId, snackbar, t])

  const deps = useMemo(() => [filters, refresh], [filters, refresh])

  const registerDisabled = useMemo(
    () => currentAnimal?.animalStatus?.name !== 'Prenha',
    [currentAnimal]
  )

  const handleSetFilters = useCallback((filterValues) => {
    setFilters({
      ...filterValues,
      q: filterValues?.name ?? null
    })
  }, [])

  const openFilterModal = useCallback(() => {
    createDialog({
      id: 'filter-fields',
      Component: FilterInseminationModal,
      props: {
        onSubmit: handleSetFilters,
        filters
      }
    })
  }, [createDialog, filters, handleSetFilters])

  const loadDryings = useCallback(
    (page, onSuccess, onError) => {
      dispatch(
        AnimalPregnancyActionsActions.loadDryings(
          { ...filters, page },
          () => {
            onSuccess()
            dispatch(AnimalActions.loadCurrentAnimal())
          },
          onError
        )
      )
    },
    [dispatch, filters]
  )

  const renderItem = useCallback(
    ({ item }) => (
      <AnimalPregnancyActionsItem
        animalPregnancy={ item }
        onEdit={ () => {
          navigation.navigate('DairyFarm', {
            screen: 'RegisterPregnancyAction',
            params: { pregnancyAction: item }
          })
        } }
      />
    ),
    [navigation]
  )

  useFocusEffect(
    useCallback(() => {
      if (!mounted.current) {
        mounted.current = true
      } else {
        setRefresh((old) => !old)
      }
    }, [])
  )

  useEffect(() => {
    dispatch(AnimalActions.loadCurrentAnimal())
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  return (
    <>
      <Top>
        <FilterButton
          id="insemination-filter-button"
          onPress={ openFilterModal }
          isActive={ !values(filters).every(isEmpty) }
          style={ { flex: 1 } }
        />
      </Top>
      <List
        data={ animalPregnancyActions }
        renderItem={ renderItem }
        onListLoad={ loadDryings }
        deps={ deps }
        style={ { marginBottom: 100 } }
      />
      <ButtonContainer>
        <View>
          <Button
            itemKey="code"
            id="register-drying"
            backgroundColor={ colors.secondary }
            color={ colors.text }
            textStyle={ { fontSize: 14 } }
            onPress={ () => {
              navigation.navigate('DairyFarm', {
                screen: 'RegisterPregnancyAction',
                params: { pregnancyAction: null }
              })
            } }
            title={ t('register {this}', { this: t('action') }) }
            disabled={ registerDisabled }
          />
        </View>
      </ButtonContainer>
    </>
  )
}

export default ListDrying
