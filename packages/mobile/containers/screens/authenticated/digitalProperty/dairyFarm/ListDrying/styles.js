import styled from 'styled-components/native'

export const Top = styled.View`
  display: flex;
  flex-direction: row;
`

export const ButtonContainer = styled.View`
  position: absolute;
  bottom: 0;
  z-index: 2;
  width: 50%;
  left: 25%;
  padding-bottom: 20px;
`
