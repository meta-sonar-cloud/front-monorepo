import React, { useCallback } from 'react'

import { useRoute, useFocusEffect } from '@react-navigation/native'

import { useScreenOptions } from '@smartcoop/mobile-containers/hooks/useScreenOptions'
import { LandscapeContainer, Container , ScreenHeaderTitleContainer, HeaderTitle } from '@smartcoop/mobile-containers/layouts/AuthenticatedLayout/theme'

const DairyFarmChartFullScreen = () => {
  const { params } = useRoute()
  const { setOptions } = useScreenOptions()

  const { title, Chart } = params

  useFocusEffect(useCallback(
    () => {
      setOptions({
        headerTitle: () => (
          <ScreenHeaderTitleContainer>
            <HeaderTitle>
              {title}
            </HeaderTitle>
          </ScreenHeaderTitleContainer>
        )
      })
    },
    [setOptions, title]
  ))

  return (
    <Container style={ { alignItems: 'center', justifyContent: 'center' } }>
      <LandscapeContainer>
        <Chart />
      </LandscapeContainer>
    </Container>
  )
}

export default DairyFarmChartFullScreen
