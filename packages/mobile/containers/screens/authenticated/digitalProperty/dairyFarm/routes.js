import React from 'react'

import { useNavigation } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'

import { useScreenOptions, withScreenOptions } from '@smartcoop/mobile-containers/hooks/useScreenOptions'

import AnimalRegisterScreen from './AnimalRegisterScreen'
import DairyFarmChartFullScreen from './DairyFarmChartFullScreen'
import LotRegisterScreen from './LotRegisterScreen'
import MilkDeliveryListScreen from './MilkDeliveryListScreen'
import MilkDeliveryRegisterScreen from './MilkDeliveryRegisterScreen'
import MilkQualityListScreen from './MilkQualityListScreen'
import MilkQualityRegisterScreen from './MilkQualityRegisterScreen'
import PriceDataListScreen from './PriceDataListScreen'
import PriceDataRegisterScreen from './PriceDataRegisterScreen'
import RegisterAnimalBirthScreen from './RegisterAnimalBirthScreen'
import RegisterAnimalPevScreen from './RegisterAnimalPevScreen'
import RegisterInseminationScreen from './RegisterInseminationScreen'
import RegisterPregnancyActionsScreen from './RegisterPregnancyActionsScreen'
import RegisterPregnancyDiagnosisScreen from './RegisterPregnancyDiagnosisScreen'

const Stack = createStackNavigator()

const DairyFarmScreenRouter = () =>{
  const navigation = useNavigation()
  const { options } = useScreenOptions(navigation)

  return (
    <Stack.Navigator screenOptions={ options } mode="modal">
      <Stack.Screen
        name="RegisterDelivery"
        component={ MilkDeliveryRegisterScreen }
      />

      <Stack.Screen
        name="RegisterInsemination"
        component={ RegisterInseminationScreen }
      />

      <Stack.Screen
        name="RegisterPregnancyAction"
        component={ RegisterPregnancyActionsScreen }
      />

      <Stack.Screen
        name="RegisterPregnancyDiagnosis"
        component={ RegisterPregnancyDiagnosisScreen }
      />

      <Stack.Screen
        name="ChartFullScreen"
        component={ DairyFarmChartFullScreen }
      />

      <Stack.Screen
        name="RegisterQuality"
        component={ MilkQualityRegisterScreen }
      />

      <Stack.Screen
        name="List"
        component={ MilkDeliveryListScreen }
      />

      <Stack.Screen
        name="ListQuality"
        component={ MilkQualityListScreen }
      />

      <Stack.Screen
        name="ListPriceData"
        component={ PriceDataListScreen }
      />

      <Stack.Screen
        name="RegisterPriceData"
        component={ PriceDataRegisterScreen }
      />

      <Stack.Screen
        name="RegisterLot"
        component={ LotRegisterScreen }
      />

      <Stack.Screen
        name="RegisterAnimal"
        component={ AnimalRegisterScreen }
      />

      <Stack.Screen
        name="RegisterAnimalPev"
        component={ RegisterAnimalPevScreen }
      />

      <Stack.Screen
        name="RegisterAnimalBirth"
        component={ RegisterAnimalBirthScreen }
      />
    </Stack.Navigator>
  )
}


export default withScreenOptions(DairyFarmScreenRouter)
