import styled from 'styled-components/native'

export const Top = styled.View`
  flex-direction: row;
  margin: 10px 10px 0px;
`

export const ButtonContainer = styled.View`
  position: absolute;
  bottom: 0;
  z-index: 2;
  width: 50%;
  left: 25%;
  padding-bottom: 20px;
`
