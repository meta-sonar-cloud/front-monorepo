import React, { useCallback, useState, useRef, useMemo, useEffect } from 'react'
import { View } from 'react-native'
import { useDispatch, useSelector } from 'react-redux'

import { useT } from '@meta-react/i18n'
import { useNavigation, useFocusEffect } from '@react-navigation/native'

import { isEmpty, values, debounce } from 'lodash'

import { useDialog } from '@smartcoop/dialog'
import AnimalItem from '@smartcoop/mobile-components/AnimalItem'
import Button from '@smartcoop/mobile-components/Button'
import FilterButton from '@smartcoop/mobile-components/FilterButton'
import InputSearch from '@smartcoop/mobile-components/InputSearch'
import List from '@smartcoop/mobile-components/List'
import FilterCattleManagementModal from '@smartcoop/mobile-containers/modals/dairyFarm/FilterCattleManagementModal'
import { useSnackbar } from '@smartcoop/snackbar'
import { AnimalActions } from '@smartcoop/stores/animal'
import { selectAnimals } from '@smartcoop/stores/animal/selectorAnimal'
import { LotActions } from '@smartcoop/stores/lot'
import { selectCurrentProperty } from '@smartcoop/stores/property/selectorProperty'
import { colors } from '@smartcoop/styles'

import { Top, ButtonContainer } from './styles'

const CattleManagement = () => {
  const { createDialog } = useDialog()
  const t = useT()
  const snackbar = useSnackbar()
  const dispatch = useCallback(useDispatch(), [])
  const [refresh, setRefresh] = useState(true)
  const [filters, setFilters] = useState({})
  const [filterText, setFilterText] = useState('')
  const { id: propertyId } = useSelector(selectCurrentProperty)
  const [debouncedFilterText, setDebouncedFilterText] = useState('')
  const animals = useSelector(selectAnimals)

  useEffect(() => {
    dispatch(
      LotActions.loadLots(
        { propertyId },
        () => {},
        (err) => {
          snackbar.error(t(err.message))
        }
      )
    )
  }, [dispatch, propertyId, snackbar, t])

  const handleParams = useCallback(
    (valuesFilters) =>
      Object.keys(valuesFilters)
        .filter(
          (key) => typeof valuesFilters[key] === 'boolean' || valuesFilters[key]
        )
        .reduce((prev, curr) => ({ ...prev, [curr]: valuesFilters[curr] }), {}),
    []
  )

  const params = useMemo(() => {
    const filterParams = {
      ...filters,
      q: debouncedFilterText
    }
    return handleParams(filterParams)
  }, [debouncedFilterText, filters, handleParams])

  const debouncedChangeSearchFilter = useCallback(
    debounce((value) => {
      setDebouncedFilterText(value)
    }, 300),
    []
  )

  const onChangeSearchFilter = useCallback(
    (value) => {
      setFilterText(value)
      debouncedChangeSearchFilter(value)
    },
    [debouncedChangeSearchFilter]
  )

  const mounted = useRef(false)
  const navigation = useNavigation()
  // const snackbar = useSnackbar()

  const deps = useMemo(() => [params, refresh, propertyId], [params, propertyId, refresh])

  const handleSetFilters = useCallback((filterValues) => {
    setFilters({
      ...filterValues,
      q: filterValues?.name ?? null
    })
  }, [])

  const openFilterModal = useCallback(() => {
    createDialog({
      id: 'filter-fields',
      Component: FilterCattleManagementModal,
      props: {
        onSubmit: handleSetFilters,
        filters
      }
    })
  }, [createDialog, filters, handleSetFilters])

  const loadAnimals = useCallback(
    (page, onSuccess, onError) => {
      dispatch(
        AnimalActions.loadAnimals({ ...params, page }, onSuccess, onError)
      )
    },
    [dispatch, params]
  )

  const handleEditAnimal = useCallback(
    (currentAnimal = {}) => {
      dispatch(AnimalActions.setCurrentAnimal(currentAnimal))
      navigation.navigate('DairyFarm', { screen: 'RegisterAnimal' })
    },
    [dispatch, navigation]
  )

  const handleOnPressAnimal = useCallback(
    (currentAnimal = {}) => {
      dispatch(AnimalActions.setCurrentAnimal(currentAnimal))
      dispatch(AnimalActions.setIsDetail(true))
      navigation.navigate('DairyFarm', { screen: 'RegisterAnimal' })
    },
    [dispatch, navigation]
  )

  const renderItem = useCallback(
    ({ item }) => (
      <AnimalItem
        animal={ item }
        onEdit={ () => handleEditAnimal(item) }
        onPress={ () => handleOnPressAnimal(item) }
      />
    ),
    [handleEditAnimal, handleOnPressAnimal]
  )

  useFocusEffect(
    useCallback(() => {
      if (!mounted.current) {
        mounted.current = true
      } else {
        setRefresh((old) => !old)
      }
    }, [])
  )

  return (
    <>
      <Top>
        <View style={ { flex: 1, marginRight: 15 } }>
          <InputSearch
            detached
            name="field-search-input"
            label={ t('search') }
            value={ filterText }
            onChange={ onChangeSearchFilter }
            style={ { marginBottom: 0 } }
          />
        </View>
        <View>
          <FilterButton
            id="lot-filter-button"
            onPress={ openFilterModal }
            isActive={
              !values({ ...filters, dead: filters?.dead ? 'dead' : '' }).every(
                isEmpty
              )
            }
            style={ { flex: 1 } }
          />
        </View>
      </Top>
      <List
        data={ animals }
        renderItem={ renderItem }
        onListLoad={ loadAnimals }
        deps={ deps }
        style={ { marginBottom: 100 } }
      />
      <ButtonContainer>
        <View>
          <Button
            itemKey="code"
            id="register-animal"
            backgroundColor={ colors.secondary }
            color={ colors.text }
            onPress={ () => {
              navigation.navigate('DairyFarm', { screen: 'RegisterAnimal' })
            } }
            title={ t('register {this}', {
              howMany: 1,
              this: t('animal', { howMany: 1 })
            }) }
          />
        </View>
      </ButtonContainer>
    </>
  )
}

export default CattleManagement
