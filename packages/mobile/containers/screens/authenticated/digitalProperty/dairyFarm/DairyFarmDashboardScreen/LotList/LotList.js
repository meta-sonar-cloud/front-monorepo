import React, { useCallback, useState, useRef, useMemo } from 'react'
import { View } from 'react-native'
import { useDispatch, useSelector } from 'react-redux'

import { useT } from '@meta-react/i18n'
import { useNavigation, useFocusEffect } from '@react-navigation/native'

import { isEmpty, values } from 'lodash'

import { useDialog } from '@smartcoop/dialog'
import Button from '@smartcoop/mobile-components/Button'
import FilterButton from '@smartcoop/mobile-components/FilterButton'
import List from '@smartcoop/mobile-components/List'
import LotItem from '@smartcoop/mobile-components/LotItem'
import ConfirmModal from '@smartcoop/mobile-components/Modal/ConfirmModal'
import FilterLotModal from '@smartcoop/mobile-containers/modals/dairyFarm/FilterLotModal'
import { useSnackbar } from '@smartcoop/snackbar'
import { LotActions } from '@smartcoop/stores/lot'
import { selectLots } from '@smartcoop/stores/lot/selectorLot'
import { selectCurrentProperty } from '@smartcoop/stores/property/selectorProperty'
import { colors } from '@smartcoop/styles'

import { Top, ButtonContainer } from './styles'

const LotList = () => {
  const { createDialog } = useDialog()
  const t = useT()
  const dispatch = useCallback(useDispatch(), [])
  const [refresh, setRefresh] = useState(true)
  const lots = useSelector(selectLots)
  const currentProperty = useSelector(selectCurrentProperty)
  const [filters, setFilters] = useState({})
  const mounted = useRef(false)
  const navigation = useNavigation()
  const snackbar = useSnackbar()

  const deps = useMemo(() => [filters, refresh, currentProperty], [
    currentProperty,
    filters,
    refresh
  ])

  const handleSetFilters = useCallback((filterValues) => {
    setFilters({
      ...filterValues,
      q: filterValues?.name ?? null
    })
  }, [])

  const openFilterModal = useCallback(() => {
    createDialog({
      id: 'filter-fields',
      Component: FilterLotModal,
      props: {
        onSubmit: handleSetFilters,
        filters
      }
    })
  }, [createDialog, filters, handleSetFilters])

  const loadLots = useCallback(
    (page, onSuccess, onError) => {
      dispatch(LotActions.loadLots({ ...filters, page }, onSuccess, onError))
    },
    [dispatch, filters]
  )

  const onDelete = useCallback(
    (lot) => {
      setRefresh(true)
      snackbar.success(
        t('your {this} was deleted', {
          howMany: 1,
          gender: 'male',
          this: t('lot')
        })
      )
      dispatch(LotActions.deleteLot(lot.id))
    },
    [dispatch, snackbar, t]
  )

  const handleDelete = useCallback(
    (lot) => {
      createDialog({
        id: 'confirm-delete',
        Component: ConfirmModal,
        props: {
          onConfirm: () => onDelete(lot),
          message: t('are you sure you want to remove the {this}?', {
            howMany: 1,
            gender: 'male',
            this: t('lot')
          })
        }
      })
    },
    [createDialog, onDelete, t]
  )

  const renderItem = useCallback(
    ({ item }) => (
      <LotItem
        name={ item.name }
        code={ item.code }
        defaultLot={ item.defaultLot }
        onEdit={ () => {
          navigation.navigate('DairyFarm', {
            screen: 'RegisterLot',
            params: { lot: item }
          })
        } }
        onDelete={ () => handleDelete(item) }
      />
    ),
    [handleDelete, navigation]
  )

  useFocusEffect(
    useCallback(() => {
      if (!mounted.current) {
        mounted.current = true
      } else {
        setRefresh((old) => !old)
      }
    }, [])
  )

  return (
    <>
      <Top>
        <FilterButton
          id="lot-filter-button"
          onPress={ openFilterModal }
          isActive={ !values(filters).every(isEmpty) }
          style={ { flex: 1 } }
        />
      </Top>
      <List
        data={ lots }
        renderItem={ renderItem }
        onListLoad={ loadLots }
        deps={ deps }
        style={ { marginBottom: 100 } }
      />
      <ButtonContainer>
        <View>
          <Button
            itemKey="code"
            id="register-lot"
            backgroundColor={ colors.secondary }
            color={ colors.text }
            onPress={ () => {
              navigation.navigate('DairyFarm', { screen: 'RegisterLot' })
            } }
            title={ t('register lots') }
          />
        </View>
      </ButtonContainer>
    </>
  )
}

export default LotList
