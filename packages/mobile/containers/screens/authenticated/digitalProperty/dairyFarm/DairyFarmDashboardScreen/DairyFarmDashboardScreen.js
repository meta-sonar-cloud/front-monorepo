import React, { useMemo, useCallback } from 'react'
import { useSelector , useDispatch } from 'react-redux'

import { useT } from '@smartcoop/i18n'
import { DairyFarmActions } from '@smartcoop/stores/dairyFarm'
import { selectCurrentSection } from '@smartcoop/stores/dairyFarm/selectorDairyFarm'

import CattleManagement from './CattleManagement'
import DairyFarmDashboard from './DairyFarmDashboard/DairyFarmDashboard'
import LotList from './LotList/LotList'
import {
  TabButton,
  ScrollViewButtons,
  SafeAreaViewList
} from './styles'

const DairyFarmDashboardScreen = () => {
  const t = useT()
  const dispatch = useCallback(useDispatch(), [])
  const currentSection = useSelector(selectCurrentSection)

  const renderType = useMemo(
    () => ({
      lot:  <LotList />,
      dairyFarm: <DairyFarmDashboard/>,
      cattleManagement:<CattleManagement />
    }[currentSection]), [currentSection]
  )

  return (
    <>
      <ScrollViewButtons horizontal>
        <TabButton
          id="button-dairy"
          activeTab={ currentSection === 'dairyFarm' }
          onPress={ () => dispatch(DairyFarmActions.setCurrentSection('dairyFarm')) }
          title={ t('my dairy farm') }
        />
        <>
          <TabButton
            id="button=cattle"
            activeTab={ currentSection === 'cattleManagement' }
            onPress={ () => dispatch(DairyFarmActions.setCurrentSection('cattleManagement')) }
            title={ t('cattle management') }
          />
          <TabButton
            id="button-lot"
            activeTab={ currentSection === 'lot' }
            onPress={ () => dispatch(DairyFarmActions.setCurrentSection('lot')) }
            title={ t('lot') }
          />
        </>
      </ScrollViewButtons>
      <SafeAreaViewList>
        {renderType}
      </SafeAreaViewList>
    </>
  )

}

export default DairyFarmDashboardScreen
