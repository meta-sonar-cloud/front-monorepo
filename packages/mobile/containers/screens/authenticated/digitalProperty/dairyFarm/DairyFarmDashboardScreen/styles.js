import { Subheading } from 'react-native-paper'

import styled from 'styled-components/native'

import Button from '@smartcoop/mobile-components/Button'
import CardTitle from '@smartcoop/mobile-components/CardTitle'
import colors from '@smartcoop/styles/colors'


export const CardItemTitle = styled(CardTitle)`
  margin-bottom: 5px;
`

export const CardsContainer = styled.View`
  flex: 1;
`
export const CardsTitle = styled.Text`
  font-family: 'Montserrat';
  font-weight: 700;
  color: ${ colors.black };
  font-size: 20px;
  margin-top: 8px;
`

export const TabButton = styled(Button).attrs(props => ({
  color: props.activeTab ? colors.white : colors.darkGrey,
  backgroundColor: props.activeTab ? colors.black : colors.backgroundHtml
}))`
  margin-right: 10px;
`

export const Actions = styled.ScrollView.attrs(() => ({ horizontal: true }))`
  flex-direction: row;
  padding: 10px;
  height: 10px;
  margin: 0;
`

export const Half = styled.View`
`

export const Cards = styled.View`
  flex: 1;
  flex-direction: row;
  justify-content: space-between;
  flex-wrap: wrap;
  margin-left: 10px;
  margin-right: 10px;
`

export const Header = styled.View`
  flex-direction: row;
  align-content: center;
  align-items: center;
  padding: 10px;
`

export const Content = styled.View`
  flex-direction: row;
  align-content: center;
  align-items: center;
  padding: 10px;
`

export const Bottom = styled.View`
  justify-content: center;
  align-content: center;
  align-items: center;
  padding: 5px;
`

export const CardButton = styled.TouchableOpacity`
  align-items: center;
  flex: 1;
`

export const CardButtonTitle = styled(Subheading)`
  font-weight: 700;
  color: ${ colors.black };
`

export const ScrollViewButtons = styled.ScrollView`
  padding: 10px 15px 5px;
  margin-bottom: 10px;
  max-height: 58px;
`

export const SafeAreaViewList = styled.SafeAreaView`
  flex: 100;
  width: 100%;
`
