import { Subheading } from 'react-native-paper'

import styled from 'styled-components/native'

import CardTitle from '@smartcoop/mobile-components/CardTitle'
import colors from '@smartcoop/styles/colors'


export const CardItemTitle = styled(CardTitle)`
  margin-bottom: 5px;
`

export const CardsContainer = styled.View`
  flex: 1;
`
export const CardsTitle = styled.Text`
  font-family: 'Montserrat';
  font-weight: 700;
  color: ${ colors.black };
  font-size: 20px;
  margin-top: 8px;
`
export const Actions = styled.View`
  flex-direction: row;
  flex-wrap: wrap;
`

export const Half = styled.View`
  width: 40%;
`

export const Cards = styled.View`
  flex: 1;
  flex-direction: row;
  justify-content: space-between;
  flex-wrap: wrap;
  margin-left: 10px;
  margin-right: 10px;
`

export const Header = styled.View`
  flex-direction: row;
  align-content: center;
  align-items: center;
  padding: 10px;
`

export const Content = styled.View`
  flex-direction: row;
  align-content: center;
  align-items: center;
  padding: 10px;
`

export const Bottom = styled.View`
  justify-content: center;
  align-content: center;
  align-items: center;
  padding: 5px;
`

export const CardButton = styled.TouchableOpacity`
  align-items: center;
  flex: 1;
`

export const CardButtonTitle = styled(Subheading)`
  font-weight: 700;
  color: ${ colors.black };
`
