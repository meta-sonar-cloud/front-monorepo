import React, { useState, useMemo, useCallback } from 'react'
import { Text } from 'react-native'
import { useSelector, useDispatch } from 'react-redux'

import { useNavigation, useFocusEffect } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import moment from 'moment/moment'

import isEmpty from 'lodash/isEmpty'
import map from 'lodash/map'

import { useDialog } from '@smartcoop/dialog'
import I18n, { useT } from '@smartcoop/i18n'
import {
  cow,
  fullScreen,
  landProductivityCows,
  concentratedMilkCows,
  feedConsumptionCows,
  milkSurface,
  lactatingCows,
  pencil
} from '@smartcoop/icons'
import Button from '@smartcoop/mobile-components/Button'
import Icon from '@smartcoop/mobile-components/Icon'
import InputMonthYear from '@smartcoop/mobile-components/InputMonthYear'
import MilkDataCard from '@smartcoop/mobile-components/MilkDataCard'
import DairyFarmDeliveryChart from '@smartcoop/mobile-containers/fragments/DairyFarmDeliveryChart'
import DairyFarmPriceChart from '@smartcoop/mobile-containers/fragments/DairyFarmPriceChart'
import DairyFarmQualityChart from '@smartcoop/mobile-containers/fragments/DairyFarmQualityChart'
import { Scroll } from '@smartcoop/mobile-containers/layouts/AuthenticatedLayout/theme'
import MilkDeliveryModal from '@smartcoop/mobile-containers/modals/MilkDeliveryModal'
import { DairyFarmActions } from '@smartcoop/stores/dairyFarm'
import { selectDashboardData } from '@smartcoop/stores/dairyFarm/selectorDairyFarm'
import { PropertyActions } from '@smartcoop/stores/property'
import { selectCurrentProperty } from '@smartcoop/stores/property/selectorProperty'
import colors from '@smartcoop/styles/colors'
import {
  momentBackMonthYearFormat,
  momentFriendlyMonthYearFormat
} from '@smartcoop/utils/dates'
import { formatCurrency } from '@smartcoop/utils/formatters'

import {
  CardsContainer,
  CardsTitle,
  Actions,
  Cards,
  CardItemTitle,
  CardButton,
  CardButtonTitle
} from './styles'

const Stack = createStackNavigator()

const DairyFarmDashboard = () => {
  const t = useT()
  const navigation = useNavigation()
  const { createDialog } = useDialog()
  const dispatch = useCallback(useDispatch(), [])

  const [date, setDate] = useState(moment().format(momentBackMonthYearFormat))
  const dashboardData = useSelector(selectDashboardData)
  const currentProperty = useSelector(selectCurrentProperty)

  useFocusEffect(
    useCallback(() => {
      dispatch(PropertyActions.reloadCurrentProperty())
    }, [dispatch])
  )

  const dairyFarm = useMemo(() => {
    if (!isEmpty(currentProperty)) {
      return currentProperty.dairyFarm[0]
    }
    return {}
  }, [currentProperty])

  const milkDeliveryEdit = useCallback(() => {
    createDialog({
      id: 'milk-delivery',
      Component: MilkDeliveryModal,
      props: {
        onCancel: () => isEmpty(dairyFarm) && navigation.navigate('Home'),
        dairyFarm
      }
    })
  }, [createDialog, dairyFarm, navigation])

  const editProperty = useCallback(() => {
    dispatch(
      PropertyActions.setEditPropertyData(currentProperty.id, currentProperty)
    )
    navigation.navigate('Property', {
      screen: 'Identification',
      params: { isEditing: true }
    })
  }, [currentProperty, dispatch, navigation])

  const iconCardMilk = useMemo(
    () => ({
      'lactating-cows': lactatingCows,
      'cow-productivity': cow,
      'concentrated-milk-relation': concentratedMilkCows,
      'monthly-feed-consumption': feedConsumptionCows,
      'land-productivity': landProductivityCows,
      'milk-surface-area': milkSurface
    }),
    []
  )

  const iconColorCardMilk = useMemo(
    () => ({
      'lactating-cows': colors.blue,
      'cow-productivity': colors.blue,
      'concentrated-milk-relation': colors.blue,
      'monthly-feed-consumption': colors.yellow,
      'land-productivity': colors.yellow,
      'milk-surface-area': colors.yellow
    }),
    []
  )

  const textCardMilk = useMemo(
    () => ({
      'lactating-cows': 'lactating cows',
      'cow-productivity': 'cow productivity',
      'concentrated-milk-relation': 'concentrated milk ratio',
      'monthly-feed-consumption': 'feed consumption',
      'land-productivity': 'land productivity',
      'milk-surface-area': 'milk surface'
    }),
    []
  )

  const formaterValue = useCallback(
    (value) => formatCurrency(value, 0, ',', '.', ''),
    []
  )

  const dateFriendly = useMemo(
    () =>
      moment(date, momentBackMonthYearFormat).format(
        momentFriendlyMonthYearFormat
      ),
    [date]
  )

  useFocusEffect(
    useCallback(() => {
      isEmpty(dairyFarm) && milkDeliveryEdit()
    }, [dairyFarm, milkDeliveryEdit])
  )

  useFocusEffect(
    useCallback(() => {
      if (!isEmpty(currentProperty)) {
        dispatch(DairyFarmActions.loadDashboardData({ date }))
      }
    }, [currentProperty, date, dispatch])
  )

  return (
    <>
      <Actions style={ { padding: 10 } }>
        <InputMonthYear
          detached
          label="Mês/Ano"
          name="monthYear"
          value={ date }
          onChange={ (value) => {
            setDate(value.format(momentBackMonthYearFormat))
          } }
          style={ { flex: 1, marginRight: 10 } }
        />
        <Button
          variant="outlined"
          onPress={ milkDeliveryEdit }
          icon={ <Icon size={ 16 } icon={ pencil } color={ colors.black } /> }
          backgroundColor={ colors.white }
          title={ t('edit company') }
        />
      </Actions>

      <Button
        variant="outlined"
        onPress={ editProperty }
        icon={ <Icon size={ 16 } icon={ pencil } color={ colors.black } /> }
        style={ { paddingLeft: 10, paddingRight: 10 } }
        backgroundColor={ colors.white }
        title={ t('edit property') }
      />

      <Scroll>
        <CardsContainer>
          <CardItemTitle
            title={ t('milk deliveries') }
            titleRight={
              <Button
                id="milk-deliveries-fullscreen"
                icon={ <Icon icon={ fullScreen } size={ 20 } /> }
                backgroundColor={ colors.white }
                onPress={ () =>
                  navigation.navigate('DairyFarm', {
                    screen: 'ChartFullScreen',
                    params: {
                      title: `${ t('milk deliveries') } - ${ dateFriendly }`,
                      Chart: DairyFarmDeliveryChart
                    }
                  })
                }
              />
            }
            footer={
              <CardButton
                onPress={ () =>
                  navigation.navigate('DairyFarm', { screen: 'List' })
                }
              >
                <I18n as={ CardButtonTitle }>register delivery</I18n>
              </CardButton>
            }
          >
            <DairyFarmDeliveryChart />
          </CardItemTitle>

          <CardItemTitle
            title={ t('quality analysis') }
            titleRight={
              <Button
                id="quality-analysis-fullscreen"
                icon={ <Icon icon={ fullScreen } size={ 20 } /> }
                backgroundColor={ colors.white }
                onPress={ () =>
                  navigation.navigate('DairyFarm', {
                    screen: 'ChartFullScreen',
                    params: {
                      title: `${ t('quality analysis') } - ${ dateFriendly }`,
                      Chart: DairyFarmQualityChart
                    }
                  })
                }
              />
            }
            footer={
              <CardButton
                onPress={ () =>
                  navigation.navigate('DairyFarm', { screen: 'ListQuality' })
                }
              >
                <I18n as={ CardButtonTitle }>register analysis</I18n>
              </CardButton>
            }
          >
            <DairyFarmQualityChart />
          </CardItemTitle>

          <CardItemTitle
            title={ t('price', { howMany: 2 }) }
            titleRight={
              <Button
                id="price-fullscreen"
                icon={ <Icon icon={ fullScreen } size={ 20 } /> }
                backgroundColor={ colors.white }
                onPress={ () =>
                  navigation.navigate('DairyFarm', {
                    screen: 'ChartFullScreen',
                    params: {
                      title: `${ t('price', { howMany: 2 }) } - ${ dateFriendly }`,
                      Chart: DairyFarmPriceChart
                    }
                  })
                }
              />
            }
            footer={
              <CardButton
                onPress={ () =>
                  navigation.navigate('DairyFarm', { screen: 'ListPriceData' })
                }
              >
                <I18n as={ CardButtonTitle }>register price</I18n>
              </CardButton>
            }
          >
            <DairyFarmPriceChart />
          </CardItemTitle>

          {dashboardData.indicators && (
            <>
              <Actions style={ { padding: 10 } }>
                <CardsTitle>
                  <I18n>indicators</I18n>
                </CardsTitle>
              </Actions>

              <Cards>
                {map(dashboardData.indicators, (card) => (
                  <MilkDataCard
                    key={ card.slug }
                    icon={ iconCardMilk[card.slug] }
                    measureUnit={ card.measureUnit }
                    title={ t(textCardMilk[card.slug]) }
                    color={ iconColorCardMilk[card.slug] }
                  >
                    <Text>{formaterValue(card.value)}</Text>
                  </MilkDataCard>
                ))}
              </Cards>
            </>
          )}
        </CardsContainer>
      </Scroll>
    </>
  )
}

// eslint-disable-next-line react/prop-types
export default ({ stackOptions }) => (
  <Stack.Navigator screenOptions={ stackOptions }>
    <Stack.Screen name="Dashboard" component={ DairyFarmDashboard } />
  </Stack.Navigator>
)
