import React, { useRef, useCallback, useMemo } from 'react'
import { View } from 'react-native'
import { useDispatch } from 'react-redux'

import { useRoute, useNavigation , useFocusEffect } from '@react-navigation/native'


import { isEmpty, toString } from 'lodash'

import I18n, { useT } from '@smartcoop/i18n'
import Button from '@smartcoop/mobile-components/Button'
import RegisterMilkDeliveryForm from '@smartcoop/mobile-containers/forms/digitalProperty/dairyFarm/RegisterMilkDeliveryForm'
import { useScreenOptions } from '@smartcoop/mobile-containers/hooks/useScreenOptions'
import AuthenticatedLayout from '@smartcoop/mobile-containers/layouts/AuthenticatedLayout'
import { Container, Item, Scroll, ButtonsContainer , ScreenHeaderTitleContainer, HeaderTitle } from '@smartcoop/mobile-containers/layouts/AuthenticatedLayout/theme'
import { useSnackbar } from '@smartcoop/snackbar'
import { DairyFarmActions } from '@smartcoop/stores/dairyFarm'


const MilkDeliveryRegisterScreen = () => {
  const formRef = useRef(null)
  const snackbar = useSnackbar()
  const navigation = useNavigation()
  const t = useT()
  const dispatch = useCallback(useDispatch(), [])
  const { setOptions } = useScreenOptions()

  const route = useRoute()

  const { milkDelivery } = route?.params ?? {}

  const defaultValues = useMemo(
    () => ({
      ...milkDelivery,
      volumeDate: !isEmpty(milkDelivery) ? toString(milkDelivery?.volumeDate) : '' ,
      volume: !isEmpty(milkDelivery) ? toString(milkDelivery?.volume) : '' ,
      condemnedVolume: !isEmpty(milkDelivery) ? toString(milkDelivery?.condemnedVolume) : '' ,
      observation: !isEmpty(milkDelivery) ? toString(milkDelivery?.observation) : '' ,
      temperature: !isEmpty(milkDelivery) ? toString(milkDelivery?.temperature) : ''
    }), [milkDelivery]
  )

  const isEditing = useMemo(
    () => defaultValues.id, [defaultValues]
  )

  const submit = useCallback(
    () => {
      formRef.current.submit()
    },
    []
  )

  const handleClose = useCallback(
    () => {
      navigation.navigate('List')
    },
    [navigation]
  )

  const onSuccess = useCallback(
    () => {
      snackbar.success(
        t(`your {this} was ${ isEditing ? 'edited' :'registered' }`, {
          howMany: 1,
          gender: 'female',
          this: t('delivery', { howMany: 1 })
        })
      )
      handleClose()
    },
    [handleClose, isEditing, snackbar, t]
  )


  const submitForms = useCallback(
    (formData) => {
      const action = isEditing ? 'updateMilkDelivery' : 'saveMilkDelivery'
      dispatch(DairyFarmActions[action](
        {
          ...defaultValues,
          ...formData,
          temperature: isEmpty(toString(formData.temperature)) ? null : toString(formData.temperature).replace(',', '.'),
          condemnedVolume: isEmpty(formData.condemnedVolume) ? null : formData.condemnedVolume.toString()
        },
        () => {onSuccess()}
      ))
    }, [defaultValues, dispatch, isEditing, onSuccess]
  )

  useFocusEffect(useCallback(
    () => {
      setOptions({
        headerTitle: () => (
          <ScreenHeaderTitleContainer>
            <I18n as={ HeaderTitle }>
              register delivery
            </I18n>
          </ScreenHeaderTitleContainer>
        )
      })
    },
    [setOptions]
  ))


  return (
    <AuthenticatedLayout>
      <>
        <Container>
          <Scroll padding={ 0 }>
            <Item
              style={ {
                flex: 1,
                paddingBottom: 0
              } }
            >
              <RegisterMilkDeliveryForm
                ref={ formRef }
                onSuccess={ onSuccess }
                onSubmit={ submitForms }
                defaultValues={ defaultValues }
                withoutSubmitButton
              />
            </Item>
          </Scroll>
          <Item>
            <ButtonsContainer style={ { paddingTop: 10 } }>
              <Button
                onPress={ handleClose }
                style={ { flex: 1 } }
                variant="outlined"
                title={ t('cancel') }
              />
              <View style={ { width: '10%' } } />
              <Button
                onPress={ submit }
                style={ { flex: 1 } }
                title={ t('save') }
              />
            </ButtonsContainer>
          </Item>
        </Container>
      </>
    </AuthenticatedLayout>
  )
}

export default MilkDeliveryRegisterScreen
