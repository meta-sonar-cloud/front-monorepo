import React, { useCallback, useMemo, useState, useEffect } from 'react'
import { SafeAreaView, View } from 'react-native'
import { useSelector, useDispatch  } from 'react-redux'

import { useFocusEffect , useNavigation } from '@react-navigation/native'

import isEmpty from 'lodash/isEmpty'

import { useDialog } from '@smartcoop/dialog'
import I18n, { useT } from '@smartcoop/i18n'
import { emptyFilter, filter } from '@smartcoop/icons'
import Button from '@smartcoop/mobile-components/Button'
import EmptyState from '@smartcoop/mobile-components/EmptyState'
import Icon from '@smartcoop/mobile-components/Icon'
import List from '@smartcoop/mobile-components/List'
import ConfirmModal from '@smartcoop/mobile-components/Modal/ConfirmModal'
import PriceDataItem from '@smartcoop/mobile-components/PriceDataItem'
import { useScreenOptions } from '@smartcoop/mobile-containers/hooks/useScreenOptions'
import AuthenticatedLayout from '@smartcoop/mobile-containers/layouts/AuthenticatedLayout'
import { Container , ScreenHeaderTitleContainer, HeaderTitle } from '@smartcoop/mobile-containers/layouts/AuthenticatedLayout/theme'
import FilterPriceDataModal from '@smartcoop/mobile-containers/modals/FilterPriceDataModal/FilterPriceDataModal'
import { useSnackbar } from '@smartcoop/snackbar'
import { DairyFarmActions } from '@smartcoop/stores/dairyFarm'
import { selectPriceData } from '@smartcoop/stores/dairyFarm/selectorDairyFarm'
import { selectCurrentProperty } from '@smartcoop/stores/property/selectorProperty'
import { selectUserCanWrite } from '@smartcoop/stores/user/selectorUser'
import colors from '@smartcoop/styles/colors'

import {
  ButtonContainer,
  SearchContainer,
  WarningDiv
} from './styles'

const PriceDataListScreen = () => {
  const dispatch = useCallback(useDispatch(), [])
  const navigation = useNavigation()
  const { createDialog } = useDialog()
  const snackbar = useSnackbar()
  const t = useT()
  const { setOptions } = useScreenOptions()

  const [forceListUpdate, setForceListUpdate] = useState()
  const [filters, setFilters] = useState({})

  const priceData = useSelector(selectPriceData)
  const currentProperty = useSelector(selectCurrentProperty)
  const userWrite = useSelector(selectUserCanWrite)

  const dairyFarm = useMemo(() => currentProperty.dairyFarm[0], [currentProperty.dairyFarm])

  const isCooperative = useMemo(
    () => !isEmpty(dairyFarm.organizationId),
    [dairyFarm]
  )

  const handleParams = useCallback(
    (values) => (
      Object.keys(values)
        .filter(
          (key) => typeof values[key] === 'boolean' || values[key]
        )
        .reduce(
          (prev, curr) => ({ ...prev, [curr]: values[curr] }), {}
        )
    ),
    []
  )

  const params = useMemo(
    () => {
      const filterParams = {
        ...filters,
        orderBy: '-price_date'
      }
      return handleParams(filterParams)
    },
    [filters, handleParams]
  )

  const deps = useMemo(
    () => [currentProperty, forceListUpdate, params],
    [currentProperty, forceListUpdate, params]
  )

  const loadPriceData = useCallback(
    (page, onSuccess, onError) => {
      if (!isEmpty(currentProperty)) {
        dispatch(DairyFarmActions.loadPriceData(
          { ...params, page },
          onSuccess,
          onError
        ))
      } else {
        onError(new Error ('current property is required'))
      }
    },
    [currentProperty, dispatch, params]
  )

  const handleFilter = useCallback(
    (values) => {
      setFilters(values)
    },
    []
  )

  const openFilterModal = useCallback(
    () => {
      createDialog({
        id: 'filter-price-data',
        Component: FilterPriceDataModal,
        props: {
          onConfirm: handleFilter,
          filters
        }
      })
    },
    [createDialog, filters, handleFilter]
  )

  const handleEdit = useCallback(
    (priceDataParam) => {
      navigation.navigate('DairyFarm',
        { screen: 'RegisterPriceData', params: { priceData: { ...priceDataParam }, dairyFarm: {} } })
    },
    [navigation]
  )

  const handleDelete = useCallback(
    ({ id }) => {
      createDialog({
        id: 'confirm-delete',
        Component: ConfirmModal,
        props: {
          onConfirm: () => {
            dispatch(DairyFarmActions.deletePriceData(
              id,
              () => {
                snackbar.success(
                  t('your {this} was deleted', {
                    howMany: 2,
                    gender: 'male',
                    this: t('price data')
                  })
                )
                setForceListUpdate(old => !old)
              },
              () => setForceListUpdate(old => !old)
            ))
          },
          message: t('are you sure you want to delete the {this}?', {
            howMany: 2,
            gender: 'male',
            this: t('price data')
          })
        }
      })
    },
    [createDialog, dispatch, snackbar, t]
  )

  const renderItem = useCallback(
    ({ item, id }) => (
      <PriceDataItem
        priceData={ item }
        key={ id }
        onEdit={ () => handleEdit(item) }
        onDelete={ () => handleDelete(item) }
      />
    ),
    [handleDelete, handleEdit]
  )

  useFocusEffect(
    useCallback(() => {
      setForceListUpdate(old => !old)
    }, [])
  )

  useEffect(() => {
    setFilters({})
  }, [])


  useFocusEffect(useCallback(
    () => {
      setOptions({
        headerTitle: () => (
          <ScreenHeaderTitleContainer>
            <I18n as={ HeaderTitle }>
              price data
            </I18n>
          </ScreenHeaderTitleContainer>
        )
      })
    },
    [setOptions]
  ))

  return (
    <AuthenticatedLayout>
      <>
        {userWrite && !isCooperative && !isEmpty(dairyFarm) && (
          <ButtonContainer>
            <View style={ { backgroundColor: colors.white } }>
              <Button
                color={ colors.text }
                backgroundColor={ colors.secondary }
                title={ t('register') }
                onPress={ () => navigation.navigate('DairyFarm',
                  { screen: 'RegisterPriceData', params: { priceData: {}, dairyFarm } })
                }
              />
            </View>
          </ButtonContainer>
        )}
        <Container>
          <SafeAreaView style={ { flex: 1 } }>
            <SearchContainer hasWarning={ isEmpty(dairyFarm) || isCooperative }>
              { (isEmpty(dairyFarm) || isCooperative) && (
                <WarningDiv>
                  <I18n>data inclusion will be done by the organization</I18n>
                </WarningDiv>
              )}
              <Button
                variant="outlined"
                style={ {
                  borderColor: colors.lightGrey,
                  backgroundColor: isEmpty(filters) ? colors.white : colors.secondary
                } }
                onPress={ openFilterModal }
                icon={
                  <Icon icon={ filter } size={ 14 } />
                }
                color={ colors.text }
                backgroundColor={ colors.secondary }
                title={ t('filtrate') }
              />
            </SearchContainer>
            <List
              data={ priceData }
              renderItem={ renderItem }
              deps={ deps }
              onListLoad={ loadPriceData }
              ListEmptyComponent={ (
                <EmptyState
                  text={ t('no results found') }
                  icon={ emptyFilter }
                />
              ) }
            />
          </SafeAreaView>
        </Container>
      </>
    </AuthenticatedLayout>
  )
}

export default PriceDataListScreen
