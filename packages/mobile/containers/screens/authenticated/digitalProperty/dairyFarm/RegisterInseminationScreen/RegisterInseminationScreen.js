import React, { useRef, useCallback, useMemo, useState } from 'react'
import { View } from 'react-native'
import { useSelector, useDispatch } from 'react-redux'

import { useRoute, useNavigation , useFocusEffect } from '@react-navigation/native'

import isEmpty from 'lodash/isEmpty'

import I18n, { useT } from '@smartcoop/i18n'
import Button from '@smartcoop/mobile-components/Button'
import RegisterInseminationForm from '@smartcoop/mobile-containers/forms/digitalProperty/dairyFarm/RegisterInseminationForm'
import { useScreenOptions } from '@smartcoop/mobile-containers/hooks/useScreenOptions'
import AuthenticatedLayout from '@smartcoop/mobile-containers/layouts/AuthenticatedLayout'
import { Container, Item, Scroll, ButtonsContainer , ScreenHeaderTitleContainer, HeaderTitle } from '@smartcoop/mobile-containers/layouts/AuthenticatedLayout/theme'
import { useSnackbar } from '@smartcoop/snackbar'
import { InseminationActions } from '@smartcoop/stores/insemination'
import { selectCurrentProperty } from '@smartcoop/stores/property/selectorProperty'

const RegisterInseminationScreen = () => {
  const formRef = useRef(null)
  const snackbar = useSnackbar()
  const dispatch = useCallback(useDispatch(), [])
  const t = useT()
  const currentProperty = useSelector(selectCurrentProperty)
  const navigation = useNavigation()
  const { setOptions } = useScreenOptions()

  const route = useRoute()
  const { insemination } = route?.params ?? {}
  const [disabledButton, setDisabledButton] = useState(false)

  // const [isLoading, setIsLoading] = useState(false)

  const isEditing = useMemo(
    () => (!isEmpty(insemination?.id)),[insemination]
  )
  const defaultValues = useMemo(
    () => (
      {
        code: '',
        name: '',
        property: {
          id: currentProperty?.id
        },
        ...insemination
      }
    ), [currentProperty, insemination]
  )

  const handleClose = useCallback(
    () => {
      navigation.goBack()
    },
    [navigation]
  )

  const onSuccess = useCallback(
    () => {
      snackbar.success(
        t(`your {this} was ${  isEditing ? 'edited' :'registered' }`, {
          howMany: 1,
          gender: 'female',
          this: t('insemination')
        })
      )
      handleClose()

    }, [handleClose, isEditing, snackbar, t]
  )

  const handleSubmit = useCallback(
    (data) => {
      // setIsLoading(true)
      dispatch(InseminationActions.saveInsemination(
        {
          ...insemination,
          ...data
        },
        onSuccess
        // () => setIsLoading(false)
      ))
    },
    [dispatch, insemination, onSuccess]
  )

  const submit = useCallback(
    () => {
      formRef.current.submit()
    },
    []
  )

  useFocusEffect(useCallback(
    () => {
      setOptions({
        headerTitle: () => (
          <ScreenHeaderTitleContainer>
            <I18n as={ HeaderTitle }>
              register insemination
            </I18n>
          </ScreenHeaderTitleContainer>
        )
      })
    },
    [setOptions]
  ))

  return (
    <AuthenticatedLayout>
      <>
        <Container>
          <Scroll padding={ 0 }>
            <Item>
              <RegisterInseminationForm
                ref={ formRef }
                onSuccess={ onSuccess }
                onSubmit={ handleSubmit }
                setDisabledButton={ setDisabledButton }
                defaultValues={ defaultValues }
                withoutSubmitButton
              />
            </Item>
          </Scroll>
          <Item>
            <ButtonsContainer style={ { paddingTop: 10 } }>
              <Button
                onPress={ handleClose }
                style={ { flex: 1 } }
                variant="outlined"
                title={ t('cancel') }
              />
              <View style={ { width: '10%' } } />
              <Button
                onPress={ submit }
                style={ { flex: 1 } }
                title={ t('save') }
                disabled={ disabledButton }
              />
            </ButtonsContainer>
          </Item>
        </Container>
      </>
    </AuthenticatedLayout>
  )
}

export default RegisterInseminationScreen
