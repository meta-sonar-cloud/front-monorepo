import React, { useState, useRef, useCallback, useMemo } from 'react'
import { View } from 'react-native'
import { useSelector, useDispatch } from 'react-redux'

import {
  useRoute,
  useNavigation,
  useFocusEffect
} from '@react-navigation/native'

import isEmpty from 'lodash/isEmpty'

import { useDialog } from '@smartcoop/dialog'
import I18n, { useT } from '@smartcoop/i18n'
import Button from '@smartcoop/mobile-components/Button'
import Loader from '@smartcoop/mobile-components/Loader'
import ConfirmModal from '@smartcoop/mobile-components/Modal/ConfirmModal'
import RegisterAnimalBirthForm from '@smartcoop/mobile-containers/forms/digitalProperty/dairyFarm/RegisterAnimalBirthForm'
import { useScreenOptions } from '@smartcoop/mobile-containers/hooks/useScreenOptions'
import AuthenticatedLayout from '@smartcoop/mobile-containers/layouts/AuthenticatedLayout'
import {
  ScreenHeaderTitleContainer,
  HeaderTitle,
  Container,
  Item,
  Scroll,
  ButtonsContainer
} from '@smartcoop/mobile-containers/layouts/AuthenticatedLayout/theme'
import { useSnackbar } from '@smartcoop/snackbar'
import { AnimalActions } from '@smartcoop/stores/animal'
import { AnimalBirthActions } from '@smartcoop/stores/animalBirth'
import { selectCurrentProperty } from '@smartcoop/stores/property/selectorProperty'

const RegisterAnimalBirthScreen = () => {
  const formRef = useRef(null)
  const snackbar = useSnackbar()
  const dispatch = useCallback(useDispatch(), [])
  const t = useT()
  const currentProperty = useSelector(selectCurrentProperty)
  const navigation = useNavigation()
  const { setOptions } = useScreenOptions()

  const [isLoading, setIsLoading] = useState(false)

  const route = useRoute()
  const { animalBirth } = route?.params ?? {}

  const { createDialog } = useDialog()

  const isEditing = useMemo(() => !isEmpty(animalBirth?.id), [animalBirth])

  const defaultValues = useMemo(
    () => ({
      code: '',
      name: '',
      property: {
        id: currentProperty?.id
      },
      ...animalBirth
    }),
    [currentProperty, animalBirth]
  )

  const handleClose = useCallback(() => {
    navigation.goBack()
  }, [navigation])

  const registerAnimalModal = useCallback(
    (response) => {
      if (response.isAlive && !isEditing) {
        createDialog({
          id: 'confirm-register-aniaml',
          Component: ConfirmModal,
          props: {
            onConfirm: () => {
              dispatch(
                AnimalActions.setCurrentAnimal({
                  ...response,
                  id: null,
                  birthDate: response.occurrenceDate,
                  animalBirthId: response.id,
                  category: 'terneira',
                  statusId: '7',
                  animalStatus: {
                    id: '7'
                  }
                })
              )
              dispatch(AnimalActions.setIsDetail(false))
              dispatch(AnimalActions.setActiveTab('registerAnimal'))
              navigation.navigate('DairyFarm', { screen: 'RegisterAnimal' })
            },
            onNegative: () => {
              handleClose()
            },
            message: t('do you want to register an animal?'),
            textButtonOnConfirm: t('yes'),
            textButtonOnNegative: t('no')
          }
        })
      }
    },
    [createDialog, dispatch, handleClose, isEditing, navigation, t]
  )

  const onSuccess = useCallback(
    (response) => {
      setIsLoading(false)
      snackbar.success(
        t(`your {this} was ${ isEditing ? 'edited' : 'registered' }`, {
          howMany: 1,
          gender: 'male',
          this: t('calving')
        })
      )
      registerAnimalModal(response)
    },
    [isEditing, registerAnimalModal, snackbar, t]
  )

  const handleSubmit = useCallback(
    (data) => {
      setIsLoading(true)
      dispatch(
        AnimalBirthActions.saveAnimalBirth(
          {
            ...animalBirth,
            ...data
          },
          (response) => onSuccess({ ...data, ...response, isNewAnimal: true }),
          (err) => {
            setIsLoading(false)
            snackbar.error(t(err))
          }
        )
      )
    },
    [dispatch, animalBirth, onSuccess, snackbar, t]
  )

  const submit = useCallback(() => {
    formRef.current.submit()
  }, [])

  useFocusEffect(
    useCallback(() => {
      setOptions({
        headerTitle: () => (
          <ScreenHeaderTitleContainer>
            <I18n as={ HeaderTitle }>register calving</I18n>
          </ScreenHeaderTitleContainer>
        )
      })
    }, [setOptions])
  )

  return (
    <AuthenticatedLayout>
      <>
        <Container>
          <Scroll padding={ 0 }>
            <Item>
              {isLoading ? (
                <Loader />
              ) : (
                <RegisterAnimalBirthForm
                  ref={ formRef }
                  onSuccess={ onSuccess }
                  onSubmit={ handleSubmit }
                  defaultValues={ defaultValues }
                  withoutSubmitButton
                />
              )}
            </Item>
          </Scroll>
          <Item>
            <ButtonsContainer style={ { paddingTop: 10 } }>
              <Button
                onPress={ handleClose }
                style={ { flex: 1 } }
                variant="outlined"
                title={ t('cancel') }
                disabled={ isLoading }
              />
              <View style={ { width: '10%' } } />
              <Button
                onPress={ submit }
                style={ { flex: 1 } }
                title={ t('save') }
                disabled={ isLoading }
              />
            </ButtonsContainer>
          </Item>
        </Container>
      </>
    </AuthenticatedLayout>
  )
}

export default RegisterAnimalBirthScreen
