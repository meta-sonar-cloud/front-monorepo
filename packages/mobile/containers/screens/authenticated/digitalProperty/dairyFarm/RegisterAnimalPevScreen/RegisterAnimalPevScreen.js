import React, { useRef, useCallback, useMemo } from 'react'
import { View } from 'react-native'
import { useDispatch } from 'react-redux'

import { useRoute, useNavigation , useFocusEffect } from '@react-navigation/native'

import isEmpty from 'lodash/isEmpty'

import I18n, { useT } from '@smartcoop/i18n'
import Button from '@smartcoop/mobile-components/Button'
import RegisterAnimalPevForm from '@smartcoop/mobile-containers/forms/digitalProperty/dairyFarm/RegisterAnimalPevForm'
import { useScreenOptions } from '@smartcoop/mobile-containers/hooks/useScreenOptions'
import AuthenticatedLayout from '@smartcoop/mobile-containers/layouts/AuthenticatedLayout'
import { ScreenHeaderTitleContainer, HeaderTitle , Container, Item, Scroll, ButtonsContainer } from '@smartcoop/mobile-containers/layouts/AuthenticatedLayout/theme'
import { useSnackbar } from '@smartcoop/snackbar'
import { AnimalActions } from '@smartcoop/stores/animal'

const RegisterAnimalPevScreen = () => {
  const formRef = useRef(null)
  const snackbar = useSnackbar()
  const dispatch = useCallback(useDispatch(), [])
  const t = useT()
  const navigation = useNavigation()
  const { setOptions } = useScreenOptions()

  const route = useRoute()
  const { pev = {} } = route?.params ?? {}

  // const [isLoading, setIsLoading] = useState(false)

  const isEditing = useMemo(
    () => (!isEmpty(pev?.id)),[pev]
  )

  const handleClose = useCallback(
    () => {
      navigation.goBack()
    },
    [navigation]
  )

  const onSuccess = useCallback(
    () => {
      snackbar.success(
        t(`your {this} was ${  isEditing ? 'edited' :'registered' }`, {
          howMany: 1,
          gender: 'male',
          this: 'PEV'
        })
      )
      handleClose()

    }, [handleClose, isEditing, snackbar, t]
  )

  const handleSubmit = useCallback(
    (data) => {
      dispatch(AnimalActions.saveAnimalPev(
        {
          ...pev,
          ...data
        },
        onSuccess
      ))
    },
    [dispatch, pev, onSuccess]
  )

  const submit = useCallback(
    () => {
      formRef.current.submit()
    },
    []
  )


  useFocusEffect(useCallback(
    () => {
      setOptions({
        headerTitle: () => (
          <ScreenHeaderTitleContainer>
            <I18n as={ HeaderTitle } params={ { this: 'PEV' } }>
              {'register {this}'}
            </I18n>
          </ScreenHeaderTitleContainer>
        )
      })
    },
    [setOptions]
  ))

  return (
    <AuthenticatedLayout>
      <>
        <Container>
          <Scroll padding={ 0 }>
            <Item>
              <RegisterAnimalPevForm
                ref={ formRef }
                onSuccess={ onSuccess }
                onSubmit={ handleSubmit }
                defaultValues={ pev }
                withoutSubmitButton
              />
            </Item>
          </Scroll>
          <Item>
            <ButtonsContainer style={ { paddingTop: 10 } }>
              <Button
                onPress={ handleClose }
                style={ { flex: 1 } }
                variant="outlined"
                title={ t('cancel') }
              />
              <View style={ { width: '10%' } } />
              <Button
                onPress={ submit }
                style={ { flex: 1 } }
                title={ t('save') }
              />
            </ButtonsContainer>
          </Item>
        </Container>
      </>
    </AuthenticatedLayout>
  )
}

export default RegisterAnimalPevScreen
