import React, { useMemo, useEffect, useCallback, useLayoutEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import { useFocusEffect, useNavigation } from '@react-navigation/native'

// import { HeaderTitle } from '@react-navigation/stack'

import isEmpty from 'lodash/isEmpty'

import I18n, { useT } from '@smartcoop/i18n'
import RegisterAnimalForm from '@smartcoop/mobile-containers/forms/digitalProperty/dairyFarm/RegisterAnimalForm'
import AnimalPevList from '@smartcoop/mobile-containers/fragments/AnimalPevList'
import { useScreenOptions } from '@smartcoop/mobile-containers/hooks/useScreenOptions'
import {
  ScreenHeaderTitleContainer,
  HeaderTitle
} from '@smartcoop/mobile-containers/layouts/AuthenticatedLayout/theme'
import ListAnimalBirth from '@smartcoop/mobile-containers/screens/authenticated/digitalProperty/dairyFarm/ListAnimalBirth'
import ListDiagnosis from '@smartcoop/mobile-containers/screens/authenticated/digitalProperty/dairyFarm/ListDiagnosis'
import ListDrying from '@smartcoop/mobile-containers/screens/authenticated/digitalProperty/dairyFarm/ListDrying'
import ListInsemination from '@smartcoop/mobile-containers/screens/authenticated/digitalProperty/dairyFarm/ListInsemination'
import { AnimalActions } from '@smartcoop/stores/animal'
import {
  selectActiveTab,
  selectCurrentAnimal
} from '@smartcoop/stores/animal/selectorAnimal'
import { colors } from '@smartcoop/styles'

import { TabButton, ScrollViewButtons, SafeAreaViewList } from './styles'

const AnimalRegisterScreen = () => {
  const t = useT()
  const navigation = useNavigation()
  const dispatch = useCallback(useDispatch(), [])
  const { setOptions } = useScreenOptions()

  const currentAnimal = useSelector(selectCurrentAnimal)
  const activeTab = useSelector(selectActiveTab)

  const disabledButton = useMemo(
    () => isEmpty(currentAnimal) || currentAnimal.isNewAnimal,
    [currentAnimal]
  )

  const renderType = useMemo(
    () =>
      ({
        registerAnimal: <RegisterAnimalForm />,
        insemination: <ListInsemination />,
        diagnosis: <ListDiagnosis />,
        drying: <ListDrying />,
        calving: <ListAnimalBirth />,
        pev: <AnimalPevList />
      }[activeTab]),
    [activeTab]
  )

  useLayoutEffect(() => {
    if (!disabledButton) {
      navigation.setOptions({
        headerTitle: (
          <ScreenHeaderTitleContainer>
            <HeaderTitle style={ { color: colors.white } }>
              {currentAnimal?.earring
                ? currentAnimal?.earring?.earringCode
                : t('register animal', { howMany: 1 })}
            </HeaderTitle>
          </ScreenHeaderTitleContainer>
        )
      })
    }
  }, [currentAnimal, disabledButton, navigation, t])

  useEffect(
    () => () => {
      dispatch(AnimalActions.resetCurrentAnimal())
      dispatch(AnimalActions.resetIsDetail())
      dispatch(AnimalActions.setActiveTab('registerAnimal'))
    },
    [dispatch]
  )

  useFocusEffect(
    useCallback(() => {
      setOptions({
        headerTitle: () => (
          <ScreenHeaderTitleContainer>
            <I18n as={ HeaderTitle } params={ { howMany: 1 } }>
              register animal
            </I18n>
          </ScreenHeaderTitleContainer>
        )
      })
    }, [setOptions])
  )

  return (
    <>
      <ScrollViewButtons horizontal>
        <TabButton
          id="button-animal"
          activeTab={ activeTab === 'registerAnimal' }
          onPress={ () => dispatch(AnimalActions.setActiveTab('registerAnimal')) }
          title={ t('register animal', { howMany: 1 }) }
        />
        <TabButton
          id="button-insemination"
          activeTab={ activeTab === 'insemination' }
          onPress={ () => dispatch(AnimalActions.setActiveTab('insemination')) }
          title={ t('insemination') }
          disabled={ disabledButton }
        />
        <TabButton
          id="button-diagnosis"
          style={ { marginRight: 10 } }
          activeTab={ activeTab === 'diagnosis' }
          onPress={ () => dispatch(AnimalActions.setActiveTab('diagnosis')) }
          title={ t('diagnosis', { howMany: 1 }) }
          disabled={ disabledButton }
        />
        <TabButton
          id="button-drying"
          style={ { marginRight: 10 } }
          activeTab={ activeTab === 'drying' }
          onPress={ () => dispatch(AnimalActions.setActiveTab('drying')) }
          title={ t('other actions') }
          disabled={ disabledButton }
        />
        <TabButton
          id="button-calving"
          style={ { marginRight: 10 } }
          activeTab={ activeTab === 'calving' }
          onPress={ () => dispatch(AnimalActions.setActiveTab('calving')) }
          title={ t('calving') }
          disabled={ disabledButton }
        />
        <TabButton
          id="button-pev"
          activeTab={ activeTab === 'pev' }
          onPress={ () => dispatch(AnimalActions.setActiveTab('pev')) }
          title="PEV"
          disabled={ disabledButton }
        />
      </ScrollViewButtons>

      <SafeAreaViewList>{renderType}</SafeAreaViewList>
    </>
  )
}

export default AnimalRegisterScreen
