import React, { useEffect, useCallback, useState, useRef, useMemo } from 'react'
import { View } from 'react-native'
import { useDispatch, useSelector } from 'react-redux'

import { useT } from '@meta-react/i18n'
import { useNavigation, useFocusEffect } from '@react-navigation/native'

import { isEmpty, values } from 'lodash'

import { useDialog } from '@smartcoop/dialog'
import Button from '@smartcoop/mobile-components/Button'
import FilterButton from '@smartcoop/mobile-components/FilterButton'
import List from '@smartcoop/mobile-components/List'
import PregnancyDiagnosticItem from '@smartcoop/mobile-components/PregnancyDiagnosticItem'
import FilterPregnancyDiagnosisModal from '@smartcoop/mobile-containers/modals/dairyFarm/FilterPregnancyDiagnosisModal'
import { AnimalActions } from '@smartcoop/stores/animal'
import { selectCurrentAnimal } from '@smartcoop/stores/animal/selectorAnimal'
import { AnimalPregnancyDiagnosticsActions } from '@smartcoop/stores/animalPregnancyDiagnostics'
import { selectAnimalPregnancyDiagnostics } from '@smartcoop/stores/animalPregnancyDiagnostics/selectorAnimalPregnancyDiagnostics'
import { InseminationActions } from '@smartcoop/stores/insemination'
import { colors } from '@smartcoop/styles'

import { Top, ButtonContainer } from './styles'

const ListDiagnosis = () => {
  const { createDialog } = useDialog()
  const t = useT()
  const dispatch = useCallback(useDispatch(), [])
  const [refresh, setRefresh] = useState(true)
  const pregnancyDiagnostics = useSelector(selectAnimalPregnancyDiagnostics)
  const currentAnimal = useSelector(selectCurrentAnimal)
  const [filters, setFilters] = useState({})
  const mounted = useRef(false)
  const navigation = useNavigation()

  const deps = useMemo(
    () => [filters, refresh],
    [filters, refresh]
  )

  const registerDisabled = useMemo(
    () => (
      currentAnimal?.animalStatus?.name !== 'Inseminada' &&
      currentAnimal?.animalStatus?.name !== 'Inseminada a confirmar' &&
      currentAnimal?.animalStatus?.name !== 'Prenha'
    ),[currentAnimal]
  )

  const handleSetFilters = useCallback(
    (filterValues) => {
      setFilters({
        ...filterValues,
        q: filterValues?.name ?? null
      })
    }, []
  )

  const openFilterModal = useCallback(
    () => {
      createDialog({
        id: 'filter-diagnosis',
        Component: FilterPregnancyDiagnosisModal,
        props: {
          onSubmit: handleSetFilters,
          filters
        }
      })
    },
    [createDialog, filters, handleSetFilters]
  )

  const loadDiagnosis = useCallback(
    (page, onSuccess, onError) => {
      dispatch(AnimalPregnancyDiagnosticsActions.loadPregnancyDiagnostics(
        { ...filters, page },
        onSuccess,
        onError
      ))
    },
    [dispatch, filters]
  )

  const renderItem = useCallback(
    ({ item }) => (
      <PregnancyDiagnosticItem
        pregnancyDiagnostic={ item }
        onEdit={ () => {navigation.navigate('DairyFarm', { screen: 'RegisterPregnancyDiagnosis', params: { pregnancyDiagnosis: item } })} }
      />
    ),
    [navigation]
  )

  useFocusEffect(
    useCallback(() => {
      if(!mounted.current) {
        mounted.current = true
      } else {
        setRefresh(old => !old)
      }
    }, [])
  )

  useEffect(
    () => {
      dispatch(InseminationActions.loadCurrentInsemination())
    },[dispatch]
  )

  useEffect(
    () => {
      dispatch(AnimalActions.loadCurrentAnimal())
    },[dispatch]
  )

  return (
    <>
      <Top>
        <FilterButton
          id="diagnosis-filter-button"
          onPress={ openFilterModal }
          isActive={ !values(filters).every(isEmpty) }
          style={ { flex: 1 } }
        />
      </Top>
      <List
        data={ pregnancyDiagnostics }
        renderItem={ renderItem }
        onListLoad={ loadDiagnosis }
        deps={ deps }
        style={ { marginBottom: 100 } }
      />
      <ButtonContainer>
        <View>
          <Button
            itemKey="code"
            id="register-diagnosis"
            backgroundColor={ colors.secondary }
            color={ colors.text }
            textStyle={ { fontSize: 14 } }
            onPress={ () => {navigation.navigate('DairyFarm', { screen: 'RegisterPregnancyDiagnosis', params: {} })} }
            title={ t('register {this}', { this: t('diagnosis', { howMany: 1 }) }) }
            disabled={ registerDisabled }
          />
        </View>
      </ButtonContainer>
    </>
  )
}

export default ListDiagnosis
