import React, { useEffect, useCallback, useState, useRef, useMemo } from 'react'
import { View } from 'react-native'
import { useDispatch, useSelector } from 'react-redux'

import { useT } from '@meta-react/i18n'
import { useNavigation, useFocusEffect } from '@react-navigation/native'

import { isEmpty, values } from 'lodash'

import { useDialog } from '@smartcoop/dialog'
import Button from '@smartcoop/mobile-components/Button'
import FilterButton from '@smartcoop/mobile-components/FilterButton'
import InseminationItem from '@smartcoop/mobile-components/InseminationItem'
import List from '@smartcoop/mobile-components/List'
import FilterInseminationModal from '@smartcoop/mobile-containers/modals/dairyFarm/FilterInseminationModal'
import { AnimalActions } from '@smartcoop/stores/animal'
import { selectCurrentAnimal } from '@smartcoop/stores/animal/selectorAnimal'
import { InseminationActions } from '@smartcoop/stores/insemination'
import { selectInseminations } from '@smartcoop/stores/insemination/selectorInsemination'
import { colors } from '@smartcoop/styles'

import { Top, ButtonContainer } from './styles'

const ListInsemination = () => {
  const { createDialog } = useDialog()
  const t = useT()
  const dispatch = useCallback(useDispatch(), [])
  const [refresh, setRefresh] = useState(true)
  const inseminations = useSelector(selectInseminations)
  const [filters, setFilters] = useState({})
  const mounted = useRef(false)
  const navigation = useNavigation()
  const currentAnimal = useSelector(selectCurrentAnimal)

  const deps = useMemo(
    () => [filters, refresh],
    [filters, refresh]
  )

  const registerDisabled = useMemo(
    () => (
      (currentAnimal?.category !== 'vaca' &&
      currentAnimal?.category !== 'novilha') ||
      (currentAnimal?.animalStatus?.name !== 'Aptas' &&
      currentAnimal?.animalStatus?.name !== 'Vazia' &&
      currentAnimal?.animalStatus?.name !== 'Nenhum')
    ),[currentAnimal]
  )

  const handleSetFilters = useCallback(
    (filterValues) => {
      setFilters({
        ...filterValues,
        q: filterValues?.name ?? null
      })
    }, []
  )

  const openFilterModal = useCallback(
    () => {
      createDialog({
        id: 'filter-fields',
        Component: FilterInseminationModal,
        props: {
          onSubmit: handleSetFilters,
          filters
        }
      })
    },
    [createDialog, filters, handleSetFilters]
  )

  const loadInseminations = useCallback(
    (page, onSuccess, onError) => {
      dispatch(InseminationActions.loadInseminations(
        { ...filters, page },
        onSuccess,
        onError
      ))
    },
    [dispatch, filters]
  )

  const renderItem = useCallback(
    ({ item }) => (

      <InseminationItem
        earringCode={ item.animal?.earring?.earringCode }
        inseminationDate={ item.inseminationDate }
        bullCode={ item.bull?.code ?? ' ' }
        bullName={ isEmpty(item.embryoBull) ? item.bull?.name : item.embryoBull }
        onEdit={ () => {navigation.navigate('DairyFarm', { screen: 'RegisterInsemination', params: { insemination: item } })} }
      />
    ),
    [navigation]
  )

  useFocusEffect(
    useCallback(() => {
      if(!mounted.current) {
        mounted.current = true
      } else {
        setRefresh(old => !old)
      }
    }, [])
  )

  useEffect(
    () => {
      dispatch(AnimalActions.loadCurrentAnimal())
    },[dispatch]
  )

  return (
    <>
      <Top>
        <FilterButton
          id="insemination-filter-button"
          onPress={ openFilterModal }
          isActive={ !values(filters).every(isEmpty) }
          style={ { flex: 1 } }
        />
      </Top>
      <List
        data={ inseminations }
        renderItem={ renderItem }
        onListLoad={ loadInseminations }
        deps={ deps }
        style={ { marginBottom: 100 } }
      />
      <ButtonContainer>
        <View>
          <Button
            itemKey="code"
            id="register-insemination"
            backgroundColor={ colors.secondary }
            color={ colors.text }
            textStyle={ { fontSize: 14 } }
            onPress={ () => {navigation.navigate('DairyFarm', { screen: 'RegisterInsemination' })} }
            title={ t('register insemination') }
            disabled={ registerDisabled }
          />
        </View>
      </ButtonContainer>
    </>
  )
}

export default ListInsemination
