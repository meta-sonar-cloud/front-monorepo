import React, { useRef, useCallback } from 'react'
import { View } from 'react-native'

import { useNavigation , useFocusEffect } from '@react-navigation/native'

import I18n, { useT } from '@smartcoop/i18n'
import Button from '@smartcoop/mobile-components/Button'
import ManagementForm from '@smartcoop/mobile-containers/forms/digitalProperty/management/ManagementForm'
import { useScreenOptions } from '@smartcoop/mobile-containers/hooks/useScreenOptions'
import { Container, Item, Scroll, ButtonsContainer , ScreenHeaderTitleContainer, HeaderTitle } from '@smartcoop/mobile-containers/layouts/AuthenticatedLayout/theme'
import { useSnackbar } from '@smartcoop/snackbar'

const CropManagementRegisterScreen = () => {
  const ManagementFormRef = useRef(null)
  const navigation = useNavigation()
  const t = useT()
  const snackbar = useSnackbar()
  const { setOptions } = useScreenOptions()

  const submitForms = useCallback(
    () => {
      ManagementFormRef.current.submit()
    },
    []
  )

  const handleClose = useCallback(
    () => {
      navigation.goBack()
    },
    [navigation]
  )

  const onSuccess = useCallback(
    () => {
      snackbar.success(
        t('your {this} was registered', {
          howMany: 1,
          gender: 'male',
          this: t('cropManagement', { howMany: 1 })
        })
      )
      handleClose()
    },
    [handleClose, snackbar, t]
  )

  useFocusEffect(useCallback(
    () => {
      setOptions({
        headerTitle: () => (
          <ScreenHeaderTitleContainer>
            <I18n as={ HeaderTitle }>
              register management
            </I18n>
          </ScreenHeaderTitleContainer>
        )
      })
    },
    [setOptions]
  ))

  return (
    <Container>
      <Scroll padding={ 0 }>
        <Item
          style={ {
            flex: 1,
            paddingTop: 20,
            paddingBottom: 0
          } }
        >
          <ManagementForm
            ref={ ManagementFormRef }
            onSuccess={ onSuccess }
            withoutSubmitButton
          />
        </Item>
      </Scroll>

      <Item>
        <ButtonsContainer style={ { paddingTop: 10 } }>
          <Button
            onPress={ handleClose }
            style={ { flex: 1 } }
            variant="outlined"
            title={ t('cancel') }
          />

          <View style={ { width: '10%' } } />

          <Button
            onPress={ submitForms }
            style={ { flex: 1 } }
            title={ t('save') }
          />
        </ButtonsContainer>
      </Item>
    </Container>
  )}

export default CropManagementRegisterScreen
