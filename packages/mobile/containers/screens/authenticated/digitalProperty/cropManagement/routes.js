import React from 'react'

import { useNavigation } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'

import { useScreenOptions, withScreenOptions } from '@smartcoop/mobile-containers/hooks/useScreenOptions'

import CropManagementDetail from './CropManagementDetail'
import CropManagementRegisterScreen from './CropManagementRegisterScreen'

const Stack = createStackNavigator()

const CropManagementScreenRouter = () => {
  const navigation = useNavigation()
  const { options } = useScreenOptions(navigation)

  return (
    <Stack.Navigator screenOptions={ options } mode="modal">
      <Stack.Screen
        name="Register"
        component={ CropManagementRegisterScreen }
      />

      <Stack.Screen
        name="Detail"
        component={ CropManagementDetail }
      />
    </Stack.Navigator>
  )
}

export default withScreenOptions(CropManagementScreenRouter)
