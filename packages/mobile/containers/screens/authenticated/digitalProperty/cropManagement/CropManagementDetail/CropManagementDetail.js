import React, { useCallback, useMemo, useEffect, useState } from 'react'
import { View, Text } from 'react-native'
import { useDispatch } from 'react-redux'

import { useRoute, useNavigation , useFocusEffect } from '@react-navigation/native'
import moment from 'moment/moment'

import isEmpty from 'lodash/isEmpty'
import map from 'lodash/map'
import upperFirst from 'lodash/upperFirst'

import { useDialog } from '@smartcoop/dialog'
import I18n, { useT } from '@smartcoop/i18n'
import { arrowDownField, arrowUpField, trash, calendar, dropPercentage, wind, rain } from '@smartcoop/icons'
import Button from '@smartcoop/mobile-components/Button'
import Divider from '@smartcoop/mobile-components/Divider'
import Icon from '@smartcoop/mobile-components/Icon'
import Loader from '@smartcoop/mobile-components/Loader'
import ConfirmModal from '@smartcoop/mobile-components/Modal/ConfirmModal'
import { useScreenOptions } from '@smartcoop/mobile-containers/hooks/useScreenOptions'
import { Container, Item, Scroll , ScreenHeaderTitleContainer, HeaderTitle } from '@smartcoop/mobile-containers/layouts/AuthenticatedLayout/theme'
import CompleteCropManagementModal from '@smartcoop/mobile-containers/modals/CompleteCropManagementModal'
import useSmartcoopApi from '@smartcoop/services/hooks/useSmartcoopApi'
import { useSnackbar } from '@smartcoop/snackbar'
import { FieldActions } from '@smartcoop/stores/field'
import colors from '@smartcoop/styles/colors'
import { momentBackDateFormat, momentFriendlyDateFormat } from '@smartcoop/utils/dates'

import {
  WheaterManagement,
  TemperatureContent,
  ButtonRegisterCropManagement,
  LoaderContainer
} from './styles'

const CropManagementDetail = () => {
  const route = useRoute()
  const navigation = useNavigation()
  const t = useT()
  const { createDialog } = useDialog()
  const dispatch = useCallback(useDispatch(), [])
  const snackbar = useSnackbar()
  const { setOptions } = useScreenOptions()

  const [loading, setLoading] = useState(false)
  const [loadingAction, setLoadingActions] = useState(false)

  const {
    canConcludeGrowingSeason,
    currentCropManagement: externalCropManagementData
  } = route.params


  const { data: remoteData, mutate } = useSmartcoopApi(`/growing-season/${ externalCropManagementData.growingSeasonId }/crops-management/${ externalCropManagementData.id }`)

  const currentCropManagement = useMemo(
    () => ({
      ...externalCropManagementData,
      ...remoteData
    }),
    [externalCropManagementData, remoteData]
  )

  useFocusEffect(useCallback(
    () => {
      setOptions({
        headerTitle: () => (
          <ScreenHeaderTitleContainer>
            <HeaderTitle style={ { color: colors.white } }>
              {currentCropManagement.operation.name}
            </HeaderTitle>
          </ScreenHeaderTitleContainer>
        )
      })
    },
    [currentCropManagement.operation.name, setOptions]
  ))

  const onSuccess = useCallback(
    () => {
      navigation.goBack()
    },
    [navigation]
  )

  const handleCropManagementDelete = useCallback(
    () => {
      createDialog({
        id: 'confirm-delete-cropManagement',
        Component: ConfirmModal,
        props: {
          onConfirm: () => {
            setLoadingActions(true)
            dispatch(FieldActions.deleteOfflineCropManagement(
              currentCropManagement.growingSeasonId,
              currentCropManagement.id,
              () => {
                setLoadingActions(false)
                snackbar.success(
                  t('your {this} was deleted', {
                    howMany: 1,
                    gender: 'male',
                    this: t('cropManagement', { howMany: 1 })
                  })
                )
                onSuccess()
              },
              () => {
                setLoadingActions(false)
              }
            ))
          },
          message: t('are you sure you want to delete the {this}?', {
            gender: 'male',
            howMany: 1,
            this: t('cropManagement', { howMany: 1 })
          })
        }
      })
    },
    [createDialog, currentCropManagement, dispatch, onSuccess, snackbar, t]
  )

  const handleCompleteCropManagement = useCallback(
    () => {
      createDialog({
        id: 'complete-crop-management',
        Component: CompleteCropManagementModal,
        props: {
          currentCropManagement,
          onSuccess,
          canConcludeGrowingSeason
        }
      })
    },
    [canConcludeGrowingSeason, createDialog, currentCropManagement, onSuccess]
  )

  const loaderSuccess = useMemo(
    () => (
      <View style={ { width: '100%', height: '100%', zIndex: 10 } }>
        <LoaderContainer>
          <Loader
            message={
              t('deleting {this}', {
                gender: 'male',
                this: t('cropManagement', { howMany: 1 })
              })
            }
          />
        </LoaderContainer>
      </View>
    ),
    [t]
  )

  useEffect(() => {
    mutate()
  }, [externalCropManagementData, mutate])

  useEffect(() => {
    setLoading(true)
    if(!isEmpty(remoteData)){
      setLoading(false)
    }
  }, [remoteData])


  return (
    <Container>
      {loadingAction && loaderSuccess}
      {!loadingAction && <ButtonRegisterCropManagement>
        <Button
          onPress={ handleCompleteCropManagement }
          color={ colors.text }
          backgroundColor={ colors.secondary }
          title={ currentCropManagement.realizationDate ? t('completed') : t('complete operation') }
          disabled={ !currentCropManagement || currentCropManagement.realizationDate }
        />
      </ButtonRegisterCropManagement>}
      <Scroll padding={ 0 }>
        <Item
          style={ {
            flex: 1,
            paddingTop: 20,
            paddingBottom: 10
          } }
        >
          {loading ? (
            <Loader
              message={
                t('loading {this}', {
                  this: t('weather forecast')
                })
              }
              width={ 70 }
            />
          ) : (
            currentCropManagement.weather && <WheaterManagement>
              <View style={ { paddingBottom: 10 } }>
                <Text style={ { fontWeight: 'bold' } }>
                  <I18n>forecast for realization date</I18n>
                </Text>
                <Text>
                  {moment(currentCropManagement.predictedDate, momentBackDateFormat).format('DD [de] MMMM, dddd')}
                </Text>
              </View>
              <View style={ { flexDirection: 'row', justifyContent: 'space-between' } }>
                <View>
                  <TemperatureContent>
                    <Text>Máx </Text>
                    <Text>{currentCropManagement.weather.high}°C </Text>
                    <View>
                      <Icon size={ 9 } icon={ arrowUpField } color={ colors.orange } />
                    </View>
                  </TemperatureContent>
                  <TemperatureContent>
                    <Text>Min </Text>
                    <Text>{currentCropManagement.weather.low}°C </Text>
                    <View>
                      <Icon size={ 9 } icon={ arrowDownField } color={ colors.blue } />
                    </View>
                  </TemperatureContent>
                </View>
                <View>
                  <View style={ { flexDirection: 'row', alignItems: 'center' } }>
                    <Icon size={ 9 } icon={ dropPercentage } color={ colors.blue } />
                    <Text>{currentCropManagement.weather.humidity}%</Text>
                  </View>
                  <View style={ { flexDirection: 'row', alignItems: 'center' } }>
                    <Icon size={ 10 } icon={ wind } color={ colors.blue } style={ { marginRight: 3 } } />
                    <Text>{currentCropManagement.weather.wind} km/h</Text>
                  </View>
                </View>
                <View>
                  <View style={ { flexDirection: 'row', alignItems: 'center' } } >
                    <Icon size={ 10 } icon={ rain } color={ colors.blue } style={ { marginRight: 3 } } />
                    <Text>{currentCropManagement.weather.precipitation}mm</Text>
                  </View>
                </View>
              </View>
            </WheaterManagement>)}
        </Item>
        <Divider />
        <Item
          style={ {
            flex: 1,
            paddingTop: 10,
            paddingBottom: 0
          } }
        >
          <View style={ { flexDirection: 'row', flexWrap: 'wrap' } }>
            {/* <Button
              variant="outlined"
              style={ { width: 100, marginRight: 15, borderColor: colors.lightGrey } }
              textStyle={ { paddingTop: 5, paddingBottom: 5 } }
              color={ colors.black }
              icon={ <Icon size={ 16 } icon={ pencil } color={ colors.black } /> }
              title={ <I18n>edit</I18n> }
              disabled
            /> */}
            <Button
              variant='outlined'
              style={ { width: 36, borderColor: colors.lightGrey } }
              icon={ <Icon size={ 16 } icon={ trash } color={ colors.red } /> }
              onPress={ handleCropManagementDelete }
            />
          </View>
        </Item>
        <Item
          style={ {
            flex: 1,
            paddingTop: 10,
            paddingBottom: 0
          } }
        >
          <View>
            <View style={ { paddingBottom: 10 } }>
              <Text style={ { fontWeight: 'bold', color: colors.black } }>
                <I18n>date for realization</I18n>
              </Text>
              <View>
                <View style={ { flexDirection: 'row', alignItems: 'center' } }>
                  <Text style={ { color: colors.black } }>
                    <I18n>scheduled to:</I18n>
                  </Text>
                  <Icon style={ { marginRight: 5, marginLeft: 3 } } size={ 12 } icon={ calendar } color={ colors.green }/>
                  <Text style={ { fontSize: 16, color: colors.black } }>{moment(currentCropManagement.predictedDate, momentBackDateFormat).format(momentFriendlyDateFormat)}</Text>
                </View>
                {currentCropManagement.realizationDate && (
                  <View style={ { flexDirection: 'row', alignItems: 'center' } }>
                    <Text style={ { color: colors.black } }>
                      <I18n>held in:</I18n>
                    </Text>
                    <Icon style={ { marginRight: 5, marginLeft: 3 } } size={ 12 } icon={ calendar } color={ colors.grey }/>
                    <Text style={ { fontSize: 16, color: colors.black } }>{moment(currentCropManagement.realizationDate, momentBackDateFormat).format(momentFriendlyDateFormat)}</Text>
                  </View>
                )}
              </View>
            </View>
            <View>
              <View style={ { flexDirection: 'row', flexWrap: 'wrap' } }>
                {!isEmpty(currentCropManagement.cropManagementItem) && (
                  map(currentCropManagement.cropManagementItem.items, (value, key) => (
                    <View style={ { paddingRight: 40, paddingBottom: 20 } } key={ key }>
                      <View>
                        <Text style={ { fontWeight: 'bold', color: colors.black } }><I18n>{key}</I18n></Text>
                      </View>
                      <View>
                        <Text style={ { fontSize: 16, color: colors.black } }>{value.name ? value.name : upperFirst(value)}</Text>
                      </View>
                    </View>
                  )))}
              </View>
              {!isEmpty(currentCropManagement.observations) && (
                <View>
                  <Text style={ { fontWeight: 'bold', color: colors.black } }>Observações</Text>
                  <View>
                    <Text style={ { fontSize: 16, color: colors.black } }>{currentCropManagement.observations}</Text>
                  </View>
                </View>
              )}
            </View>
          </View>
        </Item>
      </Scroll>
    </Container>
  )}

export default CropManagementDetail
