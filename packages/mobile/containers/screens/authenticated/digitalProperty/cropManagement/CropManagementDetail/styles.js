import styled from 'styled-components/native'

export const WheaterManagement = styled.View`
  font-weight: 600;
`

export const TemperatureContent = styled.View`
  flex-direction: row;
  align-items: center;
`

export const LoaderContainer = styled.View`
  position: absolute;
  bottom: 0;
  z-index: 3;
  width: 50%;
  left: 25%;
  padding-bottom: 100%;
`

export const ButtonRegisterCropManagement = styled.View`
  position: absolute;
  bottom: 0;
  z-index: 2;
  width: 50%;
  left: 25%;
  padding-bottom: 20px;
`
