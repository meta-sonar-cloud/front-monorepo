import React from 'react'

import { useNavigation } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'

import { useScreenOptions, withScreenOptions } from '@smartcoop/mobile-containers/hooks/useScreenOptions'

import PestReportMapScreen from './PestReportMapScreen/PestReportMapScreen'
import PestReportRegisterScreen from './PestReportRegisterScreen/PestReportRegisterScreen'

const Stack = createStackNavigator()

const PestReportScreenRouter = () => {
  const navigation = useNavigation()
  const { options } = useScreenOptions(navigation)

  return (
    <Stack.Navigator screenOptions={ options } mode="modal">
      <Stack.Screen
        name="Map"
        component={ PestReportMapScreen }
      />

      <Stack.Screen
        name="Register"
        component={ PestReportRegisterScreen }
      />
    </Stack.Navigator>
  )
}


export default withScreenOptions(PestReportScreenRouter)
