import React, { useState, useCallback, useMemo, useEffect } from 'react'

import Geolocation from '@react-native-community/geolocation'
import { useNavigation, CommonActions, useRoute, useFocusEffect } from '@react-navigation/native'

import isUndefined from 'lodash/isUndefined'

import I18n, { useT } from '@smartcoop/i18n'
import { bugMarker as bugMarkerIcon } from '@smartcoop/icons'
import Button from '@smartcoop/mobile-components/Button'
import Maps from '@smartcoop/mobile-components/Maps'
import Polygon from '@smartcoop/mobile-components/Maps/components/Polygon'
import PinMarker from '@smartcoop/mobile-components/Maps/markers/PinMarker'
import { useScreenOptions } from '@smartcoop/mobile-containers/hooks/useScreenOptions'
import { Container, Item, ButtonsContainer , ScreenHeaderTitleContainer, HeaderTitle } from '@smartcoop/mobile-containers/layouts/AuthenticatedLayout/theme'
import colors from '@smartcoop/styles/colors'
import  { getCenterCoordinates, isPointInside } from '@smartcoop/utils/maps'

import {
  InfoCardContainer,
  InfoCard,
  InfoText,
  OverlayButtonContainer
} from './styles'

const PestReportMapScreen = () => {

  const navigation = useNavigation()
  const t = useT()
  const route = useRoute()
  const { setOptions } = useScreenOptions()

  const { coordinates, pestReport } = route.params

  const [markerCoordinate, setMarkerCoordinate] = useState(pestReport?.pinLocation ?? undefined)


  const [currentRegion, setCurrentRegion] = useState({})

  const onChangeRegion = useCallback(
    (newRegion) => {
      setCurrentRegion(old => {
        if (
          newRegion.latitude !== old.latitude
          && newRegion.longitude !== old.longitude
        ) {
          return { ...old, ...newRegion }
        }
        return old
      })
    },
    []
  )

  const currentLocation = { latitude: currentRegion?.latitude, longitude: currentRegion?.longitude }

  const handlerMarkerMoved = useCallback((event) => {
    setMarkerCoordinate({
      latitude: event.nativeEvent.coordinate.latitude,
      longitude: event.nativeEvent.coordinate.longitude
    })
  }, [])

  const isInside = useMemo(
    () => isPointInside(currentLocation, !isUndefined(coordinates) ? coordinates :  []),
    [coordinates, currentLocation]
  )

  const region = useMemo(
    () => coordinates ? getCenterCoordinates(coordinates) : {},
    [coordinates]
  )

  useEffect(() => {
    Geolocation.getCurrentPosition(
      location => onChangeRegion(location.coords),
      // eslint-disable-next-line no-console
      error => console.error(error),
      { enableHighAccuracy: true }
    )
  }, [onChangeRegion])

  useEffect(() => {
    if(isInside) {
      setMarkerCoordinate(currentLocation)
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  },[isInside])

  useFocusEffect(useCallback(
    () => {
      setOptions({
        headerTitle: () => (
          <ScreenHeaderTitleContainer>
            <I18n as={ HeaderTitle }>
              report occurrence
            </I18n>
          </ScreenHeaderTitleContainer>
        )
      })
    },
    [setOptions]
  ))

  return (
    <Container>
      <Item
        style={ {
          flex: 1,
          paddingTop: 0,
          paddingBottom: 0
        } }
      >
        <Maps
          region={ region }report o
          onPress={ handlerMarkerMoved }
          overlayButton={
            <>
              <InfoCardContainer>
                <InfoCard>
                  <InfoText>click and drag the pin to the desired location</InfoText>
                </InfoCard>
              </InfoCardContainer>
              <OverlayButtonContainer>
                <ButtonsContainer>
                  <Button
                    title={ t('cancel') }
                    onPress={ () => navigation.dispatch(CommonActions.goBack()) }
                    style={ { maxwidth: '60%', marginRight: 10 } }
                    variant="outlined"
                  />
                  <Button
                    title={ t('confirm') }
                    onPress={ () => navigation.navigate('PestReport',
                      {
                        screen: 'Register',
                        params: {
                          pinLocation: { ...markerCoordinate },
                          polygonCoordinates: { ...coordinates },
                          pestReport: { ...pestReport }
                        }
                      })
                    }
                    backgroundColor={  colors.secondary }
                    color={ colors.text }
                  />
                </ButtonsContainer>
              </OverlayButtonContainer>
            </>
          }
        >
          <PinMarker
            coordinate={ markerCoordinate }
            draggable
            onDragEnd={ handlerMarkerMoved }
            customIcon={ bugMarkerIcon }
            size={ 30 }
            anchor={ {
              x: .5,
              y: .9
            } }
          />
          <Polygon points={ coordinates } />
        </Maps>
      </Item>
    </Container>

  )
}

export default PestReportMapScreen
