import React, { useCallback, useRef, useEffect, useMemo } from 'react'
import { View, Platform, PermissionsAndroid } from 'react-native'
import { useDispatch, useSelector } from 'react-redux'

import { useRoute , useNavigation , useFocusEffect } from '@react-navigation/native'

import isEmpty from 'lodash/isEmpty'
import map from 'lodash/map'

import { useDialog } from '@smartcoop/dialog'
import I18n, { useT } from '@smartcoop/i18n'
import { noImage } from '@smartcoop/icons'
import Button from '@smartcoop/mobile-components/Button'
import EmptyState from '@smartcoop/mobile-components/EmptyState'
import ConfirmModal from '@smartcoop/mobile-components/Modal/ConfirmModal'
import ThumbnailImage from '@smartcoop/mobile-components/ThumbnailImage'
import PestReportForm from '@smartcoop/mobile-containers/forms/digitalProperty/pestReport/PestReportForm/PestReportForm'
import useFile from '@smartcoop/mobile-containers/hooks/useFile'
import { useScreenOptions } from '@smartcoop/mobile-containers/hooks/useScreenOptions'
import AuthenticatedLayout from '@smartcoop/mobile-containers/layouts/AuthenticatedLayout'
import { Container, Item, Scroll, ButtonsContainer, Title , ScreenHeaderTitleContainer, HeaderTitle } from '@smartcoop/mobile-containers/layouts/AuthenticatedLayout/theme'
import SelectedImageTypeModal from '@smartcoop/mobile-containers/modals/SelectedImageTypeModal'
import { useSnackbar } from '@smartcoop/snackbar'
import { FieldActions } from '@smartcoop/stores/field'
import { selectCurrentField } from '@smartcoop/stores/field/selectorField'
import colors from '@smartcoop/styles/colors'

import { Gallery } from './styles'

const PestReportRegisterScreen = () => {
  const pestReportFormRef = useRef(null)
  const snackbar = useSnackbar()
  const navigation = useNavigation()
  const t = useT()
  const route = useRoute()
  const { setOptions } = useScreenOptions()

  const { pinLocation } = route.params

  const { growingSeason, id: fieldId } = useSelector(selectCurrentField)

  const statePestReport = useMemo(
    () => route?.params?.pestReport ?? {}, [route]
  )
  const dispatch = useCallback(useDispatch(), [])
  const { createDialog } = useDialog()
  const {
    selectedFiles,
    receivedFiles,
    isEmpty: isEmptyFiles,
    handleAdd,
    handleAddCamera,
    createFormData,
    handleRemove
  } = useFile([], statePestReport?.reportImages ?? [])

  const maxNumberFile = 20

  useEffect(() => {
    const requestCameraPermission = async () => {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.CAMERA,
          {
            title: 'Cool Photo App Camera Permission',
            message:
              'Cool Photo App needs access to your camera ' +
              'so you can take awesome pictures.',
            buttonNeutral: 'Ask Me Later',
            buttonNegative: 'Cancel',
            buttonPositive: 'OK'
          }
        )
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          console.log('You can use the camera')
        } else {
          console.log('Camera permission denied')
        }
      } catch (err) {
        console.warn(err)
      }
    }
    Platform.OS === 'android' && requestCameraPermission()
  }, [])


  const selectedCameraOrGallery = useCallback(
    () => {
      if(selectedFiles.length + receivedFiles.length < maxNumberFile) {
        createDialog({
          id: 'open-selected-image-type',
          Component: SelectedImageTypeModal,
          props: {
            openCamera: handleAddCamera,
            openGallery: handleAdd
          }
        })
      } else {
        snackbar.error(
          t('the number of files exceeds the limit of {this}', {
            this: maxNumberFile
          })
        )
      }

    }, [createDialog, handleAdd, handleAddCamera, receivedFiles, selectedFiles, snackbar, t]
  )

  const defaultValues = useMemo(
    () => ({
      reportTypes: {
        slug: ''
      },
      ...statePestReport,
      specification: statePestReport[statePestReport?.reportTypes?.slug]?.id ?? '',
      description: statePestReport?.description ?? ''
    }), [statePestReport]
  )

  const isEditing = useMemo(
    () => (!isEmpty(defaultValues?.id)), [defaultValues]
  )

  const submit = useCallback(
    () => {
      pestReportFormRef.current.submit()
    },
    []
  )

  const handleClose = useCallback(
    () => {
      navigation.navigate('Field', { screen: 'Details', params: { fieldId } })
    },
    [fieldId, navigation]
  )

  const handleGoBack = useCallback(
    () => {
      navigation.navigate('PestReport',
        {
          screen: 'Map'
        }
      )
    },
    [navigation]
  )

  const onSuccess = useCallback(
    () => {
      snackbar.success(
        t(`your {this} was ${  isEditing ? 'edited' :'registered' }`, {
          howMany: 1,
          gender: 'male',
          this: t('report', { howMany: 1 })
        })
      )
      handleClose()
    },
    [handleClose, isEditing, snackbar, t]
  )

  const submitNewFiles = useCallback(
    (updatedPestReport) => {
      if (isEmpty(selectedFiles)) {
        onSuccess()
      } else {
        dispatch(FieldActions.addPestReportFiles(createFormData(), updatedPestReport.id,
          onSuccess
        ))
      }
    },
    [createFormData, dispatch, onSuccess, selectedFiles]
  )

  const submitOldFiles = useCallback(
    (updatedPestReport) => {
      dispatch(FieldActions.editPestReportFiles(receivedFiles, updatedPestReport.id,
        () => submitNewFiles(updatedPestReport)
      ))
    },
    [dispatch, receivedFiles, submitNewFiles]
  )

  const submitForms = useCallback(
    (data) => {
      if(isEditing) {
        dispatch(FieldActions.updateOfflinePestReport(
          {
            ...data,
            pinLocation
          },
          defaultValues.id,
          (updatedPestReport) => submitOldFiles(updatedPestReport)
        ))
      } else {
        dispatch(FieldActions.saveOfflinePestReport(
          {
            ...data,
            pinLocation
          },
          growingSeason.id,
          (updatedPestReport) => submitNewFiles(updatedPestReport)
        ))
      }
    },
    [defaultValues, dispatch, growingSeason, isEditing, pinLocation, submitNewFiles, submitOldFiles]
  )

  const handleClickClose = useCallback(
    (list, removedFile) => {
      createDialog({
        id: 'confirm-delete',
        Component: ConfirmModal,
        props: {
          onConfirm: () => handleRemove(list, removedFile),
          message: t('are you sure you want to delete the {this}?', {
            howMany: 1,
            gender: 'female',
            this: t('image', { howMany: 1 })
          })
        }
      })
    },
    [createDialog, handleRemove, t]
  )

  const renderFiles = useCallback(
    () => {
      const elements = map(receivedFiles, (file) => (
        <ThumbnailImage
          key={ file.id }
          size={ 100 }
          src={ file.fileUrl }
          onClose={ () => handleClickClose('receivedFiles', file) }
        />
      )).concat(
        map(selectedFiles, (file, index) => (
          <ThumbnailImage
            key={ `${ index }-${ file.uri }` }
            size={ 100 }
            src={ file.uri }
            onClose={ () => handleClickClose('selectedFiles', file) }
          />
        ))
      )

      return elements
    },
    [handleClickClose, receivedFiles, selectedFiles]
  )

  const renderDOMFiles = useMemo(
    () => (isEmptyFiles ?
      <EmptyState
        text={ t('no image added') }
        icon={ noImage }
        action={ {
          text: t('add images') ,
          onClick: selectedCameraOrGallery
        } }
      />
      :
      renderFiles()
    ), [isEmptyFiles, renderFiles, selectedCameraOrGallery, t]
  )

  const renderButton = useMemo(
    () => (!isEmptyFiles &&
    <Button
      onPress={ selectedCameraOrGallery }
      style={ { flex: 1 } }
      color={ colors.text }
      backgroundColor={ colors.secondary }
      title={ t('add images') }
    />
    )  , [isEmptyFiles, selectedCameraOrGallery, t]
  )

  useFocusEffect(useCallback(
    () => {
      setOptions({
        headerTitle: () => (
          <ScreenHeaderTitleContainer>
            <I18n as={ HeaderTitle }>
              report occurrence
            </I18n>
          </ScreenHeaderTitleContainer>
        )
      })
    },
    [setOptions]
  ))

  return (
    <AuthenticatedLayout>
      <>
        <Container>
          <Scroll padding={ 0 }>
            <Item
              style={ {
                flex: 1,
                paddingTop: 0,
                paddingBottom: 0
              } }
            >
              <PestReportForm
                ref={ pestReportFormRef }
                onSubmit={ submitForms }
                withoutSubmitButton
                isEditing={ isEditing }
                defaultValues={ defaultValues }
              />
              <Title><I18n params={ { howMany: 2 } }>image</I18n></Title>
              <Gallery isCenter={ isEmptyFiles }>
                {renderDOMFiles}
              </Gallery>
              {renderButton}
            </Item>
          </Scroll>

          <Item>
            <ButtonsContainer style={ { paddingTop: 10 } }>
              <Button
                onPress={ handleGoBack }
                style={ { flex: 1 } }
                variant="outlined"
                title={ t('go back') }
              />

              <View style={ { width: '10%' } } />

              <Button
                onPress={ submit }
                style={ { flex: 1 } }
                title={ t('save') }
              />
            </ButtonsContainer>
          </Item>
        </Container>
      </>
    </AuthenticatedLayout>
  )
}

export default PestReportRegisterScreen
