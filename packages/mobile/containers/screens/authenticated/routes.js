import React, { useEffect, useCallback } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import { createStackNavigator } from '@react-navigation/stack'

import isEmpty from 'lodash/isEmpty'

import { useScreenOptions, withScreenOptions } from '@smartcoop/mobile-containers/hooks/useScreenOptions'
import { AVAILABLE_MODULES } from '@smartcoop/stores/module'
import { selectCurrentModule } from '@smartcoop/stores/module/selectorModule'
import { UserActions } from '@smartcoop/stores/user'

import AdministrationScreens from './administration/routes'
import ChooseModuleScreen from './ChooseModuleScreen'
import BarterScreen from './commercialization/barter'
import QuotationsScreen from './commercialization/quotations'
import CommercializationScreens from './commercialization/routes'
import CommonRoutes from './common/routes'
import CropManagementScreens from './digitalProperty/cropManagement'
import DairyFarmScreens from './digitalProperty/dairyFarm'
import FieldScreens from './digitalProperty/field'
import GrowingSeasonScreens from './digitalProperty/growingSeason'
import MachineryScreens from './digitalProperty/machinery'
import PestReportScreens from './digitalProperty/pestReport'
import PropertyScreens from './digitalProperty/property'
import DigitalPropertyScreens from './digitalProperty/routes'
import SocialScreens from './social/routes'
import TechnicalScreens from './technical/routes'
import TechnicalVisitRegisterScreens from './technical/TechnicalVisitRegisterScreen/TechnicalVisitRegisterScreen'
import WeatherStationsScreen from './weatherStation'

const Stack = createStackNavigator()

const AuthenticatedScreensRouter = () => {
  const dispatch = useCallback(useDispatch(), [])
  const { options } = useScreenOptions()

  const currentModule = useSelector(selectCurrentModule)

  useEffect(() => {
    dispatch(UserActions.loadUser())
  }, [dispatch])

  return (
    <Stack.Navigator screenOptions={ options }>
      {isEmpty(currentModule)
        ? <Stack.Screen name="ChooseModule" component={ ChooseModuleScreen } />
        : (
          <>
            {currentModule === AVAILABLE_MODULES.digitalProperty && (
              <>
                {/* tabs */}
                <Stack.Screen name="DigitalProperty" component={ DigitalPropertyScreens } />

                <Stack.Screen name="Property" component={ PropertyScreens } />
                <Stack.Screen name="Machinery" component={ MachineryScreens } />
                <Stack.Screen name="DairyFarm" component={ DairyFarmScreens } />
                <Stack.Screen name="Field" component={ FieldScreens } />
                <Stack.Screen name="GrowingSeason" component={ GrowingSeasonScreens } />
                <Stack.Screen name="PestReport" component={ PestReportScreens } />
                <Stack.Screen name="CropManagement" component={ CropManagementScreens } />
              </>
            )}

            {currentModule === AVAILABLE_MODULES.commercialization && (
              <>
                {/* tabs */}
                <Stack.Screen name="Commercialization" component={ CommercializationScreens } />

                <Stack.Screen name="Quotation" component={ QuotationsScreen } />
                <Stack.Screen name="Barter" component={ BarterScreen } />
              </>
            )}

            {currentModule === AVAILABLE_MODULES.technical && (
              <>
                {/* tabs */}
                <Stack.Screen name="Technical" component={ TechnicalScreens } />

                <Stack.Screen name="Property" component={ PropertyScreens } />
                <Stack.Screen name="DairyFarm" component={ DairyFarmScreens } />
                <Stack.Screen name="Field" component={ FieldScreens } />
                <Stack.Screen name="GrowingSeason" component={ GrowingSeasonScreens } />
                <Stack.Screen name="PestReport" component={ PestReportScreens } />
                <Stack.Screen name="CropManagement" component={ CropManagementScreens } />
                <Stack.Screen name="TechnicalRegisterVisit" component={ TechnicalVisitRegisterScreens } />
                <Stack.Screen name="WeatherStation" component={ WeatherStationsScreen } />
              </>
            )}

            {currentModule === AVAILABLE_MODULES.administration && (
              <>
                {/* tabs */}
                <Stack.Screen name="Administration" component={ AdministrationScreens } />

                <Stack.Screen name="WeatherStation" component={ WeatherStationsScreen } />
              </>
            )}

            <Stack.Screen name="Common" component={ CommonRoutes } />
            <Stack.Screen name="Social" component={ SocialScreens } />
          </>
        )}
    </Stack.Navigator>
  )
}

export default withScreenOptions(AuthenticatedScreensRouter)
