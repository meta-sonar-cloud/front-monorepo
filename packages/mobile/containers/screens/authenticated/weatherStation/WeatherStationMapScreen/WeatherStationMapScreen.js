import React, { useCallback, useEffect, useMemo } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import { useNavigation } from '@react-navigation/native'

import { toNumber } from 'lodash'
import debounce from 'lodash/debounce'
import isEmpty from 'lodash/isEmpty'
import map from 'lodash/map'

import { wirelessMarker } from '@smartcoop/icons'
import Maps from '@smartcoop/mobile-components/Maps'
import PinMarker from '@smartcoop/mobile-components/Maps/markers/PinMarker'
import { Container, Item } from '@smartcoop/mobile-containers/layouts/AuthenticatedLayout/theme'
import { selectCurrentOrganization } from '@smartcoop/stores/organization/selectorOrganization'
import { WeatherStationsActions } from '@smartcoop/stores/weatherStations'
import { selectWeatherStations } from '@smartcoop/stores/weatherStations/selectorWeatherStations'
import { getCenterCoordinates } from '@smartcoop/utils/maps'

const WeatherStationMapScreen = () => {
  const dispatch = useCallback(useDispatch(), [])

  const weatherStations = useSelector(selectWeatherStations)
  const currentOrganization = useSelector(selectCurrentOrganization)
  const navigation = useNavigation()

  const allPolygonCoordinates = useMemo(
    () => map(
      weatherStations,
      weatherStation => [ weatherStation.geolocalization.latitude, weatherStation.geolocalization.longitude]
    ),
    [weatherStations]
  )

  const region = useMemo(
    () =>
      !isEmpty(allPolygonCoordinates)
        ? getCenterCoordinates(allPolygonCoordinates)
        : undefined,
    [allPolygonCoordinates]
  )


  const clickStation = useCallback(
    (weatherStation) => {
      navigation.navigate('WeatherStation', { screen: 'Details', params: { weatherStationId: weatherStation.id } })
    },
    [navigation]
  )


  const loadFields = useCallback(
    debounce(() => {
      dispatch(
        WeatherStationsActions.loadWeatherStations()
      )
    }, 300),
    [dispatch]
  )

  useEffect(() => {
    loadFields()
  }, [dispatch, currentOrganization, loadFields])

  return (
    <Container>
      <Item
        style={ {
          flex: 1,
          paddingTop: 20,
          paddingBottom: 0
        } }
      >
        <Maps
          region={ region }
        >
          {!isEmpty(weatherStations) && (
            map(weatherStations, weatherStation => (
              <>
                <PinMarker
                  key={ weatherStation?.agrosmartId }
                  coordinate={ {
                    latitude: toNumber(weatherStation.geolocalization.latitude),
                    longitude: toNumber(weatherStation.geolocalization.longitude)
                  } }
                  customIcon={ wirelessMarker }
                  size={ 20 }
                  onPress={ () => clickStation(weatherStation) }
                />
              </>
            ))
          )}
        </Maps>
      </Item>
    </Container>
  )
}

export default WeatherStationMapScreen


