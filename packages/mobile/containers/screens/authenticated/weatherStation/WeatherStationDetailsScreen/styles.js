import { Caption, Subheading } from 'react-native-paper'

import styled from 'styled-components/native'

import colors from '@smartcoop/styles/colors'

export const HeadTitleItem = styled(Subheading)`
  font-weight: bold;
`

export const TextData = styled(Subheading)`
  line-height: 35px;
  color: ${ colors.black };
`

export const ItemData = styled.View`
  flex-direction: row;
  justify-content: space-between;
`

export const ItemSelect = styled.View`
  flex-direction: row;
  align-items: center;
`

export const LeftData = styled.View`
  flex-direction: row;
  align-items: center;
`

export const RightData = styled.View`
  flex-direction: row;
  align-items: center;
`

export const DataWeatherStation = styled.View`

`

export const TextWeatherStation = styled(Subheading)`

`

export const TextUpdate = styled(Caption)`
  padding-left: 20px;
`
