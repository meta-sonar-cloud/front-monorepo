import React, { useCallback, useEffect, useState, useMemo } from 'react'
import {
  useDispatch,
  useSelector
} from 'react-redux'

import { useRoute , useFocusEffect } from '@react-navigation/native'
import moment from 'moment/moment'

import camelCase from 'lodash/camelCase'
import filter from 'lodash/filter'
import find from 'lodash/find'
import isEmpty from 'lodash/isEmpty'
import map from 'lodash/map'

import I18n, { useT } from '@smartcoop/i18n'
import {
  rain,
  windmill,
  thermometer,
  sun,
  compass,
  dropPercentage,
  wind,
  battery } from '@smartcoop/icons'
import Divider from '@smartcoop/mobile-components/Divider'
import EmptyState from '@smartcoop/mobile-components/EmptyState'
import Icon from '@smartcoop/mobile-components/Icon'
import InputSelect from '@smartcoop/mobile-components/InputSelect'
import Loader from '@smartcoop/mobile-components/Loader'
import { useScreenOptions } from '@smartcoop/mobile-containers/hooks/useScreenOptions'
import { Container, Item, Scroll , ScreenHeaderTitleContainer, HeaderTitle } from '@smartcoop/mobile-containers/layouts/AuthenticatedLayout/theme'
import { getAllWeatherStations as getAllWeatherStationsService } from '@smartcoop/services/apis/smartcoopApi/resources/weatherStations'
import { WeatherStationsActions } from '@smartcoop/stores/weatherStations'
import { selectCurrentWeatherStation } from '@smartcoop/stores/weatherStations/selectorWeatherStations'
import colors from '@smartcoop/styles/colors'

import {
  HeadTitleItem,
  TextData,
  ItemData,
  LeftData,
  RightData,
  DataWeatherStation,
  TextWeatherStation,
  TextUpdate
} from './styles'



const WeatherStationDetailsScreen = () => {
  const route = useRoute()
  const { weatherStationId } = route.params
  const t = useT()
  const { setOptions } = useScreenOptions()

  const dispatch = useCallback(useDispatch(), [])
  const currentWeatherStation = useSelector(selectCurrentWeatherStation)
  const [currentStationId, setCurrentStationId] = useState(weatherStationId)
  const [loading, setLoading] = useState(false)

  const currentItem = useMemo(
    () => ({
      temperaturaDoAr: {
        icon: thermometer,
        color: colors.orange
      },
      direcaoDoVento: {
        icon: compass,
        color: colors.red
      },
      umidadeDoAr: {
        icon: dropPercentage,
        color: colors.blue
      },
      velocidadeDoVento: {
        icon: windmill,
        color: colors.darkGrey
      },
      radiacaoSolarMedia: {
        icon: sun,
        color: colors.yellow
      },
      rajadaDeVento: {
        icon: wind,
        color: colors.blue
      },
      temperaturaDoSolo: {
        icon: thermometer,
        color: colors.orange
      },
      tensaoDaBateria: {
        icon: battery,
        color: colors.green
      },
      precipitacao: {
        icon: rain,
        color: colors.blue
      }
    }),
    []
  )

  const temperatureSoil = useMemo(
    () => find(currentWeatherStation.descriptions, { description: 'Temperatura do Solo' }),
    [currentWeatherStation]
  )

  const arrayItemDescriptions = useMemo(
    () => filter(currentWeatherStation.descriptions, item => item.description !== 'Temperatura do Solo'),
    [currentWeatherStation]
  )

  const loadFields = useCallback(
    () => {
      setLoading(true)
      dispatch(
        WeatherStationsActions.loadCurrentWeatherStation(
          currentStationId,
          () => setLoading(false),
          () => setLoading(false)
        )
      )
    },
    [currentStationId, dispatch]
  )

  useEffect(() => {
    dispatch(WeatherStationsActions.resetCurrentWeatherStation())
  }, [dispatch, currentStationId])

  useEffect(() => {
    loadFields()
  }, [dispatch, loadFields])

  useFocusEffect(useCallback(
    () => {
      setOptions({
        headerTitle: () => (
          <ScreenHeaderTitleContainer>
            <I18n as={ HeaderTitle } params={ { howMany: 1 } }>
              weather station
            </I18n>
          </ScreenHeaderTitleContainer>
        )
      })
    },
    [setOptions]
  ))

  return (
    <Scroll padding={ 0 }>
      <Container>
        <Item
          style={ {
            flex: 1,
            paddingTop: 20,
            paddingLeft: 20,
            paddingRight: 20,
            paddingBottom: 0
          } }
        >
          <InputSelect
            detached
            label={ t('select station') }
            name="weatherStation"
            options={ getAllWeatherStationsService }
            value={ currentStationId }
            onChange={ setCurrentStationId }
          />
        </Item>

        {loading ? (
          <Loader
            message={
              <I18n>loading</I18n>
            }
          />
        ) : (
          <>
            { !currentWeatherStation?.equipmentStatus?.online ?
              <EmptyState
                text={ t('this equipment is disabled') }
              /> :
              <>
                <Divider />

                <Item
                  style={ {
                    flex: 1,
                    paddingTop: 20,
                    paddingLeft: 20,
                    paddingRight: 20,
                    paddingBottom: 0
                  } }
                >
                  <I18n
                    as={ HeadTitleItem }
                    params={ { howMany: 1 } }
                    style={ { paddingBottom: 8 } }
                  >
                weather station
                  </I18n>

                  {!isEmpty(temperatureSoil) && (
                    <ItemData>
                      <DataWeatherStation>
                        <HeadTitleItem>
                          {temperatureSoil?.description}
                        </HeadTitleItem>
                        <TextWeatherStation>
                          {temperatureSoil?.value} {temperatureSoil?.unit}
                        </TextWeatherStation>
                      </DataWeatherStation>
                      <DataWeatherStation>
                        <I18n as={ HeadTitleItem }>
                  depth
                        </I18n>
                        <TextWeatherStation>
                          {temperatureSoil.depth}
                        </TextWeatherStation>
                      </DataWeatherStation>
                    </ItemData>
                  )}
                </Item>

                <Divider />

                <Item
                  style={ {
                    flex: 1,
                    paddingLeft: 20,
                    paddingRight: 20,
                    paddingBottom: 0
                  } }
                >
                  <I18n as={ HeadTitleItem }>
                precipitation data
                  </I18n>
                  {!isEmpty(arrayItemDescriptions) && (
                    map(arrayItemDescriptions, itemData => (
                      <ItemData key={ itemData.name }>
                        <LeftData>
                          <Icon
                            icon={ currentItem[camelCase(itemData?.description)].icon }
                            size={ 18 }
                            style={ { marginRight: 5 } }
                            color={ currentItem[camelCase(itemData?.description)].color }
                          />
                          <TextData>
                            {itemData.description}
                          </TextData>
                        </LeftData>
                        <RightData>
                          <TextData>
                            {itemData.value} {itemData.unit}
                          </TextData>
                        </RightData>
                      </ItemData>
                    ))
                  )}

                </Item>
                <Divider />
                <TextUpdate>
                  {moment(currentWeatherStation?.data?.measuredAt, 'YYYY-MM-DDTHH:mm:ss.SSSSZ').format('DD/MM/YYYY [às] HH:mm:ss')}

                </TextUpdate>
              </>
            }
          </>
        )}
      </Container>
    </Scroll>
  )}

export default WeatherStationDetailsScreen
