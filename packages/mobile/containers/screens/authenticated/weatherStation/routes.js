import React from 'react'

import { useNavigation } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'

import { useScreenOptions, withScreenOptions } from '@smartcoop/mobile-containers/hooks/useScreenOptions'

import WeatherStationDetailsScreen from './WeatherStationDetailsScreen'

const Stack = createStackNavigator()

const WeatherStationsScreenRouter = () => {
  const navigation = useNavigation()
  const { options } = useScreenOptions(navigation)

  return (
    <Stack.Navigator screenOptions={ options }>
      <Stack.Screen
        name="Details"
        component={ WeatherStationDetailsScreen }
      />
    </Stack.Navigator>
  )
}

export default withScreenOptions(WeatherStationsScreenRouter)
