import React from 'react'

import { useNavigation } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'

import { useScreenOptions, withScreenOptions } from '@smartcoop/mobile-containers/hooks/useScreenOptions'

import BarterDetailsScreen from './BarterDetailsScreen'
import BarterPackageDetailsScreen from './BarterPackageDetailsScreen'
import CreateBarterScreen from './CreateBarterScreen'

const Stack = createStackNavigator()

// eslint-disable-next-line react/prop-types
const BarterRouter = () => {
  const navigation = useNavigation()
  const { options } = useScreenOptions(navigation)

  return (
    <Stack.Navigator screenOptions={ options } mode="modal">
      <Stack.Screen
        name="CreateBarterScreen"
        component={ CreateBarterScreen }
      />

      <Stack.Screen
        name="BarterDetailsScreen"
        component={ BarterDetailsScreen }
      />

      <Stack.Screen
        name="BarterPackageDetailsScreen"
        component={ BarterPackageDetailsScreen }
      />
    </Stack.Navigator>
  )
}

export default withScreenOptions(BarterRouter)
