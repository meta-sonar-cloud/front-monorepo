import React, { useState, useCallback, useMemo } from 'react'
import { View } from 'react-native'
import { useDispatch , useSelector } from 'react-redux'

import PropTypes from 'prop-types'

import { useDialog } from '@smartcoop/dialog'
import I18n, { useT } from '@smartcoop/i18n'
import Button from '@smartcoop/mobile-components/Button'
import InputQuantity from '@smartcoop/mobile-components/InputQuantity'
import InputSelect from '@smartcoop/mobile-components/InputSelect'
import SignatureOrderModal from '@smartcoop/mobile-containers/modals/Commercialization/SignatureOrderModal'
import { getUserStateRegistrations as getUserStateRegistrationsService } from '@smartcoop/services/apis/smartcoopApi/resources/stateRegistration'
import { useSnackbar } from '@smartcoop/snackbar'
import { BarterActions } from '@smartcoop/stores/barter'
import { selectCurrentOrganization } from '@smartcoop/stores/organization/selectorOrganization'
import { colors } from '@smartcoop/styles'

import {
  Container,
  Label,
  Row,
  Text,
  Divisor
} from './styles'

const Footer = ({
  barterId,
  price,
  measureUnit,
  onSuccess
}) => {
  const t = useT()
  const dispatch = useCallback(useDispatch(), [])
  const snackbar = useSnackbar()
  const { createDialog } = useDialog()

  const [packageQuantity, setPackageQuantity] = useState(1)
  const [stateRegistrationId, setStateRegistrationId] = useState()
  const [error, setError] = useState()

  const currentOrganization = useSelector(selectCurrentOrganization)

  const totalValue = useMemo(
    () => price ? packageQuantity*price : 0,
    [packageQuantity, price]
  )

  const urlParams = useMemo(
    () => ({ organizationId: currentOrganization.id }),
    [currentOrganization.id]
  )

  const onParticipate = useCallback(
    () => {
      if (!stateRegistrationId) {
        setError(t('required field'))
      } else {
        setError()
        createDialog({
          id: 'signature-order-modal',
          Component: SignatureOrderModal,
          props: {
            onSuccess: () => {
              dispatch(BarterActions.joinBarterPackage(
                { barterId, packageQuantity, stateRegistrationId },
                () => {
                  snackbar.success(t('your {this} was registered', {
                    this: t('barter'),
                    gender: 'male',
                    howMany: 1
                  }))
                },
                (err) => snackbar.error(err)
              ))
              onSuccess()
            }
          }
        })
      }
    },
    [stateRegistrationId, t, createDialog, dispatch, barterId, packageQuantity, onSuccess, snackbar]
  )

  return (
    <>
      <Divisor/>
      <Container style={ { padding: 20 } }>
        <Row style={ { alignItems: 'center' } }>
          <I18n as={ Label } style={ { fontSize: 16, color: colors.black } } params={ { this: t('package quantity'), that: '' } }>{'{this}: {that}'}</I18n>
          <View>
            <InputQuantity
              value={ packageQuantity }
              onChange={ setPackageQuantity }
              min={ 1 }
              max={ 999 }
              style={ { maxHeight: 35 } }
            />
          </View>
        </Row>
        {Number(price) > 0 && (
          <Row style={ { alignItems: 'center' } }>
            <I18n  as={ Label } style={ { fontSize: 16, color: colors.black } } params={ { howMany: 1 } }>total value</I18n>
            <Text style={ { marginLeft: 5 } }>{totalValue} {measureUnit}</Text>
          </Row>
        )}
        <InputSelect
          value={ stateRegistrationId }
          onChange={ (value) => setStateRegistrationId(value) }
          label={ t('state registration') }
          urlParams={ urlParams }
          options={ getUserStateRegistrationsService }
          asyncOptionLabelField="stateRegistration"
          asyncOptionValueField="id"
          error={ error }
          detached
        />
        <Row>
          <Button
            onPress={ onParticipate }
            backgroundColor={ colors.secondary }
            color={ colors.black }
            title={ <I18n>participate</I18n> }
            style={ { flex: 1 } }
          />
        </Row>
      </Container>
    </>
  )
}

Footer.propTypes = {
  barterId: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  price: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  measureUnit: PropTypes.string,
  onSuccess: PropTypes.func
}

Footer.defaultProps = {
  onSuccess: () => {},
  price: '',
  measureUnit: ''
}

export default Footer
