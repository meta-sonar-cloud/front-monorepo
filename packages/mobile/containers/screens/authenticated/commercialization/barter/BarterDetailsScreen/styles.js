import { Subheading, Paragraph } from 'react-native-paper'

import styled from 'styled-components/native'

import colors from '@smartcoop/styles/colors'

export const Text = styled(Subheading)`
  font-family: 'Montserrat';
`

export const Label = styled(Paragraph)`
  color: ${ colors.darkGrey };
  font-weight: bold;
  font-family: 'Montserrat';
`

export const Container = styled.View`
  flex: 1;
`

export const Divisor = styled.View`
  width: 100%;
  border: 0.5px solid #E0E0E0;
  margin: 5px 0;
`

export const Row = styled.View`
  flex-direction: row;
  flex: 1;
`

export const Column = styled.View`
  flex-direction: column;
  flex: 1;
`

export const Section = styled(Column)`
  padding: 20px 20px;
`

export const LeftItem = styled.View`
  flex: 1;
  flex-direction: row;
  justify-content: flex-start;
  align-items: center;
`

export const RightItem = styled.View`
  flex: 1;
  flex-direction: row;
  justify-content: flex-end;
`