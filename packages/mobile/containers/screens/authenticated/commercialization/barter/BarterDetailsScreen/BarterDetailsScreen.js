import React, { useMemo, useCallback } from 'react'
import { View } from 'react-native'
import { useSelector } from 'react-redux'

import { useFocusEffect } from '@react-navigation/native'
import moment from 'moment/moment'
import PropTypes from 'prop-types'

import capitalize from 'lodash/capitalize'
import isEmpty from 'lodash/isEmpty'
import map from 'lodash/map'
import omitBy from 'lodash/omitBy'
import size from 'lodash/size'

import I18n, { useT } from '@smartcoop/i18n'
import { organizationRounded } from '@smartcoop/icons'
import Icon from '@smartcoop/mobile-components/Icon'
import Loader from '@smartcoop/mobile-components/Loader'
import { useScreenOptions } from '@smartcoop/mobile-containers/hooks/useScreenOptions'
import { Scroll , ScreenHeaderTitleContainer, HeaderTitle } from '@smartcoop/mobile-containers/layouts/AuthenticatedLayout/theme'
import useSmartcoopApi from '@smartcoop/services/hooks/useSmartcoopApi'
import { selectCurrentOrganization } from '@smartcoop/stores/organization/selectorOrganization'
import { secondaryUnit } from '@smartcoop/utils/barter'
import { momentBackDateTimeFormat } from '@smartcoop/utils/dates'
import { formatNumber } from '@smartcoop/utils/formatters'

import Footer from './Footer'
import {
  Row,
  Column,
  Container,
  Divisor,
  Text,
  Section,
  Label
} from './styles'

const BarterDetailsScreen = ({ route, navigation }) => {
  const { barterOrderNumber, packageJoinMode } = route.params

  const t = useT()
  const { setOptions } = useScreenOptions()

  const currentOrganization = useSelector(selectCurrentOrganization)

  const { data: barterRequest, isValidating } = useSmartcoopApi(packageJoinMode ? `/barters/package/${ barterOrderNumber }/organization/${ currentOrganization.id }` : `/barters/${ barterOrderNumber }/organization/${ currentOrganization.id }`)

  const barter = useMemo(
    () => barterRequest || {},
    [barterRequest]
  )

  const isPackage = useMemo(
    () => barter?.packageType,
    [barter.packageType]
  )

  const organizationName = useMemo(
    () => currentOrganization.tradeName,
    [currentOrganization.tradeName]
  )

  const barterProducts = useMemo(
    () => {
      const types = {
        fertilizantes: [],
        herbicidas: [],
        fungicidas: [],
        inseticidas: [],
        sementes: [],
        outros: []
      }

      map(barter.barterProducts, (item) => types[item.productGroupName].push(item))

      return omitBy(types, (item) => isEmpty(item))
    },
    [barter]
  )

  useFocusEffect(useCallback(
    () => {
      setOptions({
        headerTitle: () => (
          <ScreenHeaderTitleContainer>
            <I18n
              as={ HeaderTitle }
              params={ { this: t('barter') } }
            >
              {'informations {this}'}
            </I18n>
          </ScreenHeaderTitleContainer>
        )
      })
    },
    [setOptions, t]
  ))

  return (
    <Container>
      {isValidating
        ? <Loader width={ 100 }/>
        : (
          <>
            <View style={ {
              flex: ((barter?.status?.statusName === 'Cancelada' && !barter.packageType) || !barter.packageType) ? 7 : 4
            } }
            >
              <Scroll>
                <Section>
                  <Row>
                    <Column>
                      <Text style={ { fontWeight: 'bold' } }>{barter.barterName || t('my package')}</Text>
                      {barter.packageType && (
                        <Row style={ { justifyContent: 'flex-start', alignItems: 'center' } }>
                          <Icon icon={ organizationRounded } size={ 20 } />
                          <Text style={ { paddingLeft: 10 } }>{organizationName}</Text>
                        </Row>
                      )}
                    </Column>

                    <Column style={ { alignItems: 'flex-end' } }>
                      <I18n as={ Label }>code</I18n>
                      <Text>{barterOrderNumber}</Text>
                    </Column>
                  </Row>

                  <Row style={ { paddingTop: 5 } }>
                    <Column>
                      <I18n as={ Label }>launch</I18n>
                      <Text>{moment(barter.releaseDate, momentBackDateTimeFormat).format('DD/MM/YYYY')}</Text>
                    </Column>

                    {barter.cropName && (
                      <Column style={ { alignItems: 'flex-end' } }>
                        <I18n as={ Label } params={ { howMany: 1 } }>crop</I18n>
                        <Text>{barter.cropName}</Text>
                      </Column>
                    )}
                  </Row>

                  <Row style={ { paddingTop: 10 } }>
                    {isPackage && Number(barter.price) >= 0 && (
                      <Column>
                        <I18n as={ Label } params={ { howMany: 1 } }>price</I18n>
                        <Text>{barter.price} {barter.measureUnit}</Text>
                      </Column>
                    )}
                  </Row>
                </Section>

                <Divisor style={ { marginTop: 10 } }/>

                <View>
                  {map(barterProducts, (item, type) => (
                    <Container key={ type }>
                      <Row style={ { paddingLeft: 20, paddingRight: 20 } }>
                        <Label>{capitalize(type)}</Label>
                      </Row>
                      {map(item, ({ productName, measureUnit, productQuantity, unitOfMeasuresForConversion, conversionFactor }, index) => (
                        <Row key={ index } style={ { justifyContent: 'space-between', paddingLeft: 20, paddingRight: 20 } }>
                          <Row style={ { flex: 2 } }>
                            <Text>{productName}</Text>
                          </Row>
                          <Row style={ { flex: 1, justifyContent: 'flex-end' } }>
                            <Text>{`${ formatNumber(productQuantity)  } `}</Text>
                            <Text>{secondaryUnit(unitOfMeasuresForConversion, conversionFactor, measureUnit)}</Text>
                          </Row>
                        </Row>
                      ))}
                      { type !== Object.keys(barterProducts)[size(barterProducts)-1] && (
                        <Divisor style={ { marginTop: 10, marginBottom: 10 } }/>
                      )}
                    </Container>
                  ))}
                </View>
              </Scroll>
            </View>

            <Footer
              isPackage={ barter.packageType }
              packageQuantity={ barter.packageQuantity }
              price={ barter.price }
              measureUnit={ barter?.measureUnit }
              barterOrderNumber={ barterOrderNumber }
              onSuccess={ () => navigation.goBack() }
              handleEditBarter={ () => navigation.navigate('Barter', { screen: 'CreateBarterScreen', params: { barterOrderNumber } }) }
              disabled={ barter?.status?.statusName !== 'Aberta' }
              status={ barter.status }
            />
          </>
        )}
    </Container>
  )}

BarterDetailsScreen.propTypes = {
  route: PropTypes.object.isRequired,
  navigation: PropTypes.object.isRequired
}

export default BarterDetailsScreen
