import React, { useState, useCallback, useMemo } from 'react'
import { View } from 'react-native'
import { useDispatch } from 'react-redux'

import PropTypes from 'prop-types'

import isEmpty from 'lodash/isEmpty'

import { useDialog } from '@smartcoop/dialog'
import I18n, { useT } from '@smartcoop/i18n'
import { edit, trash } from '@smartcoop/icons'
import Badge from '@smartcoop/mobile-components/Badge'
import Button from '@smartcoop/mobile-components/Button'
import Icon from '@smartcoop/mobile-components/Icon'
import InputQuantity from '@smartcoop/mobile-components/InputQuantity'
import ConfirmModal from '@smartcoop/mobile-components/Modal/ConfirmModal'
import { ButtonsContainer } from '@smartcoop/mobile-containers/layouts/AuthenticatedLayout/theme'
import SignatureOrderModal from '@smartcoop/mobile-containers/modals/Commercialization/SignatureOrderModal'
import { useSnackbar } from '@smartcoop/snackbar'
import { BarterActions } from '@smartcoop/stores/barter'
import { colors } from '@smartcoop/styles'

import {
  Container,
  Label,
  Text,
  Row,
  Divisor
} from './styles'

const Footer = ({
  barterOrderNumber,
  packageQuantity: externalPackageQuantity,
  isPackage,
  price,
  onSuccess,
  handleEditBarter,
  measureUnit,
  status,
  disabled
}) => {
  const t = useT()
  const dispatch = useCallback(useDispatch(), [])
  const snackbar = useSnackbar()
  const { createDialog } = useDialog()

  const [packageQuantity, setPackageQuantity] = useState(externalPackageQuantity)
  const [isEditing, setIsEditing] = useState(false)

  const totalValue = useMemo(
    () => price ? `${ packageQuantity*price } ${ measureUnit }` : 0,
    [measureUnit, packageQuantity, price]
  )

  const onEditMode = useCallback(
    () => {
      if (isPackage) {
        setIsEditing(true)

      } else {
        handleEditBarter()
      }
    },
    [handleEditBarter, isPackage]
  )

  const onRemove = useCallback(
    () => {
      createDialog({
        id: 'confirm-delete',
        Component: ConfirmModal,
        props: {
          onConfirm: () => {
            createDialog({
              id: 'signature-order-modal',
              Component: SignatureOrderModal,
              props: {
                onSuccess: () => {
                  dispatch(BarterActions.deleteBarter(
                    barterOrderNumber,
                    () => {
                      snackbar.success(t('your {this} was deleted', {
                        this: t('barter'),
                        gender: 'male',
                        howMany: 1
                      }))
                    },
                    (error) => snackbar.error(error)
                  ))
                  onSuccess()
                }
              }
            })
          },
          message: t('are you sure you want to delete the {this}?', {
            howMany: 1,
            gender: 'male',
            this: t('barter')
          })
        }
      })
    },
    [barterOrderNumber, onSuccess, createDialog, dispatch, snackbar, t]
  )

  const onEditSave = useCallback(
    () => {
      createDialog({
        id: 'signature-order-modal',
        Component: SignatureOrderModal,
        props: {
          onSuccess: () => {
            dispatch(BarterActions.joinBarterPackage(
              {
                barterOrderNumber,
                packageQuantity
              },
              () => {
                snackbar.success(t('your {this} was edited', {
                  this: t('barter'),
                  gender: 'male',
                  howMany: 1
                }))
              },
              (error) => snackbar.error(error)
            ))
            onSuccess()
          }
        }
      })
    },
    [createDialog, dispatch, barterOrderNumber, packageQuantity, onSuccess, snackbar, t]
  )

  return (
    <>
      <Divisor/>
      <Container style={ { padding: 20, paddingTop: 10, paddingBottom: 10 } }>
        {isPackage && (
          <>
            <Row style={ { alignItems: 'center' } }>
              <I18n as={ Label } style={ { fontSize: 16, color: colors.black } } params={ { this: t('package quantity'), that: !isEditing ? packageQuantity : '' } }>{'{this}: {that}'}</I18n>
              {isEditing && (
                <View>
                  <InputQuantity
                    value={ packageQuantity }
                    onChange={ setPackageQuantity }
                    min={ 1 }
                    max={ 999 }
                  />
                </View>
              )}
            </Row>
            {isPackage && Number(price) > 0 && (
              <Row style={ { alignItems: 'center' } }>
                <I18n  as={ Label } style={ { fontSize: 16, color: colors.black } } params={ { howMany: 1 } }>total value</I18n>
                <Text style={ { marginLeft: 5 } }>{totalValue}</Text>
              </Row>
            )}
          </>
        )}
        <Row style={ { justifyContent: 'space-between', alignItems: 'center' } }>
          {!isEmpty(status) && (
            <View>
              <Badge textStyle={ { fontSize: 16 } } color={ status.statusColor }>
                {status.statusName}
              </Badge>
            </View>
          )}
          {!isEditing && (
            <ButtonsContainer style={ { justifyContent: 'flex-end', width: 'auto' } }>
              <Button
                onPress={ onEditMode }
                backgroundColor={ colors.white }
                color={ colors.black }
                title={
                  <I18n>edit</I18n>
                }
                icon={
                  <Icon icon={ edit } size={ 14 } />
                }
                disabled={ disabled }
              />
              <Button
                onPress={ onRemove }
                backgroundColor={ colors.white }
                icon={
                  <Icon icon={ trash } size={ 14 } color={ colors.red } />
                }
                style={ { height: 40, marginLeft: 10 } }
                disabled={ disabled }
              />
            </ButtonsContainer>
          )}
          {isEditing && (
            <Button
              onPress={ onEditSave }
              backgroundColor={ colors.secondary }
              color={ colors.black }
              title={
                <I18n>save</I18n>
              }
              disabled={ disabled }
            />
          )}
        </Row>
      </Container>
    </>
  )
}

Footer.propTypes = {
  barterOrderNumber: PropTypes.number.isRequired,
  packageQuantity: PropTypes.number,
  isPackage: PropTypes.bool,
  onSuccess: PropTypes.func,
  handleEditBarter: PropTypes.func,
  measureUnit: PropTypes.string,
  price: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  status: PropTypes.object,
  disabled: PropTypes.bool
}

Footer.defaultProps = {
  isPackage: false,
  packageQuantity: 0,
  price: '',
  onSuccess: () => {},
  handleEditBarter: () => {},
  measureUnit: '',
  status: {},
  disabled: false
}

export default Footer