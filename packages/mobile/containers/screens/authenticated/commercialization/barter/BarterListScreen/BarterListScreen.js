import React, { useMemo, useState, useCallback } from 'react'

import debounce from 'lodash/debounce'

import { useT } from '@smartcoop/i18n'
import InputSearch from '@smartcoop/mobile-components/InputSearch'
import BarterListFragment from '@smartcoop/mobile-containers/fragments/commercialization/BarterListFragment'
import BarterPackageListFragment from '@smartcoop/mobile-containers/fragments/commercialization/BarterPackageListFragment'

import {
  InputView,
  SafeAreaViewList,
  ScrollViewButtons,
  TabButton
} from './styles'

const BarterListScreen = () => {
  const [activeTab, setActiveTab] = useState('barter')
  const [filterText, setFilterText] = useState('')
  const [debouncedFilterText, setDebouncedFilterText] = useState('')

  const t = useT()

  const params = useMemo(
    () => ({
      q: debouncedFilterText
    }), [debouncedFilterText]
  )

  const debouncedChangeSearchFilter = useCallback(
    debounce((value) => {
      setDebouncedFilterText(value)
    }, 300), []
  )

  const onChangeSearchFilter = useCallback(
    (value) => {
      setFilterText(value)
      debouncedChangeSearchFilter(value)
    }, [debouncedChangeSearchFilter]
  )

  const activeList = useMemo(
    () => {
      switch (activeTab) {
        case 'barter':
          return (
            <BarterListFragment params={ params }/>
          )
        case 'packages':
        default:
          return (
            <BarterPackageListFragment params={ params }/>
          )
      }
    },
    [activeTab, params]
  )

  return (
    <>
      <ScrollViewButtons horizontal>
        <TabButton
          activeTab={ activeTab === 'barter' }
          onPress={ () => setActiveTab('barter') }
          title={ t('barter') }
        />
        <TabButton
          activeTab={ activeTab === 'packages' }
          onPress={ () => setActiveTab('packages') }
          title={ t('package', { howMany: 2 }) }
        />
      </ScrollViewButtons>

      <InputView>
        <InputSearch
          detached
          label="Pesquisar"
          name="search"
          value={ filterText }
          onChange={ onChangeSearchFilter }
        />
      </InputView>
      <SafeAreaViewList>
        {activeList}
      </SafeAreaViewList>
    </>
  )
}

export default BarterListScreen
