import React, { useState, useCallback, useMemo, useEffect, useRef } from 'react'
import { View } from 'react-native'
import { useDispatch, useSelector } from 'react-redux'

import { useFocusEffect } from '@react-navigation/native'
import PropTypes from 'prop-types'

import { isNumber } from 'lodash'
import capitalize from 'lodash/capitalize'
import filter from 'lodash/filter'
import flow from 'lodash/fp/flow'
import mapValuesFP from 'lodash/fp/mapValues'
import omitByFP from 'lodash/fp/omitBy'
import isEmpty from 'lodash/isEmpty'
import join from 'lodash/join'
import map from 'lodash/map'
import omitBy from 'lodash/omitBy'
import size from 'lodash/size'
import uniqBy from 'lodash/uniqBy'

import { useDialog } from '@smartcoop/dialog'
import I18n, { useT } from '@smartcoop/i18n'
import Button from '@smartcoop/mobile-components/Button'
import Loader from '@smartcoop/mobile-components/Loader'
import ConfirmModal from '@smartcoop/mobile-components/Modal/ConfirmModal'
import BarterDetailsForm from '@smartcoop/mobile-containers/forms/commercialization/barter/BarterDetailsForm'
import CreateBarterFragment from '@smartcoop/mobile-containers/fragments/commercialization/CreateBarterFragment'
import { useScreenOptions } from '@smartcoop/mobile-containers/hooks/useScreenOptions'
import { ButtonsContainer, Scroll , ScreenHeaderTitleContainer, HeaderTitle } from '@smartcoop/mobile-containers/layouts/AuthenticatedLayout/theme'
import SignatureOrderModal from '@smartcoop/mobile-containers/modals/Commercialization/SignatureOrderModal'
import useSmartcoopApi from '@smartcoop/services/hooks/useSmartcoopApi'
import { useSnackbar } from '@smartcoop/snackbar'
import { BarterActions } from '@smartcoop/stores/barter'
import { selectCurrentOrganization } from '@smartcoop/stores/organization/selectorOrganization'

import {
  Container,
  Body,
  Divisor
} from './styles'

const CreateBarterScreen = ({ route, navigation }) => {
  const t = useT()
  const { createDialog } = useDialog()
  const dispatch = useCallback(useDispatch(), [])
  const snackbar = useSnackbar()
  const formRef = useRef(null)
  const { setOptions } = useScreenOptions()

  const currentOrganization = useSelector(selectCurrentOrganization)

  const { barterOrderNumber } = (route?.params || {})

  const { data: barter, isValidating } = useSmartcoopApi(barterOrderNumber ? `/barters/${ barterOrderNumber }/organization/${ currentOrganization.id }` : '')

  const [onEditMounted, setOnEditMounted] = useState(false)

  const [result, setResult] = useState({
    fertilizantes: [
      { productInternalCode: '', unit: '', quantity: '' }
    ],
    herbicidas: [
      { productInternalCode: '', unit: '', quantity: '' }
    ],
    fungicidas: [
      { productInternalCode: '', unit: '', quantity: '' }
    ],
    inseticidas: [
      { productInternalCode: '', unit: '', quantity: '' }
    ],
    sementes: [
      { productInternalCode: '', unit: '', quantity: '' }
    ],
    outros: [
      { productInternalCode: '', unit: '', quantity: '' }
    ]
  })

  const [details, setDetails] = useState({
    barterName: '',
    cropInternalCode: '',
    stateRegistration: ''
  })

  const barterDetails = useMemo(
    () => details,
    [details]
  )

  const loaderActive = useMemo(
    () => isEmpty(barter) && isValidating,
    [barter, isValidating]
  )

  const handleError = useCallback(
    (errorMessage) => {
      createDialog({
        id: 'errors-modal',
        Component: ConfirmModal,
        props: {
          message: errorMessage,
          textButtonOnConfirm: t('ok'),
          hideCancel: true
        }
      })
    },
    [createDialog, t]
  )

  const submit = useCallback(
    (products, formData) => {
      const barterProducts = []

      map(products, (data, name) => map(data, ({ productInternalCode, unit, quantity }) => {
        barterProducts.push({
          productGroupName: name,
          productInternalCode,
          measureUnit: unit,
          productQuantity: quantity
        })
      }))

      if (size(barterProducts) < 1) {
        handleError(t('select at least one {this}', { gender: 'male', this: t('product') }))
      } else {
        createDialog({
          id: 'signature-order-modal',
          Component: SignatureOrderModal,
          props: {
            onSuccess: () => {
              dispatch(BarterActions.saveProducerBarter(
                {
                  barterProducts,
                  barterOrderNumber,
                  ...formData
                },
                () => {
                  snackbar.success(t(barterOrderNumber ? 'your {this} was edited' : 'your {this} was registered', {
                    this: t('barter'),
                    gender: 'male',
                    howMany: 1
                  }))
                  navigation.navigate('Barters', { screen: 'BarterListScreen' })
                },
                (error) => snackbar.error(error)
              ))
            }
          }
        })
      }
    },
    [barterOrderNumber, createDialog, dispatch, handleError, navigation, snackbar, t]
  )

  const onSave = useCallback(
    (formData) => {
      const errors = []
      const dataValidated = flow(
        mapValuesFP.convert({ cap: false })((arrayResultType, name) => filter(arrayResultType, (row, index) => {
          const rowWithoutEmptyColumns = omitBy(row, (item) => isEmpty(item) && (isNumber(item) ? Number(item) === 0 : true))

          const fieldsFilled = size(rowWithoutEmptyColumns)

          if (fieldsFilled === 3) {
            return true
          }

          if (fieldsFilled > 0) {
            // montar array de erros
            errors.push({ type: name, rowIndex: index })
          }
          return false
        })),
        omitByFP(arrayResultType => isEmpty(arrayResultType))
      )(result)

      if (isEmpty(errors)) {
        submit(dataValidated, formData)
      } else {
        handleError(
          `${ t('found items that still need to be filled, check the following category:', { howMany: size(errors) }) }
          ${ join(map(uniqBy(errors, 'type'), ({ type }) => `${ capitalize(type) }`), ', ') }`
        )
      }
    },
    [handleError, result, submit, t]
  )

  const goBack = useCallback(
    () => {
      navigation.navigate('Barters', { screen: 'BarterListScreen' })
    },
    [navigation]
  )

  useEffect(
    () => {
      if (!isEmpty(barter)) {
        if (!onEditMounted) {
          const types = {
            fertilizantes: [],
            herbicidas: [],
            fungicidas: [],
            inseticidas: [],
            sementes: [],
            outros: []
          }

          map(barter.barterProducts, (item) => types[item.productGroupName].push({
            productInternalCode: item.productInternalCode,
            unit: item.measureUnit,
            quantity: item.productQuantity
          }))

          setDetails({
            barterName: barter.barterName || '',
            cropInternalCode: barter.cropInternalCode || '',
            stateRegistration: barter.stateRegistration || ''
          })

          setResult(types)
          setOnEditMounted(true)
        }
      }
    },
    [barter, onEditMounted, setOnEditMounted]
  )

  useFocusEffect(useCallback(
    () => {
      setOptions({
        headerTitle: () => (
          <ScreenHeaderTitleContainer>
            <I18n
              as={ HeaderTitle }
              params={ {
                gender: 'male',
                howMany: 1,
                this: t('barter')
              } }
            >
              {'new {this}'}
            </I18n>
          </ScreenHeaderTitleContainer>
        )
      })
    },
    [setOptions, t]
  ))

  return loaderActive ? <Loader width={ 100 }/> : (
    <Container>
      <Scroll padding={ 0 }>
        <Body>
          <BarterDetailsForm
            ref={ formRef }
            onSubmit={ (data) => onSave(data) }
            { ...barterDetails }
            withoutSubmitButton
          />
        </Body>
        <CreateBarterFragment
          orgId={ currentOrganization?.id }
          data={ result.fertilizantes }
          onResultChange={ setResult }
          title={ t('fertilizer', { howMany: 2 }) }
          type="fertilizantes"
        />
        <Divisor/>
        <CreateBarterFragment
          orgId={ currentOrganization?.id }
          data={ result.herbicidas }
          onResultChange={ setResult }
          title={ t('herbicide', { howMany: 2 }) }
          type="herbicidas"
        />
        <Divisor/>
        <CreateBarterFragment
          orgId={ currentOrganization?.id }
          data={ result.fungicidas }
          onResultChange={ setResult }
          title={ t('fungicide', { howMany: 2 }) }
          type="fungicidas"
        />
        <Divisor/>
        <CreateBarterFragment
          orgId={ currentOrganization?.id }
          data={ result.inseticidas }
          onResultChange={ setResult }
          title={ t('insecticide', { howMany: 2 }) }
          type="inseticidas"
        />
        <Divisor/>
        <CreateBarterFragment
          orgId={ currentOrganization?.id }
          data={ result.sementes }
          onResultChange={ setResult }
          title={ t('seed', { howMany: 2 }) }
          type="sementes"
        />
        <Divisor/>
        <CreateBarterFragment
          orgId={ currentOrganization?.id }
          data={ result.outros }
          onResultChange={ setResult }
          title={ t('other', { howMany: 2 }) }
          type="outros"
        />
      </Scroll>

      <ButtonsContainer style={ { padding: 15 } }>
        <Button
          onPress={ goBack }
          style={ { flex: 1 } }
          variant="outlined"
          title={ <I18n>cancel</I18n> }
        />
        <View style={ { width: '10%' } } />
        <Button
          onPress={ () => formRef.current.submit() }
          style={ { flex: 1 } }
          title={ <I18n>save</I18n> }
        />
      </ButtonsContainer>

    </Container>
  )
}

CreateBarterScreen.propTypes = {
  route: PropTypes.object,
  navigation: PropTypes.object
}

CreateBarterScreen.defaultProps = {
  route: {},
  navigation: {}
}

export default CreateBarterScreen


