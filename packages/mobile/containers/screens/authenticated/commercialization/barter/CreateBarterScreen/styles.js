import styled from 'styled-components/native'

import { colors } from '@smartcoop/styles'

export const Container = styled.View`
  display: flex;
  flex: 1;
`

export const Divisor = styled.View`
  width: 100%;
  border: 0.5px solid #E0E0E0;
  margin: 5px 0;
`

export const Body = styled.View`
  align-items: center;
  background-color: ${ colors.backgroundHtml };
  padding: 13px 10px 0px;
  border-radius: 4px;
  margin: 15px 20px 0 20px;
`