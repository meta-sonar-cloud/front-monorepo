/* eslint-disable react/prop-types */
import React, { useEffect, useCallback, useMemo } from 'react'
import { useSelector } from 'react-redux'

import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import { useNavigation, useFocusEffect } from '@react-navigation/native'
import styled from 'styled-components/native'

import isEmpty from 'lodash/isEmpty'

import { useDialog } from '@smartcoop/dialog'
import { useT } from '@smartcoop/i18n'
import {
  arrowDown,
  // socialNetwork,
  arrowsBackAndForth,
  home
} from '@smartcoop/icons'
import Icon from '@smartcoop/mobile-components/Icon'
import Loader from '@smartcoop/mobile-components/Loader'
import MenuHamburguerButton from '@smartcoop/mobile-containers/fragments/MenuHamburguerButton/MenuHamburguerButton'
import { useScreenOptions } from '@smartcoop/mobile-containers/hooks/useScreenOptions'
import { withDrawer } from '@smartcoop/mobile-containers/hooks/withDrawer'
import {
  ScreenHeaderTitleContainer,
  HeaderTitle
} from '@smartcoop/mobile-containers/layouts/AuthenticatedLayout/theme'
import ChangeOrganization from '@smartcoop/mobile-containers/modals/ChangeOrganizationModal'
import { selectCurrentModule } from '@smartcoop/stores/module/selectorModule'
import { selectCurrentOrganization } from '@smartcoop/stores/organization/selectorOrganization'
import colors from '@smartcoop/styles/colors'

// import SocialFeedScreen from '../social/SocialFeedScreen'
import BarterListScreen from './barter/BarterListScreen'
import QuotationsTabs from './quotations/QuotationsTabs'

export const Row = styled.View`
  flex-direction: row;
  align-items: center;
  justify-content: center;
`
export const IconContainer = styled.View`
  margin-top: 2px;
`

export const Backdrop = styled.View`
  position: absolute;
  height: 100%;
  width: 100%
  justify-content: center;
`

export const Title = styled.Text`
  color: ${ colors.white };
  font-size: 18px;
`

const Tab = createBottomTabNavigator()

const CommercializationScreensRouter = () => {
  const t = useT()
  const { createDialog } = useDialog()
  const navigation = useNavigation()
  const { setOptions, tabBarOptions } = useScreenOptions()

  const currentOrganization = useSelector(selectCurrentOrganization)
  const currentModule = useSelector(selectCurrentModule)

  const tabsOptions = useMemo(
    () => ({
      quotationsTabs: {
        title: t('home'),
        tabBarIcon: ({ color, size }) => (
          <Icon icon={ home } size={ size } color={ color } />
        )
      },
      barters: {
        title: t('barter', { howMany: 2 }),
        tabBarIcon: ({ color, size }) => (
          <Icon icon={ arrowsBackAndForth } size={ size } color={ color } />
        )
      }
      // socialFeed: {
      //   title: t('network'),
      //   tabBarIcon: ({ color, size }) => (
      //     <Icon icon={ socialNetwork } size={ size } color={ color } />
      //   )
      // }
    }),
    [t]
  )

  const changeOrganizationDialog = useCallback(() => {
    createDialog({
      id: 'organization-onboarding',
      Component: ChangeOrganization
    })
  }, [createDialog])

  useEffect(() => {
    if (isEmpty(currentOrganization) && !isEmpty(currentModule)) {
      changeOrganizationDialog()
    }
  }, [changeOrganizationDialog, currentModule, currentOrganization])

  useFocusEffect(
    useCallback(() => {
      setOptions({
        headerLeft: () => <MenuHamburguerButton navigation={ navigation } />,
        headerTitle: (props) => (
          <ScreenHeaderTitleContainer>
            <HeaderTitle onPress={ changeOrganizationDialog } { ...props }>
              <Row>
                <Title { ...props }>
                  {currentOrganization.tradeName ||
                    currentOrganization.companyName ||
                    t('select one organization')}
                </Title>
                <IconContainer>
                  <Icon icon={ arrowDown } color={ colors.white } />
                </IconContainer>
              </Row>
            </HeaderTitle>
          </ScreenHeaderTitleContainer>
        )
      })
    }, [
      changeOrganizationDialog,
      currentOrganization.companyName,
      currentOrganization.tradeName,
      navigation,
      setOptions,
      t
    ])
  )

  return isEmpty(currentOrganization) ? (
    <Backdrop>
      <Loader width={ 100 } />
    </Backdrop>
  ) : (
    <Tab.Navigator
      initialRouteName="Quotations"
      tabBarOptions={ tabBarOptions }
      lazy={ false }
    >
      <Tab.Screen
        name="QuotationsTabs"
        options={ tabsOptions.quotationsTabs }
        component={ QuotationsTabs }
      />
      <Tab.Screen
        name="Barters"
        options={ tabsOptions.barters }
        component={ BarterListScreen }
      />
      {/* <Tab.Screen
        name="SocialFeed"
        options={ tabsOptions.socialFeed }
        component={ SocialFeedScreen }
      /> */}
    </Tab.Navigator>
  )
}

export default withDrawer(CommercializationScreensRouter)
