import React from 'react'

import { useNavigation } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'

import { useScreenOptions, withScreenOptions } from '@smartcoop/mobile-containers/hooks/useScreenOptions'

import CreateSalesOrdersScreen from './CreateSalesOrdersScreen'
import SecuritiesMovementListScreen from './SecuritiesMovementListScreen'

const Stack = createStackNavigator()

const QuotationsRouter = () => {
  const navigation = useNavigation()
  const { options } = useScreenOptions(navigation)

  return (
    <Stack.Navigator screenOptions={ options } mode="modal">
      <Stack.Screen
        name="SecuritiesMovement"
        component={ SecuritiesMovementListScreen }
      />

      <Stack.Screen
        name="CreateSalesOrders"
        component={ CreateSalesOrdersScreen }
      />
    </Stack.Navigator>
  )
}

export default withScreenOptions(QuotationsRouter)
