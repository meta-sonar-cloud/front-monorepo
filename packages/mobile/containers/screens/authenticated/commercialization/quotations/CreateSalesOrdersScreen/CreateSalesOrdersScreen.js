import React, { useCallback, useRef } from 'react'
import { View } from 'react-native'

import { useNavigation , useFocusEffect } from '@react-navigation/native'

import I18n, { useT } from '@smartcoop/i18n'
import Button from '@smartcoop/mobile-components/Button'
import CreateOrderForm from '@smartcoop/mobile-containers/forms/commercialization/CreateOrderForm'
import { useScreenOptions } from '@smartcoop/mobile-containers/hooks/useScreenOptions'
import { Scroll , ScreenHeaderTitleContainer, HeaderTitle } from '@smartcoop/mobile-containers/layouts/AuthenticatedLayout/theme'

import {
  Container,
  ButtonsContainer
} from './styles'

const CreateSalesOrdersScreen = () => {
  const createOrderFormRef = useRef(null)
  const t = useT()
  const navigation = useNavigation()
  const { setOptions } = useScreenOptions()

  const handleSubmit = useCallback(
    () => {
      navigation.goBack()
    },
    [navigation]
  )

  useFocusEffect(useCallback(
    () => {
      setOptions({
        headerTitle: () => (
          <ScreenHeaderTitleContainer>
            <I18n as={ HeaderTitle }>
              new order
            </I18n>
          </ScreenHeaderTitleContainer>
        )
      })
    },
    [setOptions]
  ))

  return (
    <Container>
      <Scroll>
        <CreateOrderForm
          ref={ createOrderFormRef }
          onSubmit={ handleSubmit }
          withoutSubmitButton
        />
      </Scroll>

      <ButtonsContainer>
        <Button
          id="cancel"
          variant="outlined"
          style={ { flex: 1 } }
          onPress={ handleSubmit }
          title={ t('cancel') }
        />

        <View style={ { width: '10%' } } />

        <Button
          id="save"
          style={ { flex: 1 } }
          onPress={ () => createOrderFormRef.current.submit() }
          title={ t('save') }
        />
      </ButtonsContainer>
    </Container>
  )
}

export default CreateSalesOrdersScreen
