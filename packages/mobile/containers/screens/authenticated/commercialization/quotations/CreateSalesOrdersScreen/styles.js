import styled from 'styled-components/native'

export const Container = styled.View`
  display: flex;
  flex-direction: column;
  padding: 15px;
  flex: 1;
  width: 100%;
`

export const ButtonsContainer = styled.View`
  width: 100%;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  padding: 10px 0 5px;
`
