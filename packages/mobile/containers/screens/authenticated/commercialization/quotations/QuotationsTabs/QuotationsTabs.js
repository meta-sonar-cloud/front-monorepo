import React, { useMemo, useState, useCallback } from 'react'

import debounce from 'lodash/debounce'

import { useT } from '@smartcoop/i18n'
import InputSearch from '@smartcoop/mobile-components/InputSearch'
import AccountBalanceList from '@smartcoop/mobile-containers/fragments/commercialization/AccountBalanceList'
import ProductBalanceList from '@smartcoop/mobile-containers/fragments/commercialization/ProductBalanceList'
import ProductQuotationList from '@smartcoop/mobile-containers/fragments/commercialization/ProductQuotationList'
import ProductWithdrawList from '@smartcoop/mobile-containers/fragments/commercialization/ProductWithdrawList'
import SalesOrdersList from '@smartcoop/mobile-containers/fragments/commercialization/SalesOrdersList'
import useTerm from '@smartcoop/mobile-containers/hooks/useTerm'

import {
  InputView,
  SafeAreaViewList,
  ScrollViewButtons,
  TabButton
} from './styles'

const QuotationsTabs = () => {
  const [activeTab, setActiveTab] = useState('quotation')
  const [filterText, setFilterText] = useState('')
  const [debouncedFilterText, setDebouncedFilterText] = useState('')

  const t = useT()
  useTerm('commercialization-term')

  const params = useMemo(
    () => ({
      q: debouncedFilterText
    }), [debouncedFilterText]
  )

  const debouncedChangeSearchFilter = useCallback(
    debounce((value) => {
      setDebouncedFilterText(value)
    }, 300), []
  )

  const onChangeSearchFilter = useCallback(
    (value) => {
      setFilterText(value)
      debouncedChangeSearchFilter(value)
    }, [debouncedChangeSearchFilter]
  )

  const activeList = useMemo(
    () => {
      switch (activeTab) {
        case 'quotation':
          return <ProductQuotationList params={ params }/>
        case 'products balance':
          return <ProductBalanceList params={ params }/>
        case 'products withdraw':
          return <ProductWithdrawList params={ params }/>
        case 'sales orders':
          return <SalesOrdersList params={ params }/>
        case 'account balance':
          return <AccountBalanceList params={ params }/>
        default:
          return null
      }
    },
    [activeTab, params]
  )

  return (
    <>
      <ScrollViewButtons horizontal>
        <TabButton
          activeTab={ activeTab === 'quotation' }
          onPress={ () => setActiveTab('quotation') }
          title={ t('quotation', { howMany: 1 }) }
        />
        <TabButton
          activeTab={ activeTab === 'products balance' }
          onPress={ () => setActiveTab('products balance') }
          title={ t('products balance') }
        />
        <TabButton
          activeTab={ activeTab === 'products withdraw' }
          onPress={ () => setActiveTab('products withdraw') }
          title={ t('supplies withdrawal') }
        />
        <TabButton
          activeTab={ activeTab === 'sales orders' }
          onPress={ () => setActiveTab('sales orders') }
          title={ t('sales orders') }
        />
        <TabButton
          activeTab={ activeTab === 'account balance' }
          onPress={ () => setActiveTab('account balance') }
          title={ t('account balance') }
        />
      </ScrollViewButtons>
      {activeTab !== 'account balance' && (
        <InputView>
          <InputSearch
            detached
            label="Pesquisar"
            name="search"
            value={ filterText }
            onChange={ onChangeSearchFilter }
          />
        </InputView>
      )}
      <SafeAreaViewList>
        {activeList}
      </SafeAreaViewList>
    </>
  )
}

export default QuotationsTabs
