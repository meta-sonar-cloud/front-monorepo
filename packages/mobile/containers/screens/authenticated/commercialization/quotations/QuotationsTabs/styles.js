import styled from 'styled-components/native'

import Button from '@smartcoop/mobile-components/Button'
import colors from '@smartcoop/styles/colors'

export const InputView = styled.View`
  margin-left: 10px;
  margin-right: 10px;
`

export const ScrollViewButtons = styled.ScrollView`
  padding: 15px 0 5px 10px;
  margin-bottom: 10px;
`

export const SafeAreaViewList = styled.SafeAreaView`
  flex: 100;
  width: 100%;
`

export const TabButton = styled(Button).attrs(props => ({
  color: props.activeTab ? colors.white : colors.darkGrey,
  backgroundColor: props.activeTab ? colors.black : colors.backgroundHtml
}))`
  margin-right: 10px;
`
