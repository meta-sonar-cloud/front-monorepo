import React, { useCallback, useState, useEffect, useMemo } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import { useFocusEffect } from '@react-navigation/native'
import moment from 'moment/moment'
import PropTypes from 'prop-types'

import isEmpty from 'lodash/isEmpty'
import toNumber from 'lodash/toNumber'

import { useT } from '@smartcoop/i18n'
import AccountBalance from '@smartcoop/mobile-components/AccountBalance'
import Button from '@smartcoop/mobile-components/Button'
import DateTabs from '@smartcoop/mobile-components/DateTabs'
import List from '@smartcoop/mobile-components/List'
import { useScreenOptions } from '@smartcoop/mobile-containers/hooks/useScreenOptions'
import { ScreenHeaderTitleContainer, HeaderTitle } from '@smartcoop/mobile-containers/layouts/AuthenticatedLayout/theme'
import { selectCurrentAccount } from '@smartcoop/stores/account/selectorAccount'
import { selectCurrentOrganization } from '@smartcoop/stores/organization/selectorOrganization'
import { SecuritiesMovementActions } from '@smartcoop/stores/securitiesMovement'
import { selectSecuritiesMovement } from '@smartcoop/stores/securitiesMovement/selectorSecuritiesMovement'
import colors from '@smartcoop/styles/colors'
import { momentBackDateTimeFormat } from '@smartcoop/utils/dates'

import SecuritiesMovementItem from './SecuritiesMovementItem'
import {
  Container,
  ButtonsContainer,
  SafeAreaViewList,
  HeaderContainer
} from './styles'

const SecuritiesMovementListScreen = ({ params }) => {
  const dispatch = useCallback(useDispatch(), [])
  const t = useT()
  const { setOptions } = useScreenOptions()

  const securitiesMovement = useSelector(selectSecuritiesMovement)
  const currentOrganization = useSelector(selectCurrentOrganization)
  const currentAccount = useSelector(selectCurrentAccount)

  const [mounted, setMounted] = useState(false)
  const [type, setListType] = useState('receivable')
  const [filterDate, setFilterDate] = useState()
  const [initialDate, setInitialDate] = useState()

  const deps = useMemo(
    () => [currentOrganization, params, type, filterDate],
    [currentOrganization, params, type, filterDate]
  )

  const accountName = useMemo(() => currentAccount?.accountName, [currentAccount])

  const renderItem = useCallback(
    ({ item }) => (
      <SecuritiesMovementItem
        { ...item }
        positive={ toNumber(item.value) > 0 }
      />
    ),
    []
  )

  const loadProductsQuotation = useCallback(
    (page, onSuccess, onError) => {
      if (!isEmpty(currentOrganization)) {
        dispatch(SecuritiesMovementActions.loadSecuritiesMovement(
          { ...params, type, page, transactionDate: filterDate },
          onSuccess,
          onError
        ))
      }
    },
    [currentOrganization, dispatch, params, type, filterDate]
  )

  useEffect(
    () => () => {
      dispatch(SecuritiesMovementActions.resetSecuritiesMovement())
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [currentOrganization, dispatch]
  )

  useEffect(
    () => {
      dispatch(SecuritiesMovementActions.loadSecuritieMovementInitialDate(
        {},
        (value) => setInitialDate(value)
      ))
    },
    [dispatch, mounted, currentAccount, currentOrganization, type]
  )

  useEffect(
    () => {
      if(!mounted) {
        setMounted(true)
        dispatch(SecuritiesMovementActions.loadSecuritieMovementInitialDate(
          {},
          (value) => setInitialDate(value)
        ))
      }
    },
    [dispatch, mounted]
  )

  useFocusEffect(useCallback(
    () => {
      setOptions({
        headerTitle: () => (
          <ScreenHeaderTitleContainer>
            <HeaderTitle>
              {accountName}
            </HeaderTitle>
          </ScreenHeaderTitleContainer>
        )
      })
    },
    [accountName, setOptions]
  ))

  return (
    <Container>
      <HeaderContainer>
        <AccountBalance
          accountName={ currentAccount.accountName }
          balance={ currentAccount.currentBalance }
          style={ {
            elevation: 0,
            marginBottom: 0,
            marginLeft: 30,
            marginRight: 0
          } }
          creditLimit={ (currentAccount?.renovationDate && currentAccount?.creditLimit)
            ? {
              renovationDate: currentAccount.renovationDate,
              creditLimit: currentAccount.creditLimit
            }
            : {} }
        />
      </HeaderContainer>
      <ButtonsContainer>
        <Button
          onPress={ () => setListType('receivable') }
          title={ t('titles to receive') }
          activeOpacity={ 1 }
          color={ type === 'receivable' ? colors.black : colors.mediumGrey }
          style={ {
            flex: 1,
            backgroundColor: 'transparent',
            borderBottomWidth: 4,
            borderColor: type === 'receivable' ? colors.black : colors.transparent,
            borderRadius: 0
          } }
          textStyle={ {
            fontSize: 16,
            fontWeight: 'bold'
          } }
          variant="text"
        />
        <Button
          onPress={ () => setListType('payable') }
          title={ t('titles to pay') }
          activeOpacity={ 1 }
          color={ type === 'payable' ? colors.black : colors.mediumGrey }
          style={ {
            flex: 1,
            backgroundColor: 'transparent',
            borderBottomWidth: 4,
            borderColor: type === 'payable' ? colors.black : colors.transparent,
            borderRadius: 0
          } }
          textStyle={ {
            fontSize: 16, fontWeight: 'bold'
          } }
          variant="text"
        />
      </ButtonsContainer>
      {
        !isEmpty(initialDate) && <DateTabs
          initialDate={ moment(initialDate, momentBackDateTimeFormat) }
          endDate={ moment() }
          onChange={ (value) => setFilterDate(value.format('YYYY-MM')) }
          currentYearFormat="MMM"
          defaultValue={ moment(initialDate, 'YYYY-MM-DD HH:mm:ss') }
          horizontal
          viewLastFirst
        />
      }
      <SafeAreaViewList>
        <List
          data={ securitiesMovement }
          renderItem={ renderItem }
          onListLoad={ loadProductsQuotation }
          deps={ deps }
        />
      </SafeAreaViewList>
    </Container>
  )
}

SecuritiesMovementListScreen.propTypes = {
  params: PropTypes.object
}

SecuritiesMovementListScreen.defaultProps = {
  params: {}
}

export default SecuritiesMovementListScreen
