import styled from 'styled-components/native'

export const Container = styled.View`
  flex: 1;
`

export const ButtonsContainer = styled.View`
  flex-direction: row;
  margin-bottom: 15px;
`

export const HeaderContainer = styled.View`
  padding: 20px 20px 0;
`

export const SafeAreaViewList = styled.SafeAreaView`
  flex: 1;
`
