import styled from 'styled-components/native'

import I18n from '@smartcoop/i18n'
import colors from '@smartcoop/styles/colors'
import fonts from '@smartcoop/styles/fonts'

export const Container = styled.View`
  display: flex;
  flex: 1;
`

export const Column = styled.View`
  display: flex;
  flex: 1;
  flex-direction: column;
`

export const Row = styled.View`
  display: flex;
  flex: 1;
  flex-direction: row;
  padding-bottom: 5px;
`

export const Text = styled.Text`
  font-size: 14px;
  font-weight: 400;
  font-family: ${ fonts.fontFamilyMontserrat };
  color: ${ colors.black };
`

export const LabelText = styled(I18n)
  .attrs({
    style: {
      fontSize: '14px',
      fontFamily: fonts.fontFamilyMontserrat,
      fontWeight: 600,
      color: colors.grey,
      marginBottom: '3px'
    }
  })``

export const ValueText = styled.Text`
  font-size: 16px;
  font-weight: 600;
  font-family: ${ fonts.fontFamilyMontserrat };
  color: ${ props => props.positive ? colors.green : colors.red };
  margin-bottom: 5px;
`
