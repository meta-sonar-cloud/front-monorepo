import React, { useMemo } from 'react'

import moment from 'moment/moment'
import PropTypes from 'prop-types'

import toNumber from 'lodash/toNumber'

import { momentBackDateTimeFormat } from '@smartcoop/utils/dates'
import { formatCurrency } from '@smartcoop/utils/formatters'

import {
  Container,
  Column,
  Row,
  Text,
  ValueText,
  LabelText
} from './styles'

const SecuritiesMovementItem = (props) => {
  const {
    value,
    transactionDate,
    dueDate: externalDueDate,
    description,
    positive,
    titleNumber: externalTitleNumber,
    titleName: externalTitleName
  } = props

  const currencyValue = useMemo(
    () => formatCurrency(Math.abs(toNumber(value))),
    [value]
  )

  const formatedValue = useMemo(() => `${ positive ? '+' : '-' } ${ currencyValue }`, [currencyValue, positive])
  const launchDate = useMemo(() => moment(transactionDate, momentBackDateTimeFormat).format('DD/MM/YYYY'), [transactionDate])
  const dueDate = useMemo(() => moment(externalDueDate, momentBackDateTimeFormat).format('DD/MM/YYYY'), [externalDueDate])
  const titleNumber = useMemo(() => externalTitleNumber, [externalTitleNumber])
  const titleName = useMemo(() => externalTitleName, [externalTitleName])

  return (
    <Container>
      <Row style={ { alignItems: 'space-around' } }>
        <Column>
          <ValueText positive={ positive }>{formatedValue}</ValueText>
        </Column>
      </Row>

      <Row style={ { alignItems: 'space-around' } }>
        {titleName
          ? (
            <Column>
              <LabelText>name</LabelText>
              <Text>{ titleName }</Text>
            </Column>
          )
          : <></>
        }
        {titleNumber
          ? (
            <Column style={ { alignItems: 'flex-end' } }>
              <LabelText>number</LabelText>
              <Text>{ titleNumber }</Text>
            </Column>
          )
          : <></>
        }
      </Row>

      <Row style={ { alignItems: 'space-around' } }>
        <Column>
          <LabelText>launch date</LabelText>
          <Text>{ launchDate }</Text>
        </Column>
        <Column style={ { alignItems: 'flex-end' } }>
          <LabelText>due date</LabelText>
          <Text>{ dueDate }</Text>
        </Column>
      </Row>

      <Row>
        <Text>{description}</Text>
      </Row>
    </Container>
  )
}

SecuritiesMovementItem.propTypes = {
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  transactionDate: PropTypes.string.isRequired,
  dueDate: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  positive: PropTypes.bool,
  titleNumber: PropTypes.string,
  titleName: PropTypes.string
}

SecuritiesMovementItem.defaultProps = {
  positive: true,
  titleNumber: '',
  titleName: ''
}

export default SecuritiesMovementItem
