import React, { useMemo, useState, useCallback } from 'react'

import debounce from 'lodash/debounce'

import { useT } from '@smartcoop/i18n'
import InputSearch from '@smartcoop/mobile-components/InputSearch'
import ProductWithdrawList from '@smartcoop/mobile-containers/fragments/commercialization/ProductWithdrawList'
import AuthenticatedLayout from '@smartcoop/mobile-containers/layouts/AuthenticatedLayout'

import {
  InputView,
  SafeAreaViewList,
  ScrollViewButtons,
  TabButton
} from './styles'

const TechnicalCommercializationAreaScreen = () => {
  const [activeTab, setActiveTab] = useState('products withdraw')
  const [filterText, setFilterText] = useState('')
  const [debouncedFilterText, setDebouncedFilterText] = useState('')

  const t = useT()

  const params = useMemo(
    () => ({
      q: debouncedFilterText
    }), [debouncedFilterText]
  )

  const debouncedChangeSearchFilter = useCallback(
    debounce((value) => {
      setDebouncedFilterText(value)
    }, 300), []
  )

  const onChangeSearchFilter = useCallback(
    (value) => {
      setFilterText(value)
      debouncedChangeSearchFilter(value)
    }, [debouncedChangeSearchFilter]
  )

  const activeList = useMemo(
    () => {
      switch (activeTab) {
        case 'products withdraw':
        default:
          return <ProductWithdrawList params={ params } technical/>
      }
    },
    [activeTab, params]
  )

  return (
    <AuthenticatedLayout>
      <>
        <ScrollViewButtons horizontal>
          <TabButton
            activeTab={ activeTab === 'products withdraw' }
            onPress={ () => setActiveTab('products withdraw') }
            title={ t('supplies withdrawal') }
          />
        </ScrollViewButtons>
        <InputView>
          <InputSearch
            detached
            label="Pesquisar"
            name="search"
            value={ filterText }
            onChange={ onChangeSearchFilter }
          />
        </InputView>
        <SafeAreaViewList>
          {activeList}
        </SafeAreaViewList>
      </>
    </AuthenticatedLayout>
  )
}

export default TechnicalCommercializationAreaScreen
