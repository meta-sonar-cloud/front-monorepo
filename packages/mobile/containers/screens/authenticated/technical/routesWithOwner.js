/* eslint-disable react/prop-types */
import React, { useEffect, useCallback, useMemo } from 'react'
import { TouchableOpacity }from 'react-native'
import { useDispatch, useSelector } from 'react-redux'

import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import { useNavigation, useFocusEffect } from '@react-navigation/native'
import styled from 'styled-components/native'

import isEmpty from 'lodash/isEmpty'

import { useDialog } from '@smartcoop/dialog'
import { useT } from '@smartcoop/i18n'
import { field, arrowDown, exitModule, currencySignRounded, weatherStation, cattle } from '@smartcoop/icons'
import Icon from '@smartcoop/mobile-components/Icon'
import MenuHamburguerButton from '@smartcoop/mobile-containers/fragments/MenuHamburguerButton/MenuHamburguerButton'
import { useScreenOptions } from '@smartcoop/mobile-containers/hooks/useScreenOptions'
import { ScreenHeaderTitleContainer, HeaderTitle } from '@smartcoop/mobile-containers/layouts/AuthenticatedLayout/theme'
import ChangePropertyModal from '@smartcoop/mobile-containers/modals/ChangePropertyModal'
import { selectPermissionWeatherStations } from '@smartcoop/stores/authentication/selectorAuthentication'
import { selectCurrentProperty, selectProperties } from '@smartcoop/stores/property/selectorProperty'
import { TechnicalActions } from '@smartcoop/stores/technical'
import colors from '@smartcoop/styles/colors'

import DairyFarmDashboardScreen from '../digitalProperty/dairyFarm/DairyFarmDashboardScreen'
import FieldsMapScreen from '../digitalProperty/field/FieldsMapScreen'
import WeatherStationMapScreen from '../weatherStation/WeatherStationMapScreen'
import TechnicalCommercializationAreaScreen from './TechnicalCommercializationAreaScreen'

export const Row = styled.View`
  flex-direction: row;
  align-items: center;
  justify-content: center;
`
export const IconContainer = styled.View`
  margin-top: 2px;
`
export const Title = styled.Text`
  color: ${ colors.white };
  font-size: 18px;
`

const Tab = createBottomTabNavigator()

const TechnicalWithOwnerRouter = () => {
  const { createDialog } = useDialog()
  const t = useT()
  const dispatch = useCallback(useDispatch(), [])
  const navigation = useNavigation()
  const { setOptions, tabBarOptions } = useScreenOptions()

  const properties = useSelector(selectProperties)
  const currentProperty = useSelector(selectCurrentProperty)
  const permissionWeatherStations = useSelector(selectPermissionWeatherStations)

  const tabsOptions = useMemo(
    () =>({
      fields: {
        title: t('field', { howMany: 2 }),
        tabBarIcon: ({ color, size }) => (
          <Icon icon={ field } size={ size } color={ color } />
        )
      },
      dairyFarmDashboard: {
        title: t('dairy farm', { howMany: 2 }),
        tabBarIcon: ({ color }) => (
          <Icon icon={ cattle } size={ 28 } color={ color } />
        )
      },
      commercialization: {
        title: t('commercialization'),
        tabBarIcon: ({ color, size }) => (
          <Icon icon={ currencySignRounded } size={ size } color={ color } />
        )
      },
      weatherStation: {
        title: t('station', { howMany: 2 }),
        tabBarIcon: ({ color, size }) => (
          <Icon icon={ weatherStation } size={ size } color={ color } />
        )
      }
      // socialFeed: {
      //   title: t('network'),
      //   tabBarIcon: ({ color, size }) => (
      //     <Icon icon={ socialNetwork } size={ size } color={ color } />
      //   )
      // }
    }),
    [t]
  )

  const changePropertyDialog = useCallback(
    () => {
      createDialog({
        id: 'change-property',
        Component: ChangePropertyModal
      })
    },
    [createDialog]
  )

  const technicalExitProperty = useCallback(
    () => {
      dispatch(TechnicalActions.resetTechnicalCurrentOwner())
    },
    [dispatch]
  )

  useEffect(() => {
    if (isEmpty(currentProperty) && !isEmpty(properties)) {
      changePropertyDialog()
    }
  }, [changePropertyDialog, currentProperty, properties])

  useFocusEffect(useCallback(
    () => {
      setOptions({
        headerLeft: () => <MenuHamburguerButton navigation={ navigation } />,
        headerTitle: props => (
          <ScreenHeaderTitleContainer>
            <HeaderTitle onPress={ changePropertyDialog } { ...props }>
              <Row>
                <Title { ...props }>
                  {currentProperty.name || t('select one property')}
                </Title>
                <IconContainer>
                  <Icon icon={ arrowDown } color={ colors.white } />
                </IconContainer>
              </Row>
            </HeaderTitle>
          </ScreenHeaderTitleContainer>
        ),
        headerRight: ({ tintColor }) => (
          <TouchableOpacity onPress={ technicalExitProperty } style={ { marginRight: 10 } }>
            <Icon icon={ exitModule } color={ tintColor } size={ 24 } />
          </TouchableOpacity>
        )
      })
    },
    [changePropertyDialog, currentProperty.name, navigation, setOptions, t, technicalExitProperty]
  ))

  return (
    <Tab.Navigator
      initialRouteName="Fields"
      tabBarOptions={ tabBarOptions }
      lazy={ false }
    >
      <Tab.Screen
        name="Fields"
        options={ tabsOptions.fields }
        component={ FieldsMapScreen }
      />
      <Tab.Screen
        name="DairyFarmDashboard"
        options={ tabsOptions.dairyFarmDashboard }
        component={ DairyFarmDashboardScreen }
      />
      <Tab.Screen
        name="Commercialization"
        options={ tabsOptions.commercialization }
        component={ TechnicalCommercializationAreaScreen }
      />
      {permissionWeatherStations && (
        <Tab.Screen
          name="WeatherStations"
          options={ tabsOptions.weatherStation }
          component={ WeatherStationMapScreen }
        />
      )}
    </Tab.Navigator>
  )
}

export default TechnicalWithOwnerRouter
