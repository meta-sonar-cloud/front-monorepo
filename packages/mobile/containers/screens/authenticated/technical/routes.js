import React, { useEffect } from 'react'
import { PermissionsAndroid, Platform } from 'react-native'
import { useSelector } from 'react-redux'

import styled from 'styled-components'

import isEmpty from 'lodash/isEmpty'

import { withDrawer } from '@smartcoop/mobile-containers/hooks/withDrawer'
import { selectCurrentOwner } from '@smartcoop/stores/technical/selectorTechnical'
import { colors } from '@smartcoop/styles'

import TechnicalWithoutOwnerRouter from './routesWithoutOwner'
import TechnicalWithOwnerRouter from './routesWithOwner'

export const IconContainer = styled.View`
  margin-top: 2px;
`
export const Title = styled.Text`
  color: ${ colors.white };
  font-size: 18px;
`
export const Row = styled.View`
  flex-direction: row;
  align-items: center;
  justify-content: center;
`

const TechnicalRouter = (props) => {
  const technicalCurrentOwner = useSelector(selectCurrentOwner)

  useEffect(() => {
    if (Platform.OS === 'android') {
      PermissionsAndroid.requestMultiple([
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION
      ])
    }
  }, [])

  return isEmpty(technicalCurrentOwner) ? (
    <TechnicalWithoutOwnerRouter { ...props } />
  ) : (
    <TechnicalWithOwnerRouter { ...props } />
  )
}

export default withDrawer(TechnicalRouter)
