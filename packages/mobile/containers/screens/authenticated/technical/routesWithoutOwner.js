import React, { useCallback, useEffect, useMemo } from 'react'
import { useSelector } from 'react-redux'

import { useT } from '@meta-react/i18n'
import { useNavigation, useFocusEffect } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import styled from 'styled-components/native'

import isEmpty from 'lodash/isEmpty'

import { useDialog } from '@smartcoop/dialog'
import { arrowDown } from '@smartcoop/icons'
import Icon from '@smartcoop/mobile-components/Icon'
import MenuHamburguerButton from '@smartcoop/mobile-containers/fragments/MenuHamburguerButton/MenuHamburguerButton'
import { useScreenOptions } from '@smartcoop/mobile-containers/hooks/useScreenOptions'
import { ScreenHeaderTitleContainer, HeaderTitle } from '@smartcoop/mobile-containers/layouts/AuthenticatedLayout/theme'
import ChangeOrganization from '@smartcoop/mobile-containers/modals/ChangeOrganizationModal'
import { selectCurrentModule } from '@smartcoop/stores/module/selectorModule'
import { selectCurrentOrganization } from '@smartcoop/stores/organization/selectorOrganization'
import colors from '@smartcoop/styles/colors'

import WeatherStationMapScreen from '../weatherStation/WeatherStationMapScreen'
import TechnicalAreaScreen from './TechnicalAreaScreen'

export const Row = styled.View`
  flex-direction: row;
  align-items: center;
  justify-content: center;
`
export const IconContainer = styled.View`
  margin-top: 2px;
`
export const Title = styled.Text`
  color: ${ colors.white };
  font-size: 18px;
`

const Stack = createStackNavigator()

const TechnicalWithoutOwnerRouter = () => {
  const t = useT()
  const { createDialog } = useDialog()
  const navigation = useNavigation()
  const { setOptions } = useScreenOptions()

  const currentOrganization = useSelector(selectCurrentOrganization)
  const currentModule = useSelector(selectCurrentModule)

  const screenOptions = useMemo(() => ({ headerShown: false }), [])

  const changeOrganizationDialog = useCallback(() => {
    createDialog({
      id: 'organization-onboarding',
      Component: ChangeOrganization
    })
  }, [createDialog])

  useEffect(() => {
    if (isEmpty(currentOrganization) && !isEmpty(currentModule)) {
      changeOrganizationDialog()
    }
  }, [changeOrganizationDialog, currentModule, currentOrganization])

  useFocusEffect(
    useCallback(() => {
      setOptions({
        headerLeft: () => <MenuHamburguerButton navigation={ navigation } />,
        headerTitle: (optionsProps) => (
          <ScreenHeaderTitleContainer>
            <HeaderTitle onPress={ changeOrganizationDialog } { ...optionsProps }>
              <Row>
                <Title { ...optionsProps }>
                  {currentOrganization.tradeName ||
                    currentOrganization.companyName ||
                    t('select one organization')}
                </Title>
                <IconContainer>
                  <Icon icon={ arrowDown } color={ colors.white } />
                </IconContainer>
              </Row>
            </HeaderTitle>
          </ScreenHeaderTitleContainer>
        )
      })
    }, [changeOrganizationDialog, currentOrganization.companyName, currentOrganization.tradeName, navigation, setOptions, t])
  )

  return (
    <Stack.Navigator screenOptions={ screenOptions }>
      <Stack.Screen
        name="TechnicalArea"
        component={ TechnicalAreaScreen }
      />

      <Stack.Screen
        name="WeatherStations"
        component={ WeatherStationMapScreen }
      />
    </Stack.Navigator>
  )
}

export default TechnicalWithoutOwnerRouter
