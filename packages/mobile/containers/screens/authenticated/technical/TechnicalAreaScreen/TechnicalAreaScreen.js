import React, { useMemo, useState, useCallback } from 'react'

import isEmpty from 'lodash/isEmpty'

import { useDialog } from '@smartcoop/dialog'
import { useT } from '@smartcoop/i18n'
import { filter } from '@smartcoop/icons'
import Button from '@smartcoop/mobile-components/Button'
import Icon from '@smartcoop/mobile-components/Icon'
import TechnicalPortfolioList from '@smartcoop/mobile-containers/fragments/TechnicalPortfolioList'
import TechnicalVisitList from '@smartcoop/mobile-containers/fragments/TechnicalVisitList'
import AuthenticatedLayout from '@smartcoop/mobile-containers/layouts/AuthenticatedLayout'
import FilterTechnicalPortfolioModal from '@smartcoop/mobile-containers/modals/FilterTechnicalPortfolioModal'
import FilterTechnicalVisitModal from '@smartcoop/mobile-containers/modals/FilterTechnicalVisitModal'
import colors from '@smartcoop/styles/colors'

import {
  InputView,
  SafeAreaViewList,
  ScrollViewButtons,
  TabButton
} from './styles'

const TechnicalAreaScreen = () => {
  const { createDialog } = useDialog()
  const [activeTab, setActiveTab] = useState('portfolio')
  const [filters, setFilters] = useState({})

  const t = useT()

  const params = useMemo(
    () => ({
      ...filters
    }), [filters]
  )


  const handleFilter = useCallback(
    (values) => setFilters(values),
    []
  )

  const handleFilterFields = useCallback(
    () => {
      createDialog({
        id: 'filter-technical-visit',
        Component: activeTab === 'portfolio' ? FilterTechnicalPortfolioModal : FilterTechnicalVisitModal,
        props: {
          onSubmit: handleFilter,
          filters
        }

      })
    },
    [activeTab, createDialog, filters, handleFilter]
  )

  const activeList = useMemo(
    () => {
      switch (activeTab) {
        case 'portfolio':
          return <TechnicalPortfolioList params={ params }/>
        case 'visit list':
          return <TechnicalVisitList params={ params }/>
        default:
          return null
      }
    },
    [activeTab, params]
  )

  return (
    <AuthenticatedLayout>
      <>
        <ScrollViewButtons horizontal>
          <TabButton
            activeTab={ activeTab === 'portfolio' }
            onPress={ () => setActiveTab('portfolio') }
            title={ t('list of producers') }
          />
          <TabButton
            activeTab={ activeTab === 'visit list' }
            onPress={ () => setActiveTab('visit list') }
            title={ t('list of technical visits') }
          />
        </ScrollViewButtons>
        {activeTab === 'portfolio' && (
          <InputView>
            <Button
              title={ t('filtrate') }
              onPress={ handleFilterFields }
              variant="outlined"
              style={ {
                marginTop: 3,
                backgroundColor: isEmpty(filters) ? colors.white : colors.secondary
              } }
              icon={ <Icon size={ 22 } icon={ filter } color={ colors.black } /> }
            />
          </InputView>
        )}
        {activeTab === 'visit list' && (
          <InputView>
            <Button
              title={ t('filtrate') }
              onPress={ handleFilterFields }
              variant="outlined"
              style={ {
                marginTop: 3,
                backgroundColor: isEmpty(filters) ? colors.white : colors.secondary
              } }
              icon={ <Icon size={ 22 } icon={ filter } color={ colors.black } /> }
            />
          </InputView>
        )}
        <SafeAreaViewList>
          {activeList}
        </SafeAreaViewList>
      </>
    </AuthenticatedLayout>
  )
}

export default TechnicalAreaScreen
