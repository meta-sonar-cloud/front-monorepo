import React, { useRef, useCallback, useMemo } from 'react'
import { View } from 'react-native'

import { useRoute, useNavigation, CommonActions , useFocusEffect } from '@react-navigation/native'

import I18n, { useT } from '@smartcoop/i18n'
import Button from '@smartcoop/mobile-components/Button'
import TechnicalVisitRegisterForm from '@smartcoop/mobile-containers/forms/digitalProperty/technical/TechnicalVisitRegisterForm'
import { useScreenOptions } from '@smartcoop/mobile-containers/hooks/useScreenOptions'
import { Container, Item, Scroll, ButtonsContainer , ScreenHeaderTitleContainer, HeaderTitle } from '@smartcoop/mobile-containers/layouts/AuthenticatedLayout/theme'
import { useSnackbar } from '@smartcoop/snackbar'

const TechnicalVisitRegisterScreen = () => {
  const TechnicalVisitRegisterFormRef = useRef(null)
  const navigation = useNavigation()
  const route = useRoute()
  const t = useT()
  const snackbar = useSnackbar()
  const { setOptions } = useScreenOptions()

  const { cultivateName, fieldName, technicalVisit } = route.params

  const submitForms = useCallback(
    () => {
      TechnicalVisitRegisterFormRef.current.submit()
    },
    []
  )

  const handleClose = useCallback(
    () => {
      navigation.dispatch(CommonActions.goBack())
    },
    [navigation]
  )

  const textSnackbarSuccess = useMemo(
    () => technicalVisit?.id ? (
      t('your {this} was edited', {
        howMany: 1,
        gender: 'female',
        this: t('technical visit')
      })
    ) : (
      t('your {this} was registered', {
        howMany: 1,
        gender: 'female',
        this: t('technical visit')
      })
    ),
    [t, technicalVisit]
  )

  const onSuccess = useCallback(
    () => {
      snackbar.success(
        textSnackbarSuccess
      )
      handleClose()
    },
    [handleClose, snackbar, textSnackbarSuccess]
  )

  useFocusEffect(useCallback(
    () => {
      setOptions({
        headerTitle: () => (
          <ScreenHeaderTitleContainer>
            <I18n as={ HeaderTitle }>
              register technical visit
            </I18n>
          </ScreenHeaderTitleContainer>
        )
      })
    },
    [setOptions]
  ))

  return (
    <Container>
      <Scroll padding={ 0 }>
        <Item
          style={ {
            flex: 1,
            paddingTop: 20,
            paddingBottom: 0
          } }
        >
          <TechnicalVisitRegisterForm
            ref={ TechnicalVisitRegisterFormRef }
            cultivateName={ cultivateName }
            fieldName={ fieldName }
            technicalVisit={ technicalVisit }
            onSuccess={ onSuccess }
            withoutSubmitButton
          />
        </Item>
      </Scroll>

      <Item>
        <ButtonsContainer style={ { paddingTop: 10 } }>
          <Button
            onPress={ handleClose }
            style={ { flex: 1 } }
            variant="outlined"
            title={ t('cancel') }
          />

          <View style={ { width: '10%' } } />

          <Button
            onPress={ submitForms }
            style={ { flex: 1 } }
            title={ t('save') }
          />
        </ButtonsContainer>
      </Item>
    </Container>
  )
}

export default TechnicalVisitRegisterScreen
