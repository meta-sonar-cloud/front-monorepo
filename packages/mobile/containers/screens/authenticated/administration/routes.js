/* eslint-disable react/prop-types */
import React, { useEffect, useCallback, useMemo } from 'react'
import { PermissionsAndroid, Platform } from 'react-native'
import { useSelector } from 'react-redux'

import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import { useNavigation, useFocusEffect } from '@react-navigation/native'
import styled from 'styled-components/native'

import isEmpty from 'lodash/isEmpty'

import { useDialog } from '@smartcoop/dialog'
import { useT } from '@smartcoop/i18n'
import {
  arrowDown,
  // socialNetwork,
  weatherStation
} from '@smartcoop/icons'
import Icon from '@smartcoop/mobile-components/Icon'
// import Loader from '@smartcoop/mobile-components/Loader'
import MenuHamburguerButton from '@smartcoop/mobile-containers/fragments/MenuHamburguerButton/MenuHamburguerButton'
import { useScreenOptions } from '@smartcoop/mobile-containers/hooks/useScreenOptions'
import { withDrawer } from '@smartcoop/mobile-containers/hooks/withDrawer'
import {
  ScreenHeaderTitleContainer,
  HeaderTitle
} from '@smartcoop/mobile-containers/layouts/AuthenticatedLayout/theme'
import ChangeOrganization from '@smartcoop/mobile-containers/modals/ChangeOrganizationModal'
import { selectCurrentModule } from '@smartcoop/stores/module/selectorModule'
import { selectCurrentOrganization } from '@smartcoop/stores/organization/selectorOrganization'
import colors from '@smartcoop/styles/colors'

// import SocialFeedScreen from '../social/SocialFeedScreen'
import WeatherStationMapScreen from '../weatherStation/WeatherStationMapScreen'

export const Row = styled.View`
  flex-direction: row;
  align-items: center;
  justify-content: center;
`
export const IconContainer = styled.View`
  margin-top: 2px;
`
export const Backdrop = styled.View`
  position: absolute;
  height: 100%;
  width: 100%
  justify-content: center;
`

export const Title = styled.Text`
  color: ${ colors.white };
  font-size: 18px;
`

const Tab = createBottomTabNavigator()

const AdministrationRouter = () => {
  const { createDialog } = useDialog()
  const t = useT()
  const navigation = useNavigation()
  const { setOptions, tabBarOptions } = useScreenOptions()

  const currentOrganization = useSelector(selectCurrentOrganization)
  const currentModule = useSelector(selectCurrentModule)

  const tabsOptions = useMemo(
    () => ({
      stationMap: {
        title: t('station', { howMany: 2 }),
        tabBarIcon: ({ color }) => (
          <Icon icon={ weatherStation } size={ 28 } color={ color } />
        )
      }
      // socialFeed: {
      //   title: t('network'),
      //   tabBarIcon: ({ color, size }) => (
      //     <Icon icon={ socialNetwork } size={ size } color={ color } />
      //   )
      // }
    }),
    [t]
  )

  const changeOrganizationDialog = useCallback(() => {
    createDialog({
      id: 'organization-onboarding',
      Component: ChangeOrganization
    })
  }, [createDialog])

  useEffect(() => {
    if (isEmpty(currentOrganization && !isEmpty(currentModule))) {
      changeOrganizationDialog()
    }
  }, [changeOrganizationDialog, currentModule, currentOrganization])

  useEffect(() => {
    if (Platform.OS === 'android') {
      PermissionsAndroid.requestMultiple([
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION
      ])
    }
  }, [t])

  useFocusEffect(
    useCallback(() => {
      setOptions({
        headerLeft: () => <MenuHamburguerButton navigation={ navigation } />,
        headerTitle: (props) => (
          <ScreenHeaderTitleContainer>
            <HeaderTitle onPress={ changeOrganizationDialog } { ...props }>
              <Row>
                <Title { ...props } numberOfLines={ 1 } ellipsizeMode="tail">
                  {currentOrganization.tradeName ||
                    currentOrganization.companyName ||
                    t('select one organization')}
                </Title>
                <IconContainer>
                  <Icon icon={ arrowDown } color={ colors.white } />
                </IconContainer>
              </Row>
            </HeaderTitle>
          </ScreenHeaderTitleContainer>
        )
      })
    }, [
      changeOrganizationDialog,
      currentOrganization.companyName,
      currentOrganization.tradeName,
      navigation,
      setOptions,
      t
    ])
  )

  return (
    <Tab.Navigator
      initialRouteName="StationMap"
      tabBarOptions={ tabBarOptions }
      lazy={ false }
    >
      <Tab.Screen
        name="StationMap"
        options={ tabsOptions.stationMap }
        component={ WeatherStationMapScreen }
      />
      {/* <Tab.Screen
        name="SocialFeed"
        options={ tabsOptions.socialFeed }
        component={ SocialFeedScreen }
      /> */}
    </Tab.Navigator>
  )
}

export default withDrawer(AdministrationRouter)
