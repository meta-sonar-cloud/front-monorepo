import React, { useState, useCallback, useEffect, useRef } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import { useNavigation, useFocusEffect } from '@react-navigation/native'

import I18n from '@smartcoop/i18n'
import { barn, currency, smallPlant, dashboard } from '@smartcoop/icons'
import ActivityIndicator from '@smartcoop/mobile-components/ActivityIndicator'
import Icon from '@smartcoop/mobile-components/Icon'
import { useScreenOptions } from '@smartcoop/mobile-containers/hooks/useScreenOptions'
import useTerm from '@smartcoop/mobile-containers/hooks/useTerm'
import { ContentWithoutHeader } from '@smartcoop/mobile-containers/layouts/AuthenticatedLayout/theme'
import {
  selectUserHaveDigitalProperty,
  selectUserHaveCommercialization,
  selectModulesHaveTechnical,
  selectUserHaveAdministration
  , selectPermissionWeatherStations } from '@smartcoop/stores/authentication/selectorAuthentication'
import { ModuleActions } from '@smartcoop/stores/module'
import Logo from '@smartcoop/styles/assets/images/smartcoop-logo-inverted.svg'
import colors from '@smartcoop/styles/colors'

import {
  Container,
  Title,
  Scroll,
  ButtonsContainer,
  Button,
  ButtonTouchable,
  ButtonText,
  LogoContainer,
  Header,
  TitleContainer
} from './styles'

const ChooseModuleScreen = () => {
  const dispatch = useCallback(useDispatch(), [])
  const navigation = useNavigation()

  const mounted = useRef(false)
  const [loading, setLoading] = useState(false)

  const hasDigitalProperty = useSelector(selectUserHaveDigitalProperty)
  const hasCommercialization = useSelector(selectUserHaveCommercialization)
  const hasTechnical = useSelector(selectModulesHaveTechnical)
  const hasAdministration = useSelector(selectUserHaveAdministration)
  const permissionWeatherStations = useSelector(selectPermissionWeatherStations)
  const { setOptions } = useScreenOptions()

  const [privacyModal, selectedPrivacyTerm] = useTerm('privacy-term', false)
  const [termModal] = useTerm('use-term', false)

  useEffect(() => {
    if(mounted.current) {
      privacyModal(termModal)
    } else {
      mounted.current = true
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [selectedPrivacyTerm])

  // prevent go back
  useEffect(() => {
    navigation.addListener('beforeRemove', (e) => e.preventDefault())
  }, [navigation])

  useFocusEffect(useCallback(
    () => setOptions({ headerShown: false }),
    [setOptions]
  ))

  const initModule = useCallback(
    (action) => {
      setLoading(true)

      // force loader shown before navigation (lazy was freezing the screen)
      setTimeout(() => dispatch(ModuleActions[action]()), 200 )
    },
    [dispatch]
  )

  return (
    <>
      {loading && <ActivityIndicator />}
      <Container>
        <ContentWithoutHeader>
          <Scroll>

            <Header>
              <TitleContainer>
                <I18n as={ Title }>
                  choose a module to continue
                </I18n>
              </TitleContainer>
              <LogoContainer>
                <Logo width={ 100 }/>
              </LogoContainer>
            </Header>

            <ButtonsContainer>
              {hasDigitalProperty && (
                <Button style={ { marginBottom: 30 } }>
                  <ButtonTouchable
                    onPress={ () => initModule('initDigitalPropertyModule') }
                  >
                    <Icon icon={ barn } size={ 72 } color={ colors.secondary }/>
                    <ButtonText>
                      <I18n>digital property</I18n>
                    </ButtonText>
                  </ButtonTouchable>
                </Button>
              )}

              {hasCommercialization && (
                <Button style={ { marginBottom: 30 } }>
                  <ButtonTouchable
                    onPress={ () => initModule('initCommercializationModule') }
                  >
                    <Icon icon={ currency } size={ 72 } color={ colors.secondary }/>
                    <ButtonText>
                      <I18n>commercialization</I18n>
                    </ButtonText>
                  </ButtonTouchable>
                </Button>
              )}

              {hasTechnical && (
                <Button style={ { marginBottom: 30 } }>
                  <ButtonTouchable
                    onPress={ () => initModule('initTechnicalModule') }
                  >
                    <Icon icon={ smallPlant } size={ 72 } color={ colors.secondary }/>
                    <ButtonText>
                      <I18n>technical area</I18n>
                    </ButtonText>
                  </ButtonTouchable>
                </Button>
              )}

              {/* TODO: temporary solution while the administrator module has only weather stations on the mobile */}
              {hasAdministration && permissionWeatherStations && (
                <Button style={ { marginBottom: 30 } }>
                  <ButtonTouchable
                    onPress={ () => initModule('initAdministrationModule') }
                  >
                    <Icon icon={ dashboard } size={ 100 } color={ colors.secondary }/>
                    <ButtonText>
                      <I18n>administration</I18n>
                    </ButtonText>
                  </ButtonTouchable>
                </Button>
              )}
            </ButtonsContainer>

          </Scroll>
        </ContentWithoutHeader>
      </Container>
    </>
  )
}

export default ChooseModuleScreen
