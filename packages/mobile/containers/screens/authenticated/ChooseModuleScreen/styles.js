import { Headline, Paragraph } from 'react-native-paper'

import styled from 'styled-components/native'


import { Scroll as ExternalScroll } from '@smartcoop/mobile-containers/layouts/AuthenticatedLayout/theme'
import colors from '@smartcoop/styles/colors'

export const Container = styled.View`
  background-color: ${ colors.black };
  padding: 0 32px;
  height: 100%;
`

export const Title = styled(Paragraph)`
  color: #fff;
  font-weight: 600;
  font-family: Montserrat-Bold;
`

export const Scroll = styled(ExternalScroll)`
`

export const TitleContainer = styled.View`
  align-items: flex-start;
  flex-wrap: wrap;
  flex: 1;
  margin-right: 10%;
`
export const ButtonsContainer = styled.View`
  display: flex;
  flex-direction: column;
  justify-content: space-around;
  width: 100%;
`

export const Header = styled.View`
  flex-direction: row;
  justify-content: space-around;
  align-items: center;
  width: 100%;
  margin-top: 30px;
`

export const Button = styled.View`
  flex: 1;
  background-color: ${ colors.blackLight };
  padding: 22px;
  border-radius: 4.8px;
  width: 100%;
  height: 182px;
`

export const ButtonTouchable = styled.TouchableOpacity`
  display: flex;
`

export const ButtonText = styled(Headline)`
  color: ${ colors.white };
  font-weight: 700;
  margin-top: 15px;
`

export const LogoContainer = styled.View`
  display: flex;
  flex-direction: row;
  justify-content: flex-end;
`
