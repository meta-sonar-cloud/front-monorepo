import React from 'react'

import { useNavigation } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'

import { useScreenOptions, withScreenOptions } from '@smartcoop/mobile-containers/hooks/useScreenOptions'

import PublishScreen from './PublishScreen'

const Stack = createStackNavigator()

const SocialScreenRouter = () => {
  const navigation = useNavigation()
  const { options } = useScreenOptions(navigation)

  return (
    <Stack.Navigator screenOptions={ options } mode="modal">
      <Stack.Screen
        name="Publish"
        component={ PublishScreen }
      />
    </Stack.Navigator>
  )
}

export default withScreenOptions(SocialScreenRouter)
