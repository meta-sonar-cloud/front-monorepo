import React, { useCallback } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import { useNavigation , useFocusEffect } from '@react-navigation/native'

import I18n, { useT } from '@smartcoop/i18n'
import Post from '@smartcoop/mobile-components/Post'
import { useScreenOptions } from '@smartcoop/mobile-containers/hooks/useScreenOptions'
import AuthenticatedLayout from '@smartcoop/mobile-containers/layouts/AuthenticatedLayout'
import { ScreenHeaderTitleContainer, HeaderTitle , Scroll } from '@smartcoop/mobile-containers/layouts/AuthenticatedLayout/theme'
import { useSnackbar } from '@smartcoop/snackbar'
import { SocialActions } from '@smartcoop/stores/social'
import { selectUser } from '@smartcoop/stores/user/selectorUser'

const PublishScreen = () => {
  const t = useT()
  const snackbar = useSnackbar()
  const dispatch = useCallback(useDispatch(), [])
  const navigation = useNavigation()
  const { setOptions } = useScreenOptions()

  const user = useSelector(selectUser)

  const handleSubmitPost = useCallback(
    (data) => {
      dispatch(SocialActions.saveOfflinePost(
        data,
        () => {
          snackbar.success(t('your {this} was registered', {
            howMany: 1,
            gender: 'female',
            this: t('post', { howMany: 1 })
          }))
          navigation.goBack()
        },
        () => {
          snackbar.warning(t('we were unable to post your message now. But don\'t worry, as we will publish it as soon as possible.'))
          navigation.goBack()
        }
      ))
    },
    [dispatch, navigation, snackbar, t]
  )

  useFocusEffect(useCallback(
    () => {
      setOptions({
        headerTitle: () => (
          <ScreenHeaderTitleContainer>
            <I18n as={ HeaderTitle }>
              publish
            </I18n>
          </ScreenHeaderTitleContainer>
        )
      })
    },
    [setOptions]
  ))

  return (
    <AuthenticatedLayout>
      <Scroll padding={ 0 }>
        <Post
          user={ user }
          onSubmit={ handleSubmitPost }
        />
      </Scroll>
    </AuthenticatedLayout>
  )
}

export default PublishScreen
