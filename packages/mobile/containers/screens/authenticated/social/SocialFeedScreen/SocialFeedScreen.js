import React, { useState, useCallback, useMemo } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import { useNavigation, useFocusEffect } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'

import { useT } from '@smartcoop/i18n'
import Fab from '@smartcoop/mobile-components/Fab'
import Feed from '@smartcoop/mobile-components/Feed'
import InputSearch from '@smartcoop/mobile-components/InputSearch'
import List from '@smartcoop/mobile-components/List'
import useTerm from '@smartcoop/mobile-containers/hooks/useTerm'
import { SocialActions } from '@smartcoop/stores/social'
import { selectPosts } from '@smartcoop/stores/social/selectorSocial'

import { Container, InputView } from './styles'

const SocialFeedScreen = () => {
  const [filterText, setFilterText] = useState('')
  const [forceListUpdate, setForceListUpdate] = useState()
  const dispatch = useCallback(useDispatch(), [])

  useTerm('social-network-term')
  const t = useT()
  const navigation = useNavigation()

  const posts = useSelector(selectPosts)

  const deps = useMemo(
    () => [forceListUpdate],
    [forceListUpdate]
  )

  const handleChangeSearchFilter = useCallback(
    (value) => {
      setFilterText(value)
    },
    []
  )

  const handleCreatePost = useCallback(
    () => navigation.navigate('Social', { screen: 'Publish' }),
    [navigation]
  )

  const renderItem = useCallback(
    ({ item }) => <Feed feed={ item } />,
    []
  )

  const loadPosts = useCallback(
    (page, onSuccess, onError) => {
      dispatch(SocialActions.loadPosts(
        { page },
        onSuccess,
        onError
      ))
    },
    [dispatch]
  )

  useFocusEffect(
    useCallback(() => {
      setForceListUpdate(old => !old)
    }, [])
  )

  return (
    <>
      <Container>

        <InputView>
          <InputSearch
            detached
            name="social-search"
            onChange={ handleChangeSearchFilter }
            value={ filterText }
            placeholder={ t('search') }
            style={ { marginBottom: 0 } }
          />
        </InputView>

        <Fab onPress={ handleCreatePost } />

        <List
          data={ posts }
          renderItem={ renderItem }
          onListLoad={ loadPosts }
          deps={ deps }
          hideUpdatedAt
        />
      </Container>
    </>
  )
}

const Stack = createStackNavigator()

// eslint-disable-next-line react/prop-types
export default ({ stackOptions }) => (
  <Stack.Navigator screenOptions={ stackOptions }>
    <Stack.Screen
      name="Feed"
      component={ SocialFeedScreen }
    />
  </Stack.Navigator>
)
