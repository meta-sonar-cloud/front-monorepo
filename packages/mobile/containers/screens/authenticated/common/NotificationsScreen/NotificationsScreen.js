import React, { useCallback } from 'react'
import { TouchableOpacity } from 'react-native'
import { useSelector, useDispatch } from 'react-redux'

import { useFocusEffect } from '@react-navigation/native'

import I18n from '@smartcoop/i18n'
import { checked } from '@smartcoop/icons'
import Icon from '@smartcoop/mobile-components/Icon'
import List from '@smartcoop/mobile-components/List'
import Notification from '@smartcoop/mobile-components/Notification'
import { useScreenOptions } from '@smartcoop/mobile-containers/hooks/useScreenOptions'
import AuthenticatedLayout from '@smartcoop/mobile-containers/layouts/AuthenticatedLayout'
import { ScreenHeaderTitleContainer, HeaderTitle } from '@smartcoop/mobile-containers/layouts/AuthenticatedLayout/theme'
import { MessagingActions } from '@smartcoop/stores/messaging'
import { selectAllNotifications, selectNewNotificationsCount } from '@smartcoop/stores/messaging/selectorMessaging'
import { colors } from '@smartcoop/styles'

const NotificationsScreen = () => {
  const dispatch = useCallback(useDispatch(), [])
  const { setOptions } = useScreenOptions()

  const notifications = useSelector(selectAllNotifications)
  const unreadCount = useSelector(selectNewNotificationsCount)

  const handleNotificationClick = useCallback((notification) => {
    if (!notification.read) {
      dispatch(MessagingActions.readNotification(notification.id))
    }
  }, [dispatch])

  const getAllNotifications = useCallback((firstPage, onSuccess, onError) => {
    dispatch(MessagingActions.getAllNotifications(firstPage, onSuccess, onError))
  }, [dispatch])

  const renderItem = useCallback(
    ({ item }) => (
      <Notification
        notification={ item }
        onPress={ () => handleNotificationClick(item) }
      />
    ),
    [handleNotificationClick]
  )

  const onListLoad = useCallback(
    (page, resolve, reject) => {
      getAllNotifications(
        page === 1,
        (end) => resolve({
          page,
          totalPages: end ?  page : page + 1
        }),
        reject
      )
    },
    [getAllNotifications]
  )

  useFocusEffect(useCallback(
    () => {
      setOptions({
        headerTitle: () => (
          <ScreenHeaderTitleContainer>
            <I18n as={ HeaderTitle } params={ { howMany: 2 } }>
              notification
            </I18n>
          </ScreenHeaderTitleContainer>
        ),
        headerRight: () => unreadCount && (
          <TouchableOpacity
            onPress={ () => dispatch(MessagingActions.readAllNotifications()) }
            style={ { marginRight: 5 } }
          >
            <Icon icon={ checked } color={ colors.white } />
          </TouchableOpacity>
        )
      })
    },
    [dispatch, setOptions, unreadCount]
  ))

  return (
    <AuthenticatedLayout>
      <List
        data={ notifications }
        renderItem={ renderItem }
        onListLoad={ onListLoad }
      />
    </AuthenticatedLayout>
  )
}

export default NotificationsScreen
