import React, { useCallback, useState, useMemo } from 'react'

import { useFocusEffect } from '@react-navigation/native'

import debounce from 'lodash/debounce'

import I18n, { useT } from '@smartcoop/i18n'
import InputSearch from '@smartcoop/mobile-components/InputSearch'
import UserAccessList from '@smartcoop/mobile-containers/fragments/UserAccessList'
import { useScreenOptions } from '@smartcoop/mobile-containers/hooks/useScreenOptions'
import AuthenticatedLayout from '@smartcoop/mobile-containers/layouts/AuthenticatedLayout'
import { ScreenHeaderTitleContainer, HeaderTitle } from '@smartcoop/mobile-containers/layouts/AuthenticatedLayout/theme'

import { ModalFieldContainer, SafeAreaViewList } from './styles'

const UserAccessScreen = () => {
  const [filterText, setFilterText] = useState('')
  const [debouncedFilterText, setDebouncedFilterText] = useState('')
  const t = useT()
  const { setOptions } = useScreenOptions()

  const params = useMemo(
    () => ({
      q: debouncedFilterText
    }), [debouncedFilterText]
  )

  const debouncedChangeSearchFilter = useCallback(
    debounce((value) => {
      setDebouncedFilterText(value)
    }, 300), []
  )

  const onChangeSearchFilter = useCallback(
    (value) => {
      setFilterText(value)
      debouncedChangeSearchFilter(value)
    }, [debouncedChangeSearchFilter]
  )

  useFocusEffect(useCallback(
    () => {
      setOptions({
        headerTitle: () => (
          <ScreenHeaderTitleContainer>
            <I18n as={ HeaderTitle }>
              user access
            </I18n>
          </ScreenHeaderTitleContainer>
        )
      })
    },
    [setOptions]
  ))

  return (
    <AuthenticatedLayout>
      <ModalFieldContainer>
        <InputSearch
          detached
          name="select-search-input"
          label={ t('search') }
          value={ filterText }
          onChange={ onChangeSearchFilter }
          style={ { marginBottom: 0 } }
        />
      </ModalFieldContainer>
      <SafeAreaViewList>
        <UserAccessList params={ params } />
      </SafeAreaViewList>
    </AuthenticatedLayout>
  )
}

export default UserAccessScreen
