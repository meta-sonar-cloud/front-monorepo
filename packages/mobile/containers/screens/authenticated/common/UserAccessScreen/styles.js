import styled from 'styled-components/native'


export const ModalFieldContainer = styled.View`
  padding: 15px 15px 7px;
`

export const SafeAreaViewList = styled.SafeAreaView`
  flex: 100;
  width: 100%;
`
