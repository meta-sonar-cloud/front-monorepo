import React from 'react'

import { useNavigation } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'

import { useScreenOptions, withScreenOptions } from '@smartcoop/mobile-containers/hooks/useScreenOptions'

import EditAddressScreen from './EditAddressScreen'
import EditProfileScreen from './EditProfileScreen'
import NotificationsScreen from './NotificationsScreen'
import ProfileScreen from './ProfileScreen'
import UserAccessScreen from './UserAccessScreen'

const Stack = createStackNavigator()

const CommonRoutes = () => {
  const navigation = useNavigation()
  const { options } = useScreenOptions(navigation)

  return (
    <Stack.Navigator screenOptions={ options }>
      <Stack.Screen
        name="Notifications"
        component={ NotificationsScreen }
      />

      <Stack.Screen
        name="UserAccess"
        component={ UserAccessScreen }
      />

      <Stack.Screen
        name="Profile"
        component={ ProfileScreen }
      />

      <Stack.Screen
        name="EditProfile"
        component={ EditProfileScreen }
      />

      <Stack.Screen
        name="EditAddress"
        component={ EditAddressScreen }
      />
    </Stack.Navigator>
  )
}

export default withScreenOptions(CommonRoutes)
