import styled from 'styled-components'

import colors from '@smartcoop/styles/colors'


export const Container = styled.View`
  background: ${ colors.white };
  flex: 1;
`
