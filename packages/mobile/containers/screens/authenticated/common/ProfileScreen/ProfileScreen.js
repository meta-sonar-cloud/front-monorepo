import React, { useEffect, useCallback } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import { useFocusEffect } from '@react-navigation/native'

import I18n, { useT } from '@smartcoop/i18n'
import Button from '@smartcoop/mobile-components/Button'
import UserAdditionalInfoFragment from '@smartcoop/mobile-containers/fragments/profile/UserAdditionalInfoFragment/UserAdditionalInfoFragment'
import UserInfoFragment from '@smartcoop/mobile-containers/fragments/profile/UserInfoFragment/UserInfoFragment'
import { useScreenOptions } from '@smartcoop/mobile-containers/hooks/useScreenOptions'
import { Scroll , ScreenHeaderTitleContainer, HeaderTitle } from '@smartcoop/mobile-containers/layouts/AuthenticatedLayout/theme'
import { AuthenticationActions } from '@smartcoop/stores/authentication'
import { OrganizationActions } from '@smartcoop/stores/organization'
import { selectUserOrganizations } from '@smartcoop/stores/organization/selectorOrganization'
import { selectUser } from '@smartcoop/stores/user/selectorUser'

import { Container } from './styles'

const ProfileScreen = () => {
  const t = useT()
  const dispatch = useCallback(useDispatch(), [])
  const { setOptions } = useScreenOptions()

  const user = useSelector(selectUser)
  const userOrganizations = useSelector(selectUserOrganizations)

  useEffect(() => {
    dispatch(OrganizationActions.loadUserOrganizations())
  }, [dispatch])

  useFocusEffect(useCallback(
    () => {
      setOptions({
        headerTitle: () => (
          <ScreenHeaderTitleContainer>
            <I18n
              as={ HeaderTitle }
              params={ { howMany: 1 } }
            >
              profile
            </I18n>
          </ScreenHeaderTitleContainer>
        )
      })
    },
    [setOptions]
  ))

  return (
    <Scroll pad>
      <Container>
        <UserInfoFragment user={ user } />
        <UserAdditionalInfoFragment
          user={ user }
          userOrganizations={ userOrganizations }
        />

        <Button
          title={ t('logout') }
          onPress={ () => dispatch(AuthenticationActions.logout()) }
          style={ { margin: 20 } }
        />
      </Container>
    </Scroll>

  )
}

export default ProfileScreen
