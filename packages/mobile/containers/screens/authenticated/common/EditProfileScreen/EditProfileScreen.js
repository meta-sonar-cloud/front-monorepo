import React, { useCallback, useMemo, useRef, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import trimMask from '@meta-awesome/functions/src/trimMask'
import { useNavigation , useFocusEffect } from '@react-navigation/native'

import { useDialog } from '@smartcoop/dialog'
import I18n, { useT } from '@smartcoop/i18n'
import EditProfileForm from '@smartcoop/mobile-containers/forms/digitalProperty/profile/EditProfileForm'
import { useScreenOptions } from '@smartcoop/mobile-containers/hooks/useScreenOptions'
import { ScreenHeaderTitleContainer, HeaderTitle } from '@smartcoop/mobile-containers/layouts/AuthenticatedLayout/theme'
import { useSnackbar } from '@smartcoop/snackbar'
import { UserActions } from '@smartcoop/stores/user'
import { selectUser } from '@smartcoop/stores/user/selectorUser'

import LoadingModal from '../../../../modals/LoadingModal/LoadingModal'
import { Container } from './styles'

const EditProfileScreen = () => {
  const formRef = useRef(null)
  const snackbar = useSnackbar()
  const dispatch = useCallback(useDispatch(), [])
  const t = useT()
  const navigation = useNavigation()
  const { setOptions } = useScreenOptions()
  const { createDialog, removeDialog } = useDialog()

  const user = useSelector(selectUser)

  const defaultValues = useMemo(
    () => ({ ...user }), [user]
  )

  const closeModal = useCallback(
    () => {
      navigation.navigate('Profile')
    }, [navigation]
  )

  const loadUserSuccess = useCallback(
    () => {
      snackbar.success(
        t('your {this} was edited', {
          howMany: 1,
          gender: 'male',
          this: t('user', { howMany: 1 })
        })
      )
      closeModal()

    }, [closeModal, snackbar, t]
  )

  const loadUser = useCallback(
    () => {
      dispatch(UserActions.loadUser(
        () => {loadUserSuccess()},
        () => {closeModal()}
      ))
    }, [closeModal, dispatch, loadUserSuccess]
  )


  const submitForms = useCallback(
    (data) => {
      createDialog({
        id: 'loading',
        Component: LoadingModal
      })

      dispatch(UserActions.saveUser(
        {
          ...user,
          ...data,
          cellPhone: trimMask(data.cellPhone)
        },
        () => loadUser(),
        () => closeModal()
      ))
    }, [closeModal, createDialog, dispatch, loadUser, user]
  )

  // Remove loader on close screen
  useEffect(() => () => removeDialog({ id: 'loading' }), [removeDialog])

  useFocusEffect(useCallback(
    () => {
      setOptions({
        headerTitle: () => (
          <ScreenHeaderTitleContainer>
            <I18n as={ HeaderTitle }>
              edit profile
            </I18n>
          </ScreenHeaderTitleContainer>
        )
      })
    },
    [setOptions]
  ))

  return (
    <Container>
      <EditProfileForm
        ref={ formRef }
        defaultValues={ defaultValues }
        onSubmit={ submitForms }
        onCancel={ closeModal }
      />
    </Container>
  )
}

export default EditProfileScreen
