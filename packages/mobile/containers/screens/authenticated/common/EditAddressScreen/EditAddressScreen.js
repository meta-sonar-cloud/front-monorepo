import React, { useCallback, useMemo, useRef } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import trimMask from '@meta-awesome/functions/src/trimMask'
import { useNavigation , useFocusEffect } from '@react-navigation/native'

import { toString } from 'lodash'

import I18n, { useT } from '@smartcoop/i18n'
import EditAddressForm from '@smartcoop/mobile-containers/forms/digitalProperty/profile/EditAddressForm'
import { useScreenOptions } from '@smartcoop/mobile-containers/hooks/useScreenOptions'
import { ScreenHeaderTitleContainer, HeaderTitle } from '@smartcoop/mobile-containers/layouts/AuthenticatedLayout/theme'
import { useSnackbar } from '@smartcoop/snackbar'
import { UserActions } from '@smartcoop/stores/user'
import { selectUser } from '@smartcoop/stores/user/selectorUser'


import { Container } from './styles'

const EditAddressScreen = () => {

  const formRef = useRef(null)
  const snackbar = useSnackbar()
  const dispatch = useCallback(useDispatch(), [])
  const t = useT()
  const navigation = useNavigation()
  const { setOptions } = useScreenOptions()

  const user = useSelector(selectUser)

  const defaultValues = useMemo(
    () => ({ ...user, number: toString(user.number) }), [user]
  )

  const closeModal = useCallback(
    () => {
      navigation.navigate('Profile')
    }, [navigation]
  )

  const loadUserSuccess = useCallback(
    () => {
      snackbar.success(
        t('your {this} was edited', {
          howMany: 1,
          gender: 'male',
          this: t('address', { howMany: 1 })
        })
      )
      closeModal()

    }, [closeModal, snackbar, t]
  )

  const loadUser = useCallback(
    () => {
      dispatch(UserActions.loadUser(
        () => {loadUserSuccess()},
        () => {closeModal()}
      ))
    }, [closeModal, dispatch, loadUserSuccess]
  )


  const submitForms = useCallback(
    (data) => {
      dispatch(UserActions.saveUser(
        {
          ...data,
          postalCode: trimMask(data.postalCode)
        },
        () => {loadUser()},
        () => {closeModal()}
      ))
    }, [closeModal, dispatch, loadUser]
  )

  useFocusEffect(useCallback(
    () => {
      setOptions({
        headerTitle: () => (
          <ScreenHeaderTitleContainer>
            <I18n as={ HeaderTitle }>
              edit address
            </I18n>
          </ScreenHeaderTitleContainer>
        )
      })
    },
    [setOptions]
  ))

  return (
    <Container>
      <EditAddressForm
        ref={ formRef }
        defaultValues={ defaultValues }
        onSubmit={ submitForms }
        onCancel={ closeModal }
      />
    </Container>
  )
}

export default EditAddressScreen
