import React, { useMemo, useCallback } from 'react'
import { View } from 'react-native'

import PropTypes from 'prop-types'

import map from 'lodash/map'

import RadioButton from '@smartcoop/mobile-components/RadioButton'

import { Container, RadioGroupContainer, Label, LabelError } from './styles'

const RadioGroupStyled = (props) => {
  const {
    options,
    value,
    variant,
    style,
    label: externalLabel,
    labelStyle: externalLabelStyle,
    clearable,
    onChange,
    disabled,
    itemStyle,
    itemFontStyle,
    activeId,
    activeMessage,
    error,
    required,
    ...rest
  } = props

  const handleChange = useCallback(
    ({ value: val }) => {
      if (!disabled) {
        onChange({ value: clearable && val === value ? null : val })
      }
    },
    [clearable, disabled, onChange, value]
  )

  const customMargin = useMemo(
    () => (variant === 'row' ? { marginRight: 5 } : { marginBottom: 5 }),
    [variant]
  )

  return (
    <Container style={ { ...style } }>
      {externalLabel !== '' && (
        <View style={ { flexDirection: 'row' } }>
          <Label style={ externalLabelStyle }>
            {externalLabel}
            {required ? ' *' : ''}
          </Label>
          {error && <LabelError>{error}</LabelError>}
        </View>
      )}

      <RadioGroupContainer style={ { flexDirection: variant } }>
        {map(options, ({ label, value: optionValue }) => (
          <RadioButton
            key={ optionValue }
            label={ label }
            value={ optionValue }
            index={ optionValue }
            disabled={ disabled }
            onChange={ handleChange }
            checked={ value === optionValue }
            style={ { ...customMargin, ...itemStyle } }
            fontStyle={ { ...itemFontStyle } }
            { ...rest }
          />
        ))}
      </RadioGroupContainer>
    </Container>
  )
}

RadioGroupStyled.propTypes = {
  options: PropTypes.array.isRequired,
  value: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.bool,
    PropTypes.string
  ]),
  variant: PropTypes.oneOf(['row', 'column']),
  style: PropTypes.object,
  itemStyle: PropTypes.object,
  itemFontStyle: PropTypes.object,
  label: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
  labelStyle: PropTypes.object,
  onChange: PropTypes.func,
  clearable: PropTypes.bool,
  disabled: PropTypes.bool,
  activeId: PropTypes.string,
  activeMessage: PropTypes.string,
  error: PropTypes.string,
  required: PropTypes.bool
}

RadioGroupStyled.defaultProps = {
  value: '',
  variant: 'column',
  style: {},
  itemStyle: {},
  itemFontStyle: {},
  label: '',
  labelStyle: {},
  clearable: false,
  onChange: (value) => value,
  disabled: false,
  activeId: null,
  activeMessage: null,
  error: null,
  required: false
}

export default RadioGroupStyled
