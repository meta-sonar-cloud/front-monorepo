import styled from 'styled-components/native'

import colors from '@smartcoop/styles/colors'

export const Container = styled.View`
  height: auto;
  width: auto;

  display: flex;
  flex-direction: column;
`

export const RadioGroupContainer = styled.View`
  height: auto;
  width: auto;

  display: flex;
  flex-direction: row;
`

export const Label = styled.Text`
  margin-bottom: 8px;
`

export const LabelError = styled(Label)`
  color: ${ colors.error };
  padding-left: 10px;
`
