import React, { useMemo } from 'react'

import moment from 'moment/moment'
import PropTypes from 'prop-types'

import map from 'lodash/map'

import { publicType } from '@smartcoop/icons'
import Avatar from '@smartcoop/mobile-components/Avatar'
import Badge from '@smartcoop/mobile-components/Badge'
import Divider from '@smartcoop/mobile-components/Divider'
import Icon from '@smartcoop/mobile-components/Icon'
import ReadMore from '@smartcoop/mobile-components/ReadMore'
import avatarImg from '@smartcoop/styles/assets/images/avatar.png'
import colors from '@smartcoop/styles/colors'
import { momentBackDateTimeFormat, momentFriendlyDateTimeFormat } from '@smartcoop/utils/dates'

import { Container, AvatarSubtitle, TagsContainer, Header, Identifier, Body, Text, Footer, Timer } from './styles'

const Feed = (props) =>  {
  const {
    style,
    length,
    styleBadge,
    feed: {
      interestAreas,
      user,
      createdAt,
      text
    }
  } = props

  const createdAtFormatted = useMemo(
    () => moment(createdAt, momentBackDateTimeFormat).format(momentFriendlyDateTimeFormat),
    [createdAt]
  )

  return (
    <Container style={ style } >
      <Header>
        <Identifier>
          <Avatar
            source={ avatarImg }
            title={ user.name }
            subTitle={ (
              <AvatarSubtitle>
                <Icon icon={ publicType } size={ 16 } color={ colors.darkGrey } />
                <Timer>{createdAtFormatted}</Timer>
              </AvatarSubtitle>
            ) }
          />
        </Identifier>
      </Header>
      <Body>
        <Text>
          <ReadMore length={ length }>
            { text }
          </ReadMore>
        </Text>
      </Body>
      <TagsContainer>
        {
          map(interestAreas, (interestArea) => (
            <Badge
              key={ interestArea.id }
              color={ interestArea.color }
              style={ styleBadge }
            >
              {interestArea.name}
            </Badge>
          ))
        }
      </TagsContainer>
      <Divider />
      <Footer />
    </Container>
  )
}

Feed.propTypes = {
  style: PropTypes.object,
  feed: PropTypes.shape({
    interestAreas: PropTypes.arrayOf(PropTypes.shape({
      id: PropTypes.string,
      name: PropTypes.string,
      categoryId: PropTypes.string,
      category: PropTypes.shape({
        id: PropTypes.string,
        name: PropTypes.string
      }),
      color: PropTypes.string
    })).isRequired,
    id: PropTypes.string,
    text: PropTypes.string,
    userId: PropTypes.string,
    createdAt: PropTypes.string,
    updatedAt: PropTypes.string,
    user: PropTypes.shape({
      id: PropTypes.string,
      name: PropTypes.string,
      email: PropTypes.string,
      city: PropTypes.string,
      state: PropTypes.string,
      image: PropTypes.string
    })
  }).isRequired,
  length: PropTypes.number,
  styleBadge: PropTypes.object
}

Feed.defaultProps = {
  style: {},
  length: 200,
  styleBadge: {
    marginRight: 10,
    marginBottom: 10
  }
}

export default Feed
