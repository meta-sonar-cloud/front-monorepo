import styled from 'styled-components/native'

import colors from '@smartcoop/styles/colors'

export const Container = styled.View`
`

export const Header = styled.View`
  padding: 10px 0 15px;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  flex-wrap: wrap;
`

export const Identifier = styled.View`
  flex-direction: row;
  justify-content: center;
  align-items: center;
`

export const Timer = styled.Text`
  font-size: 13px;
  color: ${ colors.darkGrey };
  font-family: OpenSans-Light;
  padding-left: 5px;
`

export const Body = styled.View`
  padding: 0px 12px 5px;
`

export const Text = styled.Text`
  font-size: 16px;
  color: ${ colors.darkGrey };
  font-family: OpenSans-Light;
`

export const Footer = styled.View`
  flex-direction: row;
  padding: 10px 10px;
  width: 100%;
`

export const ColFooter1 = styled.View`
  flex-direction: row;
  justify-content: flex-start;
  align-items: center;
  flex-wrap: wrap;
  flex: 1;
  padding: 0 10px 5px;
`

export const TagsContainer = styled.View`
  flex-direction: row;
  width: 100%;
  justify-content: flex-start;
  align-items: center;
  flex-wrap: wrap;
  padding: 0 10px 0px;
`

export const AvatarSubtitle = styled.View`
  flex-direction: row;
  align-items: center;
  padding-top: 5px;
`
