import React, { useMemo, useCallback } from 'react'

import moment from 'moment/moment'
import PropTypes from 'prop-types'

import isUndefined from 'lodash/isUndefined'

import I18n from '@smartcoop/i18n'
import {  pencil, trash } from '@smartcoop/icons'
import Badge from '@smartcoop/mobile-components/Badge'
import Button from '@smartcoop/mobile-components/Button'
import Icon from '@smartcoop/mobile-components/Icon'
import { colors } from '@smartcoop/styles'
import { momentFriendlyDateFormat, momentBackDateFormat } from '@smartcoop/utils/dates'
import { formatNumber } from '@smartcoop/utils/formatters'

import {
  Container,
  TextGroup,
  TextLeft,
  Text,
  Title,
  TitleBold,
  Actions,
  Date
} from './styles'

const MilkDeliveryItem = (props) => {
  const {
    data,
    onEdit,
    onDelete
  } = props

  const volumeDate = useMemo(
    () => (moment(data.volumeDate, momentBackDateFormat).format(momentFriendlyDateFormat)),
    [data]
  )

  const isManualStatus = useCallback(
    (item) => (
      item.status?.slug
      && item.status?.slug === 'manual'
    ),
    []
  )

  return (
    <Container>
      <TextGroup>
        <TextLeft>
          <TitleBold>
            <I18n as={ TitleBold }>total volume</I18n>:
          </TitleBold>
          <Text>
            <I18n as={ Text }>condemned volume</I18n>:
          </Text>
          <Text>
            <I18n as={ Text }>temperature</I18n>:
          </Text>
        </TextLeft>

        <TextLeft>
          <TitleBold>
            {data.volume} <I18n as={ TitleBold }>liters</I18n>
          </TitleBold>
          <Text>
            {isUndefined(data.condemnedVolume) ? '-' : (
              <>
                {`${ data.condemnedVolume } `}
                <I18n as={ Title }>liters</I18n>
              </>
            )}
          </Text>
          <Text>
            {isUndefined(data.temperature) ? '-' : `${ formatNumber(data.temperature) } °C`}
          </Text>
        </TextLeft>

        <TextLeft>
          <Date>
            {volumeDate}
          </Date>
          <Actions>
            <Button
              variant="outlined"
              style={ { marginRight: 5, padding: 5, borderColor: colors.lightGrey } }
              icon={ <Icon size={ 14 } icon={ pencil } color={ colors.black } /> }
              backgroundColor={ colors.white }
              onPress={ onEdit }
              disabled={ !isManualStatus(data) }
            />
            <Button
              variant="outlined"
              style={ { padding: 5, borderColor: colors.lightGrey } }
              icon={ <Icon size={ 14 } icon={ trash } color={ colors.red } /> }
              backgroundColor={ colors.white }
              onPress={ onDelete }
              disabled={ !isManualStatus(data) }
            />
          </Actions>
          <Badge
            backgroundColorBadge={ data.status.color }
            color={ data.status.color }
          >
            {data.status.name}
          </Badge>
        </TextLeft>

      </TextGroup>
    </Container>
  )
}

MilkDeliveryItem.propTypes = {
  data: PropTypes.object,
  onEdit: PropTypes.func,
  onDelete: PropTypes.func
}

MilkDeliveryItem.defaultProps = {
  data: {},
  onEdit: () => {},
  onDelete: () => {}
}

export default MilkDeliveryItem
