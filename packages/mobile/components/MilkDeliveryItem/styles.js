import { Paragraph, Caption, Subheading } from 'react-native-paper'

import styled from 'styled-components'

export const Container = styled.View`
  display: flex;
  flex-direction: row;
`

export const TextGroup = styled.View`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  width: 100%;
`

export const TextLeft = styled.View`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
`

export const Actions = styled.View`
  display: flex;
  flex-direction: row;
  justify-content: flex-end;
  margin-bottom: 10px;
`

export const Title = styled(Subheading)`
`

export const TitleBold = styled(Title)`
  font-weight: bold;
`

export const Text = styled(Paragraph)`
`

export const Date = styled(Caption)`
  text-align: right;
  margin-bottom: 5px;
`
