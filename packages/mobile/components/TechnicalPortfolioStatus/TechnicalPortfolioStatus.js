import React, { useMemo } from 'react'

import PropTypes from 'prop-types'

import I18n from '@smartcoop/i18n'
import { TechnicalPortfolioStatusCode } from '@smartcoop/utils/constants'

import { Badge, Text } from './styles'

const TechnicalPortfolioStatus = (props) => {
  const { statusNumber, style } = props

  const options = useMemo(() => {
    switch (statusNumber) {
      case TechnicalPortfolioStatusCode.AGUARDANDO_RESPOSTA:
        return {
          name: (
            <I18n
              as={ (nameProps) => <Text { ...nameProps } color="(245, 125, 13)" /> }
            >
              waiting answer
            </I18n>
          ),
          background: '(245, 125, 13, 0.1)'
        }

      case TechnicalPortfolioStatusCode.NEGADO:
        return {
          name: (
            <I18n
              as={ (nameProps) => <Text { ...nameProps } color="(216, 27, 96)" /> }
            >
              denied
            </I18n>
          ),
          background: '(216, 27, 96, 0.1)'
        }

      case TechnicalPortfolioStatusCode.REVOGADO:
        return {
          name: (
            <I18n
              as={ (nameProps) => <Text { ...nameProps } color="(216, 27, 96)" /> }
            >
              revoked
            </I18n>
          ),
          background: '(216, 27, 96, 0.1)'
        }

      case TechnicalPortfolioStatusCode.SOMENTE_VISUALIZAÇÃO:
        return {
          name: (
            <I18n
              as={ (nameProps) => <Text { ...nameProps } color="(40, 159, 48)" /> }
            >
              view only
            </I18n>
          ),
          background: '(40, 159, 48, 0.1)'
        }

      case TechnicalPortfolioStatusCode.VISUALIZAÇÃO_EDIÇÃO:
        return {
          name: (
            <I18n
              as={ (nameProps) => <Text { ...nameProps } color="(40, 159, 48)" /> }
            >
              viewing and editing
            </I18n>
          ),
          background: '(40, 159, 48, 0.1)'
        }

      default:
        return {
          name: (
            <I18n
              as={ (nameProps) => <Text { ...nameProps } color="(89, 89, 89)" /> }
            >
              not requested
            </I18n>
          ),
          background: '(89, 89, 89, 0.1)'
        }
    }
  }, [statusNumber])

  return (
    <Badge color={ options.background } style={ style }>
      {options.name}
    </Badge>
  )
}

TechnicalPortfolioStatus.propTypes = {
  statusNumber: PropTypes.number,
  style: PropTypes.object
}

TechnicalPortfolioStatus.defaultProps = {
  style: {},
  statusNumber: null
}

export default TechnicalPortfolioStatus
