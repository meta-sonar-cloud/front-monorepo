import styled from 'styled-components/native'

export const Badge = styled.View`
  background-color: rgba${ props => props.color };
  border-radius: 5px;
  padding: 10px;
  text-align: center;
`

export const Text = styled.Text`
  font-size: 13px;
  font-weight: 600;
  font-family: "Open Sans";
  color: rgb${ props => props.color };
`
