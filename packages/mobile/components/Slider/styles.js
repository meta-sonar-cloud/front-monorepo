import { StyleSheet } from 'react-native'

import styled from 'styled-components/native'

import { colors } from '@smartcoop/styles'

export const aboveThumbStyles = StyleSheet.create({
  container: {
    alignItems: 'center',
    backgroundColor: colors.white,
    borderColor: colors.yellow,
    borderRadius: 50,
    borderWidth: 1,
    height: 50,
    justifyContent: 'center',
    left: -30 / 2,
    width: 50
  }
})

export const Label = styled.Text`
  font-weight: 600;
  margin-left: 8px;
  font-size: 16px;
  margin-bottom: 10px;
  color: ${ props => props.error ? colors.error : colors.black };
`
