import React, { useState, useMemo, useCallback } from 'react'
import { View, Text } from 'react-native'

import { Slider as SliderRN } from '@miblanchard/react-native-slider'
import PropTypes from 'prop-types'

import { colors } from '@smartcoop/styles'

import {
  aboveThumbStyles,
  Label
} from './styles'

const Slider = (props) => {
  const {
    value: externalValue,
    label: externalLabel,
    trackMarks,
    onChange,
    required,
    style,
    sliderProps
  } = props

  const [internalValue, setInternalValue] = useState(externalValue)
  const defaultStyles = {
    height: 40
  }

  const label = useMemo(
    () => `${ externalLabel }${ required ? ' *' : '' }`,
    [externalLabel, required]
  )

  const renderTrackMarkComponent = useCallback((index) => {
    const currentMarkValue = trackMarks[index]
    return <View>
      <Text style={ {
        marginTop: 40
      } }
      >
        {currentMarkValue}%
      </Text>
    </View>
  }, [trackMarks])

  const renderAboveThumbComponent = useCallback(
    () => (
      <View style={ aboveThumbStyles.container }>
        <Text>
          {internalValue}%
        </Text>
      </View>
    ),
    [internalValue]
  )

  return (
    <>
      <Label>{label}</Label>
      <SliderRN
        style={ {
          ...defaultStyles,
          ...style
        } }
        trackStyle={ {
          height: 10,
          borderRadius: 5,
          width: '100%'
        } }
        thumbStyle={ {
          border: 2,
          borderColor: colors.yellow
        } }
        value={ internalValue }
        maximumValue={ 100 }
        minimumValue={ 0 }
        step={ 1 }
        trackMarks={ trackMarks }
        renderTrackMarkComponent={ renderTrackMarkComponent	}
        minimumTrackTintColor={ colors.yellow }
        maximumTrackTintColor={ colors.grey }
        thumbTintColor={ colors.white }
        onValueChange={ value => setInternalValue(value[0]) }
        onSlidingComplete={ value => onChange(value[0]) }
        renderAboveThumbComponent={ renderAboveThumbComponent }
        { ...sliderProps }
      />
    </>

  )
}

Slider.propTypes = {
  value: PropTypes.number,
  label: PropTypes.string,
  onChange: PropTypes.func,
  style: PropTypes.object,
  className: PropTypes.string,
  sliderProps: PropTypes.object,
  required: PropTypes.bool,
  trackMarks: PropTypes.array
}

Slider.defaultProps = {
  trackMarks: [0, 100],
  value: null,
  label: '',
  onChange: () => {},
  style: {},
  className: '',
  sliderProps: {},
  required: false
}

export default Slider
