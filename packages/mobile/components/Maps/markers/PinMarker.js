import React, { useMemo } from 'react'
import { Marker } from 'react-native-maps'

import PropTypes from 'prop-types'

import isEmpty from 'lodash/isEmpty'
import isNil from 'lodash/isNil'

import Icon from '@smartcoop/mobile-components/Icon'

const PinMarker = ({ coordinate, size, customIcon, calloutAnchor, anchor, ...props }) => {
  const position = useMemo(
    () => {
      if (!isNil(coordinate?.latitude) && !isNil(coordinate?.longitude)) {
        return coordinate
      }
      return {}
    },
    [coordinate]
  )

  return !isEmpty(position) && (
    <Marker
      coordinate={ position }
      calloutAnchor={ calloutAnchor }
      anchor={ anchor }
      { ...props }
    >
      <Icon icon={ customIcon } size={ size } />
    </Marker>
  )
}

PinMarker.propTypes = {
  coordinate: PropTypes.shape({
    latitude: PropTypes.number,
    longitude: PropTypes.number
  }),
  size: PropTypes.number,
  calloutAnchor: PropTypes.shape({
    x: PropTypes.number,
    y: PropTypes.number
  }),
  anchor: PropTypes.shape({
    x: PropTypes.number,
    y: PropTypes.number
  }),
  customIcon: PropTypes.any.isRequired
}

PinMarker.defaultProps = {
  coordinate: {},
  anchor: {
    x: 0,
    y: 0
  },
  calloutAnchor: {
    x: 0,
    y: 0
  },
  size: 50
}

export default PinMarker
