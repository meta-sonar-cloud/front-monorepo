import React, { useState, useMemo, useEffect, useRef, useCallback } from 'react'
import { StyleSheet } from 'react-native'
import MapView from 'react-native-maps'

import Geolocation from '@react-native-community/geolocation'
import PropTypes from 'prop-types'

import isEmpty from 'lodash/isEmpty'

const styles = StyleSheet.create({
  map: {
    ...StyleSheet.absoluteFillObject
  }
})

const Maps = (props) =>  {
  const {
    region: externalRegion,
    overlayButton,
    autoUserPosition,
    mapType,
    ...rest
  } = props

  const mapRef = useRef(null)

  const defaultCoords = useMemo(
    () => ({
      latitude: -30.0,
      longitude: -51.24,
      latitudeDelta: 0.006,
      longitudeDelta: 0.006
    }),
    []
  )

  const [region, setRegion] = useState({
    ...defaultCoords,
    ...externalRegion
  })


  const onChangeRegion = useCallback(
    (newRegion) => {
      setRegion(old => ({ ...old, ...newRegion }))
    },
    []
  )


  useEffect(() => {
    if (isEmpty(externalRegion) || autoUserPosition) {
      Geolocation.getCurrentPosition(
        location => onChangeRegion(location.coords),
        // eslint-disable-next-line no-console
        error => console.error(error),
        { enableHighAccuracy: true }
      )
    }
  }, [autoUserPosition, externalRegion, onChangeRegion])

  useEffect(() => {
    onChangeRegion(externalRegion)
  }, [onChangeRegion, externalRegion])

  useEffect(
    () => {
      mapRef.current.animateToRegion(region, 300)
    },
    [region]
  )

  return !isEmpty(region) && (
    <>
      <MapView
        ref={ mapRef }
        style={ styles.map }
        initialRegion={ region }
        mapType={ mapType }
        { ...rest }
      />
      {overlayButton}
    </>
  )
}


Maps.propTypes = {
  region: PropTypes.shape({
    latitude: PropTypes.number,
    longitude: PropTypes.number,
    latitudeDelta: PropTypes.number,
    longitudeDelta: PropTypes.number
  }),
  overlayButton: PropTypes.element,
  autoUserPosition: PropTypes.bool,
  mapType: PropTypes.string
}

Maps.defaultProps = {
  region: {},
  overlayButton: null,
  autoUserPosition: false,
  mapType: 'satellite'
}

export default Maps
