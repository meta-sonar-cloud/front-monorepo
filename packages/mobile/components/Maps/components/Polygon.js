import React, { useMemo, useCallback, useEffect, forwardRef, useImperativeHandle } from 'react'
import { Platform } from 'react-native'
import { Polygon as RNPolygon, Marker, Callout } from 'react-native-maps'

import hexToRgba from 'hex-to-rgba'
import PropTypes from 'prop-types'

import filter from 'lodash/filter'
import isEmpty from 'lodash/isEmpty'
import map from 'lodash/map'

import I18n from '@smartcoop/i18n'
import { polygonEdge } from '@smartcoop/icons'
import Icon from '@smartcoop/mobile-components/Icon'
import colors from '@smartcoop/styles/colors'
import { getPolygonArea } from '@smartcoop/utils/maps'

import { Tooltip, TooltipChild } from './styles'

const Polygon = forwardRef((props, polygonRef) => {
  const {
    points,
    color,
    onChange,
    onChangeArea,
    ...rest
  } = props


  const coordinates = useMemo(
    () => map(points, ([latitude, longitude]) => ({
      latitude,
      longitude
    })),
    [points]
  )

  const anchor = Platform.OS === 'ios' ? 1 : 0.5

  const onDragendPolygonMarker = useCallback(
    (limitIndex, event) => {
      const newLimitsPoints = map(points, (limit, index) => {
        if (limitIndex === index) {
          const { latitude, longitude } = event.nativeEvent.coordinate
          return [latitude, longitude]
        }
        return limit
      })
      onChange(newLimitsPoints)
    },
    [points, onChange]
  )

  const onRemoveLimitMarker = useCallback((limitIndex) => {
    const newLimitsPoints = filter(
      points,
      (limit, index) => limitIndex !== index
    )
    onChange(newLimitsPoints)
  }, [points, onChange])

  const sortNewPoint = useCallback(
    (newPoint) => [...points, newPoint],
    [points]
  )

  useEffect(
    () => onChangeArea(getPolygonArea(points).toFixed(2)),
    [onChangeArea, points]
  )

  useImperativeHandle(polygonRef, () => ({
    sortNewPoint
  }))

  return !isEmpty(coordinates) && (
    <>
      <RNPolygon
        coordinates={ coordinates }
        strokeColor={ color }
        strokeWidth={ 2 }
        fillColor={ hexToRgba(color, 0.2) }
        { ...rest }
      />

      {onChange && map(coordinates, ({ latitude, longitude }, index) => (
        <Marker
          key={ `${ latitude }_${ longitude }` }
          coordinate={ { latitude, longitude } }
          draggable
          onDragEnd={ (event) => onDragendPolygonMarker(index, event) }
          anchor={ { x: anchor, y: anchor } }
        >
          <>
            <Icon icon={ polygonEdge } size={ 24 } color={ color } />
            <Callout tooltip onPress={ () => onRemoveLimitMarker(index) }>
              <Tooltip>
                <TooltipChild>
                  <I18n>remove</I18n>
                </TooltipChild>
              </Tooltip>
            </Callout>
          </>
        </Marker>
      ))}
    </>
  )
})

Polygon.propTypes = {
  points: PropTypes.arrayOf(PropTypes.arrayOf(PropTypes.number)),
  color: PropTypes.string,
  onChange: PropTypes.func,
  onChangeArea: PropTypes.func
}

Polygon.defaultProps = {
  points: [],
  color: colors.secondary,
  onChange: undefined,
  onChangeArea: () => {}
}

export default Polygon
