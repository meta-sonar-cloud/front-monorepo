import styled from 'styled-components/native'

import colors from '@smartcoop/styles/colors'

export const Tooltip = styled.View`
  padding: 10px;
  background-color: ${ colors.white };
  width: 150px;
  border-radius: 10px;
`

export const TooltipChild = styled.View`
  background-color: ${ colors.secondary };
  color: ${ colors.text };
  justify-content: center;
  align-items: center;
  align-content: center;
  border-radius: 5px;
  min-height: 25px;
  padding: 10px;
`
