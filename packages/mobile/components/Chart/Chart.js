import React, { useMemo } from 'react'
import { View } from 'react-native'

import HighchartsReactNative from '@highcharts/highcharts-react-native'
import PropTypes from 'prop-types'

import isEmpty from 'lodash/isEmpty'

import colors from '@smartcoop/styles/colors'

const Chart = (props) => {
  const {
    options: { data, ...externalOptions },
    ...rest
  } = props

  const modules = useMemo(
    () => ['highcharts-more', 'dumbbell', 'lollipop'],
    []
  )

  const options = useMemo(
    () => ({
      ...externalOptions,
      credits: { enabled: false },
      legend: {
        align: 'left',
        verticalAlign: 'top',
        ...(externalOptions.legend || {})
      },
      tooltip: {
        positioner () {
          // eslint-disable-next-line react/no-this-in-sfc
          return this.chart.plotBox
        },
        shape: 'square',
        crosshairs: true,
        shared: true,
        useHTML: true,
        followTouchMove: true,
        borderColor: colors.transparent,
        backgroundColor: colors.transparent,
        shadow: false,
        style: {
          fontSize: 14,
          zIndex: 9999
        },
        formatter(tooltip) {
          return `
            <style>
              .highcharts-tooltip>span {
                background-color: #fff;
                border: 1px solid #FFC80A;
                border-radius: 3px;
                padding: 5px 10px;
                box-shadow: 0 1px 3px 0 rgba(0,0,0,0.14);
              }
            </style>
            ${ tooltip.defaultFormatter.call(this, tooltip).join('') }
          `
        },
        ...(externalOptions.tooltip || {})
      }
    }),
    [externalOptions]
  )

  const dataStr = useMemo(
    () => data ? JSON.stringify(data) : undefined,
    [data]
  )

  return !isEmpty(externalOptions) && (
    <View style={ { flex: 1 } }>
      <HighchartsReactNative
        { ...rest }
        modules={ modules }
        options={ options }
        useCDN={ false }
        styles={ {
          flex: 1,
          ...(rest.style || {})
        } }
        data={ dataStr }
        loader
      />
    </View>
  )
}

Chart.propTypes = {
  options: PropTypes.object
}

Chart.defaultProps = {
  options: {}
}

export default Chart
