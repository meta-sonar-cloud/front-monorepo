import styled from 'styled-components/native'

import colors from '@smartcoop/styles/colors'

export const Container = styled.View`
  flex-direction: row;
  align-items: center;
`

export const Image = styled.Image`
	width: ${ ({ size }) => size }px;
	height: ${ ({ size }) => size }px;
	background: ${ colors.secondary };
	border-radius: ${ ({ size }) => size/2 }px;
	margin: 0 10px;
`

export const TextContainer = styled.View`
`

export const Title = styled.Text`
  font-family: Montserrat-Bold;
  font-size: 16px;
  flex-wrap: wrap;
  max-width: 85%;
`

export const SubTitle = styled.Text`
  font-size: 14px;
`
