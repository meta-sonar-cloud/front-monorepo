import React from 'react'

import PropTypes from 'prop-types'

import startsWith from 'lodash/startsWith'

import avatarImg from '@smartcoop/styles/assets/images/avatar.png'

import { Container, TextContainer, Image, Title, SubTitle } from './styles'

const Avatar = ({ title, subTitle, source, ...props }) => (
  <Container>
    <Image { ...props } source={ startsWith(source, 'http') ? { uri: source } : source } />
    {(title || subTitle) && (
      <TextContainer>
        <Title>{title}</Title>
        <SubTitle>{subTitle}</SubTitle>
      </TextContainer>
    )}
  </Container>
)

Avatar.propTypes = {
  size: PropTypes.number,
  title: PropTypes.string,
  subTitle: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.element
  ]),
  source: PropTypes.any
}

Avatar.defaultProps = {
  title: null,
  subTitle: null,
  size: 44,
  source: avatarImg
}

export default Avatar
