import React from 'react'

import codeMask from '@smartcoop/forms/masks/code.mask'
import InputNumber from '@smartcoop/mobile-components/InputNumber'

const InputCode = props => (
  <InputNumber
    { ...props }
    setMask={ codeMask }
    maxLength={ 12 }
  />
)

export default InputCode
