import React from 'react'

import PropTypes from 'prop-types'

import { BadgeContainer, BadgeText } from './styles'

const Badge = (props) => {
  const { children, color, textStyle, ...rest } = props
  return (
    <BadgeContainer { ...rest } color={ color }>
      <BadgeText color={ color } style={ textStyle }>
        {children}
      </BadgeText>
    </BadgeContainer>
  )
}

Badge.propTypes = {
  children: PropTypes.any.isRequired,
  color: PropTypes.string.isRequired,
  textStyle: PropTypes.object
}

Badge.defaultProps = {
  textStyle: {}
}

export default Badge
