import styled from 'styled-components/native'

import { hexToRgba } from '@smartcoop/styles'

export const BadgeContainer = styled.View`
  background-color: ${ ({ color }) => hexToRgba(color, .1) };
  border-radius: 5px;
  padding: 5px 10px;
`

export const BadgeText = styled.Text`
  text-align: center;
  font-size: 12px;
  font-weight: bold;
  color: ${ ({ color }) => color };
`
