import React, { useRef } from 'react'
import { View } from 'react-native'

import PropTypes from 'prop-types'

import postSchema from '@smartcoop/forms/schemas/social/post.schema'
import { useT } from '@smartcoop/i18n'
import { publicType } from '@smartcoop/icons'
import Avatar from '@smartcoop/mobile-components/Avatar'
import Button from '@smartcoop/mobile-components/Button'
import Divider from '@smartcoop/mobile-components/Divider'
import Icon from '@smartcoop/mobile-components/Icon'
import InputSelect from '@smartcoop/mobile-components/InputSelect'
import InputText from '@smartcoop/mobile-components/InputText'
import { getInterestAreas } from '@smartcoop/services/apis/smartcoopApi/resources/interestAreas'
import colors from '@smartcoop/styles/colors'

import { Container, ColFooter1, ColFooter2, Header, Identifier, Body, AvatarSubtitle, PrivacyTitle } from './styles'

const Post = (props) =>  {
  const {
    style,
    user,
    onSubmit,
    textFieldProps,
    buttonProps
  } = props

  const t = useT()
  const formRef = useRef(null)

  return (
    <Container
      style={ style }
      ref={ formRef }
      schemaConstructor={ postSchema }
      onSubmit={ onSubmit }
    >
      <Header>
        <Identifier>
          <Avatar
            source={ user.image }
            title={ user.name }
            subTitle={ (
              <AvatarSubtitle>
                <Icon icon={ publicType } size={ 16 } color={ colors.green } />
                <PrivacyTitle>{t('public')}</PrivacyTitle>
              </AvatarSubtitle>
            ) }
          />
        </Identifier>
      </Header>
      <Body>
        <InputText
          multiline
          placeholder={ t('what would you like to share with the network?') }
          name="text"
          { ...textFieldProps }
        />
      </Body>
      <Divider />
      <ColFooter1>
        <InputSelect
          multiple
          label={ t('add tags') }
          name="interestAreas"
          options={ getInterestAreas }
          style={ { marginBottom: 4 } }
        />
      </ColFooter1>
      <Divider />
      <ColFooter2>
        <View style={ { paddingBottom: 30 } }>
          <Button
            color={ colors.primary }
            backgroundColor={ colors.secondary }
            title={ t('publish') }
            onPress={ () => formRef.current.submit() }
            { ...buttonProps }
          />
        </View>
      </ColFooter2>
    </Container>
  )}

Post.propTypes = {
  style: PropTypes.any,
  user: PropTypes.shape({
    name: PropTypes.string,
    image: PropTypes.string }),
  onSubmit: PropTypes.func,
  textFieldProps: PropTypes.object,
  buttonProps: PropTypes.object
}

Post.defaultProps = {
  style: {},
  user: {
    name: '',
    image: ''
  },
  onSubmit: () => {},
  textFieldProps: {},
  buttonProps: {}
}

export default Post
