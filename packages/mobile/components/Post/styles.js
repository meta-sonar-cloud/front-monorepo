import styled from 'styled-components/native'

import Form from '@smartcoop/mobile-components/Form'
import colors from '@smartcoop/styles/colors'

export const Container = styled(Form)`
  background: ${ colors.white };
  box-shadow: 0px 0px 6px ${ colors.muted };
  border-radius: 10px;
  padding-bottom: 20px;
`

export const Header = styled.View`
  padding-top: 14px;
  padding-left: 18px;
  padding-right: 18px;
`

export const Identifier = styled.View`
  margin-bottom: 10px;
`

export const Col = styled.View`
  padding-left: 12px;
`

export const Privacy = styled.View`
  display: flex;
  align-items: center;
`

export const Type = styled.Text`
  margin-left: 4px;
  font-size: 14px;
  font-weight: bold;
  color: ${ colors.green };
`

export const Body = styled.View`
  padding-left: 18px;
  padding-right: 18px;
`

export const ColFooter1 = styled.View`
  margin-top: 6px;
  padding: 0 20px;
`

export const ColFooter2 = styled.View`
  padding: 6px 20px;
  justify-content: center;
  align-items: flex-end;
`

export const AvatarSubtitle = styled.View`
  flex-direction: row;
  align-items: center;
  padding-top: 5px;
`

export const PrivacyTitle = styled.Text`
  font-size: 13px;
  color: ${ colors.green };
  font-family: OpenSans-Light;
  padding-left: 5px;
`
