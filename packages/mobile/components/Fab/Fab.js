import { FAB } from 'react-native-paper'

import PropTypes from 'prop-types'
import styled from 'styled-components/native'

import colors from '@smartcoop/styles/colors'

const Fab = styled(FAB)`
  position: absolute;
  margin: 16px;
  right: 0;
  bottom: 0;
  z-index: 10;
  background-color: ${ colors.secondary };
`

Fab.propTypes = {
  icon: PropTypes.string,
  onPress: PropTypes.func,
  color: PropTypes.string
}

Fab.defaultProps = {
  icon: 'plus',
  onPress: () => {},
  color: colors.primary
}

export default Fab
