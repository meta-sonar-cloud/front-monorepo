import styled from 'styled-components/native'

import colors from '@smartcoop/styles/colors'

export const Container = styled.View`
  border-radius: 5px;
  line-height: 0;
  width: 98px;
  border-color: ${ colors.lightGrey };
  border-width: 0.7px;
  flex-direction: row;
  align-items: center;
`

export const IconButton = styled.View`
  border: 0;
  margin: 0;
  height: 100%;
  justify-content: center;
  align-items: center;
  padding: 0;
  box-shadow: none;
  background-color: ${ ({ disabled }) => disabled ? colors.muted : colors.transparent };
`

export const ValueContainer = styled.View`
  width: 35%;
  height: 100%;
  text-align: center;
  font-size: 18px;
  border-color: ${ colors.lightGrey };
  border-left-width: 0.7px;
  border-right-width: 0.7px;
  background-color: ${ ({ disabled }) => disabled ? colors.muted : colors.white };
  justify-content: center;
  align-items: center;
`

export const Value = styled.Text`
  font-size: 18px;
`
