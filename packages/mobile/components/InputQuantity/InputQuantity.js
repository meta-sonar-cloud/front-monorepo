import React, { useState, useEffect, useMemo, useCallback } from 'react'
import { TouchableOpacity } from 'react-native'

import PropTypes from 'prop-types'


import { trash, plus, minus } from '@smartcoop/icons'
import Icon from '@smartcoop/mobile-components/Icon'
import colors from '@smartcoop/styles/colors'

import {
  Container,
  IconButton,
  ValueContainer,
  Value
} from './styles'

const InputQuantity = (props) => {
  const {
    min,
    max,
    value,
    onChange,
    disabled,
    deletable,
    onDelete,
    forceDelete,
    leftIconColor,
    rightIconColor,
    readOnly,
    style
  } = props

  const customValue = useMemo(
    () => ((value < min) ? min : value) || min,
    [min, value]
  )

  const incrementDisabled = useMemo(
    () => customValue >= max,
    [customValue, max]
  )

  const decreaseDisabled = useMemo(
    () => customValue <= min,
    [customValue, min]
  )

  const [inputValue, setInputValue] = useState(customValue)

  const onIncrement = useCallback(
    () => {
      if (!incrementDisabled) {
        onChange(customValue + 1)
      }
    },
    [customValue, incrementDisabled, onChange]
  )

  const onDecrease = useCallback(
    () => {
      if (!decreaseDisabled) {
        onChange(customValue - 1)
      }
    },
    [customValue, decreaseDisabled, onChange]
  )

  useEffect(() => {
    if (inputValue !== customValue) {
      setInputValue(customValue)
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [customValue])

  return (
    <Container style={ { ...style } }>
      {(deletable && inputValue <= min) || forceDelete ? (
        <TouchableOpacity
          onPress={ onDelete }
          style={ { width: '30%', borderTopRightRadius: 0, borderBottomRightRadius: 0 } }
          disabled={ readOnly || disabled }
        >
          <IconButton
            onPress={ onDelete }
            variant="text"
            style={ { borderTopRightRadius: 0, borderBottomRightRadius: 0 } }
            disabled={ readOnly || disabled }
          >
            <Icon
              icon={ trash }
              color={ leftIconColor }
              size={ 18 }
            />
          </IconButton>
        </TouchableOpacity>
      ) : (
        <TouchableOpacity
          onPress={ onDecrease }
          style={ {  width: '30%',  borderTopRightRadius: 0, borderBottomRightRadius: 0 } }
          disabled={ disabled || decreaseDisabled }
        >
          <IconButton
            variant="text"
            disabled={ disabled || decreaseDisabled }
          >
            <Icon
              icon={ minus }
              color={ leftIconColor }
              size={ 18 }
            />
          </IconButton>
        </TouchableOpacity>

      )}
      <ValueContainer>
        <Value>{inputValue}</Value>
      </ValueContainer>
      <TouchableOpacity
        onPress={ onIncrement }
        style={ { width: '30%', borderTopLeftRadius: 0, borderBottomLeftRadius: 0 } }
        disabled={ disabled || incrementDisabled }
      >
        <IconButton
          variant="text"
        >
          <Icon
            icon={ plus }
            color={ rightIconColor }
            size={ 18 }
          />
        </IconButton>
      </TouchableOpacity>
    </Container>
  )
}

InputQuantity.propTypes = {
  min: PropTypes.number,
  max: PropTypes.number,
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  onChange: PropTypes.func,
  disabled: PropTypes.bool,
  readOnly: PropTypes.bool,
  deletable: PropTypes.bool,
  onDelete: PropTypes.func,
  forceDelete: PropTypes.bool,
  leftIconColor: PropTypes.string,
  rightIconColor: PropTypes.string,
  style: PropTypes.object
}

InputQuantity.defaultProps = {
  min: 1,
  max: null,
  value: null,
  onChange: () => {},
  disabled: false,
  readOnly: false,
  deletable: false,
  onDelete: () => {},
  forceDelete: false,
  leftIconColor: colors.grey,
  rightIconColor: colors.secondary,
  style: {}
}

export default InputQuantity
