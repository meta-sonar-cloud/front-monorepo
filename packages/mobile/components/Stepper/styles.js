import styled from 'styled-components/native'

import { colors } from '@smartcoop/styles'


export const Step = styled.View`
  width: 100%;
  min-height: 10px;
  margin: 10px;
  background: ${ props => props.active ? colors.yellow : colors.lightYellow };
  flex: 1;
  border-radius: 5px;
`

export const Container = styled.View`
  width: 100%;
  flex-direction: row;
  flex-wrap: wrap;
`
