import React from 'react'

import PropTypes from 'prop-types'

import {
  Container,
  Step
} from './styles'

const Stepper = props => {
  const {
    step,
    steps
  } = props

  return (
    <Container>
      {
        Array.from({ length: steps }, (_, index) => <Step key={ index } active={ (step - 1) >= index }/>)
      }
    </Container>
  )
}

Stepper.propTypes = {
  step: PropTypes.number.isRequired,
  steps: PropTypes.number.isRequired
}

export default Stepper
