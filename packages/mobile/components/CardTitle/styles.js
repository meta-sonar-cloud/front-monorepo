import { Subheading } from 'react-native-paper'

import styled from 'styled-components/native'

export const TitleContainer = styled.View`
  width: 100%;
  padding: 5px 10px 0px 20px;
  flex-direction: row;
  align-items: center;
`

export const FooterContainer = styled.View`
  width: 100%;
  padding: 5px 10px 15px;
  flex-direction: row;
  align-items: center;
`

export const Title = styled(Subheading)`
  font-family: 'Montserrat';
  font-weight: bold;
  letter-spacing: 0;
  margin: 0;
  flex: 1;
`

export const ContentContainer = styled.View`
  width: 100%;
  min-height: 60px;
`

export const TitleLeftContainer = styled.View`
  margin-right: 10px;
`

export const TitleRightContainer = styled.View`
  margin-left: 10px;
`

