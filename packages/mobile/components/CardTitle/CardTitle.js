import React from 'react'
import { View } from 'react-native'

import PropTypes from 'prop-types'

import Card from '@smartcoop/mobile-components/Card'
import Divider from '@smartcoop/mobile-components/Divider'

import {
  TitleContainer,
  FooterContainer,
  ContentContainer,
  Title,
  TitleLeftContainer,
  TitleRightContainer
} from './styles'

const CardTitle = (props) => {
  const {
    title,
    children,
    childrenStyle,
    titleLeft,
    titleRight,
    hideDivider,
    headerStyle,
    footer,
    ...rest
  } = props

  return (
    <Card { ...rest }>
      <TitleContainer style={ headerStyle }>
        {titleLeft && (
          <TitleLeftContainer>
            {titleLeft}
          </TitleLeftContainer>
        )}
        <Title>
          {title}
        </Title>
        {titleRight && (
          <TitleRightContainer>
            {titleRight}
          </TitleRightContainer>
        )}
      </TitleContainer>
      {!hideDivider && <Divider />}
      <ContentContainer style={ childrenStyle }>
        <View>
          {children}
        </View>
      </ContentContainer>
      {footer && (
        <>
          <Divider />
          <FooterContainer>
            {footer}
          </FooterContainer>
        </>
      )}
    </Card>
  )
}

CardTitle.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.arrayOf(PropTypes.element)
  ]),
  titleLeft: PropTypes.element,
  titleRight: PropTypes.element,
  title: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.string
  ]),
  childrenStyle: PropTypes.object,
  headerStyle: PropTypes.object,
  hideDivider: PropTypes.bool,
  footer: PropTypes.element
}

CardTitle.defaultProps = {
  children: null,
  titleLeft: null,
  titleRight: null,
  title: null,
  childrenStyle: {},
  headerStyle: {},
  hideDivider: false,
  footer: null
}

export { Title }

export default CardTitle
