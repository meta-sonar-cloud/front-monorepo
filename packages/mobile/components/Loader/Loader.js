import React from 'react'

import LottieView from 'lottie-react-native'
import PropTypes from 'prop-types'

import Spinner from '@smartcoop/styles/assets/animations/spinner.json'

import {
  Container,
  Message
} from './styles'

const lottieStyle = {
  height: 'auto',
  alignSelf: 'center'
}

const Loader = ({ message, width }) => (
  <Container>
    <LottieView
      source={ Spinner }
      autoPlay
      loop
      style={ { ...lottieStyle, width } }
    />
    {message && <Message>{message}</Message>}
  </Container>
)

Loader.propTypes = {
  message: PropTypes.string,
  width: PropTypes.number
}

Loader.defaultProps = {
  message: null,
  width: 200
}

export default Loader
