import styled from 'styled-components/native'

export const Container = styled.View`
  width: 100%;
  justify-content: center;
  align-items: center;
  padding: 20px;
  background-color: transparent;
`

export const Message = styled.Text`
  padding-top: 30px;
  color: #000;
  font-weight: bold;
  font-size: 16px;
  background-color: transparent;
`
