import React from 'react'
import renderer from 'react-test-renderer'

import Loader from '../Loader'

describe('Loading page', () => {
  it('renders correctly', () => {
    const tree = renderer
      .create(<Loader message="Validando seus dados..." />)
      .toJSON()
    expect(tree).toMatchSnapshot()
  })
})
