import React from 'react'
import { Text } from 'react-native'

import moment from 'moment'
import PropTypes from 'prop-types'

import { pencil } from '@smartcoop/icons'
import Button from '@smartcoop/mobile-components/Button'
import Icon from '@smartcoop/mobile-components/Icon'
import { colors } from '@smartcoop/styles'
import { momentBackDateFormat, momentFriendlyDateFormat } from '@smartcoop/utils/dates'

import {
  Container,
  Row,
  Label
} from './styles'

const AnimalPregnancyActionsItem = ({ animalPregnancy, onEdit }) => (
  <Container>
    <Row>
      <Label>
        {animalPregnancy?.animal?.earring?.earringCode}
      </Label>
      <Label>
        {moment(animalPregnancy?.accomplishedDate, momentBackDateFormat).format(momentFriendlyDateFormat)}
      </Label>
    </Row>
    <Row>
      <Text>{animalPregnancy?.type}</Text>
      <Button
        variant="outlined"
        style={ { borderColor: colors.lightGrey, minWidth: 0, minHeight: 0 } }
        color={ colors.black }
        icon={ <Icon size={ 16 } icon={ pencil } color={ colors.black } /> }
        onPress={ onEdit }
      />
    </Row>
  </Container>
)

AnimalPregnancyActionsItem.propTypes = {
  animalPregnancy: PropTypes.object,
  onEdit: PropTypes.func
}

AnimalPregnancyActionsItem.defaultProps = {
  animalPregnancy: {},
  onEdit: () => {}
}

export default AnimalPregnancyActionsItem
