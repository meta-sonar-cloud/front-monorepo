import React from 'react'
import { Text } from 'react-native'

import PropTypes from 'prop-types'

import { isEmpty } from 'lodash'

import I18n from '@smartcoop/i18n'
import { trash, pencil } from '@smartcoop/icons'
import Button from '@smartcoop/mobile-components/Button'
import Icon from '@smartcoop/mobile-components/Icon'
import { colors } from '@smartcoop/styles'

import {
  Container,
  Row,
  Label,
  Box
} from './styles'

const LotItem = ({ name, code, trinkets, defaultLot, onEdit, onDelete }) => (
  <Container>
    <Row>
      <Label>
        {name}
      </Label>
      <Label>
        {code}
      </Label>
    </Row>
    <Row>
      <Box>
        <I18n as={ Text }>trinkets</I18n>
        <Text>{isEmpty(trinkets) ? '-' : trinkets}</Text>
      </Box>
      <Row>
        <Button
          variant="outlined"
          style={ { borderColor: colors.lightGrey, marginRight: 5 } }
          color={ colors.black }
          icon={ <Icon size={ 16 } icon={ pencil } color={ defaultLot  ? colors.lightGrey : colors.black } /> }
          onPress={ onEdit }
          disabled={ defaultLot }
        />
        <Button
          variant="outlined"
          style={ { borderColor: colors.lightGrey } }
          color={ colors.black }
          icon={ <Icon size={ 16 } icon={ trash } color={ defaultLot  ? colors.lightGrey : colors.error } /> }
          onPress={ onDelete }
          disabled={ defaultLot }
        />
      </Row>
    </Row>
  </Container>
)

LotItem.propTypes = {
  code: PropTypes.string,
  trinkets: PropTypes.string,
  name: PropTypes.string,
  defaultLot: PropTypes.bool,
  onDelete: PropTypes.func,
  onEdit: PropTypes.func
}

LotItem.defaultProps = {
  code: '',
  trinkets: '',
  name: '',
  defaultLot: false,
  onDelete: () => {},
  onEdit: () => {}
}

export default LotItem
