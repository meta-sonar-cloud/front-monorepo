import { Subheading } from 'react-native-paper'

import styled from 'styled-components/native'

export const Container = styled.View`
  display: flex;
  padding: 10px;
`

export const Row = styled.View`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  padding: 5px 0;
`

export const Label = styled(Subheading)`
  font-weight: 700;
`

export const Box = styled.View`
  padding-top: 5px;
`
