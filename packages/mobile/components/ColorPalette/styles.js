
import { Caption } from 'react-native-paper'

import styled from 'styled-components/native'

import { colors } from '@smartcoop/styles'

export const Container = styled.View`
  width: 100%;
  margin-top: 10px;
`

export const ItemsGroup = styled.View`
  flex: 1;
  border-radius: 15px;
  flex-direction: row;
`

export const Item = styled.View`
  background-color: ${ ({ color }) => color };
  flex: 1;
  height: 10px;
`

export const DescriptionContainer = styled.View`
  flex-direction: row;
  justify-content: space-between;
`

export const Description = styled(Caption)`
  color: ${ colors.white };
`
