import React, { useMemo } from 'react'

import PropTypes from 'prop-types'

import map from 'lodash/map'

import  I18n from '@smartcoop/i18n'
import * as colorPalette from '@smartcoop/utils/colorPalette'

import {
  Container,
  ItemsGroup,
  Item,
  DescriptionContainer,
  Description
} from './styles'


const ColorPalette = ({ type }) => {
  const colors = useMemo(
    () => colorPalette[type] || [],
    [type]
  )

  return (
    <Container>
      <DescriptionContainer>
        <Description>{type === 'NDVI' ? '0,3' : '0,1'} - <I18n>low</I18n></Description>
        <Description>{type === 'NDVI' ? '0,9' : '0,5'} - <I18n>high</I18n></Description>
      </DescriptionContainer>
      <ItemsGroup>
        { map(colors, (item) => (
          <Item
            color={ item }
            key={ item }
          />
        )) }
      </ItemsGroup>
    </Container>
  )
}

ColorPalette.propTypes = {
  type: PropTypes.oneOf(['NDVI', 'NDRE']).isRequired
}

export default ColorPalette
