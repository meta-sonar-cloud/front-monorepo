import React from 'react'

import TextField from '@smartcoop/mobile-components/TextField'

const InputEmail = (props) => (
  <TextField
    { ...props }
    autoCompleteType="email"
    keyboardType="email-address"
    textContentType="emailAddress"
    autoCapitalize="none"
  />
)

export default InputEmail
