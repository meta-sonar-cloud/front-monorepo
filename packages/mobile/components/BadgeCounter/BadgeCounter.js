import React, { useMemo } from 'react'
import { Badge } from 'react-native-paper'

import PropTypes from 'prop-types'

const BadgeCounter = (props) => {
  const { children, max, ...rest } = props

  const counter = useMemo(
    () => {
      if (max && Number(children) > max) {
        return `+${ max }`
      }
      return children
    },
    [children, max]
  )

  return !!counter && (
    <Badge { ...rest }>
      {counter}
    </Badge>
  )
}

BadgeCounter.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number
  ]).isRequired,
  max: PropTypes.number,
  size: PropTypes.number
}

BadgeCounter.defaultProps = {
  max: 0,
  size: 20
}

export default BadgeCounter
