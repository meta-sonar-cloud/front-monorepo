import React, { useMemo } from 'react'

import PropTypes from 'prop-types'

import InputFloat from '@smartcoop/mobile-components/InputFloat'
import InputNumber from '@smartcoop/mobile-components/InputNumber'
import InputText from '@smartcoop/mobile-components/InputText'
import InputUnit from '@smartcoop/mobile-components/InputUnit'

const DynamicInput = (props) => {
  const {
    type,
    unit,
    ...rest
  } = props

  const InputType = useMemo(
    () => {
      switch (type) {
        case 'float':
          return InputFloat
        case 'integer':
          return InputNumber
        case 'number':
          return InputNumber
        default:
          return InputText
      }
    },
    [type]
  )

  return (
    <>
      { !unit ? (
        <InputType { ...rest } />
      ) : (
        <InputUnit unit={ unit } type={ type } { ...rest }/>
      ) }
    </>
  )
}

DynamicInput.propTypes = {
  type: PropTypes.string.isRequired,
  unit: PropTypes.string

}

DynamicInput.defaultProps = {
  unit: null
}

export default DynamicInput
