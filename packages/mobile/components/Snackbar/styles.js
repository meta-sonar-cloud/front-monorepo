import styled from 'styled-components/native'

import colors from '@smartcoop/styles/colors'

export const SnackbarContainer = styled.View`
  padding: 20px;
  flex-direction: row;
`

export const Title = styled.Text`
  color: ${ colors.white };
  font-size: 16px;
  flex-wrap: wrap;
`
