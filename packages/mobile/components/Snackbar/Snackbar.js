import React from 'react'
import { getStatusBarHeight } from 'react-native-iphone-x-helper'

import PropTypes from 'prop-types'
import RNSnackbar from 'rn-snackbar'

import { success, warningSnackbar, error } from '@smartcoop/icons'
import Icon from '@smartcoop/mobile-components/Icon'
import { SnackbarController, SnackbarContext } from '@smartcoop/snackbar'
import colors from '@smartcoop/styles/colors'

import { SnackbarContainer, Title } from './styles'

export const SnackbarsProvider = (props) => {
  const {
    children, options, anchorOrigin
  } = props

  const snackbarController = new SnackbarController(
    {
      openSnackbar: (message, snackOptions) => {
        let backgroundColor
        let icon
        switch(snackOptions.variant) {
          case 'info': {
            backgroundColor = colors.info
            break
          }
          case 'success': {
            backgroundColor = colors.green
            icon = success
            break
          }
          case 'warning': {
            backgroundColor = colors.warning
            icon = warningSnackbar
            break
          }
          case 'error': {
            backgroundColor = colors.error
            icon = error
            break
          }
          default:
            break
        }
        RNSnackbar.show(message, {
          style: {
            marginTop: getStatusBarHeight() + 80,
            marginLeft: 15,
            marginRight: 15,
            borderRadius: 7
          },
          backgroundColor,
          tapToClose: true,
          position: anchorOrigin.vertical,
          renderContent: () => (
            <SnackbarContainer>
              {icon && (
                <Icon
                  icon={ icon }
                  color={ colors.white }
                  style={ { marginRight: 10 } }
                />
              )}
              <Title>{message}</Title>
            </SnackbarContainer>
          )
        })
      },
      closeSnackbar: RNSnackbar.dismiss
    },
    {
      anchorOrigin,
      ...options
    }
  )

  return (
    <SnackbarContext.Provider value={ snackbarController }>
      {children}
    </SnackbarContext.Provider>
  )
}

SnackbarsProvider.propTypes = {
  /** snackbars position */
  anchorOrigin: PropTypes.shape({
    vertical: PropTypes.oneOf(['top', 'bottom'])
  }),
  /** default options to all snackbars */
  options: PropTypes.shape({
    persist: PropTypes.bool,
    autoHideDuration: PropTypes.number,
    variant: PropTypes.oneOf(['default', 'success', 'warning', 'error', 'info'])
  }),
  children: PropTypes.any
}

SnackbarsProvider.defaultProps = {
  options: {},
  anchorOrigin: {
    vertical: 'top',
    horizontal: 'right'
  },
  children: null
}
