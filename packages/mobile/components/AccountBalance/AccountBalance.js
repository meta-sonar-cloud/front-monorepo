import React from 'react'
import { TouchableOpacity } from 'react-native'

import moment from 'moment/moment'
import PropTypes from 'prop-types'

import isEmpty from 'lodash/isEmpty'

import I18n, { useT } from '@smartcoop/i18n'
import { wallet } from '@smartcoop/icons'
import Icon from '@smartcoop/mobile-components/Icon'
import colors from '@smartcoop/styles/colors'
import { momentBackDateTimeFormat, momentFriendlyDateFormat } from '@smartcoop/utils/dates'
import { formatCurrencyJs } from '@smartcoop/utils/formatters'

import {
  AccountTitle,
  ContentName,
  ContentBalance,
  HideBalance,
  TextBalance,
  Row,
  Column,
  CreditLimitLabel,
  CreditLimit
} from './styles'

const AccountBalance = ({
  hideBalance,
  accountName,
  balance,
  onPress,
  creditLimit
}) => {
  const t = useT()

  return (
    <TouchableOpacity onPress={ onPress }>
      <ContentName>
        <Icon size={ 18 } icon={ wallet } color={ colors.green }/>
        <AccountTitle>{accountName}</AccountTitle>
      </ContentName>
      <ContentBalance>
        {
          hideBalance
            ? <HideBalance/>
            : <TextBalance>{formatCurrencyJs(balance)}</TextBalance>
        }

        {!isEmpty(creditLimit) && (
          <Row style={ { marginLeft: 25 } }>
            <Column>
              <CreditLimitLabel><I18n>credit limit</I18n></CreditLimitLabel>
              <CreditLimit>{creditLimit.creditLimit ? formatCurrencyJs(creditLimit.creditLimit) : t('does not apply - short version')}</CreditLimit>
            </Column>
            <Column>
              <CreditLimitLabel><I18n>renovation</I18n></CreditLimitLabel>
              <CreditLimit>{
                creditLimit.renovationDate
                  ? moment(creditLimit.renovationDate, momentBackDateTimeFormat).format(momentFriendlyDateFormat)
                  : t('does not apply - short version')}
              </CreditLimit>
            </Column>
          </Row>
        )}
      </ContentBalance>
    </TouchableOpacity>
  )}

AccountBalance.propTypes = {
  hideBalance: PropTypes.bool,
  accountName: PropTypes.string,
  balance: PropTypes.number,
  onPress: PropTypes.func,
  creditLimit: PropTypes.shape({
    creditLimit: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    renovationDate: PropTypes.string
  })
}

AccountBalance.defaultProps = {
  hideBalance: false,
  accountName: null,
  balance: null,
  onPress: () => {},
  creditLimit: {}

}

export default AccountBalance
