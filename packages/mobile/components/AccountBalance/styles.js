import { Title } from 'react-native-paper'

import styled from 'styled-components/native'

import colors from '@smartcoop/styles/colors'


export const AccountTitle = styled(Title)`
  font-weight: 600;
  color: ${ colors.darkGrey };
  padding-left: 10px;
`

export const ContentName = styled.View`
  display: flex;
  flex-direction: row;
  align-items: center;
`

export const ContentBalance = styled.View`
  padding-left: 18px;
`

export const HideBalance = styled.View`
  background-color: ${ colors.grey };
  border-radius: 5px;
  margin-top: 6px;
  width: 166px;
  height: 25px;
`

export const TextBalance = styled(Title)`
  margin: 0;
  color: ${ colors.black };
  font-weight: 600;
  padding-left: 10px;
  padding-bottom: 3px;
  font-weight: bold;
`

export const Row = styled.View`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
`

export const Column = styled.View`
  display: flex;
  flex-direction: column;
  flex: 1;
`

export const CreditLimitLabel = styled.Text`
  color: ${ colors.darkGrey };
  font-size: 14px;
  margin: 0;
  margin-top: 10px;
`

export const CreditLimit = styled.Text`
  color: ${ colors.black };
  font-size: 16px;
  font-weight: bold;
  margin: 0;
`
