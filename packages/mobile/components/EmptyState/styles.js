import styled from 'styled-components/native'

export const Container = styled.View`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  flex: 1;
`
export const Content = styled.View`
  display: flex;
  flex-direction: column;
  margin-bottom: 10px;
`

export const Label = styled.Text`
  font-size: 16px;
  font-family: 'Montserrat';
  margin-top: 10px;
  margin-bottom: 10px;
`
