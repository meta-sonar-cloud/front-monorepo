import React from 'react'
import { TouchableWithoutFeedback } from 'react-native'

import PropTypes from 'prop-types'

import Button from '@smartcoop/mobile-components/Button'
import Icon from '@smartcoop/mobile-components/Icon'
import colors from '@smartcoop/styles/colors'

import { Container, Content, Label } from './styles'


function EmptyState({ style, contentStyle, text, icon, iconProps, action, buttonProps }) {
  return (
    <TouchableWithoutFeedback>
      <Container style={ style }>
        {icon && <Icon icon={ icon } { ...iconProps } />}
        <Content style={ contentStyle }>
          <Label>{text}</Label>
          {action.text !== '' &&
            <Button
              id="button"
              backgroundColor={ colors.secondary }
              color={ colors.primary }
              title={ action.text }
              onPress={ action.onClick }
              { ...buttonProps }
            />
          }
        </Content>
      </Container>
    </TouchableWithoutFeedback>
  )
}

EmptyState.propTypes = {
  style: PropTypes.object,
  contentStyle: PropTypes.object,
  text: PropTypes.string.isRequired,
  icon: PropTypes.func,
  iconProps: PropTypes.object,
  action: PropTypes.shape({
    text: PropTypes.string,
    onClick: PropTypes.func }),
  buttonProps: PropTypes.object
}

EmptyState.defaultProps = {
  style: {},
  contentStyle: {},
  icon: null,
  iconProps: {
    size: 176
  },
  action: {
    text: '',
    onClick: () => {}
  },
  buttonProps: {}
}

export default EmptyState
