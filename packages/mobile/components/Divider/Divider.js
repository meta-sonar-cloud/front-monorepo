import styled from 'styled-components/native'

import { colors } from '@smartcoop/styles'

const Divider = styled.View`
  display: flex;
  width: 100%;
  height: 1.5px;
  background-color: ${ colors.lightGrey };
  margin: 5px 0;
`

export default Divider
