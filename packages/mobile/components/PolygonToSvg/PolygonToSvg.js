import React, { useMemo } from 'react'
import { View } from 'react-native'
import { Svg, Path } from 'react-native-svg'

import PropTypes from 'prop-types'

import isEmpty from 'lodash/isEmpty'

import { lighten } from '@material-ui/core/styles/colorManipulator'

import colors from '@smartcoop/styles/colors'
import { polygonToSvg } from '@smartcoop/utils/maps'

const PolygonToSvg = (props) => {

  const { shape, color, width, style } = props

  const svgDeets = useMemo(
    () => !isEmpty(shape) ? polygonToSvg(shape) : {},
    [shape]
  )

  const realSize = useMemo(
    () => {
      let w = width
      let h = (width * svgDeets.height) / svgDeets.width

      if (h > width) {
        w = (w * width) / h
        h = width
      }

      return {
        width: w,
        height: h
      }
    },
    [width, svgDeets.height, svgDeets.width]
  )

  return (
    <View
      style={ {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        width,
        ...style
      } }
    >
      <Svg
        width={ realSize.width }
        height={ realSize.height }
        viewBox={ svgDeets.viewbox }
      >
        <Path
          strokeWidth={ 1 }
          stroke={ color }
          fill={ lighten(color, 0.85) }
          d={ svgDeets.path }
          vectorEffect="non-scaling-stroke"
        />
      </Svg>
    </View>
  )
}

PolygonToSvg.propTypes = {
  shape: PropTypes.arrayOf(PropTypes.shape({
    lat: PropTypes.number,
    lng: PropTypes.number
  })),
  color: PropTypes.string,
  width: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  style: PropTypes.object
}

PolygonToSvg.defaultProps = {
  shape: [],
  color: colors.orange,
  width: 100,
  style: {}
}

export default PolygonToSvg
