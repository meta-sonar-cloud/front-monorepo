import React, { useCallback, useMemo, useRef, useState } from 'react'
import { TouchableOpacity } from 'react-native'

import moment from 'moment/moment'
import PropTypes from 'prop-types'

import filter from 'lodash/filter'
import isEmpty from 'lodash/isEmpty'
import isEqual from 'lodash/isEqual'

import { useDialog } from '@smartcoop/dialog'
import { calendar } from '@smartcoop/icons'
import DatePickerModal from '@smartcoop/mobile-components/DatePicker/DatePickerModal'
import Icon from '@smartcoop/mobile-components/Icon'
import TextField from '@smartcoop/mobile-components/TextField'
import colors from '@smartcoop/styles/colors'
import { momentBackDateFormat, momentFriendlyDateFormat } from '@smartcoop/utils/dates'

const InputDateRange = (props) => {
  const {
    value,
    onChange,
    minDate,
    maxDate,
    defaultValue,
    landscape,
    ...rest
  } = props

  const { detached, disabled } = rest

  const { createDialog, removeDialog } = useDialog()

  const [calendarValue, setCalendarValue] = useState(value)

  const dates = useMemo(
    () => filter(detached ? value : calendarValue, (date) => !isEmpty(date)),
    [calendarValue, detached, value]
  )

  const datesObj = useMemo(
    () => {
      const [from = '', to = ''] = dates || []
      return { from, to }
    },
    [dates]
  )

  const inputRef = useRef(null)

  const transformRender = useCallback(
    (newDates) => {
      const newMomentFrom = moment(newDates.from, momentBackDateFormat, true)
      const fromStr = newMomentFrom.isValid() ? newMomentFrom.format(momentFriendlyDateFormat) : newMomentFrom._i

      const newMomentTo = moment(newDates.to, momentBackDateFormat, true)
      const toStr = newMomentTo.isValid() ? newMomentTo.format(momentFriendlyDateFormat) : newMomentTo._i

      if (!newDates.to || fromStr === toStr) {
        return fromStr
      }
      return `${ fromStr } - ${ toStr }`
    },
    []
  )

  const handleChangeDates = useCallback(
    (newDates) => {
      if (!detached && inputRef.current.setValue) {
        inputRef.current.setValue(newDates)
      }
      onChange(newDates)
      setCalendarValue(newDates)

      if (moment(newDates.to, momentBackDateFormat).isValid()) {
        removeDialog({ id: 'calendar-picker' })
      }
    },
    [detached, onChange, removeDialog]
  )

  // only use it outside a form
  const handleInputChange = useCallback(
    () => {},
    []
  )
  // only use it inside a form
  const handleFieldChange = useCallback(
    (newDates) => {
      if (!detached) {
        if (!isEqual(newDates, datesObj)) {
          handleChangeDates(newDates)
        } else {
          handleChangeDates({ from: '', to: '' })
        }
      }
    },
    [datesObj, detached, handleChangeDates]
  )

  const openCalendarModal = useCallback(
    () => {
      if (!disabled) {
        createDialog({
          id: 'calendar-picker',
          Component: DatePickerModal,
          props: {
            minDate,
            maxDate,
            value: datesObj,
            mode: 'range',
            onChange: handleChangeDates,
            landscape
          }
        })
      }
    },
    [createDialog, datesObj, disabled, handleChangeDates, landscape, maxDate, minDate]
  )

  const endAdornment = useMemo(
    () => (
      <TouchableOpacity
        onPress={ openCalendarModal }
      >
        <Icon
          icon={ calendar }
          size={ 22 }
          color={ colors.text }
        />
      </TouchableOpacity>
    ),
    [openCalendarModal]
  )

  if (detached) {
    rest.value = datesObj
    rest.onChange = handleInputChange
  } else {
    rest.defaultValue = defaultValue
    rest.onFieldValueChange = handleFieldChange
  }

  return (
    <TextField
      ref={ inputRef }
      transformRender={ transformRender }
      endAdornment={ endAdornment }
      onPress={ openCalendarModal }
      onFocus={ openCalendarModal }
      showSoftInputOnFocus={ false }
      { ...rest }
    />
  )
}

InputDateRange.propTypes = {
  value: PropTypes.shape({
    from: PropTypes.string,
    to: PropTypes.string
  }),
  defaultValue: PropTypes.shape({
    from: PropTypes.string,
    to: PropTypes.string
  }),
  onChange: PropTypes.func,
  minDate: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.object
  ]),
  maxDate: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.object
  ]),
  landscape: PropTypes.bool
}

InputDateRange.defaultProps = {
  value: {
    from: '',
    to: ''
  },
  defaultValue: {
    from: '',
    to: ''
  },
  onChange: () => {},
  minDate: undefined,
  maxDate: undefined,
  landscape: false
}

export default InputDateRange
