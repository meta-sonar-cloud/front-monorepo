import React from 'react'

import { useT } from '@meta-react/i18n'
import PropTypes from 'prop-types'

import { filter as filterIcon } from '@smartcoop/icons'
import { colors } from '@smartcoop/styles'

import Button from '../Button/Button'
import Icon from '../Icon/Icon'

const FilterButton = props => {
  const {
    isActive,
    onClick,
    ...otherProps
  } = props
  const t = useT()

  return (
    <Button
      id="filter-button"
      backgroundColor={ isActive ? colors.secondary : colors.white }
      color={ colors.text }
      style={ {
        marginRight: 10
      } }
      onClick={ onClick }
      title={ t('filtrate') }
      icon={
        <Icon icon={ filterIcon } size={ 14 } style={ { marginRight: 6 } } />
      }
      { ...otherProps }
    />
  )
}

FilterButton.propTypes = {
  isActive: PropTypes.bool,
  onClick: PropTypes.func
}

FilterButton.defaultProps = {
  isActive: false,
  onClick: () => {}
}


export default FilterButton
