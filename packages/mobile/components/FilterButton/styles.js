import styled from 'styled-components/native'

import Button from '@smartcoop/mobile-components/Button'

export const StyledButton = styled(Button)`
  flex: 1;
  width: ${ ({ fullWidth }) => fullWidth ? '100%' : 'auto' };
  text-transform: capitalize;
  font-size: 16px;
`
