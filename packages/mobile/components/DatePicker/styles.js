export const previousTitleStyle = {
  marginLeft: '12%',
  fontFamily: 'Montserrat-Bold'
}

export const nextTitleStyle = {
  marginRight: '12%',
  fontFamily: 'Montserrat-Bold'
}

export const dayLabelsWrapper = {
  justifyContent: 'space-between'
}
