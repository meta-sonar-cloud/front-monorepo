import React, { useState, useCallback } from 'react'

import moment from 'moment/moment'
import PropTypes from 'prop-types'

import debounce from 'lodash/debounce'
import isEqual from 'lodash/isEqual'

import { useT } from '@smartcoop/i18n'
import Modal from '@smartcoop/mobile-components/Modal'
import { momentBackDateFormat } from '@smartcoop/utils/dates'

import DatePicker from './DatePicker'


const DatePickerModal = props => {
  const {
    id,
    open,
    title,
    value: externalValue,
    onChange,
    landscape,
    ...rest
  } = props

  const t = useT()

  const [value, setValue] = useState(externalValue)

  const handleChangeDate = useCallback(
    debounce(
      (newDate) => {
        let date = newDate

        if (rest.mode === 'range') {
          const { from, to } = date
          const momentTo = moment(to, momentBackDateFormat)
          if (momentTo.isValid() && momentTo.isBefore(moment(from, momentBackDateFormat))) {
            date = { from: to, to: from }
          }
        }

        if (!isEqual(date, value)) {
          setValue(date)
          onChange(date)
        }
      },
      200,
      { leading: true, trailing: false }
    ),
    [onChange, value]
  )

  const handleChange = useCallback(
    (newDate) => {
      if (rest.mode === 'range') {
        handleChangeDate({
          from: (!value.from || value.to) ? moment(newDate).format(momentBackDateFormat) : value?.from,
          to: (!value.from || value.to) ? '' : moment(newDate).format(momentBackDateFormat)
        })
      } else {
        handleChangeDate(newDate)
      }
    },
    [handleChangeDate, rest.mode, value.from, value.to]
  )

  return (
    <Modal
      id={ id }
      open={ open }
      title={ title || t('select one interval') }
      landscape={ landscape }
    >
      <DatePicker { ...rest } value={ value } onChange={ handleChange } />
    </Modal>
  )
}

DatePickerModal.propTypes = {
  id: PropTypes.string.isRequired,
  open: PropTypes.bool.isRequired,
  value: PropTypes.any,
  onChange: PropTypes.func,
  title: PropTypes.string,
  landscape: PropTypes.bool
}

DatePickerModal.defaultProps = {
  title: null,
  value: '',
  onChange: () => {},
  landscape: false
}

export default DatePickerModal
