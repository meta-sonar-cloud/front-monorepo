import React, { useMemo } from 'react'
import CalendarPicker from 'react-native-calendar-picker'
import MonthPicker from 'react-native-month-picker'

import PropTypes from 'prop-types'

import { useT } from '@smartcoop/i18n'
import { colors } from '@smartcoop/styles'

import {
  previousTitleStyle,
  nextTitleStyle,
  dayLabelsWrapper
} from './styles'

const DatePicker = props => {
  const {
    value,
    minDate,
    maxDate,
    onChange,
    mode,
    ...rest
  } = props

  const t = useT()

  const fromDate = useMemo(
    () => mode === 'range' ? value.from : value,
    [mode, value]
  )

  const toDate = useMemo(
    () => mode === 'range' ? value.to : undefined,
    [mode, value.to]
  )

  const translations = useMemo(
    () => ({
      months: [
        t('january'),
        t('february'),
        t('march'),
        t('april'),
        t('may'),
        t('june'),
        t('july'),
        t('august'),
        t('september'),
        t('october'),
        t('november'),
        t('december')
      ],
      weekDays: [
        t('sunday short'),
        t('monday short'),
        t('tuesday short'),
        t('wednesday short'),
        t('thursday short'),
        t('friday short'),
        t('saturday short')
      ]
    }),
    [t]
  )

  if (mode === 'month') {
    return (
      <MonthPicker
        selectedDate={ value }
        onMonthChange={ onChange }
        minDate={ minDate }
        maxDate={ maxDate }
        localeLanguage='pt-br'
        nextText={ t('next') }
        prevText={ t('previous') }
        currentMonthTextStyle={ { color:  colors.secondary } }
        monthTextStyle={ {
          color:  colors.text,
          textTransform: 'capitalize'
        } }
        selectedBackgroundColor={ colors.secondary }
        selectedMonthTextStyle={ {
          color:  colors.text,
          fontWeight: 'bold'
        } }
        yearTextStyle={ {
          fontSize: 18
        } }
        containerStyle={ {
          width: '100%'
        } }
        { ...rest }
      />
    )
  }

  return (
    <CalendarPicker
      selectedStartDate={ fromDate }
      selectedEndDate={ toDate }
      onDateChange={ onChange }
      selectedDayColor={ colors.secondary }
      months={ translations.months }
      weekdays={ translations.weekDays }
      nextTitle={ t('next') }
      previousTitle={ t('previous') }
      previousTitleStyle={ previousTitleStyle }
      nextTitleStyle={ nextTitleStyle }
      dayLabelsWrapper={ dayLabelsWrapper }
      selectMonthTitle={ `${ t('select month in') } ` }
      selectYearTitle={ t('select year') }
      minDate={ minDate }
      maxDate={ maxDate }
      allowRangeSelection={ mode === 'range' }
      allowBackwardRangeSelect={ mode === 'range' }
      { ...rest }
    />
  )
}

DatePicker.propTypes = {
  mode: PropTypes.oneOf(['date', 'month', 'range']),
  value: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.object
  ]),
  minDate: PropTypes.instanceOf(Date),
  maxDate: PropTypes.instanceOf(Date),
  onChange: PropTypes.func.isRequired
}

DatePicker.defaultProps = {
  mode: 'date',
  value: undefined,
  minDate: undefined,
  maxDate: undefined
}

export default DatePicker
