import React from 'react'
import { Text } from 'react-native'

import moment from 'moment'
import PropTypes from 'prop-types'

import { capitalize } from 'lodash'

import { pencil } from '@smartcoop/icons'
import Button from '@smartcoop/mobile-components/Button'
import Icon from '@smartcoop/mobile-components/Icon'
import { colors } from '@smartcoop/styles'
import { momentBackDateFormat, momentFriendlyDateFormat } from '@smartcoop/utils/dates'

import {
  Container,
  Row
} from './styles'

const AnimalBirthItem = ({ animalBirth, onEdit }) => (
  <Container>
    <Row>
      <Text>{moment(animalBirth.occurrenceDate, momentBackDateFormat).format(momentFriendlyDateFormat)}</Text>
      <Text>{capitalize(animalBirth.gender)}</Text>

    </Row>
    <Row>
      <Text>{capitalize(animalBirth.birthType)}</Text>
      <Button
        variant="outlined"
        style={ { borderColor: colors.lightGrey, minHeight: 0, minWidth: 0 } }
        color={ colors.black }
        icon={ <Icon size={ 16 } icon={ pencil } color={ colors.black } /> }
        onPress={ onEdit }
      />
    </Row>
  </Container>
)

AnimalBirthItem.propTypes = {
  animalBirth: PropTypes.shape({
    birthType: PropTypes.any,
    mother: PropTypes.shape({
      earring: {
        earringCode: PropTypes.string
      }
    }),
    expectedDate: PropTypes.any,
    gender: PropTypes.any,
    isAlive: PropTypes.any,
    occurrenceDate: PropTypes.any,
    weight: PropTypes.any
  }).isRequired,
  onEdit: PropTypes.func.isRequired
}


export default AnimalBirthItem
