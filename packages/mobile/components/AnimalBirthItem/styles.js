import { Subheading } from 'react-native-paper'

import styled from 'styled-components/native'

export const Container = styled.View`
  display: flex;
`

export const Row = styled.View`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: flex-end;
`

export const Label = styled(Subheading)`
  font-weight: 600;
`

export const Box = styled.View`
  justify-content: flex-start;
`
