import React, { useCallback, forwardRef } from 'react'

import PropTypes from 'prop-types'
import * as Yup from 'yup'

import find from 'lodash/find'
import flow from 'lodash/fp/flow'
import map from 'lodash/map'
import mapValues from 'lodash/mapValues'
import reduce from 'lodash/reduce'

import { Scope } from '@smartcoop/forms'
import float from '@smartcoop/forms/validators/float.validator'
import minNumberValue from '@smartcoop/forms/validators/minNumberValue.validator'
import number from '@smartcoop/forms/validators/number.validator'
import required from '@smartcoop/forms/validators/required.validator'
import { useT } from '@smartcoop/i18n'
import Button from '@smartcoop/mobile-components/Button'
import DynamicInput from '@smartcoop/mobile-components/DynamicInput'
import Form from '@smartcoop/mobile-components/Form'
import { FormLabel } from '@smartcoop/mobile-containers/layouts/AuthenticatedLayout/theme'
import { toNumber } from '@smartcoop/utils/formatters'

import { Container, ButtonContainer } from './styles'


const DynamicForm = forwardRef((props, formRef) => {
  const { withoutSubmitButton, loading, onSubmit, fields } = props

  const t = useT()

  const handleSubmit = useCallback(
    (data) => {
      const onlyFields = reduce(
        fields,
        (acc, field) => [...acc, ...field.formFields],
        []
      )

      const formattedDate = mapValues(data, (formFieldsValues) => mapValues(formFieldsValues, (fieldValue, fieldName) => {
        const formField = find(onlyFields, f => f.name === fieldName)
        switch(formField.type) {
          case 'float':
            return fieldValue ? toNumber(fieldValue) : 0
          case 'number':
          case 'integer':
            return toNumber(fieldValue)
          default:
            return fieldValue
        }
      }))
      onSubmit(formattedDate)
    },
    [fields, onSubmit]
  )

  const formInputTypeSelect = useCallback(
    (type) => {
      switch (type) {
        case 'float':
          return float
        case 'integer':
        case 'number':
          return number
        default:
          return () => (param) => param
      }
    },
    []
  )

  const minimumValueSelect = useCallback(
    (type) => (
      type === 'float' || type === 'integer' || type === 'number'
        ? minNumberValue
        : () => (param) => param
    ),
    []
  )

  const handleSchemaConstructor = useCallback(
    () => Yup.object().shape(reduce(
      fields,
      (acc, field) => {
        const fieldFormFields = reduce(
          field.formFields,
          (formFieldsAcc, formField) => ({
            ...formFieldsAcc,
            [formField.name]: flow(
              formInputTypeSelect(formField.type)({ t }),
              minimumValueSelect(formField.type)({ t, field: formField.label }),
              required({ t })
            )(Yup.string())
          }),
          {}
        )
        return {
          ...acc,
          [field.id]: Yup.object().shape(fieldFormFields)
        }
      },
      {}
    )),
    [fields, formInputTypeSelect, minimumValueSelect, t]
  )

  return (
    <Container>
      <Form
        ref={ formRef }
        schemaConstructor={ handleSchemaConstructor }
        onSubmit={ handleSubmit }
        style={ { display: 'flex', flexDirection: 'column' } }
      >
        {
          map(fields, ({ name, id, formFields }) => (
            <Scope path={ id } key={ id }>
              <FormLabel>
                { name }
              </FormLabel>
              {map(formFields, ({ name: fieldName, label, type, unit, defaultValue }) => (
                <DynamicInput
                  key={ fieldName }
                  type={ type }
                  unit={ unit }
                  name={ fieldName }
                  label={ label }
                  defaultValue={ defaultValue }
                  disabled={ loading }
                />
              ))}
            </Scope>
          ))
        }

        {!withoutSubmitButton && (
          <ButtonContainer>
            <Button
              style={ { width: '48%' } }
              title={ t('next') }
              onPress={ () => formRef.current.submit() }
              disabled={ loading }
            />
          </ButtonContainer>
        )}
      </Form>
    </Container>
  )
})

DynamicForm.propTypes = {
  loading: PropTypes.bool,
  onSubmit: PropTypes.func,
  withoutSubmitButton: PropTypes.bool,
  fields: PropTypes.array.isRequired
}

DynamicForm.defaultProps = {
  loading: false,
  onSubmit: () => {},
  withoutSubmitButton: false
}

export default DynamicForm
