import { Subheading } from 'react-native-paper'

import styled from 'styled-components/native'


export const Header = styled.View`
  border-top-left-radius: 15px;
  border-top-right-radius: 15px;
  padding: 5px 10px ${ ({ paddingIos }) => paddingIos ? 15 : 5 }px;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  shadow-color: #000;
  shadow-offset: 0px -2px;
  shadow-opacity: 0.1;
  shadow-radius: 3px;
  elevation: 2;
  margin-top: -8px;
  background-color: ${ ({ color }) => color };
`

export const Text = styled(Subheading)`
  margin-right: 5px;
`

export const ClosedContainer = styled.View`
  position: absolute;
  bottom: 0;
  width: 100%;
  left: 0;
`
