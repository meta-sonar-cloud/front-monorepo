import React, { useRef, useCallback, useMemo, useState } from 'react'
import { TouchableWithoutFeedback } from 'react-native'
import { Modalize } from 'react-native-modalize'

import PropTypes from 'prop-types'

import { arrowUp } from '@smartcoop/icons'
import Icon from '@smartcoop/mobile-components/Icon'
import { colors } from '@smartcoop/styles'

import { Header, Text, ClosedContainer } from './styles'

const Accordion = (props) => {
  const {
    opened: externalOpened,
    paddingIos,
    insideTab,
    title,
    textColor,
    closedColor,
    ...rest
  } = props

  const [opened, setOpened] = useState(false)

  const modalizeRef = useRef(null)

  const modalTopOffset = useMemo(
    () => insideTab ? 140 : 80,
    [insideTab]
  )

  const handleOpen = useCallback(
    () => {
      if (modalizeRef.current) {
        modalizeRef.current.open()
        setOpened(true)
      }
    },
    []
  )

  const handleClose = useCallback(
    () => {
      setOpened(false)
    },
    []
  )

  return (
    <>
      {!opened && (
        <ClosedContainer>
          <TouchableWithoutFeedback
            onPress={ handleOpen }
          >
            <Header paddingIos={ paddingIos } color={ closedColor }>
              {!!title && <Text style={ { color: textColor } }>{title}</Text>}
              <Icon icon={ arrowUp } color={ textColor } />
            </Header>
          </TouchableWithoutFeedback>
        </ClosedContainer>
      )}

      <Modalize
        ref={ modalizeRef }
        modalTopOffset={ modalTopOffset }
        onClose={ handleClose }
        { ...rest }
      />
    </>
  )
}

Accordion.propTypes = {
  opened: PropTypes.bool,
  paddingIos: PropTypes.bool,
  insideTab: PropTypes.bool,
  title: PropTypes.string,
  textColor: PropTypes.string,
  closedColor: PropTypes.string
}

Accordion.defaultProps = {
  opened: false,
  paddingIos: false,
  insideTab: false,
  title: null,
  textColor: colors.text,
  closedColor: colors.white
}

export default Accordion
