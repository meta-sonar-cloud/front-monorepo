import { Subheading, Paragraph, Caption } from 'react-native-paper'

import styled from 'styled-components'

import colors from '@smartcoop/styles/colors'

export const Container = styled.TouchableOpacity`
  display: flex;
  flex-direction: row;
`

export const ImageContainer = styled.View`
  background-color: ${ colors.lightGrey };
  width: 70px;
  height: 70px;
  border-radius: 5px;
  border-width: 1px;
  border-color: ${ colors.muted };
`

export const ThumbImage = styled.Image`
  width: 68px;
  height: 68px;
  resize-mode: cover;
  border-radius: 5px;
`

export const LeftContainer = styled.View`
  display: flex;
  flex-direction: column;
`

export const TextGroup = styled.View`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  width: 80%;
`

export const TextLeft = styled.View`
  margin-left: 10px;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
`
export const TextRight = styled(Paragraph)`
`

export const TextBold = styled(Subheading)`
  margin: 0;
  font-weight: 600;
`

export const Text = styled(Paragraph)`
  margin-right: 10px;
`

export const Distance = styled(Caption)`
`

export const ModelYear = styled.View`
  display: flex;
  flex-direction: row;
`
export const CurrentImage = styled.Image`
  border-radius: 8px;
  background-color: ${ colors.lightGrey };
  height: 70px;
  width: 70px;
  object-fit: fill;
`
