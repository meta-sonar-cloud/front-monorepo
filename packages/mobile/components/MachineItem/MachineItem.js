import React, { useMemo } from 'react'

import PropTypes from 'prop-types'

import { isEmpty } from 'lodash'

import {
  Container,
  TextGroup,
  TextLeft,
  TextRight,
  TextBold,
  Text,
  ModelYear,
  ImageContainer,
  ThumbImage,
  Distance
} from './styles'

const MachineItem = (props) => {
  const { machine, onPress } = props

  const selectedImage = useMemo(
    () => !isEmpty(machine?.machineFiles) ? machine.machineFiles[0].fileUrl : null, [machine]
  )

  const distance = useMemo(
    () => (machine.property?.distance ? (`${ machine.property.distance  } km`) : ''),
    [machine]
  )

  const machineryData = useMemo(
    () => ({
      machineType: machine?.machineType?.description ?? null,
      machineBrand: machine?.machineBrand?.description ?? null,
      model: machine?.model ?? null,
      year: machine?.year ?? null

    }), [machine]
  )
  return (
    <Container onPress={ () => onPress(machine) }>
      <ImageContainer>
        <ThumbImage
          source={ { uri: selectedImage } }
        />
      </ImageContainer>
      <TextGroup>
        <TextLeft>
          <TextBold>{machineryData.machineType}</TextBold>
          <Text>{machineryData.machineBrand}</Text>
          <ModelYear>
            <Text>{machineryData.model}</Text>
            <Text>{machineryData.year}</Text>
          </ModelYear>
        </TextLeft>
        <TextRight>
          <Distance>{distance}</Distance>
        </TextRight>
      </TextGroup>
    </Container>
  )
}

MachineItem.propTypes = {
  machine: PropTypes.shape({
    machineType: PropTypes.object,
    machineBrand: PropTypes.object,
    year: PropTypes.number,
    model: PropTypes.string,
    machineFiles: PropTypes.array,
    property: PropTypes.object
  }),
  onPress: PropTypes.func
}

MachineItem.defaultProps = {
  machine: {
    machineType: {},
    brand: '',
    year: 0,
    model: '',
    distance: '',
    machineFiles: [],
    property: {}
  },
  onPress: () => {}
}

export default MachineItem
