import { Subheading, Paragraph, Caption } from 'react-native-paper'

import styled from 'styled-components/native'

import { colors } from '@smartcoop/styles'

export const Container = styled.TouchableOpacity`
  width: 100%;
`

export const HeaderContainer = styled.View`
  flex-direction: row;
  width: 100%;
  justify-content: space-between;
  align-items: center;
  padding-bottom: 7px;
`

export const Date = styled(Caption)`
  color: ${ ({ recent }) => recent ? colors.green : colors.mutedText };
  font-weight: ${ ({ recent }) => recent ? 'bold' : 'normal' };
`

export const UnreadSignal = styled.View`
  width: 12px;
  height: 12px;
  border-radius: 50px;
  background-color: ${ colors.red };
`

export const TitleContainer = styled.View`
  flex-direction: row;
  align-items: center;
  width: 100%;
`

export const ModuleIconContainer = styled.View`
  background-color: ${ colors.darkGrey };
  padding: 6px;
  border-radius: 50px;
  margin-right: 10px;
`

export const Title = styled(Subheading)`
  font-weight: bold;
  color: ${ ({ read }) => read ? colors.mutedText : colors.text };
`

export const BodyContainer = styled.View`
  padding-left: 37px;
  padding-right: 15px;
  width: 100%;
`

export const Body = styled(Paragraph)`
  color: ${ ({ read }) => read ? colors.mutedText : colors.text };
  padding-bottom: 5px;
`
