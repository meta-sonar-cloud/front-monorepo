import React, {
  useState,
  useCallback,
  useEffect,
  useMemo,
  useImperativeHandle,
  forwardRef
} from 'react'

import PropTypes from 'prop-types'

import filter from 'lodash/filter'
import find from 'lodash/find'
import isEmpty from 'lodash/isEmpty'
import isEqual from 'lodash/isEqual'
import map from 'lodash/map'

import { FieldProvider, useField } from '@smartcoop/forms'

import CheckboxCard from './CheckboxCard/CheckboxCard'
import {
  Container,
  CheckboxContainer,
  ErrorLabel,
  Label
} from './styles'

const CheckboxGroupCard = (props) => {
  const {
    label: externalLabel,
    options: externalOptions,
    variant,
    defaultValue: externalDefaultValue,
    cardProps
  } = props

  const defaultValue = useMemo(
    () => externalDefaultValue || [],
    // eslint-disable-next-line react-hooks/exhaustive-deps
    []
  )

  const [value, setValue] = useState(defaultValue)
  const [mounted, setMounted] = useState(false)
  const [options, setOptions] = useState(externalOptions)
  const {
    fieldName,
    fieldRef,
    formRef,
    handleChangeNative,
    setTouched,
    required,
    error,
    validateField
  } = useField()

  const label = useMemo(
    () => `${ externalLabel }${ required ? ' *' : '' }`,
    [externalLabel, required]
  )

  const onChange = useCallback(
    (newValue, checked) => {
      setValue((oldValue) => {
        if (checked) {
          // add
          return [...oldValue, newValue]
        }
        // remove
        return filter(oldValue, val => val !== newValue)
      })
    },
    []
  )

  useEffect(() => {
    if (mounted) {
      const formFieldRef = formRef.current.getFieldRef(fieldName)
      if (!isEqual(value, formFieldRef.value)) {
        handleChangeNative(value)
        if (isEmpty(value)) {
          setTouched(true)
        }
      }
    } else {
      setMounted(true)
    }
    const checkboxValues = map(options, option => ({
      ...option,
      checked: !!find(value, val => val === option.value)
    }))
    setOptions(checkboxValues)
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [value])

  useImperativeHandle(fieldRef, () => ({
    ...(fieldRef.current || {}),
    value,
    defaultValue,
    setValue,
    validateField
  }))

  return (
    <Container>
      <Label error={ error } >{label}</Label>
      <ErrorLabel>{error}</ErrorLabel>
      <CheckboxContainer style={ { flexDirection: variant } }>
        {
          map(options, (option) => (
            <CheckboxCard
              key={ option.value }
              { ...option }
              onChange={ onChange }
              { ...cardProps }
            >
              {option.children}
            </CheckboxCard>
          ))
        }
      </CheckboxContainer>
    </Container>
  )
}

CheckboxGroupCard.propTypes = {
  label: PropTypes.string,
  variant: PropTypes.oneOf(['row', 'column']).isRequired,
  options: PropTypes.arrayOf(PropTypes.shape({
    label: PropTypes.string,
    value: PropTypes.string,
    children: PropTypes.element
  })).isRequired,
  defaultValue: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.arrayOf(PropTypes.string)
  ]),
  cardProps: PropTypes.object
}

CheckboxGroupCard.defaultProps = {
  label: null,
  defaultValue: undefined,
  cardProps: {}
}

const Field = forwardRef(({ path, ...props }, ref) => (
  <FieldProvider
    ref={ ref }
    { ...props }
    registerFieldOptions={ { path } }
    FieldComponent={ CheckboxGroupCard }
  />
))

Field.propTypes = {
  label: PropTypes.string,
  variant: PropTypes.oneOf(['row', 'column']),
  options: PropTypes.arrayOf(PropTypes.shape({
    label: PropTypes.string,
    value: PropTypes.string
  })),
  name: PropTypes.string.isRequired,
  type: PropTypes.string,
  className: PropTypes.string,
  size: PropTypes.oneOf(['small', 'medium']),
  disabled: PropTypes.bool,
  onChange: PropTypes.func,
  onBlur: PropTypes.func,
  path: PropTypes.string,
  defaultValue: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.arrayOf(PropTypes.string)
  ])
}

Field.defaultProps = {
  variant: 'column',
  type: 'text',
  className: '',
  size: 'small',
  disabled: false,
  onChange: () => {},
  onBlur: () => {},
  path: 'value',
  options: [],
  defaultValue: undefined,
  label: null
}

export default Field
