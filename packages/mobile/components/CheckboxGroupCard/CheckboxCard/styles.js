import styled from 'styled-components/native'


export const CheckboxTouchableContainer = styled.TouchableOpacity`
  display: flex;
  flex-direction: row;
`

export const CheckboxContainer = styled.View`
  background-color: #FFF;
  border-radius: 5px;
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  min-height: 30px;
  padding: 11px 15px 11px 15px;
  margin-bottom: 15px;
`

export const IconContainer = styled.View`
  max-width: 20px;
  min-width: 20px;
  padding-right: 10px;
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: flex-start;
`

export const ContentContainer = styled.View`
  display: flex;
`
