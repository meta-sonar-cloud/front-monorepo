import React, { useCallback, useMemo } from 'react'

import PropTypes from 'prop-types'

import { checkboxChecked as checkedIcon, checkboxUnchecked as uncheckedIcon } from '@smartcoop/icons'
import Icon from '@smartcoop/mobile-components/Icon'
import colors from '@smartcoop/styles/colors'

import {
  CheckboxTouchableContainer,
  CheckboxContainer,
  ContentContainer,
  IconContainer
} from './styles'

const CheckboxCard = (props) => {
  const { checked, style, onChange, value, children, ...rest } = props

  const isChecked = useMemo(
    () => (
      checked ? checkedIcon : uncheckedIcon
    ),
    [checked]
  )
  const toggle = useCallback(
    () => {
      onChange(value, !checked)
    },
    [onChange, value, checked]
  )

  return (
    <CheckboxContainer { ...rest } checked={ checked } style={ { ...style } }>
      <CheckboxTouchableContainer onPress={ toggle }>
        <IconContainer>
          <Icon color={ checked ? colors.yellow : colors.black } icon={ isChecked } size={ 16 } />
        </IconContainer>
      </CheckboxTouchableContainer>
      <ContentContainer>
        {children}
      </ContentContainer>
    </CheckboxContainer>

  )
}

CheckboxCard.propTypes = {
  checked: PropTypes.oneOfType([PropTypes.bool]),
  value: PropTypes.string.isRequired,
  children: PropTypes.element.isRequired,
  onChange: PropTypes.func.isRequired,
  style: PropTypes.object
}

CheckboxCard.defaultProps = {
  style: {},
  checked: false
}

export default CheckboxCard
