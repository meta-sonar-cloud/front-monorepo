# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [0.18.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.17.0...v0.18.0) (2021-04-19)


### Bug Fixes

* **#1737:** adjustment show pev in quick access web mobile ([80081e4](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/80081e41e0eb28e8d4a211c6787afaa231c0854a)), closes [#1737](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1737) [#572](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/572)
* **#1745:** adjustment slug next actions ([056c374](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/056c3746b2046bb18162214b870231458129d567)), closes [#1745](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1745) [#580](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/580)
* **docuemntos#1766:** ajustes mapa ([f2306ab](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f2306abda00fc359d42696faeafb74263d437be1)), closes [docuemntos#1766](http://gitlab.meta.com.br/docuemntos/issues/1766)
* **documentos#1750:** adjustments ([28966d7](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/28966d704546ed37e849552d6fddf196ce808568)), closes [documentos#1750](http://gitlab.meta.com.br/documentos/issues/1750)
* **modal:** closing modal when user press back OS button ([4b36651](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4b3665141f4bc19438d57c7b17007e5eadaf8af1))


### Features

* resolve [#1714](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1714) [#1756](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1756) [#1757](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1757) [#1758](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1758) ([ecfb39f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ecfb39f2f1e64696f55ed5a93f3db260fbf062b0))
* resolve issue ([a8a64e2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a8a64e26d60cd4da0a6654efa924ab8449661974))
* resolve some issues ([0e7d7ce](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0e7d7ce1738f55d75ddd3907cb5ca197b5885e1a))
* **#1737:** adjustment animal item layout mobile ([9deccc2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9deccc24c5a9700e910263c4b26fe5787fb6937f)), closes [#1737](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1737) [#572](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/572)
* **#1737:** adjustment layout animalbirth and pev mobile ([00f6c1d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/00f6c1d90f8804fb2334a3623223f3c7ec0c3824)), closes [#1737](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1737) [#572](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/572)
* **#1745:** add next action and nex action date ([95d2229](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/95d2229bdd39c62dbf02379b85bd0c3528fa943e)), closes [#1745](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1745) [#580](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/580)
* **documentos#1737:** change in button ([25e7d20](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/25e7d200abc6d1a1b5f515ecc20016050114ff48)), closes [documentos#1737](http://gitlab.meta.com.br/documentos/issues/1737)
* **documentos#1737:** inseminationItem ([f657827](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f657827f375e615cf882e13c5bac6085faaa5fd0)), closes [documentos#1737](http://gitlab.meta.com.br/documentos/issues/1737)





# [0.17.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.16.0...v0.17.0) (2021-04-15)


### Bug Fixes

* **documentos#1704:** map adjustments ([9dcd718](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9dcd718ee90e9e0e5c55a29f652c2ccf3f820d5f)), closes [documentos#1704](http://gitlab.meta.com.br/documentos/issues/1704)


### Features

* resolve [#1735](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1735) ([cda6387](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/cda6387c44735527fe95bf60838cc9098715d96e))
* **#1725:** remove buttons delete in all list ([6fab45b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/6fab45b06ff79eee6e454fe476a6febafffa9da4)), closes [#1725](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1725) [#565](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/565)
* **documentos#1715:** renaming things and logic changes ([7b0ad3c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7b0ad3c5f4fd006fd458cfb31d4817270f4d561b)), closes [documentos#1715](http://gitlab.meta.com.br/documentos/issues/1715)
* **documentos#1749:** earring code in mobile animal birth ([a3c09eb](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a3c09eb343223afbdfd5bbe02ce07191fb04cbad)), closes [documentos#1749](http://gitlab.meta.com.br/documentos/issues/1749)





# [0.16.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.15.0...v0.16.0) (2021-04-13)


### Bug Fixes

* **ajustes finos:** ajustes finos da aplicação ([eefa46f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/eefa46fe613632b216df51736217149a8027dd16))
* **documentos#1560:** ajustes aparencia ([d7f810f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d7f810f0ddf2f083da431d974a709092033aac17)), closes [documentos#1560](http://gitlab.meta.com.br/documentos/issues/1560)
* **documentos#1632:** ajustes finos app ([0e0ffdb](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0e0ffdbce9e4c4ab1c84e30d092d1cd16f83967d)), closes [documentos#1632](http://gitlab.meta.com.br/documentos/issues/1632)


### Features

* **#1531 #1532:** create list animals, create and edit ([03bc1a2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/03bc1a23a48639d11f58f59faa7868a05a34984f)), closes [#1531](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1531) [#1532](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1532) [#499](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/499)
* **#1531 #1532:** rebase with develop ([5ca5628](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/5ca56284767d383497f3f75059af87b466694c5b)), closes [#1531](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1531) [#1532](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1532) [#499](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/499)
* **#1630 #1631:** create diagnostic item mobile ([be9c5c9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/be9c5c9bfd14630a1a1903b4a7f52c5240613913)), closes [#1630](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1630) [#1631](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1631) [#521](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/521)
* **#1630 #1631:** diagnosticitem package ([db9c767](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/db9c767e24b3d340ab1c2659f8e1dd152ad4fdab)), closes [#1630](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1630) [#1631](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1631)
* **#1664:** add register actions in cattle management menu ([6219d1d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/6219d1da626f75cdde896715f3b5304d71220448)), closes [#1664](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1664) [#539](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/539)
* **#1675 #1676:** adjustment mobile, web, and add new feature ([94afa5b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/94afa5bafdb99f6de2d5f704b875d560ef384be3)), closes [#1675](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1675) [#1676](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1676) [#546](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/546)
* **documentos#1325:** created pev crud for mobile ([f0ef8c0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f0ef8c00ffd2a45ad9b3a34ce5fee71fc1eced50)), closes [documentos#1325](http://gitlab.meta.com.br/documentos/issues/1325)
* **documentos#1495:** lint fixes ([10d5903](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/10d59035b488d73e6dc0536dd70892fe5fbd902f)), closes [documentos#1495](http://gitlab.meta.com.br/documentos/issues/1495)
* **documentos#1495:** lots in mobile ([3fdcfbe](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3fdcfbeb9b601a1672a8d10cb1dabfdbccec2022)), closes [documentos#1495](http://gitlab.meta.com.br/documentos/issues/1495)
* **documentos#1495:** modals, saga and item ([a04f400](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a04f400ff9eb8feddfbef787e6b8fd7121985631)), closes [documentos#1495](http://gitlab.meta.com.br/documentos/issues/1495)
* **documentos#1579:** adds menu item in animal ([c1e0730](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c1e0730f8646e9cd67c1cda8894fb97a686d0423)), closes [documentos#1579](http://gitlab.meta.com.br/documentos/issues/1579)
* **documentos#1579:** adds verticaldots ([bbad462](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/bbad462cc8b7e408eb01d5c4984d391a4b6b8b2a)), closes [documentos#1579](http://gitlab.meta.com.br/documentos/issues/1579)
* **documentos#1579:** animal birth item ([6683f8e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/6683f8e53178711dc58ea2609ae160a40ea0b1b5)), closes [documentos#1579](http://gitlab.meta.com.br/documentos/issues/1579)
* **documentos#1579:** insemination mobile ([767d69b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/767d69b03b2e183e6a70f74b4bf648f21bedea84)), closes [documentos#1579](http://gitlab.meta.com.br/documentos/issues/1579)
* **documentos#1579:** mobile insemination ([1bf225d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1bf225d6f5ffbafb10226bf7c5c77b13a03ed58e)), closes [documentos#1579](http://gitlab.meta.com.br/documentos/issues/1579)
* **documentos#1650:** calving in mobile ([f1218c7](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f1218c7a11e6444eee6c4210889ba2eba91c1629)), closes [documentos#1650](http://gitlab.meta.com.br/documentos/issues/1650)
* **documentos#1650:** register animalbirth screen ([5d093c8](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/5d093c823d050a358e0639a6f07bd62e8a70c2cd)), closes [documentos#1650](http://gitlab.meta.com.br/documentos/issues/1650)
* resolve [#1566](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1566) ([2fbc2ca](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2fbc2ca04a5a635ab63a491388503bbcb495cd1b))







**Note:** Version bump only for package @smartcoop/mobile-components





# [0.15.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.12.0-alpha.1...v0.15.0) (2021-03-23)


### Bug Fixes

* **#1311:** fix change property modal ([0b5d5d9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0b5d5d97a29f47cdd82f475eae3c0d59d5fd4f21)), closes [#400](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/400)
* **documentos#1458:** fix quantityinput ([3f18d92](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3f18d92595a1a84b2d33f9bf838162b5a838d63a))
* **documentos#1458:** fix typo in input select ([0235284](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/023528495cc2085e29f60d42737be0ee86ae19ce))
* **fix layout when keyboard is open:** layout with keyboard ([ab61892](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ab61892bd4a866cd3d5a4c2ee1e6b01474159acf))
* fixed mobile color palette description values ([4d9654a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4d9654ac03cd5f29bc0b7e7ad0ba6eefdd2f87eb))


### Features

* addded pickerProps prop ([5fd2c5d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/5fd2c5dd60875824142ba22bb273341abfe78ffe))
* resolve [#1504](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1504) ([33e9146](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/33e9146cba03ef22188d428f1d033c8311be04a9))
* **#1116:** link cards milk dashboard in mobile ([a52742b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a52742b959b64f4db45d3e6f72022b401f330267)), closes [#1116](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1116)
* **#1288:** adjustment key item selection ([fed84a4](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/fed84a4ee1f32a4aed07de584ddd9a10dce5f5fe)), closes [#1288](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1288)
* **#1288:** adjustment select organization ([2dbbc9c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2dbbc9c4234ad1a408da856e1c2deeccc814f993)), closes [#1288](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1288) [#395](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/395)
* **documentos#1116:** milk data card in mobile ([d9a35c5](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d9a35c5b248186642b70caa865fac72e694d1220)), closes [documentos#1116](http://gitlab.meta.com.br/documentos/issues/1116)
* **documentos#1116:** tambo for mobile ([f4d650f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f4d650f18602702f4cfccdf32758336da2041ca0)), closes [documentos#1116](http://gitlab.meta.com.br/documentos/issues/1116)
* **documentos#1240:** address component in mobile ([3bc90f0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3bc90f07c5eff93ed83d9f774259cfdedec7f6da)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** init avatar input ([d018ec3](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d018ec371fb33f01a39e91f29bbc853227b6ff34)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** style tweaks ([861ea3e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/861ea3ef96a33c80d02370d8b230f056b4bd9387)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1290:** maxLength of InputCode set to 12 ([60c0849](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/60c084906a7300bccc03f6326c375916cd780d4a)), closes [documentos#1290](http://gitlab.meta.com.br/documentos/issues/1290)
* **documentos#1292:** created date tabs component ([e360ec8](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e360ec8da3b744d7ca6f17f2aaac23a5d1fcd5df)), closes [documentos#1292](http://gitlab.meta.com.br/documentos/issues/1292)
* **documentos#1323:** removes console.log ([005a2c1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/005a2c1c08ac6cb7649419c54168a5b8d8c0f629)), closes [documentos#1323](http://gitlab.meta.com.br/documentos/issues/1323)
* **documentos#1395:** touchable button expands full width now ([9f451ad](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9f451ade95a4d11403059b615ea550535c4fb20b)), closes [documentos#1395](http://gitlab.meta.com.br/documentos/issues/1395)
* **documentos#1400:** accept terms modal mobile ([7244e0d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7244e0d161d81982fbecaa13a08148f342a94527)), closes [documentos#1400](http://gitlab.meta.com.br/documentos/issues/1400)
* **documentos#1400:** removes flex: 1 in button ([b326fa4](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b326fa4d5a405e29a89f823820eca620a54a9ee7)), closes [documentos#1400](http://gitlab.meta.com.br/documentos/issues/1400)
* **documentos#1409:** fixes label in tooltip ([8fab327](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8fab327b94b831d0bab5f6b985f5c59efe797057))
* **documentos#1436:** changes in commercialization ([f4edf6b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f4edf6b977ef4c7a8fe514f844097ab2703e1572)), closes [documentos#1436](http://gitlab.meta.com.br/documentos/issues/1436)
* **documentos#1438:** ajustes mapa ([97b7c63](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/97b7c638d12a5abd12cffe38cc78553d4457b6f8)), closes [documentos#1438](http://gitlab.meta.com.br/documentos/issues/1438)
* **documentos#1441:** barter fixes ([8575fa4](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8575fa4c8a98e6b61e167c14c4ef19e7a6a2f91b)), closes [documentos#1441](http://gitlab.meta.com.br/documentos/issues/1441)
* **documentos#1554:** alterada exibição de matricula ([a21246b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a21246b5122acd568b4b0168d61321bb485e65d8)), closes [documentos#1554](http://gitlab.meta.com.br/documentos/issues/1554)
* added style prop to mobile input quantity ([d1758b3](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d1758b3c424bc38a9a7ca291e285c33f7fcb0e42))
* fixed input select ([3463a24](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3463a2410020e27157cbd6867797d46755255d07))
* **documentos#885:** changed field screen ([4299f89](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4299f898298c9641e4e63098dfdcf85a5633c11d)), closes [documentos#885](http://gitlab.meta.com.br/documentos/issues/885)





# [0.15.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.12.0-alpha.1...v0.15.0) (2021-03-23)


### Bug Fixes

* **#1311:** fix change property modal ([0b5d5d9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0b5d5d97a29f47cdd82f475eae3c0d59d5fd4f21)), closes [#400](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/400)
* **documentos#1458:** fix quantityinput ([3f18d92](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3f18d92595a1a84b2d33f9bf838162b5a838d63a))
* **documentos#1458:** fix typo in input select ([0235284](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/023528495cc2085e29f60d42737be0ee86ae19ce))
* **fix layout when keyboard is open:** layout with keyboard ([ab61892](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ab61892bd4a866cd3d5a4c2ee1e6b01474159acf))
* fixed mobile color palette description values ([4d9654a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4d9654ac03cd5f29bc0b7e7ad0ba6eefdd2f87eb))


### Features

* addded pickerProps prop ([5fd2c5d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/5fd2c5dd60875824142ba22bb273341abfe78ffe))
* resolve [#1504](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1504) ([33e9146](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/33e9146cba03ef22188d428f1d033c8311be04a9))
* **#1116:** link cards milk dashboard in mobile ([a52742b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a52742b959b64f4db45d3e6f72022b401f330267)), closes [#1116](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1116)
* **#1288:** adjustment key item selection ([fed84a4](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/fed84a4ee1f32a4aed07de584ddd9a10dce5f5fe)), closes [#1288](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1288)
* **#1288:** adjustment select organization ([2dbbc9c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2dbbc9c4234ad1a408da856e1c2deeccc814f993)), closes [#1288](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1288) [#395](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/395)
* **documentos#1116:** milk data card in mobile ([d9a35c5](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d9a35c5b248186642b70caa865fac72e694d1220)), closes [documentos#1116](http://gitlab.meta.com.br/documentos/issues/1116)
* **documentos#1116:** tambo for mobile ([f4d650f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f4d650f18602702f4cfccdf32758336da2041ca0)), closes [documentos#1116](http://gitlab.meta.com.br/documentos/issues/1116)
* **documentos#1240:** address component in mobile ([3bc90f0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3bc90f07c5eff93ed83d9f774259cfdedec7f6da)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** init avatar input ([d018ec3](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d018ec371fb33f01a39e91f29bbc853227b6ff34)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** style tweaks ([861ea3e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/861ea3ef96a33c80d02370d8b230f056b4bd9387)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1290:** maxLength of InputCode set to 12 ([60c0849](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/60c084906a7300bccc03f6326c375916cd780d4a)), closes [documentos#1290](http://gitlab.meta.com.br/documentos/issues/1290)
* **documentos#1292:** created date tabs component ([e360ec8](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e360ec8da3b744d7ca6f17f2aaac23a5d1fcd5df)), closes [documentos#1292](http://gitlab.meta.com.br/documentos/issues/1292)
* **documentos#1323:** removes console.log ([005a2c1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/005a2c1c08ac6cb7649419c54168a5b8d8c0f629)), closes [documentos#1323](http://gitlab.meta.com.br/documentos/issues/1323)
* **documentos#1395:** touchable button expands full width now ([9f451ad](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9f451ade95a4d11403059b615ea550535c4fb20b)), closes [documentos#1395](http://gitlab.meta.com.br/documentos/issues/1395)
* **documentos#1400:** accept terms modal mobile ([7244e0d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7244e0d161d81982fbecaa13a08148f342a94527)), closes [documentos#1400](http://gitlab.meta.com.br/documentos/issues/1400)
* **documentos#1400:** removes flex: 1 in button ([b326fa4](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b326fa4d5a405e29a89f823820eca620a54a9ee7)), closes [documentos#1400](http://gitlab.meta.com.br/documentos/issues/1400)
* **documentos#1409:** fixes label in tooltip ([8fab327](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8fab327b94b831d0bab5f6b985f5c59efe797057))
* **documentos#1436:** changes in commercialization ([f4edf6b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f4edf6b977ef4c7a8fe514f844097ab2703e1572)), closes [documentos#1436](http://gitlab.meta.com.br/documentos/issues/1436)
* **documentos#1438:** ajustes mapa ([97b7c63](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/97b7c638d12a5abd12cffe38cc78553d4457b6f8)), closes [documentos#1438](http://gitlab.meta.com.br/documentos/issues/1438)
* **documentos#1441:** barter fixes ([8575fa4](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8575fa4c8a98e6b61e167c14c4ef19e7a6a2f91b)), closes [documentos#1441](http://gitlab.meta.com.br/documentos/issues/1441)
* **documentos#1554:** alterada exibição de matricula ([a21246b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a21246b5122acd568b4b0168d61321bb485e65d8)), closes [documentos#1554](http://gitlab.meta.com.br/documentos/issues/1554)
* added style prop to mobile input quantity ([d1758b3](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d1758b3c424bc38a9a7ca291e285c33f7fcb0e42))
* fixed input select ([3463a24](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3463a2410020e27157cbd6867797d46755255d07))
* **documentos#885:** changed field screen ([4299f89](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4299f898298c9641e4e63098dfdcf85a5633c11d)), closes [documentos#885](http://gitlab.meta.com.br/documentos/issues/885)





# [0.15.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.12.0-alpha.1...v0.15.0) (2021-03-23)


### Bug Fixes

* **#1311:** fix change property modal ([0b5d5d9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0b5d5d97a29f47cdd82f475eae3c0d59d5fd4f21)), closes [#400](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/400)
* **documentos#1458:** fix quantityinput ([3f18d92](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3f18d92595a1a84b2d33f9bf838162b5a838d63a))
* **documentos#1458:** fix typo in input select ([0235284](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/023528495cc2085e29f60d42737be0ee86ae19ce))
* **fix layout when keyboard is open:** layout with keyboard ([ab61892](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ab61892bd4a866cd3d5a4c2ee1e6b01474159acf))
* fixed mobile color palette description values ([4d9654a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4d9654ac03cd5f29bc0b7e7ad0ba6eefdd2f87eb))


### Features

* addded pickerProps prop ([5fd2c5d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/5fd2c5dd60875824142ba22bb273341abfe78ffe))
* resolve [#1504](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1504) ([33e9146](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/33e9146cba03ef22188d428f1d033c8311be04a9))
* **#1116:** link cards milk dashboard in mobile ([a52742b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a52742b959b64f4db45d3e6f72022b401f330267)), closes [#1116](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1116)
* **#1288:** adjustment key item selection ([fed84a4](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/fed84a4ee1f32a4aed07de584ddd9a10dce5f5fe)), closes [#1288](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1288)
* **#1288:** adjustment select organization ([2dbbc9c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2dbbc9c4234ad1a408da856e1c2deeccc814f993)), closes [#1288](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1288) [#395](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/395)
* **documentos#1116:** milk data card in mobile ([d9a35c5](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d9a35c5b248186642b70caa865fac72e694d1220)), closes [documentos#1116](http://gitlab.meta.com.br/documentos/issues/1116)
* **documentos#1116:** tambo for mobile ([f4d650f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f4d650f18602702f4cfccdf32758336da2041ca0)), closes [documentos#1116](http://gitlab.meta.com.br/documentos/issues/1116)
* **documentos#1240:** address component in mobile ([3bc90f0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3bc90f07c5eff93ed83d9f774259cfdedec7f6da)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** init avatar input ([d018ec3](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d018ec371fb33f01a39e91f29bbc853227b6ff34)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** style tweaks ([861ea3e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/861ea3ef96a33c80d02370d8b230f056b4bd9387)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1290:** maxLength of InputCode set to 12 ([60c0849](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/60c084906a7300bccc03f6326c375916cd780d4a)), closes [documentos#1290](http://gitlab.meta.com.br/documentos/issues/1290)
* **documentos#1292:** created date tabs component ([e360ec8](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e360ec8da3b744d7ca6f17f2aaac23a5d1fcd5df)), closes [documentos#1292](http://gitlab.meta.com.br/documentos/issues/1292)
* **documentos#1323:** removes console.log ([005a2c1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/005a2c1c08ac6cb7649419c54168a5b8d8c0f629)), closes [documentos#1323](http://gitlab.meta.com.br/documentos/issues/1323)
* **documentos#1395:** touchable button expands full width now ([9f451ad](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9f451ade95a4d11403059b615ea550535c4fb20b)), closes [documentos#1395](http://gitlab.meta.com.br/documentos/issues/1395)
* **documentos#1400:** accept terms modal mobile ([7244e0d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7244e0d161d81982fbecaa13a08148f342a94527)), closes [documentos#1400](http://gitlab.meta.com.br/documentos/issues/1400)
* **documentos#1400:** removes flex: 1 in button ([b326fa4](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b326fa4d5a405e29a89f823820eca620a54a9ee7)), closes [documentos#1400](http://gitlab.meta.com.br/documentos/issues/1400)
* **documentos#1409:** fixes label in tooltip ([8fab327](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8fab327b94b831d0bab5f6b985f5c59efe797057))
* **documentos#1436:** changes in commercialization ([f4edf6b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f4edf6b977ef4c7a8fe514f844097ab2703e1572)), closes [documentos#1436](http://gitlab.meta.com.br/documentos/issues/1436)
* **documentos#1438:** ajustes mapa ([97b7c63](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/97b7c638d12a5abd12cffe38cc78553d4457b6f8)), closes [documentos#1438](http://gitlab.meta.com.br/documentos/issues/1438)
* **documentos#1441:** barter fixes ([8575fa4](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8575fa4c8a98e6b61e167c14c4ef19e7a6a2f91b)), closes [documentos#1441](http://gitlab.meta.com.br/documentos/issues/1441)
* **documentos#1554:** alterada exibição de matricula ([a21246b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a21246b5122acd568b4b0168d61321bb485e65d8)), closes [documentos#1554](http://gitlab.meta.com.br/documentos/issues/1554)
* added style prop to mobile input quantity ([d1758b3](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d1758b3c424bc38a9a7ca291e285c33f7fcb0e42))
* fixed input select ([3463a24](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3463a2410020e27157cbd6867797d46755255d07))
* **documentos#885:** changed field screen ([4299f89](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4299f898298c9641e4e63098dfdcf85a5633c11d)), closes [documentos#885](http://gitlab.meta.com.br/documentos/issues/885)





# [0.14.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.12.0-alpha.1...v0.14.0) (2021-03-23)


### Bug Fixes

* **#1311:** fix change property modal ([0b5d5d9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0b5d5d97a29f47cdd82f475eae3c0d59d5fd4f21)), closes [#400](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/400)
* **documentos#1458:** fix quantityinput ([3f18d92](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3f18d92595a1a84b2d33f9bf838162b5a838d63a))
* **documentos#1458:** fix typo in input select ([0235284](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/023528495cc2085e29f60d42737be0ee86ae19ce))
* **fix layout when keyboard is open:** layout with keyboard ([ab61892](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ab61892bd4a866cd3d5a4c2ee1e6b01474159acf))
* fixed mobile color palette description values ([4d9654a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4d9654ac03cd5f29bc0b7e7ad0ba6eefdd2f87eb))


### Features

* addded pickerProps prop ([5fd2c5d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/5fd2c5dd60875824142ba22bb273341abfe78ffe))
* resolve [#1504](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1504) ([33e9146](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/33e9146cba03ef22188d428f1d033c8311be04a9))
* **#1116:** link cards milk dashboard in mobile ([a52742b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a52742b959b64f4db45d3e6f72022b401f330267)), closes [#1116](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1116)
* **#1288:** adjustment key item selection ([fed84a4](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/fed84a4ee1f32a4aed07de584ddd9a10dce5f5fe)), closes [#1288](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1288)
* **#1288:** adjustment select organization ([2dbbc9c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2dbbc9c4234ad1a408da856e1c2deeccc814f993)), closes [#1288](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1288) [#395](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/395)
* **documentos#1116:** milk data card in mobile ([d9a35c5](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d9a35c5b248186642b70caa865fac72e694d1220)), closes [documentos#1116](http://gitlab.meta.com.br/documentos/issues/1116)
* **documentos#1116:** tambo for mobile ([f4d650f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f4d650f18602702f4cfccdf32758336da2041ca0)), closes [documentos#1116](http://gitlab.meta.com.br/documentos/issues/1116)
* **documentos#1240:** address component in mobile ([3bc90f0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3bc90f07c5eff93ed83d9f774259cfdedec7f6da)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** init avatar input ([d018ec3](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d018ec371fb33f01a39e91f29bbc853227b6ff34)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** style tweaks ([861ea3e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/861ea3ef96a33c80d02370d8b230f056b4bd9387)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1290:** maxLength of InputCode set to 12 ([60c0849](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/60c084906a7300bccc03f6326c375916cd780d4a)), closes [documentos#1290](http://gitlab.meta.com.br/documentos/issues/1290)
* **documentos#1292:** created date tabs component ([e360ec8](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e360ec8da3b744d7ca6f17f2aaac23a5d1fcd5df)), closes [documentos#1292](http://gitlab.meta.com.br/documentos/issues/1292)
* **documentos#1323:** removes console.log ([005a2c1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/005a2c1c08ac6cb7649419c54168a5b8d8c0f629)), closes [documentos#1323](http://gitlab.meta.com.br/documentos/issues/1323)
* **documentos#1395:** touchable button expands full width now ([9f451ad](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9f451ade95a4d11403059b615ea550535c4fb20b)), closes [documentos#1395](http://gitlab.meta.com.br/documentos/issues/1395)
* **documentos#1400:** accept terms modal mobile ([7244e0d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7244e0d161d81982fbecaa13a08148f342a94527)), closes [documentos#1400](http://gitlab.meta.com.br/documentos/issues/1400)
* **documentos#1400:** removes flex: 1 in button ([b326fa4](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b326fa4d5a405e29a89f823820eca620a54a9ee7)), closes [documentos#1400](http://gitlab.meta.com.br/documentos/issues/1400)
* **documentos#1409:** fixes label in tooltip ([8fab327](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8fab327b94b831d0bab5f6b985f5c59efe797057))
* **documentos#1436:** changes in commercialization ([f4edf6b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f4edf6b977ef4c7a8fe514f844097ab2703e1572)), closes [documentos#1436](http://gitlab.meta.com.br/documentos/issues/1436)
* **documentos#1438:** ajustes mapa ([97b7c63](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/97b7c638d12a5abd12cffe38cc78553d4457b6f8)), closes [documentos#1438](http://gitlab.meta.com.br/documentos/issues/1438)
* **documentos#1441:** barter fixes ([8575fa4](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8575fa4c8a98e6b61e167c14c4ef19e7a6a2f91b)), closes [documentos#1441](http://gitlab.meta.com.br/documentos/issues/1441)
* **documentos#1554:** alterada exibição de matricula ([a21246b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a21246b5122acd568b4b0168d61321bb485e65d8)), closes [documentos#1554](http://gitlab.meta.com.br/documentos/issues/1554)
* added style prop to mobile input quantity ([d1758b3](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d1758b3c424bc38a9a7ca291e285c33f7fcb0e42))
* fixed input select ([3463a24](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3463a2410020e27157cbd6867797d46755255d07))
* **documentos#885:** changed field screen ([4299f89](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4299f898298c9641e4e63098dfdcf85a5633c11d)), closes [documentos#885](http://gitlab.meta.com.br/documentos/issues/885)





# [0.13.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.12.0-alpha.1...v0.13.0) (2021-03-23)


### Bug Fixes

* **#1311:** fix change property modal ([0b5d5d9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0b5d5d97a29f47cdd82f475eae3c0d59d5fd4f21)), closes [#400](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/400)
* **documentos#1458:** fix quantityinput ([3f18d92](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3f18d92595a1a84b2d33f9bf838162b5a838d63a))
* **documentos#1458:** fix typo in input select ([0235284](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/023528495cc2085e29f60d42737be0ee86ae19ce))
* **fix layout when keyboard is open:** layout with keyboard ([ab61892](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ab61892bd4a866cd3d5a4c2ee1e6b01474159acf))
* fixed mobile color palette description values ([4d9654a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4d9654ac03cd5f29bc0b7e7ad0ba6eefdd2f87eb))


### Features

* addded pickerProps prop ([5fd2c5d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/5fd2c5dd60875824142ba22bb273341abfe78ffe))
* resolve [#1504](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1504) ([33e9146](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/33e9146cba03ef22188d428f1d033c8311be04a9))
* **#1116:** link cards milk dashboard in mobile ([a52742b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a52742b959b64f4db45d3e6f72022b401f330267)), closes [#1116](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1116)
* **#1288:** adjustment key item selection ([fed84a4](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/fed84a4ee1f32a4aed07de584ddd9a10dce5f5fe)), closes [#1288](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1288)
* **#1288:** adjustment select organization ([2dbbc9c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2dbbc9c4234ad1a408da856e1c2deeccc814f993)), closes [#1288](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1288) [#395](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/395)
* **documentos#1116:** milk data card in mobile ([d9a35c5](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d9a35c5b248186642b70caa865fac72e694d1220)), closes [documentos#1116](http://gitlab.meta.com.br/documentos/issues/1116)
* **documentos#1116:** tambo for mobile ([f4d650f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f4d650f18602702f4cfccdf32758336da2041ca0)), closes [documentos#1116](http://gitlab.meta.com.br/documentos/issues/1116)
* **documentos#1240:** address component in mobile ([3bc90f0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3bc90f07c5eff93ed83d9f774259cfdedec7f6da)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** init avatar input ([d018ec3](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d018ec371fb33f01a39e91f29bbc853227b6ff34)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** style tweaks ([861ea3e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/861ea3ef96a33c80d02370d8b230f056b4bd9387)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1290:** maxLength of InputCode set to 12 ([60c0849](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/60c084906a7300bccc03f6326c375916cd780d4a)), closes [documentos#1290](http://gitlab.meta.com.br/documentos/issues/1290)
* **documentos#1292:** created date tabs component ([e360ec8](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e360ec8da3b744d7ca6f17f2aaac23a5d1fcd5df)), closes [documentos#1292](http://gitlab.meta.com.br/documentos/issues/1292)
* **documentos#1323:** removes console.log ([005a2c1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/005a2c1c08ac6cb7649419c54168a5b8d8c0f629)), closes [documentos#1323](http://gitlab.meta.com.br/documentos/issues/1323)
* **documentos#1395:** touchable button expands full width now ([9f451ad](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9f451ade95a4d11403059b615ea550535c4fb20b)), closes [documentos#1395](http://gitlab.meta.com.br/documentos/issues/1395)
* **documentos#1400:** accept terms modal mobile ([7244e0d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7244e0d161d81982fbecaa13a08148f342a94527)), closes [documentos#1400](http://gitlab.meta.com.br/documentos/issues/1400)
* **documentos#1400:** removes flex: 1 in button ([b326fa4](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b326fa4d5a405e29a89f823820eca620a54a9ee7)), closes [documentos#1400](http://gitlab.meta.com.br/documentos/issues/1400)
* **documentos#1409:** fixes label in tooltip ([8fab327](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8fab327b94b831d0bab5f6b985f5c59efe797057))
* **documentos#1436:** changes in commercialization ([f4edf6b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f4edf6b977ef4c7a8fe514f844097ab2703e1572)), closes [documentos#1436](http://gitlab.meta.com.br/documentos/issues/1436)
* **documentos#1438:** ajustes mapa ([97b7c63](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/97b7c638d12a5abd12cffe38cc78553d4457b6f8)), closes [documentos#1438](http://gitlab.meta.com.br/documentos/issues/1438)
* **documentos#1441:** barter fixes ([8575fa4](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8575fa4c8a98e6b61e167c14c4ef19e7a6a2f91b)), closes [documentos#1441](http://gitlab.meta.com.br/documentos/issues/1441)
* **documentos#1554:** alterada exibição de matricula ([a21246b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a21246b5122acd568b4b0168d61321bb485e65d8)), closes [documentos#1554](http://gitlab.meta.com.br/documentos/issues/1554)
* added style prop to mobile input quantity ([d1758b3](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d1758b3c424bc38a9a7ca291e285c33f7fcb0e42))
* fixed input select ([3463a24](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3463a2410020e27157cbd6867797d46755255d07))
* **documentos#885:** changed field screen ([4299f89](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4299f898298c9641e4e63098dfdcf85a5633c11d)), closes [documentos#885](http://gitlab.meta.com.br/documentos/issues/885)





# [0.12.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.12.0-alpha.1...v0.12.0) (2021-03-23)


### Bug Fixes

* **#1311:** fix change property modal ([0b5d5d9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0b5d5d97a29f47cdd82f475eae3c0d59d5fd4f21)), closes [#400](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/400)
* **documentos#1458:** fix quantityinput ([3f18d92](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3f18d92595a1a84b2d33f9bf838162b5a838d63a))
* **documentos#1458:** fix typo in input select ([0235284](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/023528495cc2085e29f60d42737be0ee86ae19ce))
* **fix layout when keyboard is open:** layout with keyboard ([ab61892](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ab61892bd4a866cd3d5a4c2ee1e6b01474159acf))
* fixed mobile color palette description values ([4d9654a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4d9654ac03cd5f29bc0b7e7ad0ba6eefdd2f87eb))


### Features

* addded pickerProps prop ([5fd2c5d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/5fd2c5dd60875824142ba22bb273341abfe78ffe))
* resolve [#1504](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1504) ([33e9146](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/33e9146cba03ef22188d428f1d033c8311be04a9))
* **#1116:** link cards milk dashboard in mobile ([a52742b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a52742b959b64f4db45d3e6f72022b401f330267)), closes [#1116](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1116)
* **#1288:** adjustment key item selection ([fed84a4](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/fed84a4ee1f32a4aed07de584ddd9a10dce5f5fe)), closes [#1288](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1288)
* **#1288:** adjustment select organization ([2dbbc9c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2dbbc9c4234ad1a408da856e1c2deeccc814f993)), closes [#1288](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1288) [#395](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/395)
* **documentos#1116:** milk data card in mobile ([d9a35c5](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d9a35c5b248186642b70caa865fac72e694d1220)), closes [documentos#1116](http://gitlab.meta.com.br/documentos/issues/1116)
* **documentos#1116:** tambo for mobile ([f4d650f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f4d650f18602702f4cfccdf32758336da2041ca0)), closes [documentos#1116](http://gitlab.meta.com.br/documentos/issues/1116)
* **documentos#1240:** address component in mobile ([3bc90f0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3bc90f07c5eff93ed83d9f774259cfdedec7f6da)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** init avatar input ([d018ec3](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d018ec371fb33f01a39e91f29bbc853227b6ff34)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** style tweaks ([861ea3e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/861ea3ef96a33c80d02370d8b230f056b4bd9387)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1290:** maxLength of InputCode set to 12 ([60c0849](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/60c084906a7300bccc03f6326c375916cd780d4a)), closes [documentos#1290](http://gitlab.meta.com.br/documentos/issues/1290)
* **documentos#1292:** created date tabs component ([e360ec8](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e360ec8da3b744d7ca6f17f2aaac23a5d1fcd5df)), closes [documentos#1292](http://gitlab.meta.com.br/documentos/issues/1292)
* **documentos#1323:** removes console.log ([005a2c1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/005a2c1c08ac6cb7649419c54168a5b8d8c0f629)), closes [documentos#1323](http://gitlab.meta.com.br/documentos/issues/1323)
* **documentos#1395:** touchable button expands full width now ([9f451ad](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9f451ade95a4d11403059b615ea550535c4fb20b)), closes [documentos#1395](http://gitlab.meta.com.br/documentos/issues/1395)
* **documentos#1400:** accept terms modal mobile ([7244e0d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7244e0d161d81982fbecaa13a08148f342a94527)), closes [documentos#1400](http://gitlab.meta.com.br/documentos/issues/1400)
* **documentos#1400:** removes flex: 1 in button ([b326fa4](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b326fa4d5a405e29a89f823820eca620a54a9ee7)), closes [documentos#1400](http://gitlab.meta.com.br/documentos/issues/1400)
* **documentos#1409:** fixes label in tooltip ([8fab327](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8fab327b94b831d0bab5f6b985f5c59efe797057))
* **documentos#1436:** changes in commercialization ([f4edf6b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f4edf6b977ef4c7a8fe514f844097ab2703e1572)), closes [documentos#1436](http://gitlab.meta.com.br/documentos/issues/1436)
* **documentos#1438:** ajustes mapa ([97b7c63](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/97b7c638d12a5abd12cffe38cc78553d4457b6f8)), closes [documentos#1438](http://gitlab.meta.com.br/documentos/issues/1438)
* **documentos#1441:** barter fixes ([8575fa4](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8575fa4c8a98e6b61e167c14c4ef19e7a6a2f91b)), closes [documentos#1441](http://gitlab.meta.com.br/documentos/issues/1441)
* **documentos#1554:** alterada exibição de matricula ([a21246b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a21246b5122acd568b4b0168d61321bb485e65d8)), closes [documentos#1554](http://gitlab.meta.com.br/documentos/issues/1554)
* added style prop to mobile input quantity ([d1758b3](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d1758b3c424bc38a9a7ca291e285c33f7fcb0e42))
* fixed input select ([3463a24](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3463a2410020e27157cbd6867797d46755255d07))
* **documentos#885:** changed field screen ([4299f89](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4299f898298c9641e4e63098dfdcf85a5633c11d)), closes [documentos#885](http://gitlab.meta.com.br/documentos/issues/885)





# [0.12.0-alpha.1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.12.0-alpha.0...v0.12.0-alpha.1) (2021-02-22)


### Bug Fixes

* **#1173:** resolve problems of rebase ([9a26af1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9a26af16f3b717545a0dce0d52275b5b820c2056)), closes [#1173](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1173)
* **docs:** fixed management styles ([1aa4823](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1aa4823d842b8732a888cf22c9deb7b667dcfa7a))
* **documentos#1209:** fixed firebase lifecycle ([831d393](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/831d3934bad4dd29e794559504a2c3b87dd63b20))
* **maps:** fixed PinMarker ([4783c8a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4783c8afe33fbb81ada0444c8e3761ff45277b33))
* fixed detaches components, input select and text field ([598ddfa](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/598ddfa97aa2d6a8f3205c367f863742595b6c8d))
* fixed input select mobile ([58db134](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/58db13474c860b8566b3f0eca0d33298a9e304b6))


### Features

* **#1173:** create onchange checkboxGroup ([79f1e28](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/79f1e286ed67a77ea3b3d167bc427180d424f6cd)), closes [#1173](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1173)
* **#1173:** create option select all ([e204004](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e2040046996e98718093c0cb37f9a65ac2369f65)), closes [#1173](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1173)
* **#1173:** create selected all mobile input select styled ([ff9de86](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ff9de8692f816c2dd3fa6f4c8c99d4d8516a55ea)), closes [#1173](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1173)
* **1220:** ajuste paleta de cores ([1af336d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1af336d4b9c5def20ca58cc26ac6a43ad218f45a))
* **documentos#535:** finished home screen for mobile ([b595eee](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b595eeed9efc2c6371bb04276a22abfd74a1d9db)), closes [documentos#535](http://gitlab.meta.com.br/documentos/issues/535)
* **documentos#947:** created mobile input quantity ([204e672](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/204e6722f15b73440cb45d95e9d49f3b84ba6d81)), closes [documentos#947](http://gitlab.meta.com.br/documentos/issues/947)
* **landscape:** opening chart on landscape left ([636fe6d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/636fe6d36a6aebb6f812ce00fcbdf6889cea4cc5))
* added new props to mobile confirm modal ([bc59ad0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/bc59ad0c0162f1d65e58c62da3c4980435b1ec88))
* added read less option to read more ([e75ded3](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e75ded37f45d87444202a9f556ca663f3363085e))
* **documentos#987:** adapt badge to accept other children than string ([c209f19](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c209f19db0ee2d2ed59ea686a17ca516d3897887)), closes [documentos#987](http://gitlab.meta.com.br/documentos/issues/987)





# [0.12.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.11.0...v0.12.0-alpha.0) (2021-02-12)

**Note:** Version bump only for package @smartcoop/mobile-components





# [0.11.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.11.0-alpha.0...v0.11.0) (2021-02-12)

**Note:** Version bump only for package @smartcoop/mobile-components





# [0.11.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.10.0...v0.11.0-alpha.0) (2021-02-05)

**Note:** Version bump only for package @smartcoop/mobile-components





# [0.10.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.10.0-alpha.0...v0.10.0) (2021-02-01)


### Bug Fixes

* **release:** fixed docs ([c201b35](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c201b35db9682fd1dbc5e65cd3392622ba871b3b))


### Features

* **#1004:** adjustment and create filters for technical visits ([53623e5](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/53623e5c296bcfecc245a9820631d8303645abb5)), closes [#1004](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1004)
* **#981:** adjustment maps in mobile ([ab585b8](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ab585b8c320550d0c88d18864f61dda927e01f9d)), closes [#981](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/981)
* **documentos#1006:** created technical portfolio screen ([8f964b7](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8f964b73604e5520e9b956cca6855770abc6a89c)), closes [documentos#1006](http://gitlab.meta.com.br/documentos/issues/1006)
* **documentos#102:** empty state new prop ([9eb05e9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9eb05e920ea2ac0f9703a24f13edcb897b7a6610)), closes [documentos#102](http://gitlab.meta.com.br/documentos/issues/102)
* **documentos#102:** new props emptystate ([382a2c8](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/382a2c876d657bb2b9fb6228f42b0860ab53ed97)), closes [documentos#102](http://gitlab.meta.com.br/documentos/issues/102)
* **documentos#1080 documentos#1081:** added number format ([ca0f69f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ca0f69f87e8e004f9de34d83a046931f5a21e1e0)), closes [documentos#1080](http://gitlab.meta.com.br/documentos/issues/1080) [documentos#1081](http://gitlab.meta.com.br/documentos/issues/1081)
* **documentos#987:** creates filter form milk quality ([fd24417](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/fd2441790211b8bc499e1002ae4afa276df42777)), closes [documentos#987](http://gitlab.meta.com.br/documentos/issues/987)
* **documentos#987:** integrates lists ([9ef2d40](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9ef2d4024888ab1d86d9ecc45e0e79274378fb10)), closes [documentos#987](http://gitlab.meta.com.br/documentos/issues/987)
* **documentos#987:** mock milk delivery item ([6326e26](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/6326e263433c862e1dea22e5c7b4a18348f36108)), closes [documentos#987](http://gitlab.meta.com.br/documentos/issues/987)
* wip ([e983a17](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e983a176fc1aebd08f9548307e6f8ea513384e25))





# [0.10.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.9.0...v0.10.0-alpha.0) (2021-01-19)

**Note:** Version bump only for package @smartcoop/mobile-components





# [0.9.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.8.2...v0.9.0) (2021-01-19)


### Bug Fixes

* fixed proptypes ([0089726](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0089726aa4e6ef389eecc513075f7a3e3667cc6c))
* **documentos#869:** also fixed documentos[#942](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/942) ([35481fb](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/35481fb256a5fc5a8b9a659b8acc86befbd04c3f))


### Features

* **documentos#299:** trackmarks as a prop ([f07ad50](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f07ad50ed3412195eaf3f6d5751f9d860e97bb36)), closes [documentos#299](http://gitlab.meta.com.br/documentos/issues/299)
* **documentos#299:** usecallbacks ([d61bab0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d61bab00126f6e33f19d96fe94d36c75b2988dd1)), closes [documentos#299](http://gitlab.meta.com.br/documentos/issues/299)
* added sales orders forms ([b1e3fe8](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b1e3fe88414dba234ec6c9deff8d6cb7c3616f5a))
* detached radio button mobile ([eef55b4](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/eef55b41fe798030557306736c3cecff8564766e))
* **documentos#299:** adds anchor props to pinMarker ([5597c33](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/5597c334527fca594d7268ddef0f2bf67527c1cd)), closes [documentos#299](http://gitlab.meta.com.br/documentos/issues/299)
* **documentos#299:** adds barebones version of slider in native ([52d5f16](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/52d5f168735b5876070b6be3e295676fd524f424)), closes [documentos#299](http://gitlab.meta.com.br/documentos/issues/299)
* **documentos#299:** adds label to slider in mobile ([f80fbb9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f80fbb93a436f6f532bfac01fbd886eb78dc6ed9)), closes [documentos#299](http://gitlab.meta.com.br/documentos/issues/299)
* merge Develop ([4a43e2a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4a43e2a79e976c5459355a3dd2e0892e8c081e24))
* **documentos#507:** adds distance to machine item ([e82cecd](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e82cecd521aa5760868d6660ddfacbff7e035be2)), closes [documentos#507](http://gitlab.meta.com.br/documentos/issues/507)
* **documentos#507:** semantic class to item ([4e2827e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4e2827e2b4c9b7c6e65eeb0325e6112e32fe7523)), closes [documentos#507](http://gitlab.meta.com.br/documentos/issues/507)
* **documentos#820:** added credit limit in mobile ([4104279](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4104279ffaa7567830875765b1efdc3bddaa54dc)), closes [documentos#820](http://gitlab.meta.com.br/documentos/issues/820)
* **documentos#836:** dynamic icon ([585a128](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/585a128949f0dc2866f446963da30d5b3bc8ac9a)), closes [documentos#836](http://gitlab.meta.com.br/documentos/issues/836)
* **documentos#836:** finished mobile list ([6cbf8b3](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/6cbf8b38ee2ef54367d0503f607dc38386ca56e8)), closes [documentos#836](http://gitlab.meta.com.br/documentos/issues/836)





## [0.8.2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.8.1...v0.8.2) (2021-01-06)


### Features

* **documentos#506:** thumbnail images in mobile ([94004dc](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/94004dc274944191a891989f368b9d888f9ff3c1)), closes [documentos#506](http://gitlab.meta.com.br/documentos/issues/506)





## [0.8.1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.9.0-alpha.0...v0.8.1) (2021-01-06)

**Note:** Version bump only for package @smartcoop/mobile-components





# [0.9.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.8.0...v0.9.0-alpha.0) (2021-01-05)

**Note:** Version bump only for package @smartcoop/mobile-components





# [0.8.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.8.0-alpha.0...v0.8.0) (2021-01-05)


### Bug Fixes

* **#640:** adjustment mdx in field Item ([635b98f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/635b98f1798357877bf5f037bfba0b0dee8db7b2)), closes [#640](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/640)
* **documentos#53:** removed auto sorting on add polygon point ([0f42fff](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0f42fff9cc7f4beb2d49977f43e18287c6021ab9)), closes [documentos#53](http://gitlab.meta.com.br/documentos/issues/53)
* **documentos#635:** fixed input height ([0678b66](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0678b66fb95e0674059de1801d893bb3abb36aae))


### Features

* **#541:** add new component mobile ([cda1db6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/cda1db69bba314d3e012051d6b004b07ffec2b89)), closes [#541](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/541) [#176](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/176)
* **#541:** adjustment name documents ([130ad41](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/130ad41658624dfc697bf103ed32cf839f881ec9)), closes [#541](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/541) [#176](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/176)
* **#629:** add disabled radio group in mobile ([06ded78](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/06ded7890922ed2f8bd98fff64ff85cddc4e462e)), closes [#629](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/629)
* **#640:** add new component management in mobile ([a5c4f26](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a5c4f2688eaec0482c5a7386e03b7265dc9ab4b0)), closes [#640](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/640) [#182](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/182)
* **#640:** adjustment component ([7294720](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/729472076a3a95c0db818d52c03234f6e778f216)), closes [#640](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/640) [#182](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/182)
* **445:** adicionada paleta de cores ([7e25bc2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7e25bc2d7327aeee86d534ca51ecc820b7dc6bca))
* **documentos#491:** changed mobile badge for better stylization ([7cf7589](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7cf7589c881a3dfcc3a1e6d39b33f8da2ab16fa1)), closes [documentos#491](http://gitlab.meta.com.br/documentos/issues/491)
* **documentos#506:** creates mchine item mobiel ([8f88714](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8f8871421b09f5d8d801ef14c5a063c99a8e423d)), closes [documentos#506](http://gitlab.meta.com.br/documentos/issues/506)
* **documentos#506:** empty state mobile icon validation ([b7cacfd](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b7cacfd732c10f16dff7894bea9a91ecc3d39b07)), closes [documentos#506](http://gitlab.meta.com.br/documentos/issues/506)
* **documentos#506:** new prop to radioGroup ([c88eb1d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c88eb1d1f1b698b447f7836dc34089349cf17d11)), closes [documentos#506](http://gitlab.meta.com.br/documentos/issues/506)
* **documentos#507:** style tweaks ([7bd95ef](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7bd95ef0238689ccbc3ebcea2c54caff7c125bef)), closes [documentos#507](http://gitlab.meta.com.br/documentos/issues/507)
* **documentos#507:** tweaks in item ([eed1403](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/eed1403b9de34c989b72c8b2f795b2dae08424d9)), closes [documentos#507](http://gitlab.meta.com.br/documentos/issues/507)
* **documentos#51:** wip ([45c81cc](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/45c81cc46f96334ae37160677e1babe63c5de358)), closes [documentos#51](http://gitlab.meta.com.br/documentos/issues/51)
* **documentos#687:** created Chart component for mobile ([b0fba2b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b0fba2b4f1be4054ec927ed5dde77178bef16803)), closes [documentos#687](http://gitlab.meta.com.br/documentos/issues/687)
* **documentos#688:** added chart to web ([84a8fcb](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/84a8fcb47c4d7a12a798460379ecc025c71806fb)), closes [documentos#688](http://gitlab.meta.com.br/documentos/issues/688)
* **documentos#688:** created InputDateRange for mobile ([ef71bf4](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ef71bf437ac74d21d6ac335a9496e4748cf2601a)), closes [documentos#688](http://gitlab.meta.com.br/documentos/issues/688)
* **documentos#71:** added mobile securities movement screen ([7bb7fe1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7bb7fe121a83cfb4c448e2d9d845e4204224167b)), closes [documentos#71](http://gitlab.meta.com.br/documentos/issues/71)
* **documentos#79:** finished charts ([2b089cd](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2b089cdf380708c35b8fe4a47f65fd8e20b7523f)), closes [documentos#79](http://gitlab.meta.com.br/documentos/issues/79)
* **documentos#79:** merged with develop ([7538e6f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7538e6fd5b6870ade8c87eb3c9f62e1295990fb3)), closes [documentos#79](http://gitlab.meta.com.br/documentos/issues/79)





# [0.8.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.7.0...v0.8.0-alpha.0) (2020-12-11)

**Note:** Version bump only for package @smartcoop/mobile-components





# [0.7.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.7.0-alpha.0...v0.7.0) (2020-12-11)


### Bug Fixes

* **#361:** ajust button size ([3edbab6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3edbab6bb0046e026399d02e0dc0e4dd2d6d7cb5)), closes [#361](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/361) [#143](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/143)
* **#407:** adjustment input select styled mobile ([f6ea66a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f6ea66ae4da43abf54fc49a6188baf913df60040)), closes [#407](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/407) [#164](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/164)
* **#513:** fix a bug show select button ([70eae39](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/70eae39d8de172bb2bd6ea9197b37ea9c87c71c2)), closes [#172](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/172)
* **documentos#103:** fix missing unit types in warning modal ([75abe5e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/75abe5e005bbb5f942b50fd6db30c853955e7be9))
* **documentos#436:** added scroll view around modal content ([270b786](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/270b786b547e1ef08b62e390f1bc9b3c8620237e)), closes [documentos#436](http://gitlab.meta.com.br/documentos/issues/436)
* **documentos#436:** fixed scroll and width ([a59569e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a59569e9ef6e7e319d706c5445ad5d39a58c66d5))
* **documentos328:** focusing input on label press ([f7d994d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f7d994d88f450e25dd6609b2c505b654f76cef2f))
* **doucmentos#435:** fixes marker in android maps ([76762b3](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/76762b3c3353c6f0df23df4aa1f714ab1de47087))
* **inputUnity:** fixed height ([42c9c30](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/42c9c3044b00cfc84dc12fb811fcb7b0790547fe))


### Features

* **#407:** add crop in list field mobile ([4abacb8](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4abacb89bfbcf12a03bd4b7ccfdd1e1fb9b099ff)), closes [#407](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/407) [#159](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/159)
* **#407:** adjustment filter fields mobile ([2523e03](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2523e03081c0290f5994dc6df4e08ecfc06b81bb)), closes [#407](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/407) [#164](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/164)
* **#407:** adjustment show empty state ([8bc69c2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8bc69c299a9e6f178632b0aca79a81268767a881)), closes [#407](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/407) [#164](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/164)
* **#412:** use modal confirm mobile ([8e692cb](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8e692cb524d2a1f3faf6b00efcda546ca3d27b5a)), closes [#412](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/412) [#141](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/141)
* **documentos-424:** add ReadMore component ([98ea146](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/98ea146645a1fc9e59f5f78fbe5f84cb1a86c6b3))
* **documentos#308:** changed mobile navigation ([6a06eee](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/6a06eee9034387bee975cf9eab8c20bfe960f2df)), closes [documentos#308](http://gitlab.meta.com.br/documentos/issues/308)
* **documentos#360:** created delivery locations ([295d06d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/295d06d08a8bd875612743c0a181eff07350ef85)), closes [documentos#360](http://gitlab.meta.com.br/documentos/issues/360)
* **documentos#385:** adds empty state fields ([34498ba](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/34498ba9e3f4055595fbbaacb51080c5f4e6e470)), closes [documentos#385](http://gitlab.meta.com.br/documentos/issues/385)
* **documentos#393:** social network finished on mobile devices ([e838996](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e838996f44d0b26d857c47b062b6b1ecf0bdb809)), closes [documentos#393](http://gitlab.meta.com.br/documentos/issues/393)
* **documentos#400:** added empty states ([ec28dc9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ec28dc91cc111d229e4d0a981c7acfaf378b984c)), closes [documentos#400](http://gitlab.meta.com.br/documentos/issues/400)
* **documentos#402:** editing growing season ([321da9c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/321da9cb75b830d048c43afa491b4f2911e58b03)), closes [documentos#402](http://gitlab.meta.com.br/documentos/issues/402)
* **documentos#424:** adjust ReadMore component ([bbc0aed](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/bbc0aed02c2e87b575eaa8e0b7dfdddbad944df3)), closes [documentos#424](http://gitlab.meta.com.br/documentos/issues/424)
* **documentos#426:** integrated with List component ([d35f7b3](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d35f7b33291afd1c4ae1f806b45bf3f52414315e)), closes [documentos#426](http://gitlab.meta.com.br/documentos/issues/426)
* **documentos#451:** created feed on mobile ([90668fe](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/90668fe9e1f1a0924d9e36729b427167cff8ba42)), closes [documentos#451](http://gitlab.meta.com.br/documentos/issues/451)
* **documentos#451:** created publish screen for mobile ([626b575](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/626b575f4021c206b0900c884d2f943d5238c47f)), closes [documentos#451](http://gitlab.meta.com.br/documentos/issues/451)
* **documentos#459:** created social and notifications screens ([98e93e7](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/98e93e7ae3c6e4c8f6cda5ace7518ee36cc86495)), closes [documentos#459](http://gitlab.meta.com.br/documentos/issues/459)
* **documentos#459:** created social network screen ([862b5e9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/862b5e9024c0da5c96e54f83583c7f8a6bb9e926)), closes [documentos#459](http://gitlab.meta.com.br/documentos/issues/459)





# [0.7.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.6.0...v0.7.0-alpha.0) (2020-11-27)

**Note:** Version bump only for package @smartcoop/mobile-components





# [0.6.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.5.3...v0.6.0) (2020-11-27)


### Bug Fixes

* **buttons:** fixed prop-types ([7d041bb](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7d041bb6508a05732d2d8ed394beb3a631ff2417))
* **docs:** removed input date for mobile ([9197bbb](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9197bbb095849d62c8be9f291a8bbd4efb8f40a6))
* **documentos#107:** fix missing units in styles ([92ece7d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/92ece7d2585a59c6dc02f2a37cb34f849681531c))
* **documentos#16:** refactoring input select ([c995424](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c995424700e0f1357cd7d0d22965f613f1c051d2)), closes [documentos#16](http://gitlab.meta.com.br/documentos/issues/16)
* **documentos#16:** removes div from mobile ([06b624f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/06b624f8f15b1a2cbd2fafef252eaef17c8a6522)), closes [documentos#16](http://gitlab.meta.com.br/documentos/issues/16)
* **documentos#319:** adjusts padding in choose property modal ([3910e08](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3910e080da33283a08b615e68246ae9a3c63b3ab)), closes [documentos#319](http://gitlab.meta.com.br/documentos/issues/319)
* **documentos#323:** fixed input select height ([2725e5d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2725e5dac87424872858878825d0ddf24354e278))
* **documentos#324:** added require and label styles ([f79aa9f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f79aa9f65f98c6e411cc5d09a2c956d5c25a8bce)), closes [documentos#324](http://gitlab.meta.com.br/documentos/issues/324)
* **documentos#324:** removed field property from schema for mobile ([b137514](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b13751462dd0a257447cc89b0c1edd7596214c30)), closes [documentos#324](http://gitlab.meta.com.br/documentos/issues/324)
* **documentos#389:** fixed input select focus ([7e66b7c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7e66b7cba2380cc4891fac7e0114fa617bf6cffc))


### Features

* **#282:** adjust button ([7255302](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7255302fc67a671d9f537a2db56ea9279493d734)), closes [#282](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/282) [#103](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/103)
* **#282:** adjust on the label to check if it exists ([6f59d12](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/6f59d1230ef73e6d117775f2f4c675d30ca103f2)), closes [#282](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/282) [#103](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/103)
* **#370:** add new component ([9ac6254](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9ac6254c35fd83dcb03f1fd0015b2520cdfd5b17)), closes [#370](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/370) [#118](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/118)
* **#383:** add snackbar in growing season register ([2aa60d5](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2aa60d52c4bb96e87053e5bec2816e411cd479f4)), closes [#383](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/383) [#129](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/129)
* **#383:** ajustment import colors ([32d546e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/32d546e4624cec200c797731d8d035e35cf53ab8)), closes [#383](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/383) [#129](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/129)
* **documentos#10:** changed map type to satellite ([227478c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/227478c5496dc22c75f6ddc73707db3dba64611c)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#107:** adds creatable option in inputselect mobile ([3ad4629](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3ad46293a25cf73cea33f3ba59e241cdbb25069c)), closes [documentos#107](http://gitlab.meta.com.br/documentos/issues/107)
* **documentos#107:** adds example of creatable inputselect  in docs ([e780611](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e78061114476d162b84557345417847be8ce7dc4)), closes [documentos#107](http://gitlab.meta.com.br/documentos/issues/107)
* **documentos#107:** adds style prop to inputsearch ([3d2b3df](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3d2b3df363579eb94cb0cb5578ba65bc57799801)), closes [documentos#107](http://gitlab.meta.com.br/documentos/issues/107)
* **documentos#108:** adds accountbalance in mobile too ([4973214](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4973214f94c35ac20d34c5052e957e9a89f6a73e)), closes [documentos#108](http://gitlab.meta.com.br/documentos/issues/108)
* **documentos#108:** adds list in mobile ([40bff51](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/40bff512d1eadd16fac0b511570842b17ead4530)), closes [documentos#108](http://gitlab.meta.com.br/documentos/issues/108)
* **documentos#108:** adds style prop to icon in mobile ([58c8838](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/58c8838f8b7cadfdefb9afde0a0da9363cbc4e93)), closes [documentos#108](http://gitlab.meta.com.br/documentos/issues/108)
* **documentos#156:** changed button ([0747dbb](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0747dbb37e79c548b8db90eb860139c35e93ca06)), closes [documentos#156](http://gitlab.meta.com.br/documentos/issues/156)
* **documentos#16:** adapts styles from web to mobile ([68f139d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/68f139d848fe7acc692948a6f10b4499aa524623)), closes [documentos#16](http://gitlab.meta.com.br/documentos/issues/16)
* **documentos#16:** added current field reset and field details ([7d9e830](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7d9e830c0157bf163712c66c86403eaded7efdf9)), closes [documentos#16](http://gitlab.meta.com.br/documentos/issues/16)
* **documentos#16:** added current item to select option modal\ ([e4f15cd](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e4f15cd975168a064f49be4a8eea07f97575e752)), closes [documentos#16](http://gitlab.meta.com.br/documentos/issues/16)
* **documentos#16:** added onPress to card ([ed5b5ab](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ed5b5ab57b09b370f90ab0296720c2039965cd54)), closes [documentos#16](http://gitlab.meta.com.br/documentos/issues/16)
* **documentos#16:** creates fielditem in mobile ([bc06715](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/bc06715d1be39c257caf15ecf01cfe0ac1d193fb)), closes [documentos#16](http://gitlab.meta.com.br/documentos/issues/16)
* **documentos#16:** creates fieldlist ([7a9074e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7a9074ef32c1fa3e4f3647fce3ff1a242b8f0c5c)), closes [documentos#16](http://gitlab.meta.com.br/documentos/issues/16)
* **documentos#16:** style tweaks ([bb7c6b6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/bb7c6b602a8821e7a3cff17872031503ed23b286)), closes [documentos#16](http://gitlab.meta.com.br/documentos/issues/16)
* **documentos#273:** added picker into date input ([e4cf2c7](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e4cf2c7048e68037962dbe0948ae241c78582114)), closes [documentos#273](http://gitlab.meta.com.br/documentos/issues/273)
* **documentos#286:** drawing field on mobile device ([a057a07](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a057a070ddbf50c4f633c65f2c20f68caea519ff)), closes [documentos#286](http://gitlab.meta.com.br/documentos/issues/286)
* **documentos#311:** created input month year for mobile ([6d470dd](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/6d470dde86e713fbb568af08e3b625e90b880da1)), closes [documentos#311](http://gitlab.meta.com.br/documentos/issues/311)
* **documentos#311:** created month year input for web ([18b0de2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/18b0de23fdb837bf79e06f0cc3d7e1ec24e0b7b6)), closes [documentos#311](http://gitlab.meta.com.br/documentos/issues/311)
* **documentos#370:** craeted polygon to svg for mobile ([20de6d0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/20de6d0b22399c622b9a5ff06d4b3837344e8585)), closes [documentos#370](http://gitlab.meta.com.br/documentos/issues/370)
* **documentos#372:** created snackbar for mobile ([dde3466](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/dde34669dedb5dbd72c806cdab7d68a068c5d076)), closes [documentos#372](http://gitlab.meta.com.br/documentos/issues/372)
* **documentos#382:** adds emptyfield mobile ([83bafc0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/83bafc0886262adc471ec70c3226cb79bd73ec8e)), closes [documentos#382](http://gitlab.meta.com.br/documentos/issues/382)



# [0.6.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.5.0...v0.6.0-alpha.0) (2020-11-10)





# [0.6.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.5.0...v0.6.0-alpha.0) (2020-11-10)

**Note:** Version bump only for package @smartcoop/mobile-components
## [0.5.3](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.5.2...v0.5.3) (2020-11-11)


### Bug Fixes

* **staging:** fixed bugs from release 5 ([aa1cae0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/aa1cae08a05c9653ccaf08d49b3bd630d3c4cd5a))





## [0.5.2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.5.1...v0.5.2) (2020-11-11)


### Bug Fixes

* **staging:** fixed bugs for release 5 ([f9e8b8d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f9e8b8d6a8e0f9154b2b0ab11bcebda699d322d5))





# [0.5.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.5.0-alpha.0...v0.5.0) (2020-11-10)


### Features

* **documentos#28:** adds default location as RS in mobile ([ca962d2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ca962d2f0b7a2aa688df7631a349d44582fd7c4c)), closes [documentos#28](http://gitlab.meta.com.br/documentos/issues/28)
* **documentos#72:** footer validation in mobile modal ([e6e9139](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e6e9139dd40aa21e4c70aa6f13f4e1fc7422dbae)), closes [documentos#72](http://gitlab.meta.com.br/documentos/issues/72)
* **documentos#72:** style tweaks ([5317663](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/53176634b9a248da990997c04ba3ea1f27f53075)), closes [documentos#72](http://gitlab.meta.com.br/documentos/issues/72)
* **documentos#72:** wip: added footer to mobile modal ([12becf1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/12becf1b0e824401c7e031f5369c8e2f4eb32f4d)), closes [documentos#72](http://gitlab.meta.com.br/documentos/issues/72)





# [0.5.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.4.0...v0.5.0-alpha.0) (2020-10-27)

**Note:** Version bump only for package @smartcoop/mobile-components





# [0.4.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.3.0-alpha.0...v0.4.0) (2020-10-27)


### Bug Fixes

* **ci-cd:** using dind-alpine-aws image ([e709138](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e7091388706f250eea9182d3c2119c64281785ae))
* **documentos#10:** centers label of InputText in Android ([7ff5ebb](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7ff5ebbbee14248acce66edb50cc2157bee3eb23)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#10:** removes debug styles ([b651e0a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b651e0a9342943ad06a9b15faa5efb2000acc97c)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#112:** changed mobile inputSelect font size ([50e4805](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/50e480507cf9a3f58084f89718405cb18110dac6)), closes [documentos#112](http://gitlab.meta.com.br/documentos/issues/112) [#30](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/30)
* **documentos#185:** fixed ios layout ([a408f62](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a408f621302bdabcc2f38e6284762a3aa6cbed39))
* **documentos#25:** fixed radiou group component in mobile ([a52080d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a52080d90f39201f8faa0fc9f3b038583f8a9139))
* **documentos#28:** fixed android localization issue ([cc51094](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/cc51094a5a231c0f0ec5b4429a8b7a1e402add43))
* **documentos#28:** fixed checkbox group error ([c3e6bd5](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c3e6bd5c6d228060e65c2032ebe75276e40e5c00))
* **documentos#28:** fixed dynamic form validation ([cac1c66](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/cac1c6690aded2cdd3e5ed5a6bd6360c7f0ed3d5))
* **documentos#28:** fixes stepper colors ([8dfe174](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8dfe17401b6a7ad4ef6fffbc46694dda13431cef))


### Features

* **docuementos#68:** created mobile item selection component ([611f9af](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/611f9af66369cb2b7a38599f941c24b4a0870592)), closes [docuementos#68](http://gitlab.meta.com.br/docuementos/issues/68)
* **documentos#18:** wIP:Created mobile datepicker ([be26632](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/be266326f898865711616fe260d7ecf1a6dafaaa)), closes [documentos#18](http://gitlab.meta.com.br/documentos/issues/18) [#10](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/10)
* **documentos#25:** added disabled style in mobile input ([cf872fb](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/cf872fbd1d327a805fb75b80fccc8c6cb181757b)), closes [documentos#25](http://gitlab.meta.com.br/documentos/issues/25)
* **documentos#28:** added custom icon property to mobile pin marker ([adf94d9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/adf94d91804d045b838717ada1829f47eae8ea07)), closes [documentos#28](http://gitlab.meta.com.br/documentos/issues/28)
* **documentos#28:** wIP: start map in mobile ([f70c2c7](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f70c2c7d6254a7d839434f5c39ac585c51127785)), closes [documentos#28](http://gitlab.meta.com.br/documentos/issues/28)
* **documentos#49:** created mobile checkbox group card ([cd8476b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/cd8476b87068d135f74634fc7290492c134fe23f)), closes [documentos#49](http://gitlab.meta.com.br/documentos/issues/49)
* **documentos#49:** created mobile input icon ([3b14f04](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3b14f047a74f1155caad2f4f9cb2d9e904d3ffd0)), closes [documentos#49](http://gitlab.meta.com.br/documentos/issues/49)
* **documentos#49:** created mobile input search ([9befe57](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9befe57134e2683c404f6d4ccf8e3cef71ad0e0b)), closes [documentos#49](http://gitlab.meta.com.br/documentos/issues/49)
* **documentos#68:** changed item select style ([e810953](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e81095349bfbf806d6a4660ce86a45be29326e77)), closes [documentos#68](http://gitlab.meta.com.br/documentos/issues/68)
* **documentos#87:** changed kebab formatter function ([fc300bd](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/fc300bd1968f469f3c24511dae285bcf0cd5f7f5)), closes [documentos#87](http://gitlab.meta.com.br/documentos/issues/87) [#22](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/22)
* **documentos#87:** created input to add new area to checkbox group ([236e767](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/236e7672add1c471f4a79649400c67c203ce9cac)), closes [documentos#87](http://gitlab.meta.com.br/documentos/issues/87) [#22](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/22)
* **documentos#87:** wIP: Added input to checkbox group ([261cfa1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/261cfa1026f56be4b7ea52d0885a195afbcc3094)), closes [documentos#87](http://gitlab.meta.com.br/documentos/issues/87) [#22](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/22)
* **forms:** added form.validateField(fieldName) ([5effc5c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/5effc5cf477a400358ba2d4c162b560e008c4a15))





# [0.3.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.2.0...v0.3.0-alpha.0) (2020-09-28)

**Note:** Version bump only for package @smartcoop/mobile-components





# [0.2.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.2.0-alpha.0...v0.2.0) (2020-09-28)


### Bug Fixes

* **documentos#10:** header title modal for mobile ([be8c4e0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/be8c4e053858df25b186a264fdcea24c1b3800d1)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#10:** header title modal for mobile ([ecb8dcd](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ecb8dcd917e4a1c715704092e32f2d6fd33079f5)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#10:** scroll view area ([2370ff1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2370ff1560c793985b0f27a1a20501bbffa1e685)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#28:** removed asd ([82bd041](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/82bd0416c911f53e3292eaa07b0fd5215a54f989)), closes [documentos#28](http://gitlab.meta.com.br/documentos/issues/28)
* removed test fragment ([f7a08f2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f7a08f2a991328fa653d8fa41c95f0d1c4d1f530))
* **doc:** css html overflow ([3fddc00](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3fddc00476390b1d344a626fc543eddc96b3f161))
* **documentos#28:** fixed text field default value prop ([88ceaa5](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/88ceaa5a069eb7a2be2ecc09ce84187076969a92))
* **documentos#33:** new message prop in loader and style fixes ([73bb42b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/73bb42b274c334bf7e939d365dd9ed1252afc365)), closes [documentos#33](http://gitlab.meta.com.br/documentos/issues/33)
* **documentos#60:** fixed radio button touchable width ([c3a64cc](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c3a64cccbf2aa18585caa9dff64e60a537efb056))
* **documentos#86:** fix typo in docs ([4c83223](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4c832230c1fce9ef269c5220953f51da422f916f))
* **forms:** fixed form package ([85e9656](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/85e965662d9ebcd4780f737474ef922e4f120d2a))


### Features

* **documentos#10:** added loader after submit signup form ([f73783e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f73783e063374954f51d4d1193cac555ec5bbb05)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#10:** auto fill forms with user api data ([9909ac6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9909ac6498d857e0f5ecb587d0334fab4b064036)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#10:** craeted loading and error modal ([34bc091](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/34bc091a4766d8b07fda331faf0cbec7c81a7497)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#10:** created web login screen ([34ed647](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/34ed647f1ab16e0c1dc45db8455239335ed6286d)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#10:** created web screens ([496ac64](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/496ac645455e7c7e127a68385e9d8fa8b3b4b9f6)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#22:** adds Checkbox in web ([2fe4946](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2fe4946b7c53b1ca3ae9265f97a956aa64e7e87c)), closes [documentos#22](http://gitlab.meta.com.br/documentos/issues/22) [documentos#22](http://gitlab.meta.com.br/documentos/issues/22)
* **documentos#22:** merge ([c581b54](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c581b54be5ce995e5526e779ea826870b88dd98e)), closes [documentos#22](http://gitlab.meta.com.br/documentos/issues/22)
* **documentos#22:** merge ([824f2d2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/824f2d2f9b25aecb685e3b33ab1b4f445665fc08)), closes [documentos#22](http://gitlab.meta.com.br/documentos/issues/22)
* **documentos#25:** created input unity documentos[#21](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/21) ([290d5c4](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/290d5c4445e4154d376e8d0fdcbd7851f7ad1b27)), closes [documentos#25](http://gitlab.meta.com.br/documentos/issues/25)
* **documentos#28:** added float option to input unity ([2112ac0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2112ac08a60aac4c49b8cb77e634c771e612754e)), closes [documentos#28](http://gitlab.meta.com.br/documentos/issues/28)
* **documentos#28:** added input unity to mobile dynamic form ([99acfc7](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/99acfc723bbad0c6b9ce44fd8d77db83cd38a7bf)), closes [documentos#28](http://gitlab.meta.com.br/documentos/issues/28)
* **documentos#28:** added label to radio group ([4797e3f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4797e3f12e4d432adb66ff5e7edf6767ed9b42fa)), closes [documentos#28](http://gitlab.meta.com.br/documentos/issues/28)
* **documentos#28:** created dynamic input documentos[#67](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/67) ([f6da306](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f6da3060e548ca11be8c75a46e11cb667f421bd5)), closes [documentos#28](http://gitlab.meta.com.br/documentos/issues/28)
* **documentos#28:** created helperText to TextField ([1b22624](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1b22624d77e2e84e3589d454391f39f2bf178984)), closes [documentos#28](http://gitlab.meta.com.br/documentos/issues/28)
* **documentos#28:** created maps for web ([87cc7ec](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/87cc7ec27ec25c531dd748915382294b88f11ba2)), closes [documentos#28](http://gitlab.meta.com.br/documentos/issues/28)
* **documentos#28:** created radiogroup component documentos[#60](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/60) ([476208b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/476208b276e57e381c856bbbbbd23e132eabe70b)), closes [documentos#28](http://gitlab.meta.com.br/documentos/issues/28)
* **documentos#28:** fixed dynamic form ([7b8e28d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7b8e28dade8801531a35eb5556d9dab2fe442e60))
* **documentos#28:** maps working into ios devices ([fc82239](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/fc82239ad99a69c194bd84da56611f2433b2def1)), closes [documentos#28](http://gitlab.meta.com.br/documentos/issues/28)
* **documentos#28:** using useMemo into maps for mobile ([84a8854](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/84a885475de04b870df8d52aa769601b86971f38)), closes [documentos#28](http://gitlab.meta.com.br/documentos/issues/28)
* **documentos#32:** removed watermelon. added redux saga ([cd97772](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/cd977725490d8654afc2dad1a0e7bc3d6b16b4dd)), closes [documentos#32](http://gitlab.meta.com.br/documentos/issues/32)
* **documentos#57:** added Header to modal mobile component ([96c7a79](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/96c7a797b83bd8f820be0999a252aa1ed83f3f7b)), closes [documentos#57](http://gitlab.meta.com.br/documentos/issues/57) [#18](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/18)
* **documentos#57:** created modal component to web and mobile ([c3610ea](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c3610eabd1cecab013f46fe3c116d5f7cea00551)), closes [documentos#57](http://gitlab.meta.com.br/documentos/issues/57) [#18](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/18)
* **documentos#57:** fixed eslint warnings ([eb54073](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/eb54073ccffef79348afd2114d33ce3e08191da5)), closes [#18](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/18)
* **documentos#57:** wIP: Creating modal ([21d3e6f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/21d3e6ff100b0754ae00137e18789aa08ddd78db)), closes [documentos#57](http://gitlab.meta.com.br/documentos/issues/57) [#18](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/18)
* **documentos#57:** wIP: Creating modal ([266d15f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/266d15f1ca1fc87a299655e91d14bbcc1ef36e34)), closes [documentos#57](http://gitlab.meta.com.br/documentos/issues/57) [#18](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/18)
* **documentos#81:** imports datatable to web ([0ae583a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0ae583ad95cb56f98bbe662bb132f0652a1e8a79)), closes [documentos#81](http://gitlab.meta.com.br/documentos/issues/81) [documentos#81](http://gitlab.meta.com.br/documentos/issues/81)
* **documentos#86:** adds stepper ([d3e487d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d3e487daaca6d793b0bdeb82ba5e2c1af252c3dd)), closes [documentos#86](http://gitlab.meta.com.br/documentos/issues/86)
* **documentos#86:** fix docs ([db5792b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/db5792ba3f99fbec85953ab17da6b3735976a1ec))





# [0.2.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.1.0...v0.2.0-alpha.0) (2020-09-13)

**Note:** Version bump only for package @smartcoop/mobile-components





# [0.1.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.1.0-alpha.0...v0.1.0) (2020-09-13)


### Bug Fixes

* **documentos#10:** added width 100% to Form documentos[#51](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/51) ([c74ef22](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c74ef22105d9cb2aa318eb3ffe1b8dba087dda97)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#10:** fix input number atribute order ([0633549](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/06335496cd0e1b2ec24a5da83e8ff5e62bf4785f))
* **documentos#10:** fix input select ([3225ceb](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3225ceb629d8cb7bf84a1760a8a36c284cad4a38))
* **documentos#10:** fixed select \o/ documentos[#30](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/30) ([56ab086](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/56ab08628ed8b0859112cc76f6b2da989098d5a0))
* **documentos#10:** reset form ([61be317](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/61be317be8449425670aad04159fe176d510c97d)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **forms:** fix validations ([7c413a5](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7c413a5c916867af845ec3726b4c3d2deae0ddec))
* **forms:** validating cpf and cnpj ([a16da5b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a16da5b080973e282b84400f5be9ce1764db812a))
* **mobile-components:** dependencies fixed ([abafb37](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/abafb37e2d53ea0d0a25c08b06e2e4b5100d4a9d))


### Features

* **documentos#10:** added createpassword screen ([fed3b11](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/fed3b11499d134768ab2b77e3da0130e1c5ec184)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#10:** added invalid number message into dictionary ([2343511](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2343511b99e5dda6ffa5945c6e11b6d71c8848d0)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10) [#4](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/4)
* **documentos#10:** added navigation ([cdfba1f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/cdfba1f176e4bae75d4dc54477efb3c9d1d2068d)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#10:** applying documentos[#37](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/37) into documentos[#39](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/39) ([c8e043e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c8e043ee97e72ea17b2ebd347852ea2cd999e755)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#10:** created cooperatives screen ([64bbac6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/64bbac6146a39901b28e2c406f50675757eab107)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#10:** created identification screen ([02919dc](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/02919dcd19bd38e94b40047c9766523e53cd7d86)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#10:** created input code documentos[#51](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/51) ([1a9d25d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1a9d25d87fb5371aeab53fe9e8c0b38f4284ccbd)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#10:** created input select for mobile ([f4756b6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f4756b692e534db401e7781a8e584f141b688e1a)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10) [#3](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/3)
* **documentos#10:** created InputCep for mobile and web ([fb3753e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/fb3753e38e6a918c2a11d8b10979bc58f684b612)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10) [#6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/6)
* **documentos#10:** created inputDate web and mobile documentos[#18](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/18) ([fd30c7d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/fd30c7d25403c4fbbef7aeb6ed1d1f6f9ba226f5)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#10:** created InputNumber on mobile and web ([fa73f47](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/fa73f477330111329a6249f7a851e029fc2eede4)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10) [#31](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/31)
* **documentos#10:** created InputPhone on mobile and web ([ca48da4](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ca48da4fe7fa83d3051a9925b73452b5aa4a68ba)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10) [#2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/2)
* **documentos#10:** created select input and perf forms ([e4e83ab](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e4e83ab2bda01613b50076155333d3c7ce487607)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#10:** detach apps screens ([b7d9d50](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b7d9d507c89bd74a89c9addb28fc3e9bdd810e91)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#10:** finished onboard mocked ([b20abfa](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b20abfaf5a51d791934136a03c7c4714e1cda6b3)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#10:** login and signup screens ([e4b9d02](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e4b9d0278f4785aa31928f9ec3bc6e5de4ff98e2)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#10:** mobile inputs styled as material design ([8a9e3cc](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8a9e3cce883733f69190c3eb53ed92aba70e4026)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10) [#7](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/7)
* **documentos#33:** adds loader ([94ba79b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/94ba79b854af259764997a01b23a72d365512e17)), closes [documentos#33](http://gitlab.meta.com.br/documentos/issues/33) [documentos#33](http://gitlab.meta.com.br/documentos/issues/33)
* **documentos#52:** implements datatable in React Native ([be706df](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/be706dfc8f99450ceb9f792328cf85564528b5a3)), closes [documentos#52](http://gitlab.meta.com.br/documentos/issues/52) [documentos#52](http://gitlab.meta.com.br/documentos/issues/52)
* **forms:** @smarcoop/forms finished ([d95dc20](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d95dc20603902124c3b35f531986dcb08e108879))
* **forms:** single validate for web and mobile forms ([9bbeb71](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9bbeb71f1a9c0acac8d6e817e614159eda808a5d))
* **mobile-components:** added input masks ([1e5a7fd](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1e5a7fd25a7faec83814cd330741c420f494a4c1))


### WIP

* **documentos#18:** added date picker component ([17f6e7d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/17f6e7d7588216fc816bbd6f1b9ffe40ed2d7f9e)), closes [documentos#18](http://gitlab.meta.com.br/documentos/issues/18)


### BREAKING CHANGES

* **documentos#18:** yes





# 0.1.0-alpha.0 (2020-08-21)


### Bug Fixes

* **test:** fix coverage report ([f774e6a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f774e6a99b31d858d8ea587632aa13bd6d1b103c))
