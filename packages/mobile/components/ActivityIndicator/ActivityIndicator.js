import React from 'react'

import PropTypes from 'prop-types'

import { Container, LoaderContainer, LoaderStyled } from './styles'

const ActivityIndicator = ({ loading }) => loading && (
  <Container>
    <LoaderContainer>
      <LoaderStyled />
    </LoaderContainer>
  </Container>
)

ActivityIndicator.propTypes = {
  loading: PropTypes.bool
}

ActivityIndicator.defaultProps = {
  loading: true
}

export default ActivityIndicator
