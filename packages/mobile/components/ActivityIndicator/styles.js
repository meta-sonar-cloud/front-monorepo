import hexToRgba from 'hex-to-rgba'
import styled from 'styled-components/native'

import colors from '@smartcoop/styles/colors'

export const Container = styled.View`
  position: absolute;
  left: 0;
  right: 0;
  top: 0;
  bottom: 0;
  align-items: center;
  justify-content: center;
  background-color: ${ colors.shadow };
  z-index: 99999;
`

export const LoaderContainer = styled.View`
  padding: 40px;
  background-color: ${ hexToRgba(colors.black, 0.9) };
  border-radius: 20px;
`

export const LoaderStyled = styled.ActivityIndicator.attrs({
  size: 'large',
  color: colors.secondary
})``
