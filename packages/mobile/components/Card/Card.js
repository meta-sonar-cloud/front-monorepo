import React, { useEffect, useRef, useMemo } from 'react'
import { Animated, Keyboard, TouchableWithoutFeedback } from 'react-native'

import PropTypes from 'prop-types'

import isArray from 'lodash/isArray'
import reduce from 'lodash/reduce'

import { Container, ChildrenContainer } from './styles'

const Card = (props) => {
  const { children, show, style, onPress, childrenStyle, headerRight } = props

  const mounted = useRef(false)
  const animationOpacity = useRef(new Animated.Value(0))

  const realStyle = useMemo(
    () => {
      if (isArray(style)) {
        return reduce(style, (acc, st) => ({ ...acc, ...st }), {})
      }
      return style
    },
    [style]
  )

  useEffect(() => {
    setTimeout(
      () => {
        Animated.timing(animationOpacity.current, {
          toValue: show ? 1 : 0,
          duration: 200,
          useNativeDriver: true
        }).start()
        mounted.current = true
      },
      mounted.current ? 1 : 100
    )
  }, [show])

  return (
    <Container
      style={ { ...realStyle, opacity: animationOpacity.current } }
    >
      <TouchableWithoutFeedback
        onPress={ () => { Keyboard.dismiss(); onPress() } }
        accessible={ false }
      >
        <>
          {headerRight}
          <ChildrenContainer style={ childrenStyle }>
            {children}
          </ChildrenContainer>
        </>
      </TouchableWithoutFeedback>
    </Container>
  )
}

Card.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.arrayOf(PropTypes.element)
  ]),
  show: PropTypes.bool,
  style: PropTypes.any,
  onPress: PropTypes.func,
  childrenStyle: PropTypes.any,
  headerRight: PropTypes.element
}

Card.defaultProps = {
  children: null,
  show: true,
  style: {},
  onPress: () => {},
  childrenStyle: {},
  headerRight: null
}

export default Card
