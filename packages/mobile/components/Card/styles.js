import { Animated } from 'react-native'

import styled from 'styled-components/native'

import { colors } from '@smartcoop/styles'

export const Container = styled(Animated.View)`
  border-radius: 5px;
  background-color: ${ colors.white };
  shadow-color: #000;
  shadow-offset: 0px 1px;
  shadow-opacity: 0.2;
  shadow-radius: 3px;
  elevation: 2;
  margin: 10px 10px 50px;
`

export const ChildrenContainer = styled.View`
  justify-content: flex-start;
`
