import React, { useState, useMemo } from 'react'
import { TouchableOpacity } from 'react-native'

import { eyeClosed, eyeOpened } from '@smartcoop/icons'
import Icon from '@smartcoop/mobile-components/Icon'
import TextField from '@smartcoop/mobile-components/TextField'
import { colors } from '@smartcoop/styles'

const InputPassword = (props) => {
  const [passwordMasked, setPasswordMasked] = useState(true)

  const endAdornment = useMemo(
    () => (
      <TouchableOpacity onPress={ () => setPasswordMasked(old => !old) }>
        <Icon
          icon={ passwordMasked ? eyeClosed : eyeOpened }
          size={ 24 }
          color={ colors.mutedText }
        />
      </TouchableOpacity>
    ),
    [passwordMasked]
  )

  return (
    <TextField
      { ...props }
      secureTextEntry={ passwordMasked }
      textContentType="password"
      autoCompleteType="password"
      endAdornment={ endAdornment }
    />
  )
}

export default InputPassword
