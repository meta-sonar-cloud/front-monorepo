import React, { useMemo } from 'react'
import { View } from 'react-native'

import PropTypes from 'prop-types'

import hourMask from '@smartcoop/forms/masks/hour.mask'
import { clock } from '@smartcoop/icons'
import Icon from '@smartcoop/mobile-components/Icon'
import TextField from '@smartcoop/mobile-components/TextField'

const InputNumber = props => {

  const endAdornment = useMemo(
    () => (
      <View>
        <Icon
          icon={ clock }
          size={ 22 }
        />
      </View>
    ),
    []
  )

  return (
    <TextField
      setMask={ hourMask }
      { ...props }
      keyboardType="number-pad"
      autoCapitalize="none"
      endAdornment={ endAdornment }
    />
  )

}

InputNumber.propTypes = {
  /** Defines mask characters's limit  */
  maxLength: PropTypes.number
}

InputNumber.defaultProps = {
  maxLength: 10
}

export default InputNumber
