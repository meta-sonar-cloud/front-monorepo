import styled from 'styled-components/native'

import { colors } from '@smartcoop/styles'

export const Container = styled.View`
  display: flex;
  flex-direction: column;
  flex: 1;
  position: relative;
  margin-top: 5px;
`

export const ImageContainer = styled.View`
`

export const Image = styled.Image`
  width: 120px;
  height: 120px;
  border-radius: 60px;
`

export const IconButtonContainer = styled.View`
  position: absolute;
  right: 0px;
  bottom: -5px;
  border-radius: 20px;
  justify-content: center;
  align-items: center;
  width: 40px;
  height: 40px;
  background: ${ colors.yellow };
`
