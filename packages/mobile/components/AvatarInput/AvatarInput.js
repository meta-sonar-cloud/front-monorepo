import React, { useEffect, useMemo, useState } from 'react'
import { Platform, PermissionsAndroid , TouchableOpacity } from 'react-native'

import PropTypes from 'prop-types'

import isEmpty from 'lodash/isEmpty'

import { userCircle, camera } from '@smartcoop/icons'
import Icon from '@smartcoop/mobile-components/Icon'
import useFile from '@smartcoop/mobile-containers/hooks/useFile'
import { colors } from '@smartcoop/styles'

import { ImageContainer, Container, IconButtonContainer, Image } from './styles'


const AvatarInput = (props) => {
  const {
    src,
    onChange,
    style
  } = props
  const [selectedFile, setSelectedFile] = useState()

  const {
    selectedFiles,
    handleAdd
  } = useFile([], [])

  const source = useMemo(
    () => selectedFile?.uri || src, [selectedFile, src]
  )

  const renderImage = useMemo(
    () => (isEmpty(source) ?
      <Icon icon={ userCircle } size={ 120 } /> :
      <Image source={ { uri: source } }/>)
    ,[source]
  )

  useEffect(() => {
    const requestCameraPermission = async () => {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.CAMERA,
          {
            title: 'Camera Permission',
            message:
              'Smartcoop needs access to your camera ' +
              'so you can insert your profile image.',
            buttonNeutral: 'Ask Me Later',
            buttonNegative: 'Cancel',
            buttonPositive: 'OK'
          }
        )
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          console.log('You can use the camera')
        } else {
          console.log('Camera permission denied')
        }
      } catch (err) {
        console.warn(err)
      }
    }
    Platform.OS === 'android' && requestCameraPermission()
  }, [])

  useEffect(
    () =>{
      setSelectedFile(selectedFiles[0])
      onChange(selectedFiles[0])
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [selectedFiles]
  )

  return (
    <Container style={ style }>
      <TouchableOpacity onPress={ handleAdd }>
        <ImageContainer>
          {renderImage}
          <IconButtonContainer>
            <Icon
              icon={ camera }
              color={ colors.white }
            />
          </IconButtonContainer>
        </ImageContainer>
      </TouchableOpacity>
    </Container>
  )
}

AvatarInput.propTypes = {
  style: PropTypes.object,
  src: PropTypes.string,
  onChange: PropTypes.func
}

AvatarInput.defaultProps = {
  style: {},
  src: '',
  onChange: () => {}
}

export default AvatarInput
