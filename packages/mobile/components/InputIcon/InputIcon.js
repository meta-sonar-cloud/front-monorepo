import React from 'react'
import { TouchableOpacity } from 'react-native'

import PropTypes from 'prop-types'

import DynamicInput from '@smartcoop/mobile-components/DynamicInput'
import Icon from '@smartcoop/mobile-components/Icon'

const InputIcon = (props) => {
  const { onClick, size, icon, color, style, type, ...rest } = props

  return (
    <DynamicInput
      { ...rest }
      type={ type }
      style={ style }
      endAdornment= { (
        <TouchableOpacity onPress={ () => onClick() } >
          <Icon
            icon={ icon }
            size={ size }
            color={ color }
          />
        </TouchableOpacity>
      ) }
    />
  )
}

InputIcon.propTypes = {
  icon: PropTypes.func.isRequired,
  size: PropTypes.number,
  color: PropTypes.string,
  style: PropTypes.object,
  onClick: PropTypes.func,
  type: PropTypes.string
}

InputIcon.defaultProps = {
  size: 18,
  color: '#151818',
  style: {},
  onClick: () => {},
  type: 'text'
}

export default InputIcon
