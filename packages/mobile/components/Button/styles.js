import { Button } from 'react-native-paper'

import hexToRgba from 'hex-to-rgba'
import styled from 'styled-components/native'

import colors from '@smartcoop/styles/colors'

export const ButtonStyled = styled(Button).attrs(props => ({
  labelStyle: {
    marginTop: props.children ? 8 : 10,
    marginBottom: props.children ? 10 : 10,
    paddingRight: props.children ? 5: 0,
    marginRight: props.children ? 15 : 0,
    ...(props.labelStyle || {})
  },
  compact: false,
  ...props
}))`
  background-color: ${ ({ backgroundColor, mode, disabled }) => {
    let color = colors.primary
    if (backgroundColor) {
      color = backgroundColor
    }
    if (mode === 'outlined' || mode === 'text') {
      color = colors.white
    }
    return hexToRgba(color, disabled ? 0.5 : 1)
  } };
  padding-top: 0;
  padding-bottom: 0;
`

export const Text = styled.Text`
  font-size: 17px;
  color: ${ ({ color, variant }) => {
    if (color) {
      return color
    }
    if (variant === 'outlined' || variant === 'text') {
      return colors.primary
    }
    return colors.white
  } };
  text-transform: none;
  letter-spacing: 0;
  font-weight: bold;
  width: 100%;
  align-items: center;
  justify-content: center;
  flex-direction: row;

`
