import React from 'react'

import { shallow } from 'enzyme'
import toJson from 'enzyme-to-json'

import Button from '../Button'

describe('Button', () => {
  const spyOnClick = jest.fn()

  const wrapper = shallow(
    <Button onPress={ spyOnClick }>
      content
    </Button>
  )

  it('should render correctly', () => {
    expect(toJson(wrapper)).toMatchSnapshot()
  })

  it('should simulate click one time', () => {
    wrapper.simulate('press')
    expect(spyOnClick).toBeCalledTimes(1)
  })
})