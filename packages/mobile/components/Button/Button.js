import React from 'react'

import PropTypes from 'prop-types'

import { ButtonStyled, Text } from './styles'

const Button = (props) => {
  const {
    title,
    color,
    variant,
    icon,
    textStyle,
    ...rest
  } = props

  return (
    <ButtonStyled
      { ...rest }
      mode={ variant }
      icon={ () => icon }
    >
      {title && (
        <Text color={ color } variant={ variant } style={ textStyle }>
          {title}
        </Text>
      )}
    </ButtonStyled>
  )
}

Button.propTypes = {
  onPress: PropTypes.func,
  title: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.element
  ]),
  backgroundColor: PropTypes.string,
  color: PropTypes.string,
  variant: PropTypes.oneOf(['contained', 'outlined', 'text']),
  icon: PropTypes.element,
  textStyle: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.array
  ])
}

Button.defaultProps = {
  onPress: () => {},
  title: '',
  backgroundColor: undefined,
  color: undefined,
  variant: 'contained',
  icon: null,
  textStyle: {}
}

export default Button
