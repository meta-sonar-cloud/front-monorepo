import React, { useMemo, useState, useRef, useEffect, useCallback } from 'react'

import moment from 'moment/moment'
import PropTypes from 'prop-types'

import map from 'lodash/map'

import { colors } from '@smartcoop/styles'
import { generateArrayOfDate } from '@smartcoop/utils/dates'

import { Container, Item, Tab, ScrollViewButtons } from './styles'

const DateTabs = props => {
  const {
    endDate,
    initialDate,
    mode,
    format,
    currentYearFormat,
    onChange,
    viewLastFirst,
    defaultValue,
    horizontal,
    ...rest
  } = props

  const scrollViewRef = useRef(null)

  const [mounted, setMounted] = useState(false)
  const [selectedItem, setSelectedItem] = useState(defaultValue)

  const dates = useMemo(
    () => generateArrayOfDate(initialDate, endDate, mode),
    [endDate, initialDate, mode]
  )

  const getValueIndex = useCallback(
    (value) => dates.findIndex((item) => item.isSame(value, mode)),
    [dates, mode]
  )

  useEffect(
    () => {
      if(!mounted) {
        setMounted(true)
        if (defaultValue) {
          scrollViewRef.current.scrollTo({ x: getValueIndex(defaultValue)*80 })
        }
      }
    },
    [defaultValue, getValueIndex, mounted]
  )

  return (
    <Container>
      <ScrollViewButtons ref={ scrollViewRef } horizontal={ horizontal } { ...rest }>
        {
          map(dates, (date, index) => (
            <Item key={ index }>
              <Tab
                onPress={ () => { setSelectedItem(date); onChange(date) } }
                active={ date.isSame(selectedItem, mode) }
                color={ date.isSame(selectedItem, mode) ? colors.active : colors.blackLight }
                textStyle={ { fontSize: 16, textTransform: 'capitalize' } }
                // Check if the date is in the actual year and change the exibition
                title={ moment().isSame(date, 'year') ? date.format(currentYearFormat || format) : date.format(format) }
              />
            </Item>
          ))
        }

      </ScrollViewButtons>
    </Container>
  )
}

DateTabs.propTypes = {
  initialDate: PropTypes.object.isRequired,
  endDate: PropTypes.object,
  mode: PropTypes.oneOf(['day', 'month', 'year']),
  format: PropTypes.string,
  currentYearFormat: PropTypes.string,
  tabsQuantityToBeShown: PropTypes.number,
  navigateQuantity: PropTypes.number,
  viewLastFirst: PropTypes.bool,
  defaultValue: PropTypes.object,
  horizontal: PropTypes.bool,
  onChange: PropTypes.func
}

DateTabs.defaultProps = {
  endDate: moment(),
  mode: 'month',
  format: 'MMM/YY',
  currentYearFormat: null,
  tabsQuantityToBeShown: 10,
  navigateQuantity: 3,
  viewLastFirst: false,
  defaultValue: null,
  horizontal: false,
  onChange: () => {}
}

export default DateTabs
