import styled from 'styled-components/native'

import Button from '@smartcoop/mobile-components/Button'
import { colors } from '@smartcoop/styles'

export const Container = styled.View`
`

export const Item = styled.View`
`

export const Tab = styled(Button)`
  background-color: ${ props => props.active ? colors.blackLight : colors.backgroundHtml };
  box-shadow: none;
  margin: 0 4px;
`

export const ScrollViewButtons = styled.ScrollView`
  padding: 0;
  margin: 0;
`