import { Animated, Dimensions } from 'react-native'
import { getStatusBarHeight } from 'react-native-iphone-x-helper'

import styled from 'styled-components/native'

import { colors, hexToRgba } from '@smartcoop/styles'

export const SafeAreaContainer = styled(Animated.View)`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  flex: 1;
`

export const ModalContainer = styled.View`
  width: 100%;
  flex: 1;
`

export const ContentView = styled.View`
  width: 100%;
  max-width: 100%;
  max-height: ${ Dimensions.get('window').height * 0.7 }px;
`


export const CenteredView = styled.View`
  flex: 1;
  width: 100%;
  padding-top: ${ getStatusBarHeight() }px;
  justify-content: center;
  align-items: center;
  background-color: ${ hexToRgba(colors.black, 0.5) };
`

export const ModalStyled = styled.View`
  border-width: 0;
  width: 100%;
  flex: 1;
`

export const ModalView = styled.View`
  max-width: 90%;
  background-color: ${ colors.white };
  border-radius: 20px;
  padding: ${ ({ hideHeader }) => hideHeader ? '35px' : '10px' } 25px 20px;
  elevation: 5;
  shadow-color: ${ colors.black };
  shadow-offset: 0px 1px;
  shadow-opacity: 0.2;
  shadow-radius: 3px;
`

export const ModalViewLandscape = styled(ModalView)`
  transform: rotate(90deg);
  justify-content: flex-start;
`

export const HeaderView = styled.View`
  margin: 0;
  padding: 0;
`
