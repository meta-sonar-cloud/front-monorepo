import React, { useCallback } from 'react'
import { Text } from 'react-native'

import PropTypes from 'prop-types'

import I18n, { useT } from '@smartcoop/i18n'
import Button from '@smartcoop/mobile-components/Button'
import Modal from '@smartcoop/mobile-components/Modal'
import colors from '@smartcoop/styles/colors'

import { Container, ButtonGroup, Title } from './styles'

const ConfirmModal = (props) => {
  const {
    open,
    handleClose,
    message,
    id,
    onConfirm,
    onNegative,
    textButtonOnConfirm,
    textButtonOnNegative,
    hideCancel
  } = props

  const t = useT()

  const confirmModal = useCallback(
    () => {
      onConfirm()
      handleClose()
    },
    [handleClose, onConfirm]
  )

  const closeModal = useCallback(
    () => {
      onNegative()
      handleClose()
    },
    [handleClose, onNegative]
  )

  // const textButtonNegative = useMemo(
  //   () =>
  //   ,[t, textButtonOnNegative]
  // )

  return (
    <Modal id={ id } open={ open } hideHeader escape={ false }>
      <Container>
        <Title><I18n>warning</I18n></Title>
        <Text>{message}</Text>
        <ButtonGroup>
          {!hideCancel && (
            <Button
              title={ `${ textButtonOnNegative || t('cancel') }` }
              color={ colors.black }
              variant='outlined'
              onPress={ closeModal }
              style={ { marginRight: '5%', textTransform: 'none' } }
            />
          )}
          <Button
            title={ `${ textButtonOnConfirm || t('confirm') }` }
            color={ colors.white }
            onPress={ confirmModal }
            style={ { backgroundColor: colors.black, textTransform: 'none' } }
          />
        </ButtonGroup>
      </Container>
    </Modal>
  )
}

ConfirmModal.propTypes = {
  id: PropTypes.string.isRequired,
  open: PropTypes.bool.isRequired,
  onNegative: PropTypes.func,
  textButtonOnConfirm: PropTypes.string,
  textButtonOnNegative: PropTypes.string,
  handleClose: PropTypes.func.isRequired,
  message: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.element
  ]),
  onConfirm: PropTypes.func,
  hideCancel: PropTypes.bool
}

ConfirmModal.defaultProps = {
  textButtonOnConfirm: '',
  textButtonOnNegative: '',
  onNegative: () => {},
  message: null,
  onConfirm: () => {},
  hideCancel: false
}

export default ConfirmModal
