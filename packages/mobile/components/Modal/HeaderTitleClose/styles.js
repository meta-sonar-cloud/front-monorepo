import { Subheading } from 'react-native-paper'

import styled from 'styled-components/native'


export const Title = styled(Subheading)`
  font-weight: bold;
  text-align: left;
  margin: 0;
  padding-top: 15px;
`

export const HeaderView = styled.View`
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`

export const IconsContainer = styled.View`
  position: absolute;
  flex-direction: row;
  top: -2px;
  right: -15px;
  justify-content: flex-end;
`

export const IconButton = styled.TouchableOpacity`
  padding-left: 20px;
`

