import React from 'react'

import PropTypes from 'prop-types'

import { close } from '@smartcoop/icons'
import Icon from '@smartcoop/mobile-components/Icon'
import colors from '@smartcoop/styles/colors'

import { HeaderView, Title, IconButton, IconsContainer } from './styles'

const HeaderTitleClose = ({ title, handleClose }) => (
  <HeaderView>
    <Title>
      {title}
    </Title>
    {handleClose && (
      <IconsContainer>
        <IconButton onPress={ handleClose }>
          <Icon icon={ close } size={ 18 } color={ colors.grey } />
        </IconButton>
      </IconsContainer>
    )}
  </HeaderView>
)

HeaderTitleClose.propTypes = {
  handleClose: PropTypes.func,
  title: PropTypes.oneOfType([PropTypes.string, PropTypes.element, PropTypes.object])
}

HeaderTitleClose.defaultProps = {
  title: '',
  handleClose: undefined
}

export default HeaderTitleClose
