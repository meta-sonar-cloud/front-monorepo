import React, { useRef, useCallback, useEffect, useMemo } from 'react'
import { Animated, BackHandler } from 'react-native'
import { ActivityIndicator } from 'react-native-paper'

import PropTypes from 'prop-types'

import { useDialog } from '@smartcoop/dialog'

import HeaderTitleClose from './HeaderTitleClose'
import {
  SafeAreaContainer,
  ModalContainer,
  CenteredView,
  ContentView,
  ModalView,
  ModalViewLandscape,
  HeaderView,
  ModalStyled
} from './styles'

const Modal = (props) => {
  const {
    id,
    children,
    open,
    hideHeader,
    title,
    FooterComponent,
    escape,
    style,
    headerStyle,
    landscape,
    loading
  } = props

  const { removeDialog } = useDialog()

  const animationOpacity = useRef(new Animated.Value(0))

  const handleClose = useCallback(() => removeDialog({ id }), [
    id,
    removeDialog
  ])

  const ModalViewComponent = useMemo(
    () => (landscape ? ModalViewLandscape : ModalView),
    [landscape]
  )

  useEffect(() => {
    Animated.timing(animationOpacity.current, {
      toValue: open ? 1 : 0,
      duration: 150,
      useNativeDriver: true
    }).start()
  }, [open])

  useEffect(() => {
    const handleBackButton = () => {
      if (escape) handleClose()
      return true
    }

    BackHandler.addEventListener('hardwareBackPress', handleBackButton)
    return () => {
      BackHandler.removeEventListener('hardwareBackPress', handleBackButton)
    }
  }, [escape, handleClose])

  return (
    <SafeAreaContainer style={ { opacity: animationOpacity.current } }>
      <ModalContainer>
        <ModalStyled>
          <CenteredView>
            <ModalViewComponent style={ style } hideHeader={ hideHeader }>
              {loading && (
                <ActivityIndicator
                  animating={ loading }
                  style={ { minWidth: '70%', minHeight: '30%' } }
                />
              )}
              {!loading && (
                <>
                  {!hideHeader && (
                    <HeaderView style={ headerStyle }>
                      <HeaderTitleClose
                        title={ title }
                        handleClose={ escape ? handleClose : undefined }
                      />
                    </HeaderView>
                  )}
                  <ContentView>{children}</ContentView>
                  {FooterComponent && <FooterComponent />}
                </>
              )}
            </ModalViewComponent>
          </CenteredView>
        </ModalStyled>
      </ModalContainer>
    </SafeAreaContainer>
  )
}

Modal.propTypes = {
  id: PropTypes.string.isRequired,
  children: PropTypes.element.isRequired,
  hideHeader: PropTypes.bool,
  title: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.element,
    PropTypes.object
  ]),
  open: PropTypes.bool,
  FooterComponent: PropTypes.func,
  escape: PropTypes.bool,
  style: PropTypes.any,
  headerStyle: PropTypes.object,
  landscape: PropTypes.bool,
  loading: PropTypes.bool
}

Modal.defaultProps = {
  hideHeader: false,
  FooterComponent: null,
  title: '',
  open: false,
  escape: true,
  style: {},
  headerStyle: {},
  landscape: false,
  loading: false
}

export default Modal
