import React from 'react'

import PropTypes from 'prop-types'

import TextField from '@smartcoop/mobile-components/TextField'

const InputFloat = ({ decimalScale, ...props }) => (
  <TextField
    { ...props }
    type="money"
    maskOptions={ {
      precision: decimalScale,
      separator: ',',
      delimiter: '',
      unit: '',
      suffixUnit: ''
    } }
    keyboardType="number-pad"
    autoCapitalize="none"
  />
)

InputFloat.propTypes = {
  /** Defines mask characters's limit  */
  maxLength: PropTypes.number,
  decimalScale: PropTypes.number
}

InputFloat.defaultProps = {
  maxLength: 10,
  decimalScale: 2
}

export default InputFloat
