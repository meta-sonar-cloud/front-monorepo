import React from 'react'
import { TouchableOpacity } from 'react-native'

import PropTypes from 'prop-types'

import { verticalDots } from '@smartcoop/icons'
import Icon from '@smartcoop/mobile-components/Icon'
import { colors } from '@smartcoop/styles'


const VerticalDotsIconButton = ({ iconColor, ...props }) => (
  <TouchableOpacity
    { ...props }
  >
    <Icon icon={ verticalDots } size={ 18 } color={ iconColor } />
  </TouchableOpacity>
)

VerticalDotsIconButton.propTypes = {
  iconColor: PropTypes.string
}

VerticalDotsIconButton.defaultProps = {
  iconColor: colors.mutedText
}

export default VerticalDotsIconButton
