import { Subheading, Paragraph } from 'react-native-paper'

import styled from 'styled-components/native'

import { colors } from '@smartcoop/styles'


export const ContainerField = styled.View`
  flex-direction: row;
  width: 100%;
  align-items: center;
`

export const TextGroup = styled.View`
  justify-content: space-between;
  flex-direction: row;
  flex: 1;
`

export const TextLeft = styled.View`
  margin-left: 10px;
  flex-direction: column;
  justify-content: space-between;
  align-self: center;
`

export const TextRight = styled.View`
  flex-direction: column;
  align-items: flex-end;
  justify-content: center;
  align-self: flex-start;
  margin-right: 10px;
`

export const TextBold = styled(Subheading)`
  margin: 0;
  font-weight: 600;
`

export const Text = styled(Paragraph)`
  margin: 2px;
`

export const TemperatureContainer = styled.View`
  justify-content: center;
  align-items: center;
  flex-direction: row;
`

export const TemperatureText = styled(Paragraph)`
  font-weight: 700;
`

export const CropTextGroup = styled.View`
  flex-direction: row;
`

export const PolygonContainer = styled.View`
  width: 60px;
  height: 60px;
  background-color: ${ colors.backgroundHtml };
  justify-content: center;
  align-items: center;
  border-width: 1px;
  border-color: ${ colors.lightGrey };
  border-radius: 4px;
  margin-right: 10px;
`

export const CropText = styled(Paragraph)`
`
