import React from 'react'
import { TouchableOpacity } from 'react-native'

import PropTypes from 'prop-types'

import isEmpty from 'lodash/isEmpty'
import map from 'lodash/map'

import I18n from '@smartcoop/i18n'
import { arrowUpField, arrowDownField } from '@smartcoop/icons'
import Icon from '@smartcoop/mobile-components/Icon'
import PolygonToSvg from '@smartcoop/mobile-components/PolygonToSvg'
import colors from '@smartcoop/styles/colors'


import {
  ContainerField,
  TextGroup,
  TextLeft,
  TextRight,
  TextBold,
  Text,
  TemperatureText,
  TemperatureContainer,
  CropTextGroup,
  CropText,
  PolygonContainer
} from './styles'


const FieldItem = (props) => {
  const {
    field,
    onPress
  } = props

  const { growingSeason, currentWeather } = field

  return (
    <TouchableOpacity
      onPress={ () => onPress({ field }) }
    >
      <ContainerField>

        <PolygonContainer>
          <PolygonToSvg
            shape={ map(field.polygonCoordinates, ([ lat, lng ]) => ({ lat, lng })) }
            width={ 40 }
          />
        </PolygonContainer>

        <TextGroup>
          <TextLeft>
            <TextBold>{field.fieldName}</TextBold>
            {
              !isEmpty(growingSeason) ? (
                <CropTextGroup>
                  <CropText>{growingSeason.crop?.description}</CropText>
                  <CropText>{growingSeason.sowingYear}</CropText>
                </CropTextGroup>
              )
                :
                <Text style={ { fontStyle: 'italic' } }>
                  <I18n>no crop</I18n>
                </Text>
            }
            <Text>{parseFloat(field.area).toFixed(2)} ha</Text>
          </TextLeft>
          <TextRight>
            <TemperatureContainer>
              <Icon style={ { marginRight: 7 } } size={ 14 } icon={ arrowUpField } color={ colors.orange } />
              <Text>{currentWeather.high}°C</Text>
            </TemperatureContainer>
            <TemperatureContainer >
              <Icon style={ { marginRight: 7 } } size={ 14 } icon={ arrowDownField } color={ colors.blue } />
              <Text>{currentWeather.low}°C</Text>
            </TemperatureContainer>
            <TemperatureText style={ { margin: 0 } }>{currentWeather.precipitation} mm</TemperatureText>
          </TextRight>
        </TextGroup>
      </ContainerField>
    </TouchableOpacity>
  )
}

FieldItem.propTypes = {
  field: PropTypes.shape({
    fieldName: PropTypes.string,
    area: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    growingSeason: PropTypes.object,
    currentWeather: PropTypes.object,
    precipitation: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    polygonCoordinates: PropTypes.array,
    plantation: PropTypes.string
  }),
  onPress: PropTypes.func
}

FieldItem.defaultProps = {
  field: {
    fieldName: null,
    growingSeason: {},
    area: 0,
    precipitation: null,
    polygonCoordinates: [],
    plantation: null
  },
  onPress: () => {}
}

export default FieldItem
