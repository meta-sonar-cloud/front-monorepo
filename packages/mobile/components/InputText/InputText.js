import React from 'react'

import TextField from '@smartcoop/mobile-components/TextField'

const InputText = (props) => <TextField { ...props } />

export default InputText
