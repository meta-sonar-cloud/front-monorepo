import React, {
  forwardRef,
  useState,
  useEffect,
  useCallback,
  useMemo,
  useRef,
  useImperativeHandle,
  memo
} from 'react'
import { TouchableWithoutFeedback } from 'react-native'
import { TextInputMask } from 'react-native-masked-text'

import PropTypes from 'prop-types'

import { warning, info } from '@smartcoop/icons'
import Icon from '@smartcoop/mobile-components/Icon'
import colors from '@smartcoop/styles/colors'
import { toNumber } from '@smartcoop/utils/formatters'

import {
  Container,
  HelperContainer,
  InputStyled,
  TextInputStyled,
  HelperText,
  InputInternalContainer,
  HelperTextContainer,
  HelperTextIconContainer,
  AdornmentContainer
} from './styles'


// eslint-disable-next-line no-unused-vars
const InputStyledWithRef = memo(forwardRef((props, ref) => {

  // eslint-disable-next-line react/prop-types
  const { inputRef, name, error } = props

  return (
    <InputStyled
      ref={ inputRef }
      dense
      mode='outlined'
      name={ name }
      error={ !!error }
      { ...props }
    />
  )
}))

const InputMasked = memo((props) => {
  const {
    // eslint-disable-next-line react/prop-types
    type = 'custom', maskOptions, inputRef, focused, mask, ...rest
  } = props

  return (
    <TextInputMask
      { ...rest }
      type={ type }
      options={ {
        mask,
        ...maskOptions
      } }
      customTextInput={ InputStyledWithRef }
      customTextInputProps={ {
        inputRef,
        focused
      } }
    />
  )
})

const TextFieldStyled = forwardRef((props, inputRef) => {
  const {
    numberFormatProps,
    mask,
    value,
    onChange,
    onBlur,
    required,
    disabled,
    error,
    helperText: externalHelperText,
    setMask,
    transformRender,
    zIndex,
    label: externalLabel,
    shrink: externalShrink,
    type,
    disableAutoUnfocus,
    disableFocusOnPress,
    endAdornment,
    startAdornment,
    onChangeFocus,
    onPress,
    onFocus,
    readOnly,
    inputStyle,
    autoHeight,
    maxHeight,
    name,
    // inputHeight: externalInputHeight,
    notEditable,
    ...rest
  } = props

  const ref = useRef(null)

  // const realInputHeight = useMemo(
  //   () => Math.min(maxHeight, rest.multiline ? externalInputHeight * 2.5 : externalInputHeight),
  //   [externalInputHeight, maxHeight, rest.multiline]
  // )

  const [focused, setFocused] = useState(false)
  // const [inputHeight, setInputHeight] = useState(realInputHeight)

  // useEffect(() => {
  //   setInputHeight(realInputHeight)
  // }, [realInputHeight])

  const label = useMemo(
    () => `${ externalLabel }${ (externalLabel && required) ? ' *' : '' }`,
    [externalLabel, required]
  )

  const Input = useMemo(
    () => ((type || mask) ? InputMasked : InputStyledWithRef),
    [type, mask]
  )

  const transformedValue = useMemo(
    () => transformRender(value),
    [value, transformRender]
  )

  const setFocus = useCallback(() => {
    if (!disableFocusOnPress) {
      setFocused(true)
      if (ref.current?.focus) {
        ref.current.focus()
      }
    }
  }, [disableFocusOnPress])

  const removeFocus = useCallback(() => setFocused(false), [])

  const handleBlur = useCallback(() => {
    if (!disableAutoUnfocus) {
      removeFocus()
    }
    onBlur()
  }, [disableAutoUnfocus, onBlur, removeFocus])

  // const onContentSizeChange = useCallback(({ nativeEvent }) => {
  //   if (rest.multiline || autoHeight) {
  //     if (!maxHeight || nativeEvent.contentSize.height <= maxHeight) {
  //       setInputHeight(Math.min(maxHeight, Math.max(nativeEvent.contentSize.height, realInputHeight)))
  //     }
  //   }
  // }, [autoHeight, maxHeight, realInputHeight, rest.multiline])

  const handleChange = useCallback(
    (...params) => {
      if (!notEditable) {
        if (type === 'money') {
          const [val] = params
          onChange((val || '').toString() ? toNumber(val): '')
        } else {
          onChange(...params)
        }
      }
    },
    [notEditable, onChange, type]
  )
  const handleFocus = useCallback(
    (...params) => {
      setFocus(...params)
      onFocus(...params)
    },
    [onFocus, setFocus]
  )

  useEffect(() => {
    onChangeFocus(focused)
  }, [focused, onChangeFocus])

  useImperativeHandle(inputRef, () => ({
    ...ref.current,
    setFocus,
    removeFocus
  }))

  return (
    <TouchableWithoutFeedback onPress={ onPress }>
      <Container zIndex={ zIndex } style={ rest.style }>

        <Input
          { ...rest }
          mask={ mask }
          label={ label }
          value={ transformedValue }
          type={ type }
          onFocus={ handleFocus }
          onPress={ onPress }
          onBlur={ handleBlur }
          onChangeText={ handleChange }
          error={ !!error }
          name={ name }
          // onContentSizeChange={ onContentSizeChange }
          editable={ !disabled && !readOnly }
          required={ required }
          inputRef={ ref }
          focused={ focused }
          disabled={ disabled }
          render={ (inputProps) => (
            <InputInternalContainer>
              {startAdornment && (
                <AdornmentContainer>
                  {startAdornment}
                </AdornmentContainer>
              )}
              <TextInputStyled { ...inputProps } />
              {endAdornment && (
                <AdornmentContainer>
                  {endAdornment}
                </AdornmentContainer>
              )}
            </InputInternalContainer>
          ) }
        />

        {(!!externalHelperText || !!error) && (
          <HelperContainer>

            {!!externalHelperText && (
              <HelperTextContainer>
                <HelperTextIconContainer>
                  <Icon
                    icon={ info }
                    color={ colors.darkGrey }
                    size={ 12 }
                  />
                </HelperTextIconContainer>
                <HelperText type="info">
                  {externalHelperText}
                </HelperText>
              </HelperTextContainer>
            )}

            {!!error && (
              <HelperTextContainer>
                <HelperTextIconContainer>
                  <Icon
                    icon={ warning }
                    color={ colors.error }
                    size={ 12 }
                  />
                </HelperTextIconContainer>
                <HelperText type="error">
                  {error}
                </HelperText>
              </HelperTextContainer>
            )}

          </HelperContainer>
        )}

      </Container>
    </TouchableWithoutFeedback>
  )
})

TextFieldStyled.propTypes = {
  label: PropTypes.string,
  numberFormatProps: PropTypes.object,
  mask: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.array
  ]),
  value: PropTypes.any,
  onChange: PropTypes.func,
  onBlur: PropTypes.func,
  onFocus: PropTypes.func,
  setMask: PropTypes.func,
  required: PropTypes.bool,
  error: PropTypes.string,
  disabled: PropTypes.bool,
  readOnly: PropTypes.bool,
  helperText: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.element,
    PropTypes.object
  ]),
  type: PropTypes.string,
  transformRender: PropTypes.func,
  zIndex: PropTypes.number,
  disableFocusOnPress: PropTypes.bool,
  disableAutoUnfocus: PropTypes.bool,
  startAdornment: PropTypes.oneOfType([
    PropTypes.bool,
    PropTypes.object,
    PropTypes.element
  ]),
  endAdornment: PropTypes.oneOfType([
    PropTypes.bool,
    PropTypes.object,
    PropTypes.element
  ]),
  shrink: PropTypes.bool,
  maskOptions: PropTypes.object,
  inputStyle: PropTypes.object,
  onChangeFocus: PropTypes.func,
  onPress: PropTypes.func,
  inputHeight: PropTypes.number,
  maxHeight: PropTypes.number,
  autoHeight: PropTypes.bool,
  notEditable: PropTypes.bool,
  name: PropTypes.string
}

TextFieldStyled.defaultProps = {
  label: '',
  numberFormatProps: undefined,
  mask: undefined,
  onBlur: () => {},
  onFocus: () => {},
  setMask: undefined,
  required: false,
  error: null,
  disabled: false,
  readOnly: false,
  helperText: null,
  type: undefined,
  value: '',
  onChange: () => {},
  transformRender: v => v,
  zIndex: null,
  disableFocusOnPress: false,
  disableAutoUnfocus: false,
  endAdornment: undefined,
  startAdornment: undefined,
  shrink: false,
  maskOptions: {},
  inputStyle: {},
  onChangeFocus: () => {},
  onPress: () => {},
  inputHeight: 40,
  maxHeight: 200,
  autoHeight: false,
  notEditable: false,
  name: undefined
}

export default TextFieldStyled
