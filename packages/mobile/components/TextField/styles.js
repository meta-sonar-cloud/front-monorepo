import { TextInput, HelperText as RNPHelperText } from 'react-native-paper'

import styled from 'styled-components/native'

import colors from '@smartcoop/styles/colors'

export const Container = styled.View`
  margin: 0 0 15px;
  width: 100%;
  align-items: flex-start;
`

export const InputStyled = styled(TextInput)`
  width: 100%;
  padding: 0;
  margin-top: -5px;
  background-color: ${ ({ disabled }) => {
    if (disabled) {
      return colors.backgroundHtml
    }
    return colors.white
  } };
`

export const HelperContainer = styled.View`
  align-items: flex-start;
  justify-content: center;
`

export const HelperTextContainer = styled.View`
  align-items: center;
  justify-content: center;
  flex-direction: row;
  padding-left: 10px;
`

export const HelperText = styled(RNPHelperText)`
  align-items: center;
  justify-content: center;
  padding-left: 3px;
`

export const HelperTextIconContainer = styled.View`
  justify-content: center;
`

export const AdornmentContainer = styled.View`
  align-items: center;
  justify-content: center;
  padding: 0 10px;
`

export const InputInternalContainer = styled.View`
  flex-direction: row;
`

export const TextInputStyled = styled.TextInput`
  flex: 1;
`
