import React, {
  forwardRef,
  useEffect,
  useState,
  useCallback,
  useMemo,
  useImperativeHandle
} from 'react'

import PropTypes from 'prop-types'

import { FieldProvider, useField } from '@smartcoop/forms'

import TextFieldStyled from './TextFieldStyled'

const TextFieldForm = forwardRef((props, externalRef) => {
  const {
    defaultValue,
    zIndex,
    onFieldValueChange,
    transformValue,
    ...rest
  } = props

  const [value, setValue] = useState(defaultValue)
  const [mounted, setMounted] = useState(false)

  const {
    mask,
    error,
    fieldRef,
    handleChangeNative,
    handleBlurNative,
    required,
    resetField,
    externalOnChange,
    validateField,
    fieldName
  } = useField()

  const handleChange = useCallback(
    (newValue) => setValue(newValue),
    []
  )

  useEffect(() => {
    const newValue = transformValue(value)
    onFieldValueChange(newValue)
    if (mounted) {
      handleChangeNative(newValue)
    } else {
      setMounted(true)
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [value])

  const imperativeHandles = useMemo(
    () => ({
      defaultValue,
      setValue,
      resetField,
      externalOnChange,
      validateField
    }),
    [defaultValue, externalOnChange, resetField, validateField]
  )

  useImperativeHandle(fieldRef, () => ({
    ...fieldRef.current,
    ...imperativeHandles
  }))

  useImperativeHandle(externalRef, () => ({
    ...fieldRef.current,
    ...imperativeHandles
  }))

  return (
    <TextFieldStyled
      { ...rest }
      ref={ fieldRef }
      value={ value }
      onChange={ handleChange }
      onBlur={ handleBlurNative }
      mask={ mask }
      error={ error }
      required={ required }
      name={ fieldName }
    />
  )
})

TextFieldForm.propTypes = {
  defaultValue: PropTypes.any,
  transformValue: PropTypes.func,
  onFieldValueChange: PropTypes.func,
  zIndex: PropTypes.number
}

TextFieldForm.defaultProps = {
  defaultValue: '',
  transformValue: v => v,
  onFieldValueChange: () => {},
  zIndex: undefined
}

const Field = forwardRef(({ path, ...props }, ref) => (
  <FieldProvider
    ref={ ref }
    { ...props }
    registerFieldOptions={ { path } }
    FieldComponent={ TextFieldForm }
  />
))

Field.propTypes = {
  name: PropTypes.string.isRequired,
  disabled: PropTypes.bool,
  onChange: PropTypes.func,
  onBlur: PropTypes.func,
  transformValue: PropTypes.func,
  onFieldValueChange: PropTypes.func,
  path: PropTypes.string,
  defaultValue: PropTypes.any,
  helperText: PropTypes.string
}

Field.defaultProps = {
  disabled: false,
  onChange: () => {},
  transformValue: v => v,
  onFieldValueChange: () => {},
  onBlur: () => {},
  path: 'value',
  defaultValue: '',
  helperText: null
}

export default Field
