import React, { useCallback, useMemo, useRef, useState } from 'react'
import { TouchableOpacity } from 'react-native'

import moment from 'moment/moment'
import PropTypes from 'prop-types'

import { useDialog } from '@smartcoop/dialog'
import { calendar } from '@smartcoop/icons'
import DatePickerModal from '@smartcoop/mobile-components/DatePicker/DatePickerModal'
import Icon from '@smartcoop/mobile-components/Icon'
import TextField from '@smartcoop/mobile-components/TextField'
import colors from '@smartcoop/styles/colors'
import { momentBackMonthYearFormat, momentFriendlyMonthYearFormat } from '@smartcoop/utils/dates'

const InputMonthYear = (props) => {
  const {
    value,
    onChange,
    minDate,
    maxDate,
    pickerProps,
    ...rest
  } = props

  const { detached, disabled } = rest

  const { createDialog, removeDialog } = useDialog()

  const [calendarValue, setCalendarValue] = useState(value)

  const date = useMemo(
    () => detached ? value : calendarValue,
    [calendarValue, detached, value]
  )

  const inputRef = useRef(null)

  const transformRender = useCallback(
    (newDate) => {
      const newMoment = moment(newDate, momentBackMonthYearFormat, true)
      if (newMoment.isValid()) {
        return newMoment.format(momentFriendlyMonthYearFormat)
      }
      return newMoment._i
    },
    []
  )

  const transformValue = useCallback(
    (newDate) => {
      const newMoment = moment(newDate, momentFriendlyMonthYearFormat, true)
      if (newMoment.isValid()) {
        return newMoment.format(momentBackMonthYearFormat)
      }
      return newMoment._i
    },
    []
  )

  // only use it outside a form
  const handleInputChange = useCallback(
    (newDate) => {
      onChange(transformValue(newDate))
    },
    [onChange, transformValue]
  )

  // only use it inside a form
  const handleFieldChange = useCallback(
    (newDate) => {
      if (!detached) {
        if (newDate !== value && moment(newDate, momentBackMonthYearFormat, true).isValid()) {
          setCalendarValue(newDate)
        } else {
          setCalendarValue('')
        }
      }
    },
    [detached, value]
  )

  const endAdornment = useMemo(
    () => (
      <TouchableOpacity
        onPress={ () => {
          if (!disabled) {
            createDialog({
              id: 'calendar-picker',
              Component: DatePickerModal,
              props: {
                minDate,
                maxDate,
                value: date,
                mode: 'month',
                onChange: (newDate) => {
                  if (!detached) {
                    inputRef.current.setValue(moment(newDate).format(momentBackMonthYearFormat))
                  } else {
                    onChange(newDate)
                  }
                  removeDialog({ id: 'calendar-picker' })
                },
                ...pickerProps
              }
            })
          }
        } }
      >
        <Icon
          icon={ calendar }
          size={ 22 }
          color={ colors.text }
        />
      </TouchableOpacity>
    ),
    [createDialog, date, detached, disabled, maxDate, minDate, onChange, pickerProps, removeDialog]
  )

  if (detached) {
    rest.value = date
    rest.onChange = handleInputChange
  } else {
    rest.transformValue = transformValue
    rest.onFieldValueChange = handleFieldChange
  }

  return (
    <TextField
      ref={ inputRef }
      type="datetime"
      maskOptions={ {
        format: momentFriendlyMonthYearFormat
      } }
      transformRender={ transformRender }
      endAdornment={ endAdornment }
      { ...rest }
    />
  )
}

InputMonthYear.propTypes = {
  value: PropTypes.string,
  onChange: PropTypes.func,
  minDate: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.object
  ]),
  maxDate: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.object
  ]),
  pickerProps: PropTypes.object
}

InputMonthYear.defaultProps = {
  value: '',
  onChange: () => {},
  minDate: undefined,
  maxDate: null,
  pickerProps: {}
}

export default InputMonthYear
