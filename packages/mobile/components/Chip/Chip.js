import React from 'react'

import PropTypes from 'prop-types'

import { closeBold } from '@smartcoop/icons'
import Icon from '@smartcoop/mobile-components/Icon'
import { colors } from '@smartcoop/styles'

import { Container, Text, IconContainer } from './styles'

const Chip = props => {
  const { title, onDelete, ...rest } = props
  return (
    <Container withIcon={ !!onDelete } { ...rest }>
      <Text>
        {title}
      </Text>
      {onDelete && (
        <IconContainer onPress={ onDelete }>
          <Icon icon={ closeBold } size={ 12 } color={ colors.text } />
        </IconContainer>
      )}
    </Container>
  )
}

Chip.propTypes = {
  title: PropTypes.string.isRequired,
  onDelete: PropTypes.func
}

Chip.defaultProps = {
  onDelete: undefined
}

export default Chip
