import styled from 'styled-components/native'

import { colors } from '@smartcoop/styles'

export const Container = styled.View`
  display: flex;
  flex-direction: row;
  background-color: ${ colors.muted };
  margin-right: 2px;
  padding: ${ ({ withIcon }) => withIcon
    ? '5px 5px 5px 8px'
    : '5px 8px' };
  border-radius: 50px;
  text-align: center;
  justify-content: space-between;
`

export const Text = styled.Text`
  font-size: 13px;
`

export const IconContainer = styled.TouchableOpacity`
  padding: 3px;
  margin-left: 4px;
  border-radius: 50px;
  background-color: ${ colors.mutedText };
`
