import { Subheading } from 'react-native-paper'

import styled from 'styled-components/native'

export const Container = styled.View`
  display: flex;
`

export const Row = styled.TouchableOpacity`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
`

export const Label = styled(Subheading)`
  font-weight: 700;
  margin: 0;
  padding: 0;
`

export const Box = styled.View`
  padding-top: 5px;
`

