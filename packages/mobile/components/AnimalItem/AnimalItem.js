import React, { useState, useCallback, useMemo } from 'react'
import { Text, View } from 'react-native'
import { Menu } from 'react-native-paper'
import { useDispatch } from 'react-redux'

import { useNavigation } from '@react-navigation/native'
import moment from 'moment/moment'
import PropTypes from 'prop-types'

import { useT } from '@smartcoop/i18n'
import { pencil } from '@smartcoop/icons'
import Button from '@smartcoop/mobile-components/Button'
import Icon from '@smartcoop/mobile-components/Icon'
import { AnimalActions } from '@smartcoop/stores/animal'
import { colors } from '@smartcoop/styles'
import { AnimalStatusCode } from '@smartcoop/utils/constants'
import {
  momentBackDateTimeFormat,
  momentFriendlyDateFormat
} from '@smartcoop/utils/dates'

import VerticalDotsIconButton from '../VerticalDotsIconButton/VerticalDotsIconButton'
import { Container, Row, Label, Box } from './styles'

const AnimalItem = ({ animal, onEdit, onPress }) => {
  const t = useT()
  const dispatch = useCallback(useDispatch(), [])
  const [visible, setVisible] = useState(false)
  const openMenu = useCallback(() => setVisible(true), [])
  const closeMenu = useCallback(() => setVisible(false), [])
  const navigation = useNavigation()
  const setAnimal = useCallback(() => {
    dispatch(AnimalActions.setCurrentAnimal(animal))
    dispatch(AnimalActions.setIsDetail(true))
  }, [animal, dispatch])
  const handleRegisterInsemination = useCallback(() => {
    setAnimal()
    navigation.navigate('DairyFarm', {
      screen: 'RegisterInsemination',
      params: { insemination: {} }
    })
    closeMenu()
  }, [closeMenu, navigation, setAnimal])

  const handleRegisterAnimalBirth = useCallback(() => {
    setAnimal()
    navigation.navigate('DairyFarm', {
      screen: 'RegisterAnimalBirth',
      params: { animalBirth: {} }
    })
    closeMenu()
  }, [closeMenu, navigation, setAnimal])

  const handleRegisterPregnancyActions = useCallback(() => {
    setAnimal()
    navigation.navigate('DairyFarm', {
      screen: 'RegisterPregnancyAction',
      params: { pregnancyAction: {} }
    })
    closeMenu()
  }, [closeMenu, navigation, setAnimal])

  const handleRegisterPregnancyDiagnosis = useCallback(() => {
    setAnimal()
    navigation.navigate('DairyFarm', {
      screen: 'RegisterPregnancyDiagnosis',
      params: { pregnancyDiagnosis: {} }
    })
    closeMenu()
  }, [closeMenu, navigation, setAnimal])

  const handleRegisterPev = useCallback(() => {
    setAnimal()
    navigation.navigate('DairyFarm', {
      screen: 'RegisterAnimalPev',
      params: { pev: {} }
    })
    closeMenu()
  }, [closeMenu, navigation, setAnimal])

  const changeNextAction = useMemo(
    () => ({
      inseminar: 'Inseminar',
      diagnostico_1toque: 'Diagnóstico primeiro toque',
      secagem: 'Secagem',
      pre_parto: 'Pré-parto',
      parto: 'Parto',
      liberar_pev: 'Liberar PEV',
      diagnostico_confirmar: 'Diagnóstico confirmar'
    }),
    []
  )

  const showDisplayNextActionName = useMemo(
    () => (animal?.nextAction?.proximaAcao ? 'flex' : 'none'),
    [animal]
  )

  const showDisplayNextActionDate = useMemo(
    () => (animal?.nextAction?.dataProximaAcao ? 'flex' : 'none'),
    [animal]
  )

  return (
    <Container>
      <Row>
        <Label onPress={ onPress } style={ { flex: 1 } }>
          {animal?.earring?.earringCode}
        </Label>
        <Menu
          visible={ visible }
          onDismiss={ closeMenu }
          anchor={ <VerticalDotsIconButton onPress={ openMenu } /> }
        >
          <Menu.Item
            onPress={ handleRegisterInsemination }
            title={ t('register insemination') }
            disabled={
              (animal?.category !== 'vaca' &&
              animal?.category !== 'novilha') ||
              (animal.animalStatus.id !== AnimalStatusCode.VAZIA &&
              animal.animalStatus.id !== AnimalStatusCode.APTAS &&
              animal.animalStatus.id !== AnimalStatusCode.NENHUM)
            }
          />
          <Menu.Item
            onPress={ handleRegisterPregnancyDiagnosis }
            title={ t('register {this}', {
              this: t('diagnosis', { howMany: 1 })
            }) }
            disabled={
              (animal?.category !== 'vaca' &&
              animal?.category !== 'novilha') ||
              (animal.animalStatus.id !== AnimalStatusCode.INSEMINADA &&
              animal.animalStatus.id !== AnimalStatusCode.INSEMINADA_A_CONFIRMAR &&
              animal.animalStatus.id !== AnimalStatusCode.PRENHA )
            }
          />
          <Menu.Item
            onPress={ handleRegisterPregnancyActions }
            title={ t('register {this}', { this: t('other actions') }) }
            disabled={
              (animal?.category !== 'vaca' &&
              animal?.category !== 'novilha') ||
              animal.animalStatus.id !== AnimalStatusCode.PRENHA
            }
          />
          <Menu.Item
            onPress={ handleRegisterAnimalBirth }
            title={ t('register {this}', { this: t('calving') }) }
            disabled={
              (animal?.category !== 'vaca' &&
              animal?.category !== 'novilha') ||
              (animal.animalStatus.id !== AnimalStatusCode.PRENHA &&
              animal.animalStatus.id !== AnimalStatusCode.PEV)
            }
          />
          <Menu.Item
            onPress={ handleRegisterPev }
            title={ t('register {this}', { this: t('pev') }) }
            disabled={
              (animal?.category !== 'vaca' &&
              animal?.category !== 'novilha') ||
              animal.animalStatus.id !== AnimalStatusCode.PEV
            }
          />
        </Menu>
      </Row>
      <Row onPress={ onPress }>
        <Box>
          <View>
            <Text style={ { display: showDisplayNextActionName } }>
              {changeNextAction[animal?.nextAction?.proximaAcao]}
            </Text>
          </View>
          <View>
            <Text>{animal?.lot?.name}</Text>
          </View>
        </Box>
        <Box>
          <View>
            <Text style={ { display: showDisplayNextActionDate } }>
              {moment(
                animal?.nextAction?.dataProximaAcao,
                momentBackDateTimeFormat
              ).format(momentFriendlyDateFormat)}
            </Text>
          </View>
          <View>
            <Text>{animal?.animalStatus?.name}</Text>
          </View>
        </Box>
        <View>
          <View>
            <Button
              variant="outlined"
              style={ {
                borderColor: colors.lightGrey,
                minWidth: 0,
                minHeight: 0
              } }
              color={ colors.black }
              icon={ <Icon size={ 16 } icon={ pencil } color={ colors.black } /> }
              onPress={ onEdit }
            />
          </View>
        </View>
      </Row>
    </Container>
  )
}
AnimalItem.propTypes = {
  animal: PropTypes.object,
  onPress: PropTypes.func,
  onEdit: PropTypes.func
}
AnimalItem.defaultProps = {
  animal: {},
  onPress: () => {},
  onEdit: () => {}
}
export default AnimalItem
