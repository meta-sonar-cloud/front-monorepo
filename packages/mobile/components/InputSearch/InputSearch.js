import React from 'react'

import PropTypes from 'prop-types'

import { search } from '@smartcoop/icons'
import InputIcon from '@smartcoop/mobile-components/InputIcon'

const InputSearch = (props) => {
  const { label, name, ...rest } = props

  return (
    <InputIcon
      { ...rest }
      icon={ search }
      label={ label }
      name={ name }
    />
  )
}

InputSearch.propTypes = {
  name: PropTypes.string.isRequired,
  label: PropTypes.string
}

InputSearch.defaultProps = {
  label: ''
}

export default InputSearch
