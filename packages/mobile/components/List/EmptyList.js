import React from 'react'
import { View, TouchableWithoutFeedback } from 'react-native'

import I18n from '@smartcoop/i18n'

import { NoDataContainer } from './styles'

const EmptyList = () => (
  <TouchableWithoutFeedback>
    <View>
      <NoDataContainer>
        <I18n>no data found</I18n>
      </NoDataContainer>
    </View>
  </TouchableWithoutFeedback>
)

export default EmptyList
