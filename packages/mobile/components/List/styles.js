import { Caption } from 'react-native-paper'

import styled from 'styled-components/native'

import colors from '@smartcoop/styles/colors'


export const Infos = styled.View`
  display: flex;
  flex-direction: row;
  justify-content: space-around;
`

export const FlatList = styled.FlatList`
  width: 100%;
`

export const Italic = styled(Caption)`
  font-style: italic;
  color: ${ colors.blackLight };
  padding-top: 5px;
  margin: 0;
`

export const NoDataContainer = styled.View`
  justify-content: center;
  align-items: center;
  margin-top: 10px;
`

export const Item = styled.View`
  border-color: ${ colors.lightGrey };
  border-top-width: 1px;
  border-bottom-width: ${ ({ last }) => last ? 2 : 0 }px;
  padding: 10px 15px;
`
