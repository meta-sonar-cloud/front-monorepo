import React, { useState, useRef, useCallback, useEffect, useMemo } from 'react'
import { RefreshControl, TouchableWithoutFeedback, ActivityIndicator , View } from 'react-native'

import { useFocusEffect } from '@react-navigation/native'
import moment from 'moment/moment'
import PropTypes from 'prop-types'

import isEmpty from 'lodash/isEmpty'
import size from 'lodash/size'

import I18n from '@smartcoop/i18n'
import colors from '@smartcoop/styles/colors'

import EmptyList from './EmptyList'
import { FlatList, Infos, Italic, Item } from './styles'

const List = (props) => {
  const {
    data,
    renderItem: externalRenderItem,
    onListLoad,
    onListLoadError,
    onListLoaded,
    hideUpdatedAt,
    obs,
    deps,
    ListEmptyComponent,
    itemKey,
    nestedScrollEnabled,
    ...rest
  } = props

  const mounted = useRef(false)

  const [fetching, setFetching] = useState(false)
  const [refreshing, setRefreshing] = useState(false)
  const [moreLoading, setMoreLoading] = useState(false)

  const [lastUpdated, setLastUpdated] = useState(null)
  const [nextPage, setNextPage] = useState(1)

  const lastIndex = useMemo(
    () => size(data) - 1,
    [data]
  )

  const renderItem = useCallback(
    (item) => (
      <TouchableWithoutFeedback>
        <Item
          first={ item.index === 0 }
          last={ item.index === lastIndex }
        >
          {externalRenderItem(item)}
        </Item>
      </TouchableWithoutFeedback>
    ),
    [externalRenderItem, lastIndex]
  )

  const handleListUpdated = useCallback(
    (paginationInfo) => {
      setFetching(false)
      setRefreshing(false)
      setMoreLoading(false)
      if (paginationInfo) {
        if (paginationInfo.page >= paginationInfo.totalPages) {
          setNextPage(0)
        } else {
          setNextPage(paginationInfo.page + 1)
        }
      } else {
        setNextPage(0)
      }
      setLastUpdated(moment())
      onListLoaded()
    },
    [onListLoaded]
  )

  const handleListUpdateError = useCallback(
    () => {
      setFetching(false)
      setRefreshing(false)
      setMoreLoading(false)
      onListLoadError()
    },
    [onListLoadError]
  )

  const handleListUpdate = useCallback(
    (page, next = nextPage, showLoader = true) => {
      if (!refreshing) {
        return new Promise((resolve, reject) => {
          if (page && page <= next) {

            setFetching(true)

            if (showLoader || (page === 1 && isEmpty(data))) {
              setRefreshing(true)
            }
            if (showLoader && page > 1) {
              setMoreLoading(true)
            }

            onListLoad(page, resolve, reject)
          }
        })
          .then(handleListUpdated)
          .catch(handleListUpdateError)
      }
      return undefined
    },
    [data, handleListUpdateError, handleListUpdated, nextPage, onListLoad, refreshing]
  )

  const onEndReached = useCallback(
    () => {
      handleListUpdate(nextPage)
    },
    [handleListUpdate, nextPage]
  )

  const onRefresh = useCallback(
    (showLoader = true) => {
      setNextPage(1)
      handleListUpdate(1, 1, showLoader)
    },
    [handleListUpdate]
  )

  useEffect(() => {
    onRefresh(mounted.current)
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [deps])


  useFocusEffect(
    useCallback(() => {
      mounted.current = false
      setTimeout(() => {
        mounted.current = true
      }, 100)
      return () => {
        if (mounted.current) {
          mounted.current = false
        }
      }
    }, [])
  )

  return (
    <View>
      <Infos>
        {!hideUpdatedAt && lastUpdated && (
          <Italic>
            <I18n as={ Italic }>last updated</I18n>: {lastUpdated.format('DD/MM/YYYY[ às] HH:mm')}
          </Italic>
        )}
        {obs && <Italic>{obs}</Italic>}
      </Infos>
      <FlatList
        data={ data }
        renderItem={ renderItem }
        keyExtractor={ item => item[itemKey].toString() }
        nestedScrollEnabled={ nestedScrollEnabled }
        ListEmptyComponent={ fetching ? undefined : ListEmptyComponent }
        onEndReached={ fetching ? undefined : onEndReached }
        onEndReachedThreshold={ 1 }
        ListFooterComponent={ () => moreLoading && (
          <ActivityIndicator
            size={ 30 }
            color={ colors.muted }
            style={ { padding: 10 } }
          />
        ) }
        contentContainerStyle={ {
          paddingTop: nestedScrollEnabled ? 0 : 10,
          paddingBottom: nestedScrollEnabled ? 0 : 80,
          minHeight: '100%'
        } }
        refreshControl={ (
          <RefreshControl
            refreshing={ refreshing }
            onRefresh={ onRefresh }
          />
        ) }
        { ...rest }
      />
    </View>
  )
}

List.propTypes = {
  data: PropTypes.array,
  deps: PropTypes.array,
  renderItem: PropTypes.func.isRequired,
  onListLoad: PropTypes.func.isRequired,
  onListLoadError: PropTypes.func,
  onListLoaded: PropTypes.func,
  hideUpdatedAt: PropTypes.bool,
  obs: PropTypes.string,
  itemKey: PropTypes.string,
  ListEmptyComponent: PropTypes.oneOfType([
    PropTypes.bool,
    PropTypes.element
  ]),
  nestedScrollEnabled: PropTypes.bool
}

List.defaultProps = {
  data: [],
  deps: [],
  onListLoadError: () => {},
  onListLoaded: () => {},
  hideUpdatedAt: false,
  obs: null,
  itemKey: 'id',
  ListEmptyComponent: <EmptyList />,
  nestedScrollEnabled: false
}

export default List
