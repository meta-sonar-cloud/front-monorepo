import React, {
  useState,
  useCallback,
  useEffect,
  useMemo,
  useImperativeHandle,
  forwardRef
} from 'react'


import trimMask from '@meta-awesome/functions/src/trimMask'
import PropTypes from 'prop-types'

import filter from 'lodash/filter'
import find from 'lodash/find'
import isEmpty from 'lodash/isEmpty'
import isEqual from 'lodash/isEqual'
import kebabCase from 'lodash/kebabCase'
import map from 'lodash/map'

import { FieldProvider, useField } from '@smartcoop/forms'
import { useT } from '@smartcoop/i18n'
import { plus } from '@smartcoop/icons'
import Divider from '@smartcoop/mobile-components/Divider'
import Icon from '@smartcoop/mobile-components/Icon'

import CheckboxButton from './CheckboxButton'
import {
  Container,
  CheckboxContainer,
  ErrorLabel,
  Label,
  AddNewArea,
  AddNewAreaContainer
} from './styles'

const CheckboxGroup = (props) => {
  const {
    label: externalLabel,
    options: externalOptions,
    variant,
    defaultValue: externalDefaultValue,
    extendable,
    handleChange,
    styleContainer
  } = props
  const t = useT()
  const defaultValue = useMemo(
    () => externalDefaultValue || [],
    // eslint-disable-next-line react-hooks/exhaustive-deps
    []
  )

  const [value, setValue] = useState(defaultValue)
  const [mounted, setMounted] = useState(false)
  const [newArea, setNewArea] = useState('')
  const [options, setOptions] = useState(externalOptions)
  const {
    fieldName,
    fieldRef,
    formRef,
    handleChangeNative,
    setTouched,
    required,
    error,
    validateField
  } = useField()

  const label = useMemo(
    () => `${ externalLabel }${ required ? ' *' : '' }`,
    [externalLabel, required]
  )

  const onChange = useCallback(
    (newValue, checked) => {
      setValue((oldValue) => {
        if (checked) {
          // add
          return [...oldValue, newValue]
        }
        // remove
        return filter(oldValue, val => val !== newValue)
      })
    },
    []
  )

  const addNewAreaToGroup = useCallback(() => {
    if(trimMask(newArea)) {
      const formatedLabel = kebabCase(newArea)

      const newOptions = [
        ...options,
        {
          label: newArea,
          value: formatedLabel,
          checked: true
        }
      ]
      setOptions(newOptions)
      setValue([...value, formatedLabel])
      setNewArea('')
    }
  }, [newArea, options, value])

  useEffect(() => {
    handleChange(value)
  },
  [handleChange, value])

  useEffect(() => {
    if (mounted) {
      const formFieldRef = formRef.current.getFieldRef(fieldName)
      if (!isEqual(value, formFieldRef.value)) {
        handleChangeNative(value)
        if (isEmpty(value)) {
          setTouched(true)
        }
      }
    } else {
      setMounted(true)
    }
    const checkboxValues = map(options, option => ({
      ...option,
      checked: !!find(value, val => val === option.value)
    }))
    setOptions(checkboxValues)
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [value])

  useImperativeHandle(fieldRef, () => ({
    ...(fieldRef.current || {}),
    value,
    defaultValue,
    setValue,
    validateField
  }))

  return (
    <Container>
      <Label error={ error } >{label}</Label>
      <ErrorLabel>{error}</ErrorLabel>
      <CheckboxContainer style={ { flexDirection: variant, ...styleContainer } }>
        {
          map(options, (option) => (
            <CheckboxButton
              key={ option.value }
              { ...option }
              onChange={ onChange }
            />
          ))
        }
      </CheckboxContainer>

      {extendable ? (
        <>
          <Divider />
          <AddNewAreaContainer>
            <Icon icon={ plus } size={ 16 }/>
            <AddNewArea
              placeholder={ t('add another acticity') }
              onChangeText={ text => setNewArea(text) }
              value={ newArea }
              onKeyPress={ ({ nativeEvent }) => nativeEvent.key === 'Enter' && addNewAreaToGroup() }
            />
          </AddNewAreaContainer>
        </>
      ) : null}
    </Container>
  )
}

CheckboxGroup.propTypes = {
  label: PropTypes.string,
  variant: PropTypes.oneOf(['row', 'column']).isRequired,
  extendable: PropTypes.bool,
  handleChange: PropTypes.func,
  styleContainer: PropTypes.object,
  options: PropTypes.arrayOf(PropTypes.shape({
    label: PropTypes.string,
    value: PropTypes.string
  })).isRequired,
  defaultValue: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.arrayOf(PropTypes.string)
  ])
}

CheckboxGroup.defaultProps = {
  label: null,
  defaultValue: undefined,
  handleChange: () => {},
  extendable: false,
  styleContainer: {}
}

const Field = forwardRef(({ path, ...props }, ref) => (
  <FieldProvider
    ref={ ref }
    { ...props }
    registerFieldOptions={ { path } }
    FieldComponent={ CheckboxGroup }
  />
))

Field.propTypes = {
  label: PropTypes.string,
  variant: PropTypes.oneOf(['row', 'column']),
  options: PropTypes.arrayOf(PropTypes.shape({
    label: PropTypes.string,
    value: PropTypes.string
  })),
  name: PropTypes.string.isRequired,
  type: PropTypes.string,
  className: PropTypes.string,
  size: PropTypes.oneOf(['small', 'medium']),
  disabled: PropTypes.bool,
  onChange: PropTypes.func,
  onBlur: PropTypes.func,
  path: PropTypes.string,
  defaultValue: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.arrayOf(PropTypes.string)
  ])
}

Field.defaultProps = {
  variant: 'column',
  type: 'text',
  className: '',
  size: 'small',
  disabled: false,
  onChange: () => {},
  onBlur: () => {},
  path: 'value',
  options: [],
  defaultValue: undefined,
  label: null
}

export default Field
