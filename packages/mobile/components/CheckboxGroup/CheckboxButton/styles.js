import styled from 'styled-components/native'

import { colors } from '@smartcoop/styles'


export const CheckboxTouchableContainer = styled.TouchableOpacity`
  display: flex;
  flex-direction: row;
`

export const CheckboxContainer = styled.View`
  background-color: ${ props => props.checked ? colors.secondary : colors.backgroundHtml };
  border-radius: 5px;
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  min-height: 30px;
  padding: 15px;
  margin-bottom: 15px;
`

export const IconContainer = styled.View`
  max-width: 20px;
  min-width: 20px;
  display: flex;
  justify-content: center;
  align-items: center;
`

export const LabelContainer = styled.View`
  display: flex;
  justify-content: center;
`

export const Label = styled.Text`
  font-weight: 600;
  margin-left: 8px;
  font-size: 16px;
`
