import React, { useCallback, useMemo } from 'react'

import PropTypes from 'prop-types'

import { checkboxChecked as checkedIcon, checkboxUnchecked as uncheckedIcon } from '@smartcoop/icons'
import Icon from '@smartcoop/mobile-components/Icon'
import { colors } from '@smartcoop/styles'

import {
  CheckboxTouchableContainer,
  CheckboxContainer,
  LabelContainer,
  IconContainer,
  Label
} from './styles'



const CheckboxButton = (props) => {
  const { disabled, checked, label, style, onChange, value } = props

  const isChecked = useMemo(
    () => (
      checked ? checkedIcon : uncheckedIcon
    ),
    [checked]
  )
  const toggle = useCallback(
    () => {
      if(!disabled) {
        onChange(value, !checked)
      }
    },
    [disabled, onChange, value, checked]
  )

  return (
    <CheckboxContainer checked={ checked } style={ { ...style } }>
      <CheckboxTouchableContainer onPress={ toggle }>
        <IconContainer>
          <Icon icon={ isChecked } size={ 16 } color={ disabled ? colors.lightGrey : colors.black } />
        </IconContainer>
        <LabelContainer>
          <Label>
            {label}
          </Label>
        </LabelContainer>
      </CheckboxTouchableContainer>
    </CheckboxContainer>

  )
}

CheckboxButton.propTypes = {
  checked: PropTypes.oneOfType([PropTypes.bool]),
  disabled: PropTypes.oneOfType([PropTypes.bool]),
  value: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  style: PropTypes.object
}

CheckboxButton.defaultProps = {
  style: {},
  checked: false,
  disabled: false
}

export default CheckboxButton
