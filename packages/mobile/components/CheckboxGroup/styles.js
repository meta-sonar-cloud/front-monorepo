import styled from 'styled-components/native'

import { colors } from '@smartcoop/styles'

export const Container = styled.View`
  display: flex;
  flex-direction: column;
  flex-wrap: wrap;
`

export const AddNewAreaContainer = styled.View`
  flex-direction: row;
`

export const CheckboxContainer = styled.View`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  flex-wrap: wrap;
`

export const AddNewArea = styled.TextInput`
  margin: 10px;
  font-size: 16px;
  width: 100%;
`

export const Label = styled.Text`
  font-weight: 600;
  margin-left: 8px;
  font-size: 16px;
  margin-bottom: 10px;
  color: ${ props => props.error ? colors.error : colors.black };
`
export const ErrorLabel = styled(Label)`
  font-weight: normal;
  margin-left: 8px;
  font-size: 13px;
  margin-bottom: 10px;
  color: ${ colors.error };
`
