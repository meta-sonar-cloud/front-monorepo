import React, { useCallback, useState, useMemo } from 'react'
import { FlatList, Text } from 'react-native'

import PropTypes from 'prop-types'

import filter from 'lodash/filter'
import find from 'lodash/find'
import filterFP from 'lodash/fp/filter'
import flow from 'lodash/fp/flow'
import uniqFP from 'lodash/fp/uniq'
import indexOf from 'lodash/indexOf'
import isEmpty from 'lodash/isEmpty'
import kebabCase from 'lodash/kebabCase'
import map from 'lodash/map'
import size from 'lodash/size'

import { useT } from '@smartcoop/i18n'
import { checkboxChecked, checkboxUnchecked, unchecked as radioUnchecked, checked as radioChecked } from '@smartcoop/icons'
import Button from '@smartcoop/mobile-components/Button'
import Icon from '@smartcoop/mobile-components/Icon'
import InputSearch from '@smartcoop/mobile-components/InputSearch'
import colors from '@smartcoop/styles/colors'

import {
  ModalOptions,
  OptionListItem,
  ModalContainer,
  ModalFieldContainer,
  SafeAreaOptions,
  ButtonContainer,
  OptionContainer,
  OptionText
} from './styles'

const SelectOptionsModal = (props) => {
  const {
    id,
    open,
    handleClose,
    multiple,
    options: externalOptions,
    label,
    value,
    clearable,
    handleChange,
    creatable,
    checkBoxSelectAll,
    onCreateOptions
  } = props

  const [inputSearch, setInputSearch] = useState('')
  const [internalValue, setInternalValue] = useState(value)
  const [internalCreatedOptions, setInternalCreatedOptions] = useState([])

  const t = useT()

  const options = useMemo(
    () => [...internalCreatedOptions, ...externalOptions],
    [externalOptions, internalCreatedOptions]
  )

  const visibleOptions = useMemo(
    () => options.filter((option) => {
      const parts = (inputSearch || '').trim().split(/[ \-:]+/)
      const regex = new RegExp(`(${ parts.join('|') })`, 'ig')
      return regex.test(option.label)
    }),
    [options, inputSearch]
  )

  const handleSelectOption = useCallback(
    (option) => {
      setInternalValue((old) => {
        const { value: newValue } = option
        if (!multiple) {
          return newValue
        }
        return flow(
          filterFP(item => item),
          uniqFP
        )([...old, newValue])
      })
    },
    [multiple]
  )

  const handleUnselectOption = useCallback(
    (option) => {
      setInternalValue((old) => {
        const { value: newValue } = option

        if (!multiple) {
          if (clearable) {
            return ''
          }
          return newValue
        }

        return flow(
          filterFP(item => item !== option.value),
          uniqFP
        )([...old, newValue])
      })
    },
    [clearable, multiple]
  )

  const onConfirm = useCallback(
    () => {
      onCreateOptions(internalCreatedOptions)
      handleClose()

      let activeItem

      if (multiple) {
        activeItem = filter(options, (option) => indexOf(internalValue, option.value) > -1)
      } else {
        activeItem = find(options, (option) => option.value === internalValue)
      }

      handleChange(internalValue, activeItem)
    },
    [onCreateOptions, internalCreatedOptions, handleClose, options, multiple, handleChange, internalValue]
  )

  const allSelected = useMemo(
    () => multiple && size(options) === size(internalValue),
    [internalValue, multiple, options]
  )

  const handleCreateOption = useCallback(
    () => {
      const newOption = { value: kebabCase(inputSearch), label: inputSearch, created: true }
      const hasItem = find(options, option => kebabCase(option.label) === kebabCase(newOption.label))
      if (!hasItem) {
        setInternalCreatedOptions(old => [newOption, ...old])
        handleSelectOption(newOption)
        setInputSearch('')
      }
    },
    [handleSelectOption, inputSearch, options]
  )

  const handleSearchPress = useCallback(
    () => {
      if (creatable && inputSearch !== '') {
        handleCreateOption()
      }
    },
    [creatable, handleCreateOption, inputSearch]
  )

  const createRow = useCallback(
    ({ label: optionLabel, key, handlePress, checked }) => {
      let icon

      if (multiple) {
        icon = checked ? checkboxChecked : checkboxUnchecked
      } else {
        icon = checked ? radioChecked : radioUnchecked
      }

      return (
        <OptionListItem
          key={ key }
          onPress={ handlePress }
        >
          <Icon
            icon={ icon }
            color={ checked ? colors.secondary : colors.muted }
            size={ 16 }
          />
          <Text style={ { paddingLeft: 10 } }>
            {optionLabel}
          </Text>
        </OptionListItem>
      )
    },
    [multiple]
  )

  const renderItem = useCallback(
    ({ item, id: itemId }) => {
      const checked = multiple
        ? !!find(internalValue, val => val === item.value)
        : internalValue === item.value

      const handlePress = () => checked ? handleUnselectOption(item) : handleSelectOption(item)

      return createRow({
        checked,
        label: item.label,
        key: itemId,
        handlePress
      })
    },
    [createRow, handleSelectOption, handleUnselectOption, internalValue, multiple]
  )

  const renderSelectAll = useCallback(
    () => {

      const handleSelectAll = () => {
        if(allSelected) {
          setInternalValue([])
        } else {
          setInternalValue(map(options, item => item.value))
        }
      }

      return createRow({
        checked: allSelected,
        label: t('select all'),
        key: 'select-all',
        handlePress: handleSelectAll
      })
    },
    [allSelected, createRow, options, t]
  )

  const flatListExtraData = useMemo(
    () => ({ internalValue, visibleOptions }),
    [internalValue, visibleOptions]
  )


  const noOptionsText = useMemo(
    () => creatable
      ? `${ t('press enter to include')  } "${ inputSearch }"`
      : t('no options'),
    [creatable, inputSearch, t]
  )

  return (
    <ModalOptions
      id={ id }
      open={ open }
      title={ t('select {this}', { this: label }) }
    >
      <ModalContainer>
        <ModalFieldContainer>
          <InputSearch
            detached
            name="select-search-input"
            label={ t('search') }
            value={ inputSearch }
            onChange={ setInputSearch }
            onSubmitEditing={ handleSearchPress }
          />
        </ModalFieldContainer>
        <SafeAreaOptions>
          {isEmpty(visibleOptions)
            ? (
              <OptionContainer>
                <OptionText>
                  {noOptionsText}
                </OptionText>
              </OptionContainer>
            ) : (
              <FlatList
                ListHeaderComponent={
                  () => checkBoxSelectAll && multiple && renderSelectAll()
                }
                data={ visibleOptions }
                renderItem={ renderItem }
                keyExtractor={ item => item.value }
                nestedScrollEnabled
                extraData={ flatListExtraData }
              />
            )}
        </SafeAreaOptions>
        <ButtonContainer>
          <Button
            title={ `${ t('select {this}', { this: '' }) }${ multiple ? ` (${ size(internalValue) })` : '' }` }
            onPress={ onConfirm }
          />
        </ButtonContainer>
      </ModalContainer>
    </ModalOptions>
  )
}

SelectOptionsModal.propTypes = {
  id: PropTypes.string.isRequired,
  open: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  multiple: PropTypes.bool.isRequired,
  label: PropTypes.string,
  options: PropTypes.array.isRequired,
  value: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.arrayOf(PropTypes.string)
  ]).isRequired,
  clearable: PropTypes.bool.isRequired,
  creatable: PropTypes.bool.isRequired,
  handleChange: PropTypes.func.isRequired,
  onCreateOptions: PropTypes.func.isRequired,
  checkBoxSelectAll: PropTypes.bool.isRequired
}

SelectOptionsModal.defaultProps = {
  label: ''
}

export default SelectOptionsModal
