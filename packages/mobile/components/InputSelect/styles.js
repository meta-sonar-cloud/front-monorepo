import { SafeAreaView, Dimensions, KeyboardAvoidingView, Platform } from 'react-native'

import styled from 'styled-components/native'

import Modal from '@smartcoop/mobile-components/Modal'
import { colors, hexToRgba } from '@smartcoop/styles'

export const Container = styled.View`
  width: 100%;
  display: flex;
  flex-direction: row;
`

export const OptionsView = styled.View`
  width: 100%;
  max-height: 170px;
  padding: 2px 0;
  background-color: ${ colors.white };
  border-radius: 4px;
`

export const OptionContainer = styled.View`
  background-color: ${ ({ odd }) => hexToRgba(odd ? colors.backgroundHtml : colors.white, 0.98) };
`

export const OptionText = styled.Text`
  padding: 10px;
`

export const SingleValueContainer = styled.Text`
  padding: 0;
  font-size: 14px;
`

export const AdornmentContainer = styled.View`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
`

export const ModalOptions = styled(Modal).attrs({
  headerStyle: {
    paddingLeft: 15,
    paddingRight: 15
  }
})`
  width: 80%;
  margin: 20px 0;
  padding-left: 0;
  padding-right: 0;
  padding-bottom: 0;
  border-radius: 4px;
  overflow: hidden;
`

export const ModalFieldContainer = styled.View`
  padding-left: 15px;
  padding-right: 15px;
`

export const ModalContainer = styled(KeyboardAvoidingView).attrs({
  behavior: 'padding',
  keyboardVerticalOffset: Platform.OS === 'ios' ? Dimensions.get('window').height * 0.29 : 0
})`
  padding-top: 20px;
  height: ${ Dimensions.get('window').height * 0.5 }px;
`

export const ButtonContainer = styled.View`
  justify-content: flex-end;
  padding: 20px;
`

export const OptionListItem = styled.TouchableOpacity`
  padding: 10px 15px;
  border-bottom-color: ${ colors.backgroundHtml };
  border-bottom-width: 1px;
  flex-direction: row;
  align-items: center;
`

export const SafeAreaOptions = styled(SafeAreaView)`
  border-top-color: ${ colors.backgroundHtml };
  border-top-width: 1px;
  flex: 1;
`