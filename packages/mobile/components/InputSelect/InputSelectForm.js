import React, {
  useState,
  useCallback,
  useEffect,
  useMemo,
  useImperativeHandle,
  forwardRef
} from 'react'

import PropTypes from 'prop-types'

import isEmpty from 'lodash/isEmpty'
import isEqual from 'lodash/isEqual'

import { FieldProvider, useField } from '@smartcoop/forms'

import InputSelectStyled from './InputSelectStyled'

const InputSelectForm = (props) => {
  const {
    multiple,
    defaultValue: externalDefaultValue,
    zIndex,
    ...rest
  } = props

  const defaultValue = useMemo(
    () => externalDefaultValue || (multiple ? [] : ''),
    // eslint-disable-next-line react-hooks/exhaustive-deps
    []
  )

  const [value, setValue] = useState(defaultValue)
  const [mounted, setMounted] = useState(false)
  const [asyncItem, setAsyncItem] = useState()

  const {
    fieldName,
    fieldRef,
    formRef,
    handleChangeNative,
    handleBlurNative,
    setTouched,
    resetField,
    externalOnChange,
    validateField,
    required,
    error
  } = useField()

  const onChange = useCallback(
    (newValue, item) => {
      setValue(newValue)
      setAsyncItem(item)
      handleBlurNative(newValue)
    },
    [handleBlurNative]
  )

  useEffect(() => {
    if (mounted) {
      const formFieldValue = formRef.current.getFieldValue(fieldName)
      if (!isEqual(value, formFieldValue)) {
        handleChangeNative(value, asyncItem)
        if (isEmpty(value)) {
          setTouched(true)
        }
      }
    } else {
      setMounted(true)
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [value])

  useImperativeHandle(fieldRef, () => ({
    ...(fieldRef.current || {}),
    value,
    defaultValue,
    setValue,
    resetField,
    externalOnChange,
    validateField
  }))

  return (
    <InputSelectStyled
      { ...rest }
      value={ value }
      onChange={ onChange }
      required={ required }
      error={ error }
      multiple={ multiple }
      zIndex={ zIndex }
    />
  )
}

InputSelectForm.propTypes = {
  defaultValue: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.arrayOf(PropTypes.string)
  ]),
  zIndex: PropTypes.number.isRequired,
  multiple: PropTypes.bool
}

InputSelectForm.defaultProps = {
  defaultValue: undefined,
  multiple: false
}


const Field = forwardRef(({ path, ...props }, ref) => (
  <FieldProvider
    ref={ ref }
    { ...props }
    registerFieldOptions={ { path } }
    FieldComponent={ InputSelectForm }
  />
))

Field.propTypes = {
  name: PropTypes.string.isRequired,
  onChange: PropTypes.func,
  onBlur: PropTypes.func,
  path: PropTypes.string,
  multiple: PropTypes.bool,
  defaultValue: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.arrayOf(PropTypes.string)
  ])
}

Field.defaultProps = {
  onChange: () => {},
  onBlur: () => {},
  path: 'value',
  multiple: false,
  defaultValue: undefined
}

export default Field
