import React, { useState, useCallback, useMemo, useEffect } from 'react'
import { TouchableOpacity, ActivityIndicator } from 'react-native'

import PropTypes from 'prop-types'

import { isNumber } from 'lodash'
import camelCase from 'lodash/camelCase'
import filter from 'lodash/filter'
import find from 'lodash/find'
import get from 'lodash/get'
import indexOf from 'lodash/indexOf'
import isEmpty from 'lodash/isEmpty'
import isFunction from 'lodash/isFunction'
import map from 'lodash/map'
import uniqBy from 'lodash/uniqBy'
import upperFirst from 'lodash/upperFirst'

import { useDialog } from '@smartcoop/dialog'
import { arrowDown, close } from '@smartcoop/icons'
import Chip from '@smartcoop/mobile-components/Chip'
import Icon from '@smartcoop/mobile-components/Icon'
import TextField from '@smartcoop/mobile-components/TextField'
import { colors } from '@smartcoop/styles'

import SelectOptionsModal from './SelectOptionsModal'
import { Container, AdornmentContainer, SingleValueContainer } from './styles'

const InputSelectStyled = (props) => {
  const {
    options: externalOptions,
    multiple,
    creatable,
    clearable,
    zIndex,
    value,
    onChange,
    required,
    error,
    deps,
    loaderEnable,
    asyncOptionLabelField,
    asyncOptionValueField,
    urlParams,
    queryParams,
    checkBoxSelectAll,
    disabled,
    ...rest
  } = props

  const { label } = rest

  const { createDialog } = useDialog()

  const [loading, setLoading] = useState(false)
  const [asyncOptions, setAsyncOptions] = useState([])
  const [asyncData, setAsyncData] = useState([])
  const [createdOptions, setCreatedOptions] = useState([])

  const asyncMode = useMemo(() => isFunction(externalOptions), [
    externalOptions
  ])

  const options = useMemo(() => {
    let newOptions = [
      ...(asyncMode ? asyncOptions : externalOptions),
      ...createdOptions
    ]
    if (creatable && !find(newOptions, { value }) && !isEmpty(value)) {
      newOptions = [{ value, label: upperFirst(value) }, ...newOptions]
    }
    return uniqBy(newOptions, 'value')
  }, [
    asyncMode,
    asyncOptions,
    creatable,
    createdOptions,
    externalOptions,
    value
  ])

  const optionSelected = useMemo(() => {
    const defaultReturn = multiple ? [] : ''
    if (multiple) {
      return map(value, (defVal) =>
        find(options, (option) => option.value === defVal)
      )
    }
    if (!isEmpty(value)) {
      return find(options, (option) => option.value === value) || defaultReturn
    }
    return defaultReturn
  }, [multiple, options, value])

  const onCreateOptions = useCallback((newOptions) => {
    setCreatedOptions((old) => [...newOptions, ...old])
  }, [])

  const handleChange = useCallback(
    (newValue, option) => {
      let activeAsyncItem

      if (multiple) {
        activeAsyncItem = filter(
          asyncData,
          (item) => indexOf(newValue, get(item, asyncOptionValueField)) > -1
        )
      } else {
        activeAsyncItem = find(
          asyncData,
          (item) => get(item, asyncOptionValueField) === newValue
        )
      }

      onChange(newValue, isEmpty(activeAsyncItem) ? option : activeAsyncItem)
    },
    [asyncData, asyncOptionValueField, multiple, onChange]
  )

  const removeOption = useCallback(
    (valueToDelete) => {
      const newValues = filter(value, (item) => item !== valueToDelete)
      handleChange(newValues)
    },
    [handleChange, value]
  )

  const handleOpenOptions = useCallback(() => {
    if (!loading && !disabled) {
      createDialog({
        id: 'select',
        Component: SelectOptionsModal,
        props: {
          multiple,
          options,
          label,
          value,
          creatable,
          clearable,
          checkBoxSelectAll,
          handleChange,
          onCreateOptions
        }
      })
    }
  }, [
    loading,
    disabled,
    createDialog,
    multiple,
    options,
    label,
    value,
    creatable,
    clearable,
    checkBoxSelectAll,
    handleChange,
    onCreateOptions
  ])

  const loadAsyncOptions = useCallback(async () => {
    try {
      setLoading(true)
      const {
        data: { data }
      } = await externalOptions(
        {
          page: 1,
          limit: process.env.REACT_APP_FAKE_PAGINATION_SIZE || 100,
          ...queryParams
        },
        urlParams
      )

      setAsyncData(data)
      setAsyncOptions(
        map(data, (option) => ({
          value: isNumber(get(option, asyncOptionValueField))
            ? get(option, asyncOptionValueField).toString()
            : get(option, asyncOptionValueField),
          label: get(option, asyncOptionLabelField)
        }))
      )
    } finally {
      setLoading(false)
    }
  }, [
    asyncOptionLabelField,
    asyncOptionValueField,
    externalOptions,
    queryParams,
    urlParams
  ])

  const startAdornment = useMemo(
    () =>
      multiple ? (
        <AdornmentContainer>
          {map(
            optionSelected,
            (item) =>
              item && (
                <Chip
                  key={ item.value }
                  title={ camelCase(item.label) }
                  onDelete={
                    clearable ? () => removeOption(item.value) : undefined
                  }
                  style={ { margin: 3 } }
                />
              )
          )}
        </AdornmentContainer>
      ) : (
        <AdornmentContainer>
          {!!optionSelected && (
            <SingleValueContainer>{optionSelected.label}</SingleValueContainer>
          )}
        </AdornmentContainer>
      ),
    [clearable, multiple, optionSelected, removeOption]
  )

  const endAdornment = useMemo(
    () => (
      <AdornmentContainer>
        {loading && <ActivityIndicator size={ 24 } />}

        {!multiple && clearable && !isEmpty(value) && (
          <TouchableOpacity onPress={ () => handleChange('') }>
            <Icon icon={ close } size={ 24 } color={ colors.mutedText } />
          </TouchableOpacity>
        )}

        <TouchableOpacity onPress={ handleOpenOptions }>
          <Icon icon={ arrowDown } size={ 24 } color={ colors.mutedText } />
        </TouchableOpacity>
      </AdornmentContainer>
    ),
    [loading, multiple, clearable, value, handleOpenOptions, handleChange]
  )

  useEffect(() => {
    if (asyncMode && loaderEnable) {
      loadAsyncOptions()
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [asyncMode, queryParams, urlParams, deps])

  return (
    <>
      <Container style={ zIndex ? { zIndex } : {} }>
        <TextField
          detached
          { ...rest }
          disabled={ disabled }
          onPress={ handleOpenOptions }
          disableAutoUnfocus
          disableFocusOnPress
          endAdornment={ endAdornment }
          startAdornment={ startAdornment }
          value={ isEmpty(value) ? '' : ' ' }
          required={ required }
          error={ error }
          readOnly
          // inputHeight={ multiple ? Math.max(Math.ceil(size(value)/4), 1) * 55 : 40 }
        />
      </Container>
    </>
  )
}

InputSelectStyled.propTypes = {
  zIndex: PropTypes.number,
  options: PropTypes.oneOfType([
    PropTypes.arrayOf(
      PropTypes.shape({
        label: PropTypes.string,
        value: PropTypes.string
      })
    ),
    PropTypes.func
  ]),
  multiple: PropTypes.bool,
  creatable: PropTypes.bool,
  checkBoxSelectAll: PropTypes.bool,
  loaderEnable: PropTypes.bool,
  deps: PropTypes.array,
  clearable: PropTypes.bool,
  value: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.arrayOf(PropTypes.string)
  ]),
  onChange: PropTypes.func,
  required: PropTypes.bool,
  error: PropTypes.string,
  asyncOptionLabelField: PropTypes.string,
  asyncOptionValueField: PropTypes.string,
  urlParams: PropTypes.object,
  queryParams: PropTypes.object,
  disabled: PropTypes.bool
}

InputSelectStyled.defaultProps = {
  options: [],
  multiple: false,
  creatable: false,
  checkBoxSelectAll: false,
  clearable: false,
  loaderEnable: true,
  deps: [],
  value: '',
  onChange: () => {},
  required: false,
  error: null,
  asyncOptionLabelField: 'name',
  asyncOptionValueField: 'id',
  urlParams: {},
  queryParams: {},
  zIndex: null,
  disabled: false
}

export default InputSelectStyled
