import styled from 'styled-components/native'

export const Preview = styled.ImageBackground`
  width: ${ (props) => `${ props.size }px` };
  height: ${ (props) => `${ props.size }px` };
  border-radius: 5px;
  resize-mode: cover;
  margin-right: 10px;
  margin-bottom: 10px;

  display: flex;
  flex-direction: row;
  justify-content: flex-end;
`
