import React from 'react'
import { TouchableOpacity } from 'react-native'

import PropTypes from 'prop-types'

import { closeRounded } from '@smartcoop/icons'
import Icon from '@smartcoop/mobile-components/Icon'
import colors from '@smartcoop/styles/colors'

import { Preview } from './styles'

const ThumbnailImage = ({ src, style, size, onClose }) => (
  <Preview
    source={ { uri: src } }
    style={ style }
    size={ size }
  >
    {onClose &&
      <TouchableOpacity
        style={ { marginHorizontal: 5.5, marginVertical: 5.5 } }
        onPress={ onClose }
      >
        <Icon
          icon={ closeRounded }
          border={ colors.grey }
          color={ colors.darkGrey }
        />
      </TouchableOpacity>
    }
  </Preview>
)

ThumbnailImage.propTypes = {
  src: PropTypes.string.isRequired,
  style: PropTypes.object,
  size: PropTypes.number,
  onClose: PropTypes.func
}

ThumbnailImage.defaultProps = {
  style: {},
  size: 150,
  onClose: null
}

export default ThumbnailImage
