import React from 'react'

import PropTypes from 'prop-types'

import numberMask from '@smartcoop/forms/masks/number.mask'
import TextField from '@smartcoop/mobile-components/TextField'

const InputNumber = props => (
  <TextField
    setMask={ numberMask }
    { ...props }
    keyboardType="number-pad"
    autoCapitalize="none"
  />
)

InputNumber.propTypes = {
  /** Defines mask characters's limit  */
  maxLength: PropTypes.number
}

InputNumber.defaultProps = {
  maxLength: 10
}

export default InputNumber
