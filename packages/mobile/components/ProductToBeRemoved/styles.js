import styled from 'styled-components/native'

import colors from '@smartcoop/styles/colors'

export const ContentName = styled.Text `
  font-weight: 600;
  font-size: 16px;
  color: ${ colors.darkGrey };
`
export const Container = styled.View `
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  width: 100%;
`

export const ProductQuantity = styled.Text `
  margin: 0;
  color: ${ colors.black };
  font-size: 16px;
`
