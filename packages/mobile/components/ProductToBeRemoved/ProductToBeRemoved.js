import React from 'react'

import PropTypes from 'prop-types'

import I18n from '@smartcoop/i18n'

import {
  ContentName,
  ProductQuantity,
  Container
} from './styles'

const ProductToBeRemoved = ({
  productName,
  quantity
}) => (
  <Container>
    <ContentName>
      {productName}
    </ContentName>
    <ProductQuantity>
      {quantity}
      <I18n>units</I18n>
    </ProductQuantity>
  </Container>
)

ProductToBeRemoved.propTypes = {
  productName: PropTypes.string,
  quantity: PropTypes.number
}

ProductToBeRemoved.defaultProps = {
  productName: null,
  quantity: null
}

export default ProductToBeRemoved
