import React from 'react'
import { SvgXml } from 'react-native-svg'

import PropTypes from 'prop-types'

const Icon = ({ icon, style, ...iconProps }) => icon && (
  <SvgXml style={ style } xml={ icon(iconProps) } />
)

Icon.propTypes = {
  icon: PropTypes.func.isRequired,
  size: PropTypes.number,
  style: PropTypes.any,
  color: PropTypes.string
}

Icon.defaultProps = {
  size: 24,
  style: {},
  color: undefined
}

export default Icon
