import React, { forwardRef } from 'react'

import { Form as Unform } from '@unform/mobile'
import PropTypes from 'prop-types'

import { FormProvider } from '@smartcoop/forms'

const Form = forwardRef((props, formRef) => (
  <FormProvider
    ref={ formRef }
    { ...props }
    UnformComponent={ Unform }
    style={ {
      zIndex: 1,
      width: '100%',
      paddingTop: 8,
      ...props.style
    } }
  />
))

Form.propTypes = {
  schemaConstructor: PropTypes.func,
  onSubmit: PropTypes.func,
  children: PropTypes.any,
  style: PropTypes.any
}

Form.defaultProps = {
  schemaConstructor: undefined,
  onSubmit: () => {},
  children: null,
  style: {}
}

export default Form
