import React, { useMemo, useCallback } from 'react'

import PropTypes from 'prop-types'

import filter from 'lodash/filter'
import isEmpty from 'lodash/isEmpty'
import map from 'lodash/map'

import Divider from '@smartcoop/mobile-components/Divider'
import Icon from '@smartcoop/mobile-components/Icon'

import {
  Container,
  Row,
  RowSelected,
  ItemRow,
  IconContainer,
  Label,
  ContainerLabel,
  Title
} from './styles'

const ItemSelection = (props) => {
  const { selectedIconColor, unselectedIconColor, options, title, unselectedIcon, selectedIcon, selected, onChange, filterFunction } = props

  const items = useMemo(
    () => {
      if (isEmpty(selected)) return options

      return filter(options, (option) => (filterFunction(option, selected)))
    },
    [filterFunction, options, selected]
  )

  const handleItemChange = useCallback(
    (item) => {
      onChange(item)
    },
    [onChange]
  )
  return (
    <Container>
      {!isEmpty(selected) && (
        <>
          <RowSelected onPress={ () => handleItemChange(selected) }>
            {selectedIcon &&
              <IconContainer>
                <Icon icon={ selectedIcon } size={ 25 } color={ selectedIconColor }/>
              </IconContainer>
            }
            <ContainerLabel>
              <Label>{ selected.label }</Label>
            </ContainerLabel>
          </RowSelected>

          <Divider />
        </>
      )}

      <Row style={ { paddingBottom: 7 } }>
        <Title>{ title }</Title>
      </Row>
      {
        map(items, (item) => (
          <ItemRow key={ item.value + item.label } onPress={ () => handleItemChange(item) }>
            {unselectedIcon &&
              <IconContainer>
                <Icon icon={ unselectedIcon } size={ 25 } color={ unselectedIconColor } />
              </IconContainer>
            }
            <ContainerLabel>
              <Label>{ item.label }</Label>
            </ContainerLabel>
          </ItemRow>
        ))
      }
    </Container>
  )
}

ItemSelection.propTypes = {
  onChange: PropTypes.func,
  options: PropTypes.array.isRequired,
  title: PropTypes.string,
  filterFunction: PropTypes.func,
  unselectedIcon: PropTypes.func,
  selectedIcon: PropTypes.func,
  selected: PropTypes.object,
  selectedIconColor: PropTypes.string,
  unselectedIconColor: PropTypes.string
}

ItemSelection.defaultProps = {
  onChange: (param) => (param),
  title: 'float',
  unselectedIcon: null,
  selectedIcon: null,
  filterFunction: (option, selected) => option.value !== selected.value,
  selected: {},
  selectedIconColor: undefined,
  unselectedIconColor: undefined
}

export default ItemSelection
