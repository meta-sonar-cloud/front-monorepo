import { Caption, Paragraph } from 'react-native-paper'

import styled from 'styled-components/native'

import colors from '@smartcoop/styles/colors'


export const Container = styled.View`
  width: 100%;
  flex-direction: column;
  margin: 0;
  padding: 0;
`

export const Row = styled.View`
  width: 100%;
  flex-direction: row;
  align-items: center;
`

export const ItemRow = styled.TouchableOpacity`
  width: 100%;

  flex-direction: row;
  align-items: center;
  align-self: flex-start;
  margin-bottom: 10px;

`

export const ContainerLabel = styled.View`
  width: 100%;
  flex-direction: row;
  flex-wrap: wrap;
  align-self: flex-start;
`

export const IconContainer = styled.View`
  padding-right: 5px;
`

export const Label = styled(Paragraph)`
  color: ${ colors.black };
  font-size: 14px;
  font-weight: 600;
`

export const RowSelected = styled(ItemRow)`
  margin-top: 15px;
  margin-bottom: 10px;
`

export const Title = styled(Caption)`
  color: ${ colors.black };
  font-weight: 700;
`

