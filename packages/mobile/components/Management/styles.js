import { Subheading, Paragraph } from 'react-native-paper'

import styled from 'styled-components/native'


export const Container = styled.View`
  display: flex;
  flex-direction: column;
`

export const ManagementName = styled(Subheading)`
  margin: 0;
  padding: 0;
`

export const Date = styled(Paragraph)`
`

export const FieldName = styled(Subheading)`
  padding-left: 5px;
  margin: 0;
  padding: 0;
`

export const DateContent = styled.View`
  display: flex;
  flex-direction: row;
  align-items: center;
  padding-left: 5px;
`

export const Header = styled.View`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  width: 100%;
  margin-bottom: 5px;
`

export const TextContent = styled.View`
  display: flex;
  flex-direction: row;
  align-items: center;
`
