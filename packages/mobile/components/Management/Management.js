import React, { useMemo } from 'react'
import { TouchableOpacity, View } from 'react-native'

import moment from 'moment/moment'
import PropTypes from 'prop-types'

import { checked, calendar } from '@smartcoop/icons'
import Icon from '@smartcoop/mobile-components/Icon'
import colors from '@smartcoop/styles/colors'
import { momentFriendlyDateFormat, momentBackDateFormat } from '@smartcoop/utils/dates'

import {
  Container,
  ManagementName,
  DateContent,
  TextContent,
  FieldName,
  Date,
  Header
} from './styles'

const Management = ({
  date,
  managementType,
  done,
  onPress,
  cropManagement,
  fieldName
}) => {

  const colorChecked = useMemo(
    () => !done ? colors.lightGrey : colors.green
    ,
    [done]
  )

  const colorDate = useMemo(
    () => {
      if(done) {
        return colors.grey
      }
      if(moment().format('YYYY-MM-DD') > date){
        return colors.red
      }
      return colors.green

    },
    [date, done]
  )

  const dateManagement = useMemo(
    () => done ? moment(done, momentBackDateFormat).format(momentFriendlyDateFormat) : moment(date, momentBackDateFormat).format(momentFriendlyDateFormat)
    ,
    [date, done]
  )

  const ListComponent = useMemo(
    () => onPress ? TouchableOpacity : View,
    [onPress]
  )

  return (
    <ListComponent onPress={ () => onPress && onPress(cropManagement) }>
      <Container>
        <Header>
          <TextContent>
            <Icon
              size={ 18 }
              icon={ checked }
              color={ colorChecked }
              style={ { marginRight: 10 } }
            />
            <ManagementName>{managementType}</ManagementName>
          </TextContent>
          {fieldName && (
            <FieldName>
              {fieldName}
            </FieldName>
          )}
        </Header>
        <DateContent>
          <Icon style={ { marginRight: 5 } } size={ 10 } icon={ calendar } color={ colorDate }/>
          <Date style={ { color: colorDate } }>{dateManagement}</Date>
        </DateContent>
      </Container>
    </ListComponent>
  )}

Management.propTypes = {
  date: PropTypes.string,
  managementType: PropTypes.string,
  done: PropTypes.string,
  onPress: PropTypes.func,
  cropManagement: PropTypes.object,
  fieldName: PropTypes.string
}

Management.defaultProps = {
  date: null,
  managementType: null,
  done: null,
  onPress: undefined,
  cropManagement: {},
  fieldName: null
}

export default Management
