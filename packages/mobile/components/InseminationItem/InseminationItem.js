import React from 'react'
import { Text } from 'react-native'

import moment from 'moment'
import PropTypes from 'prop-types'

import { pencil } from '@smartcoop/icons'
import Button from '@smartcoop/mobile-components/Button'
import Icon from '@smartcoop/mobile-components/Icon'
import { colors } from '@smartcoop/styles'
import { momentBackDateFormat, momentFriendlyDateFormat } from '@smartcoop/utils/dates'

import {
  Container,
  Row,
  Label,
  Box
} from './styles'

const InseminationItem = ({ earringCode, inseminationDate, bullName, bullCode, onEdit }) => (
  <Container >
    <Row>
      <Label>
        {earringCode}
      </Label>
      <Label>
        {moment(inseminationDate, momentBackDateFormat).format(momentFriendlyDateFormat)}
      </Label>
    </Row>
    <Row>
      <Box>
        <Text>{`${ bullName  }    ${ bullCode  }`}</Text>
      </Box>
      <Row>
        <Button
          variant="outlined"
          style={ { borderColor: colors.lightGrey, minWidth: 0, minHeight: 0 } }
          color={ colors.black }
          icon={ <Icon size={ 16 } icon={ pencil } color={ colors.black } /> }
          onPress={ onEdit }
        />
      </Row>
    </Row>
  </Container>
)

InseminationItem.propTypes = {
  bullCode: PropTypes.string,
  earringCode: PropTypes.string,
  inseminationDate: PropTypes.string,
  bullName: PropTypes.string,
  onEdit: PropTypes.func
}

InseminationItem.defaultProps = {
  bullCode: '',
  earringCode: '',
  inseminationDate: '',
  bullName: '',
  onEdit: () => {}
}

export default InseminationItem
