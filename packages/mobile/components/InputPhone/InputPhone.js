import React from 'react'

import phoneMask from '@smartcoop/forms/masks/phone.mask'
import TextField from '@smartcoop/mobile-components/TextField'

const InputPhone = (props) => (
  <TextField
    { ...props }
    setMask={ phoneMask }
    keyboardType="number-pad"
    autoCapitalize="none"
  />
)

export default InputPhone
