import React, { useCallback, useMemo, useRef, useState } from 'react'
import { TouchableOpacity } from 'react-native'

import moment from 'moment/moment'
import PropTypes from 'prop-types'


import { useDialog } from '@smartcoop/dialog'
import { calendar } from '@smartcoop/icons'
import DatePickerModal from '@smartcoop/mobile-components/DatePicker/DatePickerModal'
import Icon from '@smartcoop/mobile-components/Icon'
import TextField from '@smartcoop/mobile-components/TextField'
import colors from '@smartcoop/styles/colors'
import { momentBackDateFormat, momentFriendlyDateFormat } from '@smartcoop/utils/dates'

const InputDate = (props) => {
  const {
    value,
    onChange,
    minDate,
    maxDate,
    onChangeForm,
    pickerProps,
    ...rest
  } = props

  const { detached, disabled } = rest

  const { createDialog, removeDialog } = useDialog()

  const [calendarValue, setCalendarValue] = useState(value)

  const date = useMemo(
    () => detached ? value : calendarValue,
    [calendarValue, detached, value]
  )

  const inputRef = useRef(null)

  const transformRender = useCallback(
    (newDate) => {
      const newMoment = moment(newDate, momentBackDateFormat, true)
      if (newMoment.isValid()) {
        return newMoment.format(momentFriendlyDateFormat)
      }
      return newMoment._i
    },
    []
  )

  const transformValue = useCallback(
    (newDate) => {
      const newMoment = moment(newDate, momentFriendlyDateFormat, true)
      if (newMoment.isValid()) {
        return newMoment.format(momentBackDateFormat)
      }
      return newMoment._i
    },
    []
  )

  // only use it outside a form
  const handleInputChange = useCallback(
    (newDate) => {
      onChange(transformValue(newDate))
    },
    [onChange, transformValue]
  )

  // only use it inside a form
  const handleFieldChange = useCallback(
    (newDate) => {
      if (!detached) {
        if (newDate !== value && moment(newDate, momentBackDateFormat, true).isValid()) {
          setCalendarValue(newDate)
          onChangeForm(newDate)
        } else {
          setCalendarValue('')
          onChangeForm('')
        }
      }
    },
    [detached, onChangeForm, value]
  )

  const endAdornment = useMemo(
    () => (
      <TouchableOpacity
        onPress={ () => {
          if (!disabled) {
            createDialog({
              id: 'calendar-picker',
              Component: DatePickerModal,
              props: {
                title: 'Selecione uma data',
                minDate,
                maxDate,
                value: date,
                onChange: (newDate) => {
                  inputRef.current.setValue(moment(newDate).format(momentBackDateFormat))
                  removeDialog({ id: 'calendar-picker' })
                },
                ...pickerProps
              }
            })
          }
        } }
      >
        <Icon
          icon={ calendar }
          size={ 22 }
          color={ colors.text }
        />
      </TouchableOpacity>
    ),
    [createDialog, date, disabled, maxDate, minDate, pickerProps, removeDialog]
  )

  if (detached) {
    rest.value = date
    rest.onChange = handleInputChange
  } else {
    rest.transformValue = transformValue
    rest.onFieldValueChange = handleFieldChange
  }

  return (
    <TextField
      ref={ inputRef }
      type="datetime"
      maskOptions={ {
        format: momentFriendlyDateFormat
      } }
      transformRender={ transformRender }
      endAdornment={ endAdornment }
      { ...rest }
    />
  )
}

InputDate.propTypes = {
  value: PropTypes.string,
  onChange: PropTypes.func,
  onChangeForm: PropTypes.func,
  pickerProps: PropTypes.object,
  minDate: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.object
  ]),
  maxDate: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.object
  ])
}

InputDate.defaultProps = {
  value: '',
  onChangeForm: () => {},
  onChange: () => {},
  pickerProps: {},
  minDate: undefined,
  maxDate: undefined
}

export default InputDate
