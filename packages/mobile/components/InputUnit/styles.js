import styled from 'styled-components/native'

import colors from '@smartcoop/styles/colors'

export const AdornmentView = styled.View`
  min-width: 42px;
  padding: 0px 5px;
  flex: 0.9;
  justify-content: center;
  align-items: center;
  background-color: ${ colors.backgroundHtml };
  border-radius: 4px;
  margin-right: -9px;
  margin-top: -2px;
`

export const AdornmentText = styled.Text`
  color: ${ colors.darkGrey };
`
