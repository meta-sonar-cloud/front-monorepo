import React, { useMemo } from 'react'

import PropTypes from 'prop-types'

import InputFloat from '@smartcoop/mobile-components/InputFloat'
import InputNumber from '@smartcoop/mobile-components/InputNumber'

import { AdornmentView, AdornmentText } from './styles'

const InputUnit = (props) => {
  const { type, unit, ...rest } = props

  const InputType = useMemo(
    () => type === 'integer' ? InputNumber : InputFloat,
    [type],
    []
  )

  return (
    <InputType
      endAdornment={ (
        <AdornmentView>
          <AdornmentText>
            { unit }
          </AdornmentText>
        </AdornmentView>
      ) }
      { ...rest }
    />
  )
}

InputUnit.propTypes = {
  unit: PropTypes.string,
  type: PropTypes.string
}

InputUnit.defaultProps = {
  unit: null,
  type: 'float'
}

export default InputUnit
