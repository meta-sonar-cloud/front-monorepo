import React from 'react'
import { Text } from 'react-native'

import moment from 'moment'
import PropTypes from 'prop-types'

// import { pencil } from '@smartcoop/icons'
// import Button from '@smartcoop/mobile-components/Button'
// import Icon from '@smartcoop/mobile-components/Icon'
// import { colors } from '@smartcoop/styles'
import { momentBackDateFormat, momentFriendlyDateFormat } from '@smartcoop/utils/dates'

import {
  Container,
  Row,
  Label,
  Box
} from './styles'

const PregnancyDiagnosticItem = ({ pregnancyDiagnostic }) => (
  <Container>
    <Row>
      <Label>
        {pregnancyDiagnostic?.animal?.earring?.earringCode}
      </Label>
      <Label>
        {moment(pregnancyDiagnostic?.realizationDate, momentBackDateFormat).format(momentFriendlyDateFormat)}
      </Label>
    </Row>
    <Row>
      <Box>
        <Text>
          {pregnancyDiagnostic?.diagnosisType?.typeName}
        </Text>
        <Text>{pregnancyDiagnostic?.result === 'Positivo' ? 'Prenha' : pregnancyDiagnostic?.result}</Text>
      </Box>
      {/* <Button
        variant="outlined"
        style={ { borderColor: colors.lightGrey, minWidth: 0, minHeight: 0 } }
        color={ colors.black }
        icon={ <Icon size={ 16 } icon={ pencil } color={ colors.black } /> }
        onPress={ onEdit }
      /> */}
    </Row>
  </Container>
)

PregnancyDiagnosticItem.propTypes = {
  pregnancyDiagnostic: PropTypes.object
}

PregnancyDiagnosticItem.defaultProps = {
  pregnancyDiagnostic: {}
}

export default PregnancyDiagnosticItem
