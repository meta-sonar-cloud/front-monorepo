import hexToRgba from 'hex-to-rgba'
import styled from 'styled-components/native'

import { colors } from '@smartcoop/styles'
import fonts from '@smartcoop/styles/fonts'

export const Container = styled.View`
  flex-direction: row;
  box-shadow: 0px 0px 4.07652px rgba(0, 0, 0, 0.25);
  border-radius: 5px;
  font-weight: 600;
  font-size: 14px;
  color: ${ colors.darkGrey };
  background-color: ${ colors.white };
  width: 100%;
  margin-bottom: 10px;
  elevation: 5;
`

export const LeftSide = styled.View`
  background-color: ${ props => hexToRgba(props.backgroundColor, .15) };
  padding: 10px;
`

export const RightSide = styled.View`
  flex: 1;
  flex-wrap: wrap;
  padding: 5px 10px;
`

export const Line = styled.View`
  flex-direction: row;
  justify-content: space-between;
`

export const Big = styled.Text`
  font-size: 32px;
  color: ${ colors.darkGrey };
  font-weight: 700;
`

export const Unit = styled.Text`
  color: ${ colors.mediumGrey };
  margin-left: 5px;
`

export const Title = styled.Text`
  flex: 1;
  font-family: ${ fonts.fontFamilyMontserrat };
  color: ${ colors.mediumGrey };
  font-weight: 700;
`
