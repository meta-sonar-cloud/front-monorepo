import styled from 'styled-components/native'

import { colors } from '@smartcoop/styles'


export const Cell = styled.Text`
  width: 100%;
  margin: 0 10px;
  text-transform: uppercase;
`
export const Row = styled.View`
  width: 100%;
  flex-direction: row;
  justify-content: space-evenly;
  border: 1px solid ${ colors.grey };
  border-left-width: 0px;
  border-right-width: 0px;
  border-bottom-width: 0;
`

export const Header = styled(Row)`
  border: none;
`

export const Body = styled.ScrollView`
  flex: 1;
  height: 100%;
  width: 100%;
`

export const CellHeader = styled(Cell)`
  color: #595959;
  text-transform: capitalize;
`

export const Column = styled.View`
  flex-direction: column;
  padding: 10px;
  flex: 1;
`
export const Table = styled.View`
  width: 100%;
  border: 1px solid ${ colors.grey };
  border-left-width: 0;
  border-right-width: 0;
  border-top-width: 0;
`
