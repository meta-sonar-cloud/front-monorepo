import React from 'react'

import PropTypes from 'prop-types'

import get from 'lodash/get'
import map from 'lodash/map'

import {
  Table,
  Row,
  Header,
  Body,
  Column,
  Cell,
  CellHeader
} from './styles'

const DataTable = props => {
  const {
    data,
    columns
  } = props

  return (
    <Table>
      <Header>
        {
          map(columns, ({ field, title, style, headerStyle }) => (
            <Column key={ field } style={ style }>
              <CellHeader style={ headerStyle }>{ title }</CellHeader>
            </Column>
          ))
        }
      </Header>
      <Body>
        {map(data, (item, i) => (
          <Row key={ i }>
            {map(columns, ({ field, render, style = {}, cellStyle = {} }) => {
              const fieldData = get(item, field)
              let result = fieldData

              if (render) {
                result = render(fieldData, item)
              }

              return (
                <Column key={ field } style={ style }>
                  <Cell style={ cellStyle }>{ result }</Cell>
                </Column>
              )
            })}
          </Row>
        ))}
      </Body>

    </Table>
  )
}

DataTable.propTypes = {
  data: PropTypes.arrayOf(PropTypes.object).isRequired,
  columns: PropTypes.arrayOf(PropTypes.shape({
    title: PropTypes.string.isRequired,
    field: PropTypes.string.isRequired,
    render: PropTypes.func
  })).isRequired
}

export default DataTable
