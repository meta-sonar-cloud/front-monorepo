import styled from 'styled-components/native'

export const Link = styled.Text`
  font-size: 14px;
  text-decoration: underline;
`
