import React, { useState } from 'react'
import { TouchableOpacity } from 'react-native'

import PropTypes from 'prop-types'

import truncate from 'lodash/truncate'

import I18n from '@smartcoop/i18n'

import { Link } from './styles'

const ReadMore = ({ children, length, readLess }) =>  {
  const [truncated, setTruncated] = useState(length < children.length)
  const truncatedText = `${ truncate(children, { length }) } `

  return (
    <>
      {truncated ? truncatedText : children}
      {truncated && (
        <TouchableOpacity onPress={ () => setTruncated(!truncated) }>
          <I18n as={ Link }>
            read more
          </I18n>
        </TouchableOpacity>
      )}
      {truncated && readLess && (
        <TouchableOpacity onPress={ () => setTruncated(!truncated) }>
          <I18n as={ Link }>
            read less
          </I18n>
        </TouchableOpacity>
      )}
    </>
  )
}

ReadMore.propTypes = {
  children: PropTypes.string.isRequired,
  length: PropTypes.number.isRequired,
  readLess: PropTypes.bool
}

ReadMore.defaultProps = {
  readLess: false
}

export default ReadMore
