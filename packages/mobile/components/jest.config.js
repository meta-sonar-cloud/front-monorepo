const jestConfig = require('@smartcoop/jest-config')

module.exports = {
  ...jestConfig,
  preset: 'react-native',
  setupFiles: ['./jest.setup.js']
}
