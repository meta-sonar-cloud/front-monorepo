import { Paragraph, Caption, Subheading } from 'react-native-paper'

import styled from 'styled-components'

export const Container = styled.View`
  display: flex;
  flex-direction: row;
  width: 100%;
`

export const TextGroup = styled.View`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  flex: 1;
`

export const TextLeft = styled.View`
  margin-left: 10px;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  align-self: center;
`

export const TextRight = styled.View`
  display: flex;
  flex-direction: column;
  align-items: flex-end;
  justify-content: center;
  align-self: flex-start;
  margin-right: 10px;
`

export const Actions = styled.View`
  display: flex;
  flex-direction: row;
  justify-content: flex-end;
`

export const Text = styled(Paragraph)`
`

export const Price = styled(Subheading)`
  font-weight: bold;
`

export const Date = styled(Caption)`
  text-align: right;
  margin-bottom: 5px;
`
