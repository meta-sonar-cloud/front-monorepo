import React, { useMemo } from 'react'

import moment from 'moment/moment'
import PropTypes from 'prop-types'

import { isEmpty } from 'lodash'

import { useT } from '@smartcoop/i18n'
import {  pencil, trash } from '@smartcoop/icons'
import Button from '@smartcoop/mobile-components/Button'
import Icon from '@smartcoop/mobile-components/Icon'
import { colors } from '@smartcoop/styles'
import { momentBackDateFormat, momentFriendlyMonthYearFormat } from '@smartcoop/utils/dates'
import { formatCurrency } from '@smartcoop/utils/formatters'

import {
  Container,
  TextGroup,
  TextLeft,
  TextRight,
  Text,
  Actions,
  Date,
  Price
} from './styles'

const PriceDataItem = (props) => {
  const {
    priceData,
    onEdit,
    onDelete
  } = props

  const t = useT()

  const isCooperative = useMemo(
    () => !isEmpty(priceData.organizationId),
    [priceData]
  )

  const date = useMemo(
    () => moment(priceData.priceDate, momentBackDateFormat).format(momentFriendlyMonthYearFormat),
    [priceData.priceDate]
  )
  const price = useMemo(
    () => formatCurrency(priceData.price),
    [priceData.price]
  )

  return (
    <Container>
      <TextGroup>
        <TextLeft>
          <Price>{price}</Price>
          <Text>
            {`${ t('delivery', { howMany: 1 }) } ${ isCooperative ? t('cooperative', { howMany: 1 }) : t('third company', { howMany: 1 }) }`}
          </Text>
        </TextLeft>
        <TextRight>
          <Date>
            {date}
          </Date>
          <Actions>
            <Button
              variant="outlined"
              style={ { marginRight: 5, padding: 8, borderColor: colors.lightGrey } }
              icon={ <Icon size={ 14 } icon={ pencil } color={ colors.black } /> }
              backgroundColor={ colors.white }
              onPress={ onEdit }
              disabled={ isCooperative }
            />
            <Button
              variant="outlined"
              style={ { padding: 8, borderColor: colors.lightGrey } }
              icon={ <Icon size={ 14 } icon={ trash } color={ colors.red } /> }
              backgroundColor={ colors.white }
              onPress={ onDelete }
              disabled={ isCooperative }
            />
          </Actions>
        </TextRight>
      </TextGroup>
    </Container>
  )
}

PriceDataItem.propTypes = {
  priceData: PropTypes.object,
  onEdit: PropTypes.func,
  onDelete: PropTypes.func
}

PriceDataItem.defaultProps = {
  priceData: {},
  onEdit: () => {},
  onDelete: () => {}
}

export default PriceDataItem
