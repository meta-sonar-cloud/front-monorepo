import React, { useCallback, useMemo } from 'react'

import PropTypes from 'prop-types'

import { checked as checkedIcon, unchecked as uncheckedIcon } from '@smartcoop/icons'
import Icon from '@smartcoop/mobile-components/Icon'
import { secondary as secondaryColor } from '@smartcoop/styles/colors'

import {
  RadioTouchableContainer,
  Container,
  IconContainer,
  LabelContainer,
  Label
} from './styles'

const RadioButton = (props) => {
  const { checked, label, value, onChange, style, fontStyle, disabled } = props

  const onToggle = useCallback(
    () => {
      onChange({ value })
    },
    [onChange, value]
  )

  const selectIcon = useMemo(
    () => (
      checked ? checkedIcon : uncheckedIcon
    ),
    [checked]
  )

  const checkedStyle = useMemo(
    () => (
      checked ? { backgroundColor: secondaryColor } : { }
    ),
    [checked]
  )

  return (
    <Container style={ { ...checkedStyle, ...style } }>
      <RadioTouchableContainer disabled={ disabled } onPress={ onToggle }>
        <IconContainer>
          <Icon icon={ selectIcon } size={ 12 }/>
        </IconContainer>

        <LabelContainer>
          <Label style={ { ...fontStyle } }>{ label }</Label>
        </LabelContainer>
      </RadioTouchableContainer>
    </Container>
  )
}

RadioButton.propTypes = {
  checked: PropTypes.bool.isRequired,
  label: PropTypes.string.isRequired,
  disabled: PropTypes.bool,
  value: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.bool,
    PropTypes.string
  ]).isRequired,
  onChange: PropTypes.func.isRequired,
  style: PropTypes.object,
  fontStyle: PropTypes.object
}

RadioButton.defaultProps = {
  disabled: false,
  style: {},
  fontStyle: {}
}
export default RadioButton
