import { Paragraph } from 'react-native-paper'

import styled from 'styled-components/native'

import colors from '@smartcoop/styles/colors'

export const RadioTouchableContainer = styled.TouchableOpacity`
  display: flex;
  flex-direction: row;
`

export const Container = styled.View`
  background-color: ${ colors.backgroundHtml };
  border-radius: 5px;
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  min-height: 30px;
  padding: 5px;
`

export const IconContainer = styled.View`
  max-width: 20px;
  min-width: 20px;
  display: flex;
  justify-content: center;
  align-items: center;
`

export const LabelContainer = styled.View`
  display: flex;
  justify-content: center;
  padding-right: 5px;
`

export const Label = styled(Paragraph)`
`
