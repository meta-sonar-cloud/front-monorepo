import styled from 'styled-components/native'

import { colors } from '@smartcoop/styles'

export const Bold = styled.Text`
  font-weight: 700;
  color: ${ colors.black };
`

export const Text = styled.Text`
  font-weight: 400;
  font-size: 14px;
`

export const Actions = styled.View`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  background: white;
`

export const Buttons = styled.View`
  flex-direction: row;
`
