import React, { useMemo } from 'react'
import { View } from 'react-native'

import PropTypes from 'prop-types'


import {
  pencil
} from '@smartcoop/icons'
import Button from '@smartcoop/mobile-components/Button'
import Card from '@smartcoop/mobile-components/Card'
import Icon from '@smartcoop/mobile-components/Icon'
import { colors } from '@smartcoop/styles'
import { formatCep } from '@smartcoop/utils/formatters'

import {
  Actions,
  Bold,
  Buttons,
  Text
} from './styles'


const Address = (props) => {
  const {
    style,
    onEdit,
    fields
  } = props

  const streetWithNumber = useMemo(
    () => (
      fields.street
        ? `${ fields.street }${
          fields.number
            ? `, ${ fields.number }`
            : '' }`
        : null
    ),
    [fields.number, fields.street]
  )

  const cityStateCountry = useMemo(
    () => {
      const { city, state, country } = fields

      if (!city && !state && !country) return null

      return `${
        city || ''
      }${
        state
          ? `${ city ? '/' : '' }${ state }`
          : ''
      }${
        country
          ? ` ${ city || state ? '-' : '' } ${ country }`
          : ''
      }`
    },
    [fields]
  )

  const district = useMemo(
    () => (fields?.district || fields?.neighborhood), [fields]
  )
  const postalCode = useMemo(
    () => formatCep(fields.postalCode), [fields]
  )

  return (
    <Card childrenStyle={ { margin: 15 } } style={ style }>
      <Actions>
        <Bold>
          {streetWithNumber}
        </Bold>
        <Buttons>
          <Button
            variant="outlined"
            style={ { marginRight: 5, padding: 8, borderColor: colors.lightGrey } }
            icon={ <Icon size={ 14 } icon={ pencil } color={ colors.black } /> }
            backgroundColor={ colors.white }
            onPress={ onEdit }
          />
        </Buttons>
      </Actions>
      <View>
        <Text>{district}</Text>
        <Text>{cityStateCountry}</Text>
        <Text>{postalCode}</Text>
      </View>
    </Card>
  )
}

Address.propTypes = {
  style: PropTypes.object,
  fields: PropTypes.shape({
    companyDocument: PropTypes.string,
    complement: PropTypes.string,
    street: PropTypes.string,
    neighborhood: PropTypes.string,
    district: PropTypes.string,
    postalCode: PropTypes.string,
    city: PropTypes.string,
    state: PropTypes.string,
    country: PropTypes.string,
    number: PropTypes.oneOfType([PropTypes.string, PropTypes.number])
  }).isRequired,
  onEdit: PropTypes.func
}

Address.defaultProps = {
  style: {},
  onEdit: null
}

export default Address
