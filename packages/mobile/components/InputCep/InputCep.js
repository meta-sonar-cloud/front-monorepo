import React, { useCallback, useRef } from 'react'

import PropTypes from 'prop-types'

import cepMask from '@smartcoop/forms/masks/cep.mask'
import TextField from '@smartcoop/mobile-components/TextField'
import { searchCep } from '@smartcoop/services/apis/brasilApi'

const InputCep = (props) => {
  const { onChange, onAddressChange, ...otherProps } = props

  const fieldRef = useRef(null)

  const handleChange = useCallback(
    async (e) => {
      onChange(e)
      if (e.target.value.length === 9) {
        try {
          const address = await searchCep(e.target.value)
          onAddressChange(address)
        } catch (err) {
          fieldRef.current.setCustomError(err.message)
        }
      } else if (fieldRef.current.customError) {
        fieldRef.current.setCustomError(null)
      }
    },
    [onAddressChange, onChange]
  )

  return (
    <TextField
      ref={ fieldRef }
      { ...otherProps }
      setMask={ cepMask }
      onChange={ handleChange }
      keyboardType="number-pad"
      autoCapitalize="none"
    />
  )
}

InputCep.propTypes = {
  onChange: PropTypes.func,
  onAddressChange: PropTypes.func
}

InputCep.defaultProps = {
  onChange: () => {},
  onAddressChange: () => {}
}

export default InputCep
