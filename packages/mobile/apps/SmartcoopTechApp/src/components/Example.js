import React from 'react'
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  StatusBar,
  Text
} from 'react-native'
import { REACT_APP_AMBIENTE } from 'react-native-dotenv'
import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions
} from 'react-native/Libraries/NewAppScreen'

import I18n, { useT } from '@smartcoop/i18n'
import Button from '@smartcoop/mobile-components/Button'

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: Colors.lighter
  },
  engine: {
    position: 'absolute',
    right: 0
  },
  body: {
    backgroundColor: Colors.white
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: Colors.black
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: Colors.dark
  },
  highlight: {
    fontWeight: '700'
  },
  footer: {
    color: Colors.dark,
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right'
  }
})

const Example = () => {
  const t = useT()
  return (
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView>
        <ScrollView
          contentInsetAdjustmentBehavior="automatic"
          style={ styles.scrollView }
        >
          <Header />
          {global.HermesInternal == null ? null : (
            <View style={ styles.engine }>
              <Text style={ styles.footer }>Engine: Hermes</Text>
            </View>
          )}
          <View style={ styles.body }>
            <View style={ styles.sectionContainer }>
              <Text style={ styles.sectionTitle }>Step One</Text>
              <Text style={ styles.sectionDescription }>
                Edit <Text style={ styles.highlight }>Example.js</Text> to change this
                screen and then come back to see your edits.
              </Text>
              <I18n>hello world</I18n>
              <Button onPress={ () => console.warn(t('hello world')) }>
                content
              </Button>
              <Text>
                {REACT_APP_AMBIENTE}
              </Text>
            </View>
            <View style={ styles.sectionContainer }>
              <Text style={ styles.sectionTitle }>See Your Changes</Text>
              <Text style={ styles.sectionDescription }>
                <ReloadInstructions />
              </Text>
            </View>
            <View style={ styles.sectionContainer }>
              <Text style={ styles.sectionTitle }>Debug</Text>
              <Text style={ styles.sectionDescription }>
                <DebugInstructions />
              </Text>
            </View>
            <View style={ styles.sectionContainer }>
              <Text style={ styles.sectionTitle }>Learn More</Text>
              <Text style={ styles.sectionDescription }>
                Read the docs to discover what to do next:
              </Text>
            </View>
            <LearnMoreLinks />
          </View>
        </ScrollView>
      </SafeAreaView>
    </>
  )
}

export default Example
