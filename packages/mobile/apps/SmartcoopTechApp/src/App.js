import React from 'react'
import { View, Text } from 'react-native'

import { I18nProvider, dictionaries } from '@smartcoop/i18n'

import Example from '~/components/Example'

const App = () => (
  <I18nProvider
    language="pt-BR"
    dictionaries={ dictionaries }
    fallbackComponent={ <View /> }
    textComponent={ Text }
  >
    <Example />
  </I18nProvider>
)

export default App
