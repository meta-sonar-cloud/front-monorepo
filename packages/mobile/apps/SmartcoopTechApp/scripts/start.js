const { execTerminalCommand } = require('@smartcoop/env-config')

const processEnvFile = require('./utils/processEnvFile')

processEnvFile({
  callback: () => {
    const childProcess = execTerminalCommand('yarn react-native start')

    childProcess.on('error', error => {
      console.error(error)
    })

    childProcess.on('close', code => {
      if (code > 0) {
        console.error(`Command failed with code ${ code }`)
      }
    })
  }
})
