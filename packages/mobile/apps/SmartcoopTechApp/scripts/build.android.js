const path = require('path')

const { execTerminalCommand } = require('@smartcoop/env-config')

const processEnvFile = require('./utils/processEnvFile')

processEnvFile({
  callback: () => {
    const childProcess = execTerminalCommand(
      `${ process.platform !== 'powershell' ? './' : '' }gradlew assembleRelease`,
      { cwd: path.resolve(process.cwd(), 'android') }
    )

    childProcess.on('error', error => {
      console.error(error)
    })

    childProcess.on('close', code => {
      if (code > 0) {
        console.error(`Command failed with code ${ code }`)
      }
    })
  }
})
