/**
 * @format
 */

import { AppRegistry } from 'react-native'
import { setCustomText } from 'react-native-global-props'

import App from '~/App'

import { name as appName } from './app.json'

setCustomText({
  style: {
    fontFamily: 'OpenSans-Regular'
  }
})

AppRegistry.registerComponent(appName, () => App)
