/* eslint-disable no-return-assign */
const dotenv = require('dotenv')
const fs = require('fs')

const { execTerminalCommand } = require('@smartcoop/env-config')

const processEnvFile = require('./utils/processEnvFile')

processEnvFile({
  envPath: 'env/',
  callback: ({ originalEnvPath, envFile, argv }) => {

    const envConfig = dotenv.parse(fs.readFileSync(originalEnvPath))

    Object.entries(envConfig).map(
      ([configName, configValue]) => (process.env[configName] = configValue)
    )

    const childProcess = execTerminalCommand(`node ./scripts/firebase.ios.js --env ${ argv.env }`)

    childProcess.on('error', error => {
      console.error(error)
    })

    childProcess.on('close', code => {
      if (code > 0) {
        console.error(`Command failed with code ${ code }`)
      }

      const childProcess2 = execTerminalCommand(`env-cmd -f env/${ envFile } react-native bundle --entry-file index.js --platform ios --dev false --bundle-output ios/main.jsbundle --assets-dest ios`)

      childProcess2.on('error', error => {
        console.error(error)
      })

      childProcess2.on('close', async (code2) => {
        if (code2 > 0) {
          console.error(`Command failed with code ${ code2 }`)
        }
      })
    })
  }
})
