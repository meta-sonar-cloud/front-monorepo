const yargs = require('yargs')

const { execTerminalCommand } = require('@smartcoop/env-config')

const { argv } = yargs.usage('Usage: $0 <command> [options]').options({
  env: {
    choices: ['development', 'staging', 'production'],
    default: 'development',
    demandOption: false,
    describe: 'Environment',
    nargs: 1
  }
})

const childProcess = execTerminalCommand(`node ./scripts/firebase.ios.js --env ${ argv.env }`)

childProcess.on('error', error => {
  console.error(error)
})

childProcess.on('close', code => {
  if (code > 0) {
    console.error(`Command failed with code ${ code }`)
  }

  const childProcess2 = execTerminalCommand('react-native run-ios')

  childProcess2.on('error', error => {
    console.error(error)
  })

  childProcess2.on('close', code2 => {
    if (code2 > 0) {
      console.error(`Command failed with code ${ code2 }`)
    }
  })
})
