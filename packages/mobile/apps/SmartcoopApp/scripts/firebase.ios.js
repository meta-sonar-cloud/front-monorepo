const fs = require('fs')
const path = require('path')
const yargs = require('yargs')

const { argv } = yargs.usage('Usage: $0 <command> [options]').options({
  env: {
    choices: ['development', 'staging', 'production'],
    default: 'development',
    demandOption: false,
    describe: 'Environment',
    nargs: 1
  }
})

const exec = () => new Promise((resolve, reject) => {
  const from  = `./ios/firebase/GoogleService-Info.${ argv.env }.plist`
  const to = './ios/GoogleService-Info.plist'
  fs.copyFile(
    path.resolve(process.cwd(), from),
    path.resolve(process.cwd(), to),
    (error) => {
      if (error) {
        console.error(`error: ${ error.toString() }`)
        reject(error)
        return
      }
      console.log(`Firebase ios: ${ from } copied to ${ to }`)
      resolve()
    }
  )
})

exec()
