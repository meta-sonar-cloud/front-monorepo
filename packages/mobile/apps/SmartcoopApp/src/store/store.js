import { REACT_APP_AMBIENTE } from 'react-native-dotenv'

import { createStore, applyMiddleware } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension'
import { createLogger } from 'redux-logger'
import {
  offlineMiddleware,
  suspendSaga,
  consumeActionMiddleware
} from 'redux-offline-queue'
import { persistStore, REHYDRATE } from 'redux-persist'
import createSagaMiddleware, { END } from 'redux-saga'

import smartcoopApi from '@smartcoop/services/apis/smartcoopApi'
import { authenticatedMiddleware } from '@smartcoop/stores'
import { AuthenticationActions } from '@smartcoop/stores/authentication'

import reducers from './reducers'
import sagas from './sagas'

const middlewares = [authenticatedMiddleware]

if (REACT_APP_AMBIENTE === 'development' && __DEV__) {
  middlewares.push(createLogger({}))
}

const sagaMiddleware = createSagaMiddleware()

middlewares.push(offlineMiddleware({
  stateName: 'network',
  additionalTriggers: REHYDRATE
}))
middlewares.push(suspendSaga(sagaMiddleware))
middlewares.push(consumeActionMiddleware())

const store = createStore(
  reducers,
  composeWithDevTools(applyMiddleware(...middlewares))
)

store.close = () => store.dispatch(END)
store.persist = () => persistStore(store, {}, () => {
  store.dispatch(AuthenticationActions.initAuthSagas(store.dispatch))
})
store.runSagas = sagaMiddleware.run
store.runSagas(sagas)

/*
  Add header Authorization when store contains accessToken
  or remove when it not contains accessToken
*/
store.subscribe(() => {
  const { accessToken } = store.getState().authentication

  if (accessToken) {
    smartcoopApi.defaults.headers.common.Authorization = accessToken
  } else {
    const { Authorization, ...common } = smartcoopApi.defaults.headers.common || {}
    smartcoopApi.defaults.headers.common = common
  }
})

smartcoopApi.interceptors.response.use(
  response => response,
  (error) => {
    const {
      response: { status, config: { url } }
    } = error

    if (
      // (status === 403 || status === 401)
      (status === 401)
      && url !== '/auth/logout'
      && url !== '/auth/refresh-token?isLogout'
    ) {
      const { accessToken } = store.getState().authentication
      if (accessToken) {
        store.dispatch(AuthenticationActions.logout())
      }
    }

    return Promise.reject(
      // (status === 403 || status === 401)
      (status === 401)
        ? {
          ...error,
          message: 'Access denied! Please try to login again!'
        }
        : error
    )
  }
)

export default store
