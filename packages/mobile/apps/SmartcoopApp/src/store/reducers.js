import { combineReducers } from 'redux'
import { persistReducer } from 'redux-persist'
import autoMergeLevel2 from 'redux-persist/lib/stateReconciler/autoMergeLevel2'

import storage from '@react-native-community/async-storage'

import { blacklistTransform, circularTransform } from '@smartcoop/stores'
/* ducks from @smartcoop/stores */
import account from '@smartcoop/stores/account'
import animal from '@smartcoop/stores/animal'
import animalBirth from '@smartcoop/stores/animalBirth'
import animalPregnancyActions from '@smartcoop/stores/animalPregnancyActions'
import animalPregnancyDiagnostics from '@smartcoop/stores/animalPregnancyDiagnostics'
import authentication from '@smartcoop/stores/authentication'
import barter from '@smartcoop/stores/barter'
import dairyFarm from '@smartcoop/stores/dairyFarm'
import field from '@smartcoop/stores/field'
import fieldsMonitoration from '@smartcoop/stores/fieldsMonitoration'
import insemination from '@smartcoop/stores/insemination'
import lot from '@smartcoop/stores/lot'
import machine from '@smartcoop/stores/machine'
import messaging from '@smartcoop/stores/messaging'
import moduleReducer from '@smartcoop/stores/module'
import network from '@smartcoop/stores/network'
import organization from '@smartcoop/stores/organization'
import productGroup from '@smartcoop/stores/productGroup'
import productsBalance from '@smartcoop/stores/productsBalance'
import productsQuotation from '@smartcoop/stores/productsQuotation'
import productsWithdraw from '@smartcoop/stores/productsWithdraw'
import property from '@smartcoop/stores/property'
import salesOrders from '@smartcoop/stores/salesOrders'
import sateliteSvc from '@smartcoop/stores/sateliteSvc'
import securitiesMovement from '@smartcoop/stores/securitiesMovement'
import social from '@smartcoop/stores/social'
import technical from '@smartcoop/stores/technical'
import user from '@smartcoop/stores/user'
import weatherForecast from '@smartcoop/stores/weatherForecast'
import weatherStations from '@smartcoop/stores/weatherStations'

const rootPersistConfig = {
  key: 'root',
  storage,
  blacklist: ['network', 'app']
}

const authenticationPersistConfig = {
  key: 'authentication',
  storage
}

const userPersistConfig = {
  key: 'user',
  storage
}

const propertyPersistConfig = {
  key: 'property',
  storage
}

const fieldPersistConfig = {
  key: 'field',
  storage
}

const organizationPersistConfig = {
  key: 'organization',
  storage
}

const modulePersistConfig = {
  key: 'module',
  storage
}

const networkPersistConfig = {
  key: 'network',
  storage,
  transforms: [blacklistTransform('isConnected')]
}

const productsQuotationPersistConfig = {
  key: 'productsQuotation',
  storage
}

const productsBalancePersistConfig = {
  key: 'productsBalance',
  storage
}

const productsWithdrawPersistConfig = {
  key: 'productsWithdraw',
  storage
}

const accountPersistConfig = {
  key: 'account',
  storage
}

const socialPersistConfig = {
  key: 'social',
  storage
}

const salesOrdersConfig = {
  key: 'salesOrders',
  storage
}

const inseminationConfig = {
  key: 'insemination',
  storage
}

const securitiesMovementPersistConfig = {
  key: 'securitiesMovement',
  storage
}

const weatherForecastConfig = {
  key: 'weatherForecast',
  storage
}

const machinePersistConfig = {
  key: 'machine',
  storage
}

const sateliteSvcPersistConfig = {
  key: 'sateliteSvc',
  storage
}

const messagingPersistConfig = {
  key: 'messaging',
  storage,
  stateReconciler: autoMergeLevel2,
  transforms: [circularTransform]
}

const productGroupPersistConfig = {
  key: 'productGroup',
  storage
}

const dairyFarmPersistConfig = {
  key: 'dairyFarm',
  storage
}

const technicalPersistConfig = {
  key: 'technical',
  storage
}

const barterPersistConfig = {
  key: 'barter',
  storage
}

const fieldsMonitorationPersistConfig = {
  key: 'fieldsMonitoration',
  storage
}

const lotPersistConfig = {
  key: 'lot',
  storage
}

const animalPersistConfig = {
  key: 'animal',
  storage
}

const animalBirthPersistConfig = {
  key: 'animalBirth',
  storage
}

const weatherStationsPersistConfig = {
  key: 'weatherStations',
  storage
}

const animalPregnancyActionsPersistConfig = {
  key: 'animalPregnancyActions',
  storage
}

const animalPregnancyDiagnosticsPersistConfig = {
  key: 'animalPregnancyDiagnostics',
  storage
}

const rootReducer = combineReducers({
  network: persistReducer(networkPersistConfig, network),
  authentication: persistReducer(authenticationPersistConfig, authentication),
  organization: persistReducer(organizationPersistConfig, organization),
  user: persistReducer(userPersistConfig, user),
  property: persistReducer(propertyPersistConfig, property),
  field: persistReducer(fieldPersistConfig, field),
  module: persistReducer(modulePersistConfig, moduleReducer),
  productsQuotation: persistReducer(productsQuotationPersistConfig, productsQuotation),
  productsBalance: persistReducer(productsBalancePersistConfig, productsBalance),
  productsWithdraw: persistReducer(productsWithdrawPersistConfig, productsWithdraw),
  account: persistReducer(accountPersistConfig, account),
  social: persistReducer(socialPersistConfig, social),
  salesOrders: persistReducer(salesOrdersConfig, salesOrders),
  weatherForecast: persistReducer(weatherForecastConfig, weatherForecast),
  machine: persistReducer(machinePersistConfig, machine),
  securitiesMovement: persistReducer(securitiesMovementPersistConfig, securitiesMovement),
  sateliteSvc: persistReducer(sateliteSvcPersistConfig, sateliteSvc),
  messaging: persistReducer(messagingPersistConfig, messaging),
  insemination: persistReducer(inseminationConfig, insemination),
  productGroup: persistReducer(productGroupPersistConfig, productGroup),
  dairyFarm: persistReducer(dairyFarmPersistConfig, dairyFarm),
  technical: persistReducer(technicalPersistConfig, technical),
  barter: persistReducer(barterPersistConfig, barter),
  fieldsMonitoration: persistReducer(fieldsMonitorationPersistConfig, fieldsMonitoration),
  lot: persistReducer(lotPersistConfig, lot),
  animal: persistReducer(animalPersistConfig, animal),
  animalBirth: persistReducer(animalBirthPersistConfig, animalBirth),
  weatherStations: persistReducer(weatherStationsPersistConfig, weatherStations),
  animalPregnancyActions: persistReducer(animalPregnancyActionsPersistConfig, animalPregnancyActions),
  animalPregnancyDiagnostics: persistReducer(animalPregnancyDiagnosticsPersistConfig, animalPregnancyDiagnostics)
})

export default persistReducer(rootPersistConfig, rootReducer)
