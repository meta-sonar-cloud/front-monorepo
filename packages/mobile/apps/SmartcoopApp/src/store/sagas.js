import { eventChannel } from 'redux-saga'
import { all, spawn } from 'redux-saga/effects'

import NetInfo from '@react-native-community/netinfo'

/* sagas from @smartcoop/stores */
import sagasAccount from '@smartcoop/stores/account/sagasAccount'
import sagasAnimal from '@smartcoop/stores/animal/sagasAnimal'
import sagasAnimalBirth from '@smartcoop/stores/animalBirth/sagasAnimalBirth'
import sagasAnimalPregnancyActions from '@smartcoop/stores/animalPregnancyActions/sagasAnimalPregnancyActions'
import sagasAnimalPregnancyDiagnostics from '@smartcoop/stores/animalPregnancyDiagnostics/sagasAnimalPregnancyDiagnostics'
import sagasAuthentication from '@smartcoop/stores/authentication/sagasAuthentication'
import sagasBarter from '@smartcoop/stores/barter/sagasBarter'
import sagasDairyFarm from '@smartcoop/stores/dairyFarm/sagasDairyFarm'
import sagasField from '@smartcoop/stores/field/sagasField'
import sagasFieldsMonitoration from '@smartcoop/stores/fieldsMonitoration/sagasFieldsMonitoration'
import sagasInsemination from '@smartcoop/stores/insemination/sagasInsemination'
import sagasLot from '@smartcoop/stores/lot/sagasLot'
import sagasMachine from '@smartcoop/stores/machine/sagasMachine'
import sagasMessaging from '@smartcoop/stores/messaging/sagasMessaging'
import sagasModule from '@smartcoop/stores/module/sagasModule'
import { startWatchingNetworkConnectivity } from '@smartcoop/stores/network/sagasNetwork'
import sagasOrganization from '@smartcoop/stores/organization/sagasOrganization'
import sagasProductGroup from '@smartcoop/stores/productGroup/sagasProductGroup'
import sagasProductsBalance from '@smartcoop/stores/productsBalance/sagasProductsBalance'
import sagasProductsQuotation from '@smartcoop/stores/productsQuotation/sagasProductsQuotation'
import sagasProductsWithdraw from '@smartcoop/stores/productsWithdraw/sagasProductsWithdraw'
import sagasProperty from '@smartcoop/stores/property/sagasProperty'
import sagasSalesOrders from '@smartcoop/stores/salesOrders/sagasSalesOrders'
import sagaSateliteSvc from '@smartcoop/stores/sateliteSvc/sagaSateliteSvc'
import sagaSecuritiesMovement from '@smartcoop/stores/securitiesMovement/sagaSecuritiesMovement'
import sagasSocial from '@smartcoop/stores/social/sagasSocial'
import sagasTechnical from '@smartcoop/stores/technical/sagasTechnical'
import sagasUser from '@smartcoop/stores/user/sagasUser'
import sagaWeatherForecast from '@smartcoop/stores/weatherForecast/sagaWeatherForecast'
import sagasWeatherStations from '@smartcoop/stores/weatherStations/sagasWeatherStations'

export default function* root() {
  const networkChannel = eventChannel(emitter => {
    const unsubscribe = NetInfo.addEventListener(({ isConnected }) => emitter(isConnected))
    return unsubscribe
  })

  yield all([
    spawn(startWatchingNetworkConnectivity(networkChannel)),
    ...sagasAuthentication,
    ...sagasUser,
    ...sagasProperty,
    ...sagasField,
    ...sagasOrganization,
    ...sagasProductsQuotation,
    ...sagasProductsWithdraw,
    ...sagasProductsBalance,
    ...sagasAccount,
    ...sagasModule,
    ...sagasSocial,
    ...sagasSalesOrders,
    ...sagaWeatherForecast,
    ...sagasMachine,
    ...sagaSecuritiesMovement,
    ...sagaSateliteSvc,
    ...sagasMessaging,
    ...sagasProductGroup,
    ...sagasDairyFarm,
    ...sagasTechnical,
    ...sagasBarter,
    ...sagasFieldsMonitoration,
    ...sagasLot,
    ...sagasAnimal,
    ...sagasAnimalBirth,
    ...sagasWeatherStations,
    ...sagasAnimalPregnancyActions,
    ...sagasAnimalPregnancyDiagnostics,
    ...sagasInsemination
  ])
}
