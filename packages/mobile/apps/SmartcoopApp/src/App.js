import React from 'react'
import { View, Text } from 'react-native'
import { configureFonts, DefaultTheme, Provider as PaperProvider } from 'react-native-paper'

import { DialogProvider } from '@smartcoop/dialog'
import { I18nProvider, dictionaries } from '@smartcoop/i18n'
import { SnackbarsProvider } from '@smartcoop/mobile-components/Snackbar'
import { StoreProvider } from '@smartcoop/stores'
import colors from '@smartcoop/styles/colors'

import ScreensRouter from '~/screens/ScreensRouter'
import store from '~/store'

import 'moment/locale/pt-br'

const theme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    primary: colors.primary
  },
  fonts: configureFonts({
    ios: {
      bold: {
        fontFamily: 'OpenSans-Bold',
        fontWeight: 'normal'
      },
      regular: {
        fontFamily: 'OpenSans-Regular',
        fontWeight: 'normal'
      },
      medium: {
        fontFamily: 'OpenSans-Regular',
        fontWeight: 'normal'
      },
      light: {
        fontFamily: 'OpenSans-Light',
        fontWeight: 'normal'
      },
      thin: {
        fontFamily: 'OpenSans-Light',
        fontWeight: 'normal'
      }
    },
    android: {
      bold: {
        fontFamily: 'OpenSans-Bold',
        fontWeight: 'normal'
      },
      regular: {
        fontFamily: 'OpenSans-Regular',
        fontWeight: 'normal'
      },
      medium: {
        fontFamily: 'OpenSans-Regular',
        fontWeight: 'normal'
      },
      light: {
        fontFamily: 'OpenSans-Light',
        fontWeight: 'normal'
      },
      thin: {
        fontFamily: 'OpenSans-Light',
        fontWeight: 'normal'
      }
    }
  })
}

// eslint-disable-next-line react/prop-types
const Providers = ({ children }) => (
  <StoreProvider store={ store }>
    <I18nProvider
      language="pt-BR"
      dictionaries={ dictionaries }
      fallbackComponent={ <View /> }
      textComponent={ Text }
    >
      <PaperProvider theme={ theme }>
        <SnackbarsProvider>
          <DialogProvider>
            {children}
          </DialogProvider>
        </SnackbarsProvider>
      </PaperProvider>
    </I18nProvider>
  </StoreProvider>
)

const App = () => (
  <Providers>
    <ScreensRouter />
  </Providers>
)

export default App
