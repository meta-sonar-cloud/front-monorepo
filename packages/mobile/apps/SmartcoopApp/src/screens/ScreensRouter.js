import React, { useEffect, useCallback } from 'react'
import { Alert, StatusBar } from 'react-native'
import { useSelector, useDispatch } from 'react-redux'

import 'react-native-gesture-handler'

import { NavigationContainer } from '@react-navigation/native'

import debounce from 'lodash/debounce'

import { useT } from '@smartcoop/i18n'
import Authenticated from '@smartcoop/mobile-containers/screens/authenticated'
import Guest from '@smartcoop/mobile-containers/screens/guest'
import smartcoopApi from '@smartcoop/services/apis/smartcoopApi'
import { useSnackbar } from '@smartcoop/snackbar'
import { selectAuthenticated } from '@smartcoop/stores/authentication/selectorAuthentication'
import { selectIsConnected } from '@smartcoop/stores/network/selectorNetwork'
import colors from '@smartcoop/styles/colors'

const ScreensRouter = () => {
  const snackbar = useSnackbar()
  const t = useT()

  const dispatch = useCallback(useDispatch(), [])

  const authenticated = useSelector(selectAuthenticated)
  const isConnected = useSelector(selectIsConnected)

  const createSnackbarError = useCallback(debounce(snackbar.error, 1000), [])

  useEffect(() => {
    if (!isConnected) {
      Alert.alert(
        'Sem conexão com o servidor',
        '\nAlgumas ações ficarão indisponíveis até você retornar à internet.'
      )
    }
  }, [dispatch, isConnected])

  useEffect(() => {
    smartcoopApi.interceptors.response.use(
      (response) => response,
      (error) => {
        const { response } = error
        if (
          response.data?.message &&
          response.status !== 404 &&
          response.status !== 403
        ) {
          createSnackbarError(t(response.data.message))
        }
        return Promise.reject(error)
      }
    )
  }, [createSnackbarError, snackbar, t])

  return (
    <>
      <StatusBar
        barStyle={ authenticated ? 'light-content' : 'dark-content' }
        backgroundColor={ authenticated ? colors.primary : colors.backgroundHtml }
      />
      <NavigationContainer>
        {authenticated ? <Authenticated /> : <Guest />}
      </NavigationContainer>
    </>
  )
}

export default ScreensRouter
