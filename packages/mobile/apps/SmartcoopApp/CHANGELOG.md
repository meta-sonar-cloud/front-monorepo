# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [0.18.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.17.0...v0.18.0) (2021-04-19)


### Bug Fixes

* **docuemntos#1766:** ajustes mapa ([f2306ab](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f2306abda00fc359d42696faeafb74263d437be1)), closes [docuemntos#1766](http://gitlab.meta.com.br/docuemntos/issues/1766)
* **documentos#1702:** request permission ([34df4d0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/34df4d0c45500e58224ce62fc50ad28aa17715f0)), closes [documentos#1702](http://gitlab.meta.com.br/documentos/issues/1702)
* **documentos#1750:** adjustments ([28966d7](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/28966d704546ed37e849552d6fddf196ce808568)), closes [documentos#1750](http://gitlab.meta.com.br/documentos/issues/1750)
* **documentos#1755:** adjustments ([096f90b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/096f90b47500de13e083e959f99be6f044dec0b6)), closes [documentos#1755](http://gitlab.meta.com.br/documentos/issues/1755)
* **fix info.plist:** report apple ([b44b3e4](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b44b3e43ab11f098fccacac6f8c41d3853ffbd0e))
* **navigation:** technical module navigation ([4333c69](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4333c693c2a865f66817fa5b1f4e970d42bd9478))
* **performance:** remove redux logs when mode release ([3b62bcb](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3b62bcbe651bc351ac1cf0aa8279a54c33509daa))





# [0.17.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.16.0...v0.17.0) (2021-04-15)


### Bug Fixes

* **documentos#1645:** change technical menu ([976aa56](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/976aa567ab6dc68da7c6cbda8024e8111bfe4901)), closes [documentos#1645](http://gitlab.meta.com.br/documentos/issues/1645)





# [0.16.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.15.0...v0.16.0) (2021-04-13)


### Bug Fixes

* **ajustes finos:** ajustes finos da aplicação ([eefa46f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/eefa46fe613632b216df51736217149a8027dd16))
* **ajustes tambo:** ajustes em fluxos do tambo e ativacao de usuarios ([8fe2e56](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8fe2e5628f91a6d809e81142daba2dded80c38c4))
* **auth:** apply on mobile app ([5b37097](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/5b37097a522647e08df0b39f94fef407bc08d58c))
* **documentos#1552:** added debounce to snackbar.error from api error ([15a2992](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/15a2992e9d6872c7620780ee223235c60277df7f)), closes [documentos#1552](http://gitlab.meta.com.br/documentos/issues/1552)
* **documentos#1576:** ajustes conversao numerica ([035a7c2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/035a7c29bcaef1da263db55cd90cc61ebf959d98)), closes [documentos#1576](http://gitlab.meta.com.br/documentos/issues/1576)
* **documentos#1586:** correções e ajustes finos ([e6dbcc0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e6dbcc0d7966935a77519dbb4d8077d3bafcf3c5)), closes [documentos#1586](http://gitlab.meta.com.br/documentos/issues/1586)
* **documentos#1615:** adicionado loading mapa de chuva ([fe0a93f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/fe0a93f5ed54463062911ea58da0e5a395319cbc)), closes [documentos#1615](http://gitlab.meta.com.br/documentos/issues/1615)
* **documentos#1632:** ajustes finos app ([0e0ffdb](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0e0ffdbce9e4c4ab1c84e30d092d1cd16f83967d)), closes [documentos#1632](http://gitlab.meta.com.br/documentos/issues/1632)
* **documentos#1661:** label adjustments ([127efad](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/127efadd69c6afe28c674c9cf351b2adfc0e6283)), closes [documentos#1661](http://gitlab.meta.com.br/documentos/issues/1661)


### Features

* **documentos#1650:** calving in mobile ([f1218c7](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f1218c7a11e6444eee6c4210889ba2eba91c1629)), closes [documentos#1650](http://gitlab.meta.com.br/documentos/issues/1650)
* merge com a develop ([702e143](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/702e1434c3eab83403ccbd51d34a6340a05613b9))
* **#1484:** create store weather station ([8c38c7c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8c38c7c1bb4b1c1d834d4d7d6509b97095eb8492)), closes [#1484](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1484) [#484](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/484)
* **#1531 #1532:** create list animals, create and edit ([03bc1a2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/03bc1a23a48639d11f58f59faa7868a05a34984f)), closes [#1531](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1531) [#1532](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1532) [#499](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/499)
* **#1531 #1532:** create list animals, create and edit ([97f0423](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/97f04231c8983a8789ece8acba3091fe08fe6591)), closes [#1531](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1531) [#1532](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1532) [#499](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/499)
* **docuemntos#1558:** ajustes visual datas ([d563dd4](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d563dd4971e826303f748cc9371207b27d89a6d9)), closes [docuemntos#1558](http://gitlab.meta.com.br/docuemntos/issues/1558)
* **documentos#1316:** added new action ([fc08c4c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/fc08c4cceb5d079c4aaf4449b3f0e12361b2e32c)), closes [documentos#1316](http://gitlab.meta.com.br/documentos/issues/1316)
* **documentos#1316:** added new action ([1ca5cd5](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1ca5cd51161eba75e407cf36c47d3a0b5d0e4344)), closes [documentos#1316](http://gitlab.meta.com.br/documentos/issues/1316)
* **documentos#1495:** modals, saga and item ([a04f400](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a04f400ff9eb8feddfbef787e6b8fd7121985631)), closes [documentos#1495](http://gitlab.meta.com.br/documentos/issues/1495)
* **documentos#1579:** mobile insemination ([1bf225d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1bf225d6f5ffbafb10226bf7c5c77b13a03ed58e)), closes [documentos#1579](http://gitlab.meta.com.br/documentos/issues/1579)
* **documentos#1579:** mobile insemination ([10c2ff2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/10c2ff2171498fa004d5fc58ac3ba9266347753a)), closes [documentos#1579](http://gitlab.meta.com.br/documentos/issues/1579)


### BREAKING CHANGES

* **ajustes tambo:** \







**Note:** Version bump only for package @smartcoop/app





# [0.15.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.12.0-alpha.1...v0.15.0) (2021-03-23)


### Bug Fixes

* **documentos#1447:** incrmenetada versao ios ([049fdb9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/049fdb9592837444132df02e1f832b475c066dd7)), closes [documentos#1447](http://gitlab.meta.com.br/documentos/issues/1447)
* **documentos#1463:** ajustes dimensao mapa ([e0213e6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e0213e6afa98458c98dd38cb79abafa81898f77b)), closes [documentos#1463](http://gitlab.meta.com.br/documentos/issues/1463)
* **field indicators layout:** field indicators layout ([dd6d6c7](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/dd6d6c734ef7d398b70042bf7dc5e391abc30dbc))
* **keyboard hiding inputs:** keyboard hiding inputs ([ca76f6e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ca76f6ebd147717813ba4ee336b955c600796059))


### Features

* resolve [#1523](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1523) ([bf353c3](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/bf353c3e7fceecbc43a53bd729d0b13e82c0ed40))
* **documentos#1323:** now acceptterm modal in mobile renders HTML ([3df0efc](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3df0efc569b88ecc21861769a164f8796a9c7c7f)), closes [documentos#1323](http://gitlab.meta.com.br/documentos/issues/1323)
* **documentos#1399:** adds v1/ suffix in api urls ([bbc3d71](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/bbc3d71a3089018ec73fc225f8c9ff8aadb24294))
* **documentos#1463:** ajustes finos mapa de chuva ([b25fc96](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b25fc9669fad05fb451c83ffb378bcd72038d3c5)), closes [documentos#1463](http://gitlab.meta.com.br/documentos/issues/1463)
* **documentos#885:** changed field screen ([4299f89](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4299f898298c9641e4e63098dfdcf85a5633c11d)), closes [documentos#885](http://gitlab.meta.com.br/documentos/issues/885)





# [0.14.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.12.0-alpha.1...v0.14.0) (2021-03-23)


### Bug Fixes

* **documentos#1447:** incrmenetada versao ios ([049fdb9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/049fdb9592837444132df02e1f832b475c066dd7)), closes [documentos#1447](http://gitlab.meta.com.br/documentos/issues/1447)
* **documentos#1463:** ajustes dimensao mapa ([e0213e6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e0213e6afa98458c98dd38cb79abafa81898f77b)), closes [documentos#1463](http://gitlab.meta.com.br/documentos/issues/1463)
* **field indicators layout:** field indicators layout ([dd6d6c7](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/dd6d6c734ef7d398b70042bf7dc5e391abc30dbc))
* **keyboard hiding inputs:** keyboard hiding inputs ([ca76f6e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ca76f6ebd147717813ba4ee336b955c600796059))


### Features

* resolve [#1523](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1523) ([bf353c3](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/bf353c3e7fceecbc43a53bd729d0b13e82c0ed40))
* **documentos#1323:** now acceptterm modal in mobile renders HTML ([3df0efc](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3df0efc569b88ecc21861769a164f8796a9c7c7f)), closes [documentos#1323](http://gitlab.meta.com.br/documentos/issues/1323)
* **documentos#1399:** adds v1/ suffix in api urls ([bbc3d71](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/bbc3d71a3089018ec73fc225f8c9ff8aadb24294))
* **documentos#1463:** ajustes finos mapa de chuva ([b25fc96](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b25fc9669fad05fb451c83ffb378bcd72038d3c5)), closes [documentos#1463](http://gitlab.meta.com.br/documentos/issues/1463)
* **documentos#885:** changed field screen ([4299f89](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4299f898298c9641e4e63098dfdcf85a5633c11d)), closes [documentos#885](http://gitlab.meta.com.br/documentos/issues/885)





# [0.13.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.12.0-alpha.1...v0.13.0) (2021-03-23)


### Bug Fixes

* **documentos#1447:** incrmenetada versao ios ([049fdb9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/049fdb9592837444132df02e1f832b475c066dd7)), closes [documentos#1447](http://gitlab.meta.com.br/documentos/issues/1447)
* **documentos#1463:** ajustes dimensao mapa ([e0213e6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e0213e6afa98458c98dd38cb79abafa81898f77b)), closes [documentos#1463](http://gitlab.meta.com.br/documentos/issues/1463)
* **field indicators layout:** field indicators layout ([dd6d6c7](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/dd6d6c734ef7d398b70042bf7dc5e391abc30dbc))
* **keyboard hiding inputs:** keyboard hiding inputs ([ca76f6e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ca76f6ebd147717813ba4ee336b955c600796059))


### Features

* resolve [#1523](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1523) ([bf353c3](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/bf353c3e7fceecbc43a53bd729d0b13e82c0ed40))
* **documentos#1323:** now acceptterm modal in mobile renders HTML ([3df0efc](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3df0efc569b88ecc21861769a164f8796a9c7c7f)), closes [documentos#1323](http://gitlab.meta.com.br/documentos/issues/1323)
* **documentos#1399:** adds v1/ suffix in api urls ([bbc3d71](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/bbc3d71a3089018ec73fc225f8c9ff8aadb24294))
* **documentos#1463:** ajustes finos mapa de chuva ([b25fc96](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b25fc9669fad05fb451c83ffb378bcd72038d3c5)), closes [documentos#1463](http://gitlab.meta.com.br/documentos/issues/1463)
* **documentos#885:** changed field screen ([4299f89](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4299f898298c9641e4e63098dfdcf85a5633c11d)), closes [documentos#885](http://gitlab.meta.com.br/documentos/issues/885)





# [0.12.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.12.0-alpha.1...v0.12.0) (2021-03-23)


### Bug Fixes

* **documentos#1447:** incrmenetada versao ios ([049fdb9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/049fdb9592837444132df02e1f832b475c066dd7)), closes [documentos#1447](http://gitlab.meta.com.br/documentos/issues/1447)
* **documentos#1463:** ajustes dimensao mapa ([e0213e6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e0213e6afa98458c98dd38cb79abafa81898f77b)), closes [documentos#1463](http://gitlab.meta.com.br/documentos/issues/1463)
* **field indicators layout:** field indicators layout ([dd6d6c7](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/dd6d6c734ef7d398b70042bf7dc5e391abc30dbc))
* **keyboard hiding inputs:** keyboard hiding inputs ([ca76f6e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ca76f6ebd147717813ba4ee336b955c600796059))


### Features

* resolve [#1523](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1523) ([bf353c3](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/bf353c3e7fceecbc43a53bd729d0b13e82c0ed40))
* **documentos#1323:** now acceptterm modal in mobile renders HTML ([3df0efc](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3df0efc569b88ecc21861769a164f8796a9c7c7f)), closes [documentos#1323](http://gitlab.meta.com.br/documentos/issues/1323)
* **documentos#1399:** adds v1/ suffix in api urls ([bbc3d71](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/bbc3d71a3089018ec73fc225f8c9ff8aadb24294))
* **documentos#1463:** ajustes finos mapa de chuva ([b25fc96](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b25fc9669fad05fb451c83ffb378bcd72038d3c5)), closes [documentos#1463](http://gitlab.meta.com.br/documentos/issues/1463)
* **documentos#885:** changed field screen ([4299f89](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4299f898298c9641e4e63098dfdcf85a5633c11d)), closes [documentos#885](http://gitlab.meta.com.br/documentos/issues/885)





# [0.12.0-alpha.1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.12.0-alpha.0...v0.12.0-alpha.1) (2021-02-22)


### Bug Fixes

* **auth:** fixed multiples refresh token ([b976597](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b976597b48bfce6639d77453f295a8a3437d836a))
* **documentos#1209:** fixed firebase lifecycle ([831d393](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/831d3934bad4dd29e794559504a2c3b87dd63b20))


### Features

* **1220:** ajuste paleta de cores ([1af336d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1af336d4b9c5def20ca58cc26ac6a43ad218f45a))
* **documentos#1115:** created barter store and api resource ([9a35185](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9a3518557cc700310607d6920ae6f739a4733bb1)), closes [documentos#1115](http://gitlab.meta.com.br/documentos/issues/1115)
* **documentos#535:** finished home screen for mobile ([b595eee](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b595eeed9efc2c6371bb04276a22abfd74a1d9db)), closes [documentos#535](http://gitlab.meta.com.br/documentos/issues/535)





# [0.12.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.11.0...v0.12.0-alpha.0) (2021-02-12)

**Note:** Version bump only for package @smartcoop/app





# [0.11.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.11.0-alpha.0...v0.11.0) (2021-02-12)

**Note:** Version bump only for package @smartcoop/app





# [0.11.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.10.0...v0.11.0-alpha.0) (2021-02-05)


### Bug Fixes

* general sales orders fixes ([0b06c10](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0b06c10a0c82fd30d2ab34dd2132a95e6f5ee563))





# [0.10.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.10.0-alpha.0...v0.10.0) (2021-02-01)


### Bug Fixes

* **firebase:** authentication ([ff7ad04](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ff7ad04bcc129940ea3b43480e31964dc61e9d6b))
* **firebase:** fixed on web ([e048482](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e04848292e5f0c5b775a497e364b39ec0ea3cf8d))
* **mobile:** fixed mobile envs ([e4dc3c8](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e4dc3c8feb2b1e3f636a86fdcda870a757f0bd29))
* **mobile:** script build:ios ([a4d3cde](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a4d3cde1b75c623d75180b4b7aeba12894961185))
* **orientation:** ajustada orientacao da tela ([02cb25d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/02cb25de1b12cd805301dce8ce10c2367ed5e088))
* onboarding map location ([ed3d816](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ed3d816045f3b4d6312bd316e7cf063b14198ef3))
* **release:** fixed docs ([c201b35](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c201b35db9682fd1dbc5e65cd3392622ba871b3b))


### Features

* **documentos#1002:** publicacao em loja ([2089649](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2089649d0cc86c33fbbd3108a78aae19dc54f68e)), closes [documentos#1002](http://gitlab.meta.com.br/documentos/issues/1002)
* **documentos#1006:** created technical portfolio screen ([8f964b7](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8f964b73604e5520e9b956cca6855770abc6a89c)), closes [documentos#1006](http://gitlab.meta.com.br/documentos/issues/1006)
* **documentos#987:** declares reducer and saga ([e528774](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e528774cb79a78fc86bc981ab4756898f2c1e2b1)), closes [documentos#987](http://gitlab.meta.com.br/documentos/issues/987)





# [0.10.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.9.0...v0.10.0-alpha.0) (2021-01-19)

**Note:** Version bump only for package @smartcoop/app





# [0.9.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.8.2...v0.9.0) (2021-01-19)



### Bug Fixes

* **documentos#869:** also fixed documentos[#942](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/942) ([35481fb](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/35481fb256a5fc5a8b9a659b8acc86befbd04c3f))


### Features

* added sales orders forms ([b1e3fe8](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b1e3fe88414dba234ec6c9deff8d6cb7c3616f5a))
* **documentos#299:** adds slider package in mobile ([15e5cce](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/15e5cce41e93b8625b68c83277af274e6d22c4f7)), closes [documentos#299](http://gitlab.meta.com.br/documentos/issues/299)
* **documentos#299:** adds slider package in react native ([dca5077](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/dca5077c69e1091958c2e318e718a426cac94250)), closes [documentos#299](http://gitlab.meta.com.br/documentos/issues/299)
* **documentos#836:** added notifications for web ([0b7388c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0b7388ca6ec0da439a7acf516d50f4954f224f9c)), closes [documentos#836](http://gitlab.meta.com.br/documentos/issues/836)
* **documentos#836:** finished firebase integration ([f6db099](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f6db099715a083b74d4559d0c064c8d0e212ae1c)), closes [documentos#836](http://gitlab.meta.com.br/documentos/issues/836)
* **documentos#836:** finished mobile list ([6cbf8b3](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/6cbf8b38ee2ef54367d0503f607dc38386ca56e8)), closes [documentos#836](http://gitlab.meta.com.br/documentos/issues/836)
* **documentos#836:** firebase login and logout for mobile ([fbd31c4](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/fbd31c47a6b6a9d1d255945a6091b0376aa31fbb)), closes [documentos#836](http://gitlab.meta.com.br/documentos/issues/836)





## [0.8.2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.8.1...v0.8.2) (2021-01-06)

**Note:** Version bump only for package @smartcoop/app





## [0.8.1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.9.0-alpha.0...v0.8.1) (2021-01-06)

**Note:** Version bump only for package @smartcoop/app





# [0.9.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.8.0...v0.9.0-alpha.0) (2021-01-05)

**Note:** Version bump only for package @smartcoop/app





# [0.8.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.8.0-alpha.0...v0.8.0) (2021-01-05)


### Bug Fixes

* **android:** fixed maps ([00108bf](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/00108bf9b016eb3a1a75a0fb154d86363805bd29))
* **documentos#321:** implementada satelite mobile ([7a6f77e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7a6f77e9aeb52482b60c4524c85dd47019a192db)), closes [documentos#321](http://gitlab.meta.com.br/documentos/issues/321)
* **documentos#491:** app crash ([3dfd3a3](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3dfd3a31d2fbcbbeb3c698575d49ec0dab8bc93a)), closes [documentos#491](http://gitlab.meta.com.br/documentos/issues/491)
* **publicacao-appstore:** ajustes de imagens ([ce23f2e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ce23f2e340669d3ee9a92686c711236b8c5f1943))


### Features

* **321:** previsao do tempo mobile ([0db0a4b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0db0a4b8761cd1deb3c498313fcd0de1e3409fd7))
* **app-store:** ajustes para publicacao na app store ([5694641](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/56946417be37325719ddaa0285f7d92341f75e79))
* **docuementos#491:** created sales orders actions ([e1e7778](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e1e7778ef63bae23f81f15e0040540a8944a037f)), closes [docuementos#491](http://gitlab.meta.com.br/docuementos/issues/491)
* **documentos#176 documentos#143:** product balance and withdraw list ([076dae2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/076dae26e34aecb1a671e86e4d40eb574521c4db)), closes [documentos#176](http://gitlab.meta.com.br/documentos/issues/176) [documentos#143](http://gitlab.meta.com.br/documentos/issues/143)
* **documentos#300:** created refresh token mechanism ([54ee2f1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/54ee2f1626aee3e6c60d7a1412305fce7fef5d01)), closes [documentos#300](http://gitlab.meta.com.br/documentos/issues/300)
* **documentos#506:** adds missing comma in sagas.js ([b73c618](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b73c61881fb7051b12e4aca358921d1b0931cd96)), closes [documentos#506](http://gitlab.meta.com.br/documentos/issues/506)
* **documentos#506:** declares store in mobile and web ([f745b95](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f745b9544db93a59b68769d4c4ccff1c36b39387)), closes [documentos#506](http://gitlab.meta.com.br/documentos/issues/506)
* **documentos#506:** declares store in mobile and web ([5dc34d2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/5dc34d209ef99d39b81dce052d2f4d31a1bfee32)), closes [documentos#506](http://gitlab.meta.com.br/documentos/issues/506)
* **documentos#506:** declares store in mobile and web ([ac196c9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ac196c91072fe67957b5300fa126494908315a3b)), closes [documentos#506](http://gitlab.meta.com.br/documentos/issues/506)
* **documentos#506:** fixing mobile navigation ([508ca89](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/508ca89dad43d2903db28d1c06e91fe5a11dae56))
* **documentos#687:** created Chart component for mobile ([b0fba2b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b0fba2b4f1be4054ec927ed5dde77178bef16803)), closes [documentos#687](http://gitlab.meta.com.br/documentos/issues/687)
* **documentos#71:** created securities movement actions ([ae0a0bc](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ae0a0bc512165ea38731e3ab71b17fcc4d3e0b76)), closes [documentos#71](http://gitlab.meta.com.br/documentos/issues/71)





# [0.8.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.7.0...v0.8.0-alpha.0) (2020-12-11)

**Note:** Version bump only for package @smartcoop/app





# [0.7.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.7.0-alpha.0...v0.7.0) (2020-12-11)


### Bug Fixes

* **fields:** styles ([a3545ab](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a3545ab046b6fd6ade53dbcf07771657161fde0b))


### Features

* **documentos#393:** social network finished on mobile devices ([e838996](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e838996f44d0b26d857c47b062b6b1ecf0bdb809)), closes [documentos#393](http://gitlab.meta.com.br/documentos/issues/393)
* **documentos#402:** editing growing season ([321da9c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/321da9cb75b830d048c43afa491b4f2911e58b03)), closes [documentos#402](http://gitlab.meta.com.br/documentos/issues/402)
* **documentos#451:** created publish screen for mobile ([626b575](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/626b575f4021c206b0900c884d2f943d5238c47f)), closes [documentos#451](http://gitlab.meta.com.br/documentos/issues/451)





# [0.7.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.6.0...v0.7.0-alpha.0) (2020-11-27)

**Note:** Version bump only for package @smartcoop/app





# [0.6.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.5.3...v0.6.0) (2020-11-27)


### Features

* **documentos#107:** creates sags of productquotation ([d426455](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d42645597a6bf0f886a725fc7e26a4531ead840a)), closes [documentos#107](http://gitlab.meta.com.br/documentos/issues/107)
* **documentos#107:** populates list ([5dadeeb](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/5dadeeb5ce4132191ef29a9fe73b3667ba4b2ca6)), closes [documentos#107](http://gitlab.meta.com.br/documentos/issues/107)
* **documentos#108:** creates storage of accounts and accounts balance ([65f1beb](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/65f1beb25e36efa0f3fb9c2919ad41907cdc87a5)), closes [documentos#108](http://gitlab.meta.com.br/documentos/issues/108)
* **documentos#16:** added growing season store to web and mobile ([d9a586e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d9a586e67f947be1fe933777e864576cf842d89e)), closes [documentos#16](http://gitlab.meta.com.br/documentos/issues/16)
* **documentos#242:** wip: added dependencies ([9534ab3](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9534ab326e200c81bb52641dd331cc998bab542d)), closes [documentos#242](http://gitlab.meta.com.br/documentos/issues/242) [#75](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/75)
* **documentos#242:** wip: Added splashscreen to app ([374929e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/374929e8e2c681c7a29f814574ffca0d6b3b412c)), closes [documentos#242](http://gitlab.meta.com.br/documentos/issues/242) [#75](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/75)
* **documentos#311:** created input month year for mobile ([6d470dd](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/6d470dde86e713fbb568af08e3b625e90b880da1)), closes [documentos#311](http://gitlab.meta.com.br/documentos/issues/311)
* **documentos#372:** created snackbar for mobile ([dde3466](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/dde34669dedb5dbd72c806cdab7d68a068c5d076)), closes [documentos#372](http://gitlab.meta.com.br/documentos/issues/372)



# [0.6.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.5.0...v0.6.0-alpha.0) (2020-11-10)





# [0.6.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.5.0...v0.6.0-alpha.0) (2020-11-10)

**Note:** Version bump only for package @smartcoop/app
## [0.5.3](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.5.2...v0.5.3) (2020-11-11)


### Bug Fixes

* **staging:** fixed bugs from release 5 ([aa1cae0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/aa1cae08a05c9653ccaf08d49b3bd630d3c4cd5a))





## [0.5.2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.5.1...v0.5.2) (2020-11-11)

**Note:** Version bump only for package @smartcoop/app





## [0.5.1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.5.0...v0.5.1) (2020-11-10)


### Bug Fixes

* **staging:** fixed http connection and turned of datatable sorting ([eccae09](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/eccae098f786dadcfdc7d24723f9c80df106e6fb))





# [0.5.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.5.0-alpha.0...v0.5.0) (2020-11-10)


### Features

* **documentos#101:** created mobile navigation ([61b6e26](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/61b6e26770a454f968a7c314008f9af448c69752)), closes [documentos#101](http://gitlab.meta.com.br/documentos/issues/101)





# [0.5.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.4.0...v0.5.0-alpha.0) (2020-10-27)

**Note:** Version bump only for package @smartcoop/app





# [0.4.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.3.0-alpha.0...v0.4.0) (2020-10-27)


### Bug Fixes

* **android:** fixed apk build proccess ([6904860](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/69048606f3326d03dc5727ec0ba6937451f43c1f))
* **ci-cd:** fixed env for mobile and ci-cd pipeline ([8c10dd9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8c10dd9a6b1f88de7982f285315b0d8eaa2825b0))
* **documentos#10:** attached organization reducer and sagas for mobile ([20e0712](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/20e0712efd0b1a3e1982852122ea1e9022c7aece)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#132:** fix fonts ([86411d9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/86411d952258466480657462de9c6d8c0c55f059))
* **documentos#135:** removed fonts from react native ([663f986](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/663f986d2ef5133008e999b1164e57140b530201)), closes [documentos#135](http://gitlab.meta.com.br/documentos/issues/135)
* **documentos#139:** asking for permissions ([0c28ead](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0c28ead1ba34822b5383b3046b9bf6946456a14f)), closes [documentos#139](http://gitlab.meta.com.br/documentos/issues/139)
* **documentos#185:** fixed ios layout ([a408f62](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a408f621302bdabcc2f38e6284762a3aa6cbed39))


### Features

* **android:** created build:android command for mobile ([9406cbe](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9406cbe76653c09df4eed8c974ca0754a94c1350))
* **documentos#132:** wIP: Added font to test ([c097181](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c0971819b97b9a2929ffb1723d210ed970fb85de)), closes [documentos#132](http://gitlab.meta.com.br/documentos/issues/132) [#31](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/31)
* **documentos#132:** wip: Added fonts to mobile ([a1212ba](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a1212bac57fda5018518af4049d8bac2aff21fbe)), closes [documentos#132](http://gitlab.meta.com.br/documentos/issues/132) [#31](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/31)
* **documentos#132:** wIP: Applicated fonts to mobile ([193cd20](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/193cd20899b60aa36eb7d12d3c12121413b0cea6)), closes [documentos#132](http://gitlab.meta.com.br/documentos/issues/132) [#31](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/31)
* **documentos#135:** redux offline queue ([7deba4f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7deba4f419b4104188247143901b1d7ea38de210)), closes [documentos#135](http://gitlab.meta.com.br/documentos/issues/135)
* **documentos#135:** sorting out user reducer ([f49641f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f49641f9fe65c0e080a2d5750ec281fd72f52843)), closes [documentos#135](http://gitlab.meta.com.br/documentos/issues/135)
* **documentos#18:** wIP:Created mobile datepicker ([be26632](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/be266326f898865711616fe260d7ecf1a6dafaaa)), closes [documentos#18](http://gitlab.meta.com.br/documentos/issues/18) [#10](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/10)
* **documentos#185:** created basic navigation for web and mobile ([4b144ed](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4b144edcd2c7d7338100f5da5105df4ccec87969)), closes [documentos#185](http://gitlab.meta.com.br/documentos/issues/185)
* **documentos#25:** created mobile field initial screen ([e5e9d07](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e5e9d07ced3a4a925fa81b30da3422ec36297650)), closes [documentos#25](http://gitlab.meta.com.br/documentos/issues/25)
* **documentos#87:** wIP: Added input to checkbox group ([261cfa1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/261cfa1026f56be4b7ea52d0885a195afbcc3094)), closes [documentos#87](http://gitlab.meta.com.br/documentos/issues/87) [#22](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/22)
* **forms:** added form.validateField(fieldName) ([5effc5c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/5effc5cf477a400358ba2d4c162b560e008c4a15))
* **services:** turn cloud api on ([e3739f3](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e3739f3cb0a8b8507d60ba2575e02bb1c85b2615))





# [0.3.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.2.0...v0.3.0-alpha.0) (2020-09-28)

**Note:** Version bump only for package @smartcoop/app





# [0.2.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.2.0-alpha.0...v0.2.0) (2020-09-28)


### Bug Fixes

* **android:** removed trash from watermelon ([4fae7a6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4fae7a6de8b5f3807d41d52edf39bf55f22ce2d7))
* **documentos#57:** removed dialog example screen ([28b67a0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/28b67a04b45ccf981f6b1310bf8d080d0f9cbd1b)), closes [documentos#57](http://gitlab.meta.com.br/documentos/issues/57)


### Features

* **documentos#10:** auto fill forms with user api data ([9909ac6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9909ac6498d857e0f5ecb587d0334fab4b064036)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#10:** craeted loading and error modal ([34bc091](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/34bc091a4766d8b07fda331faf0cbec7c81a7497)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#10:** created web login screen ([34ed647](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/34ed647f1ab16e0c1dc45db8455239335ed6286d)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#10:** finished onboarding w/ recover password for mobile ([d8d7623](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d8d7623f801f871a0473a9d7f1334a975a379207)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#10:** login for mobile ([39b6072](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/39b60720db516f0b8e669d8691d4f61e404144e2)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#28:** created maps for web ([87cc7ec](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/87cc7ec27ec25c531dd748915382294b88f11ba2)), closes [documentos#28](http://gitlab.meta.com.br/documentos/issues/28)
* **documentos#28:** maps working into ios devices ([fc82239](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/fc82239ad99a69c194bd84da56611f2433b2def1)), closes [documentos#28](http://gitlab.meta.com.br/documentos/issues/28)
* **documentos#32:** added database into web ([1f6ad73](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1f6ad73cc2f1de976bce6aab7835337f1f8d2e05)), closes [documentos#32](http://gitlab.meta.com.br/documentos/issues/32)
* **documentos#32:** created @smartcoop/database with watermelondb ([f3040da](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f3040dadc35c09bad7fe4751508b0c4fdfd45426)), closes [documentos#32](http://gitlab.meta.com.br/documentos/issues/32)
* **documentos#32:** created service smartcoopApi ([0568f87](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0568f873e9d2f0d1fccde3f53d4c29651222cadd)), closes [documentos#32](http://gitlab.meta.com.br/documentos/issues/32)
* **documentos#32:** created useDatabase, useCollection and useAction ([d9d61e7](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d9d61e7967c048e521f23668540dc84d4bbf00a0)), closes [documentos#32](http://gitlab.meta.com.br/documentos/issues/32)
* **documentos#32:** created user database schema ([1a4ef98](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1a4ef983580dde6bff81f77c7eba368cda7880ff)), closes [documentos#32](http://gitlab.meta.com.br/documentos/issues/32)
* **documentos#32:** migrated database apps to apps ([1b35dec](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1b35decd02507f4f8ee6cdc1f6e9a06f1c8474f5)), closes [documentos#32](http://gitlab.meta.com.br/documentos/issues/32)
* **documentos#32:** removed watermelon. added redux saga ([cd97772](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/cd977725490d8654afc2dad1a0e7bc3d6b16b4dd)), closes [documentos#32](http://gitlab.meta.com.br/documentos/issues/32)
* **documentos#57:** created modal component to web and mobile ([c3610ea](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c3610eabd1cecab013f46fe3c116d5f7cea00551)), closes [documentos#57](http://gitlab.meta.com.br/documentos/issues/57) [#18](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/18)
* **documentos#57:** wIP: Creating modal ([21d3e6f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/21d3e6ff100b0754ae00137e18789aa08ddd78db)), closes [documentos#57](http://gitlab.meta.com.br/documentos/issues/57) [#18](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/18)
* **documentos#57:** wIP: Creating modal ([266d15f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/266d15f1ca1fc87a299655e91d14bbcc1ef36e34)), closes [documentos#57](http://gitlab.meta.com.br/documentos/issues/57) [#18](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/18)





# [0.2.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.1.0...v0.2.0-alpha.0) (2020-09-13)

**Note:** Version bump only for package @smartcoop/app





# [0.1.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.1.0-alpha.0...v0.1.0) (2020-09-13)


### Bug Fixes

* **forms:** validating cpf and cnpj ([a16da5b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a16da5b080973e282b84400f5be9ce1764db812a))
* **mobile-components:** fixed dependencies ([074beee](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/074beee6d0e30374b557709e08dac1a24e784d34))


### Features

* **documentos#10:** added navigation ([cdfba1f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/cdfba1f176e4bae75d4dc54477efb3c9d1d2068d)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#10:** added svg renderization ([e7cbfa0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e7cbfa0147cff6c39bb0386d34a213fec6206c46)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#10:** applying documentos[#37](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/37) into documentos[#39](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/39) ([c8e043e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c8e043ee97e72ea17b2ebd347852ea2cd999e755)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#10:** created input select for mobile ([f4756b6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f4756b692e534db401e7781a8e584f141b688e1a)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10) [#3](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/3)
* **documentos#10:** detach apps screens ([b7d9d50](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b7d9d507c89bd74a89c9addb28fc3e9bdd810e91)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#10:** mobile inputs styled as material design ([8a9e3cc](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8a9e3cce883733f69190c3eb53ed92aba70e4026)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10) [#7](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/7)
* **forms:** @smarcoop/forms finished ([d95dc20](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d95dc20603902124c3b35f531986dcb08e108879))
* **forms:** single validate for web and mobile forms ([9bbeb71](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9bbeb71f1a9c0acac8d6e817e614159eda808a5d))
* **mobile-components:** added input masks ([1e5a7fd](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1e5a7fd25a7faec83814cd330741c420f494a4c1))


### WIP

* **documentos#18:** added date picker component ([17f6e7d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/17f6e7d7588216fc816bbd6f1b9ffe40ed2d7f9e)), closes [documentos#18](http://gitlab.meta.com.br/documentos/issues/18)


### BREAKING CHANGES

* **documentos#18:** yes
