const yargs = require('yargs')

const { execTerminalCommand } = require('@smartcoop/env-config')

const { argv } = yargs.usage('Usage: $0 <command> [options]').options({
  env: {
    choices: ['development', 'staging', 'production'],
    default: 'development',
    demandOption: false,
    describe: 'Environment',
    nargs: 1
  },
  only: {
    choices: ['', 'functions', 'firestore', 'storage'],
    default: '',
    demandOption: false,
    describe: 'Firebase service',
    nargs: 1
  },
  token: {
    default: '',
    describe: 'Firebase deploy token',
    nargs: 1
  }
})

const exec = () => new Promise((resolve, reject) => {
  const childProcess = execTerminalCommand('firebase use --clear', {}, [
    { param: '--only', hasValue: !!argv.only },
    { param: '--token', hasValue: !!argv.token }
  ])

  childProcess.on('error', error => {
    console.error(error)
    reject(error)
  })

  childProcess.on('close', code => {
    if (code > 0) {
      console.error(`Command failed with code ${ code }`)
      reject()
    }

    const childProcess2 = execTerminalCommand(`firebase use ${ argv.env }`, {}, [
      { param: '--only', hasValue: true },
      { param: '--token', hasValue: true }
    ])

    childProcess2.on('error', error => {
      console.error(error)
      reject(error)
    })

    childProcess2.on('close', code2 => {
      if (code2 > 0) {
        console.error(`Command failed with code ${ code2 }`)
        reject()
      }

      const childProcess3 = execTerminalCommand('firebase deploy')

      childProcess3.on('error', error => {
        console.error(error)
        reject(error)
      })

      childProcess3.on('close', code3 => {
        if (code3 > 0) {
          console.error(`Command failed with code ${ code3 }`)
          reject()
        }
        resolve()
      })
    })

  })
})

exec()
