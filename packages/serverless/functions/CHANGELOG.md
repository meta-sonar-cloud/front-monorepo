# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [0.18.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.17.0...v0.18.0) (2021-04-19)

**Note:** Version bump only for package @smartcoop/functions





# [0.17.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.16.0...v0.17.0) (2021-04-15)

**Note:** Version bump only for package @smartcoop/functions





# [0.16.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.15.0...v0.16.0) (2021-04-13)

**Note:** Version bump only for package @smartcoop/functions







**Note:** Version bump only for package @smartcoop/functions





# [0.15.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.12.0-alpha.1...v0.15.0) (2021-03-23)

**Note:** Version bump only for package @smartcoop/functions





# [0.15.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.12.0-alpha.1...v0.15.0) (2021-03-23)

**Note:** Version bump only for package @smartcoop/functions





# [0.15.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.12.0-alpha.1...v0.15.0) (2021-03-23)

**Note:** Version bump only for package @smartcoop/functions





# [0.14.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.12.0-alpha.1...v0.14.0) (2021-03-23)

**Note:** Version bump only for package @smartcoop/functions





# [0.13.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.12.0-alpha.1...v0.13.0) (2021-03-23)

**Note:** Version bump only for package @smartcoop/functions





# [0.12.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.12.0-alpha.1...v0.12.0) (2021-03-23)

**Note:** Version bump only for package @smartcoop/functions





# [0.12.0-alpha.1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.12.0-alpha.0...v0.12.0-alpha.1) (2021-02-22)

**Note:** Version bump only for package @smartcoop/functions





# [0.12.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.11.0...v0.12.0-alpha.0) (2021-02-12)

**Note:** Version bump only for package @smartcoop/functions





# [0.11.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.11.0-alpha.0...v0.11.0) (2021-02-12)

**Note:** Version bump only for package @smartcoop/functions





# [0.11.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.10.0...v0.11.0-alpha.0) (2021-02-05)

**Note:** Version bump only for package @smartcoop/functions





# [0.10.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.10.0-alpha.0...v0.10.0) (2021-02-01)


### Bug Fixes

* **firebase:** fixed on web ([e048482](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e04848292e5f0c5b775a497e364b39ec0ea3cf8d))





# [0.10.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.9.0...v0.10.0-alpha.0) (2021-01-19)

**Note:** Version bump only for package @smartcoop/functions





# [0.9.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.8.2...v0.9.0) (2021-01-19)


### Features

* **documentos#836:** created notifications list ([f062230](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f062230640b49f3df88ad8a2a6ec5b447913dc69)), closes [documentos#836](http://gitlab.meta.com.br/documentos/issues/836)
* **documentos#836:** finished firebase integration ([f6db099](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f6db099715a083b74d4559d0c064c8d0e212ae1c)), closes [documentos#836](http://gitlab.meta.com.br/documentos/issues/836)
