/* eslint-disable prefer-destructuring */
/* eslint-disable max-len */
const bodyParser = require('body-parser')
const cors = require('cors')
const express = require('express')
const firebase = require('firebase-admin')
const functions = require('firebase-functions')
const moment = require('moment/moment')

const chunk = require('lodash/chunk')
const forEach = require('lodash/forEach')
const get = require('lodash/get')
const isEmpty = require('lodash/isEmpty')
const map = require('lodash/map')

firebase.initializeApp()

const db = firebase.firestore()

const app = express()

app.use(cors())

app.use(bodyParser.json())

const getAuthToken = (req, res, next) => {
  if (
    req.headers.authorization &&
    req.headers.authorization.split(' ')[0] === 'Bearer'
  ) {
    req.authToken = req.headers.authorization.split(' ')[1]
  } else {
    req.authToken = null
  }
  next()
}

const checkIfAuthenticated = (req, res, next) => {
  getAuthToken(req, res, async () => {
    try {
      const { authToken } = req
      const userInfo = await firebase
        .auth()
        .verifyIdToken(authToken)
      req.authId = userInfo.uid
      return next()
    } catch (e) {
      return res
        .status(401)
        .send({ error: 'You are not authorized to make this request' })
    }
  })
}

app.post('/notification/create', async (req, res) => {
  try {
    await db
      .collection('notifications')
      .doc()
      .set(req.body)

    res.send('ok')
  } catch (e) {
    // eslint-disable-next-line no-console
    console.error('error on create notification', e)
    res
      .status(422)
      .send('failure')
  }
})

app.post('/users/notifications/readAll', checkIfAuthenticated, async (req, res) => {
  try {
    const { body: { userId } } = req

    if (userId) {

      const query = await db
        .collection(`user/${ userId }/notifications`)
        .where('read', '==', false)
        .get()

      const packages = chunk(query.docs, 500)

      const promises = map(packages, (pkg) => {
        const batch = db.batch()
        forEach(pkg, (snapshot) => {
          batch.update(snapshot.ref, { read: true })
        })
        return batch.commit()
      })

      await Promise.all(promises)

      res.send('ok')
    } else {
      throw new Error('Parameter user is required')
    }
  } catch (e) {
    // eslint-disable-next-line no-console
    console.error('error on set all notifications as read')
    res.status(422).send(e.toString())
  }
})

const api = functions
  .runWith({ memory: '2GB', timeoutSeconds: 120 })
  .https
  .onRequest(app)


/*
*
* triggers
*
*/
const creatingUser = functions.firestore
  .document('user/{userId}')
  .onCreate((snap) => {
    const newUser = snap.data()
    const userId = snap.id

    db.doc(`user/${ userId }`).set({
      ...newUser,
      /*
      * set max limit in hours to notify (inside the app)
      * when the user go back to the app
      */
      hoursPendingAttention: 1
    })

    return Promise.resolve()
  })

const creatingNotification = functions.firestore
  .document('notifications/{notificationId}')
  .onCreate((snap) => {
    const newNotification = snap.data()
    const notificationId = snap.id

    const { users, message, data } = newNotification

    const createdAt = moment().format('YYYY-MM-DD HH:mm:ssZ')

    db.doc(`notifications/${ notificationId }`).set({
      ...newNotification,
      createdAt
    })

    forEach(users, (user) => {
      db.collection(`user/${ user }/notifications`)
        .doc(notificationId)
        .set({
          id: notificationId,
          createdAt,
          message,
          data,
          notified: false,
          read: false
        })
    })

    return Promise.resolve()
  })

const creatingUserNotification = functions.firestore
  .document('user/{userId}/notifications/{notificationId}')
  .onCreate(async (snap, context) => {
    try {
      const {
        params: { userId }
      } = context

      const { message, data = {} } = snap.data()

      const { title, body } = message

      const doc = await db
        .collection('user')
        .doc(userId)
        .get()

      if (doc.exists) {
        const user = doc.data()
        const devices = get(user, 'devices', [])
        if (!isEmpty(devices)) {
          const thread = moment().format('YYYY-MM-DD')
          const firebaseProjectId = process.env.GCLOUD_PROJECT || firebase.instanceId().app.options.projectId
          const logoUrl = `https://firebasestorage.googleapis.com/v0/b/${ firebaseProjectId }.appspot.com/o/assets%2Fic_launcher.png?alt=media`
          await firebase
            .messaging()
            .sendMulticast({
              tokens: devices,
              notification: {
                title,
                body
              },
              data,
              android: {
                collapseKey: thread,
                priority: 'high',
                notification: {
                  defaultSound: true,
                  defaultVibrateTimings: true,
                  priority: 'high',
                  channelId: '500'
                }
              },
              apns: {
                payload: {
                  aps: {
                    threadId: thread,
                    sound: 'default'
                  }
                }
              },
              webpush: {
                notification: {
                  image: logoUrl,
                  icon: logoUrl
                }
              }
            })
        }
      }
      return Promise.resolve()
    } catch (e) {
      // eslint-disable-next-line no-console
      console.error('error on create user notification', e)
      return Promise.reject()
    }
  })

module.exports = {
  api,
  creatingUser,
  creatingNotification,
  creatingUserNotification
}
