import arrowDown from './svgs/arrowDown'
import arrowDownField from './svgs/arrowDownField'
import arrowLeft from './svgs/arrowLeft'
import arrowLeftSimple from './svgs/arrowLeftSimple'
import arrowRightSimple from './svgs/arrowRightSimple'
import arrowsBackAndForth from './svgs/arrowsBackAndForth'
import arrowUp from './svgs/arrowUp'
import arrowUpField from './svgs/arrowUpField'
import barn from './svgs/barn'
import barnRounded from './svgs/barnRounded'
import barnRoundedBlackFilling from './svgs/barnRoundedBlackFilling'
import barnYellow from './svgs/barnYellow'
import battery from './svgs/battery'
import bell from './svgs/bell'
import blocked from './svgs/blocked'
import bug from './svgs/bug'
import bugMarker from './svgs/bugMarker'
import bugMarkerRound from './svgs/bugMarkerRound'
import calendar from './svgs/calendar'
import calendarSingle from './svgs/calendarSingle'
import camera from './svgs/camera'
import cameraRounded from './svgs/cameraRounded'
import carrot from './svgs/carrot'
import cattle from './svgs/cattle'
import change from './svgs/change'
import chart from './svgs/chart'
import checkboxChecked from './svgs/checkboxChecked'
import checkboxUnchecked from './svgs/checkboxUnchecked'
import checked from './svgs/checked'
import chevronRight from './svgs/chevronRight'
import circle from './svgs/circle'
import clock from './svgs/clock'
import close from './svgs/close'
import closeBold from './svgs/closeBold'
import closeRounded from './svgs/closeRounded'
import comment from './svgs/comment'
import compass from './svgs/compass'
import concentratedMilkCows from './svgs/concentratedMilkCows'
import cooperate from './svgs/cooperate'
import cow from './svgs/cow'
import currency from './svgs/currency'
import currencySignRounded from './svgs/currencySignRounded'
import currencyWithoutSpace from './svgs/currencyWithoutSpace'
import cutLayout from './svgs/cutLayout'
import dashboard from './svgs/dashboard'
import doc from './svgs/doc'
import download from './svgs/download'
import dropPercentage from './svgs/dropPercentage'
import edit from './svgs/edit'
import editField from './svgs/editField'
import emptyCrop from './svgs/emptyCrop'
import emptyFilter from './svgs/emptyFilter'
import emptyManagement from './svgs/emptyManagement'
import error from './svgs/error'
import exit from './svgs/exit'
import exitFullScreen from './svgs/exitFullScreen'
import exitModule from './svgs/exitModule'
import expandMore from './svgs/expandMore'
import eyeClosed from './svgs/eyeClosed'
import eyeOpened from './svgs/eyeOpened'
import feedConsumptionCows from './svgs/feedConsumptionCows'
import field from './svgs/field'
import filter from './svgs/filter'
import firstPage from './svgs/firstPage'
import frost from './svgs/frost'
import fullScreen from './svgs/fullScreen'
import hail from './svgs/hail'
import hamburguerMenu from './svgs/hamburguerMenu'
import hammer from './svgs/hammer'
import heatmap from './svgs/heatmap'
import home from './svgs/home'
import homeGroup from './svgs/homeGroup'
import info from './svgs/info'
import lactatingCows from './svgs/lactatingCows'
import landProductivityCows from './svgs/landProductivityCows'
import lastPage from './svgs/lastPage'
import leaft from './svgs/leaft'
import leftDoubleArrow from './svgs/leftDoubleArrow'
import logout from './svgs/logout'
import marker from './svgs/marker'
import markerClosed from './svgs/markerClosed'
import milkSurface from './svgs/milkSurface'
import minus from './svgs/minus'
import newCropManagement from './svgs/newCropManagement'
import nextPage from './svgs/nextPage'
import noAddress from './svgs/noAddress'
import noFieldRegistered from './svgs/noFieldRegistered'
import noImage from './svgs/noImage'
import nonCompliance from './svgs/nonCompliance'
import notFound from './svgs/notFound'
import openLayout from './svgs/openLayout'
import organization from './svgs/organization'
import organizationRounded from './svgs/organizationRounded'
import pencil from './svgs/pencil'
import picture from './svgs/picture'
import plantAndHand from './svgs/plantAndHand'
import plus from './svgs/plus'
import polygonEdge from './svgs/polygonEdge'
import previousPage from './svgs/previousPage'
import productGroup from './svgs/productGroup'

import productGroup3d from './svgs/productGroup3d'
import products from './svgs/products'
import products3d from './svgs/products3d'
import productWall from './svgs/productWall'
import publicType from './svgs/publicType'
import rain from './svgs/rain'
import search from './svgs/search'
import shield from './svgs/shield'
import shoppingCart from './svgs/shoppingCart'
import shoppingPlatform from './svgs/shoppingPlatform'
import signature from './svgs/signature'
import smallPlant from './svgs/smallPlant'
import socialNetwork from './svgs/socialNetwork'
import success from './svgs/success'
import successFlower from './svgs/successFlower'
import sun from './svgs/sun'
import supplier from './svgs/supplier'
import supplierRounded from './svgs/supplierRounded'
import thermometer from './svgs/thermometer'
import touchGesture from './svgs/touchGesture'
import tractor from './svgs/tractor'
import trash from './svgs/trash'
import unchecked from './svgs/unchecked'
import undo from './svgs/undo'
import user from './svgs/user'
import userCircle from './svgs/userCircle'
import userShield from './svgs/userShield'
import verticalDots from './svgs/verticalDots'
import wallet from './svgs/wallet'
import warning from './svgs/warning'
import warningSnackbar from './svgs/warningSnackbar'
import weatherStation from './svgs/weatherStation'
import weatherStationMarker from './svgs/weatherStationMarker'
import wheat from './svgs/wheat'
import wheater from './svgs/wheater'
import wind from './svgs/wind'
import windmill from './svgs/windmill'
import wireless from './svgs/wireless'
import wirelessMarker from './svgs/wirelessMarker'
import zoomIn from './svgs/zoomIn'
import zoomOut from './svgs/zoomOut'


export {
  arrowDown,
  arrowLeft,
  arrowUp,
  arrowUpField,
  arrowDownField,
  arrowLeftSimple,
  arrowRightSimple,
  barn,
  barnYellow,
  battery,
  bell,
  bug,
  bugMarker,
  bugMarkerRound,
  calendar,
  weatherStation,
  calendarSingle,
  carrot,
  camera,
  cameraRounded,
  checkboxChecked,
  change,
  cattle,
  chart,
  cow,
  concentratedMilkCows,
  compass,
  checkboxUnchecked,
  checked,
  shoppingPlatform,
  chevronRight,
  circle,
  close,
  closeBold,
  clock,
  currency,
  currencySignRounded,
  currencyWithoutSpace,
  cutLayout,
  dashboard,
  dropPercentage,
  edit,
  editField,
  emptyCrop,
  emptyManagement,
  error,
  exit,
  exitFullScreen,
  exitModule,
  expandMore,
  eyeClosed,
  eyeOpened,
  feedConsumptionCows,
  field,
  filter,
  firstPage,
  fullScreen,
  hamburguerMenu,
  hammer,
  heatmap,
  home,
  homeGroup,
  info,
  lactatingCows,
  landProductivityCows,
  lastPage,
  leaft,
  leftDoubleArrow,
  logout,
  marker,
  markerClosed,
  milkSurface,
  minus,
  nextPage,
  newCropManagement,
  noFieldRegistered,
  notFound,
  organization,
  organizationRounded,
  openLayout,
  plantAndHand,
  plus,
  pencil,
  picture,
  polygonEdge,
  previousPage,
  rain,
  search,
  shoppingCart,
  smallPlant,
  socialNetwork,
  success,
  successFlower,
  sun,
  supplier,
  supplierRounded,
  trash,
  unchecked,
  undo,
  user,
  userCircle,
  userShield,
  warning,
  warningSnackbar,
  wheat,
  wallet,
  signature,
  shield,
  noAddress,
  zoomIn,
  emptyFilter,
  download,
  zoomOut,
  publicType,
  doc,
  wind,
  wheater,
  windmill,
  wirelessMarker,
  weatherStationMarker,
  wireless,
  touchGesture,
  tractor,
  thermometer,
  noImage,
  closeRounded,
  verticalDots,
  frost,
  hail,
  comment,
  cooperate,
  barnRounded,
  barnRoundedBlackFilling,
  products,
  productGroup,
  nonCompliance,
  products3d,
  productGroup3d,
  productWall,
  blocked,
  arrowsBackAndForth
}
