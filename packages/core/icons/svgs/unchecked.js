// eslint-disable-next-line spaced-comment
export default ({ size, color = '#1D1D1B' } = {}) => /*html*/`
  <svg width="${ size }" height="${ size }" viewBox="0 0 16 16" fill="none">
    <circle cx="8" cy="8" r="7" stroke="${ color }" stroke-width="2"/>
  </svg>
`
