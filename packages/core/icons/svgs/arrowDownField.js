// eslint-disable-next-line spaced-comment
export default ({ size, color = '#000' } = {}) => /*html*/`
  <svg width="${ size }" height="${ size }" viewBox="0 0 15 9">
    <path fill="${ color }" d="M1.13601 0.987942H13.2729C14.1125 0.987942 14.5323 2.0021 13.9379 2.59644L7.87189 8.66722C7.50396 9.03514 6.9049 9.03514 6.53697 8.66722L0.470913 2.59644C-0.12343 2.0021 0.296383 0.987942 1.13601 0.987942Z" />
  </svg>
`
