// eslint-disable-next-line spaced-comment
export default ({ size, color = '#000' } = {}) => /*html*/`
  <svg width="${ size }" height="${ size }" viewBox="0 0 15 8">
    <path fill="${ color }" d="M13.2715 7.96529H1.13736C0.297154 7.96529 -0.12361 6.94943 0.470496 6.35532L6.53755 0.288261C6.90584 -0.0800195 7.50301 -0.0800195 7.87129 0.288261L13.9383 6.35532C14.5325 6.94943 14.1117 7.96529 13.2715 7.96529Z" />
  </svg>
`
