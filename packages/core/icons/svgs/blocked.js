// eslint-disable-next-line spaced-comment
export default ({ size, color = '#E41D1B' } = {}) => /*html*/`
  <svg width="${ size }" height="${ size }" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path d="M7.75 0C3.46875 0 0 3.46875 0 7.75C0 12.0312 3.46875 15.5 7.75 15.5C12.0312 15.5 15.5 12.0312 15.5 7.75C15.5 3.46875 12.0312 0 7.75 0ZM3.625 9C3.41875 9 3.25 8.83125 3.25 8.625V6.875C3.25 6.66875 3.41875 6.5 3.625 6.5H11.875C12.0813 6.5 12.25 6.66875 12.25 6.875V8.625C12.25 8.83125 12.0813 9 11.875 9H3.625Z" fill="${ color }"/>
  </svg>
`
