// eslint-disable-next-line spaced-comment
export default ({ size, color = '#000' } = {}) => /*html*/`
  <svg width="${ size }" height="${ size }" viewBox="0 0 24 24">
    <path fill="${ color }" d="M20 14H4V10H20V14Z" />
  </svg>
`
