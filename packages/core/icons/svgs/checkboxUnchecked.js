// eslint-disable-next-line spaced-comment
export default ({ size, color = '#1D1D1B' } = {}) => /*html*/`
<svg width="${ size }" height="${ size }" viewBox="0 0 ${ size } ${ size }" fill="none" xmlns="http://www.w3.org/2000/svg">
<mask id="path-1-inside-1" fill="white">
<rect width="${ size }" height="${ size }" rx="1.7"/>
</mask>
<rect width="${ size }" height="${ size }" rx="1.7" stroke="${ color }" stroke-width="4" mask="url(#path-1-inside-1)"/>
</svg>
`
