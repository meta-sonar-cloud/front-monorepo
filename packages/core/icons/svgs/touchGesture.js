export default ({ size, color = '#595959' } = {}) => /* html */`
  <svg width="${ size }" height="${ size }" viewBox="0 0 22 21" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path d="M11.0697 18.4749L7.91133 15.8974C7.49456 15.5641 7.31057 15.0183 7.44049 14.5007V14.5007C7.54091 14.0992 7.81811 13.7649 8.19413 13.5918C8.57015 13.4188 9.00436 13.4258 9.37466 13.6107L10.353 14.0999V8.47488C10.353 7.78453 10.9126 7.22488 11.603 7.22488V7.22488C12.2933 7.22488 12.853 7.78453 12.853 8.47488V12.2249L14.5138 12.5024C15.7193 12.7032 16.6029 13.7461 16.603 14.9682V18.4749" stroke="${ color }" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
    <path d="M1.60254 2.22488V7.22488H6.60254" stroke="${ color }" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
    <path d="M20.3525 5.04166C14.64 0.939428 6.84429 1.35693 1.60254 6.04583" stroke="${ color }" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
  </svg>
`
