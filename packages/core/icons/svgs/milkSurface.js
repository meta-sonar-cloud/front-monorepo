export default ({ size, color = '#000' } = {}) => /* html */`
<svg width="${ size }" height="${ size }" viewBox="0 0 35 35" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M17.5 7.58008H4.37037C3.06125 7.58008 2 8.64133 2 9.95045V31.2838C2 32.5929 3.06125 33.6542 4.37037 33.6542H25.7037C27.0128 33.6542 28.0741 32.5929 28.0741 31.2838V25.6543" stroke="${ color }" stroke-width="2" stroke-linecap="round"/>
<path fill-rule="evenodd" clip-rule="evenodd" d="M33.9988 14.8584C34.0372 17.424 32.6904 19.8114 30.4747 21.1054C28.259 22.3994 25.5179 22.3994 23.3022 21.1054C21.0865 19.8114 19.7397 17.424 19.7781 14.8584C19.7781 8.76381 25.8727 1.6543 26.8877 1.6543C27.9026 1.6543 33.9988 8.76381 33.9988 14.8584Z" stroke="${ color }" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
<path d="M26.8881 17.1928C25.5681 17.1928 24.498 16.1227 24.498 14.8027" stroke="${ color }" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
<path d="M2 27.7285L28.0741 27.7285" stroke="${ color }" stroke-width="2" stroke-linecap="round"/>
<path d="M2 21.8027L18 21.6543" stroke="${ color }" stroke-width="2" stroke-linecap="round"/>
<path d="M2 14.6914L16 14.6543" stroke="${ color }" stroke-width="2" stroke-linecap="round"/>
</svg>
`
