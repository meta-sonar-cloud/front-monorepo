// eslint-disable-next-line spaced-comment
export default ({ size, color = '#1D1D1B' } = {}) => /*html*/`
<svg width="${ size }" height="${ size }" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
  <path d="M8.59,16.58L13.17,12L8.59,7.41L10,6L16,12L10,18L8.59,16.58Z" fill="${ color }"/>
</svg>
`
