import icon00 from './weatherIcons/00'
import icon01 from './weatherIcons/01'
import icon02 from './weatherIcons/02'
import icon03 from './weatherIcons/03'
import icon04 from './weatherIcons/04'
import icon05 from './weatherIcons/05'
import icon06 from './weatherIcons/06'
import icon07 from './weatherIcons/07'
import icon08 from './weatherIcons/08'
import icon09 from './weatherIcons/09'
import icon10 from './weatherIcons/10'
import icon11 from './weatherIcons/11'
import icon12 from './weatherIcons/12'
import icon13 from './weatherIcons/13'
import icon14 from './weatherIcons/14'
import icon15 from './weatherIcons/15'
import icon16 from './weatherIcons/16'
import icon17 from './weatherIcons/17'
import icon18 from './weatherIcons/18'
import icon19 from './weatherIcons/19'
import icon20 from './weatherIcons/20'
import icon21 from './weatherIcons/21'
import icon22 from './weatherIcons/22'
import icon23 from './weatherIcons/23'
import icon24 from './weatherIcons/24'
import icon25 from './weatherIcons/25'
import icon26 from './weatherIcons/26'
import icon27 from './weatherIcons/27'
import icon28 from './weatherIcons/28'
import icon29 from './weatherIcons/29'
import icon30 from './weatherIcons/30'
import icon31 from './weatherIcons/31'
import icon32 from './weatherIcons/32'
import icon33 from './weatherIcons/33'
import icon34 from './weatherIcons/34'
import icon35 from './weatherIcons/35'
import icon36 from './weatherIcons/36'
import icon37 from './weatherIcons/37'
import icon38 from './weatherIcons/38'
import icon39 from './weatherIcons/39'
import icon40 from './weatherIcons/40'
import icon41 from './weatherIcons/41'
import icon42 from './weatherIcons/42'
import icon43 from './weatherIcons/43'
import icon44 from './weatherIcons/44'
import icon45 from './weatherIcons/45'
import icon46 from './weatherIcons/46'
import icon47 from './weatherIcons/47'
import iconna from './weatherIcons/na'

export {
  icon00,
  icon01,
  icon02,
  icon03,
  icon04,
  icon05,
  icon06,
  icon07,
  icon08,
  icon09,
  icon10,
  icon11,
  icon12,
  icon13,
  icon14,
  icon15,
  icon16,
  icon17,
  icon18,
  icon19,
  icon20,
  icon21,
  icon22,
  icon23,
  icon24,
  icon25,
  icon26,
  icon27,
  icon28,
  icon29,
  icon30,
  icon31,
  icon32,
  icon33,
  icon34,
  icon35,
  icon36,
  icon37,
  icon38,
  icon39,
  icon40,
  icon41,
  icon42,
  icon43,
  icon44,
  icon45,
  icon46,
  icon47,
  iconna
}
