# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [0.18.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.17.0...v0.18.0) (2021-04-19)

**Note:** Version bump only for package @smartcoop/icons





# [0.17.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.16.0...v0.17.0) (2021-04-15)

**Note:** Version bump only for package @smartcoop/icons





# [0.16.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.15.0...v0.16.0) (2021-04-13)


### Bug Fixes

* **#1484:** remove console log and change battery name ([8dfe9ea](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8dfe9ea3ffc23dfb83b47ccb1165aadb92a7030b)), closes [#1484](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1484)
* **#1601:** change icon stations. Apply for technical ([5bc5205](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/5bc52050ccbf45943b38f429387552af9b88e3ab)), closes [#1601](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1601) [#510](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/510)







**Note:** Version bump only for package @smartcoop/icons





# [0.15.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.12.0-alpha.1...v0.15.0) (2021-03-23)


### Bug Fixes

* **#1375:** change frost icon ([f7c82c6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f7c82c652393a7b7d50269b386b41f11e7357894)), closes [#1375](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1375) [#443](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/443)


### Features

* **#1221:** change report icon marker ([e11875f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e11875f51a8e396928442496842a1e38720487b6)), closes [#1221](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1221) [#444](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/444)
* **documentos#1323:** new icons ([cfbf981](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/cfbf981d2348bd00b6359f8c578d348598992bf3)), closes [documentos#1323](http://gitlab.meta.com.br/documentos/issues/1323)
* wip ([2cf8dd6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2cf8dd6979ab9a9ee338858a586f2587ca18899e))
* **#1116:** link card milk dashboard with backend ([ac23145](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ac23145e2a63479585c7d38a16c8187c794cd05f)), closes [#1116](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1116)
* **documentos#1116:** tambo for mobile ([f4d650f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f4d650f18602702f4cfccdf32758336da2041ca0)), closes [documentos#1116](http://gitlab.meta.com.br/documentos/issues/1116)





# [0.15.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.12.0-alpha.1...v0.15.0) (2021-03-23)


### Bug Fixes

* **#1375:** change frost icon ([f7c82c6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f7c82c652393a7b7d50269b386b41f11e7357894)), closes [#1375](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1375) [#443](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/443)


### Features

* **#1221:** change report icon marker ([e11875f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e11875f51a8e396928442496842a1e38720487b6)), closes [#1221](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1221) [#444](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/444)
* **documentos#1323:** new icons ([cfbf981](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/cfbf981d2348bd00b6359f8c578d348598992bf3)), closes [documentos#1323](http://gitlab.meta.com.br/documentos/issues/1323)
* wip ([2cf8dd6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2cf8dd6979ab9a9ee338858a586f2587ca18899e))
* **#1116:** link card milk dashboard with backend ([ac23145](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ac23145e2a63479585c7d38a16c8187c794cd05f)), closes [#1116](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1116)
* **documentos#1116:** tambo for mobile ([f4d650f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f4d650f18602702f4cfccdf32758336da2041ca0)), closes [documentos#1116](http://gitlab.meta.com.br/documentos/issues/1116)





# [0.15.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.12.0-alpha.1...v0.15.0) (2021-03-23)


### Bug Fixes

* **#1375:** change frost icon ([f7c82c6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f7c82c652393a7b7d50269b386b41f11e7357894)), closes [#1375](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1375) [#443](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/443)


### Features

* **#1221:** change report icon marker ([e11875f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e11875f51a8e396928442496842a1e38720487b6)), closes [#1221](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1221) [#444](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/444)
* **documentos#1323:** new icons ([cfbf981](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/cfbf981d2348bd00b6359f8c578d348598992bf3)), closes [documentos#1323](http://gitlab.meta.com.br/documentos/issues/1323)
* wip ([2cf8dd6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2cf8dd6979ab9a9ee338858a586f2587ca18899e))
* **#1116:** link card milk dashboard with backend ([ac23145](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ac23145e2a63479585c7d38a16c8187c794cd05f)), closes [#1116](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1116)
* **documentos#1116:** tambo for mobile ([f4d650f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f4d650f18602702f4cfccdf32758336da2041ca0)), closes [documentos#1116](http://gitlab.meta.com.br/documentos/issues/1116)





# [0.14.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.12.0-alpha.1...v0.14.0) (2021-03-23)


### Bug Fixes

* **#1375:** change frost icon ([f7c82c6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f7c82c652393a7b7d50269b386b41f11e7357894)), closes [#1375](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1375) [#443](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/443)


### Features

* **#1221:** change report icon marker ([e11875f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e11875f51a8e396928442496842a1e38720487b6)), closes [#1221](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1221) [#444](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/444)
* **documentos#1323:** new icons ([cfbf981](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/cfbf981d2348bd00b6359f8c578d348598992bf3)), closes [documentos#1323](http://gitlab.meta.com.br/documentos/issues/1323)
* wip ([2cf8dd6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2cf8dd6979ab9a9ee338858a586f2587ca18899e))
* **#1116:** link card milk dashboard with backend ([ac23145](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ac23145e2a63479585c7d38a16c8187c794cd05f)), closes [#1116](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1116)
* **documentos#1116:** tambo for mobile ([f4d650f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f4d650f18602702f4cfccdf32758336da2041ca0)), closes [documentos#1116](http://gitlab.meta.com.br/documentos/issues/1116)





# [0.13.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.12.0-alpha.1...v0.13.0) (2021-03-23)


### Bug Fixes

* **#1375:** change frost icon ([f7c82c6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f7c82c652393a7b7d50269b386b41f11e7357894)), closes [#1375](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1375) [#443](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/443)


### Features

* **#1221:** change report icon marker ([e11875f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e11875f51a8e396928442496842a1e38720487b6)), closes [#1221](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1221) [#444](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/444)
* **documentos#1323:** new icons ([cfbf981](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/cfbf981d2348bd00b6359f8c578d348598992bf3)), closes [documentos#1323](http://gitlab.meta.com.br/documentos/issues/1323)
* wip ([2cf8dd6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2cf8dd6979ab9a9ee338858a586f2587ca18899e))
* **#1116:** link card milk dashboard with backend ([ac23145](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ac23145e2a63479585c7d38a16c8187c794cd05f)), closes [#1116](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1116)
* **documentos#1116:** tambo for mobile ([f4d650f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f4d650f18602702f4cfccdf32758336da2041ca0)), closes [documentos#1116](http://gitlab.meta.com.br/documentos/issues/1116)





# [0.12.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.12.0-alpha.1...v0.12.0) (2021-03-23)


### Bug Fixes

* **#1375:** change frost icon ([f7c82c6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f7c82c652393a7b7d50269b386b41f11e7357894)), closes [#1375](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1375) [#443](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/443)


### Features

* **#1221:** change report icon marker ([e11875f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e11875f51a8e396928442496842a1e38720487b6)), closes [#1221](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1221) [#444](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/444)
* **documentos#1323:** new icons ([cfbf981](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/cfbf981d2348bd00b6359f8c578d348598992bf3)), closes [documentos#1323](http://gitlab.meta.com.br/documentos/issues/1323)
* wip ([2cf8dd6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2cf8dd6979ab9a9ee338858a586f2587ca18899e))
* **#1116:** link card milk dashboard with backend ([ac23145](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ac23145e2a63479585c7d38a16c8187c794cd05f)), closes [#1116](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1116)
* **documentos#1116:** tambo for mobile ([f4d650f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f4d650f18602702f4cfccdf32758336da2041ca0)), closes [documentos#1116](http://gitlab.meta.com.br/documentos/issues/1116)





# [0.12.0-alpha.1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.12.0-alpha.0...v0.12.0-alpha.1) (2021-02-22)


### Bug Fixes

* **auth:** fixed multiples refresh token ([b976597](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b976597b48bfce6639d77453f295a8a3437d836a))


### Features

* added new icons ([ccfd223](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ccfd223d6ac2ca534afde01db8af36161b4113be))
* **documentos#535:** finished home screen for mobile ([b595eee](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b595eeed9efc2c6371bb04276a22abfd74a1d9db)), closes [documentos#535](http://gitlab.meta.com.br/documentos/issues/535)





# [0.12.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.11.0...v0.12.0-alpha.0) (2021-02-12)

**Note:** Version bump only for package @smartcoop/icons





# [0.11.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.11.0-alpha.0...v0.11.0) (2021-02-12)


### Features

* **documentos#1179:** adds new icon ([78b02e8](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/78b02e82a6f3fa29c71ff4e9bd1829c81215e857)), closes [documentos#1179](http://gitlab.meta.com.br/documentos/issues/1179)
* **documentos#1179:** milk data card in web ([31e1154](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/31e115406a69aaa5e5a8d49d4adb02764668f428)), closes [documentos#1179](http://gitlab.meta.com.br/documentos/issues/1179)
* **documentos#1197:** some tweaks ([bfa58b1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/bfa58b12461718b4a18c8b54a231256cfab561e4)), closes [documentos#1197](http://gitlab.meta.com.br/documentos/issues/1197)
* **web:** added InputQuantity component ([ef37950](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ef37950d52cd8e8601e940a9a2987458c32db9b1))
* resolve [#1159](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1159) ([cc0f52e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/cc0f52ee82fc09af1db6d239c49aa8e0f67545dc))
* wip ([ec87885](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ec878853d3b3be26e7a97c2a2a81698f4d5532e8))





# [0.11.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.10.0...v0.11.0-alpha.0) (2021-02-05)

**Note:** Version bump only for package @smartcoop/icons





# [0.10.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.10.0-alpha.0...v0.10.0) (2021-02-01)


### Bug Fixes

* **onboarding:** fixed property location ([dd942a1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/dd942a138a6f2fc50f748097e9038e631605d8fa))


### Features

* **documentos#987:** changes to correct icon in web and mobile ([c53d5b4](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c53d5b44736b72bbdb3cbc7001c8b73058522d05)), closes [documentos#987](http://gitlab.meta.com.br/documentos/issues/987)
* resolve merge conflicts ([d76333d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d76333db93b61f9897987ee033d050dcdc48d8fe))
* wip ([4dc386b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4dc386b623a070ce963ddc5f7d948d1e36ab7adf))
* wip ([ea43347](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ea4334798c6090ec6819e6390596359d2bf439de))
* wip ([0ca3dd6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0ca3dd60b0ecc3ff53c4915f74af3a0bba4c6c0b))
* **documentos#102:** adds camera icons ([0f7a2b8](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0f7a2b8413812a6410b63b5f3ad6ca4e6890ab4d)), closes [documentos#102](http://gitlab.meta.com.br/documentos/issues/102)
* **documentos#102:** new icon and translations ([cb986ea](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/cb986ea511b90597593b1461c09d2074b59cbe6d)), closes [documentos#102](http://gitlab.meta.com.br/documentos/issues/102)
* **documentos#102:** signature icon ([4aa2346](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4aa2346822598b62f741a1284054090c1e8d2cc3)), closes [documentos#102](http://gitlab.meta.com.br/documentos/issues/102)
* wip ([e983a17](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e983a176fc1aebd08f9548307e6f8ea513384e25))





# [0.10.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.9.0...v0.10.0-alpha.0) (2021-01-19)

**Note:** Version bump only for package @smartcoop/icons





# [0.9.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.8.2...v0.9.0) (2021-01-19)


### Bug Fixes

* fixed change property modal icon ([bdcfe65](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/bdcfe6524249d44fee358b71eeaa320e86aae95c))


### Features

* **close menu map:** close menu map ([af6689e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/af6689e0ea492137bff19f0a2453333baef06d7c))
* **documentos#299:** adds bug icon ([3e53fcf](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3e53fcf2d960cd44172e335e8e4f62f453c3ea6a)), closes [documentos#299](http://gitlab.meta.com.br/documentos/issues/299)
* **documentos#299:** adds bug marker icon ([d861fda](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d861fda4be5f9ffb4d27be7b42ae381ba3228c05)), closes [documentos#299](http://gitlab.meta.com.br/documentos/issues/299)
* **documentos#299:** bug marker round icon ([1069b11](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1069b1107cdef46e38b46e533d828f44fce76f91)), closes [documentos#299](http://gitlab.meta.com.br/documentos/issues/299)
* **documentos#299:** linter ([2dec769](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2dec769c882c757c44cb15771bd42037e56248b9)), closes [documentos#299](http://gitlab.meta.com.br/documentos/issues/299)
* merge Develop ([4a43e2a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4a43e2a79e976c5459355a3dd2e0892e8c081e24))
* resolve [#805](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/805) ([1f50f3d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1f50f3db7106888842074940651e632681517f6f))
* wIP ([80ae937](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/80ae937854988750ccbe9f424a095c216731f73a))





## [0.8.2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.8.1...v0.8.2) (2021-01-06)


### Features

* **property actions:** added actions to delete and edit property ([c80765a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c80765aaf6a2352a2c5ef2666eb6d2301280c0e4))





## [0.8.1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.9.0-alpha.0...v0.8.1) (2021-01-06)

**Note:** Version bump only for package @smartcoop/icons





# [0.9.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.8.0...v0.9.0-alpha.0) (2021-01-05)

**Note:** Version bump only for package @smartcoop/icons





# [0.8.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.8.0-alpha.0...v0.8.0) (2021-01-05)


### Bug Fixes

* **documentos#601:** adjust merge requests ([cd40704](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/cd40704a3413e9210d44a0b7b5ec1a8861d80f55)), closes [documentos#601](http://gitlab.meta.com.br/documentos/issues/601)


### Features

* **#321:** conflicts resolved ([2b2205b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2b2205bac5173dbc49edfd63c7ac21fec9288a57)), closes [#321](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/321)
* **#539:** add new icon download ([c17dc20](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c17dc208d6abc39baa231ee512a7d5bf927ce959)), closes [#539](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/539) [#174](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/174)
* **#629:** add new icons ([ca62eb4](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ca62eb44603e48507314502b37054a2586e06648)), closes [#629](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/629)
* **#629:** add new icons ([b11efcf](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b11efcffdde104d718200a2ad1d868d23d1e76f3)), closes [#629](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/629)
* **321:** previsao do tempo mobile ([0db0a4b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0db0a4b8761cd1deb3c498313fcd0de1e3409fd7))
* **documentos#688:** added chart to web ([84a8fcb](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/84a8fcb47c4d7a12a798460379ecc025c71806fb)), closes [documentos#688](http://gitlab.meta.com.br/documentos/issues/688)
* **documentos#688:** finished field details ([f051b4e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f051b4ec43bbd5103f739761adbec3d395561cc2)), closes [documentos#688](http://gitlab.meta.com.br/documentos/issues/688)
* **documentos#696:** resolve Task ([e567033](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e567033cce6fa2e0b6e42628487ddc6f2afb10de)), closes [documentos#696](http://gitlab.meta.com.br/documentos/issues/696)





# [0.8.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.7.0...v0.8.0-alpha.0) (2020-12-11)

**Note:** Version bump only for package @smartcoop/icons





# [0.7.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.7.0-alpha.0...v0.7.0) (2020-12-11)


### Features

* **documentos#308:** changed mobile navigation ([6a06eee](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/6a06eee9034387bee975cf9eab8c20bfe960f2df)), closes [documentos#308](http://gitlab.meta.com.br/documentos/issues/308)
* **documentos#360:** created delivery locations ([295d06d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/295d06d08a8bd875612743c0a181eff07350ef85)), closes [documentos#360](http://gitlab.meta.com.br/documentos/issues/360)
* **documentos#385:** adds empty state fields ([34498ba](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/34498ba9e3f4055595fbbaacb51080c5f4e6e470)), closes [documentos#385](http://gitlab.meta.com.br/documentos/issues/385)
* **documentos#400:** added empty states ([ec28dc9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ec28dc91cc111d229e4d0a981c7acfaf378b984c)), closes [documentos#400](http://gitlab.meta.com.br/documentos/issues/400)
* **documentos#411:** adds new icon ([7db5298](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7db5298348ae25d31ae6f2b2e06a60146c6700ca)), closes [documentos#411](http://gitlab.meta.com.br/documentos/issues/411)
* **documentos#428:** add Post component ([7f9e9be](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7f9e9be79a4ee8cfb6e61df7f3ea93ae83eee9c2)), closes [documentos#428](http://gitlab.meta.com.br/documentos/issues/428)
* **documentos#459:** created social and notifications screens ([98e93e7](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/98e93e7ae3c6e4c8f6cda5ace7518ee36cc86495)), closes [documentos#459](http://gitlab.meta.com.br/documentos/issues/459)





# [0.7.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.6.0...v0.7.0-alpha.0) (2020-11-27)

**Note:** Version bump only for package @smartcoop/icons





# [0.6.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.5.3...v0.6.0) (2020-11-27)


### Features

* added new icon ([9bb3432](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9bb34326534769eb817232bea3a7e3175972ad69))
* **#282:** add news icons and news text ([f54764d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f54764d73781b89046e7c538309f038168fd1909)), closes [#282](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/282) [#103](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/103)
* **#383:** add news icons ([65bd794](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/65bd79436a0b62b2fc9ab93236e9da658c89e52d)), closes [#383](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/383) [#129](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/129)
* **#383:** add snackbar in growing season register ([2aa60d5](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2aa60d52c4bb96e87053e5bec2816e411cd479f4)), closes [#383](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/383) [#129](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/129)
* **documentos#194:** added new icon and fixed another ([389f378](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/389f3789a1882d4794251ba574a52e517a91842b))
* **documentos#277:** wip: added pagination icons ([b5b8b0d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b5b8b0d5a0be2876f4c902542af597b9e3394e5b)), closes [documentos#277](http://gitlab.meta.com.br/documentos/issues/277)



# [0.6.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.5.0...v0.6.0-alpha.0) (2020-11-10)


### Features

* **#279:** add new icon for task ([f889ef1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f889ef1ee9bdf318b41242c8f002387e943a6291)), closes [#279](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/279) [#85](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/85)





# [0.6.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.5.0...v0.6.0-alpha.0) (2020-11-10)

**Note:** Version bump only for package @smartcoop/icons





# [0.5.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.5.0-alpha.0...v0.5.0) (2020-11-10)


### Bug Fixes

* **documentos#101:** fixed currency svg viewBox ([e2148a2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e2148a29d3dae3a9c2053f01257c861b53fe69c9))


### Features

* **documentos#101:** add correct icons ([5f477f5](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/5f477f5a996c0e6929b7d1879aa88b3a50cd25c9)), closes [documentos#101](http://gitlab.meta.com.br/documentos/issues/101)
* **documentos#101:** added new icon ([51c5581](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/51c558172f9afe6a155374372dfb2cf51572189e)), closes [documentos#101](http://gitlab.meta.com.br/documentos/issues/101)
* **documentos#101:** added new icons ([81563dd](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/81563ddcb0ca6383c7b87f0246a56e0efaa75418)), closes [documentos#101](http://gitlab.meta.com.br/documentos/issues/101)
* **documentos#101:** created mobile navigation ([61b6e26](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/61b6e26770a454f968a7c314008f9af448c69752)), closes [documentos#101](http://gitlab.meta.com.br/documentos/issues/101)
* **documentos#101:** created module screen for web ([a0d8301](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a0d830137be31ed28c7bad0bc76672012b5af4eb)), closes [documentos#101](http://gitlab.meta.com.br/documentos/issues/101)
* **documentos#101:** fixes icon color ([d1418bd](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d1418bd7d601e2aabba32e9f02e029664ba13d01))
* **documentos#259:** adds new icons ([da9935f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/da9935fa3365a742529d0dff27875e0192e56bc6)), closes [documentos#259](http://gitlab.meta.com.br/documentos/issues/259) [#81](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/81)
* **documentos#259:** adds new icons svg ([b4c4a1a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b4c4a1aaf3e75f446f72bf460fde1af9f4b4751b)), closes [documentos#259](http://gitlab.meta.com.br/documentos/issues/259) [#81](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/81)





# [0.5.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.4.0...v0.5.0-alpha.0) (2020-10-27)

**Note:** Version bump only for package @smartcoop/icons





# [0.4.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.3.0-alpha.0...v0.4.0) (2020-10-27)


### Bug Fixes

* **documentos#116:** changed viewbox of marker icon ([1c4dfb9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1c4dfb9d8fc5132e2eabe207847b7f50cc4ff28c)), closes [documentos#116](http://gitlab.meta.com.br/documentos/issues/116)


### Features

* **documentos#164:** created checkbox tree view web component ([467b234](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/467b234bd9cc98ab93b35ed5bf1d3892d27b9f16)), closes [documentos#164](http://gitlab.meta.com.br/documentos/issues/164)
* **documentos#185:** created basic navigation for web and mobile ([4b144ed](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4b144edcd2c7d7338100f5da5105df4ccec87969)), closes [documentos#185](http://gitlab.meta.com.br/documentos/issues/185)
* **documentos#25:** added left arrow icon ([d497357](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d497357e0c086a94caaf4030b2b2fb8cf7a9ed1f)), closes [documentos#25](http://gitlab.meta.com.br/documentos/issues/25)
* **documentos#28:** adds new icon markerClosed ([f5bd831](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f5bd8312207650be80bd1fe78b7f31287227169b)), closes [documentos#28](http://gitlab.meta.com.br/documentos/issues/28)
* **documentos#49:** added carrot, wheat, smallPlant icons ([f2d8d48](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f2d8d48906379251407a64dc0247c091c3c05e1b)), closes [documentos#49](http://gitlab.meta.com.br/documentos/issues/49)
* **documentos#49:** added search icon ([cccd34e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/cccd34e5cef5abe6603508a614656ccebc6d30b0)), closes [documentos#49](http://gitlab.meta.com.br/documentos/issues/49)
* **documentos#49:** adds dozen of icons ([b694aaa](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b694aaa02114080ce29bcdd3d44d13c931a9e055)), closes [documentos#49](http://gitlab.meta.com.br/documentos/issues/49)
* **documentos#53:** created polygon map component ([13306bd](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/13306bd9eabac3c3e46f1810207d6caaa102ea28)), closes [documentos#53](http://gitlab.meta.com.br/documentos/issues/53)
* **documentos#68:** added new icons ([96128eb](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/96128ebd39df9fa033f81a6a6a138cbf89b9e85b)), closes [documentos#68](http://gitlab.meta.com.br/documentos/issues/68)
* **documentos#87:** adds plus icon ([1bdc909](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1bdc909fdd5e6c5342b70c5fa59db9cdee717676)), closes [documentos#87](http://gitlab.meta.com.br/documentos/issues/87)





# [0.3.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.2.0...v0.3.0-alpha.0) (2020-09-28)

**Note:** Version bump only for package @smartcoop/icons





# [0.2.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.2.0-alpha.0...v0.2.0) (2020-09-28)


### Bug Fixes

* **documentos#86:** fix typo in docs ([4c83223](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4c832230c1fce9ef269c5220953f51da422f916f))


### Features

* **#57:** created SuccessModal container on mobile ([53ecb6c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/53ecb6c16fd43613a7b3705ca136c43218cdc214)), closes [#57](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/57) [#18](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/18)
* **documentos#22:** merge ([824f2d2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/824f2d2f9b25aecb685e3b33ab1b4f445665fc08)), closes [documentos#22](http://gitlab.meta.com.br/documentos/issues/22)
* **documentos#25:** added field icon ([9f46d95](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9f46d95b0c88081a3aa1a63b3140068190b89eca)), closes [documentos#25](http://gitlab.meta.com.br/documentos/issues/25)
* **documentos#28:** added new icon ([1304711](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1304711090f54815d1a350fde4e23e8cebea74c9)), closes [documentos#28](http://gitlab.meta.com.br/documentos/issues/28)
* **documentos#28:** added new icon ([518790c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/518790cf48ed0b1f0e823fdb8e9cecb2a8b6f064)), closes [documentos#28](http://gitlab.meta.com.br/documentos/issues/28)
* **documentos#28:** added new icons ([dd6785b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/dd6785bf15766054288d669b45990f40a35d80db)), closes [documentos#28](http://gitlab.meta.com.br/documentos/issues/28)
* **documentos#28:** created helperText to TextField ([1b22624](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1b22624d77e2e84e3589d454391f39f2bf178984)), closes [documentos#28](http://gitlab.meta.com.br/documentos/issues/28)
* **documentos#28:** created helperText to TextField ([1ce7a34](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1ce7a34fcc4249c9fc19c38d644ca4ee7baaa23f)), closes [documentos#28](http://gitlab.meta.com.br/documentos/issues/28)
* **documentos#28:** created maps for web ([87cc7ec](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/87cc7ec27ec25c531dd748915382294b88f11ba2)), closes [documentos#28](http://gitlab.meta.com.br/documentos/issues/28)
* **documentos#32:** added database into web ([1f6ad73](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1f6ad73cc2f1de976bce6aab7835337f1f8d2e05)), closes [documentos#32](http://gitlab.meta.com.br/documentos/issues/32)
* **documentos#57:** created modal component to web and mobile ([c3610ea](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c3610eabd1cecab013f46fe3c116d5f7cea00551)), closes [documentos#57](http://gitlab.meta.com.br/documentos/issues/57) [#18](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/18)
* **documentos#87:** adds plus icon ([8dc4617](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8dc4617d7cd33fb4d9a17238f8f2ac360c17a672)), closes [documentos#87](http://gitlab.meta.com.br/documentos/issues/87)





# [0.2.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.1.0...v0.2.0-alpha.0) (2020-09-13)

**Note:** Version bump only for package @smartcoop/icons





# [0.1.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.1.0-alpha.0...v0.1.0) (2020-09-13)


### Features

* **documentos#10:** created input select for mobile ([f4756b6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f4756b692e534db401e7781a8e584f141b688e1a)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10) [#3](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/3)
* **documentos#10:** finished onboard mocked ([b20abfa](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b20abfaf5a51d791934136a03c7c4714e1cda6b3)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
