# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [0.18.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.17.0...v0.18.0) (2021-04-19)


### Bug Fixes

* **#1745:** add new errors text ([4a163ce](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4a163cea4de3aea23637d65fedf579ada09de273)), closes [#1745](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1745)
* **documentos#1750:** adjustments ([28966d7](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/28966d704546ed37e849552d6fddf196ce808568)), closes [documentos#1750](http://gitlab.meta.com.br/documentos/issues/1750)
* **documentos#1755:** adjustments ([096f90b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/096f90b47500de13e083e959f99be6f044dec0b6)), closes [documentos#1755](http://gitlab.meta.com.br/documentos/issues/1755)
* **rain map:** disabe=le rain map ([d844428](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d84442868f35be97df179f8c1782464cf797df05))


### Features

* resolve issue ([8bffd06](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8bffd062c5ccbce31982179dfd6bd52b3bb3c8c9))
* resolve issue ([1525d8e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1525d8eba60317edfb1d7b6215dda063c3ace556))
* **#1737:** adjustment layout animalbirth and pev mobile ([00f6c1d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/00f6c1d90f8804fb2334a3623223f3c7ec0c3824)), closes [#1737](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1737) [#572](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/572)
* **#1745:** add next action and nex action date ([95d2229](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/95d2229bdd39c62dbf02379b85bd0c3528fa943e)), closes [#1745](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1745) [#580](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/580)
* **documentos#1737:** new translations ([ac7b59e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ac7b59e7884b7fba0e6c5e9e79db514debaaa0e1)), closes [documentos#1737](http://gitlab.meta.com.br/documentos/issues/1737)





# [0.17.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.16.0...v0.17.0) (2021-04-15)


### Bug Fixes

* **#1701:** adjustment filter vaca in register animal ([7b723f4](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7b723f48a10d605d10e745d83c79d7f8ed02bcb8)), closes [#1701](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1701) [#556](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/556)


### Features

* resolve [#1735](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1735) ([cda6387](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/cda6387c44735527fe95bf60838cc9098715d96e))
* **documentos#1715:** renaming things and logic changes ([7b0ad3c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7b0ad3c5f4fd006fd458cfb31d4817270f4d561b)), closes [documentos#1715](http://gitlab.meta.com.br/documentos/issues/1715)
* resolve duplicate key translation ([68f700b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/68f700b541f302db568b7f95b1f0ff627d67a8d1))
* translate errors messages ([6304058](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/6304058c7821fd08696f522cdf10aecdf08b7ed4))
* **documentos#1674:** new translations ([903ef84](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/903ef84e4bea64d97e6fafd4fac3034a4fe5d8d4)), closes [documentos#1674](http://gitlab.meta.com.br/documentos/issues/1674)





# [0.16.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.15.0...v0.16.0) (2021-04-13)


### Bug Fixes

* **#1484:** remove console log and change battery name ([8dfe9ea](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8dfe9ea3ffc23dfb83b47ccb1165aadb92a7030b)), closes [#1484](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1484)
* **ajustes finos:** ajustes finos da aplicação ([eefa46f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/eefa46fe613632b216df51736217149a8027dd16))
* **documentos#1560:** ajustes aparencia ([d7f810f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d7f810f0ddf2f083da431d974a709092033aac17)), closes [documentos#1560](http://gitlab.meta.com.br/documentos/issues/1560)
* **documentos#1586:** correções e ajustes finos ([e6dbcc0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e6dbcc0d7966935a77519dbb4d8077d3bafcf3c5)), closes [documentos#1586](http://gitlab.meta.com.br/documentos/issues/1586)
* **documentos#1632:** ajustes finos app ([0e0ffdb](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0e0ffdbce9e4c4ab1c84e30d092d1cd16f83967d)), closes [documentos#1632](http://gitlab.meta.com.br/documentos/issues/1632)
* **documentos#1661:** label adjustments ([127efad](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/127efadd69c6afe28c674c9cf351b2adfc0e6283)), closes [documentos#1661](http://gitlab.meta.com.br/documentos/issues/1661)


### Features

* changes on translation ([40b7343](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/40b7343c080d4302cb5300c137a8d3ce4d53efde))
* commit sprint review changes ([5d1713e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/5d1713e100f7f72b3fe4f573f3f22d47c7f1af8b))
* merge changes ([35a5835](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/35a58359f51dd7ff362918bbaf494d1507786e25))
* merge com a develop ([702e143](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/702e1434c3eab83403ccbd51d34a6340a05613b9))
* merge develop ([911015c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/911015ced65c53a6bc66e9bc87e2bf7c99f2ab39))
* resolve [#1640](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1640) ([970a28c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/970a28cdd5d08848e35ba5bbac14f2d1a742b2d9))
* resolve [#1657](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1657) ([962243d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/962243d49498f655cd4fd681b3075095af9e77eb))
* resolve conflicts ([b7f71a7](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b7f71a70e510327718573e62114b2e438e74417f))
* resolve web ([0cff104](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0cff104e64bcd22bdc858ea99003b22fe958c811))
* sprint review changes ([3727971](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3727971ac0a218114430bf26dfa9915966a131ca))
* **#1484:** integration with backend weather stations details ([c776b4e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c776b4eb5720ad698dc40ce90bc003c42b18027a)), closes [#1484](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1484) [#484](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/484)
* **#1531 #1532:** create list animals, create and edit ([03bc1a2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/03bc1a23a48639d11f58f59faa7868a05a34984f)), closes [#1531](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1531) [#1532](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1532) [#499](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/499)
* **#1531 #1532:** create list animals, create and edit ([97f0423](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/97f04231c8983a8789ece8acba3091fe08fe6591)), closes [#1531](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1531) [#1532](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1532) [#499](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/499)
* **#1649:** create new texts ([b5c5d87](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b5c5d87f65b9f921c8973a8118e416c60bc868fa)), closes [#1649](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1649)
* **documentos#1316:** added new translations ([67aa284](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/67aa28480e88183d53f454b2c21e64b4042aad8c)), closes [documentos#1316](http://gitlab.meta.com.br/documentos/issues/1316)
* **documentos#1316:** added new translations ([e302a27](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e302a27c02d6318da375a203c2a635bb41973b5f)), closes [documentos#1316](http://gitlab.meta.com.br/documentos/issues/1316)
* **documentos#1325:** created PEV on web ([d0d6dda](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d0d6ddae85eeab6cd3b022ac0cf40c8db8aa7d06)), closes [documentos#1325](http://gitlab.meta.com.br/documentos/issues/1325)
* **documentos#1490:** wip LotList ([ebcbf71](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ebcbf71bdfaa03e8f18c82bb005479bba921932d)), closes [documentos#1490](http://gitlab.meta.com.br/documentos/issues/1490)
* **documentos#1495:** modals, saga and item ([a04f400](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a04f400ff9eb8feddfbef787e6b8fd7121985631)), closes [documentos#1495](http://gitlab.meta.com.br/documentos/issues/1495)
* **documentos#1495:** new translations ([0024d97](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0024d97f6f7e5b685122e29a47a6a0ba345c61e3)), closes [documentos#1495](http://gitlab.meta.com.br/documentos/issues/1495)
* **documentos#1495:** wip ([485fa41](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/485fa41d3ad5bbcb945121c52d7c6ca8e25d1e84)), closes [documentos#1495](http://gitlab.meta.com.br/documentos/issues/1495)
* **documentos#1579:** creates dropdown menu ([3726ef9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3726ef98f33b057b5f269598894e2cc7174b512c)), closes [documentos#1579](http://gitlab.meta.com.br/documentos/issues/1579)
* **documentos#1579:** creates dropdown menu ([3cd3342](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3cd334246c3e3ec8b6aedac15559ee82d209149b)), closes [documentos#1579](http://gitlab.meta.com.br/documentos/issues/1579)
* **documentos#1579:** mobile insemination ([1bf225d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1bf225d6f5ffbafb10226bf7c5c77b13a03ed58e)), closes [documentos#1579](http://gitlab.meta.com.br/documentos/issues/1579)
* **documentos#1579:** mobile insemination ([10c2ff2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/10c2ff2171498fa004d5fc58ac3ba9266347753a)), closes [documentos#1579](http://gitlab.meta.com.br/documentos/issues/1579)
* **documentos#1579:** new translations ([9c175c0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9c175c016e09f715b66ebecaaa903953aef9b365)), closes [documentos#1579](http://gitlab.meta.com.br/documentos/issues/1579)
* **documentos#1650:** calving in mobile ([f1218c7](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f1218c7a11e6444eee6c4210889ba2eba91c1629)), closes [documentos#1650](http://gitlab.meta.com.br/documentos/issues/1650)
* wip ([8a64b05](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8a64b05a75f08e112e58615cb42c7edf15303438))
* **documentos#1579:** filters in web ([6494181](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/64941810de441edc856318583f5776dd41749431)), closes [documentos#1579](http://gitlab.meta.com.br/documentos/issues/1579)
* **documentos#1579:** new translations ([a3cd9e2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a3cd9e230440e283160fb9973a95475816d5a076)), closes [documentos#1579](http://gitlab.meta.com.br/documentos/issues/1579)
* resolve [#1604](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1604) ([06bdda8](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/06bdda8ab85f34d6589443ab77810d75b9663267))
* resolve [#1611](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1611) ([b9d1a9c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b9d1a9c40045e9f2ab2f783dd3fbead65663b9dc))
* resolve [#1617](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1617) pt2 ([6b319c0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/6b319c0f618223266b5f5fb0c2b62335e584e16c))







**Note:** Version bump only for package @smartcoop/i18n





# [0.15.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.12.0-alpha.1...v0.15.0) (2021-03-23)


### Bug Fixes

* **#1280:** fix register milk quality mobile ([79e8429](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/79e8429ecfdf191d8f66e97004c844a6bbb76b2d))
* **#1301:** adjustment pt-br ([cc43b84](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/cc43b845f48d64fa3803ec81ba545e85e6dae4d9)), closes [#1301](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1301)
* **#1301:** fix i18n ([c905457](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c9054574287206f454af9bf4e84f8ec4e999a46d))
* **#1331:** fix open field like technical ([f6f2806](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f6f2806822f42e8e4a4e3028cabe612691dc9e6d))
* **documentos#1447:** ajustes mapa de chuva e relatorios ([9cd51a9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9cd51a9037291d437f5b847da66e8b7bf8a67998)), closes [documentos#1447](http://gitlab.meta.com.br/documentos/issues/1447)


### Features

* added new translation ([caac0b6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/caac0b6ad535ec5dd8472ebdfe4cdfcb0cdbced2))
* changed translation ([09b3535](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/09b35354884853c5fff12912f85ec5f8cd158b90))
* other changes to [#1523](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1523) ([737f718](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/737f718ce1eb816ff75e8762ce9dba84a86f7af7))
* resolve [#1385](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1385) ([0e47e32](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0e47e327bd8e84487252d935ed5c9fdf854580c0))
* resolve [#1424](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1424) ([9b126fa](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9b126fa8f24238c6a4abdb6b0482c02111d10e4a))
* resolve [#1523](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1523) ([bf353c3](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/bf353c3e7fceecbc43a53bd729d0b13e82c0ed40))
* resolving other bugs [#1523](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1523) ([8c82180](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8c82180d555659a897c2f7ab35c0f11b2588958c))
* **#1116:** link card milk dashboard with backend ([ac23145](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ac23145e2a63479585c7d38a16c8187c794cd05f)), closes [#1116](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1116)
* **#1182:** adjustment text pest report location and pin ([21c7da1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/21c7da14eb242f378890d6947cabbda961edff37)), closes [#1182](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1182) [#430](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/430)
* **#1301:** adjustment graphs web ([c567013](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c567013b84c3ab9443e25ba45bdddfbfc94f5ecc)), closes [#1301](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1301) [#399](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/399)
* **#1386:** adjustment graph milk quality ([9f25b09](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9f25b09a490c558c1e56eff23a86b132237e63ec)), closes [#1386](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1386) [#439](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/439)
* **999:** mapa de chuva web ([bffe52a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/bffe52a6d6356b79488ef867aabc3eb384af2435))
* **documentos#1116:** tambo for mobile ([f4d650f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f4d650f18602702f4cfccdf32758336da2041ca0)), closes [documentos#1116](http://gitlab.meta.com.br/documentos/issues/1116)
* **documentos#1136:** new translations ([17ac854](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/17ac8542f7377458140410f8431189719ecb316a)), closes [documentos#1136](http://gitlab.meta.com.br/documentos/issues/1136)
* **documentos#1240:** new translations ([e971f98](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e971f984cf0e4e1c1f06740bae6f2e757bcb4d81)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** new translations ([9839110](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/983911083b30e7eedd4e9302f860782ae51424cb)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** new translations ([a0065b5](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a0065b5a5128a730b80a000cac88a8c0b7d547f3)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1275:** changed web satellite images warning ([784e19c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/784e19cc5009d961ee2fc4945ef8d21633e8fc08)), closes [documentos#1275](http://gitlab.meta.com.br/documentos/issues/1275)
* **documentos#1282:** added new translation ([9a8035a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9a8035a75f4c382cf20dd024a9441431cef002b8)), closes [documentos#1282](http://gitlab.meta.com.br/documentos/issues/1282)
* **documentos#1290:** new translations ([f431dfc](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f431dfcf4b91f9296f2cfed8e5bff3c83441ef6c)), closes [documentos#1290](http://gitlab.meta.com.br/documentos/issues/1290)
* **documentos#1290:** new translations ([7190e06](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7190e069d652453de5d2e549b34211c2cc43d44d)), closes [documentos#1290](http://gitlab.meta.com.br/documentos/issues/1290)
* **documentos#1357:** changes in field monitoration ([2508a09](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2508a09a73f04732609b124c0c08aeaa59bd7668)), closes [documentos#1357](http://gitlab.meta.com.br/documentos/issues/1357)
* **documentos#1374:** new translations ([7144750](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7144750f917073641f475f4795f4db7ab288d7e6)), closes [documentos#1374](http://gitlab.meta.com.br/documentos/issues/1374)
* **documentos#1377:** terms page ([264599e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/264599e1181a0effe8ee8de496db5bc4b6bc1735)), closes [documentos#1377](http://gitlab.meta.com.br/documentos/issues/1377)
* **documentos#1391:** changes in commercialization values ([0a89d4a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0a89d4ad88723432951d1f726d9c122bada2d5d7)), closes [documentos#1391](http://gitlab.meta.com.br/documentos/issues/1391)
* **documentos#1400:** modal in web ([1113a0f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1113a0f9e31717ac455b11240c9bb9cf6f64aed6)), closes [documentos#1400](http://gitlab.meta.com.br/documentos/issues/1400)
* **documentos#1423:** adds warning snackbar if mngmt isn conc. ([13f4553](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/13f455348b48a86f2648c3d876244e7bca22cecb)), closes [documentos#1423](http://gitlab.meta.com.br/documentos/issues/1423)
* added new translation ([b4bedd1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b4bedd1ca20453b18218689309ad70ab34335a12))
* added new translation ([f02f12e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f02f12ea2acaf43e34f01d449f8046ddbbd55c5a))
* added new translation ([d4dc40b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d4dc40b95e6816c33babd835c7a9c3ff96fc597e))
* added new translation ([13c38d5](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/13c38d5aaae9a3d6b8901092bcbc60923cfeb4b3))
* added new translation ([a85fed1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a85fed1ff7a4ca8effb142a3f2df55eae18e1b33))
* added new translations ([49fa46c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/49fa46cd78620ba9590ae5642c32effbd523a6b2))
* added new translations ([97206d8](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/97206d88fc22b106bd7ae805c8a302585fc2a9b1))
* added new translations ([b50b59c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b50b59c7a7d744e2101aa3d3bc6998c5f4248d30))
* merge develop ([2731148](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2731148d7380e3ab5c8ec38d198b1773467e4364))
* resolve [#1335](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1335) ([6f4e819](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/6f4e819baa9fd1662f335ffba7a84f740613f13d))
* wip ([b3b6eb5](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b3b6eb57ca79dc596447e6b748d58c3b74087f14))
* wip ([2cf8dd6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2cf8dd6979ab9a9ee338858a586f2587ca18899e))
* **documentos#885:** changed field screen ([4299f89](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4299f898298c9641e4e63098dfdcf85a5633c11d)), closes [documentos#885](http://gitlab.meta.com.br/documentos/issues/885)





# [0.15.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.12.0-alpha.1...v0.15.0) (2021-03-23)


### Bug Fixes

* **#1280:** fix register milk quality mobile ([79e8429](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/79e8429ecfdf191d8f66e97004c844a6bbb76b2d))
* **#1301:** adjustment pt-br ([cc43b84](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/cc43b845f48d64fa3803ec81ba545e85e6dae4d9)), closes [#1301](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1301)
* **#1301:** fix i18n ([c905457](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c9054574287206f454af9bf4e84f8ec4e999a46d))
* **#1331:** fix open field like technical ([f6f2806](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f6f2806822f42e8e4a4e3028cabe612691dc9e6d))
* **documentos#1447:** ajustes mapa de chuva e relatorios ([9cd51a9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9cd51a9037291d437f5b847da66e8b7bf8a67998)), closes [documentos#1447](http://gitlab.meta.com.br/documentos/issues/1447)


### Features

* added new translation ([caac0b6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/caac0b6ad535ec5dd8472ebdfe4cdfcb0cdbced2))
* changed translation ([09b3535](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/09b35354884853c5fff12912f85ec5f8cd158b90))
* other changes to [#1523](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1523) ([737f718](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/737f718ce1eb816ff75e8762ce9dba84a86f7af7))
* resolve [#1385](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1385) ([0e47e32](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0e47e327bd8e84487252d935ed5c9fdf854580c0))
* resolve [#1424](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1424) ([9b126fa](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9b126fa8f24238c6a4abdb6b0482c02111d10e4a))
* resolve [#1523](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1523) ([bf353c3](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/bf353c3e7fceecbc43a53bd729d0b13e82c0ed40))
* resolving other bugs [#1523](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1523) ([8c82180](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8c82180d555659a897c2f7ab35c0f11b2588958c))
* **#1116:** link card milk dashboard with backend ([ac23145](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ac23145e2a63479585c7d38a16c8187c794cd05f)), closes [#1116](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1116)
* **#1182:** adjustment text pest report location and pin ([21c7da1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/21c7da14eb242f378890d6947cabbda961edff37)), closes [#1182](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1182) [#430](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/430)
* **#1301:** adjustment graphs web ([c567013](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c567013b84c3ab9443e25ba45bdddfbfc94f5ecc)), closes [#1301](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1301) [#399](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/399)
* **#1386:** adjustment graph milk quality ([9f25b09](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9f25b09a490c558c1e56eff23a86b132237e63ec)), closes [#1386](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1386) [#439](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/439)
* **999:** mapa de chuva web ([bffe52a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/bffe52a6d6356b79488ef867aabc3eb384af2435))
* **documentos#1116:** tambo for mobile ([f4d650f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f4d650f18602702f4cfccdf32758336da2041ca0)), closes [documentos#1116](http://gitlab.meta.com.br/documentos/issues/1116)
* **documentos#1136:** new translations ([17ac854](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/17ac8542f7377458140410f8431189719ecb316a)), closes [documentos#1136](http://gitlab.meta.com.br/documentos/issues/1136)
* **documentos#1240:** new translations ([e971f98](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e971f984cf0e4e1c1f06740bae6f2e757bcb4d81)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** new translations ([9839110](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/983911083b30e7eedd4e9302f860782ae51424cb)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** new translations ([a0065b5](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a0065b5a5128a730b80a000cac88a8c0b7d547f3)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1275:** changed web satellite images warning ([784e19c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/784e19cc5009d961ee2fc4945ef8d21633e8fc08)), closes [documentos#1275](http://gitlab.meta.com.br/documentos/issues/1275)
* **documentos#1282:** added new translation ([9a8035a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9a8035a75f4c382cf20dd024a9441431cef002b8)), closes [documentos#1282](http://gitlab.meta.com.br/documentos/issues/1282)
* **documentos#1290:** new translations ([f431dfc](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f431dfcf4b91f9296f2cfed8e5bff3c83441ef6c)), closes [documentos#1290](http://gitlab.meta.com.br/documentos/issues/1290)
* **documentos#1290:** new translations ([7190e06](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7190e069d652453de5d2e549b34211c2cc43d44d)), closes [documentos#1290](http://gitlab.meta.com.br/documentos/issues/1290)
* **documentos#1357:** changes in field monitoration ([2508a09](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2508a09a73f04732609b124c0c08aeaa59bd7668)), closes [documentos#1357](http://gitlab.meta.com.br/documentos/issues/1357)
* **documentos#1374:** new translations ([7144750](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7144750f917073641f475f4795f4db7ab288d7e6)), closes [documentos#1374](http://gitlab.meta.com.br/documentos/issues/1374)
* **documentos#1377:** terms page ([264599e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/264599e1181a0effe8ee8de496db5bc4b6bc1735)), closes [documentos#1377](http://gitlab.meta.com.br/documentos/issues/1377)
* **documentos#1391:** changes in commercialization values ([0a89d4a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0a89d4ad88723432951d1f726d9c122bada2d5d7)), closes [documentos#1391](http://gitlab.meta.com.br/documentos/issues/1391)
* **documentos#1400:** modal in web ([1113a0f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1113a0f9e31717ac455b11240c9bb9cf6f64aed6)), closes [documentos#1400](http://gitlab.meta.com.br/documentos/issues/1400)
* **documentos#1423:** adds warning snackbar if mngmt isn conc. ([13f4553](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/13f455348b48a86f2648c3d876244e7bca22cecb)), closes [documentos#1423](http://gitlab.meta.com.br/documentos/issues/1423)
* added new translation ([b4bedd1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b4bedd1ca20453b18218689309ad70ab34335a12))
* added new translation ([f02f12e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f02f12ea2acaf43e34f01d449f8046ddbbd55c5a))
* added new translation ([d4dc40b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d4dc40b95e6816c33babd835c7a9c3ff96fc597e))
* added new translation ([13c38d5](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/13c38d5aaae9a3d6b8901092bcbc60923cfeb4b3))
* added new translation ([a85fed1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a85fed1ff7a4ca8effb142a3f2df55eae18e1b33))
* added new translations ([49fa46c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/49fa46cd78620ba9590ae5642c32effbd523a6b2))
* added new translations ([97206d8](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/97206d88fc22b106bd7ae805c8a302585fc2a9b1))
* added new translations ([b50b59c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b50b59c7a7d744e2101aa3d3bc6998c5f4248d30))
* merge develop ([2731148](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2731148d7380e3ab5c8ec38d198b1773467e4364))
* resolve [#1335](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1335) ([6f4e819](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/6f4e819baa9fd1662f335ffba7a84f740613f13d))
* wip ([b3b6eb5](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b3b6eb57ca79dc596447e6b748d58c3b74087f14))
* wip ([2cf8dd6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2cf8dd6979ab9a9ee338858a586f2587ca18899e))
* **documentos#885:** changed field screen ([4299f89](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4299f898298c9641e4e63098dfdcf85a5633c11d)), closes [documentos#885](http://gitlab.meta.com.br/documentos/issues/885)





# [0.15.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.12.0-alpha.1...v0.15.0) (2021-03-23)


### Bug Fixes

* **#1280:** fix register milk quality mobile ([79e8429](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/79e8429ecfdf191d8f66e97004c844a6bbb76b2d))
* **#1301:** adjustment pt-br ([cc43b84](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/cc43b845f48d64fa3803ec81ba545e85e6dae4d9)), closes [#1301](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1301)
* **#1301:** fix i18n ([c905457](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c9054574287206f454af9bf4e84f8ec4e999a46d))
* **#1331:** fix open field like technical ([f6f2806](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f6f2806822f42e8e4a4e3028cabe612691dc9e6d))
* **documentos#1447:** ajustes mapa de chuva e relatorios ([9cd51a9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9cd51a9037291d437f5b847da66e8b7bf8a67998)), closes [documentos#1447](http://gitlab.meta.com.br/documentos/issues/1447)


### Features

* added new translation ([caac0b6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/caac0b6ad535ec5dd8472ebdfe4cdfcb0cdbced2))
* changed translation ([09b3535](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/09b35354884853c5fff12912f85ec5f8cd158b90))
* other changes to [#1523](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1523) ([737f718](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/737f718ce1eb816ff75e8762ce9dba84a86f7af7))
* resolve [#1385](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1385) ([0e47e32](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0e47e327bd8e84487252d935ed5c9fdf854580c0))
* resolve [#1424](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1424) ([9b126fa](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9b126fa8f24238c6a4abdb6b0482c02111d10e4a))
* resolve [#1523](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1523) ([bf353c3](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/bf353c3e7fceecbc43a53bd729d0b13e82c0ed40))
* resolving other bugs [#1523](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1523) ([8c82180](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8c82180d555659a897c2f7ab35c0f11b2588958c))
* **#1116:** link card milk dashboard with backend ([ac23145](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ac23145e2a63479585c7d38a16c8187c794cd05f)), closes [#1116](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1116)
* **#1182:** adjustment text pest report location and pin ([21c7da1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/21c7da14eb242f378890d6947cabbda961edff37)), closes [#1182](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1182) [#430](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/430)
* **#1301:** adjustment graphs web ([c567013](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c567013b84c3ab9443e25ba45bdddfbfc94f5ecc)), closes [#1301](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1301) [#399](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/399)
* **#1386:** adjustment graph milk quality ([9f25b09](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9f25b09a490c558c1e56eff23a86b132237e63ec)), closes [#1386](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1386) [#439](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/439)
* **999:** mapa de chuva web ([bffe52a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/bffe52a6d6356b79488ef867aabc3eb384af2435))
* **documentos#1116:** tambo for mobile ([f4d650f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f4d650f18602702f4cfccdf32758336da2041ca0)), closes [documentos#1116](http://gitlab.meta.com.br/documentos/issues/1116)
* **documentos#1136:** new translations ([17ac854](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/17ac8542f7377458140410f8431189719ecb316a)), closes [documentos#1136](http://gitlab.meta.com.br/documentos/issues/1136)
* **documentos#1240:** new translations ([e971f98](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e971f984cf0e4e1c1f06740bae6f2e757bcb4d81)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** new translations ([9839110](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/983911083b30e7eedd4e9302f860782ae51424cb)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** new translations ([a0065b5](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a0065b5a5128a730b80a000cac88a8c0b7d547f3)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1275:** changed web satellite images warning ([784e19c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/784e19cc5009d961ee2fc4945ef8d21633e8fc08)), closes [documentos#1275](http://gitlab.meta.com.br/documentos/issues/1275)
* **documentos#1282:** added new translation ([9a8035a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9a8035a75f4c382cf20dd024a9441431cef002b8)), closes [documentos#1282](http://gitlab.meta.com.br/documentos/issues/1282)
* **documentos#1290:** new translations ([f431dfc](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f431dfcf4b91f9296f2cfed8e5bff3c83441ef6c)), closes [documentos#1290](http://gitlab.meta.com.br/documentos/issues/1290)
* **documentos#1290:** new translations ([7190e06](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7190e069d652453de5d2e549b34211c2cc43d44d)), closes [documentos#1290](http://gitlab.meta.com.br/documentos/issues/1290)
* **documentos#1357:** changes in field monitoration ([2508a09](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2508a09a73f04732609b124c0c08aeaa59bd7668)), closes [documentos#1357](http://gitlab.meta.com.br/documentos/issues/1357)
* **documentos#1374:** new translations ([7144750](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7144750f917073641f475f4795f4db7ab288d7e6)), closes [documentos#1374](http://gitlab.meta.com.br/documentos/issues/1374)
* **documentos#1377:** terms page ([264599e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/264599e1181a0effe8ee8de496db5bc4b6bc1735)), closes [documentos#1377](http://gitlab.meta.com.br/documentos/issues/1377)
* **documentos#1391:** changes in commercialization values ([0a89d4a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0a89d4ad88723432951d1f726d9c122bada2d5d7)), closes [documentos#1391](http://gitlab.meta.com.br/documentos/issues/1391)
* **documentos#1400:** modal in web ([1113a0f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1113a0f9e31717ac455b11240c9bb9cf6f64aed6)), closes [documentos#1400](http://gitlab.meta.com.br/documentos/issues/1400)
* **documentos#1423:** adds warning snackbar if mngmt isn conc. ([13f4553](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/13f455348b48a86f2648c3d876244e7bca22cecb)), closes [documentos#1423](http://gitlab.meta.com.br/documentos/issues/1423)
* added new translation ([b4bedd1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b4bedd1ca20453b18218689309ad70ab34335a12))
* added new translation ([f02f12e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f02f12ea2acaf43e34f01d449f8046ddbbd55c5a))
* added new translation ([d4dc40b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d4dc40b95e6816c33babd835c7a9c3ff96fc597e))
* added new translation ([13c38d5](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/13c38d5aaae9a3d6b8901092bcbc60923cfeb4b3))
* added new translation ([a85fed1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a85fed1ff7a4ca8effb142a3f2df55eae18e1b33))
* added new translations ([49fa46c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/49fa46cd78620ba9590ae5642c32effbd523a6b2))
* added new translations ([97206d8](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/97206d88fc22b106bd7ae805c8a302585fc2a9b1))
* added new translations ([b50b59c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b50b59c7a7d744e2101aa3d3bc6998c5f4248d30))
* merge develop ([2731148](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2731148d7380e3ab5c8ec38d198b1773467e4364))
* resolve [#1335](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1335) ([6f4e819](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/6f4e819baa9fd1662f335ffba7a84f740613f13d))
* wip ([b3b6eb5](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b3b6eb57ca79dc596447e6b748d58c3b74087f14))
* wip ([2cf8dd6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2cf8dd6979ab9a9ee338858a586f2587ca18899e))
* **documentos#885:** changed field screen ([4299f89](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4299f898298c9641e4e63098dfdcf85a5633c11d)), closes [documentos#885](http://gitlab.meta.com.br/documentos/issues/885)





# [0.14.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.12.0-alpha.1...v0.14.0) (2021-03-23)


### Bug Fixes

* **#1280:** fix register milk quality mobile ([79e8429](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/79e8429ecfdf191d8f66e97004c844a6bbb76b2d))
* **#1301:** adjustment pt-br ([cc43b84](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/cc43b845f48d64fa3803ec81ba545e85e6dae4d9)), closes [#1301](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1301)
* **#1301:** fix i18n ([c905457](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c9054574287206f454af9bf4e84f8ec4e999a46d))
* **#1331:** fix open field like technical ([f6f2806](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f6f2806822f42e8e4a4e3028cabe612691dc9e6d))
* **documentos#1447:** ajustes mapa de chuva e relatorios ([9cd51a9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9cd51a9037291d437f5b847da66e8b7bf8a67998)), closes [documentos#1447](http://gitlab.meta.com.br/documentos/issues/1447)


### Features

* added new translation ([caac0b6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/caac0b6ad535ec5dd8472ebdfe4cdfcb0cdbced2))
* changed translation ([09b3535](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/09b35354884853c5fff12912f85ec5f8cd158b90))
* other changes to [#1523](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1523) ([737f718](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/737f718ce1eb816ff75e8762ce9dba84a86f7af7))
* resolve [#1385](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1385) ([0e47e32](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0e47e327bd8e84487252d935ed5c9fdf854580c0))
* resolve [#1424](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1424) ([9b126fa](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9b126fa8f24238c6a4abdb6b0482c02111d10e4a))
* resolve [#1523](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1523) ([bf353c3](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/bf353c3e7fceecbc43a53bd729d0b13e82c0ed40))
* resolving other bugs [#1523](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1523) ([8c82180](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8c82180d555659a897c2f7ab35c0f11b2588958c))
* **#1116:** link card milk dashboard with backend ([ac23145](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ac23145e2a63479585c7d38a16c8187c794cd05f)), closes [#1116](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1116)
* **#1182:** adjustment text pest report location and pin ([21c7da1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/21c7da14eb242f378890d6947cabbda961edff37)), closes [#1182](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1182) [#430](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/430)
* **#1301:** adjustment graphs web ([c567013](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c567013b84c3ab9443e25ba45bdddfbfc94f5ecc)), closes [#1301](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1301) [#399](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/399)
* **#1386:** adjustment graph milk quality ([9f25b09](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9f25b09a490c558c1e56eff23a86b132237e63ec)), closes [#1386](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1386) [#439](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/439)
* **999:** mapa de chuva web ([bffe52a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/bffe52a6d6356b79488ef867aabc3eb384af2435))
* **documentos#1116:** tambo for mobile ([f4d650f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f4d650f18602702f4cfccdf32758336da2041ca0)), closes [documentos#1116](http://gitlab.meta.com.br/documentos/issues/1116)
* **documentos#1136:** new translations ([17ac854](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/17ac8542f7377458140410f8431189719ecb316a)), closes [documentos#1136](http://gitlab.meta.com.br/documentos/issues/1136)
* **documentos#1240:** new translations ([e971f98](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e971f984cf0e4e1c1f06740bae6f2e757bcb4d81)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** new translations ([9839110](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/983911083b30e7eedd4e9302f860782ae51424cb)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** new translations ([a0065b5](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a0065b5a5128a730b80a000cac88a8c0b7d547f3)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1275:** changed web satellite images warning ([784e19c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/784e19cc5009d961ee2fc4945ef8d21633e8fc08)), closes [documentos#1275](http://gitlab.meta.com.br/documentos/issues/1275)
* **documentos#1282:** added new translation ([9a8035a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9a8035a75f4c382cf20dd024a9441431cef002b8)), closes [documentos#1282](http://gitlab.meta.com.br/documentos/issues/1282)
* **documentos#1290:** new translations ([f431dfc](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f431dfcf4b91f9296f2cfed8e5bff3c83441ef6c)), closes [documentos#1290](http://gitlab.meta.com.br/documentos/issues/1290)
* **documentos#1290:** new translations ([7190e06](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7190e069d652453de5d2e549b34211c2cc43d44d)), closes [documentos#1290](http://gitlab.meta.com.br/documentos/issues/1290)
* **documentos#1357:** changes in field monitoration ([2508a09](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2508a09a73f04732609b124c0c08aeaa59bd7668)), closes [documentos#1357](http://gitlab.meta.com.br/documentos/issues/1357)
* **documentos#1374:** new translations ([7144750](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7144750f917073641f475f4795f4db7ab288d7e6)), closes [documentos#1374](http://gitlab.meta.com.br/documentos/issues/1374)
* **documentos#1377:** terms page ([264599e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/264599e1181a0effe8ee8de496db5bc4b6bc1735)), closes [documentos#1377](http://gitlab.meta.com.br/documentos/issues/1377)
* **documentos#1391:** changes in commercialization values ([0a89d4a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0a89d4ad88723432951d1f726d9c122bada2d5d7)), closes [documentos#1391](http://gitlab.meta.com.br/documentos/issues/1391)
* **documentos#1400:** modal in web ([1113a0f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1113a0f9e31717ac455b11240c9bb9cf6f64aed6)), closes [documentos#1400](http://gitlab.meta.com.br/documentos/issues/1400)
* **documentos#1423:** adds warning snackbar if mngmt isn conc. ([13f4553](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/13f455348b48a86f2648c3d876244e7bca22cecb)), closes [documentos#1423](http://gitlab.meta.com.br/documentos/issues/1423)
* added new translation ([b4bedd1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b4bedd1ca20453b18218689309ad70ab34335a12))
* added new translation ([f02f12e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f02f12ea2acaf43e34f01d449f8046ddbbd55c5a))
* added new translation ([d4dc40b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d4dc40b95e6816c33babd835c7a9c3ff96fc597e))
* added new translation ([13c38d5](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/13c38d5aaae9a3d6b8901092bcbc60923cfeb4b3))
* added new translation ([a85fed1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a85fed1ff7a4ca8effb142a3f2df55eae18e1b33))
* added new translations ([49fa46c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/49fa46cd78620ba9590ae5642c32effbd523a6b2))
* added new translations ([97206d8](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/97206d88fc22b106bd7ae805c8a302585fc2a9b1))
* added new translations ([b50b59c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b50b59c7a7d744e2101aa3d3bc6998c5f4248d30))
* merge develop ([2731148](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2731148d7380e3ab5c8ec38d198b1773467e4364))
* resolve [#1335](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1335) ([6f4e819](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/6f4e819baa9fd1662f335ffba7a84f740613f13d))
* wip ([b3b6eb5](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b3b6eb57ca79dc596447e6b748d58c3b74087f14))
* wip ([2cf8dd6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2cf8dd6979ab9a9ee338858a586f2587ca18899e))
* **documentos#885:** changed field screen ([4299f89](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4299f898298c9641e4e63098dfdcf85a5633c11d)), closes [documentos#885](http://gitlab.meta.com.br/documentos/issues/885)





# [0.13.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.12.0-alpha.1...v0.13.0) (2021-03-23)


### Bug Fixes

* **#1280:** fix register milk quality mobile ([79e8429](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/79e8429ecfdf191d8f66e97004c844a6bbb76b2d))
* **#1301:** adjustment pt-br ([cc43b84](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/cc43b845f48d64fa3803ec81ba545e85e6dae4d9)), closes [#1301](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1301)
* **#1301:** fix i18n ([c905457](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c9054574287206f454af9bf4e84f8ec4e999a46d))
* **#1331:** fix open field like technical ([f6f2806](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f6f2806822f42e8e4a4e3028cabe612691dc9e6d))
* **documentos#1447:** ajustes mapa de chuva e relatorios ([9cd51a9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9cd51a9037291d437f5b847da66e8b7bf8a67998)), closes [documentos#1447](http://gitlab.meta.com.br/documentos/issues/1447)


### Features

* added new translation ([caac0b6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/caac0b6ad535ec5dd8472ebdfe4cdfcb0cdbced2))
* changed translation ([09b3535](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/09b35354884853c5fff12912f85ec5f8cd158b90))
* other changes to [#1523](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1523) ([737f718](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/737f718ce1eb816ff75e8762ce9dba84a86f7af7))
* resolve [#1385](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1385) ([0e47e32](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0e47e327bd8e84487252d935ed5c9fdf854580c0))
* resolve [#1424](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1424) ([9b126fa](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9b126fa8f24238c6a4abdb6b0482c02111d10e4a))
* resolve [#1523](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1523) ([bf353c3](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/bf353c3e7fceecbc43a53bd729d0b13e82c0ed40))
* resolving other bugs [#1523](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1523) ([8c82180](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8c82180d555659a897c2f7ab35c0f11b2588958c))
* **#1116:** link card milk dashboard with backend ([ac23145](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ac23145e2a63479585c7d38a16c8187c794cd05f)), closes [#1116](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1116)
* **#1182:** adjustment text pest report location and pin ([21c7da1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/21c7da14eb242f378890d6947cabbda961edff37)), closes [#1182](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1182) [#430](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/430)
* **#1301:** adjustment graphs web ([c567013](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c567013b84c3ab9443e25ba45bdddfbfc94f5ecc)), closes [#1301](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1301) [#399](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/399)
* **#1386:** adjustment graph milk quality ([9f25b09](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9f25b09a490c558c1e56eff23a86b132237e63ec)), closes [#1386](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1386) [#439](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/439)
* **999:** mapa de chuva web ([bffe52a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/bffe52a6d6356b79488ef867aabc3eb384af2435))
* **documentos#1116:** tambo for mobile ([f4d650f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f4d650f18602702f4cfccdf32758336da2041ca0)), closes [documentos#1116](http://gitlab.meta.com.br/documentos/issues/1116)
* **documentos#1136:** new translations ([17ac854](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/17ac8542f7377458140410f8431189719ecb316a)), closes [documentos#1136](http://gitlab.meta.com.br/documentos/issues/1136)
* **documentos#1240:** new translations ([e971f98](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e971f984cf0e4e1c1f06740bae6f2e757bcb4d81)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** new translations ([9839110](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/983911083b30e7eedd4e9302f860782ae51424cb)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** new translations ([a0065b5](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a0065b5a5128a730b80a000cac88a8c0b7d547f3)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1275:** changed web satellite images warning ([784e19c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/784e19cc5009d961ee2fc4945ef8d21633e8fc08)), closes [documentos#1275](http://gitlab.meta.com.br/documentos/issues/1275)
* **documentos#1282:** added new translation ([9a8035a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9a8035a75f4c382cf20dd024a9441431cef002b8)), closes [documentos#1282](http://gitlab.meta.com.br/documentos/issues/1282)
* **documentos#1290:** new translations ([f431dfc](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f431dfcf4b91f9296f2cfed8e5bff3c83441ef6c)), closes [documentos#1290](http://gitlab.meta.com.br/documentos/issues/1290)
* **documentos#1290:** new translations ([7190e06](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7190e069d652453de5d2e549b34211c2cc43d44d)), closes [documentos#1290](http://gitlab.meta.com.br/documentos/issues/1290)
* **documentos#1357:** changes in field monitoration ([2508a09](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2508a09a73f04732609b124c0c08aeaa59bd7668)), closes [documentos#1357](http://gitlab.meta.com.br/documentos/issues/1357)
* **documentos#1374:** new translations ([7144750](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7144750f917073641f475f4795f4db7ab288d7e6)), closes [documentos#1374](http://gitlab.meta.com.br/documentos/issues/1374)
* **documentos#1377:** terms page ([264599e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/264599e1181a0effe8ee8de496db5bc4b6bc1735)), closes [documentos#1377](http://gitlab.meta.com.br/documentos/issues/1377)
* **documentos#1391:** changes in commercialization values ([0a89d4a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0a89d4ad88723432951d1f726d9c122bada2d5d7)), closes [documentos#1391](http://gitlab.meta.com.br/documentos/issues/1391)
* **documentos#1400:** modal in web ([1113a0f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1113a0f9e31717ac455b11240c9bb9cf6f64aed6)), closes [documentos#1400](http://gitlab.meta.com.br/documentos/issues/1400)
* **documentos#1423:** adds warning snackbar if mngmt isn conc. ([13f4553](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/13f455348b48a86f2648c3d876244e7bca22cecb)), closes [documentos#1423](http://gitlab.meta.com.br/documentos/issues/1423)
* added new translation ([b4bedd1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b4bedd1ca20453b18218689309ad70ab34335a12))
* added new translation ([f02f12e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f02f12ea2acaf43e34f01d449f8046ddbbd55c5a))
* added new translation ([d4dc40b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d4dc40b95e6816c33babd835c7a9c3ff96fc597e))
* added new translation ([13c38d5](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/13c38d5aaae9a3d6b8901092bcbc60923cfeb4b3))
* added new translation ([a85fed1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a85fed1ff7a4ca8effb142a3f2df55eae18e1b33))
* added new translations ([49fa46c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/49fa46cd78620ba9590ae5642c32effbd523a6b2))
* added new translations ([97206d8](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/97206d88fc22b106bd7ae805c8a302585fc2a9b1))
* added new translations ([b50b59c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b50b59c7a7d744e2101aa3d3bc6998c5f4248d30))
* merge develop ([2731148](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2731148d7380e3ab5c8ec38d198b1773467e4364))
* resolve [#1335](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1335) ([6f4e819](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/6f4e819baa9fd1662f335ffba7a84f740613f13d))
* wip ([b3b6eb5](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b3b6eb57ca79dc596447e6b748d58c3b74087f14))
* wip ([2cf8dd6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2cf8dd6979ab9a9ee338858a586f2587ca18899e))
* **documentos#885:** changed field screen ([4299f89](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4299f898298c9641e4e63098dfdcf85a5633c11d)), closes [documentos#885](http://gitlab.meta.com.br/documentos/issues/885)





# [0.12.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.12.0-alpha.1...v0.12.0) (2021-03-23)


### Bug Fixes

* **#1280:** fix register milk quality mobile ([79e8429](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/79e8429ecfdf191d8f66e97004c844a6bbb76b2d))
* **#1301:** adjustment pt-br ([cc43b84](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/cc43b845f48d64fa3803ec81ba545e85e6dae4d9)), closes [#1301](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1301)
* **#1301:** fix i18n ([c905457](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c9054574287206f454af9bf4e84f8ec4e999a46d))
* **#1331:** fix open field like technical ([f6f2806](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f6f2806822f42e8e4a4e3028cabe612691dc9e6d))
* **documentos#1447:** ajustes mapa de chuva e relatorios ([9cd51a9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9cd51a9037291d437f5b847da66e8b7bf8a67998)), closes [documentos#1447](http://gitlab.meta.com.br/documentos/issues/1447)


### Features

* added new translation ([caac0b6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/caac0b6ad535ec5dd8472ebdfe4cdfcb0cdbced2))
* changed translation ([09b3535](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/09b35354884853c5fff12912f85ec5f8cd158b90))
* other changes to [#1523](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1523) ([737f718](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/737f718ce1eb816ff75e8762ce9dba84a86f7af7))
* resolve [#1385](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1385) ([0e47e32](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0e47e327bd8e84487252d935ed5c9fdf854580c0))
* resolve [#1424](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1424) ([9b126fa](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9b126fa8f24238c6a4abdb6b0482c02111d10e4a))
* resolve [#1523](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1523) ([bf353c3](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/bf353c3e7fceecbc43a53bd729d0b13e82c0ed40))
* resolving other bugs [#1523](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1523) ([8c82180](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8c82180d555659a897c2f7ab35c0f11b2588958c))
* **#1116:** link card milk dashboard with backend ([ac23145](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ac23145e2a63479585c7d38a16c8187c794cd05f)), closes [#1116](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1116)
* **#1182:** adjustment text pest report location and pin ([21c7da1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/21c7da14eb242f378890d6947cabbda961edff37)), closes [#1182](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1182) [#430](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/430)
* **#1301:** adjustment graphs web ([c567013](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c567013b84c3ab9443e25ba45bdddfbfc94f5ecc)), closes [#1301](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1301) [#399](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/399)
* **#1386:** adjustment graph milk quality ([9f25b09](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9f25b09a490c558c1e56eff23a86b132237e63ec)), closes [#1386](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1386) [#439](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/439)
* **999:** mapa de chuva web ([bffe52a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/bffe52a6d6356b79488ef867aabc3eb384af2435))
* **documentos#1116:** tambo for mobile ([f4d650f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f4d650f18602702f4cfccdf32758336da2041ca0)), closes [documentos#1116](http://gitlab.meta.com.br/documentos/issues/1116)
* **documentos#1136:** new translations ([17ac854](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/17ac8542f7377458140410f8431189719ecb316a)), closes [documentos#1136](http://gitlab.meta.com.br/documentos/issues/1136)
* **documentos#1240:** new translations ([e971f98](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e971f984cf0e4e1c1f06740bae6f2e757bcb4d81)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** new translations ([9839110](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/983911083b30e7eedd4e9302f860782ae51424cb)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** new translations ([a0065b5](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a0065b5a5128a730b80a000cac88a8c0b7d547f3)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1275:** changed web satellite images warning ([784e19c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/784e19cc5009d961ee2fc4945ef8d21633e8fc08)), closes [documentos#1275](http://gitlab.meta.com.br/documentos/issues/1275)
* **documentos#1282:** added new translation ([9a8035a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9a8035a75f4c382cf20dd024a9441431cef002b8)), closes [documentos#1282](http://gitlab.meta.com.br/documentos/issues/1282)
* **documentos#1290:** new translations ([f431dfc](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f431dfcf4b91f9296f2cfed8e5bff3c83441ef6c)), closes [documentos#1290](http://gitlab.meta.com.br/documentos/issues/1290)
* **documentos#1290:** new translations ([7190e06](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7190e069d652453de5d2e549b34211c2cc43d44d)), closes [documentos#1290](http://gitlab.meta.com.br/documentos/issues/1290)
* **documentos#1357:** changes in field monitoration ([2508a09](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2508a09a73f04732609b124c0c08aeaa59bd7668)), closes [documentos#1357](http://gitlab.meta.com.br/documentos/issues/1357)
* **documentos#1374:** new translations ([7144750](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7144750f917073641f475f4795f4db7ab288d7e6)), closes [documentos#1374](http://gitlab.meta.com.br/documentos/issues/1374)
* **documentos#1377:** terms page ([264599e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/264599e1181a0effe8ee8de496db5bc4b6bc1735)), closes [documentos#1377](http://gitlab.meta.com.br/documentos/issues/1377)
* **documentos#1391:** changes in commercialization values ([0a89d4a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0a89d4ad88723432951d1f726d9c122bada2d5d7)), closes [documentos#1391](http://gitlab.meta.com.br/documentos/issues/1391)
* **documentos#1400:** modal in web ([1113a0f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1113a0f9e31717ac455b11240c9bb9cf6f64aed6)), closes [documentos#1400](http://gitlab.meta.com.br/documentos/issues/1400)
* **documentos#1423:** adds warning snackbar if mngmt isn conc. ([13f4553](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/13f455348b48a86f2648c3d876244e7bca22cecb)), closes [documentos#1423](http://gitlab.meta.com.br/documentos/issues/1423)
* added new translation ([b4bedd1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b4bedd1ca20453b18218689309ad70ab34335a12))
* added new translation ([f02f12e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f02f12ea2acaf43e34f01d449f8046ddbbd55c5a))
* added new translation ([d4dc40b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d4dc40b95e6816c33babd835c7a9c3ff96fc597e))
* added new translation ([13c38d5](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/13c38d5aaae9a3d6b8901092bcbc60923cfeb4b3))
* added new translation ([a85fed1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a85fed1ff7a4ca8effb142a3f2df55eae18e1b33))
* added new translations ([49fa46c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/49fa46cd78620ba9590ae5642c32effbd523a6b2))
* added new translations ([97206d8](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/97206d88fc22b106bd7ae805c8a302585fc2a9b1))
* added new translations ([b50b59c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b50b59c7a7d744e2101aa3d3bc6998c5f4248d30))
* merge develop ([2731148](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2731148d7380e3ab5c8ec38d198b1773467e4364))
* resolve [#1335](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1335) ([6f4e819](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/6f4e819baa9fd1662f335ffba7a84f740613f13d))
* wip ([b3b6eb5](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b3b6eb57ca79dc596447e6b748d58c3b74087f14))
* wip ([2cf8dd6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2cf8dd6979ab9a9ee338858a586f2587ca18899e))
* **documentos#885:** changed field screen ([4299f89](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4299f898298c9641e4e63098dfdcf85a5633c11d)), closes [documentos#885](http://gitlab.meta.com.br/documentos/issues/885)





# [0.12.0-alpha.1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.12.0-alpha.0...v0.12.0-alpha.1) (2021-02-22)


### Bug Fixes

* **documentos#1240:** bug fixes ([bb4562a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/bb4562aa03fb046c9ee90103777f114de1d30b8d)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)


### Features

* added new translation ([c2b283d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c2b283d325b79c0e506eca2b540f561612746d2f))
* added new translation ([e2dbf29](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e2dbf2970776ed10527b5018679ced4aef3beb56))
* added new translations ([4ffc42d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4ffc42d8d7b6a33d80a636f29d0aa650090e7ae1))
* added new translations ([a930055](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a930055c67d8d28bc2060dac4b961967060abe8f))
* merge develop ([14437f2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/14437f2c05646aadaf548086d38f70dcf5054eda))
* merge develop ([79eb412](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/79eb412585a0950ecf6c39cb7132c1648068fbca))
* resolve [#1223](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1223) ([63b4f42](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/63b4f420a8b31ba0dd810591d679e5140ce6596f))
* resolve [#1244](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1244) ([2bdd8b6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2bdd8b6fb2f2d71672a5688054bf25a4d4db000b))
* resolve [#1260](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1260) ([12114c2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/12114c2f9f29942a0c124bf71adb04b03ba7c4a0))
* **documentos#1233:** adjustments to satelite image view ([7067ebd](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7067ebd59103007d509c352ae4f045b067b2157c)), closes [documentos#1233](http://gitlab.meta.com.br/documentos/issues/1233)
* **documentos#535:** finished home screen for mobile ([b595eee](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b595eeed9efc2c6371bb04276a22abfd74a1d9db)), closes [documentos#535](http://gitlab.meta.com.br/documentos/issues/535)
* **documentos#947:** added new translations ([ee77422](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ee77422e0bed31ece32bdb5d5beca1d0877dc8bc)), closes [documentos#947](http://gitlab.meta.com.br/documentos/issues/947)
* resolve [#992](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/992) ([3cc24d7](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3cc24d721b1e0e8243e9ec84d51677e3ee50de32))
* **documentos#987:** new translations ([b732c53](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b732c53cd6a8d891c5593d3b03cbfc177d8a297e)), closes [documentos#987](http://gitlab.meta.com.br/documentos/issues/987)
* **documentos#987:** new translatiosn ([9e677e3](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9e677e3b67f0a2b5d0c702b9e1a9299a0622acc4)), closes [documentos#987](http://gitlab.meta.com.br/documentos/issues/987)





# [0.12.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.11.0...v0.12.0-alpha.0) (2021-02-12)

**Note:** Version bump only for package @smartcoop/i18n





# [0.11.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.11.0-alpha.0...v0.11.0) (2021-02-12)


### Bug Fixes

* **style:** fixed behaviour of the index navigator ([fa4a17e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/fa4a17e05a5c995a1189b225612c8eaa4664d301))


### Features

* **documentos#1173:** new translatiosn ([8eb1d02](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8eb1d029597bc6c3ec1a06d48be19f7331f1217c)), closes [documentos#1173](http://gitlab.meta.com.br/documentos/issues/1173)
* **documentos#1197:** new translatiosn ([6654adc](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/6654adc8e04ddc3eff21204e72cfa03178ab25f6)), closes [documentos#1197](http://gitlab.meta.com.br/documentos/issues/1197)
* merge ([d140afe](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d140afe349dc1eb0ffaa60c4d003ca6d257b876f))
* resolve [#1117](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1117) ([28e2c23](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/28e2c2337354377dcc21ce4b40037525a7ab0900))
* wip ([ec87885](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ec878853d3b3be26e7a97c2a2a81698f4d5532e8))
* **#1142:** resolve conflicts ([3abbaa9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3abbaa9ec9eaaa849f653fa5c073f89fabd56167)), closes [#1142](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1142)
* resolve [#1158](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1158) and [#1159](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1159) ([2f6d494](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2f6d49444320d8c3ba78955b7073e81ca422dd3a))
* wip ([9028291](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9028291ac804514bf2d887a9106feba4dd7f79ab))
* wip ([395287f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/395287fad709cf45b1d58c1f8517d60fe4f80bb0))
* wip ([6e51d5c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/6e51d5cab2862c611d09c96d23a4df259d1124f3))





# [0.11.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.10.0...v0.11.0-alpha.0) (2021-02-05)

**Note:** Version bump only for package @smartcoop/i18n





# [0.10.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.10.0-alpha.0...v0.10.0) (2021-02-01)


### Bug Fixes

* fixed number translation ([46bd110](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/46bd110b9a7fef9e4642bb2e2a955f3d294a2ddd))
* **#1003:** adjustment title technical visit screen mobile ([f3aa7a8](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f3aa7a8bc0143235b9f06bd5a6eb8a32284da92a)), closes [#1003](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1003)
* **lint:** fixed lint ([2ff2c20](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2ff2c20f4c63a8b554fc10c408d14d8657544323))


### Features

* **documentos#1006:** created technical portfolio screen ([8f964b7](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8f964b73604e5520e9b956cca6855770abc6a89c)), closes [documentos#1006](http://gitlab.meta.com.br/documentos/issues/1006)
* **documentos#987:** new translations ([94955aa](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/94955aaee8742545f4577275acc874f9a7a0d8e1)), closes [documentos#987](http://gitlab.meta.com.br/documentos/issues/987)
* added new translations ([b52bfe2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b52bfe279c4e8046b02750322324c80b4eb4b068))
* added new translations ([2534b58](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2534b58143c9349cbcb94859381c7346c7377ae1))
* changes sales orders validation message ([24063ba](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/24063bae6003280b31dfbe8fdc508494ca85bfd5))
* resolve [#1042](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1042) ([0e71dc1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0e71dc1ea7ac612b73620b29efb259d301340f8d))
* resolve [#1044](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1044) ([5a5038c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/5a5038c68c9a2834348789b85fe013f1e49c3ec1))
* **att calendar:** att calendar ([b7e1530](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b7e153074e5ac740c9f448915fd3350358a103e1))
* **documentos#1007:** merge ([a4b87ec](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a4b87ec22510f0250422035ac135ba5b12b5aa3b)), closes [documentos#1007](http://gitlab.meta.com.br/documentos/issues/1007)
* **documentos#1007:** merge ([7ca3928](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7ca392846c11d5f33f8ac0e76923934867baa4ae)), closes [documentos#1007](http://gitlab.meta.com.br/documentos/issues/1007)
* **documentos#1007:** merge ([60257b7](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/60257b75717fa0a8c34393609f81d315e8505bbe)), closes [documentos#1007](http://gitlab.meta.com.br/documentos/issues/1007)
* **documentos#102:** adds new translations ([ac2a5fa](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ac2a5facb5768960d32f6e839b0ae2b53a25df89)), closes [documentos#102](http://gitlab.meta.com.br/documentos/issues/102)
* **documentos#102:** adds new translations ([26f4791](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/26f4791ba7650304256156c04d95a94deaa4540b)), closes [documentos#102](http://gitlab.meta.com.br/documentos/issues/102)
* **documentos#102:** new icon and translations ([cb986ea](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/cb986ea511b90597593b1461c09d2074b59cbe6d)), closes [documentos#102](http://gitlab.meta.com.br/documentos/issues/102)
* **documentos#102:** new translations ([171b693](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/171b6934b1076e883ce5d5354d0b7850959a174f)), closes [documentos#102](http://gitlab.meta.com.br/documentos/issues/102)
* **documentos#987:** new translations ([d01e495](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d01e495ff0ed6b51d35236cdc158b75f3de88a5b)), closes [documentos#987](http://gitlab.meta.com.br/documentos/issues/987)
* **documentos#987:** new translations ([f9a0fb4](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f9a0fb441bddb178c56e7455bdc88ec70e975604)), closes [documentos#987](http://gitlab.meta.com.br/documentos/issues/987)
* **documentos#987:** new translations ([f1ffb63](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f1ffb63677f374fccfa48f8d5e754750b57102ef)), closes [documentos#987](http://gitlab.meta.com.br/documentos/issues/987)
* **documentos#987:** new translations ([51f14cb](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/51f14cb765eb71156d0b5ae5c9ec1f712b56f6d7)), closes [documentos#987](http://gitlab.meta.com.br/documentos/issues/987)
* **documentos#987:** new translations ([1966298](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/19662989c0a52d9e9a409ee8aee3cff9d332d939)), closes [documentos#987](http://gitlab.meta.com.br/documentos/issues/987)
* **documentos#987:** new translations ([51b27d9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/51b27d9b127b7e416793c6e24acf795212be11f6)), closes [documentos#987](http://gitlab.meta.com.br/documentos/issues/987)
* resolve merge conflicts ([0646a07](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0646a07c03edcc03fe7ad4c3b58200a3fdaf5369))
* **documentos#987:** new translations ([2e17f83](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2e17f830d195fe67c75b17de7ee21683103b6e8d)), closes [documentos#987](http://gitlab.meta.com.br/documentos/issues/987)
* **documentos#987:** new translations ([146b880](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/146b8804304fec4bd9bba650498f5b6e5d62ca67)), closes [documentos#987](http://gitlab.meta.com.br/documentos/issues/987)
* **documentos#987:** new translations ([e92ba07](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e92ba07e3693e7f1debc02a75ced7769054503dc)), closes [documentos#987](http://gitlab.meta.com.br/documentos/issues/987)
* resolve [#990](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/990) ([0a9226e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0a9226e45797c9e3c129b2919235c48ea23ebd51))
* resolve merge conflicts ([d76333d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d76333db93b61f9897987ee033d050dcdc48d8fe))
* wip ([4dc386b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4dc386b623a070ce963ddc5f7d948d1e36ab7adf))
* wip ([937c1de](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/937c1dee1be0f88f8c0dbae3553c84e2622bd2df))
* **documentos#987:** new translations ([fff269b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/fff269bc039a783715760a0f961d47a7c4b910b4)), closes [documentos#987](http://gitlab.meta.com.br/documentos/issues/987)
* wip ([ea43347](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ea4334798c6090ec6819e6390596359d2bf439de))
* wip ([094d424](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/094d4242f82345f35ecd7b4227e3c497da75c966))
* **documentos#987:** new translations ([5baeffc](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/5baeffc9ad1e87f8026a96ec1e99567f1327dff1)), closes [documentos#987](http://gitlab.meta.com.br/documentos/issues/987)
* **documentos#987:** new translations ([d55d2ef](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d55d2efdb27fe06a24b6e3a908f5d1e63de7d955)), closes [documentos#987](http://gitlab.meta.com.br/documentos/issues/987)
* wip ([0ca3dd6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0ca3dd60b0ecc3ff53c4915f74af3a0bba4c6c0b))
* wip ([87376e6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/87376e6eac23b997905919a6c85665ee38de740d))





# [0.10.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.9.0...v0.10.0-alpha.0) (2021-01-19)

**Note:** Version bump only for package @smartcoop/i18n





# [0.9.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.8.2...v0.9.0) (2021-01-19)


### Bug Fixes

* **#882:** add unit dose in fertilization form ([0b2d800](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0b2d80047bb9c9bef1f40dc4acdbcdc74c7a652f)), closes [#882](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/882)


### Features

* **#873:** add new texts ([59d7c54](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/59d7c546b681dfd69be81a5a9775bab480448d0f)), closes [#873](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/873)
* added new translations ([a6aa0d8](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a6aa0d87ea466092b53b6af738bd87c499c86507))
* changed sales order forms ([a4dccbe](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a4dccbe39baa24fcd5462a81c56f7ebdffe55e3b))
* merge Develop ([13f52cf](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/13f52cfce97d5e68ce271d48dc7e493ee065e28c))
* **#849:** adjustment layout ([8a2168b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8a2168b97d05cf9cd3ce0f29dcee001666ae1f91)), closes [#849](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/849)
* **#862:** add new texts ([044bcb7](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/044bcb73993e184a17931ba10200f4c94a8e43ed)), closes [#862](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/862) [#224](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/224)
* **ajustada label hour:** ajustada label hour ([cb630bc](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/cb630bc0c4781e2c3b0800bf7d0fe6796fde5fce))
* **documentos#299:** fix translation typo ([8f375a6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8f375a66d6c1f6dc66f8ca700b32f05948006e85))
* **documentos#299:** new translation ([6f379c6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/6f379c678ea180843318f55ecb021ffe94b17653)), closes [documentos#299](http://gitlab.meta.com.br/documentos/issues/299)
* **documentos#299:** new translation ([f515752](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f5157522c22fe364f510cdae94f8768a87aa5b4d)), closes [documentos#299](http://gitlab.meta.com.br/documentos/issues/299)
* **documentos#299:** new translations ([0dae89b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0dae89b249aef380c75c9c4acfd1f7e34b841dfb)), closes [documentos#299](http://gitlab.meta.com.br/documentos/issues/299)
* **documentos#299:** new translations ([2570e3c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2570e3c30a2668cac360e75d198e3c8c2255fe4a)), closes [documentos#299](http://gitlab.meta.com.br/documentos/issues/299)
* added new translations ([6fdfb60](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/6fdfb6014b45286569222369c96fe939178b08a2))
* merge develop ([5b164a0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/5b164a012ca32970cb8bf82724781871ab529e58))
* merge Develop ([4a43e2a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4a43e2a79e976c5459355a3dd2e0892e8c081e24))
* rebase with develop ([da7e90e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/da7e90e31d9e8205abbd429866bcb9956f34a0ca))
* rebase with develop ([c17badd](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c17baddb1b38f98b530f474f11e9bec44987ee96))
* resolve [#894](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/894) ([4fa8b43](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4fa8b43fd81e54697bfe7d3d701ef434fe0798a7))
* resolve [#920](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/920) ([d4f7839](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d4f783957ef1027d031ad535835c3b1f9f75a7a3))
* **documentos#836:** created notifications list ([f062230](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f062230640b49f3df88ad8a2a6ec5b447913dc69)), closes [documentos#836](http://gitlab.meta.com.br/documentos/issues/836)
* resolve [#805](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/805) ([1f50f3d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1f50f3db7106888842074940651e632681517f6f))
* wIP ([1fdaea5](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1fdaea5105a25ec89d89395a7db1ab22a87bbc22))
* wIP ([80ae937](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/80ae937854988750ccbe9f424a095c216731f73a))
* wIP - Rain Record ([7513f0d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7513f0d51ab117c4df1913df5e8d48d3548c7a08))
* wIP Social Comments ([9f3dad3](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9f3dad309defcb5115fd48a60d7158a466a06855))
* **geotiff download:** added button to download a .tiff satelite image ([4ec244c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4ec244cc57d41825fe39934d8c191b0b152bc621))
* **satelite images:** added cloudy weather indicator and ordered dates ([97467ad](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/97467ad8c35a88742560b9abd4941e8914b2e679))





## [0.8.2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.8.1...v0.8.2) (2021-01-06)


### Features

* **documentos#491:** added new translations ([4cbdef6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4cbdef618ff90db84078cd4f8f4bc08ec198b195)), closes [documentos#491](http://gitlab.meta.com.br/documentos/issues/491)
* **documentos#725:** new translation ([fcc7157](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/fcc7157bcc678623396909521f34cbc7861c34cd)), closes [documentos#725](http://gitlab.meta.com.br/documentos/issues/725)
* **documentos#725:** plural -> singular ([cda594a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/cda594a15d61315153938184b713f28733555ec9)), closes [documentos#725](http://gitlab.meta.com.br/documentos/issues/725)
* **feats commented:** removed activities on property edition ([1617b29](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1617b296f29041eb3290b962b7842b678a10257b))
* **property actions:** added actions to delete and edit property ([c80765a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c80765aaf6a2352a2c5ef2666eb6d2301280c0e4))





## [0.8.1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.9.0-alpha.0...v0.8.1) (2021-01-06)


### Features

* **#629:** adjustment ([7e88df1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7e88df14f427ca39278d148e00644c7529441264)), closes [#629](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/629)





# [0.9.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.8.0...v0.9.0-alpha.0) (2021-01-05)

**Note:** Version bump only for package @smartcoop/i18n





# [0.8.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.8.0-alpha.0...v0.8.0) (2021-01-05)


### Bug Fixes

* **documentos#321:** adicionada exibição de dias na previsão do tempo ([25c8472](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/25c8472a3463d28226a65887b6ddd61c31d68d0c)), closes [documentos#321](http://gitlab.meta.com.br/documentos/issues/321)
* **documentos#321:** implementada satelite mobile ([7a6f77e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7a6f77e9aeb52482b60c4524c85dd47019a192db)), closes [documentos#321](http://gitlab.meta.com.br/documentos/issues/321)
* **documentos#603:** adjust pt-BR file ([ea8bb80](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ea8bb80ba96c914635a4b716a1976e01634379da)), closes [documentos#603](http://gitlab.meta.com.br/documentos/issues/603)


### Features

* added new translations ([fd2cb8d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/fd2cb8d65acdb0df50c64f6b7b635ebfab745c35))
* added new translations ([2df236c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2df236c2e005cf6bc86400471db48f8a44906bea))
* **#539:** add new text ([5b7d535](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/5b7d535491dd215f6ee304c9e517774756297541)), closes [#539](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/539) [#174](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/174)
* **#541:** add new texts ([2dcc956](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2dcc95622f87dee2aa987765f7a21c42d00558c3)), closes [#541](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/541) [#176](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/176)
* **#629:** add new texts ([8657507](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8657507eb402728d55d56abfeb4b5608291a063d)), closes [#629](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/629)
* **321:** previsao do tempo mobile ([0db0a4b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0db0a4b8761cd1deb3c498313fcd0de1e3409fd7))
* **445:** adicionada paleta de cores ([7e25bc2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7e25bc2d7327aeee86d534ca51ecc820b7dc6bca))
* **803:** implementado destaque de melhor proposta ([72903f9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/72903f957ace89b4ed13e0d722bd810871079278))
* **ajustado background login:** ajustado background login ([d8a6955](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d8a695504c16b793d3e4a62c36ff33fa75d0ddf9))
* **ajustes sms e direcionamento:** ajustes sms e direcionamento ([a7f7a74](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a7f7a742a8a6f4c977ca06d48f7063c459b567a1))
* **documentos#143:** adds new translation ([1bd401b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1bd401bea52c1c97ff1e6308dc710ed67acda239)), closes [documentos#143](http://gitlab.meta.com.br/documentos/issues/143)
* **documentos#176 documentos#143:** product balance and withdraw list ([076dae2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/076dae26e34aecb1a671e86e4d40eb574521c4db)), closes [documentos#176](http://gitlab.meta.com.br/documentos/issues/176) [documentos#143](http://gitlab.meta.com.br/documentos/issues/143)
* **documentos#492:** adds button to receive ([6785be2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/6785be284961801f320a075216e11ba853fcfe1a)), closes [documentos#492](http://gitlab.meta.com.br/documentos/issues/492)
* **documentos#501:** added new strings ([b791e21](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b791e21188b7f8094eac454944d6962296692b47)), closes [documentos#501](http://gitlab.meta.com.br/documentos/issues/501)
* **documentos#501:** added new strings ([383f6ae](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/383f6aeb5313e5567454ce8a7f233e736115744b)), closes [documentos#501](http://gitlab.meta.com.br/documentos/issues/501)
* **documentos#501:** new translations ([7972528](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/797252854e20f0d63bac5518a79a551fddd3cb5d)), closes [documentos#501](http://gitlab.meta.com.br/documentos/issues/501)
* **documentos#501:** new translations ([475a275](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/475a275393ba69e09beb99c75557b2341c443077)), closes [documentos#501](http://gitlab.meta.com.br/documentos/issues/501)
* **documentos#502:** temporary machine details ([30e5885](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/30e5885b480fd51c27ba47f53129dbe79a98a26e)), closes [documentos#502](http://gitlab.meta.com.br/documentos/issues/502)
* **documentos#504:** adds new translations ([3489f9d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3489f9dc1a1f246bd238ade8e4c408ffbd8448c8)), closes [documentos#504](http://gitlab.meta.com.br/documentos/issues/504)
* **documentos#506:** adds new translations ([b86f2e9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b86f2e990317e51aed40132cbeda07c3f527634e)), closes [documentos#506](http://gitlab.meta.com.br/documentos/issues/506)
* **documentos#506:** new translations ([7b5d7e1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7b5d7e12d786546f84057541df7ed9a83712c35d)), closes [documentos#506](http://gitlab.meta.com.br/documentos/issues/506)
* **documentos#506:** new translations ([3d0f274](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3d0f274a8cecec9c89cbfc7ff1b7a12b6a81325f)), closes [documentos#506](http://gitlab.meta.com.br/documentos/issues/506)
* **documentos#507:** adds new translations ([5a6b734](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/5a6b734cc7768fab079963fdfd606f092e2416dc)), closes [documentos#507](http://gitlab.meta.com.br/documentos/issues/507)
* **documentos#688:** added AccordinsGroup into FieldDetailsScreen - web ([a8dfac0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a8dfac0f34255ec0aa03f8ad0401842b5c005b1f)), closes [documentos#688](http://gitlab.meta.com.br/documentos/issues/688)
* **documentos#688:** added chart to web ([84a8fcb](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/84a8fcb47c4d7a12a798460379ecc025c71806fb)), closes [documentos#688](http://gitlab.meta.com.br/documentos/issues/688)
* **documentos#688:** created InputDateRange for mobile ([ef71bf4](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ef71bf437ac74d21d6ac335a9496e4748cf2601a)), closes [documentos#688](http://gitlab.meta.com.br/documentos/issues/688)
* **documentos#688:** finished field details ([f051b4e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f051b4ec43bbd5103f739761adbec3d395561cc2)), closes [documentos#688](http://gitlab.meta.com.br/documentos/issues/688)
* **documentos#702:** new translations ([ae54d4b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ae54d4b94daaec8670139fdd21617344d5d94054)), closes [documentos#702](http://gitlab.meta.com.br/documentos/issues/702)
* **propery list screen:** update to the propery list screen ([7a7f4cf](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7a7f4cfa55c112a0591e90fe527d1ef4db91f617))
* created sales orders list ([294f2f3](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/294f2f3f7a9181f5bf44c0ec4b49d4b9fc50538e))
* **documentos#71:** created securities movement list screen ([ff7f6f8](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ff7f6f8d33c16e6468eab269a9f4384b95cde697)), closes [documentos#71](http://gitlab.meta.com.br/documentos/issues/71)
* **documentos#79:** added rainMm into field timeline chart ([d258c00](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d258c0043f4b363a13b01870f70964a517b8bdbb)), closes [documentos#79](http://gitlab.meta.com.br/documentos/issues/79)
* **merge com develop:** merge com develop ([fd25aa9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/fd25aa96da3ce80b7aca3568603a80d8a0b7b104))





# [0.8.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.7.0...v0.8.0-alpha.0) (2020-12-11)

**Note:** Version bump only for package @smartcoop/i18n





# [0.7.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.7.0-alpha.0...v0.7.0) (2020-12-11)


### Bug Fixes

* **#518:** do not delete field with registered cultivation ([e0b5746](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e0b57461080af127a09d62922d8d798a72ddc1c4)), closes [#518](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/518) [#170](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/170)
* **documentos#498:** adds new translation ([2d3c63e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2d3c63ec3b52f3d1bb76693c78632674ebc178be)), closes [documentos#498](http://gitlab.meta.com.br/documentos/issues/498)
* **material-table:** added patch from material-table ([9aa5f72](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9aa5f724d68dd39cba765fb1fa69a44ac217c06b))


### Features

* **#350:** add new texts ([1cd684d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1cd684d7554037df05c618dd5474aef82f40b15a)), closes [#350](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/350) [#133](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/133)
* **#397:** add new text for Confirm Modal ([538563c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/538563c40bf1443b5625e9c5f64d83da6a37452b)), closes [#397](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/397) [#139](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/139)
* **#407:** adjustment pt-br ([8af813a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8af813a63e055333c4dce2b53371ffe2ca1a4e76)), closes [#407](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/407) [#164](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/164)
* **#420:** add news demand status and refactory ([4ced1d3](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4ced1d3c48bc76e546b2a913a23079ba3c9ec3af)), closes [#420](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/420) [#146](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/146)
* **#518:** add new text and fix other ([641010d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/641010defaea37554b35ddd5679d698824fc17c4))
* **$407:** add new text ([c49f2d6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c49f2d6ed6f7ee628d5a7e91e4871da3302abd4e)), closes [#159](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/159)
* **documentos#103:** add translations to edit fields ([05a0f1e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/05a0f1e080e31771045ad27c759a28fbf4975e21)), closes [documentos#103](http://gitlab.meta.com.br/documentos/issues/103)
* **documentos#195:** added new translations ([9439b8e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9439b8eef22b150d42538cd30384f6437d9573ac)), closes [documentos#195](http://gitlab.meta.com.br/documentos/issues/195)
* **documentos#195:** added new translations ([735ad4b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/735ad4b512f5eec6a10da4b721b04bcea715717a)), closes [documentos#195](http://gitlab.meta.com.br/documentos/issues/195)
* **documentos#239:** fix missing comma ([e9ab5fe](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e9ab5fe50b63cf15510589cf9c013a923d219991))
* **documentos#262:** add new translations ([d8aed93](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d8aed93d9a0f5b14705e2a28a8ab25398f2f2929)), closes [documentos#262](http://gitlab.meta.com.br/documentos/issues/262)
* **documentos#308:** changed mobile navigation ([6a06eee](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/6a06eee9034387bee975cf9eab8c20bfe960f2df)), closes [documentos#308](http://gitlab.meta.com.br/documentos/issues/308)
* **documentos#360:** created delivery locations ([295d06d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/295d06d08a8bd875612743c0a181eff07350ef85)), closes [documentos#360](http://gitlab.meta.com.br/documentos/issues/360)
* **documentos#385:** adds empty state fields ([34498ba](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/34498ba9e3f4055595fbbaacb51080c5f4e6e470)), closes [documentos#385](http://gitlab.meta.com.br/documentos/issues/385)
* **documentos#393:** social network finished on mobile devices ([e838996](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e838996f44d0b26d857c47b062b6b1ecf0bdb809)), closes [documentos#393](http://gitlab.meta.com.br/documentos/issues/393)
* **documentos#398:** adds new translation ([986619a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/986619ad78ad87bd3638c267f42d94fd01a0fc53)), closes [documentos#398](http://gitlab.meta.com.br/documentos/issues/398)
* **documentos#398:** adds new translations ([ea3c23a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ea3c23adefdf04dffe052130b09da6fdbdfe95e5)), closes [documentos#398](http://gitlab.meta.com.br/documentos/issues/398)
* **documentos#398:** adds translations ([7049764](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/70497644877dadbbd811555f27f551b9f55295e6)), closes [documentos#398](http://gitlab.meta.com.br/documentos/issues/398)
* **documentos#398:** new translations ([025aa78](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/025aa78501ca73e43df644618801aa19ecc39fdc)), closes [documentos#398](http://gitlab.meta.com.br/documentos/issues/398)
* **documentos#400:** added empty states ([ec28dc9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ec28dc91cc111d229e4d0a981c7acfaf378b984c)), closes [documentos#400](http://gitlab.meta.com.br/documentos/issues/400)
* **documentos#424:** adjust ReadMore component ([bbc0aed](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/bbc0aed02c2e87b575eaa8e0b7dfdddbad944df3)), closes [documentos#424](http://gitlab.meta.com.br/documentos/issues/424)
* **documentos#428:** add Post component ([7f9e9be](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7f9e9be79a4ee8cfb6e61df7f3ea93ae83eee9c2)), closes [documentos#428](http://gitlab.meta.com.br/documentos/issues/428)
* **documentos#451:** created publish screen for mobile ([626b575](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/626b575f4021c206b0900c884d2f943d5238c47f)), closes [documentos#451](http://gitlab.meta.com.br/documentos/issues/451)
* **documentos#452:** localization of moment string ([921b0f9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/921b0f94944278357ad556d572afa1aa65a4507d)), closes [documentos#452](http://gitlab.meta.com.br/documentos/issues/452)
* **documentos#455:** added new translation ([985e5ef](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/985e5efee2445bd6866ed88d60cca0ed157652e8)), closes [documentos#455](http://gitlab.meta.com.br/documentos/issues/455)
* **documettos#362:** added new strings ([aed1aab](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/aed1aab82d017f230b1155dd0fcb037090caa1d8)), closes [documettos#362](http://gitlab.meta.com.br/documettos/issues/362)
* added new string ([5123964](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/512396487728d26b77da466204f4c2c22896a222))
* **documentos#459:** created social and notifications screens ([98e93e7](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/98e93e7ae3c6e4c8f6cda5ace7518ee36cc86495)), closes [documentos#459](http://gitlab.meta.com.br/documentos/issues/459)
* **documentos#459:** created social network screen ([862b5e9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/862b5e9024c0da5c96e54f83583c7f8a6bb9e926)), closes [documentos#459](http://gitlab.meta.com.br/documentos/issues/459)
* **documettos#362:** added new strings ([8848239](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8848239a1f201c5d419ae160dfa393f31428ddee)), closes [documettos#362](http://gitlab.meta.com.br/documettos/issues/362)





# [0.7.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.6.0...v0.7.0-alpha.0) (2020-11-27)

**Note:** Version bump only for package @smartcoop/i18n





# [0.6.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.5.3...v0.6.0) (2020-11-27)


### Bug Fixes

* **documentos#107:** adds comma in translations ([a66460e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a66460e803b335c433c02c568017912eb0910f14)), closes [documentos#107](http://gitlab.meta.com.br/documentos/issues/107)
* **documentos#16:** adds new translation ([51fafb4](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/51fafb4be80661196873c2104bd010fbfb0643f1)), closes [documentos#16](http://gitlab.meta.com.br/documentos/issues/16)
* **documentos#342:** changed shopping platform translation ([1161e50](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1161e507346f3f56b01844bdde2ef0191876b02b)), closes [documentos#342](http://gitlab.meta.com.br/documentos/issues/342)
* **documentos#353:** removed "to start" from select property modal ([915d11f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/915d11f3fed33c08eaa5bad55d0616ee6bcf080d)), closes [documentos#353](http://gitlab.meta.com.br/documentos/issues/353) [#120](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/120)
* **documentos#353:** removed all others "to start" ([a65f078](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a65f078e1b41b2274c1f3e67ea25894d945ec912)), closes [documentos#353](http://gitlab.meta.com.br/documentos/issues/353) [#120](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/120)
* **documentos#353:** removing comment ([d68dc7f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d68dc7fba479ef61ace00c3725303efc19a048e2)), closes [documentos#353](http://gitlab.meta.com.br/documentos/issues/353) [#120](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/120)


### Features

* **#282:** add news icons and news text ([f54764d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f54764d73781b89046e7c538309f038168fd1909)), closes [#282](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/282) [#103](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/103)
* **#291:** add new text ([2ce2533](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2ce2533512f3204f183d2a640a4ff937f6b94c62)), closes [#291](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/291) [#109](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/109)
* **#307:** add new text i18n ([2326aa9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2326aa9707e69b1945399c7b5e80fda83db4bfbd)), closes [#307](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/307)
* **#381:** add new texts hide and show balance ([3459c6a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3459c6af6e843b8d7d33196ccabe264705a41e41)), closes [#381](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/381) [#125](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/125)
* **documentos#107:** added new translations ([0fb937f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0fb937fd76853b0abd43b194eb0269dc50c51d9e)), closes [documentos#107](http://gitlab.meta.com.br/documentos/issues/107)
* **documentos#107:** merge translations ([67456fc](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/67456fc06005f0102c3bd6dc70437c8c924843d7)), closes [documentos#107](http://gitlab.meta.com.br/documentos/issues/107)
* **documentos#16:** added new translations ([386caf9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/386caf97ccefde8789611aaf4417a64f3aaf9299)), closes [documentos#16](http://gitlab.meta.com.br/documentos/issues/16)
* **documentos#16:** added new translations ([879ae09](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/879ae09a373a0e6cf6cd2b517b4b4707e8c9738c)), closes [documentos#16](http://gitlab.meta.com.br/documentos/issues/16)
* **documentos#16:** adds new translations ([dcafb3e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/dcafb3edba438ea7e6c6d04dee62c6ce42692b3d)), closes [documentos#16](http://gitlab.meta.com.br/documentos/issues/16)
* **documentos#194:** added new translation ([3cfbc40](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3cfbc4048ef2f5b7566bf96e7f8200cda7519cc9)), closes [documentos#194](http://gitlab.meta.com.br/documentos/issues/194)
* **documentos#194:** improved quotation translation ([f5c7550](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f5c7550fb08c68c7fb4132691a66e233431f5483)), closes [documentos#194](http://gitlab.meta.com.br/documentos/issues/194)
* **documentos#273:** added picker into date input ([e4cf2c7](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e4cf2c7048e68037962dbe0948ae241c78582114)), closes [documentos#273](http://gitlab.meta.com.br/documentos/issues/273)
* **documentos#276:** adding last request date ([872a5e5](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/872a5e5a8fecb4272707e1ff00ee0e613a7a625b)), closes [documentos#276](http://gitlab.meta.com.br/documentos/issues/276) [#106](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/106)
* **documentos#277:** wip: fixer rows per page layout ([0cd2048](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0cd20489cb6b8c8b011c9ed55ef48d19ae7f8b06)), closes [documentos#277](http://gitlab.meta.com.br/documentos/issues/277) [#89](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/89)
* **documentos#286:** drawing field on mobile device ([a057a07](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a057a070ddbf50c4f633c65f2c20f68caea519ff)), closes [documentos#286](http://gitlab.meta.com.br/documentos/issues/286)
* **documentos#373:** added new translation ([b74a6e6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b74a6e6ed2ebc018b8c9ceb7bb45eb17991f178d)), closes [documentos#373](http://gitlab.meta.com.br/documentos/issues/373)
* **documentos#388:** added new translations ([17fa7c2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/17fa7c2a2ec672a665238c29ca08d56dd2e72e57)), closes [documentos#388](http://gitlab.meta.com.br/documentos/issues/388)



# [0.6.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.5.0...v0.6.0-alpha.0) (2020-11-10)


### Features

* **documentos#16:** added new translations ([8519a81](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8519a817afa34688dae5bee60ad669046fa2cbec)), closes [documentos#16](http://gitlab.meta.com.br/documentos/issues/16)





# [0.6.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.5.0...v0.6.0-alpha.0) (2020-11-10)

**Note:** Version bump only for package @smartcoop/i18n
## [0.5.2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.5.1...v0.5.2) (2020-11-11)


### Bug Fixes

* **staging:** fixed bugs for release 5 ([f9e8b8d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f9e8b8d6a8e0f9154b2b0ab11bcebda699d322d5))





# [0.5.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.5.0-alpha.0...v0.5.0) (2020-11-10)


### Bug Fixes

* **ci-cd:** release pipeline ([0a010cb](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0a010cb7932d43f913b5fadbee671d89a720fdae))
* **ci-cd:** release pipeline ([a5ab35c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a5ab35c7b6bad813ed7ab46dffdffa29579c69af))
* **ci-cd:** release pipeline ([b6e12cd](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b6e12cdcf895476d121ed4bf86b96ab116b9999d))


### Features

* **documentos#101:** added new translation ([b538797](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b538797b36d3eca1b7894e2631020d62ab431a3d)), closes [documentos#101](http://gitlab.meta.com.br/documentos/issues/101)
* **documentos#101:** created mobile navigation ([61b6e26](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/61b6e26770a454f968a7c314008f9af448c69752)), closes [documentos#101](http://gitlab.meta.com.br/documentos/issues/101)
* **documentos#101:** created module screen for web ([a0d8301](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a0d830137be31ed28c7bad0bc76672012b5af4eb)), closes [documentos#101](http://gitlab.meta.com.br/documentos/issues/101)
* **documentos#253 documentos#254:** fixed translation and add a new one ([e87ac09](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e87ac09d6ea83e8275fb3c2558fa1c4c0bfa6a7b))
* **documentos#93:** created form ([79dc053](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/79dc053e867209bbb6c2f6b345878daef8e2b73d)), closes [documentos#93](http://gitlab.meta.com.br/documentos/issues/93)





# [0.5.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.4.0...v0.5.0-alpha.0) (2020-10-27)

**Note:** Version bump only for package @smartcoop/i18n





# [0.4.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.3.0-alpha.0...v0.4.0) (2020-10-27)



### Bug Fixes

* **documentos#139:** requesting android permission ([0eb6797](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0eb67971f58ee114267e252db1f596392623b992)), closes [documentos#139](http://gitlab.meta.com.br/documentos/issues/139)
* **documentos#63:** fix strings ([b2e6857](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b2e6857714fe2e086189c905f437d7b4f8e1d51e))


### Features

* **documentos#10:** validating password by cpf phone date of birth ([4aefbe0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4aefbe0b0eab8a548aaac32f4b2b008492f1fed8)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#127:** added new translations ([646c97f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/646c97fd5e7b213eaf03dde7a4fa9ddf5aca3019)), closes [documentos#127](http://gitlab.meta.com.br/documentos/issues/127)
* **documentos#127:** added new translations ([bfdc9a2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/bfdc9a235f98cdb670c59ba185552a73faf4738f)), closes [documentos#127](http://gitlab.meta.com.br/documentos/issues/127)
* **documentos#132:** wip: Added fonts to mobile ([a1212ba](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a1212bac57fda5018518af4049d8bac2aff21fbe)), closes [documentos#132](http://gitlab.meta.com.br/documentos/issues/132) [#31](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/31)
* **documentos#164:** created checkbox tree view web component ([467b234](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/467b234bd9cc98ab93b35ed5bf1d3892d27b9f16)), closes [documentos#164](http://gitlab.meta.com.br/documentos/issues/164)
* **documentos#18:** wIP:Created mobile datepicker ([be26632](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/be266326f898865711616fe260d7ecf1a6dafaaa)), closes [documentos#18](http://gitlab.meta.com.br/documentos/issues/18) [#10](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/10)
* **documentos#181:** sorting payment options by quantity of days ([2911c42](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2911c42e0a094723b5f6204b737a7ad234980038)), closes [documentos#181](http://gitlab.meta.com.br/documentos/issues/181)
* **documentos#182:** wip: changed login to render slug ([9c2aa6f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9c2aa6fd61b59b5533d7cce96d8e82ef77c557fe)), closes [documentos#182](http://gitlab.meta.com.br/documentos/issues/182) [#55](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/55)
* **documentos#185:** created basic navigation for web and mobile ([4b144ed](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4b144edcd2c7d7338100f5da5105df4ccec87969)), closes [documentos#185](http://gitlab.meta.com.br/documentos/issues/185)
* **documentos#200:** wIP: Created organizations sessions on details ([fa79cbe](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/fa79cbe8600884bd202e78267e26b534a55d5415)), closes [documentos#200](http://gitlab.meta.com.br/documentos/issues/200) [#61](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/61)
* **documentos#200:** wip: fixed styled component name ([5504115](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/550411589ddec35edb11a306a53a4c083053bfcf)), closes [#61](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/61)
* **documentos#200:** wip: styles ([a9792b3](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a9792b3e69adc01343b48cb68b8cda6487aec897)), closes [documentos#200](http://gitlab.meta.com.br/documentos/issues/200) [#61](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/61)
* **documentos#25:** added new translation ([7602d4f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7602d4fee8f74721ecc6fb360ec77f129212f5a7)), closes [documentos#25](http://gitlab.meta.com.br/documentos/issues/25)
* **documentos#25:** added new translation ([dda2729](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/dda2729cc1bdff2490fa678e8363f2dc3e3b2701)), closes [documentos#25](http://gitlab.meta.com.br/documentos/issues/25)
* **documentos#28:** added new translation. Fix another ([fba5b9d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/fba5b9d8cdf33722569be0c0d19c241cb10556d2))
* **documentos#28:** adds new translation ([500c105](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/500c10532a0ced2daf24842d36c6b130c84e4f21)), closes [documentos#28](http://gitlab.meta.com.br/documentos/issues/28)
* **documentos#49:** added new tranlsations ([3d4fd88](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3d4fd88d50763b0b57f49f2828f691d9f2296cf9)), closes [documentos#49](http://gitlab.meta.com.br/documentos/issues/49)
* **documentos#49:** added new translation ([2383f23](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2383f2332cfd10675ffef14650f0c972ea10b97e)), closes [documentos#49](http://gitlab.meta.com.br/documentos/issues/49)
* **documentos#49:** added new translation ([f2fe847](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f2fe8474b772942fe1aacc6e62d8e5fbfc801362)), closes [documentos#49](http://gitlab.meta.com.br/documentos/issues/49)
* **documentos#49:** added new translation ([ba75bcf](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ba75bcf72dd912c917c2ecdb8454d9f5e615a6f8)), closes [documentos#49](http://gitlab.meta.com.br/documentos/issues/49)
* **documentos#49:** added new translation ([b7196f7](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b7196f7989e9d023886e6ca5c6dc17ed87ec5ba2)), closes [documentos#49](http://gitlab.meta.com.br/documentos/issues/49)
* **documentos#49:** added new translation ([d707ce2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d707ce2a49a7438bb2ef7dd34e66e22c2288e9e8)), closes [documentos#49](http://gitlab.meta.com.br/documentos/issues/49)
* **documentos#49:** added new translations ([02ecee0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/02ecee027c225bdaee51198070a4408b77407c5e)), closes [documentos#49](http://gitlab.meta.com.br/documentos/issues/49)
* **documentos#49:** added new translations ([6693451](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/66934512f30359e7ffeeea36cff04c902dc8ac52)), closes [documentos#49](http://gitlab.meta.com.br/documentos/issues/49)
* **documentos#49:** changed translation ([2eba608](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2eba6085b9161066ba9d04ff3791ea4b8baa373b)), closes [documentos#49](http://gitlab.meta.com.br/documentos/issues/49)
* **documentos#49:** created DataTable async for web ([609c7cb](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/609c7cb05d0df3503703363912d3bde9ede825b4)), closes [documentos#49](http://gitlab.meta.com.br/documentos/issues/49)
* **documentos#49:** merge ([f18397e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f18397e1bdd1e275fb12aeab9ea91f32008ec8e8)), closes [documentos#49](http://gitlab.meta.com.br/documentos/issues/49)
* **documentos#53:** added new translation ([063c865](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/063c86588d386fb8ef0ddac29c19d315993a6082)), closes [documentos#53](http://gitlab.meta.com.br/documentos/issues/53)
* **documentos#53:** created polygon map component ([13306bd](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/13306bd9eabac3c3e46f1810207d6caaa102ea28)), closes [documentos#53](http://gitlab.meta.com.br/documentos/issues/53)
* **documentos#68:** added new translations ([86a03f8](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/86a03f836f67f7fa7e7c183a601c74a7c37bc5b5)), closes [documentos#68](http://gitlab.meta.com.br/documentos/issues/68)
* cherry-pick ([195ec7f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/195ec7f8cd2dd610f569b541e5b7841ea38b389c))
* **documentos#87:** created input to add new area to checkbox group ([236e767](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/236e7672add1c471f4a79649400c67c203ce9cac)), closes [documentos#87](http://gitlab.meta.com.br/documentos/issues/87) [#22](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/22)





# [0.3.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.2.0...v0.3.0-alpha.0) (2020-09-28)

**Note:** Version bump only for package @smartcoop/i18n




# [0.2.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.2.0-alpha.0...v0.2.0) (2020-09-28)


### Bug Fixes

* **documentos#10:** fixed password reset message ([445c7db](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/445c7dbc9fe93bf9e46d71da5d4bbdab6dd6f357))


### Features

* **documentos#10:** craeted loading and error modal ([34bc091](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/34bc091a4766d8b07fda331faf0cbec7c81a7497)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#10:** created web login screen ([34ed647](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/34ed647f1ab16e0c1dc45db8455239335ed6286d)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#10:** created web screens ([496ac64](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/496ac645455e7c7e127a68385e9d8fa8b3b4b9f6)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#10:** finished productor onboarding ([4eb4a89](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4eb4a89ed42ba9a8161999c29bf941a95c5c1119)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#10:** login for mobile ([39b6072](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/39b60720db516f0b8e669d8691d4f61e404144e2)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#25:** added new translations ([bddec6c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/bddec6c451df188cf78e0b7fe168a03907e1a2a7)), closes [documentos#25](http://gitlab.meta.com.br/documentos/issues/25)
* **documentos#25:** added new translations documentos[#67](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/67) ([f49471b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f49471baecec21a89fb5930660e02c9ab56ebf0b)), closes [documentos#25](http://gitlab.meta.com.br/documentos/issues/25)
* **documentos#25:** changed web field form style ([7636b12](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7636b126893c4a69ac658da201f5c1d6b4e07634)), closes [documentos#25](http://gitlab.meta.com.br/documentos/issues/25)
* **documentos#28:** added new translation ([f44d4f0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f44d4f005933ffa88d0e0f41d63247de4742ea6a)), closes [documentos#28](http://gitlab.meta.com.br/documentos/issues/28)
* **documentos#28:** added new translation ([666733e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/666733ee965d2172f07bb455029a43244d4bd0c6)), closes [documentos#28](http://gitlab.meta.com.br/documentos/issues/28)
* **documentos#28:** added new translation ([c200122](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c200122241111d412c399cecc2729f0a2d755d9f)), closes [documentos#28](http://gitlab.meta.com.br/documentos/issues/28)
* **documentos#28:** added new translation ([3d909ae](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3d909ae20b095a194906833bd938f53cb1328828)), closes [documentos#28](http://gitlab.meta.com.br/documentos/issues/28)
* **documentos#28:** added new translations ([4ad1122](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4ad1122b8beef80c54a6c917654387f412b8a3d6)), closes [documentos#28](http://gitlab.meta.com.br/documentos/issues/28)
* **documentos#28:** added new translations ([a8b6051](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a8b6051efc37378ccde075e2fe642feacdfa67fe)), closes [documentos#28](http://gitlab.meta.com.br/documentos/issues/28)
* **documentos#28:** added new translations ([e463bd3](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e463bd3a6e8baeb21af56feb8b271170108279b0)), closes [documentos#28](http://gitlab.meta.com.br/documentos/issues/28)
* **documentos#28:** added new translations ([c726d25](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c726d250486f7c8c77bc5c298a5f571333e4dba6)), closes [documentos#28](http://gitlab.meta.com.br/documentos/issues/28)
* **documentos#28:** added new translations ([dcfb1b0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/dcfb1b0709c2c1b044da585cb9c9bd625275230e)), closes [documentos#28](http://gitlab.meta.com.br/documentos/issues/28)
* **documentos#28:** added new translations ([525aab4](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/525aab4737133e89fd2664ab48e6d01624d1c230)), closes [documentos#28](http://gitlab.meta.com.br/documentos/issues/28)
* **documentos#28:** added new translations ([a063307](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a0633078aa6ae9255a4b43a9eb9f2c4a55192e8b)), closes [documentos#28](http://gitlab.meta.com.br/documentos/issues/28)
* **documentos#28:** created maps for web ([87cc7ec](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/87cc7ec27ec25c531dd748915382294b88f11ba2)), closes [documentos#28](http://gitlab.meta.com.br/documentos/issues/28)
* **documentos#40:** removed Smartcoop text from GuestCard ([cef7dcf](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/cef7dcf30f65da670d1433da7fa4969da75c0066)), closes [documentos#40](http://gitlab.meta.com.br/documentos/issues/40) [#14](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/14)
* **documentos#57:** created modal component to web and mobile ([c3610ea](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c3610eabd1cecab013f46fe3c116d5f7cea00551)), closes [documentos#57](http://gitlab.meta.com.br/documentos/issues/57) [#18](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/18)





# [0.2.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.1.0...v0.2.0-alpha.0) (2020-09-13)

**Note:** Version bump only for package @smartcoop/i18n





# [0.1.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.1.0-alpha.0...v0.1.0) (2020-09-13)


### Bug Fixes

* **documentos#10:** fixed select \o/ documentos[#30](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/30) ([56ab086](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/56ab08628ed8b0859112cc76f6b2da989098d5a0))
* **documentos#10:** fixed select \o/ documentos[#30](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/30) ([c043c42](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c043c4211b46564f7bf0a50b323a8043d1dfbccc))
* **forms:** fix validations ([7c413a5](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7c413a5c916867af845ec3726b4c3d2deae0ddec))
* **forms:** validating cpf and cnpj ([a16da5b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a16da5b080973e282b84400f5be9ce1764db812a))


### Features

* **#10:** created web InputCpfCnpj with mask and validations ([a28957a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a28957ae1578d15e1d20395b6d3df7124638cdc1)), closes [#10](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/10) [#17](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/17)
* **documentos#10:** added invalid number message into dictionary ([2343511](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2343511b99e5dda6ffa5945c6e11b6d71c8848d0)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10) [#4](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/4)
* **documentos#10:** added new translation ([692018f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/692018f8d9c00707d6aaab91a97e658c3aa541e8)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#10:** added new translation documentos[#51](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/51) ([d13dd8b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d13dd8bb11665d819d5d7a35becdf8dbf44ae2b7)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#10:** added new translations ([d9d5a2c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d9d5a2c1bd5ea8f0c410cd3a5ab8200f7ba7a825)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#10:** added new translations ([761f156](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/761f1562b35a06cd8e7b1a455270c4e1292b8218)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#10:** added new translations documentos[#51](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/51) ([782c318](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/782c3181f7d731d8f76efebb838309b925b2f31f)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#10:** added new translations documentos[#51](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/51) ([1160b90](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1160b9064214f57c4580fd24799549c49c253344)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#10:** added new translations documentos[#51](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/51) ([92ecb9d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/92ecb9d8915a14b455e56f314cd7f24b6a566a3a)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#10:** created cooperatives screen ([64bbac6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/64bbac6146a39901b28e2c406f50675757eab107)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#10:** created identification screen ([02919dc](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/02919dcd19bd38e94b40047c9766523e53cd7d86)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#10:** created identification screen ([5342ee6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/5342ee65eb7129e62b3de0367b3ef086c8e05a4f)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#10:** created InputCep for mobile and web ([fb3753e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/fb3753e38e6a918c2a11d8b10979bc58f684b612)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10) [#6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/6)
* **documentos#10:** created inputDate web and mobile documentos[#18](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/18) ([fd30c7d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/fd30c7d25403c4fbbef7aeb6ed1d1f6f9ba226f5)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#10:** created reset code screen ([50c1030](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/50c10306ebc726aa901d804c675b2c822fda1b85)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#10:** finished onboard mocked ([b20abfa](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b20abfaf5a51d791934136a03c7c4714e1cda6b3)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#10:** login and signup screens ([e4b9d02](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e4b9d0278f4785aa31928f9ec3bc6e5de4ff98e2)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#10:** login and signup screens ([4558694](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4558694308db08a04873004ca9e0db6dce6bbba7)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **forms:** single validate for web and mobile forms ([9bbeb71](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9bbeb71f1a9c0acac8d6e817e614159eda808a5d))





# 0.1.0-alpha.0 (2020-08-21)

**Note:** Version bump only for package @smartcoop/i18n
