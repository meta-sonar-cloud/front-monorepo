import bytes from 'bytes'

import { forEach } from 'lodash'

export const isValidSize = (file, maxSize) => (file.size || file.fileSize) < maxSize

export const convertFileSize = size => bytes(size)

export const createFormData = (files, fileKey = 'upload', body = {}) => {
  const data = new FormData()

  forEach(files, (file) => {
    data.append(fileKey, file)
  })

  Object.keys(body).forEach((key) => {
    data.append(key, body[key])
  })

  return data
}
