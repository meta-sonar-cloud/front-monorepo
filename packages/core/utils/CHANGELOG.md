# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [0.18.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.17.0...v0.18.0) (2021-04-19)


### Features

* resolve some issues ([0e7d7ce](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0e7d7ce1738f55d75ddd3907cb5ca197b5885e1a))





# [0.17.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.16.0...v0.17.0) (2021-04-15)


### Bug Fixes

* **#1686:** adjustment filter status register animal ([b4f2a7a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b4f2a7aef3c9a0a17701a1bfaa5b86e0d75da2f4)), closes [#1686](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1686) [#548](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/548)
* **#1701:** adjustment filter vaca in register animal ([7b723f4](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7b723f48a10d605d10e745d83c79d7f8ed02bcb8)), closes [#1701](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1701) [#556](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/556)


### Features

* resolve [#1694](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1694) ([1718b0d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1718b0d3f02d02786637da9aa00062cc41652144))
* resolve [#1714](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1714) ([2ad674a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2ad674a12ad0c343aa6a18d39fb71a140b38e86a))
* resolve [#1735](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1735) ([cda6387](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/cda6387c44735527fe95bf60838cc9098715d96e))





# [0.16.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.15.0...v0.16.0) (2021-04-13)


### Bug Fixes

* **#1484:** remove console log and change battery name ([8dfe9ea](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8dfe9ea3ffc23dfb83b47ccb1165aadb92a7030b)), closes [#1484](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1484)
* **#1686:** add filter status animal in register animal ([18c5404](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/18c5404c5e9685fd54103864161317151bc70a52)), closes [#1686](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1686) [#548](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/548)
* **auth:** fixed first refresh token and added free actions ([fc2eea9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/fc2eea987612d3f4d4c783bb59cb3df82cc6a4d1))
* **documentos#1582:** fix tambo and format number ([6fac8d2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/6fac8d2c04a12ddd073a6df2d55859d56e11557a))


### Features

* **#1484:** create screens stations and administration module mobile ([becfd36](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/becfd36d9d7c1dceda69af09426beba688a84127)), closes [#1484](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1484) [#484](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/484)
* **#1484:** integration with backend weather stations details ([c776b4e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c776b4eb5720ad698dc40ce90bc003c42b18027a)), closes [#1484](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1484) [#484](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/484)
* **#1649:** create constants file for save animal type ([4a9298a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4a9298a75553d8bef5594629c9032dc14d0620df)), closes [#1649](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1649) [#533](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/533)







**Note:** Version bump only for package @smartcoop/utils





# [0.15.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.12.0-alpha.1...v0.15.0) (2021-03-23)


### Bug Fixes

* **#1185:** adjustment validation file.size ([e82b234](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e82b234b90d7cc7541ba68096920a26a89617b69)), closes [#1185](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1185)
* **#1396:** fix convert string in charts.js ([a73564b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a73564b0a8dc0939d4204f0b06220575742e6cb6)), closes [#446](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/446)
* **documentos#1510:** creates formatNumberNative to resolve location issues ([066eb63](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/066eb63c7ef366148cb49cd426a0d4d9b1e9e299)), closes [documentos#1510](http://gitlab.meta.com.br/documentos/issues/1510)


### Features

* **#1182:** adjustment text pest report location and pin ([21c7da1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/21c7da14eb242f378890d6947cabbda961edff37)), closes [#1182](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1182) [#430](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/430)
* **#1185:** using camera for add new images in pest report ([4437125](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/44371252a7ad104654d5373fcef2f572814cf4e7)), closes [#1185](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1185)
* **#1280:** adjustment scale name in graph milk quality ([5edf8e9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/5edf8e900fb8b1c340caa521f932dd5ed5d09937)), closes [#1280](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1280)
* **#1301:** adjustment graphs web ([c567013](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c567013b84c3ab9443e25ba45bdddfbfc94f5ecc)), closes [#1301](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1301) [#399](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/399)
* **#1301:** adjustment label graph and create scale price ([a2230a5](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a2230a5da8051bbf0919690f9b0bb56f9100ba31)), closes [#1301](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1301) [#399](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/399)
* **#1386:** adjustment graph milk quality ([9f25b09](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9f25b09a490c558c1e56eff23a86b132237e63ec)), closes [#1386](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1386) [#439](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/439)
* **documentos#1116:** buttons to redirect to each screen accordingly ([8783bb6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8783bb659e0e17bc2ed1a5f27016dab7769d92ce)), closes [documentos#1116](http://gitlab.meta.com.br/documentos/issues/1116)
* **documentos#1116:** tambo for mobile ([f4d650f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f4d650f18602702f4cfccdf32758336da2041ca0)), closes [documentos#1116](http://gitlab.meta.com.br/documentos/issues/1116)
* **documentos#1116:** tambo for web ([bb95716](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/bb95716083aa68b842490db35a2a34f292726f62)), closes [documentos#1116](http://gitlab.meta.com.br/documentos/issues/1116)
* **documentos#1287:** added min and max into timeline chart ([4397278](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4397278c144645bfccd6d7bbd6045816e3e0e4d0)), closes [documentos#1287](http://gitlab.meta.com.br/documentos/issues/1287)
* **documentos#1292:** created function to get the dates between dates ([3b18c8a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3b18c8a2b6d4e51723e044922a99231ed94fbf8a)), closes [documentos#1292](http://gitlab.meta.com.br/documentos/issues/1292)
* **documentos#1323:** converts string to numbers ([62bc6d4](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/62bc6d4553750488e6c9efd17e4073c9801a5746)), closes [documentos#1323](http://gitlab.meta.com.br/documentos/issues/1323)
* **documentos#1323:** converts string toNumber ([14e12fd](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/14e12fdd17307a0339bbe77bb9f4940f295def01)), closes [documentos#1323](http://gitlab.meta.com.br/documentos/issues/1323)
* **documentos#1323:** new icon + new login image background ([54cf57d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/54cf57d3c61ec328aba99391ea836eba09fce9dd)), closes [documentos#1323](http://gitlab.meta.com.br/documentos/issues/1323)
* **documentos#1357:** changes in field monitoration ([2508a09](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2508a09a73f04732609b124c0c08aeaa59bd7668)), closes [documentos#1357](http://gitlab.meta.com.br/documentos/issues/1357)
* **documentos#1423:** adds warning snackbar if mngmt isn conc. ([13f4553](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/13f455348b48a86f2648c3d876244e7bca22cecb)), closes [documentos#1423](http://gitlab.meta.com.br/documentos/issues/1423)
* created new formatter ([0a2ed8e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0a2ed8ed3a7f99c4bc1783f35479c96b470924d1))





# [0.15.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.12.0-alpha.1...v0.15.0) (2021-03-23)


### Bug Fixes

* **#1185:** adjustment validation file.size ([e82b234](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e82b234b90d7cc7541ba68096920a26a89617b69)), closes [#1185](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1185)
* **#1396:** fix convert string in charts.js ([a73564b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a73564b0a8dc0939d4204f0b06220575742e6cb6)), closes [#446](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/446)
* **documentos#1510:** creates formatNumberNative to resolve location issues ([066eb63](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/066eb63c7ef366148cb49cd426a0d4d9b1e9e299)), closes [documentos#1510](http://gitlab.meta.com.br/documentos/issues/1510)


### Features

* **#1182:** adjustment text pest report location and pin ([21c7da1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/21c7da14eb242f378890d6947cabbda961edff37)), closes [#1182](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1182) [#430](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/430)
* **#1185:** using camera for add new images in pest report ([4437125](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/44371252a7ad104654d5373fcef2f572814cf4e7)), closes [#1185](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1185)
* **#1280:** adjustment scale name in graph milk quality ([5edf8e9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/5edf8e900fb8b1c340caa521f932dd5ed5d09937)), closes [#1280](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1280)
* **#1301:** adjustment graphs web ([c567013](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c567013b84c3ab9443e25ba45bdddfbfc94f5ecc)), closes [#1301](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1301) [#399](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/399)
* **#1301:** adjustment label graph and create scale price ([a2230a5](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a2230a5da8051bbf0919690f9b0bb56f9100ba31)), closes [#1301](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1301) [#399](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/399)
* **#1386:** adjustment graph milk quality ([9f25b09](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9f25b09a490c558c1e56eff23a86b132237e63ec)), closes [#1386](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1386) [#439](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/439)
* **documentos#1116:** buttons to redirect to each screen accordingly ([8783bb6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8783bb659e0e17bc2ed1a5f27016dab7769d92ce)), closes [documentos#1116](http://gitlab.meta.com.br/documentos/issues/1116)
* **documentos#1116:** tambo for mobile ([f4d650f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f4d650f18602702f4cfccdf32758336da2041ca0)), closes [documentos#1116](http://gitlab.meta.com.br/documentos/issues/1116)
* **documentos#1116:** tambo for web ([bb95716](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/bb95716083aa68b842490db35a2a34f292726f62)), closes [documentos#1116](http://gitlab.meta.com.br/documentos/issues/1116)
* **documentos#1287:** added min and max into timeline chart ([4397278](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4397278c144645bfccd6d7bbd6045816e3e0e4d0)), closes [documentos#1287](http://gitlab.meta.com.br/documentos/issues/1287)
* **documentos#1292:** created function to get the dates between dates ([3b18c8a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3b18c8a2b6d4e51723e044922a99231ed94fbf8a)), closes [documentos#1292](http://gitlab.meta.com.br/documentos/issues/1292)
* **documentos#1323:** converts string to numbers ([62bc6d4](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/62bc6d4553750488e6c9efd17e4073c9801a5746)), closes [documentos#1323](http://gitlab.meta.com.br/documentos/issues/1323)
* **documentos#1323:** converts string toNumber ([14e12fd](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/14e12fdd17307a0339bbe77bb9f4940f295def01)), closes [documentos#1323](http://gitlab.meta.com.br/documentos/issues/1323)
* **documentos#1323:** new icon + new login image background ([54cf57d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/54cf57d3c61ec328aba99391ea836eba09fce9dd)), closes [documentos#1323](http://gitlab.meta.com.br/documentos/issues/1323)
* **documentos#1357:** changes in field monitoration ([2508a09](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2508a09a73f04732609b124c0c08aeaa59bd7668)), closes [documentos#1357](http://gitlab.meta.com.br/documentos/issues/1357)
* **documentos#1423:** adds warning snackbar if mngmt isn conc. ([13f4553](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/13f455348b48a86f2648c3d876244e7bca22cecb)), closes [documentos#1423](http://gitlab.meta.com.br/documentos/issues/1423)
* created new formatter ([0a2ed8e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0a2ed8ed3a7f99c4bc1783f35479c96b470924d1))





# [0.15.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.12.0-alpha.1...v0.15.0) (2021-03-23)


### Bug Fixes

* **#1185:** adjustment validation file.size ([e82b234](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e82b234b90d7cc7541ba68096920a26a89617b69)), closes [#1185](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1185)
* **#1396:** fix convert string in charts.js ([a73564b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a73564b0a8dc0939d4204f0b06220575742e6cb6)), closes [#446](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/446)
* **documentos#1510:** creates formatNumberNative to resolve location issues ([066eb63](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/066eb63c7ef366148cb49cd426a0d4d9b1e9e299)), closes [documentos#1510](http://gitlab.meta.com.br/documentos/issues/1510)


### Features

* **#1182:** adjustment text pest report location and pin ([21c7da1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/21c7da14eb242f378890d6947cabbda961edff37)), closes [#1182](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1182) [#430](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/430)
* **#1185:** using camera for add new images in pest report ([4437125](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/44371252a7ad104654d5373fcef2f572814cf4e7)), closes [#1185](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1185)
* **#1280:** adjustment scale name in graph milk quality ([5edf8e9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/5edf8e900fb8b1c340caa521f932dd5ed5d09937)), closes [#1280](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1280)
* **#1301:** adjustment graphs web ([c567013](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c567013b84c3ab9443e25ba45bdddfbfc94f5ecc)), closes [#1301](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1301) [#399](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/399)
* **#1301:** adjustment label graph and create scale price ([a2230a5](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a2230a5da8051bbf0919690f9b0bb56f9100ba31)), closes [#1301](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1301) [#399](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/399)
* **#1386:** adjustment graph milk quality ([9f25b09](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9f25b09a490c558c1e56eff23a86b132237e63ec)), closes [#1386](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1386) [#439](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/439)
* **documentos#1116:** buttons to redirect to each screen accordingly ([8783bb6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8783bb659e0e17bc2ed1a5f27016dab7769d92ce)), closes [documentos#1116](http://gitlab.meta.com.br/documentos/issues/1116)
* **documentos#1116:** tambo for mobile ([f4d650f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f4d650f18602702f4cfccdf32758336da2041ca0)), closes [documentos#1116](http://gitlab.meta.com.br/documentos/issues/1116)
* **documentos#1116:** tambo for web ([bb95716](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/bb95716083aa68b842490db35a2a34f292726f62)), closes [documentos#1116](http://gitlab.meta.com.br/documentos/issues/1116)
* **documentos#1287:** added min and max into timeline chart ([4397278](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4397278c144645bfccd6d7bbd6045816e3e0e4d0)), closes [documentos#1287](http://gitlab.meta.com.br/documentos/issues/1287)
* **documentos#1292:** created function to get the dates between dates ([3b18c8a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3b18c8a2b6d4e51723e044922a99231ed94fbf8a)), closes [documentos#1292](http://gitlab.meta.com.br/documentos/issues/1292)
* **documentos#1323:** converts string to numbers ([62bc6d4](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/62bc6d4553750488e6c9efd17e4073c9801a5746)), closes [documentos#1323](http://gitlab.meta.com.br/documentos/issues/1323)
* **documentos#1323:** converts string toNumber ([14e12fd](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/14e12fdd17307a0339bbe77bb9f4940f295def01)), closes [documentos#1323](http://gitlab.meta.com.br/documentos/issues/1323)
* **documentos#1323:** new icon + new login image background ([54cf57d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/54cf57d3c61ec328aba99391ea836eba09fce9dd)), closes [documentos#1323](http://gitlab.meta.com.br/documentos/issues/1323)
* **documentos#1357:** changes in field monitoration ([2508a09](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2508a09a73f04732609b124c0c08aeaa59bd7668)), closes [documentos#1357](http://gitlab.meta.com.br/documentos/issues/1357)
* **documentos#1423:** adds warning snackbar if mngmt isn conc. ([13f4553](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/13f455348b48a86f2648c3d876244e7bca22cecb)), closes [documentos#1423](http://gitlab.meta.com.br/documentos/issues/1423)
* created new formatter ([0a2ed8e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0a2ed8ed3a7f99c4bc1783f35479c96b470924d1))





# [0.14.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.12.0-alpha.1...v0.14.0) (2021-03-23)


### Bug Fixes

* **#1185:** adjustment validation file.size ([e82b234](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e82b234b90d7cc7541ba68096920a26a89617b69)), closes [#1185](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1185)
* **#1396:** fix convert string in charts.js ([a73564b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a73564b0a8dc0939d4204f0b06220575742e6cb6)), closes [#446](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/446)
* **documentos#1510:** creates formatNumberNative to resolve location issues ([066eb63](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/066eb63c7ef366148cb49cd426a0d4d9b1e9e299)), closes [documentos#1510](http://gitlab.meta.com.br/documentos/issues/1510)


### Features

* **#1182:** adjustment text pest report location and pin ([21c7da1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/21c7da14eb242f378890d6947cabbda961edff37)), closes [#1182](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1182) [#430](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/430)
* **#1185:** using camera for add new images in pest report ([4437125](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/44371252a7ad104654d5373fcef2f572814cf4e7)), closes [#1185](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1185)
* **#1280:** adjustment scale name in graph milk quality ([5edf8e9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/5edf8e900fb8b1c340caa521f932dd5ed5d09937)), closes [#1280](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1280)
* **#1301:** adjustment graphs web ([c567013](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c567013b84c3ab9443e25ba45bdddfbfc94f5ecc)), closes [#1301](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1301) [#399](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/399)
* **#1301:** adjustment label graph and create scale price ([a2230a5](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a2230a5da8051bbf0919690f9b0bb56f9100ba31)), closes [#1301](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1301) [#399](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/399)
* **#1386:** adjustment graph milk quality ([9f25b09](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9f25b09a490c558c1e56eff23a86b132237e63ec)), closes [#1386](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1386) [#439](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/439)
* **documentos#1116:** buttons to redirect to each screen accordingly ([8783bb6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8783bb659e0e17bc2ed1a5f27016dab7769d92ce)), closes [documentos#1116](http://gitlab.meta.com.br/documentos/issues/1116)
* **documentos#1116:** tambo for mobile ([f4d650f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f4d650f18602702f4cfccdf32758336da2041ca0)), closes [documentos#1116](http://gitlab.meta.com.br/documentos/issues/1116)
* **documentos#1116:** tambo for web ([bb95716](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/bb95716083aa68b842490db35a2a34f292726f62)), closes [documentos#1116](http://gitlab.meta.com.br/documentos/issues/1116)
* **documentos#1287:** added min and max into timeline chart ([4397278](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4397278c144645bfccd6d7bbd6045816e3e0e4d0)), closes [documentos#1287](http://gitlab.meta.com.br/documentos/issues/1287)
* **documentos#1292:** created function to get the dates between dates ([3b18c8a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3b18c8a2b6d4e51723e044922a99231ed94fbf8a)), closes [documentos#1292](http://gitlab.meta.com.br/documentos/issues/1292)
* **documentos#1323:** converts string to numbers ([62bc6d4](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/62bc6d4553750488e6c9efd17e4073c9801a5746)), closes [documentos#1323](http://gitlab.meta.com.br/documentos/issues/1323)
* **documentos#1323:** converts string toNumber ([14e12fd](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/14e12fdd17307a0339bbe77bb9f4940f295def01)), closes [documentos#1323](http://gitlab.meta.com.br/documentos/issues/1323)
* **documentos#1323:** new icon + new login image background ([54cf57d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/54cf57d3c61ec328aba99391ea836eba09fce9dd)), closes [documentos#1323](http://gitlab.meta.com.br/documentos/issues/1323)
* **documentos#1357:** changes in field monitoration ([2508a09](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2508a09a73f04732609b124c0c08aeaa59bd7668)), closes [documentos#1357](http://gitlab.meta.com.br/documentos/issues/1357)
* **documentos#1423:** adds warning snackbar if mngmt isn conc. ([13f4553](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/13f455348b48a86f2648c3d876244e7bca22cecb)), closes [documentos#1423](http://gitlab.meta.com.br/documentos/issues/1423)
* created new formatter ([0a2ed8e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0a2ed8ed3a7f99c4bc1783f35479c96b470924d1))





# [0.13.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.12.0-alpha.1...v0.13.0) (2021-03-23)


### Bug Fixes

* **#1185:** adjustment validation file.size ([e82b234](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e82b234b90d7cc7541ba68096920a26a89617b69)), closes [#1185](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1185)
* **#1396:** fix convert string in charts.js ([a73564b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a73564b0a8dc0939d4204f0b06220575742e6cb6)), closes [#446](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/446)
* **documentos#1510:** creates formatNumberNative to resolve location issues ([066eb63](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/066eb63c7ef366148cb49cd426a0d4d9b1e9e299)), closes [documentos#1510](http://gitlab.meta.com.br/documentos/issues/1510)


### Features

* **#1182:** adjustment text pest report location and pin ([21c7da1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/21c7da14eb242f378890d6947cabbda961edff37)), closes [#1182](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1182) [#430](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/430)
* **#1185:** using camera for add new images in pest report ([4437125](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/44371252a7ad104654d5373fcef2f572814cf4e7)), closes [#1185](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1185)
* **#1280:** adjustment scale name in graph milk quality ([5edf8e9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/5edf8e900fb8b1c340caa521f932dd5ed5d09937)), closes [#1280](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1280)
* **#1301:** adjustment graphs web ([c567013](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c567013b84c3ab9443e25ba45bdddfbfc94f5ecc)), closes [#1301](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1301) [#399](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/399)
* **#1301:** adjustment label graph and create scale price ([a2230a5](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a2230a5da8051bbf0919690f9b0bb56f9100ba31)), closes [#1301](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1301) [#399](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/399)
* **#1386:** adjustment graph milk quality ([9f25b09](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9f25b09a490c558c1e56eff23a86b132237e63ec)), closes [#1386](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1386) [#439](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/439)
* **documentos#1116:** buttons to redirect to each screen accordingly ([8783bb6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8783bb659e0e17bc2ed1a5f27016dab7769d92ce)), closes [documentos#1116](http://gitlab.meta.com.br/documentos/issues/1116)
* **documentos#1116:** tambo for mobile ([f4d650f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f4d650f18602702f4cfccdf32758336da2041ca0)), closes [documentos#1116](http://gitlab.meta.com.br/documentos/issues/1116)
* **documentos#1116:** tambo for web ([bb95716](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/bb95716083aa68b842490db35a2a34f292726f62)), closes [documentos#1116](http://gitlab.meta.com.br/documentos/issues/1116)
* **documentos#1287:** added min and max into timeline chart ([4397278](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4397278c144645bfccd6d7bbd6045816e3e0e4d0)), closes [documentos#1287](http://gitlab.meta.com.br/documentos/issues/1287)
* **documentos#1292:** created function to get the dates between dates ([3b18c8a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3b18c8a2b6d4e51723e044922a99231ed94fbf8a)), closes [documentos#1292](http://gitlab.meta.com.br/documentos/issues/1292)
* **documentos#1323:** converts string to numbers ([62bc6d4](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/62bc6d4553750488e6c9efd17e4073c9801a5746)), closes [documentos#1323](http://gitlab.meta.com.br/documentos/issues/1323)
* **documentos#1323:** converts string toNumber ([14e12fd](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/14e12fdd17307a0339bbe77bb9f4940f295def01)), closes [documentos#1323](http://gitlab.meta.com.br/documentos/issues/1323)
* **documentos#1323:** new icon + new login image background ([54cf57d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/54cf57d3c61ec328aba99391ea836eba09fce9dd)), closes [documentos#1323](http://gitlab.meta.com.br/documentos/issues/1323)
* **documentos#1357:** changes in field monitoration ([2508a09](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2508a09a73f04732609b124c0c08aeaa59bd7668)), closes [documentos#1357](http://gitlab.meta.com.br/documentos/issues/1357)
* **documentos#1423:** adds warning snackbar if mngmt isn conc. ([13f4553](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/13f455348b48a86f2648c3d876244e7bca22cecb)), closes [documentos#1423](http://gitlab.meta.com.br/documentos/issues/1423)
* created new formatter ([0a2ed8e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0a2ed8ed3a7f99c4bc1783f35479c96b470924d1))





# [0.12.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.12.0-alpha.1...v0.12.0) (2021-03-23)


### Bug Fixes

* **#1185:** adjustment validation file.size ([e82b234](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e82b234b90d7cc7541ba68096920a26a89617b69)), closes [#1185](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1185)
* **#1396:** fix convert string in charts.js ([a73564b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a73564b0a8dc0939d4204f0b06220575742e6cb6)), closes [#446](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/446)
* **documentos#1510:** creates formatNumberNative to resolve location issues ([066eb63](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/066eb63c7ef366148cb49cd426a0d4d9b1e9e299)), closes [documentos#1510](http://gitlab.meta.com.br/documentos/issues/1510)


### Features

* **#1182:** adjustment text pest report location and pin ([21c7da1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/21c7da14eb242f378890d6947cabbda961edff37)), closes [#1182](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1182) [#430](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/430)
* **#1185:** using camera for add new images in pest report ([4437125](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/44371252a7ad104654d5373fcef2f572814cf4e7)), closes [#1185](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1185)
* **#1280:** adjustment scale name in graph milk quality ([5edf8e9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/5edf8e900fb8b1c340caa521f932dd5ed5d09937)), closes [#1280](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1280)
* **#1301:** adjustment graphs web ([c567013](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c567013b84c3ab9443e25ba45bdddfbfc94f5ecc)), closes [#1301](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1301) [#399](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/399)
* **#1301:** adjustment label graph and create scale price ([a2230a5](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a2230a5da8051bbf0919690f9b0bb56f9100ba31)), closes [#1301](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1301) [#399](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/399)
* **#1386:** adjustment graph milk quality ([9f25b09](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9f25b09a490c558c1e56eff23a86b132237e63ec)), closes [#1386](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1386) [#439](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/439)
* **documentos#1116:** buttons to redirect to each screen accordingly ([8783bb6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8783bb659e0e17bc2ed1a5f27016dab7769d92ce)), closes [documentos#1116](http://gitlab.meta.com.br/documentos/issues/1116)
* **documentos#1116:** tambo for mobile ([f4d650f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f4d650f18602702f4cfccdf32758336da2041ca0)), closes [documentos#1116](http://gitlab.meta.com.br/documentos/issues/1116)
* **documentos#1116:** tambo for web ([bb95716](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/bb95716083aa68b842490db35a2a34f292726f62)), closes [documentos#1116](http://gitlab.meta.com.br/documentos/issues/1116)
* **documentos#1287:** added min and max into timeline chart ([4397278](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4397278c144645bfccd6d7bbd6045816e3e0e4d0)), closes [documentos#1287](http://gitlab.meta.com.br/documentos/issues/1287)
* **documentos#1292:** created function to get the dates between dates ([3b18c8a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3b18c8a2b6d4e51723e044922a99231ed94fbf8a)), closes [documentos#1292](http://gitlab.meta.com.br/documentos/issues/1292)
* **documentos#1323:** converts string to numbers ([62bc6d4](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/62bc6d4553750488e6c9efd17e4073c9801a5746)), closes [documentos#1323](http://gitlab.meta.com.br/documentos/issues/1323)
* **documentos#1323:** converts string toNumber ([14e12fd](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/14e12fdd17307a0339bbe77bb9f4940f295def01)), closes [documentos#1323](http://gitlab.meta.com.br/documentos/issues/1323)
* **documentos#1323:** new icon + new login image background ([54cf57d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/54cf57d3c61ec328aba99391ea836eba09fce9dd)), closes [documentos#1323](http://gitlab.meta.com.br/documentos/issues/1323)
* **documentos#1357:** changes in field monitoration ([2508a09](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2508a09a73f04732609b124c0c08aeaa59bd7668)), closes [documentos#1357](http://gitlab.meta.com.br/documentos/issues/1357)
* **documentos#1423:** adds warning snackbar if mngmt isn conc. ([13f4553](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/13f455348b48a86f2648c3d876244e7bca22cecb)), closes [documentos#1423](http://gitlab.meta.com.br/documentos/issues/1423)
* created new formatter ([0a2ed8e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0a2ed8ed3a7f99c4bc1783f35479c96b470924d1))





# [0.12.0-alpha.1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.12.0-alpha.0...v0.12.0-alpha.1) (2021-02-22)


### Features

* **1220:** ajuste paleta de cores ([1af336d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1af336d4b9c5def20ca58cc26ac6a43ad218f45a))





# [0.12.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.11.0...v0.12.0-alpha.0) (2021-02-12)

**Note:** Version bump only for package @smartcoop/utils





# [0.11.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.11.0-alpha.0...v0.11.0) (2021-02-12)


### Bug Fixes

* **documentos#535:** transform pie to donut ([d9bdd64](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d9bdd6480e2f2269e6829a1ea12233114653f743)), closes [documentos#535](http://gitlab.meta.com.br/documentos/issues/535)





# [0.11.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.10.0...v0.11.0-alpha.0) (2021-02-05)

**Note:** Version bump only for package @smartcoop/utils





# [0.10.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.10.0-alpha.0...v0.10.0) (2021-02-01)


### Bug Fixes

* **documentos#1116:** catching error on formatNumber ([2d7baed](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2d7baed8129b630b7b2f1733925b504c72638d95)), closes [documentos#1116](http://gitlab.meta.com.br/documentos/issues/1116)
* **onboarding:** fixed property location ([dd942a1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/dd942a138a6f2fc50f748097e9038e631605d8fa))


### Features

* **documentos#102:** new utils function ([0fbcd29](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0fbcd29922b37596467696ba0cd271647909edf5)), closes [documentos#102](http://gitlab.meta.com.br/documentos/issues/102)
* resolve [#990](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/990) ([0a9226e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0a9226e45797c9e3c129b2919235c48ea23ebd51))





# [0.10.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.9.0...v0.10.0-alpha.0) (2021-01-19)

**Note:** Version bump only for package @smartcoop/utils





# [0.9.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.8.2...v0.9.0) (2021-01-19)


### Bug Fixes

* fixed number formatter ([835909c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/835909cafac9892825df275109662e5a83d8c2c7))


### Features

* added number formatter ([b2222fe](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b2222fe5f0ef01c55b6315e8fc1c28ed2ad46f28))
* merge Develop ([4a43e2a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4a43e2a79e976c5459355a3dd2e0892e8c081e24))
* **documentos#836:** dynamic icon ([585a128](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/585a128949f0dc2866f446963da30d5b3bc8ac9a)), closes [documentos#836](http://gitlab.meta.com.br/documentos/issues/836)
* resolve [#894](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/894) ([4fa8b43](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4fa8b43fd81e54697bfe7d3d701ef434fe0798a7))





## [0.8.1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.9.0-alpha.0...v0.8.1) (2021-01-06)

**Note:** Version bump only for package @smartcoop/utils





# [0.9.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.8.0...v0.9.0-alpha.0) (2021-01-05)

**Note:** Version bump only for package @smartcoop/utils





# [0.8.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.8.0-alpha.0...v0.8.0) (2021-01-05)


### Bug Fixes

* **review:** changed details for review - sprint 8 ([5a5efd1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/5a5efd10c60837597c5e61928ea80db1bce2600b))


### Features

* **321:** previsao do tempo mobile ([0db0a4b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0db0a4b8761cd1deb3c498313fcd0de1e3409fd7))
* **445:** adicionada paleta de cores ([7e25bc2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7e25bc2d7327aeee86d534ca51ecc820b7dc6bca))
* **documentos#688:** added chart to web ([84a8fcb](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/84a8fcb47c4d7a12a798460379ecc025c71806fb)), closes [documentos#688](http://gitlab.meta.com.br/documentos/issues/688)
* **documentos#688:** adjust field timeline chart ([003022c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/003022c800c0163a13a8abd396551e88f9691481)), closes [documentos#688](http://gitlab.meta.com.br/documentos/issues/688)
* **documentos#79:** added rainMm into field timeline chart ([d258c00](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d258c0043f4b363a13b01870f70964a517b8bdbb)), closes [documentos#79](http://gitlab.meta.com.br/documentos/issues/79)
* **documentos#79:** finished charts ([2b089cd](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2b089cdf380708c35b8fe4a47f65fd8e20b7523f)), closes [documentos#79](http://gitlab.meta.com.br/documentos/issues/79)
* **documentos#79:** merged with develop ([7538e6f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7538e6fd5b6870ade8c87eb3c9f62e1295990fb3)), closes [documentos#79](http://gitlab.meta.com.br/documentos/issues/79)





# [0.8.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.7.0...v0.8.0-alpha.0) (2020-12-11)

**Note:** Version bump only for package @smartcoop/utils





# [0.7.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.7.0-alpha.0...v0.7.0) (2020-12-11)


### Features

* **documentos#103:** created get center coordinates function ([13115d1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/13115d1c32710d8ff4277315f394bac9f4bbae1f)), closes [documentos#103](http://gitlab.meta.com.br/documentos/issues/103)
* **documentos#360:** created delivery locations ([295d06d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/295d06d08a8bd875612743c0a181eff07350ef85)), closes [documentos#360](http://gitlab.meta.com.br/documentos/issues/360)
* **documentos#451:** created feed on mobile ([90668fe](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/90668fe9e1f1a0924d9e36729b427167cff8ba42)), closes [documentos#451](http://gitlab.meta.com.br/documentos/issues/451)





# [0.7.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.6.0...v0.7.0-alpha.0) (2020-11-27)

**Note:** Version bump only for package @smartcoop/utils





# [0.6.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.5.3...v0.6.0) (2020-11-27)


### Features

* **documentos#107:** adds currency formatter ([f4ae679](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f4ae679baf5eb8bb7f4a7b73281f3bdb3077e67d)), closes [documentos#107](http://gitlab.meta.com.br/documentos/issues/107)
* **documentos#286:** drawing field on mobile device ([a057a07](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a057a070ddbf50c4f633c65f2c20f68caea519ff)), closes [documentos#286](http://gitlab.meta.com.br/documentos/issues/286)
* **documentos#311:** created month year input for web ([18b0de2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/18b0de23fdb837bf79e06f0cc3d7e1ec24e0b7b6)), closes [documentos#311](http://gitlab.meta.com.br/documentos/issues/311)
* **documentos#370:** craeted polygon to svg for mobile ([20de6d0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/20de6d0b22399c622b9a5ff06d4b3837344e8585)), closes [documentos#370](http://gitlab.meta.com.br/documentos/issues/370)



# [0.6.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.5.0...v0.6.0-alpha.0) (2020-11-10)





# [0.6.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.5.0...v0.6.0-alpha.0) (2020-11-10)

**Note:** Version bump only for package @smartcoop/utils





# [0.5.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.5.0-alpha.0...v0.5.0) (2020-11-10)


### Features

* **documentos#93:** listing delivery locations by current organization ([a2aa3e2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a2aa3e2e8612159d9218e609af4ae2357ee675b5)), closes [documentos#93](http://gitlab.meta.com.br/documentos/issues/93)





# [0.5.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.4.0...v0.5.0-alpha.0) (2020-10-27)

**Note:** Version bump only for package @smartcoop/utils





# [0.4.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.3.0-alpha.0...v0.4.0) (2020-10-27)

**Note:** Version bump only for package @smartcoop/utils
