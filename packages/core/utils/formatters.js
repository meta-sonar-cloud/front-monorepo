import currency from 'currency.js'
import format from 'formatar-valores'

import isNumber from 'lodash/isNumber'
import toNumberLodash from 'lodash/toNumber'
import upperCase from 'lodash/upperCase'

export const formatCpfCnpj = (num) => format.cpfCnpj(num)

export const formatCep = (num) => format.cep(num)

export const formatPhone = (num) => format.telefone(num)

export const formatNumber = (num, locale = 'pt-BR') => {
  try {
    if (num === undefined) {
      return 0
    }

    let number = num
    if (!isNumber(number)) {
      number = Number(num)
    }
    return number.toLocaleString(locale)
  } catch (e) {
    console.log('formatNumber error:', e)
    return num
  }
}

export const formatCurrency = (
  amount,
  decimalCount = 2,
  decimal = ',',
  thousands = '.',
  prefix = 'R$ '
) => {
  try {
    const number = isNaN(Math.abs(decimalCount)) ? 2 : decimalCount
    const negativeSign = amount < 0 ? '-' : ''
    const amountToParse = Math.abs(Number(amount) || 0)

    const i = parseInt(amountToParse.toFixed(number), 10).toString()
    const j = i.length > 3 ? i.length % 3 : 0

    return (
      prefix +
      negativeSign +
      (j ? i.substr(0, j) + thousands : '') +
      i.substr(j).replace(/(\d{3})(?=\d)/g, `$1${ thousands }`) +
      (number
        ? decimal +
          Math.abs(amount - i)
            .toFixed(decimalCount)
            .slice(2)
        : '')
    )
  } catch (e) {
    console.error(`[Format Currency]: ${ e }`)
    return false
  }
}

export const formatNumberNative = (
  amount,
  decimalCount = 2,
  decimal = ',',
  thousands = '.'
) => {
  try {
    const number = isNaN(Math.abs(decimalCount)) ? 2 : decimalCount
    const negativeSign = amount < 0 ? '-' : ''
    const amountToParse = Math.abs(Number(amount) || 0)

    const i = parseInt(amountToParse.toFixed(number), 10).toString()
    const j = i.length > 3 ? i.length % 3 : 0

    return (
      negativeSign +
      (j ? i.substr(0, j) + thousands : '') +
      i.substr(j).replace(/(\d{3})(?=\d)/g, `$1${ thousands }`) +
      (number
        ? decimal +
          Math.abs(amount - i)
            .toFixed(decimalCount)
            .slice(2)
        : '')
    )
  } catch (e) {
    console.error(`[Format Number]: ${ e }`)
    return false
  }
}

export const formatCurrencyIntl = (number) =>
  new Intl.NumberFormat('pt-BR', { style: 'currency', currency: 'BRL' }).format(
    number
  )

export const toNumber = (num = '') => {
  if (!num) {
    return 0
  }

  if (typeof num === 'string') {
    const strNormalized = num.replace('.', '').replace(',', '.')
    return toNumberLodash(strNormalized)
  }

  return num
}

export const constantCase = (str) => upperCase(str).replace(/ /g, '_')

export const formatCurrencyJs = (amount) =>
  currency(amount, { symbol: 'R$ ', separator: '.', decimal: ',' }).format()

export const formatNumberJs = (amount) =>
  currency(amount, { symbol: '', separator: '.', decimal: ',' }).format()
