export default {
  interestAreas: [
    {
      id: '1a1a1a1a1',
      name: 'SOJA',
      categoryId: '4aa11a1a',
      category: {
        id: '1a51a51a',
        name: 'isane'
      },
      color: '#9b4998'
    },
    {
      id: '2b2b2b2b2',
      name: 'CCGL',
      categoryId: '4aa11a1a',
      category: {
        id: '1a51a51a',
        name: 'isane'
      },
      color: '#32d1d1'
    },
    {
      id: '3c3c3c3c3',
      name: 'SMARTCOOP',
      categoryId: '4aa11a1a',
      category: {
        id: '1a51a51a',
        name: 'isane'
      },
      color: '#4fa36a'
    }
  ],
  id: '5fca6f073c7192e1f96a325f',
  text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. At elit mauris vel amet massa ac volutpat. Tempus mollis pellentesque est consequat fermentum, et feugiat pharetra, vel. Vivamus turpis senectus fringilla arcu, aenean sagittis facilisis.',
  userId: 'Eleanor Pena',
  imageUrl: 'https://placekitten.com/600/300',
  createdAt: 'Fri Dec 04 2020 14:16:55 GMT-0300 (GMT-03:00)',
  updatedAt: 'Mon Dec 07 2020 11:31:54 GMT-0300 (GMT-03:00)',
  user: {
    id: '7d1b9de6-bd6d-48a7-9788-63863d84d3db',
    name: 'Adriano Righi',
    email: 'adriano.righi@meta.com.br',
    city: 'Vale Vêneto',
    state: 'RS'
  }
}
