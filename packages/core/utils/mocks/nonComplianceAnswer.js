import moment from 'moment/moment'

export default {
  id: '5fca6f073c7192e1f96a325f',
  text: 'Mussum Ipsum, cacilds vidis litro abertis. Não sou faixa preta cumpadi, sou preto inteiris, INTEIRIS! Pra lá, depois divoltis porris, paradis. Paisis, filhis, espiritis santis. Leite de capivaris, leite de mula manquis sem cabeça.',
  userId: 'Eleanor Pena',
  imageUrl: 'https://placekitten.com/600/300',
  createdAt: moment().format(),
  updatedAt: moment().format(),
  user: {
    id: '7d1b9de6-bd6d-48a7-9788-63863d84d3db',
    name: 'Wendel Macedo',
    email: 'wendel.oliveira@meta.com.br',
    city: 'Ananindeua',
    state: 'PA'
  }
}
