import { formatNumberJs } from './formatters'

export const secondaryUnit =
  (unitOfMeasuresForConversion = '', conversionFactor = '', measureUnit = '') => (
    `${ `${ unitOfMeasuresForConversion } ${  conversionFactor && unitOfMeasuresForConversion && `${ 'de'  } `  }` +
    `${ conversionFactor && formatNumberJs(conversionFactor) }`  } ` +
    `${  measureUnit }`
  )
