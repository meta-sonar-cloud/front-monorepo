export const TYPE_ANIMALS = {
  animals: 'animals',
  bulls: 'bulls'
}

export const SLUG_ANIMALS_STATUS = {
  prenha: 'prenha',
  inseminada: 'inseminada',
  inseminadaAConfirmar: 'inseminada_a_confirmar',
  pev: 'pev',
  aptas: 'aptas',
  vazia: 'vazia',
  nenhum: 'nenhum'
}

export const AnimalStatusCode = {
  VAZIA: '1',
  INSEMINADA: '2',
  INSEMINADA_A_CONFIRMAR: '3',
  PRENHA: '4',
  PEV: '5',
  APTAS: '6',
  NENHUM: '7'
}

export const ANIMALS_CATEGORY = {
  touro: 'touro',
  boi: 'boi',
  terneira: 'terneira',
  novilha: 'novilha',
  vaca: 'vaca'
}

export const ProposalStatusCode = {
  AGUARDANDO_PROPOSTA: 0,
  PARTICIPANDO: 1,
  REJEITADO: 2,
  AGUARDANDO_RESPOSTA: 3,
  APROVADO: 4,
  RECUSADO: 5,
  ENTREGUE: 6,
  ATRASADO: 7,
  EXPIRADO: 8,
  INVALID: 9,
  AGUARDANDO_COOPERATIVAS: 10
}

export const DemandStatusCode = {
  ABERTA: 1,
  FINALIZADO: 2,
  CANCELADA: 3,
  AGUARDANDO_RESPOSTA: 4,
  AGUARDANDO_DECISAO: 5,
  PROPOSTA_ACEITA: 6,
  RECUSADO: 7,
  AGUARDANDO_ENTREGA: 8,
  ATRASADO: 9,
  ENTREGUE: 10,
  EXPIRADO: 11
}

export const TechnicalPortfolioStatusCode = {
  AGUARDANDO_RESPOSTA: 1,
  NEGADO: 2,
  REVOGADO: 3,
  SOMENTE_VISUALIZAÇÃO: 4,
  VISUALIZAÇÃO_EDIÇÃO: 5
}
