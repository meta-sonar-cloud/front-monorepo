import { eventChannel } from 'redux-saga'
import { put, take } from 'redux-saga/effects'

import isEmpty from 'lodash/isEmpty'
import reverse from 'lodash/reverse'

import {
  firebaseAuth,
  firebaseFirestore,
  firebaseMessaging,
  firebaseFunctions
} from '@smartcoop/services/apis/firebase/firebase'

const firestoreDocPathToCollectionDocFormat = (docPath) => {
  const [doc, ...collection] = reverse(docPath.split('/'))
  return {
    collection: reverse(collection).join('/'),
    doc
  }
}

const firestoreGetQueryByDocPath = (docPath) => {
  const { collection, doc } = firestoreDocPathToCollectionDocFormat(docPath)

  if (isEmpty(collection)) {
    throw new Error('documento have not a collection')
  }

  return firebaseFirestore()
    .collection(collection)
    .doc(doc)
}

let authSubscriber

export default {
  auth: {
    signInWithCustomToken: (firebaseCustomToken) => new Promise((resolve) => {
      // Register background handler
      firebaseMessaging.setBackgroundMessageHandler(async remoteMessage => {
        console.log('Message handled in the background!', remoteMessage)
      })

      authSubscriber = firebaseAuth().onAuthStateChanged(resolve)
      firebaseAuth().signInWithCustomToken(firebaseCustomToken)
    }),

    channel: () => eventChannel(emitter => {
      const subscriber = firebaseAuth().onAuthStateChanged(user => emitter({ user }))
      return subscriber
    }),

    signOut: async () => {
      if (authSubscriber) {
        await authSubscriber()
      }
      return firebaseAuth().signOut()
    }
  },


  functions: {
    call: (endpoint, queryParams, requestConfig) => firebaseFunctions(
      endpoint,
      {
        ...requestConfig,
        data: requestConfig.body
      }
    )
  },


  messaging: {
    tokenRefreshChannel: () => eventChannel(emitter => {
      const subscriber = firebaseMessaging.onTokenRefresh(token => emitter(token))
      return subscriber
    })
  },


  firestore: {
    getDocument: async (docPath) => {
      const query = firestoreGetQueryByDocPath(docPath)
      return query.get()
    },

    setDocument: async (docPath, data) => {
      const query = firestoreGetQueryByDocPath(docPath)
      return query.set(data)
    },

    updateDocument: (docPath, key, value) => {
      const query = firestoreGetQueryByDocPath(docPath)
      return query.update({ [key]: value })
    },

    getCollection: (query) => {
      const querySnapshot = query.get()
      return querySnapshot
    },

    syncCollection: function* syncCollection(query, options) {
      const {
        successActionCreator,
        failureActionCreator,
        transform = payload => payload
      } = options

      const channel = eventChannel(emitter => {
        const onSuccess = querySnapshot => emitter([false, transform(querySnapshot)])
        const onError = error => emitter([error])

        const subscriber = query.onSnapshot(onSuccess, onError)
        return subscriber
      })

      try {
        while (true) {
          const [error, data] = yield take(channel)
          if (error) {
            yield put(failureActionCreator(error))
          } else {
            yield put(successActionCreator(data))
          }
        }
      } finally {
        channel.close()
      }
    }
  }
}
