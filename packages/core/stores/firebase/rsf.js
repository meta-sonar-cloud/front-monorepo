import ReduxSagaFirebase from 'redux-saga-firebase'

import { firebaseApp } from '@smartcoop/services/apis/firebase'

const rsf = new ReduxSagaFirebase(firebaseApp)

export default rsf
