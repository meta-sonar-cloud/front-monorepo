import { REACT_APP_FAKE_PAGINATION_SIZE } from 'react-native-dotenv'

import { call, put, select, takeLatest } from 'redux-saga/effects'

import {
  createFieldPrecipations,
  editFieldPrecipations,
  getPropertyPrecipitations,
  deleteFieldPrecipitation
} from '@smartcoop/services/apis/smartcoopApi/resources/fieldsMonitoration'
import { selectCurrentProperty } from '@smartcoop/stores/property/selectorProperty'

import { FieldsMonitorationActions, FieldsMonitorationTypes } from './duckFieldsMonitoration'

function* saveFieldsMonitoration( { fieldMonitoration, precipitationId, onSuccess = () => {}, onError = () => {} } ) {
  try {
    const currentProperty = yield select(selectCurrentProperty)

    yield call(
      precipitationId ? editFieldPrecipations : createFieldPrecipations,
      fieldMonitoration,
      { propertyId: currentProperty.id, precipitationId }
    )
    yield call(onSuccess)
  } catch (err) {
    const error = err.message
    yield put(FieldsMonitorationActions.fieldsMonitorationError(error))
    yield call(onError, error)
  }
}

function* loadFieldsMonitoration({ params = {}, onSuccess = () => {}, onError = () => {} }) {
  try {
    const currentProperty = yield select(selectCurrentProperty)
    const { data: { data, ...pagination } } = yield call(getPropertyPrecipitations, {
      limit: process.env.REACT_APP_FAKE_PAGINATION_SIZE || REACT_APP_FAKE_PAGINATION_SIZE,
      ...params
    },
    { propertyId: currentProperty.id }
    )
    yield put(FieldsMonitorationActions.loadFieldsMonitorationSuccess(
      data,
      pagination.page,
      () => onSuccess(data)
    ))

  } catch (err) {
    const error = err.message
    yield put(FieldsMonitorationActions.fieldsMonitorationError(error))
    yield call(onError, error)
  }
}

function* loadFieldsMonitorationSuccess({ onSuccess }) {
  yield call(onSuccess)
}

function* deleteFieldsMonitoration({ precipitationId, onSuccess = () => {}, onError = () => {} }) {
  try {
    const currentProperty = yield select(selectCurrentProperty)

    yield call(deleteFieldPrecipitation, { propertyId: currentProperty.id, precipitationId })
    yield call(onSuccess)
  } catch (err) {
    const error = err.message
    yield put(FieldsMonitorationActions.fieldsMonitorationError(error))
    yield call(onError, error)
  }
}

export default [
  takeLatest(FieldsMonitorationTypes.SAVE_FIELDS_MONITORATION, saveFieldsMonitoration),
  takeLatest(FieldsMonitorationTypes.LOAD_FIELDS_MONITORATION, loadFieldsMonitoration),
  takeLatest(FieldsMonitorationTypes.LOAD_FIELDS_MONITORATION_SUCCESS, loadFieldsMonitorationSuccess),
  takeLatest(FieldsMonitorationTypes.DELETE_FIELDS_MONITORATION, deleteFieldsMonitoration)
]
