import { AVAILABLE_MODULES } from '@smartcoop/stores/module'
import { selectCurrentModule } from '@smartcoop/stores/module/selectorModule'
import { selectTechnicalWrite } from '@smartcoop/stores/technical/selectorTechnical'

export const selectUser = state => state.user.data

export const selectUserId = state => state.user?.data?.id

export const selectUserCanWrite = state => {
  const currentModule = selectCurrentModule(state)

  switch (currentModule) {
    case AVAILABLE_MODULES.technical:
      return selectTechnicalWrite(state)
    default:
      return true
  }
}
