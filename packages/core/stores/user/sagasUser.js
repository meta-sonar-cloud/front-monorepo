import { call, put, select, takeLatest } from 'redux-saga/effects'

import { isEmpty } from 'lodash'

import {
  getUserByCpf,
  updateUser as updateUserService,
  createProfilePicture as createProfilePictureService,
  updateProfilePicture as updateProfilePictureService
} from '@smartcoop/services/apis/smartcoopApi/resources/user'

import { UserActions, UserTypes } from './duckUser'
import { selectUser } from './selectorUser'

function* loadUser({ onSuccess = () => {}, onError = () => {} }) {
  try {
    const { document: doc } = yield select(selectUser)

    const user = yield call(getUserByCpf, {}, { document: doc })

    yield put(UserActions.loadUserSuccess(
      user,
      onSuccess
    ))
  } catch (error) {
    yield put(UserActions.userError(error))
    yield call(onError, error)
  }
}

function* loadUserSuccess({ onSuccess = () => {} }) {
  yield call(onSuccess)
}

function* updateUser({ onSuccess = () => {} }) {
  yield call(onSuccess)
}

function* saveUser({ params = {}, onSuccess = () => {}, onError = () => {} }) {
  try {
    const { password, ...user } = yield select(selectUser)

    const data = isEmpty(params) ? user : params

    yield call(updateUserService, {
      ...data,
      activeRegister: true
    },
    { userId: user.id }
    )

    yield call(onSuccess)
  } catch (error) {
    yield put(UserActions.userError(error))
    yield call(onError, error)
  }
}

function* saveInitialUser({ params = {}, onSuccess = () => {}, onError = () => {} }) {
  try {
    const { password, ...user } = yield select(selectUser)

    const data = isEmpty(params) ? user : params

    yield call(updateUserService, {
      ...data,
      activeRegister: true
    },
    { userId: user.id }
    )

    yield call(onSuccess)
  } catch (error) {
    yield put(UserActions.userError(error))
    yield call(onError, error)
  }
}

function* saveUserPicture({ params, onSuccess = () => {}, onError = () => {} }) {
  try {
    const user = yield select(selectUser)
    isEmpty(user.profilePhoto) ?
      yield call(createProfilePictureService,
        params,
        { userId: user.id }
      ) :
      yield call(updateProfilePictureService,
        params,
        { userId: user.id, photoId: user.profilePhoto.id }
      )

    yield call(onSuccess)
  } catch (error) {
    yield put(UserActions.userError(error))
    yield call(onError, error)
  }
}

export default [
  takeLatest(UserTypes.LOAD_USER, loadUser),
  takeLatest(UserTypes.LOAD_USER_SUCCESS, loadUserSuccess),
  takeLatest(UserTypes.UPDATE_USER, updateUser),
  takeLatest(UserTypes.SAVE_USER, saveUser),
  takeLatest(UserTypes.SAVE_INITIAL_USER, saveInitialUser),
  takeLatest(UserTypes.SAVE_USER_PICTURE, saveUserPicture)
]
