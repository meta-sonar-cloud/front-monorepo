import { markActionsOffline } from 'redux-offline-queue'
import { createActions, createReducer } from 'reduxsauce'

import { AuthenticationTypes } from '../authentication'
import { makeActionFree } from '../stores'

/* Initial State */
const INITIAL_STATE = {
  data: {},
  error: null
}

/**
 * Creating actions and types with reduxsauce.
 */
export const { Types, Creators } = createActions({
  loadUser: ['onSuccess', 'onError'],
  loadUserSuccess: ['user', 'onSuccess'],
  updateUser: ['user', 'onSuccess'],
  saveUser: ['params', 'onSuccess', 'onError'],
  saveInitialUser: ['params', 'onSuccess', 'onError'],
  saveUserPicture: ['params', 'onSuccess', 'onError'],

  userError: ['error']
})

makeActionFree('loadUser')
makeActionFree('loadUserSuccess')
makeActionFree('updateUser')
makeActionFree('userError')
makeActionFree('saveInitialUser')

/**
 * Assigning offline actions
 */
markActionsOffline(Creators, [
  'saveUser',
  'saveUserPicture'
])

/**
 * Reducers functions
 */
const loadUserSuccess = (state = INITIAL_STATE, { user }) => ({
  ...state,
  error: INITIAL_STATE.error,
  data: {
    ...state.data,
    ...user
  }
})

const updateUser = (state = INITIAL_STATE, { user }) => ({
  ...state,
  error: INITIAL_STATE.error,
  data: {
    ...state.data,
    ...user
  }
})

const userError = (state = INITIAL_STATE, { error }) => ({
  ...state,
  error
})

const logout = () => ({ ...INITIAL_STATE })

const resetAuthentication = () => ({ ...INITIAL_STATE })

/**
 * Creating reducer with Types.
 */
export default createReducer(INITIAL_STATE, {
  [Types.LOAD_USER_SUCCESS]: loadUserSuccess,
  [Types.UPDATE_USER]: updateUser,

  [Types.USER_ERROR]: userError,

  [AuthenticationTypes.FIREBASE_LOGOUT_SUCCESS]: logout,
  [AuthenticationTypes.RESET_AUTHENTICATION]: resetAuthentication
})

export {
  Types as UserTypes,
  Creators as UserActions
}
