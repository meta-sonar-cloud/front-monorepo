import JwtDecode from 'jwt-decode'

import find from 'lodash/find'
import { includes } from 'lodash/fp'
import filterFP from 'lodash/fp/filter'
import flow from 'lodash/fp/flow'
import sizeFP from 'lodash/fp/size'
import size from 'lodash/size'

import { AVAILABLE_MODULES } from '@smartcoop/stores/module'

export const selectAuthenticated = (state) => state.authentication.authenticated

export const selectFirebaseAuthenticated = (state) =>
  !!state.authentication.firebaseIdToken

export const selectAccessToken = (state) => state.authentication.accessToken

export const selectRefreshToken = (state) => state.authentication.refreshToken

export const selectChangingToken = (state) => state.authentication.changingToken

export const selectExpiresAt = (state) => state.authentication.expiresAt

export const selectFirebaseCustomToken = (state) =>
  state.authentication.firebaseCustomToken

export const selectFirebaseIdToken = (state) =>
  state.authentication.firebaseIdToken

export const selectHasOrganizationToken = (state) => {
  const accessToken = selectAccessToken(state)
  return !!JwtDecode(accessToken)?.organizationId
}

export const selectPermissionsLoaded = (state) =>
  state.authentication.permissionsLoaded

export const selectUserModules = (state) =>
  state.authentication.permissions.modules || []

export const selectAllModules = (state) => state.authentication.allModules || []

export const selectUserProfile = (state) =>
  state.authentication.permissions.profile || {}

export const selectTerms = (state) => state.authentication.terms

export const loadedPermissions = (state) =>
  state.authentication.permissions.loadedPermissions

export const selectPermissionWeatherStations = (state) =>
  state.authentication.permissions.permissionWeatherStations

export const selectUserIsSupplier = (state) => {
  const userProfile = selectUserProfile(state)
  return includes(userProfile.slug, 'supplier')
}

export const selectUserIsOrganization = (state) => {
  const userProfile = selectUserProfile(state)
  return includes(userProfile.slug, 'buyer')
}

export const selectUserIsAdmin = (state) => {
  const userProfile = selectUserProfile(state)
  return includes(userProfile.slug, 'admin')
}

export const selectUserIsRuralOwner = (state) => {
  const userProfile = selectUserProfile(state)
  return includes(userProfile.slug, 'associated-rural-producer')
}

export const selectUserIsTechnician = (state) => {
  const userProfile = selectUserProfile(state)
  return includes(userProfile.slug, 'technical')
}

export const selectUserHaveDigitalProperty = (state) => {
  const userModules = selectAllModules(state)
  return !!find(userModules, { slug: AVAILABLE_MODULES.digitalProperty })
}

export const selectUserHaveCommercialization = (state) => {
  const userModules = selectAllModules(state)
  return !!find(userModules, { slug: AVAILABLE_MODULES.commercialization })
}

export const selectUserHaveShoppingPlatform = (state) => {
  const userModules = selectAllModules(state)
  return !!find(userModules, { slug: AVAILABLE_MODULES.shoppingPlatform })
}

export const selectModulesHaveTechnical = (state) => {
  const userModules = selectAllModules(state)
  return !!find(userModules, { slug: AVAILABLE_MODULES.technical })
}

export const selectUserHaveTechnical = (state) => {
  const userModules = selectUserModules(state)
  return !!find(userModules, { slug: AVAILABLE_MODULES.technical })
}

export const selectUserHaveAdministration = (state) => {
  const userModules = selectAllModules(state)
  return !!find(userModules, { slug: AVAILABLE_MODULES.administration })
}

export const selectUserHasMultipleModules = (state, isMobile = false) => {
  const userModules = selectAllModules(state)

  if (!isMobile) return size(userModules) > 1

  const result = flow(
    filterFP((mod) => mod.slug !== AVAILABLE_MODULES.shoppingPlatform),
    sizeFP
  )(userModules)
  return result > 1
}

export const selectUserProfileIsSmartcoop = (state) => {
  const userProfile = selectUserProfile(state)

  return includes(userProfile.slug, 'smartcoop')
}

export const selectUserProfileIsBuyer = (state) => {
  const userProfile = selectUserProfile(state)

  return includes(userProfile.slug, 'buyer')
}

export const selectUserProfileIsAdmin = (state) => {
  const userProfile = selectUserProfile(state)

  return includes(userProfile.slug, 'admin')
}
