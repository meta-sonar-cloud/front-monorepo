/* eslint-disable no-param-reassign */
import {
  takeEvery,
  call,
  put,
  select,
  take,
  takeLatest,
  delay
} from 'redux-saga/effects'

import trimMask from '@meta-awesome/functions/src/trimMask'
import JwtDecode from 'jwt-decode'
import moment from 'moment/moment'

import { map, split, spread, union, uniqBy } from 'lodash'
import includes from 'lodash/includes'
import isEmpty from 'lodash/isEmpty'
import isEqual from 'lodash/isEqual'
import size from 'lodash/size'
import toString from 'lodash/toString'

import api from '@smartcoop/services/apis/smartcoopApi'
import {
  login as loginService,
  logout as logoutService,
  createVerificationCode,
  validateVerificationCode as validateVerificationCodeService,
  refreshToken as refreshTokenService,
  getTokenByOrganization as getTokenByOrganizationService,
  getTerms as getTermsService,
  getUserOnboarding as getUserOnboardingService,
  updateTerm as updateTermService
} from '@smartcoop/services/apis/smartcoopApi/resources/authentication'
import { getPermissions as getPermissionsService } from '@smartcoop/services/apis/smartcoopApi/resources/permissions'
import { updateUser as updateUserService } from '@smartcoop/services/apis/smartcoopApi/resources/user'

import rsf from '../firebase/rsf'
import { MessagingTypes, MessagingActions } from '../messaging/duckMessaging'
import { selectFirebaseInitialized } from '../messaging/selectorMessaging'
import { ModuleActions } from '../module/duckModule'
import { selectCurrentModule } from '../module/selectorModule'
import { OrganizationActions } from '../organization'
import { selectCurrentOrganization } from '../organization/selectorOrganization'
import { UserActions } from '../user'
import { selectUser } from '../user/selectorUser'
import {
  AuthenticationActions,
  AuthenticationTypes
} from './duckAuthentication'
import {
  selectAuthenticated,
  selectAccessToken,
  selectRefreshToken,
  selectExpiresAt,
  selectUserModules,
  selectFirebaseCustomToken,
  selectFirebaseIdToken,
  selectAllModules,
  selectFirebaseAuthenticated
} from './selectorAuthentication'

function* login({
  document: doc,
  password,
  onSuccess = () => {},
  onError = () => {}
}) {
  try {
    const {
      accessToken,
      refreshToken,
      expiresAt,
      tokenType,
      firebaseCustomToken
    } = yield call(loginService, {
      document: doc,
      password
    })

    yield put(
      AuthenticationActions.refreshTokenSuccess(
        `${ tokenType } ${ accessToken }`,
        refreshToken,
        new Date(expiresAt)
      )
    )

    yield delay(200)

    yield put(AuthenticationActions.loadPermissions())

    yield put(AuthenticationActions.setFirebaseCustomToken(firebaseCustomToken))

    yield call(onSuccess)

    yield put(AuthenticationActions.loginSuccess())
  } catch (err) {
    const error = 'we didn\'t find your data'
    yield put(AuthenticationActions.authenticationError(error))
    yield call(onError, error)
  }
}

function* setFirebaseCustomToken() {
  yield put(AuthenticationActions.firebaseLogin())
}

function* validateSignUp({
  document: doc,
  onSuccess = () => {},
  onError = () => {},
  mobile = false
}) {
  const ERRORS = {
    USER_ALREADY_REGISTERED: 'user already registered',
    USER_SUPPLIER_DONT_HAVE_ACCESS_MOBILE:
      'you don\'t have access to this application.\nplease sign up on the desktop version'
  }

  try {
    const data = yield call(getUserOnboardingService, {}, { document: doc })

    const user = {
      ...data,
      number: toString(data.number)
    }

    if (user.activeRegister) {
      throw new Error(ERRORS.USER_ALREADY_REGISTERED)
    }

    if (user.typeSlug === 'supplier' && mobile) {
      throw new Error(ERRORS.USER_SUPPLIER_DONT_HAVE_ACCESS_MOBILE)
    }

    yield put(UserActions.updateUser(user))

    yield put(OrganizationActions.loadUserOrganizations(onSuccess))
  } catch (err) {
    const error = Object.values(ERRORS).includes(err.message)
      ? err.message
      : 'we didn\'t find your data, contact your cooperative'
    yield put(AuthenticationActions.authenticationError(error))
    yield call(onError, error)
  }
}

function* requestVerificationCode({
  protocol,
  onSuccess = () => {},
  onError = () => {}
}) {
  try {
    const user = yield select(selectUser)

    yield call(createVerificationCode, {
      document: user.document,
      protocol
    })

    yield call(onSuccess)
  } catch (error) {
    yield call(onError, error)
  }
}

function* validateVerificationCode({
  code,
  onSuccess = () => {},
  onError = () => {}
}) {
  try {
    const user = yield select(selectUser)

    const {
      accessToken,
      refreshToken,
      expiresAt,
      tokenType,
      firebaseCustomToken
    } = yield call(validateVerificationCodeService, {
      document: trimMask(user.document),
      code
    })

    yield put(
      AuthenticationActions.refreshTokenSuccess(
        `${ tokenType } ${ accessToken }`,
        refreshToken,
        expiresAt
      )
    )

    yield delay(200)

    yield put(AuthenticationActions.loadPermissions())

    yield put(AuthenticationActions.setFirebaseCustomToken(firebaseCustomToken))

    yield call(onSuccess)
  } catch (error) {
    yield put(AuthenticationActions.authenticationError(error))
    yield call(onError, error)
  }
}

function* changePassword({
  password,
  onSuccess = () => {},
  onError = () => {}
}) {
  try {
    const { id } = yield select(selectUser)

    yield call(updateUserService, { password }, { userId: id })

    yield put(UserActions.loadUser(onSuccess))
  } catch (error) {
    yield put(AuthenticationActions.authenticationError(error))
    yield call(onError, error)
  }
}

function* recoverPassword({
  document: doc,
  onSuccess = () => {},
  onError = () => {}
}) {
  try {
    const user = yield call(getUserOnboardingService, {}, { document: doc })

    if (!user.activeRegister) {
      throw new Error('user doesn\'t registered')
    }

    yield put(UserActions.updateUser(user, onSuccess))
  } catch (err) {
    const error =
      err.message === 'user doesn\'t registered'
        ? err.message
        : 'we didn\'t find your data, contact your cooperative'
    yield put(AuthenticationActions.authenticationError(error))
    yield call(onError, error)
  }
}

function* getTokenByOrganization({
  organization = {},
  onSuccess = () => {},
  onError = () => {}
}) {
  try {
    const oldAccessToken = yield select(selectAccessToken)
    const oldRefreshToken = yield select(selectRefreshToken)

    const { accessToken, refreshToken, expiresAt, tokenType } = yield call(
      getTokenByOrganizationService,
      {
        accessToken: split(oldAccessToken, ' ')[1],
        refreshToken: oldRefreshToken,
        registry: organization.registry
      },
      { organizationId: organization.id }
    )

    yield put(
      AuthenticationActions.refreshTokenSuccess(
        `${ tokenType } ${ accessToken }`,
        refreshToken,
        expiresAt
      )
    )
  } catch (error) {
    yield call(onError, error)
  } finally {
    yield delay(200)
  }

  yield put(AuthenticationActions.loadPermissions(onSuccess))
  yield delay(200)
}

function* loadPermissions({ onSuccess = () => {} }) {
  try {
    const { data } = yield call(getPermissionsService)
    const permissionWeatherStations = includes(
      map(
        data,
        (permission) =>
          permission?.organizationsUser?.organization?.weatherStationsEnabled
      ),
      true
    )
    const allModulesStore = yield select(selectAllModules)
    const modules = uniqBy(
      spread(union)(map(data, ({ modules: internalModule }) => internalModule)),
      'id'
    )
    const slug = [...map(data, (item) => item.profile.slug)]

    const allModules = isEmpty(allModulesStore) ? modules : allModulesStore

    yield put(AuthenticationActions.loadAllModules(allModules))

    yield put(
      AuthenticationActions.loadPermissionsSuccess(
        {
          modules,
          profile: { slug },
          loadedPermissions: true,
          permissionWeatherStations
        },
        onSuccess
      )
    )
  } catch (error) {
    console.error('loadPermissions error', error)
  }
}

function* loadPermissionsSuccess({ onSuccess = () => {} }) {
  const userModules = yield select(selectUserModules)
  const currentModule = yield select(selectCurrentModule)
  const currentOrganization = yield select(selectCurrentOrganization)
  const userModulesSlugs = !isEmpty(userModules)
    ? map(userModules, ({ slug }) => slug)
    : []

  if (
    !!currentModule &&
    !isEmpty(currentOrganization) &&
    !userModulesSlugs.includes(currentModule)
  ) {
    yield put(OrganizationActions.clearCurrentOrganization())
    yield put(ModuleActions.exitCurrentModule())
  }

  if (size(userModules) === 0) {
    yield put(AuthenticationActions.logout())
  }

  if (size(userModules) === 1 && currentModule !== userModules[0].slug) {
    yield put(ModuleActions.initModuleBySlug(userModules[0].slug))
  }

  yield call(onSuccess)
}

function* refreshTokenSuccess({ accessToken, onSuccess = () => {} }) {
  if (!isEmpty(accessToken)) {
    const { document: doc } = JwtDecode(split(accessToken, ' ')[1])
    // TODO organizationId tbm retorna no jwt

    yield put(UserActions.updateUser({ document: doc }))

    yield delay(2000)

    yield startRefreshTokenInterceptor()
    yield call(onSuccess)
  }
}

function* logout() {
  try {
    const accessToken = yield select(selectAccessToken)
    const refreshToken = yield select(selectRefreshToken)

    if (accessToken) {
      try {
        yield call(logoutService, {
          accessToken,
          refreshToken
        })
      } catch (e) {
        // eslint-disable-next-line no-console
        console.error('could not logout on api')
      }
    }

    yield put(AuthenticationActions.logoutSuccess())

    const firebaseAuthenticated = yield select(selectFirebaseAuthenticated)
    if (firebaseAuthenticated) {
      yield put(AuthenticationActions.firebaseLogout())
    }
  } catch (err) {
    const error = err.message
    yield put(AuthenticationActions.authenticationError(error))
  }
}

function* logoutSuccess() {
  yield stopRefreshTokenInterceptor()
}

let firebaseWasInitialized = false
function* syncFirebaseUserSaga() {
  const authenticated = yield select(selectAuthenticated)
  const channel = yield call(rsf.auth.channel)
  try {
    if (authenticated && !firebaseWasInitialized) {
      yield put(MessagingActions.initCloudMessaging())
      yield put(MessagingActions.subscribeUserNotifications())
      firebaseWasInitialized = true
    }
    while (true) {
      const { user } = yield take(channel)
      if (user) {
        const accessToken = yield select(selectAccessToken)
        const oldFirebaseIdToken = yield select(selectFirebaseIdToken)
        const firebaseIdToken = yield user.getIdToken()

        if (isEmpty(accessToken)) {
          yield put(AuthenticationActions.firebaseLogout())
        } else if (
          !isEmpty(firebaseIdToken) &&
          !isEqual(oldFirebaseIdToken, firebaseIdToken)
        ) {
          yield put(AuthenticationActions.firebaseLoginSuccess(firebaseIdToken))
          yield delay(500)
          if (!firebaseWasInitialized) {
            yield put(MessagingActions.initCloudMessaging())
            yield put(MessagingActions.subscribeUserNotifications())
            firebaseWasInitialized = true
          }
        }
      } else {
        firebaseWasInitialized = false
      }
    }
  } finally {
    channel.close()
  }
}

function* firebaseLogin() {
  try {
    const firebaseCustomToken = yield select(selectFirebaseCustomToken)
    yield call(rsf.auth.signInWithCustomToken, firebaseCustomToken)
  } catch (e) {
    // eslint-disable-next-line no-console
    console.error('Error on firebase login', e)
    throw e
  }
}

function* firebaseLogout() {
  try {
    firebaseWasInitialized = false
    yield put({ type: MessagingTypes.UNSUBSCRIBE_USER_NOTIFICATIONS })
    yield put({ type: MessagingTypes.UNSET_CLOUD_MESSAGING_TOKEN })
    yield take(MessagingTypes.UNSET_CLOUD_MESSAGING_TOKEN_SUCCESS)
    yield call(rsf.auth.signOut)
  } catch (e) {
    // eslint-disable-next-line no-console
    console.error('firebase logout error', e)
  } finally {
    yield put(AuthenticationActions.firebaseLogoutSuccess())
  }
}

function* doRefreshToken({ isLogout }) {
  yield delay(5000)

  const oldAccessToken = yield select(selectAccessToken)
  const oldRefreshToken = yield select(selectRefreshToken)

  const { accessToken, refreshToken, expiresAt, tokenType } = yield call(
    refreshTokenService,
    {
      accessToken: split(oldAccessToken, ' ')[1],
      refreshToken: oldRefreshToken
    },
    isLogout
  )

  yield put(
    AuthenticationActions.refreshTokenSuccess(
      `${ tokenType } ${ accessToken }`,
      refreshToken,
      expiresAt
    )
  )
}

function* tryRefreshToken({ isLogout, onSuccess }) {
  yield put(AuthenticationActions.refreshToken(isLogout))
  yield take(AuthenticationTypes.REFRESH_TOKEN_SUCCESS)
  const accessToken = yield select(selectAccessToken)
  yield call(onSuccess, accessToken)
}

let refreshTokenInterceptor
let globalDispatch

async function stopRefreshTokenInterceptor() {
  if (refreshTokenInterceptor) {
    await api.interceptors.request.eject(refreshTokenInterceptor)
    refreshTokenInterceptor = undefined
  }
}

function* startRefreshTokenInterceptor(onSuccess = () => {}) {
  if (refreshTokenInterceptor !== undefined) {
    yield call(stopRefreshTokenInterceptor)
  }

  const authenticated = yield select(selectAuthenticated)
  const oldExpiresAt = yield select(selectExpiresAt)

  refreshTokenInterceptor = api.interceptors.request.use(
    (config) =>
      new Promise((resolve) => {
        if (
          authenticated &&
          config.url !== '/auth' &&
          config.url !== '/auth/refresh-token' &&
          config.url !== '/auth/refresh-token?isLogout' &&
          moment().isAfter(moment(oldExpiresAt))
        ) {
          globalDispatch(
            AuthenticationActions.tryRefreshToken(
              config.url === '/auth/logout',
              (accessToken) => {
                config.headers.common.Authorization = accessToken
                resolve(config)
              }
            )
          )
        } else {
          resolve(config)
        }
      })
  )

  yield call(onSuccess)
}

function* initAuthSagas({ dispatch }) {
  if (dispatch && !globalDispatch) {
    globalDispatch = dispatch
  }

  const authenticated = yield select(selectAuthenticated)
  const firebaseInitialized = yield select(selectFirebaseInitialized)

  if (authenticated) {
    if (!refreshTokenInterceptor) {
      yield startRefreshTokenInterceptor()
    }
  } else if (firebaseInitialized) {
    yield put(AuthenticationActions.firebaseLogout())
  }
}

function* loadTermsSuccess({ onSuccess = () => {} }) {
  yield call(onSuccess)
}

function* loadTerms({ onSuccess = () => {}, onError = () => {} }) {
  try {
    const { data } = yield call(getTermsService, {})
    yield put(AuthenticationActions.loadTermsSuccess(data, onSuccess))
  } catch (error) {
    yield put(AuthenticationActions.authenticationError(error))
    yield call(onError, error)
  }
}

function* updateTerm({ slug, onSuccess = () => {}, onError = () => {} }) {
  try {
    yield call(updateTermService, { ...slug })
    yield put(AuthenticationActions.loadTerms(onSuccess))
  } catch (error) {
    yield put(AuthenticationActions.authenticationError(error))
    yield call(onError, error)
  }
}

export default [
  takeLatest(AuthenticationTypes.VALIDATE_SIGN_UP, validateSignUp),

  takeLatest(
    AuthenticationTypes.SET_FIREBASE_CUSTOM_TOKEN,
    setFirebaseCustomToken
  ),
  takeLatest(AuthenticationTypes.FIREBASE_LOGIN, firebaseLogin),
  takeLatest(AuthenticationTypes.FIREBASE_LOGOUT, firebaseLogout),
  takeLatest(AuthenticationTypes.SYNC_FIREBASE_USER_SAGA, syncFirebaseUserSaga),

  takeLatest(
    AuthenticationTypes.REQUEST_VERIFICATION_CODE,
    requestVerificationCode
  ),
  takeLatest(
    AuthenticationTypes.VALIDATE_VERIFICATION_CODE,
    validateVerificationCode
  ),

  takeLatest(AuthenticationTypes.UPDATE_TERM, updateTerm),

  takeEvery(AuthenticationTypes.TRY_REFRESH_TOKEN, tryRefreshToken),
  takeLatest(AuthenticationTypes.REFRESH_TOKEN, doRefreshToken),
  takeLatest(AuthenticationTypes.REFRESH_TOKEN_SUCCESS, refreshTokenSuccess),
  takeLatest(AuthenticationTypes.INIT_AUTH_SAGAS, initAuthSagas),

  takeLatest(
    AuthenticationTypes.GET_TOKEN_BY_ORGANIZATION,
    getTokenByOrganization
  ),

  takeLatest(AuthenticationTypes.LOAD_PERMISSIONS, loadPermissions),
  takeLatest(
    AuthenticationTypes.LOAD_PERMISSIONS_SUCCESS,
    loadPermissionsSuccess
  ),

  takeLatest(AuthenticationTypes.CHANGE_PASSWORD, changePassword),
  takeLatest(AuthenticationTypes.RECOVER_PASSWORD, recoverPassword),

  takeLatest(AuthenticationTypes.LOAD_TERMS, loadTerms),
  takeLatest(AuthenticationTypes.LOAD_TERMS_SUCCESS, loadTermsSuccess),

  takeLatest(AuthenticationTypes.LOGIN, login),
  takeLatest(AuthenticationTypes.LOGOUT, logout),
  takeLatest(AuthenticationTypes.LOGOUT_SUCCESS, logoutSuccess)
]
