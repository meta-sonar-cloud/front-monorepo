import { all, put, takeLatest, take, select } from 'redux-saga/effects'

import { OrganizationActions } from '../organization'
import { PropertyActions } from '../property'
import { UserTypes } from '../user'
import { selectUserId } from '../user/selectorUser'
import { AVAILABLE_MODULES, ModuleActions, ModuleTypes } from './duckModule'

function* initModuleBySlug({ slug }) {
  yield put(OrganizationActions.clearCurrentOrganization())
  yield put(OrganizationActions.loadUserOrganizationsByModule())
  const userId = yield select(selectUserId)

  if (!userId) {
    yield take(UserTypes.LOAD_USER_SUCCESS)
  }

  switch (slug) {
    case AVAILABLE_MODULES.digitalProperty:
      return yield put(ModuleActions.initDigitalPropertyModule())
    case AVAILABLE_MODULES.shoppingPlatform:
      return yield put(ModuleActions.initShoppingPlatformModule())
    case AVAILABLE_MODULES.commercialization:
      return yield put(ModuleActions.initCommercializationModule())
    case AVAILABLE_MODULES.technical:
      return yield put(ModuleActions.initTechnicalModule())
    case AVAILABLE_MODULES.administration:
      return yield put(ModuleActions.initAdministrationModule())
    default:
      throw new Error('user does not any module')
  }
}

function* initDigitalPropertyModule() {
  yield put(OrganizationActions.clearCurrentOrganization())
  yield all([
    put(PropertyActions.loadProperties()),
    put(PropertyActions.loadActivities())
  ])
}

function* initShoppingPlatformModule() {
  yield put(OrganizationActions.clearCurrentOrganization())
  yield put(OrganizationActions.loadUserOrganizationsByModule())
}

function* initCommercializationModule() {
  yield put(OrganizationActions.clearCurrentOrganization())
  yield put(OrganizationActions.loadUserOrganizationsByModule())
}

function* initTechnicalModule() {
  yield put(OrganizationActions.clearCurrentOrganization())
  yield put(OrganizationActions.loadUserOrganizationsByModule())
}

function* initAdministrationModule() {
  yield put(OrganizationActions.clearCurrentOrganization())
  yield put(OrganizationActions.loadUserOrganizationsByModule())
}

function* exitCurrentModule() {
  yield put(OrganizationActions.clearCurrentOrganization())
}

export default [
  takeLatest(ModuleTypes.INIT_MODULE_BY_SLUG, initModuleBySlug),
  takeLatest(
    ModuleTypes.INIT_DIGITAL_PROPERTY_MODULE,
    initDigitalPropertyModule
  ),
  takeLatest(
    ModuleTypes.INIT_SHOPPING_PLATFORM_MODULE,
    initShoppingPlatformModule
  ),
  takeLatest(
    ModuleTypes.INIT_COMMERCIALIZATION_MODULE,
    initCommercializationModule
  ),
  takeLatest(ModuleTypes.INIT_TECHNICAL_MODULE, initTechnicalModule),
  takeLatest(ModuleTypes.INIT_ADMINISTRATION_MODULE, initAdministrationModule),

  takeLatest(ModuleTypes.EXIT_CURRENT_MODULE, exitCurrentModule)
]
