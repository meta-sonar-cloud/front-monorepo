import { AVAILABLE_MODULES } from './duckModule'

export const selectCurrentModule = (state) => state.module.currentModule

export const selectModuleIsTechnical = (state) => selectCurrentModule(state) === AVAILABLE_MODULES.technical
