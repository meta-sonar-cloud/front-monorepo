export const selectIndicators = state => state.sateliteSvc.indicators

export const selectIndicatorsLoaded = state => state.sateliteSvc.loaded

export const selectHasMissingImages = state => state.sateliteSvc.hasMissingImages
