export const selectNewNotificationsCount = (state) => state.messaging.unreadCounter

export const selectLastNotifications = (state) => state.messaging.lastNotifications

export const selectAllNotifications = (state) => state.messaging.allNotifications || []

export const selectLastAllNotificationVisible = (state) => state.messaging.lastVisible

export const selectHoursPendingAttention = (state) => state.messaging.hoursPendingAttention

export const selectFirebaseInitialized = (state) => state.messaging.firebaseInitialized
