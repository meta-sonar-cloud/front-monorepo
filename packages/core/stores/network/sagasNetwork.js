import { OFFLINE, ONLINE } from 'redux-offline-queue'
import { put, take, call, delay } from 'redux-saga/effects'

import api from '@smartcoop/services/apis/smartcoopApi'

function* pingApi() {
  try {
    yield call(api.get, '/', { timeout: 5000 })
    yield put({ type: ONLINE })
  } catch (e) {
    if (e.request.status === 404) {
      // could see backend server
      yield put({ type: ONLINE })
    } else {
      // could not see backend server
      // probably out of vpn
      yield put({ type: OFFLINE })
      yield delay(5000)
      yield pingApi()
    }
  }
}

export function startWatchingNetworkConnectivity(networkChannel) {
  return function* watchNetworkConnectivity() {
    try {
      while (true) {
        const hasInternet = yield take(networkChannel)
        if (hasInternet) {
          yield pingApi()
        } else {
          yield put({ type: OFFLINE })
        }
      }
    } finally {
      networkChannel.close()
    }
  }
}
