export const selectProductGroups = state => state.productGroup.productGroups

export const selectCurrentProductGroup = state => state.productGroup.currentProductGroup
