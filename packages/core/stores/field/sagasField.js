import { call, put, select, takeLatest } from 'redux-saga/effects'

import { createCropManagement, deleteCropManagements, editCropManagement } from '@smartcoop/services/apis/smartcoopApi/resources/cropManagement'
import { createField, deleteField as deleteFieldService, editField, getField, getFieldsByPropertyId } from '@smartcoop/services/apis/smartcoopApi/resources/field'
import { createGrowingSeasons, deleteGrowingSeasons, editGrowingSeasons } from '@smartcoop/services/apis/smartcoopApi/resources/growingSeasons'
import {
  createPestReport as createPestReportService,
  deletePestReport as deletePestReportService,
  editPestReport,
  postFilesPestReport,
  putFilesPestReport
} from '@smartcoop/services/apis/smartcoopApi/resources/pestReport'
import { selectCurrentProperty } from '@smartcoop/stores/property/selectorProperty'


import { FieldActions, FieldTypes } from './duckField'
import { selectCurrentField } from './selectorField'



function* loadFields({ params = {}, onSuccess = () => {}, onError = () => {} }) {
  try {
    const currentProperty = yield select(selectCurrentProperty)
    const data = yield call(getFieldsByPropertyId, params, { propertyId: currentProperty.id })

    yield put(FieldActions.loadFieldsSuccess(
      data,
      onSuccess
    ))
  } catch (err) {
    const error = err.message
    yield put(FieldActions.fieldError(error))
    yield call(onError, error)
  }
}

function* loadCurrentField({ fieldId, onSuccess = () => {}, onError = () => {} }) {
  try {
    yield put(FieldActions.toggleCurrentFieldLoading(true))
    let id = fieldId
    const currentField = yield select(selectCurrentField)
    if (!fieldId && currentField.id) {
      id = currentField.id
    }
    const data = yield call(getField, { fieldId: id })
    yield put(FieldActions.setCurrentField(data))
    yield put(FieldActions.toggleCurrentFieldLoading(false))
    yield call(onSuccess)
  } catch (err) {
    const error = err.message
    yield put(FieldActions.fieldError(error))
    yield call(onError, error)
  }
}

function* loadFieldsSuccess({ onSuccess }) {
  yield call(onSuccess)
}

function* saveOfflineField({ field, onSuccess = () => {}, onError = () => {} }) {
  yield put(FieldActions.saveField(field, onSuccess, onError))
}

function* saveField({ field, onSuccess = () => {}, onError = () => {} }) {
  try {
    const property = yield select(selectCurrentProperty)

    const { data } = yield call(
      field.id ? editField : createField,
      { ...field, propertyId: property.id }, { fieldId: field.id }
    )

    if (field.id) {
      yield put(FieldActions.loadCurrentField(field.id))
    }

    yield call(onSuccess, data.id)
  } catch (err) {
    const error = err.message
    yield put(FieldActions.fieldError(error))
    yield call(onError, err)
  }
}

function* updateOfflineField({ field, onSuccess = () => {}, onError = () => {} }) {
  yield put(FieldActions.saveField(field, onSuccess, onError))
}

function* deleteOfflineField({ fieldId, onSuccess = () => {}, onError = () => {} }) {
  yield put(FieldActions.deleteField(fieldId, onSuccess, onError))
}

function* deleteField({ fieldId, onSuccess = () => {}, onError = () => {} }) {
  try {
    yield call(deleteFieldService, { fieldId })
    yield call(loadFields, '', onSuccess)
  } catch (err) {
    const error = err.message
    yield put(FieldActions.fieldError(error))
    yield call(onError, err)
  }
}

function* saveGrowingSeason({ growingSeason, onSuccess = () => {}, onError = () => {} }) {
  try {
    const field = yield select(selectCurrentField)

    yield call(
      field.growingSeason?.id ? editGrowingSeasons : createGrowingSeasons,
      { ...growingSeason, fieldId: field.id },
      { growingSeasonId: field.growingSeason?.id }
    )

    yield put(FieldActions.loadCurrentField())
    yield call(onSuccess)
  } catch (err) {
    const error = err.message
    yield put(FieldActions.fieldError(error))
    yield call(onError, err)
  }
}

function* saveOfflineGrowingSeason({ growingSeason, onSuccess = () => {}, onError = () => {} }) {
  yield put(FieldActions.saveGrowingSeason(growingSeason, onSuccess, onError))
}

function* deleteOfflineGrowingSeason({ growingSeasonId, onSuccess = () => {}, onError = () => {} }) {
  yield put(FieldActions.deleteGrowingSeason(growingSeasonId, onSuccess, onError))
}

function* deleteGrowingSeason({ growingSeasonId, onSuccess = () => {}, onError = () => {} }) {
  try {
    yield put(FieldActions.toggleCurrentFieldLoading(true))
    yield call(deleteGrowingSeasons, { growingSeasonId })
    yield call(onSuccess)
    yield put(FieldActions.loadCurrentField())
  } catch (err) {
    const error = err.message
    yield put(FieldActions.fieldError(error))
    yield call(onError, err)
  }
}

function* saveOfflineCropManagement({ cropManagement, onSuccess = () => {}, onError = () => {} }) {
  try {
    yield put(FieldActions.toggleCurrentFieldLoading(true))
    const field = yield select(selectCurrentField)

    const cropService = cropManagement.id ?
      editCropManagement : createCropManagement

    yield call(
      cropService,
      cropManagement,
      {
        growingSeasonId: field.growingSeason.id,
        cropManagementId: cropManagement.id
      }
    )

    if(cropManagement.id){
      yield put(FieldActions.loadCurrentField(field.id))
    }

    yield call(onSuccess)

  } catch (err) {
    const error = err.message
    yield put(FieldActions.fieldError(error))
    yield call(onError, err)
  }
}

function* deleteOfflineCropManagement({ growingSeasonId, cropManagementId, onSuccess = () => {}, onError = () => {} }) {
  yield put(FieldActions.deleteCropManagement(growingSeasonId, cropManagementId, onSuccess, onError))
}

function* deleteCropManagement({ growingSeasonId, cropManagementId, onSuccess = () => {}, onError = () => {} }) {
  try {
    yield put(FieldActions.toggleCurrentFieldLoading(true))
    yield call(deleteCropManagements, { growingSeasonId, cropManagementId })
    yield put(FieldActions.loadCurrentField())
    yield call(onSuccess)
  } catch (err) {
    const error = err.message
    yield put(FieldActions.fieldError(error))
    yield call(onError, err)
  }
}

function* deletePestReport({ reportId, onSuccess = () => {}, onError = () => {} }) {
  try {
    yield call(deletePestReportService, { reportId })
    yield put(FieldActions.loadCurrentField(null, onSuccess, onError))
  } catch (err) {
    const error = err.message
    yield put(FieldActions.fieldError(error))
    yield call(onError, err)
  }
}

function* savePestReport({ pestReport, growingSeasonId, onSuccess = () => {}, onError = () => {} }) {
  try {
    const { data } = yield call(
      createPestReportService,
      pestReport,
      { growingSeasonId }
    )

    yield put(FieldActions.loadCurrentField())
    yield call(onSuccess, data)
  } catch (err) {
    const error = err.message
    yield put(FieldActions.fieldError(error))
    yield call(onError)
  }
}

function* saveOfflinePestReport({ pestReport, growingSeasonId, onSuccess = () => {}, onError = () => {} }) {
  yield put(FieldActions.savePestReport(pestReport, growingSeasonId, onSuccess, onError))
}

function* deleteOfflinePestReport({ reportId, onSuccess = () => {}, onError = () => {} }) {
  yield put(FieldActions.deletePestReport(reportId, onSuccess, onError))
}

function* updatePestReport({ params, reportId, onSuccess = () => {}, onError = () => {} }) {
  try {
    const { data } = yield call(editPestReport, params, { reportId })

    yield put(FieldActions.loadCurrentField())
    yield call(onSuccess, data)
  } catch (error) {
    yield put(FieldActions.fieldError(error))
    yield call(onError, error)
  }
}

function* updateOfflinePestReport({ params, reportId, onSuccess = () => {}, onError = () => {} }) {
  yield put(FieldActions.updatePestReport(params, reportId, onSuccess, onError))
}

function* addPestReportFiles({ params, reportId, onSuccess = () => {}, onError = () => {} }) {
  try {
    yield call(postFilesPestReport, params, { reportId })
    yield call(onSuccess)
  } catch (error) {
    yield put(FieldActions.fieldError(error))
    yield call(onError, error)
  }
}

function* editPestReportFiles({ params, reportId, onSuccess = () => {}, onError = () => {} }) {
  try {
    yield call(putFilesPestReport, { oldFiles: params }, { reportId })
    yield call(onSuccess)
  } catch (error) {
    yield put(FieldActions.fieldError(error))
    yield call(onError, error)
  }
}

export default [
  takeLatest(FieldTypes.LOAD_FIELDS, loadFields),
  takeLatest(FieldTypes.LOAD_FIELDS_SUCCESS, loadFieldsSuccess),

  takeLatest(FieldTypes.LOAD_CURRENT_FIELD, loadCurrentField),

  takeLatest(FieldTypes.SAVE_FIELD, saveField),
  takeLatest(FieldTypes.SAVE_OFFLINE_FIELD, saveOfflineField),

  takeLatest(FieldTypes.UPDATE_OFFLINE_FIELD, updateOfflineField),

  takeLatest(FieldTypes.DELETE_FIELD, deleteField),
  takeLatest(FieldTypes.DELETE_OFFLINE_FIELD, deleteOfflineField),


  takeLatest(FieldTypes.SAVE_GROWING_SEASON, saveGrowingSeason),
  takeLatest(FieldTypes.SAVE_OFFLINE_GROWING_SEASON, saveOfflineGrowingSeason),

  takeLatest(FieldTypes.DELETE_GROWING_SEASON, deleteGrowingSeason),
  takeLatest(FieldTypes.DELETE_OFFLINE_GROWING_SEASON, deleteOfflineGrowingSeason),

  takeLatest(FieldTypes.SAVE_OFFLINE_CROP_MANAGEMENT, saveOfflineCropManagement),

  takeLatest(FieldTypes.DELETE_OFFLINE_CROP_MANAGEMENT, deleteOfflineCropManagement),
  takeLatest(FieldTypes.DELETE_CROP_MANAGEMENT, deleteCropManagement),

  takeLatest(FieldTypes.SAVE_PEST_REPORT, savePestReport),
  takeLatest(FieldTypes.SAVE_OFFLINE_PEST_REPORT, saveOfflinePestReport),
  takeLatest(FieldTypes.DELETE_OFFLINE_PEST_REPORT, deleteOfflinePestReport),
  takeLatest(FieldTypes.DELETE_PEST_REPORT, deletePestReport),
  takeLatest(FieldTypes.UPDATE_PEST_REPORT, updatePestReport),
  takeLatest(FieldTypes.UPDATE_OFFLINE_PEST_REPORT, updateOfflinePestReport),
  takeLatest(FieldTypes.ADD_PEST_REPORT_FILES, addPestReportFiles),
  takeLatest(FieldTypes.EDIT_PEST_REPORT_FILES, editPestReportFiles)
]
