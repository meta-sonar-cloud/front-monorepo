import { markActionsOffline } from 'redux-offline-queue'
import { createActions, createReducer } from 'reduxsauce'

import filter from 'lodash/filter'
import map from 'lodash/map'

import { AuthenticationTypes } from '../authentication'

// const INITIAL_FIELD = {
//   fieldName: '',
//   area: undefined,
//   fieldMode: undefined,
//   irrigated: undefined,
//   polygonCoordinates: undefined
// }

// Initial state
const INITIAL_STATE = {
  fields: [],
  currentField: {},
  error: null,
  loadingCurrentField: false
}

/**
 * Creating actions and types with reduxsauce.
 */
const { Types, Creators } = createActions({
  loadFields: ['params', 'onSuccess', 'onError'],
  loadFieldsSuccess: ['fields', 'onSuccess'],
  resetFields: [],

  saveField: ['field', 'onSuccess', 'onError'],
  saveOfflineField: ['field', 'onSuccess', 'onError'],
  updateOfflineField: ['field'],

  deleteField: ['fieldId'],
  deleteOfflineField: ['fieldId'],

  setCurrentField: ['currentField'],
  resetCurrentField: [],

  loadCurrentField: ['fieldId', 'onSuccess', 'onError'],

  saveGrowingSeason: ['growingSeason', 'onSuccess', 'onError'],
  saveOfflineGrowingSeason: ['growingSeason', 'onSuccess', 'onError'],

  deleteGrowingSeason: ['growingSeasonId', 'onSuccess', 'onError'],
  deleteOfflineGrowingSeason: ['growingSeasonId', 'onSuccess', 'onError'],

  saveCropManagement: ['cropManagement', 'onSuccess', 'onError'],
  saveOfflineCropManagement: ['cropManagement', 'onSuccess', 'onError'],

  deleteCropManagement: ['growingSeasonId', 'cropManagementId', 'onSuccess', 'onError'],
  deleteOfflineCropManagement: ['growingSeasonId', 'cropManagementId', 'onSuccess', 'onError'],

  fieldError: ['error'],

  savePestReport: ['pestReport', 'growingSeasonId', 'onSuccess', 'onError'],
  saveOfflinePestReport: ['pestReport', 'growingSeasonId', 'onSuccess', 'onError'],

  deletePestReport: ['reportId', 'onSuccess', 'onError'],
  deleteOfflinePestReport: ['reportId', 'onSuccess', 'onError'],

  updatePestReport: ['params', 'reportId', 'onSuccess', 'onError'],
  updateOfflinePestReport: ['params', 'reportId', 'onSuccess', 'onError'],

  addPestReportFiles: ['params', 'reportId', 'onSuccess', 'onError'],
  editPestReportFiles: ['params', 'reportId', 'onSuccess', 'onError'],

  toggleCurrentFieldLoading: ['loading']
})

/**
 * Assigning offline actions
 */
markActionsOffline(Creators,
  [
    'saveField',
    'deleteField',
    'saveOfflinePestReport',
    'deleteOfflinePestReport',
    'updateOfflinePestReport',
    'addPestReportFiles',
    'editPestReportFiles'
  ]
)

/**
 * Reducers functions
 */
const loadFieldsSuccess = (state = INITIAL_STATE, { fields }) => ({
  ...state,
  error: INITIAL_STATE.error,
  fields
})

const toggleCurrentFieldLoading = (state = INITIAL_STATE, { loading }) => ({
  ...state,
  loadingCurrentField: loading
})


// const saveOfflineField = (state = INITIAL_STATE, { field }) => ({
//   ...state,
//   fields: [
//     ...state.fields,
//     {
//       ...INITIAL_FIELD,
//       ...field
//     }
//   ]
// })

const updateOfflineField = (state = INITIAL_STATE, { field }) => ({
  ...state,
  fields: map(state.fields, (oldField) => (oldField.id === field.id ? field : oldField))
})

const deleteOfflineField = (state = INITIAL_STATE, { fieldId }) => ({
  ...state,
  fields: filter(state.fields, ({ id }) => id !== fieldId)
})

const fieldError = (state = INITIAL_STATE, { error }) => ({
  ...state,
  error
})

const setCurrentField = (state = INITIAL_STATE, { currentField }) => ({
  ...state,
  currentField
})

const resetCurrentField = (state = INITIAL_STATE) => ({
  ...state,
  currentField: INITIAL_STATE.currentField
})

const resetFields = (state = INITIAL_STATE) => ({
  ...state,
  fields: INITIAL_STATE.fields
})

const saveOfflineGrowingSeason = (state = INITIAL_STATE, { growingSeason }) => ({
  ...state,
  currentField: {
    ...state.currentField,
    growingSeason: state.currentField.growingSeason
      ? {
        ...state.currentField.growingSeason,
        ...growingSeason
      }
      : state.currentField.growingSeason
  }
})

const deleteOfflineGrowingSeason = (state = INITIAL_STATE) => ({
  ...state,
  currentField: {
    ...state.currentField,
    growingSeason: {}
  }
})

const saveOfflinePestReport = (state = INITIAL_STATE, { pestReport }) => ({
  ...state,
  currentField: {
    ...state.currentField,
    growingSeason: state.currentField.growingSeason
      ? {
        ...state.currentField.growingSeason,
        pestReport
      }
      : state.currentField.growingSeason.pestReport
  }
})

const deleteOfflinePestReport = (state = INITIAL_STATE) => ({
  ...state,
  currentField: {
    ...state.currentField,
    growingSeason: {
      plagueReport: {}
    }
  }
})


const logout = () => ({ ...INITIAL_STATE })

/**
 * Creating reducer with Types.
 */
export default createReducer(INITIAL_STATE, {
  [Types.LOAD_FIELDS_SUCCESS]: loadFieldsSuccess,
  [Types.RESET_FIELDS]: resetFields,

  // [Types.SAVE_OFFLINE_FIELD]: saveOfflineField,
  [Types.UPDATE_OFFLINE_FIELD]: updateOfflineField,
  [Types.DELETE_OFFLINE_FIELD]: deleteOfflineField,

  [Types.FIELD_ERROR]: fieldError,

  [Types.SET_CURRENT_FIELD]: setCurrentField,
  [Types.RESET_CURRENT_FIELD]: resetCurrentField,

  [Types.SAVE_OFFLINE_GROWING_SEASON]: saveOfflineGrowingSeason,
  [Types.DELETE_OFFLINE_GROWING_SEASON]: deleteOfflineGrowingSeason,

  [Types.SAVE_OFFLINE_PEST_REPORT]: saveOfflinePestReport,
  [Types.DELETE_OFFLINE_PEST_REPORT]: deleteOfflinePestReport,

  [Types.TOGGLE_CURRENT_FIELD_LOADING]: toggleCurrentFieldLoading,

  [AuthenticationTypes.LOGOUT]: logout
})

export { Types as FieldTypes, Creators as FieldActions }
