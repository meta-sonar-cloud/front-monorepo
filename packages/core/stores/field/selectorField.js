export const selectFields = state => state.field.fields
export const selectCurrentField = state => state.field.currentField || {}
export const selectLoadingCurrentField = state => state.field.loadingCurrentField
