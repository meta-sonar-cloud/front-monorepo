import { call, put, takeLatest } from 'redux-saga/effects'

import { getCropManagement } from '@smartcoop/services/apis/smartcoopApi/resources/cropManagement'

import { CropManagementActions, CropManagementTypes } from './duckCropManagement'

function* loadCropManagement({ params = {}, onSuccess = () => {}, onError = () => {} }) {
  try {
    const { growingSeasonId, cropManagementId, externalCropManagementData } = params
    const { data } = yield call(getCropManagement, {}, { growingSeasonId, cropManagementId } )

    const cropManagement = {
      ...externalCropManagementData,
      ...data
    }

    yield put(CropManagementActions.loadCropManagementSuccess(
      cropManagement,
      () => onSuccess()
    ))
  } catch (err) {
    const error = err.message
    yield put(CropManagementActions.cropManagementError(error))
    yield call(onError, error)
  }
}

function* loadCropManagementSuccess({ onSuccess = () => {} }) {
  yield call(onSuccess)
}


export default [
  takeLatest(CropManagementTypes.LOAD_CROP_MANAGEMENT, loadCropManagement),
  takeLatest(CropManagementTypes.LOAD_CROP_MANAGEMENT_SUCCESS, loadCropManagementSuccess)

]
