import { createActions, createReducer } from 'reduxsauce'

import { AuthenticationTypes } from '../authentication/duckAuthentication'

// Initial state
const INITIAL_STATE = {
  cropManagements: [],
  cropManagement: {},
  error: null
}

/**
 * Creating actions and types with reduxsauce.
 */
const { Types, Creators } = createActions({
  loadCropManagement: ['params', 'onSuccess', 'onError'],
  loadCropManagementSuccess: ['cropManagement', 'onSuccess'],

  cropManagementError: ['error']
})


/**
 * Reducers functions
 */
const loadCropManagementSuccess = (state = INITIAL_STATE, { cropManagement }) => ({
  ...state,
  error: INITIAL_STATE.error,
  cropManagement
})


const cropManagementError = (state = INITIAL_STATE, { error }) => ({
  ...state,
  error
})


const logout = () => ({ ...INITIAL_STATE })

/**
 * Creating reducer with Types.
 */
export default createReducer(INITIAL_STATE, {
  [Types.LOAD_CROP_MANAGEMENT_SUCCESS]: loadCropManagementSuccess,

  [Types.CROP_MANAGEMENT_ERROR]: cropManagementError,

  [AuthenticationTypes.LOGOUT]: logout
})

export {
  Types as CropManagementTypes,
  Creators as CropManagementActions
}
