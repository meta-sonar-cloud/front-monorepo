import { REACT_APP_FAKE_PAGINATION_SIZE } from 'react-native-dotenv'

import { select, call, put, takeLatest } from 'redux-saga/effects'

import isEmpty from 'lodash/isEmpty'

import {
  createAnimalPregnancyDiagnostic,
  editAnimalPregnancyDiagnostic,
  getAnimalPregnancyDiagnostic,
  getAllAnimalPregnancyDiagnostics
} from '@smartcoop/services/apis/smartcoopApi/resources/animalPregnancyDiagnostics'
import { selectCurrentAnimal } from '@smartcoop/stores/animal/selectorAnimal'

import {
  AnimalPregnancyDiagnosticsActions,
  AnimalPregnancyDiagnosticsTypes
} from './duckAnimalPregnancyDiagnostics'

function* savePregnancyDiagnostic({
  params,
  onSuccess = () => {},
  onError = () => {}
}) {
  try {
    const currentAnimal = yield select(selectCurrentAnimal)
    const { data } = yield call(
      isEmpty(params.id)
        ? createAnimalPregnancyDiagnostic
        : editAnimalPregnancyDiagnostic,
      params,
      {
        animalId: currentAnimal.id,
        pregnancyId: params.id
      }
    )
    yield call(onSuccess, data)

  } catch (err) {
    const error = err.message
    yield put(AnimalPregnancyDiagnosticsActions.pregnancyDiagnosticError(error))
    yield call(onError, error)
  }
}

function* loadPregnancyDiagnostics({
  params = {},
  onSuccess = () => {},
  onError = () => {}
}) {
  try {
    const currentAnimal = yield select(selectCurrentAnimal)

    const {
      data: { data, ...pagination }
    } = yield call(
      getAllAnimalPregnancyDiagnostics,
      {
        limit:
          process.env.REACT_APP_FAKE_PAGINATION_SIZE ||
          REACT_APP_FAKE_PAGINATION_SIZE,
        ...params
      },
      { animalId: currentAnimal.id }
    )

    yield put(
      AnimalPregnancyDiagnosticsActions.loadPregnancyDiagnosticsSuccess(
        data,
        pagination.page,
        onSuccess(data)
      )
    )
  } catch (err) {
    const error = err.message
    yield put(AnimalPregnancyDiagnosticsActions.pregnancyDiagnosticError(error))
    yield call(onError, error)
  }
}

function* loadPregnancyDiagnosticsSuccess({ onSuccess = () => {} }) {
  yield call(onSuccess)
}

function* loadPregnancyDiagnostic({
  params,
  onSuccess = () => {},
  onError = () => {}
}) {
  try {
    const currentAnimal = yield select(selectCurrentAnimal)
    const { pregnancyId } = params
    const { data } = yield call(
      getAnimalPregnancyDiagnostic,
      {},
      { animalId: currentAnimal.id, pregnancyId }
    )
    yield call(onSuccess, data)
  } catch (err) {
    const error = err.message
    yield put(AnimalPregnancyDiagnosticsActions.pregnancyDiagnosticError(error))
    yield call(onError, err)
  }
}

export default [
  takeLatest(
    AnimalPregnancyDiagnosticsTypes.LOAD_PREGNANCY_DIAGNOSTICS,
    loadPregnancyDiagnostics
  ),
  takeLatest(
    AnimalPregnancyDiagnosticsTypes.LOAD_PREGNANCY_DIAGNOSTICS_SUCCESS,
    loadPregnancyDiagnosticsSuccess
  ),

  takeLatest(
    AnimalPregnancyDiagnosticsTypes.LOAD_PREGNANCY_DIAGNOSTIC,
    loadPregnancyDiagnostic
  ),
  // takeLatest(
  //   AnimalPregnancyDiagnosticsTypes.LOAD_PREGNANCY_DIAGNOSTICS_SUCCESS,
  //   loadPregnancyDiagnosticSuccess
  // ),

  takeLatest(
    AnimalPregnancyDiagnosticsTypes.SAVE_PREGNANCY_DIAGNOSTIC,
    savePregnancyDiagnostic
  )
]
