export const selectAnimalPregnancyDiagnostics = (state) => state.animalPregnancyDiagnostics.pregnancyDiagnostics

export const selectCurrentAnimalPregnancyDiagnostic = (state) =>
  state.AnimalPregnancyDiagnostics.currentPregnancyDiagnostic
