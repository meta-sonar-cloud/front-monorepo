import { call, put, select, takeLatest } from 'redux-saga/effects'

import {
  getUserProductsBalance
} from '@smartcoop/services/apis/smartcoopApi/resources/productBalance'

import { selectCurrentOrganization } from '../organization/selectorOrganization'
import { ProductsBalanceActions, ProductsBalanceTypes } from './duckProductsBalance'

function* loadProductsBalance({ params = {}, onSuccess = () => {}, onError = () => {} }) {
  try {
    const currentOrganization = yield select(selectCurrentOrganization)

    const { data: { data: products, ...pagination } } = yield call(getUserProductsBalance, {
      limit: 10,
      ...params
    }, { organizationId: currentOrganization.id })

    yield put(ProductsBalanceActions.loadProductsBalanceSuccess(
      products,
      pagination.page,
      () => onSuccess(pagination)
    ))
  } catch (error) {
    yield put(ProductsBalanceActions.productsBalanceError(error))
    yield call(onError, error)
  }
}

function* loadProductsBalanceSuccess({ onSuccess = () => {} }) {
  yield call(onSuccess)
}

export default [
  takeLatest(ProductsBalanceTypes.LOAD_PRODUCTS_BALANCE, loadProductsBalance),
  takeLatest(ProductsBalanceTypes.LOAD_PRODUCTS_BALANCE_SUCCESS, loadProductsBalanceSuccess)
]
