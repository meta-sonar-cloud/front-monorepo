import { createActions, createReducer } from 'reduxsauce'

import { AuthenticationTypes } from '../authentication'

/* Initial State */
const INITIAL_STATE = {
  products: [],
  error: null
}

/**
 * Creating actions and types with reduxsauce.
 */
const { Types, Creators } = createActions({
  loadProductsBalance: ['params', 'onSuccess', 'onError'],
  loadProductsBalanceSuccess: ['products', 'page', 'onSuccess'],

  resetProductsBalance: [],

  productsBalanceError: ['error']
})


const loadProductsBalance = (state = INITIAL_STATE) => ({
  ...state,
  error: INITIAL_STATE.error
})

const loadProductsBalanceSuccess = (state = INITIAL_STATE, { products, page }) => ({
  ...state,
  products: page === 1 ? products : [
    ...state.products,
    ...products
  ]
})

const productsBalanceError = (state = INITIAL_STATE, { error }) => ({
  ...state,
  error
})

const resetProductsBalance = (state = INITIAL_STATE) => ({
  ...state,
  products: INITIAL_STATE.productsBalance
})

const logout = () => ({ ...INITIAL_STATE })

/**
 * Creating reducer with Types.
 */
export default createReducer(INITIAL_STATE, {

  [Types.LOAD_PRODUCTS_BALANCE]: loadProductsBalance,
  [Types.LOAD_PRODUCTS_BALANCE_SUCCESS]: loadProductsBalanceSuccess,

  [Types.RESET_PRODUCTS_BALANCE]: resetProductsBalance,

  [Types.PRODUCTS_BALANCE_ERROR]: productsBalanceError,

  [AuthenticationTypes.LOGOUT]: logout
})

export {
  Types as ProductsBalanceTypes,
  Creators as ProductsBalanceActions
}
