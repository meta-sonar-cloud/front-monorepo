export const selectWeatherStations = state => state.weatherStations.weatherStations

export const selectCurrentWeatherStation = state => state.weatherStations.currentWeatherStation
