export const selectMilkDeliveries = state => state.dairyFarm.milkDeliveries

export const selectMilkQualities = state => state.dairyFarm.milkQualities
export const selectDashboardData = state => state.dairyFarm.dashboardData

export const selectCurrentMilkDelivery = state => state.dairyFarm.currentMilkDelivery

export const selectPriceData = state => state.dairyFarm.priceData

export const selectCurrentPriceData = state => state.dairyFarm.currentPriceData

export const selectCurrentSection = state => state.dairyFarm.currentSection
