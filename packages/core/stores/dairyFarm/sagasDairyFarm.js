import { REACT_APP_FAKE_PAGINATION_SIZE } from 'react-native-dotenv'


import { select, call, put, takeLatest } from 'redux-saga/effects'

import { find, isEmpty } from 'lodash'

import {
  getMilkDeliveries as  getMilkDeliveriesService,
  getMilkQualities as  getMilkQualitiesService,
  createMilkDelivery as createMilkDeliveryService,
  createMilkQuality as createMilkQualityService,
  createDairyFarm as createDairyFarmService,
  getDairyFarm as getDairyFarmService,
  updateDairyFarm as updateDairyFarmService,
  updateMilkDelivery as updateMilkDeliveryService,
  updateMilkQuality as updateMilkQualityService,
  deleteMilkDelivery as deleteMilkDeliveryService,
  deleteMilkQuality as deleteMilkQualityService,
  getDashboardData as getDashboardDataService,
  getPriceData,
  createPriceData,
  editPriceData,
  deletePriceData as deletePriceDataService
} from '@smartcoop/services/apis/smartcoopApi/resources/dairyFarm'
import { getProperties } from '@smartcoop/services/apis/smartcoopApi/resources/property'
import { selectCurrentProperty } from '@smartcoop/stores/property/selectorProperty'

import { PropertyActions } from '../property/duckProperty'
import { DairyFarmActions, DairyFarmTypes } from './duckDairyFarm'

function* loadMilkDeliveries({ params = {}, onSuccess = () => {}, onError = () => {} }) {
  try {
    const currentProperty = yield select(selectCurrentProperty)
    const dairyFarmId = currentProperty?.dairyFarm[0].id

    const { data: { data, ...pagination } } = yield call(
      getMilkDeliveriesService,
      {
        limit: process.env.REACT_APP_FAKE_PAGINATION_SIZE || REACT_APP_FAKE_PAGINATION_SIZE,
        ...params
      },
      { dairyFarmId }
    )
    yield put(DairyFarmActions.loadMilkDeliveriesSuccess(
      data,
      pagination.page
    ))
    onSuccess(data)
  } catch (err) {
    const error = err.message
    yield put(DairyFarmActions.dairyFarmError(error))
    yield call(onError, error)
  }
}

function* loadMilkQualities({ params = {}, onSuccess = () => {}, onError = () => {} }) {
  try {
    const currentProperty = yield select(selectCurrentProperty)
    const dairyFarmId = currentProperty?.dairyFarm[0].id

    const { data: { data, ...pagination } } = yield call(
      getMilkQualitiesService,
      {
        limit: process.env.REACT_APP_FAKE_PAGINATION_SIZE || REACT_APP_FAKE_PAGINATION_SIZE,
        ...params
      },
      { dairyFarmId }
    )

    yield put(DairyFarmActions.loadMilkQualitiesSuccess(
      data,
      pagination.page
    ))
    onSuccess(pagination)
  } catch (err) {
    const error = err.message
    yield put(DairyFarmActions.dairyFarmError(error))
    yield call(onError, error)
  }
}

function* loadDashboardData({ params = {}, onSuccess = () => {}, onError = () => {} }) {
  try {
    const currentProperty = yield select(selectCurrentProperty)
    const dairyFarmId = currentProperty?.dairyFarm[0].id

    const { data } = yield call(getDashboardDataService, {
      ...params
    },
    { dairyFarmId }
    )
    yield put(DairyFarmActions.loadDashboardDataSuccess(
      data,
      () => onSuccess(data)
    ))
  } catch (err) {
    const error = err.message
    yield put(DairyFarmActions.dairyFarmError(error))
    yield call(onError, error)
  }
}

function* loadMilkDeliveriesSuccess({ onSuccess = () => {} }) {
  yield call(onSuccess)
}

function* loadDashboardDataSuccess({ onSuccess = () => {} }) {
  yield call(onSuccess)
}

function* saveMilkDelivery({ params, onSuccess = () => {}, onError = () => {} }) {
  try {
    const currentProperty = yield select(selectCurrentProperty)
    const dairyFarmId = currentProperty?.dairyFarm[0].id
    const { data } = yield call(createMilkDeliveryService, params, { dairyFarmId })
    yield call(onSuccess, data)
  } catch (error) {
    yield put(DairyFarmActions.dairyFarmError(error))
    yield call(onError, error)
  }
}

function* saveMilkQuality({ params, onSuccess = () => {}, onError = () => {} }) {
  try {
    const currentProperty = yield select(selectCurrentProperty)
    const dairyFarmId = currentProperty?.dairyFarm[0].id
    const { data } = yield call(createMilkQualityService, params, { dairyFarmId })
    yield call(onSuccess, data)
  } catch (error) {
    yield put(DairyFarmActions.dairyFarmError(error))
    yield call(onError, error)
  }
}

function* loadTambo({ onSuccess = () => {}, onError = () => {} }) {
  try {
    const currentProperty = yield select(selectCurrentProperty)
    yield call(getDairyFarmService, { propertyId: currentProperty.id })
    yield call(onSuccess)
  } catch (err) {
    yield call(onError)
  }
}

function* saveTamboType({ params, onSuccess = () => {}, onError = () => {} }) {
  try {
    const currentProperty = yield select(selectCurrentProperty)
    const service = isEmpty(currentProperty.dairyFarm) ? createDairyFarmService : updateDairyFarmService
    const { data } = yield call(service, { ...params }, { propertyId: currentProperty.id } )

    const properties = yield call(getProperties, {
      limit: process.env.REACT_APP_FAKE_PAGINATION_SIZE || REACT_APP_FAKE_PAGINATION_SIZE
    })

    const updatedCurrentProperty = find(properties.data, { id: currentProperty.id })

    yield put(PropertyActions.saveCurrentProperty(updatedCurrentProperty))
    yield call(onSuccess, data)
  } catch (error) {
    yield put(DairyFarmActions.dairyFarmError(error))
    yield call(onError, error)
  }
}

function* updateMilkDelivery({ params, onSuccess = () => {}, onError = () => {} }) {
  try {
    const currentProperty = yield select(selectCurrentProperty)
    const dairyFarmId = currentProperty?.dairyFarm[0].id
    const { data } = yield call(updateMilkDeliveryService, params, { dairyFarmId, milkDeliveryId: params.id })
    yield call(onSuccess, data)
  } catch (error) {
    yield put(DairyFarmActions.dairyFarmError(error))
    yield call(onError, error)
  }
}

function* deleteMilkDelivery({ milkDeliveryId, onSuccess = () => {}, onError = () => {} }) {
  try {
    const currentProperty = yield select(selectCurrentProperty)
    const dairyFarmId = currentProperty?.dairyFarm[0].id

    yield call(deleteMilkDeliveryService, { dairyFarmId, milkDeliveryId })
    yield call(onSuccess)
  } catch (err) {
    const error = err.message
    yield put(DairyFarmActions.dairyFarmError(error))
    yield call(onError, error)
  }
}

function* updateMilkQuality({ params, onSuccess = () => {}, onError = () => {} }) {
  try {
    const currentProperty = yield select(selectCurrentProperty)
    const dairyFarmId = currentProperty?.dairyFarm[0].id
    const { data } = yield call(updateMilkQualityService, params, { dairyFarmId, milkQualityId: params.id })
    yield call(onSuccess, data)
  } catch (error) {
    yield put(DairyFarmActions.dairyFarmError(error))
    yield call(onError, error)
  }
}

function* deleteMilkQuality({ milkQualityId, onSuccess = () => {}, onError = () => {} }) {
  try {
    const currentProperty = yield select(selectCurrentProperty)
    const dairyFarmId = currentProperty?.dairyFarm[0].id

    yield call(deleteMilkQualityService, { dairyFarmId, milkQualityId })
    yield call(onSuccess)
  } catch (err) {
    const error = err.message
    yield put(DairyFarmActions.dairyFarmError(error))
    yield call(onError, error)
  }
}

function* loadPriceData({ params = {}, onSuccess = () => {}, onError = () => {} }) {
  try {
    const currentProperty = yield select(selectCurrentProperty)
    const dairyFarmId = currentProperty?.dairyFarm[0].id

    const { data: { data, ...pagination } } = yield call(getPriceData, {
      limit: process.env.REACT_APP_FAKE_PAGINATION_SIZE || REACT_APP_FAKE_PAGINATION_SIZE,
      ...params
    },
    { dairyFarmId }
    )

    yield put(DairyFarmActions.loadPriceDataSuccess(
      data,
      pagination.page,
      () => onSuccess(data)
    ))
  } catch (err) {
    const error = err.message
    yield put(DairyFarmActions.dairyFarmError(error))
    yield call(onError, error)
  }
}

function* loadPriceDataSuccess({ onSuccess = () => {} }) {
  yield call(onSuccess)
}

function* savePriceData({ params, onSuccess = () => {}, onError = () => {} }) {
  try {
    const currentProperty = yield select(selectCurrentProperty)
    const dairyFarmId = currentProperty?.dairyFarm[0].id
    const { data } = yield call(createPriceData, params, { dairyFarmId })
    yield call(onSuccess, data)
  } catch (error) {
    yield put(DairyFarmActions.dairyFarmError(error))
    yield call(onError, error)
  }
}

function* updatePriceData({ params, priceDataId, onSuccess = () => {}, onError = () => {} }) {
  try {
    const currentProperty = yield select(selectCurrentProperty)
    const dairyFarmId = currentProperty?.dairyFarm[0].id
    const { data } = yield call(editPriceData, { ...params, dairyFarmId }, { dairyFarmId, priceDataId })
    yield call(onSuccess, data)
  } catch (error) {
    yield put(DairyFarmActions.dairyFarmError(error))
    yield call(onError, error)
  }
}

function* deletePriceData({ priceDataId, onSuccess = () => {}, onError = () => {} }) {
  try {
    const currentProperty = yield select(selectCurrentProperty)
    const dairyFarmId = currentProperty?.dairyFarm[0].id

    yield call(deletePriceDataService, { dairyFarmId, priceDataId })
    yield call(onSuccess)
  } catch (err) {
    const error = err.message
    yield put(DairyFarmActions.dairyFarmError(error))
    yield call(onError, error)
  }
}

export default [
  takeLatest(DairyFarmTypes.LOAD_DASHBOARD_DATA, loadDashboardData),
  takeLatest(DairyFarmTypes.LOAD_DASHBOARD_DATA_SUCCESS, loadDashboardDataSuccess),

  takeLatest(DairyFarmTypes.LOAD_MILK_DELIVERIES, loadMilkDeliveries),
  takeLatest(DairyFarmTypes.LOAD_MILK_DELIVERIES_SUCCESS, loadMilkDeliveriesSuccess),

  takeLatest(DairyFarmTypes.LOAD_MILK_QUALITIES, loadMilkQualities),


  takeLatest(DairyFarmTypes.SAVE_MILK_QUALITY, saveMilkQuality),
  takeLatest(DairyFarmTypes.UPDATE_MILK_QUALITY, updateMilkQuality),
  takeLatest(DairyFarmTypes.DELETE_MILK_QUALITY, deleteMilkQuality),

  takeLatest(DairyFarmTypes.SAVE_MILK_DELIVERY, saveMilkDelivery),
  takeLatest(DairyFarmTypes.UPDATE_MILK_DELIVERY, updateMilkDelivery),
  takeLatest(DairyFarmTypes.DELETE_MILK_DELIVERY, deleteMilkDelivery),

  takeLatest(DairyFarmTypes.LOAD_PRICE_DATA, loadPriceData),
  takeLatest(DairyFarmTypes.LOAD_PRICE_DATA_SUCCESS, loadPriceDataSuccess),

  takeLatest(DairyFarmTypes.SAVE_TAMBO_TYPE, saveTamboType),
  takeLatest(DairyFarmTypes.LOAD_TAMBO, loadTambo),

  takeLatest(DairyFarmTypes.SAVE_PRICE_DATA, savePriceData),
  takeLatest(DairyFarmTypes.UPDATE_PRICE_DATA, updatePriceData),
  takeLatest(DairyFarmTypes.DELETE_PRICE_DATA, deletePriceData)
]
