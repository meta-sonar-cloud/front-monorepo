export const selectSalesOrders = state => state.salesOrders.orders
export const selectSalesOrdersProducts = state => state.salesOrders.products
export const selectSalesOrdersBatches = state => state.salesOrders.batches
export const selectSalesOrdersSettlementDates = state => state.salesOrders.settlementDates
