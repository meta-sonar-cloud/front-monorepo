export const selectBarters = state => state.barter.barters
export const selectBarterPackages = state => state.barter.packages
