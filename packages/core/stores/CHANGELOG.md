# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [0.18.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.17.0...v0.18.0) (2021-04-19)


### Features

* merge develop ([65962b2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/65962b2d2d1752343c77861b94a416495ea3cb77))
* resolve [#1714](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1714) [#1756](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1756) [#1757](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1757) [#1758](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1758) ([ecfb39f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ecfb39f2f1e64696f55ed5a93f3db260fbf062b0))
* resolve issue ([60f477a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/60f477ad99649dc0114ce43732b55010e05ccbf5))
* resolve some issues ([0e7d7ce](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0e7d7ce1738f55d75ddd3907cb5ca197b5885e1a))
* wip ([a20c949](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a20c949da809ca74018f6f2ff36d63d6fa877bcc))
* **documentos#1744:** changes get of calving ([60d0cd3](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/60d0cd351cf40aff67d38cf064d7309a50329193)), closes [documentos#1744](http://gitlab.meta.com.br/documentos/issues/1744)
* wip ([ed88867](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ed88867dbcb9af5b51eff1164ff708db209a3b95))
* wip [#2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/2) ([417c840](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/417c840a8e8ca542911be4172234a862bf4b55b6))
* **documentos#1737:** new translations ([ac7b59e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ac7b59e7884b7fba0e6c5e9e79db514debaaa0e1)), closes [documentos#1737](http://gitlab.meta.com.br/documentos/issues/1737)





# [0.17.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.16.0...v0.17.0) (2021-04-15)


### Bug Fixes

* **auth:** logout loop ([86da8da](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/86da8da6ef3ef5320bec3e04694c3d497d288f1c))


### Features

* resolve [#1735](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1735) ([cda6387](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/cda6387c44735527fe95bf60838cc9098715d96e))
* **documentos#1674:** activetab in redux ([e4735ea](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e4735ea1b8d55cbc4d29ba9dd9d307fbff4435df)), closes [documentos#1674](http://gitlab.meta.com.br/documentos/issues/1674)
* **documentos#1674:** get last insemination date in redux ([6b38a04](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/6b38a043afa7848488b719d42bed1f78e38619c6)), closes [documentos#1674](http://gitlab.meta.com.br/documentos/issues/1674)
* **documentos#1715:** fix internal/external in mobile ([203baa6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/203baa688c8f675dbc3bbc21dc2b2f711917f85f))
* **documentos#1715:** renaming things and logic changes ([7b0ad3c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7b0ad3c5f4fd006fd458cfb31d4817270f4d561b)), closes [documentos#1715](http://gitlab.meta.com.br/documentos/issues/1715)
* resolve [#1694](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1694) ([56458c3](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/56458c39a36852a4e8021f6de384b985181fffa0))





# [0.16.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.15.0...v0.16.0) (2021-04-13)


### Bug Fixes

* **auth:** fixed first refresh token and added free actions ([fc2eea9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/fc2eea987612d3f4d4c783bb59cb3df82cc6a4d1))
* **documentos#1549:** fixed web create password api comunication steps ([65880c9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/65880c9ef3ecbaf8ef34a687d547b612e7f5a300))
* **documentos#1551:** keep user data until firebase logout ([bb1ad91](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/bb1ad91cf86ebebf811a0b3c27ad674efc7ee60c)), closes [documentos#1551](http://gitlab.meta.com.br/documentos/issues/1551)
* **documentos#1642:** correção ativação ([2b25b08](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2b25b088c76585b50f59c0be3abe893d31bf6310)), closes [documentos#1642](http://gitlab.meta.com.br/documentos/issues/1642)


### Features

* **#1675 #1676:** adjustment mobile, web, and add new feature ([94afa5b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/94afa5bafdb99f6de2d5f704b875d560ef384be3)), closes [#1675](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1675) [#1676](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1676) [#546](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/546)
* wip ([892dd6c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/892dd6ce5a4659ed7e9d0093f4161605065b7f0a))
* **#1630 #1631:** create list pregnancy diagnostics ([d46d5d7](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d46d5d767b1ca9fd6cfb48775ebf9a10ce55a274)), closes [#1630](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1630) [#1631](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1631) [#521](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/521)
* **documentos#1650:** calving in mobile ([f1218c7](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f1218c7a11e6444eee6c4210889ba2eba91c1629)), closes [documentos#1650](http://gitlab.meta.com.br/documentos/issues/1650)
* changes i forgot ([c9cc2bf](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c9cc2bf265c2166735caeffa945c768c317f0174))
* merge com a develop ([702e143](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/702e1434c3eab83403ccbd51d34a6340a05613b9))
* wip ([e0606ab](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e0606ab15f17990390f972120a71bef8ffb7304e))
* wip ([8a64b05](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8a64b05a75f08e112e58615cb42c7edf15303438))
* **#1484:** create store weather station ([8c38c7c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8c38c7c1bb4b1c1d834d4d7d6509b97095eb8492)), closes [#1484](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1484) [#484](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/484)
* **#1484:** integration with backend weather stations details ([c776b4e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c776b4eb5720ad698dc40ce90bc003c42b18027a)), closes [#1484](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1484) [#484](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/484)
* **#1484:** map station list  integration api, web and mobile ([828c5e9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/828c5e9871594d5a6ad6623348cfcaf4cebea8c2)), closes [#1484](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1484) [#484](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/484)
* **#1601:** adjustment weather stations and validation show stations ([6d17d60](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/6d17d60b76cee650c066156cd3568e92a6331e4f)), closes [#1601](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1601) [#510](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/510)
* **documentos#1032:** added permission validator ([9b65368](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9b6536896ee9eea1c56af979b18d3850f4e25c40)), closes [documentos#1032](http://gitlab.meta.com.br/documentos/issues/1032)
* **documentos#1316:** created animal pregnancy store ([86b6739](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/86b6739884b91f0ef6892a5757e15a423b7f63c9)), closes [documentos#1316](http://gitlab.meta.com.br/documentos/issues/1316)
* **documentos#1325:** created pev crud for mobile ([f0ef8c0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f0ef8c00ffd2a45ad9b3a34ce5fee71fc1eced50)), closes [documentos#1325](http://gitlab.meta.com.br/documentos/issues/1325)
* **documentos#1325:** created PEV on web ([d0d6dda](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d0d6ddae85eeab6cd3b022ac0cf40c8db8aa7d06)), closes [documentos#1325](http://gitlab.meta.com.br/documentos/issues/1325)
* **documentos#1494:** added loadfields after deleteField action ([e128118](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e128118afd7029a277feea31e287aa291954296b)), closes [documentos#1494](http://gitlab.meta.com.br/documentos/issues/1494)
* **documentos#1579:** creates dropdown menu ([3726ef9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3726ef98f33b057b5f269598894e2cc7174b512c)), closes [documentos#1579](http://gitlab.meta.com.br/documentos/issues/1579)
* **documentos#1579:** filters in web ([6494181](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/64941810de441edc856318583f5776dd41749431)), closes [documentos#1579](http://gitlab.meta.com.br/documentos/issues/1579)
* wip ([a1daf61](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a1daf6149af8d967fff4afe4009a7f2a116d8917))
* **documentos#1316:** created animal pregnancy store ([71300db](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/71300dbb609ea58cd69749f6e05dd55b1198a81d)), closes [documentos#1316](http://gitlab.meta.com.br/documentos/issues/1316)
* **documentos#1490:** wip LotList ([ebcbf71](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ebcbf71bdfaa03e8f18c82bb005479bba921932d)), closes [documentos#1490](http://gitlab.meta.com.br/documentos/issues/1490)
* **documentos#1495:** lots in mobile ([3fdcfbe](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3fdcfbeb9b601a1672a8d10cb1dabfdbccec2022)), closes [documentos#1495](http://gitlab.meta.com.br/documentos/issues/1495)
* **documentos#1495:** modals, saga and item ([a04f400](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a04f400ff9eb8feddfbef787e6b8fd7121985631)), closes [documentos#1495](http://gitlab.meta.com.br/documentos/issues/1495)
* **documentos#1579:** creates dropdown menu ([3cd3342](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3cd334246c3e3ec8b6aedac15559ee82d209149b)), closes [documentos#1579](http://gitlab.meta.com.br/documentos/issues/1579)
* **documentos#1579:** insemination mobile ([767d69b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/767d69b03b2e183e6a70f74b4bf648f21bedea84)), closes [documentos#1579](http://gitlab.meta.com.br/documentos/issues/1579)
* **documentos#1579:** insemination mobile ([cd499e9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/cd499e9e73af560aaee00585a7d5a1defc82ea07)), closes [documentos#1579](http://gitlab.meta.com.br/documentos/issues/1579)
* **documentos#1579:** mobile insemination ([1bf225d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1bf225d6f5ffbafb10226bf7c5c77b13a03ed58e)), closes [documentos#1579](http://gitlab.meta.com.br/documentos/issues/1579)
* **documentos#1579:** mobile insemination ([10c2ff2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/10c2ff2171498fa004d5fc58ac3ba9266347753a)), closes [documentos#1579](http://gitlab.meta.com.br/documentos/issues/1579)
* **documents#1490:** wip ([284a0ba](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/284a0ba9bb54e95ccf82a92a28bb307afbc66e25)), closes [documents#1490](http://gitlab.meta.com.br/documents/issues/1490)







**Note:** Version bump only for package @smartcoop/stores





# [0.15.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.12.0-alpha.1...v0.15.0) (2021-03-23)


### Bug Fixes

* **#1217:** add onSuccess in save growingseason ([2d3a97f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2d3a97f55f91984b7ebbf53315afa2aca7751ff0)), closes [#1217](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1217)
* **documenots#1292:** fixed securities movement saga ([75eb7ea](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/75eb7ea431a8edd9f97266d5576c91de5480438f))


### Features

* resolve [#1450](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1450) (WEB) ([9812c7b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9812c7be3076f4ff48599fcb8ecf76f3ebd3b7d7))
* **#1288:** adjustment select organization ([2dbbc9c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2dbbc9c4234ad1a408da856e1c2deeccc814f993)), closes [#1288](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1288) [#395](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/395)
* **#1414:** remove console log and adjustment editing property ([b83b5db](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b83b5dbbc6715b4ceb0ba8d90fd25d7904a4bca9)), closes [#1414](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1414) [#454](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/454)
* **documenots#1292:** added new action in securities movement store ([8a78097](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8a7809774c7b0900ec1d7dfce0feca16be02404b)), closes [documenots#1292](http://gitlab.meta.com.br/documenots/issues/1292)
* **documentos#1116:** tambo for mobile ([f4d650f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f4d650f18602702f4cfccdf32758336da2041ca0)), closes [documentos#1116](http://gitlab.meta.com.br/documentos/issues/1116)
* **documentos#1232:** adjust some selectors ([a4b4dc5](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a4b4dc5c5a2fb9756672fbe8087380d55a888dbc)), closes [documentos#1232](http://gitlab.meta.com.br/documentos/issues/1232)
* **documentos#1232:** removes consle log ([f4dea86](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f4dea86a27885b1c161cde0fd2cd59fa4eac5258)), closes [documentos#1232](http://gitlab.meta.com.br/documentos/issues/1232)
* **documentos#1233:** permissions tweaks ([c3d0da0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c3d0da0c6009cdb75d84f0722bc501d948781afe)), closes [documentos#1233](http://gitlab.meta.com.br/documentos/issues/1233)
* **documentos#1240:** add updateSignature to organzationStore ([283be60](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/283be607a59b92d1c6241dad577db4d557d52de7)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** generates new eletronic signature only if v. password ([c79572a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c79572af40f26a81eb3f1cc5aa6e07e5291b544c)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** updateSignature calling correctyl ([15b1a4e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/15b1a4e5a824249a89f0e087861d920bda53b20e)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1290:** duck, saga and selector of terms ([477827d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/477827df5645ecc3c5c38fdaf6ac2619216854a6)), closes [documentos#1290](http://gitlab.meta.com.br/documentos/issues/1290)
* **documentos#1290:** fix selector ([9d5afed](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9d5afed4989b70e0fa352e4c59dc5c01f868dc5a))
* **documentos#1290:** fixes ([93e2f6f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/93e2f6f1ca671d12b34b2f21fbd7c531bb4a0ef5)), closes [documentos#1290](http://gitlab.meta.com.br/documentos/issues/1290)
* **documentos#1305:** removes birthday from onboarding ([5eda681](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/5eda6815388bc7289d1d33d1e6896faca4e4aa68)), closes [documentos#1305](http://gitlab.meta.com.br/documentos/issues/1305)
* **documentos#1323:** fixes technician in web ([3ea9b64](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3ea9b6409460f5af500e67cccc1c50e11ab543b7))
* **documentos#1323:** removes clg ([81c467a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/81c467a84aef2cd5b61e17a5753d3d09d23fb34e)), closes [documentos#1323](http://gitlab.meta.com.br/documentos/issues/1323)
* **documentos#1323:** removes dprecated contains, using includes now ([e95f2b3](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e95f2b34d755a42acc08f86493d032c03cdd5cce)), closes [documentos#1323](http://gitlab.meta.com.br/documentos/issues/1323)
* **documentos#1323:** updates slugs ([55b8b86](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/55b8b86762cc17d362b39566daac61ad0a92dd55)), closes [documentos#1323](http://gitlab.meta.com.br/documentos/issues/1323)
* **documentos#1332:** changes in permissions ([5872eab](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/5872eabcbdf88d56064c26edd5552fa9969d12c6)), closes [documentos#1332](http://gitlab.meta.com.br/documentos/issues/1332)
* **documentos#1332:** updates selectors ([120e9d2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/120e9d289ee170214d0143c5f948125f2d9ffc8f)), closes [documentos#1332](http://gitlab.meta.com.br/documentos/issues/1332)
* **documentos#1357:** fixes permission error ([caed226](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/caed22667aa0259d611894310d4afa266634c2d5))
* **documentos#1374:** firebase changes ([df6a585](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/df6a585a34570777c468c98c134cd8fbd855601c)), closes [documentos#1374](http://gitlab.meta.com.br/documentos/issues/1374)
* **documentos#1377:** commercialization fix ([978a707](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/978a70771c50cc5ede07b13219a3d15942e31689)), closes [documentos#1377](http://gitlab.meta.com.br/documentos/issues/1377)
* **documentos#1436:** changes in commercialization ([f4edf6b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f4edf6b977ef4c7a8fe514f844097ab2703e1572)), closes [documentos#1436](http://gitlab.meta.com.br/documentos/issues/1436)
* **documentos#1437:** removes clg ([a9344c4](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a9344c43517ddd93a3ec16756a32d92ac96fb044)), closes [documentos#1437](http://gitlab.meta.com.br/documentos/issues/1437)
* **documentos#1448:** check if access token exists b4 refresh ([c22543c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c22543cb02ad4a22e9d319d16b04faf31f30e9b1)), closes [documentos#1448](http://gitlab.meta.com.br/documentos/issues/1448)
* created web administration module ([458a743](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/458a743b04049e374da04acd96d94cc68c66a9f0))
* resolve [#1285](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1285) ([c0b4d91](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c0b4d91ccdb7ff7adcaaf87175f1c32fe17b1c48))
* resolve [#1291](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1291) ([0580b9c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0580b9cf4f5435a78c9ef27a39ca6cf4cbdf9899))
* **documentso#1332:** changes in auth to align with back-end ([e57dda8](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e57dda85e30915d07d74052ad7fd00c592f3a739)), closes [documentso#1332](http://gitlab.meta.com.br/documentso/issues/1332)
* **doucmentos#1232:** shows all user modules in choosemodulescreen ([8405ea3](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8405ea36080c4bed643150f5632ec666fe0e51e5)), closes [doucmentos#1232](http://gitlab.meta.com.br/doucmentos/issues/1232)
* merge develop ([2731148](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2731148d7380e3ab5c8ec38d198b1773467e4364))
* wip ([b3b6eb5](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b3b6eb57ca79dc596447e6b748d58c3b74087f14))
* wip ([2cf8dd6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2cf8dd6979ab9a9ee338858a586f2587ca18899e))
* wip ([16b3eef](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/16b3eef778dda67fd818f50aaaefb2eb54901e34))





# [0.15.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.12.0-alpha.1...v0.15.0) (2021-03-23)


### Bug Fixes

* **#1217:** add onSuccess in save growingseason ([2d3a97f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2d3a97f55f91984b7ebbf53315afa2aca7751ff0)), closes [#1217](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1217)
* **documenots#1292:** fixed securities movement saga ([75eb7ea](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/75eb7ea431a8edd9f97266d5576c91de5480438f))


### Features

* resolve [#1450](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1450) (WEB) ([9812c7b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9812c7be3076f4ff48599fcb8ecf76f3ebd3b7d7))
* **#1288:** adjustment select organization ([2dbbc9c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2dbbc9c4234ad1a408da856e1c2deeccc814f993)), closes [#1288](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1288) [#395](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/395)
* **#1414:** remove console log and adjustment editing property ([b83b5db](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b83b5dbbc6715b4ceb0ba8d90fd25d7904a4bca9)), closes [#1414](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1414) [#454](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/454)
* **documenots#1292:** added new action in securities movement store ([8a78097](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8a7809774c7b0900ec1d7dfce0feca16be02404b)), closes [documenots#1292](http://gitlab.meta.com.br/documenots/issues/1292)
* **documentos#1116:** tambo for mobile ([f4d650f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f4d650f18602702f4cfccdf32758336da2041ca0)), closes [documentos#1116](http://gitlab.meta.com.br/documentos/issues/1116)
* **documentos#1232:** adjust some selectors ([a4b4dc5](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a4b4dc5c5a2fb9756672fbe8087380d55a888dbc)), closes [documentos#1232](http://gitlab.meta.com.br/documentos/issues/1232)
* **documentos#1232:** removes consle log ([f4dea86](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f4dea86a27885b1c161cde0fd2cd59fa4eac5258)), closes [documentos#1232](http://gitlab.meta.com.br/documentos/issues/1232)
* **documentos#1233:** permissions tweaks ([c3d0da0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c3d0da0c6009cdb75d84f0722bc501d948781afe)), closes [documentos#1233](http://gitlab.meta.com.br/documentos/issues/1233)
* **documentos#1240:** add updateSignature to organzationStore ([283be60](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/283be607a59b92d1c6241dad577db4d557d52de7)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** generates new eletronic signature only if v. password ([c79572a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c79572af40f26a81eb3f1cc5aa6e07e5291b544c)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** updateSignature calling correctyl ([15b1a4e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/15b1a4e5a824249a89f0e087861d920bda53b20e)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1290:** duck, saga and selector of terms ([477827d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/477827df5645ecc3c5c38fdaf6ac2619216854a6)), closes [documentos#1290](http://gitlab.meta.com.br/documentos/issues/1290)
* **documentos#1290:** fix selector ([9d5afed](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9d5afed4989b70e0fa352e4c59dc5c01f868dc5a))
* **documentos#1290:** fixes ([93e2f6f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/93e2f6f1ca671d12b34b2f21fbd7c531bb4a0ef5)), closes [documentos#1290](http://gitlab.meta.com.br/documentos/issues/1290)
* **documentos#1305:** removes birthday from onboarding ([5eda681](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/5eda6815388bc7289d1d33d1e6896faca4e4aa68)), closes [documentos#1305](http://gitlab.meta.com.br/documentos/issues/1305)
* **documentos#1323:** fixes technician in web ([3ea9b64](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3ea9b6409460f5af500e67cccc1c50e11ab543b7))
* **documentos#1323:** removes clg ([81c467a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/81c467a84aef2cd5b61e17a5753d3d09d23fb34e)), closes [documentos#1323](http://gitlab.meta.com.br/documentos/issues/1323)
* **documentos#1323:** removes dprecated contains, using includes now ([e95f2b3](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e95f2b34d755a42acc08f86493d032c03cdd5cce)), closes [documentos#1323](http://gitlab.meta.com.br/documentos/issues/1323)
* **documentos#1323:** updates slugs ([55b8b86](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/55b8b86762cc17d362b39566daac61ad0a92dd55)), closes [documentos#1323](http://gitlab.meta.com.br/documentos/issues/1323)
* **documentos#1332:** changes in permissions ([5872eab](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/5872eabcbdf88d56064c26edd5552fa9969d12c6)), closes [documentos#1332](http://gitlab.meta.com.br/documentos/issues/1332)
* **documentos#1332:** updates selectors ([120e9d2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/120e9d289ee170214d0143c5f948125f2d9ffc8f)), closes [documentos#1332](http://gitlab.meta.com.br/documentos/issues/1332)
* **documentos#1357:** fixes permission error ([caed226](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/caed22667aa0259d611894310d4afa266634c2d5))
* **documentos#1374:** firebase changes ([df6a585](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/df6a585a34570777c468c98c134cd8fbd855601c)), closes [documentos#1374](http://gitlab.meta.com.br/documentos/issues/1374)
* **documentos#1377:** commercialization fix ([978a707](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/978a70771c50cc5ede07b13219a3d15942e31689)), closes [documentos#1377](http://gitlab.meta.com.br/documentos/issues/1377)
* **documentos#1436:** changes in commercialization ([f4edf6b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f4edf6b977ef4c7a8fe514f844097ab2703e1572)), closes [documentos#1436](http://gitlab.meta.com.br/documentos/issues/1436)
* **documentos#1437:** removes clg ([a9344c4](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a9344c43517ddd93a3ec16756a32d92ac96fb044)), closes [documentos#1437](http://gitlab.meta.com.br/documentos/issues/1437)
* **documentos#1448:** check if access token exists b4 refresh ([c22543c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c22543cb02ad4a22e9d319d16b04faf31f30e9b1)), closes [documentos#1448](http://gitlab.meta.com.br/documentos/issues/1448)
* created web administration module ([458a743](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/458a743b04049e374da04acd96d94cc68c66a9f0))
* resolve [#1285](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1285) ([c0b4d91](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c0b4d91ccdb7ff7adcaaf87175f1c32fe17b1c48))
* resolve [#1291](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1291) ([0580b9c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0580b9cf4f5435a78c9ef27a39ca6cf4cbdf9899))
* **documentso#1332:** changes in auth to align with back-end ([e57dda8](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e57dda85e30915d07d74052ad7fd00c592f3a739)), closes [documentso#1332](http://gitlab.meta.com.br/documentso/issues/1332)
* **doucmentos#1232:** shows all user modules in choosemodulescreen ([8405ea3](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8405ea36080c4bed643150f5632ec666fe0e51e5)), closes [doucmentos#1232](http://gitlab.meta.com.br/doucmentos/issues/1232)
* merge develop ([2731148](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2731148d7380e3ab5c8ec38d198b1773467e4364))
* wip ([b3b6eb5](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b3b6eb57ca79dc596447e6b748d58c3b74087f14))
* wip ([2cf8dd6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2cf8dd6979ab9a9ee338858a586f2587ca18899e))
* wip ([16b3eef](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/16b3eef778dda67fd818f50aaaefb2eb54901e34))





# [0.15.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.12.0-alpha.1...v0.15.0) (2021-03-23)


### Bug Fixes

* **#1217:** add onSuccess in save growingseason ([2d3a97f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2d3a97f55f91984b7ebbf53315afa2aca7751ff0)), closes [#1217](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1217)
* **documenots#1292:** fixed securities movement saga ([75eb7ea](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/75eb7ea431a8edd9f97266d5576c91de5480438f))


### Features

* resolve [#1450](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1450) (WEB) ([9812c7b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9812c7be3076f4ff48599fcb8ecf76f3ebd3b7d7))
* **#1288:** adjustment select organization ([2dbbc9c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2dbbc9c4234ad1a408da856e1c2deeccc814f993)), closes [#1288](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1288) [#395](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/395)
* **#1414:** remove console log and adjustment editing property ([b83b5db](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b83b5dbbc6715b4ceb0ba8d90fd25d7904a4bca9)), closes [#1414](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1414) [#454](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/454)
* **documenots#1292:** added new action in securities movement store ([8a78097](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8a7809774c7b0900ec1d7dfce0feca16be02404b)), closes [documenots#1292](http://gitlab.meta.com.br/documenots/issues/1292)
* **documentos#1116:** tambo for mobile ([f4d650f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f4d650f18602702f4cfccdf32758336da2041ca0)), closes [documentos#1116](http://gitlab.meta.com.br/documentos/issues/1116)
* **documentos#1232:** adjust some selectors ([a4b4dc5](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a4b4dc5c5a2fb9756672fbe8087380d55a888dbc)), closes [documentos#1232](http://gitlab.meta.com.br/documentos/issues/1232)
* **documentos#1232:** removes consle log ([f4dea86](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f4dea86a27885b1c161cde0fd2cd59fa4eac5258)), closes [documentos#1232](http://gitlab.meta.com.br/documentos/issues/1232)
* **documentos#1233:** permissions tweaks ([c3d0da0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c3d0da0c6009cdb75d84f0722bc501d948781afe)), closes [documentos#1233](http://gitlab.meta.com.br/documentos/issues/1233)
* **documentos#1240:** add updateSignature to organzationStore ([283be60](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/283be607a59b92d1c6241dad577db4d557d52de7)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** generates new eletronic signature only if v. password ([c79572a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c79572af40f26a81eb3f1cc5aa6e07e5291b544c)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** updateSignature calling correctyl ([15b1a4e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/15b1a4e5a824249a89f0e087861d920bda53b20e)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1290:** duck, saga and selector of terms ([477827d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/477827df5645ecc3c5c38fdaf6ac2619216854a6)), closes [documentos#1290](http://gitlab.meta.com.br/documentos/issues/1290)
* **documentos#1290:** fix selector ([9d5afed](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9d5afed4989b70e0fa352e4c59dc5c01f868dc5a))
* **documentos#1290:** fixes ([93e2f6f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/93e2f6f1ca671d12b34b2f21fbd7c531bb4a0ef5)), closes [documentos#1290](http://gitlab.meta.com.br/documentos/issues/1290)
* **documentos#1305:** removes birthday from onboarding ([5eda681](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/5eda6815388bc7289d1d33d1e6896faca4e4aa68)), closes [documentos#1305](http://gitlab.meta.com.br/documentos/issues/1305)
* **documentos#1323:** fixes technician in web ([3ea9b64](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3ea9b6409460f5af500e67cccc1c50e11ab543b7))
* **documentos#1323:** removes clg ([81c467a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/81c467a84aef2cd5b61e17a5753d3d09d23fb34e)), closes [documentos#1323](http://gitlab.meta.com.br/documentos/issues/1323)
* **documentos#1323:** removes dprecated contains, using includes now ([e95f2b3](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e95f2b34d755a42acc08f86493d032c03cdd5cce)), closes [documentos#1323](http://gitlab.meta.com.br/documentos/issues/1323)
* **documentos#1323:** updates slugs ([55b8b86](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/55b8b86762cc17d362b39566daac61ad0a92dd55)), closes [documentos#1323](http://gitlab.meta.com.br/documentos/issues/1323)
* **documentos#1332:** changes in permissions ([5872eab](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/5872eabcbdf88d56064c26edd5552fa9969d12c6)), closes [documentos#1332](http://gitlab.meta.com.br/documentos/issues/1332)
* **documentos#1332:** updates selectors ([120e9d2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/120e9d289ee170214d0143c5f948125f2d9ffc8f)), closes [documentos#1332](http://gitlab.meta.com.br/documentos/issues/1332)
* **documentos#1357:** fixes permission error ([caed226](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/caed22667aa0259d611894310d4afa266634c2d5))
* **documentos#1374:** firebase changes ([df6a585](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/df6a585a34570777c468c98c134cd8fbd855601c)), closes [documentos#1374](http://gitlab.meta.com.br/documentos/issues/1374)
* **documentos#1377:** commercialization fix ([978a707](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/978a70771c50cc5ede07b13219a3d15942e31689)), closes [documentos#1377](http://gitlab.meta.com.br/documentos/issues/1377)
* **documentos#1436:** changes in commercialization ([f4edf6b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f4edf6b977ef4c7a8fe514f844097ab2703e1572)), closes [documentos#1436](http://gitlab.meta.com.br/documentos/issues/1436)
* **documentos#1437:** removes clg ([a9344c4](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a9344c43517ddd93a3ec16756a32d92ac96fb044)), closes [documentos#1437](http://gitlab.meta.com.br/documentos/issues/1437)
* **documentos#1448:** check if access token exists b4 refresh ([c22543c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c22543cb02ad4a22e9d319d16b04faf31f30e9b1)), closes [documentos#1448](http://gitlab.meta.com.br/documentos/issues/1448)
* created web administration module ([458a743](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/458a743b04049e374da04acd96d94cc68c66a9f0))
* resolve [#1285](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1285) ([c0b4d91](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c0b4d91ccdb7ff7adcaaf87175f1c32fe17b1c48))
* resolve [#1291](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1291) ([0580b9c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0580b9cf4f5435a78c9ef27a39ca6cf4cbdf9899))
* **documentso#1332:** changes in auth to align with back-end ([e57dda8](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e57dda85e30915d07d74052ad7fd00c592f3a739)), closes [documentso#1332](http://gitlab.meta.com.br/documentso/issues/1332)
* **doucmentos#1232:** shows all user modules in choosemodulescreen ([8405ea3](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8405ea36080c4bed643150f5632ec666fe0e51e5)), closes [doucmentos#1232](http://gitlab.meta.com.br/doucmentos/issues/1232)
* merge develop ([2731148](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2731148d7380e3ab5c8ec38d198b1773467e4364))
* wip ([b3b6eb5](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b3b6eb57ca79dc596447e6b748d58c3b74087f14))
* wip ([2cf8dd6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2cf8dd6979ab9a9ee338858a586f2587ca18899e))
* wip ([16b3eef](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/16b3eef778dda67fd818f50aaaefb2eb54901e34))





# [0.14.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.12.0-alpha.1...v0.14.0) (2021-03-23)


### Bug Fixes

* **#1217:** add onSuccess in save growingseason ([2d3a97f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2d3a97f55f91984b7ebbf53315afa2aca7751ff0)), closes [#1217](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1217)
* **documenots#1292:** fixed securities movement saga ([75eb7ea](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/75eb7ea431a8edd9f97266d5576c91de5480438f))


### Features

* resolve [#1450](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1450) (WEB) ([9812c7b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9812c7be3076f4ff48599fcb8ecf76f3ebd3b7d7))
* **#1288:** adjustment select organization ([2dbbc9c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2dbbc9c4234ad1a408da856e1c2deeccc814f993)), closes [#1288](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1288) [#395](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/395)
* **#1414:** remove console log and adjustment editing property ([b83b5db](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b83b5dbbc6715b4ceb0ba8d90fd25d7904a4bca9)), closes [#1414](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1414) [#454](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/454)
* **documenots#1292:** added new action in securities movement store ([8a78097](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8a7809774c7b0900ec1d7dfce0feca16be02404b)), closes [documenots#1292](http://gitlab.meta.com.br/documenots/issues/1292)
* **documentos#1116:** tambo for mobile ([f4d650f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f4d650f18602702f4cfccdf32758336da2041ca0)), closes [documentos#1116](http://gitlab.meta.com.br/documentos/issues/1116)
* **documentos#1232:** adjust some selectors ([a4b4dc5](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a4b4dc5c5a2fb9756672fbe8087380d55a888dbc)), closes [documentos#1232](http://gitlab.meta.com.br/documentos/issues/1232)
* **documentos#1232:** removes consle log ([f4dea86](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f4dea86a27885b1c161cde0fd2cd59fa4eac5258)), closes [documentos#1232](http://gitlab.meta.com.br/documentos/issues/1232)
* **documentos#1233:** permissions tweaks ([c3d0da0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c3d0da0c6009cdb75d84f0722bc501d948781afe)), closes [documentos#1233](http://gitlab.meta.com.br/documentos/issues/1233)
* **documentos#1240:** add updateSignature to organzationStore ([283be60](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/283be607a59b92d1c6241dad577db4d557d52de7)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** generates new eletronic signature only if v. password ([c79572a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c79572af40f26a81eb3f1cc5aa6e07e5291b544c)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** updateSignature calling correctyl ([15b1a4e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/15b1a4e5a824249a89f0e087861d920bda53b20e)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1290:** duck, saga and selector of terms ([477827d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/477827df5645ecc3c5c38fdaf6ac2619216854a6)), closes [documentos#1290](http://gitlab.meta.com.br/documentos/issues/1290)
* **documentos#1290:** fix selector ([9d5afed](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9d5afed4989b70e0fa352e4c59dc5c01f868dc5a))
* **documentos#1290:** fixes ([93e2f6f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/93e2f6f1ca671d12b34b2f21fbd7c531bb4a0ef5)), closes [documentos#1290](http://gitlab.meta.com.br/documentos/issues/1290)
* **documentos#1305:** removes birthday from onboarding ([5eda681](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/5eda6815388bc7289d1d33d1e6896faca4e4aa68)), closes [documentos#1305](http://gitlab.meta.com.br/documentos/issues/1305)
* **documentos#1323:** fixes technician in web ([3ea9b64](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3ea9b6409460f5af500e67cccc1c50e11ab543b7))
* **documentos#1323:** removes clg ([81c467a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/81c467a84aef2cd5b61e17a5753d3d09d23fb34e)), closes [documentos#1323](http://gitlab.meta.com.br/documentos/issues/1323)
* **documentos#1323:** removes dprecated contains, using includes now ([e95f2b3](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e95f2b34d755a42acc08f86493d032c03cdd5cce)), closes [documentos#1323](http://gitlab.meta.com.br/documentos/issues/1323)
* **documentos#1323:** updates slugs ([55b8b86](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/55b8b86762cc17d362b39566daac61ad0a92dd55)), closes [documentos#1323](http://gitlab.meta.com.br/documentos/issues/1323)
* **documentos#1332:** changes in permissions ([5872eab](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/5872eabcbdf88d56064c26edd5552fa9969d12c6)), closes [documentos#1332](http://gitlab.meta.com.br/documentos/issues/1332)
* **documentos#1332:** updates selectors ([120e9d2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/120e9d289ee170214d0143c5f948125f2d9ffc8f)), closes [documentos#1332](http://gitlab.meta.com.br/documentos/issues/1332)
* **documentos#1357:** fixes permission error ([caed226](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/caed22667aa0259d611894310d4afa266634c2d5))
* **documentos#1374:** firebase changes ([df6a585](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/df6a585a34570777c468c98c134cd8fbd855601c)), closes [documentos#1374](http://gitlab.meta.com.br/documentos/issues/1374)
* **documentos#1377:** commercialization fix ([978a707](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/978a70771c50cc5ede07b13219a3d15942e31689)), closes [documentos#1377](http://gitlab.meta.com.br/documentos/issues/1377)
* **documentos#1436:** changes in commercialization ([f4edf6b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f4edf6b977ef4c7a8fe514f844097ab2703e1572)), closes [documentos#1436](http://gitlab.meta.com.br/documentos/issues/1436)
* **documentos#1437:** removes clg ([a9344c4](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a9344c43517ddd93a3ec16756a32d92ac96fb044)), closes [documentos#1437](http://gitlab.meta.com.br/documentos/issues/1437)
* **documentos#1448:** check if access token exists b4 refresh ([c22543c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c22543cb02ad4a22e9d319d16b04faf31f30e9b1)), closes [documentos#1448](http://gitlab.meta.com.br/documentos/issues/1448)
* created web administration module ([458a743](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/458a743b04049e374da04acd96d94cc68c66a9f0))
* resolve [#1285](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1285) ([c0b4d91](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c0b4d91ccdb7ff7adcaaf87175f1c32fe17b1c48))
* resolve [#1291](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1291) ([0580b9c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0580b9cf4f5435a78c9ef27a39ca6cf4cbdf9899))
* **documentso#1332:** changes in auth to align with back-end ([e57dda8](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e57dda85e30915d07d74052ad7fd00c592f3a739)), closes [documentso#1332](http://gitlab.meta.com.br/documentso/issues/1332)
* **doucmentos#1232:** shows all user modules in choosemodulescreen ([8405ea3](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8405ea36080c4bed643150f5632ec666fe0e51e5)), closes [doucmentos#1232](http://gitlab.meta.com.br/doucmentos/issues/1232)
* merge develop ([2731148](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2731148d7380e3ab5c8ec38d198b1773467e4364))
* wip ([b3b6eb5](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b3b6eb57ca79dc596447e6b748d58c3b74087f14))
* wip ([2cf8dd6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2cf8dd6979ab9a9ee338858a586f2587ca18899e))
* wip ([16b3eef](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/16b3eef778dda67fd818f50aaaefb2eb54901e34))





# [0.13.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.12.0-alpha.1...v0.13.0) (2021-03-23)


### Bug Fixes

* **#1217:** add onSuccess in save growingseason ([2d3a97f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2d3a97f55f91984b7ebbf53315afa2aca7751ff0)), closes [#1217](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1217)
* **documenots#1292:** fixed securities movement saga ([75eb7ea](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/75eb7ea431a8edd9f97266d5576c91de5480438f))


### Features

* resolve [#1450](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1450) (WEB) ([9812c7b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9812c7be3076f4ff48599fcb8ecf76f3ebd3b7d7))
* **#1288:** adjustment select organization ([2dbbc9c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2dbbc9c4234ad1a408da856e1c2deeccc814f993)), closes [#1288](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1288) [#395](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/395)
* **#1414:** remove console log and adjustment editing property ([b83b5db](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b83b5dbbc6715b4ceb0ba8d90fd25d7904a4bca9)), closes [#1414](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1414) [#454](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/454)
* **documenots#1292:** added new action in securities movement store ([8a78097](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8a7809774c7b0900ec1d7dfce0feca16be02404b)), closes [documenots#1292](http://gitlab.meta.com.br/documenots/issues/1292)
* **documentos#1116:** tambo for mobile ([f4d650f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f4d650f18602702f4cfccdf32758336da2041ca0)), closes [documentos#1116](http://gitlab.meta.com.br/documentos/issues/1116)
* **documentos#1232:** adjust some selectors ([a4b4dc5](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a4b4dc5c5a2fb9756672fbe8087380d55a888dbc)), closes [documentos#1232](http://gitlab.meta.com.br/documentos/issues/1232)
* **documentos#1232:** removes consle log ([f4dea86](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f4dea86a27885b1c161cde0fd2cd59fa4eac5258)), closes [documentos#1232](http://gitlab.meta.com.br/documentos/issues/1232)
* **documentos#1233:** permissions tweaks ([c3d0da0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c3d0da0c6009cdb75d84f0722bc501d948781afe)), closes [documentos#1233](http://gitlab.meta.com.br/documentos/issues/1233)
* **documentos#1240:** add updateSignature to organzationStore ([283be60](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/283be607a59b92d1c6241dad577db4d557d52de7)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** generates new eletronic signature only if v. password ([c79572a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c79572af40f26a81eb3f1cc5aa6e07e5291b544c)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** updateSignature calling correctyl ([15b1a4e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/15b1a4e5a824249a89f0e087861d920bda53b20e)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1290:** duck, saga and selector of terms ([477827d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/477827df5645ecc3c5c38fdaf6ac2619216854a6)), closes [documentos#1290](http://gitlab.meta.com.br/documentos/issues/1290)
* **documentos#1290:** fix selector ([9d5afed](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9d5afed4989b70e0fa352e4c59dc5c01f868dc5a))
* **documentos#1290:** fixes ([93e2f6f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/93e2f6f1ca671d12b34b2f21fbd7c531bb4a0ef5)), closes [documentos#1290](http://gitlab.meta.com.br/documentos/issues/1290)
* **documentos#1305:** removes birthday from onboarding ([5eda681](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/5eda6815388bc7289d1d33d1e6896faca4e4aa68)), closes [documentos#1305](http://gitlab.meta.com.br/documentos/issues/1305)
* **documentos#1323:** fixes technician in web ([3ea9b64](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3ea9b6409460f5af500e67cccc1c50e11ab543b7))
* **documentos#1323:** removes clg ([81c467a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/81c467a84aef2cd5b61e17a5753d3d09d23fb34e)), closes [documentos#1323](http://gitlab.meta.com.br/documentos/issues/1323)
* **documentos#1323:** removes dprecated contains, using includes now ([e95f2b3](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e95f2b34d755a42acc08f86493d032c03cdd5cce)), closes [documentos#1323](http://gitlab.meta.com.br/documentos/issues/1323)
* **documentos#1323:** updates slugs ([55b8b86](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/55b8b86762cc17d362b39566daac61ad0a92dd55)), closes [documentos#1323](http://gitlab.meta.com.br/documentos/issues/1323)
* **documentos#1332:** changes in permissions ([5872eab](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/5872eabcbdf88d56064c26edd5552fa9969d12c6)), closes [documentos#1332](http://gitlab.meta.com.br/documentos/issues/1332)
* **documentos#1332:** updates selectors ([120e9d2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/120e9d289ee170214d0143c5f948125f2d9ffc8f)), closes [documentos#1332](http://gitlab.meta.com.br/documentos/issues/1332)
* **documentos#1357:** fixes permission error ([caed226](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/caed22667aa0259d611894310d4afa266634c2d5))
* **documentos#1374:** firebase changes ([df6a585](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/df6a585a34570777c468c98c134cd8fbd855601c)), closes [documentos#1374](http://gitlab.meta.com.br/documentos/issues/1374)
* **documentos#1377:** commercialization fix ([978a707](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/978a70771c50cc5ede07b13219a3d15942e31689)), closes [documentos#1377](http://gitlab.meta.com.br/documentos/issues/1377)
* **documentos#1436:** changes in commercialization ([f4edf6b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f4edf6b977ef4c7a8fe514f844097ab2703e1572)), closes [documentos#1436](http://gitlab.meta.com.br/documentos/issues/1436)
* **documentos#1437:** removes clg ([a9344c4](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a9344c43517ddd93a3ec16756a32d92ac96fb044)), closes [documentos#1437](http://gitlab.meta.com.br/documentos/issues/1437)
* **documentos#1448:** check if access token exists b4 refresh ([c22543c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c22543cb02ad4a22e9d319d16b04faf31f30e9b1)), closes [documentos#1448](http://gitlab.meta.com.br/documentos/issues/1448)
* created web administration module ([458a743](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/458a743b04049e374da04acd96d94cc68c66a9f0))
* resolve [#1285](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1285) ([c0b4d91](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c0b4d91ccdb7ff7adcaaf87175f1c32fe17b1c48))
* resolve [#1291](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1291) ([0580b9c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0580b9cf4f5435a78c9ef27a39ca6cf4cbdf9899))
* **documentso#1332:** changes in auth to align with back-end ([e57dda8](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e57dda85e30915d07d74052ad7fd00c592f3a739)), closes [documentso#1332](http://gitlab.meta.com.br/documentso/issues/1332)
* **doucmentos#1232:** shows all user modules in choosemodulescreen ([8405ea3](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8405ea36080c4bed643150f5632ec666fe0e51e5)), closes [doucmentos#1232](http://gitlab.meta.com.br/doucmentos/issues/1232)
* merge develop ([2731148](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2731148d7380e3ab5c8ec38d198b1773467e4364))
* wip ([b3b6eb5](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b3b6eb57ca79dc596447e6b748d58c3b74087f14))
* wip ([2cf8dd6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2cf8dd6979ab9a9ee338858a586f2587ca18899e))
* wip ([16b3eef](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/16b3eef778dda67fd818f50aaaefb2eb54901e34))





# [0.12.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.12.0-alpha.1...v0.12.0) (2021-03-23)


### Bug Fixes

* **#1217:** add onSuccess in save growingseason ([2d3a97f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2d3a97f55f91984b7ebbf53315afa2aca7751ff0)), closes [#1217](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1217)
* **documenots#1292:** fixed securities movement saga ([75eb7ea](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/75eb7ea431a8edd9f97266d5576c91de5480438f))


### Features

* resolve [#1450](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1450) (WEB) ([9812c7b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9812c7be3076f4ff48599fcb8ecf76f3ebd3b7d7))
* **#1288:** adjustment select organization ([2dbbc9c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2dbbc9c4234ad1a408da856e1c2deeccc814f993)), closes [#1288](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1288) [#395](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/395)
* **#1414:** remove console log and adjustment editing property ([b83b5db](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b83b5dbbc6715b4ceb0ba8d90fd25d7904a4bca9)), closes [#1414](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1414) [#454](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/454)
* **documenots#1292:** added new action in securities movement store ([8a78097](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8a7809774c7b0900ec1d7dfce0feca16be02404b)), closes [documenots#1292](http://gitlab.meta.com.br/documenots/issues/1292)
* **documentos#1116:** tambo for mobile ([f4d650f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f4d650f18602702f4cfccdf32758336da2041ca0)), closes [documentos#1116](http://gitlab.meta.com.br/documentos/issues/1116)
* **documentos#1232:** adjust some selectors ([a4b4dc5](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a4b4dc5c5a2fb9756672fbe8087380d55a888dbc)), closes [documentos#1232](http://gitlab.meta.com.br/documentos/issues/1232)
* **documentos#1232:** removes consle log ([f4dea86](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f4dea86a27885b1c161cde0fd2cd59fa4eac5258)), closes [documentos#1232](http://gitlab.meta.com.br/documentos/issues/1232)
* **documentos#1233:** permissions tweaks ([c3d0da0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c3d0da0c6009cdb75d84f0722bc501d948781afe)), closes [documentos#1233](http://gitlab.meta.com.br/documentos/issues/1233)
* **documentos#1240:** add updateSignature to organzationStore ([283be60](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/283be607a59b92d1c6241dad577db4d557d52de7)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** generates new eletronic signature only if v. password ([c79572a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c79572af40f26a81eb3f1cc5aa6e07e5291b544c)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** updateSignature calling correctyl ([15b1a4e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/15b1a4e5a824249a89f0e087861d920bda53b20e)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1290:** duck, saga and selector of terms ([477827d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/477827df5645ecc3c5c38fdaf6ac2619216854a6)), closes [documentos#1290](http://gitlab.meta.com.br/documentos/issues/1290)
* **documentos#1290:** fix selector ([9d5afed](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9d5afed4989b70e0fa352e4c59dc5c01f868dc5a))
* **documentos#1290:** fixes ([93e2f6f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/93e2f6f1ca671d12b34b2f21fbd7c531bb4a0ef5)), closes [documentos#1290](http://gitlab.meta.com.br/documentos/issues/1290)
* **documentos#1305:** removes birthday from onboarding ([5eda681](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/5eda6815388bc7289d1d33d1e6896faca4e4aa68)), closes [documentos#1305](http://gitlab.meta.com.br/documentos/issues/1305)
* **documentos#1323:** fixes technician in web ([3ea9b64](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3ea9b6409460f5af500e67cccc1c50e11ab543b7))
* **documentos#1323:** removes clg ([81c467a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/81c467a84aef2cd5b61e17a5753d3d09d23fb34e)), closes [documentos#1323](http://gitlab.meta.com.br/documentos/issues/1323)
* **documentos#1323:** removes dprecated contains, using includes now ([e95f2b3](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e95f2b34d755a42acc08f86493d032c03cdd5cce)), closes [documentos#1323](http://gitlab.meta.com.br/documentos/issues/1323)
* **documentos#1323:** updates slugs ([55b8b86](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/55b8b86762cc17d362b39566daac61ad0a92dd55)), closes [documentos#1323](http://gitlab.meta.com.br/documentos/issues/1323)
* **documentos#1332:** changes in permissions ([5872eab](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/5872eabcbdf88d56064c26edd5552fa9969d12c6)), closes [documentos#1332](http://gitlab.meta.com.br/documentos/issues/1332)
* **documentos#1332:** updates selectors ([120e9d2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/120e9d289ee170214d0143c5f948125f2d9ffc8f)), closes [documentos#1332](http://gitlab.meta.com.br/documentos/issues/1332)
* **documentos#1357:** fixes permission error ([caed226](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/caed22667aa0259d611894310d4afa266634c2d5))
* **documentos#1374:** firebase changes ([df6a585](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/df6a585a34570777c468c98c134cd8fbd855601c)), closes [documentos#1374](http://gitlab.meta.com.br/documentos/issues/1374)
* **documentos#1377:** commercialization fix ([978a707](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/978a70771c50cc5ede07b13219a3d15942e31689)), closes [documentos#1377](http://gitlab.meta.com.br/documentos/issues/1377)
* **documentos#1436:** changes in commercialization ([f4edf6b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f4edf6b977ef4c7a8fe514f844097ab2703e1572)), closes [documentos#1436](http://gitlab.meta.com.br/documentos/issues/1436)
* **documentos#1437:** removes clg ([a9344c4](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a9344c43517ddd93a3ec16756a32d92ac96fb044)), closes [documentos#1437](http://gitlab.meta.com.br/documentos/issues/1437)
* **documentos#1448:** check if access token exists b4 refresh ([c22543c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c22543cb02ad4a22e9d319d16b04faf31f30e9b1)), closes [documentos#1448](http://gitlab.meta.com.br/documentos/issues/1448)
* created web administration module ([458a743](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/458a743b04049e374da04acd96d94cc68c66a9f0))
* resolve [#1285](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1285) ([c0b4d91](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c0b4d91ccdb7ff7adcaaf87175f1c32fe17b1c48))
* resolve [#1291](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1291) ([0580b9c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0580b9cf4f5435a78c9ef27a39ca6cf4cbdf9899))
* **documentso#1332:** changes in auth to align with back-end ([e57dda8](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e57dda85e30915d07d74052ad7fd00c592f3a739)), closes [documentso#1332](http://gitlab.meta.com.br/documentso/issues/1332)
* **doucmentos#1232:** shows all user modules in choosemodulescreen ([8405ea3](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8405ea36080c4bed643150f5632ec666fe0e51e5)), closes [doucmentos#1232](http://gitlab.meta.com.br/doucmentos/issues/1232)
* merge develop ([2731148](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2731148d7380e3ab5c8ec38d198b1773467e4364))
* wip ([b3b6eb5](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b3b6eb57ca79dc596447e6b748d58c3b74087f14))
* wip ([2cf8dd6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2cf8dd6979ab9a9ee338858a586f2587ca18899e))
* wip ([16b3eef](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/16b3eef778dda67fd818f50aaaefb2eb54901e34))





# [0.12.0-alpha.1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.12.0-alpha.0...v0.12.0-alpha.1) (2021-02-22)


### Bug Fixes

* **auth:** fixed multiples refresh token ([b976597](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b976597b48bfce6639d77453f295a8a3437d836a))
* **documentos#1209:** fixed firebase lifecycle ([831d393](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/831d3934bad4dd29e794559504a2c3b87dd63b20))


### Features

* merge develop ([c7c8f96](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c7c8f96d843e6c6e4edffbcef241c3c9f096257b))
* **documentos#1115:** created barter store and api resource ([9a35185](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9a3518557cc700310607d6920ae6f739a4733bb1)), closes [documentos#1115](http://gitlab.meta.com.br/documentos/issues/1115)
* **documentos#1233:** adjust to math rounding ([3fec498](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3fec498d1f2fd0d5b84fd9fbee90ea9d274b8327)), closes [documentos#1233](http://gitlab.meta.com.br/documentos/issues/1233)
* **documentos#1233:** adjustments to satelite image view ([7067ebd](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7067ebd59103007d509c352ae4f045b067b2157c)), closes [documentos#1233](http://gitlab.meta.com.br/documentos/issues/1233)
* **documentos#535:** finished home screen for mobile ([b595eee](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b595eeed9efc2c6371bb04276a22abfd74a1d9db)), closes [documentos#535](http://gitlab.meta.com.br/documentos/issues/535)
* **documentos#947:** added new barter routes ([a6822b8](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a6822b87f205b32733f20f0b439655ee4d94e9f7)), closes [documentos#947](http://gitlab.meta.com.br/documentos/issues/947)
* **documentos#947:** created new barter store methods ([6520b6c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/6520b6cbc6691ce52e686875d1326a0cd21dabca)), closes [documentos#947](http://gitlab.meta.com.br/documentos/issues/947)
* merge develop ([14437f2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/14437f2c05646aadaf548086d38f70dcf5054eda))
* merge develop ([79eb412](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/79eb412585a0950ecf6c39cb7132c1648068fbca))
* resolve [#1036](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1036) ([f248bb6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f248bb6b37ef7e7000fc92b87832b8d5fd497c2d))
* resolve [#992](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/992) ([3cc24d7](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3cc24d721b1e0e8243e9ec84d51677e3ee50de32))
* resolve mr conflicts ([27c5993](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/27c5993600bf6cc58f0e12706d0a18e49b484ce1))
* wip ([452dd2d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/452dd2d1eaeeddea616d23ca757bcba9864baab8))
* **documentos#987:** add milk quality to store ([dd7384c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/dd7384c01c3f6f573345b8c13d7953e7c70f4625)), closes [documentos#987](http://gitlab.meta.com.br/documentos/issues/987)





# [0.12.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.11.0...v0.12.0-alpha.0) (2021-02-12)

**Note:** Version bump only for package @smartcoop/stores





# [0.11.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.11.0-alpha.0...v0.11.0) (2021-02-12)


### Features

* **#1142:** adjustment register technical visit ([6b11511](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/6b11511de1d069510ea4532532aa095e665f677e)), closes [#1142](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1142) [#319](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/319)
* **#1146:** create user access area in mobile ([63fa182](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/63fa182f721a275a0755dd562af346475849ea42)), closes [#1146](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1146) [#320](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/320)
* **documentos#1197:** dashboard store ([c377491](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c37749174689fbdafa89b75d0a2e0ca579251b7f)), closes [documentos#1197](http://gitlab.meta.com.br/documentos/issues/1197)
* **documentos#987:** isAdmin selector ([465c5a4](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/465c5a413deb2e8a15e9146414926f51ee34b826)), closes [documentos#987](http://gitlab.meta.com.br/documentos/issues/987)
* resolve [#1117](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1117) ([f295d11](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f295d11daa1e6f8a733392d15ee431cc8a0db78e))
* resolve [#1117](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1117) ([28e2c23](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/28e2c2337354377dcc21ce4b40037525a7ab0900))
* wip ([395287f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/395287fad709cf45b1d58c1f8517d60fe4f80bb0))





# [0.11.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.10.0...v0.11.0-alpha.0) (2021-02-05)


### Bug Fixes

* **onboarding:** fixed save user workflow ([a361e15](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a361e15d4dc30efa0d9d1c964174f3a52ef11975))


### Features

* **technical:** turn on permissions ([5f75712](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/5f75712c9ebd3e83ec4cb86762ac20ddb0696720))





# [0.10.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.10.0-alpha.0...v0.10.0) (2021-02-01)


### Bug Fixes

* **fields:** fixed access current field ([1cc7f2b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1cc7f2be0c59c9f93d27732a40abfd26a4cef1b4))
* **firebase:** fixed on web ([e048482](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e04848292e5f0c5b775a497e364b39ec0ea3cf8d))
* **login:** fixed firebase login and logout ([aee9d4d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/aee9d4d2f4ac03cf74338cbb8699fdf061cda307))


### Features

* **#1004:** adjustment show technical visits and register ([7c06744](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7c06744d7fc13b44151f1b9354a2ab9d5f247426)), closes [#1004](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1004)
* **#1004:** adjustments ([cd86db0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/cd86db0251fd3ae7c8091fb957696cedbf822053)), closes [#1004](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1004)
* **#1004:** implement edit technical visit ([f8f20c2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f8f20c25a9c93631192366dfed0931dda25aa13d)), closes [#1004](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1004) [#275](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/275)
* **auth:** added login by cpf/cnpj ([3fb8616](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3fb8616f1773e6686c4be2a5a4893884ab86b84d))
* **documentos#1006:** created technical portfolio screen ([8f964b7](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8f964b73604e5520e9b956cca6855770abc6a89c)), closes [documentos#1006](http://gitlab.meta.com.br/documentos/issues/1006)
* **documentos#102:** duck, sagas related avatar ([0798d3e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0798d3e69786679d7e293b3e08bef85bbe19b922)), closes [documentos#102](http://gitlab.meta.com.br/documentos/issues/102)
* **documentos#102:** edits duck and saga to update user when convenient ([5bd6b36](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/5bd6b36dfbb80a5ee8c95bfa92afac566a003ab1)), closes [documentos#102](http://gitlab.meta.com.br/documentos/issues/102)
* **documentos#1086:** added create order custom validations ([c1e8ab4](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c1e8ab45a9e20462a765bf60e2143a05781c7472)), closes [documentos#1086](http://gitlab.meta.com.br/documentos/issues/1086)
* **documentos#534:** added technical module to mobile ([007789a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/007789a8ca387d04401095875ba867d35388cbc9)), closes [documentos#534](http://gitlab.meta.com.br/documentos/issues/534)
* **documentos#974:** added products withdraw list in technical ([d241e8a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d241e8aa6106e4a1fa0b9cc9a2ea367729129e8c)), closes [documentos#974](http://gitlab.meta.com.br/documentos/issues/974)
* **documentos#974:** added products withdraw list in technical ([11a7a23](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/11a7a23a4ee992f6a3ee60b88c35f792c2bfe1b5)), closes [documentos#974](http://gitlab.meta.com.br/documentos/issues/974)
* **documentos#987:** adds dairyFarmId in sagas ([107a589](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/107a589f6a579e25bace3503c5bd5bd1512ec7d1)), closes [documentos#987](http://gitlab.meta.com.br/documentos/issues/987)
* **documentos#987:** correct actions in register ([b4746de](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b4746de1eb9d93ae464e01437dc3a92d60cf095c)), closes [documentos#987](http://gitlab.meta.com.br/documentos/issues/987)
* **documentos#987:** creates duckDairyFarm ([16c2275](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/16c22756e2563d03eaad16155955ee4f1d345c09)), closes [documentos#987](http://gitlab.meta.com.br/documentos/issues/987)
* **documentos#987:** creates sagaDairyFarm ([540864a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/540864a1591e8c6a8ff0cc48bd3808415b1c4f13)), closes [documentos#987](http://gitlab.meta.com.br/documentos/issues/987)
* **documentos#987:** declares reducer and saga ([e528774](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e528774cb79a78fc86bc981ab4756898f2c1e2b1)), closes [documentos#987](http://gitlab.meta.com.br/documentos/issues/987)
* **documentos#987:** fixes duckdairyfarm ([baf0b38](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/baf0b38fc481b679bb39303eaa4599c6134b1649))
* **doucmentos#987:** fixes in resouces and duck ([2372c5e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2372c5e7ceb653a3b90002064adfaf94f0083ea8))
* resolve merge conflicts ([d76333d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d76333db93b61f9897987ee033d050dcdc48d8fe))
* wip ([ea43347](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ea4334798c6090ec6819e6390596359d2bf439de))
* wip ([094d424](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/094d4242f82345f35ecd7b4227e3c497da75c966))





# [0.10.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.9.0...v0.10.0-alpha.0) (2021-01-19)

**Note:** Version bump only for package @smartcoop/stores





# [0.9.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.8.2...v0.9.0) (2021-01-19)


### Bug Fixes

* **#971:** adjustment loading detail field ([d8a9d2a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d8a9d2a8d524c07afcf3473d81c5eadc0efb48bd)), closes [#971](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/971) [#263](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/263)
* **documentos#836:** getting user notifications before ask permission ([53c928f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/53c928f71bff691e53ad7274bf7a8a8cc1fa7b57)), closes [documentos#836](http://gitlab.meta.com.br/documentos/issues/836)
* **property activities:** fixed property activities ([fcfaa17](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/fcfaa17786cbfdf1b0b71d87454802379d8a95f7))


### Features

* **#873:** integration order create and signature electronic ([1f72cb3](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1f72cb3e3cc5d70f80d31c8e4fe48b8658a4ba2f)), closes [#873](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/873) [#260](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/260)
* added sales order delete ([ff4f588](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ff4f588585eb4c76f9a544ee4c650d5551eae85b))
* added sales orders forms ([b1e3fe8](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b1e3fe88414dba234ec6c9deff8d6cb7c3616f5a))
* merge Develop ([13f52cf](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/13f52cfce97d5e68ce271d48dc7e493ee065e28c))
* **#862:** adjustment register and edit field ([4aa85af](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4aa85af740e380188f806588a53dbe96b006fb55)), closes [#862](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/862)
* **#862:** implemented onboarding for supplier ([a639b79](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a639b79882d5c01fd04dd43a363c5580039992a2)), closes [#862](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/862)
* **#901:** create actions for price history ([a60b133](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a60b13355f3b721a6c056f893c82a3838eeb1401)), closes [#901](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/901) [#241](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/241)
* **documentos#299:** changes in delete service ([c303f67](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c303f6794dbf0c8b28640d92ac3d90cfc3429825)), closes [documentos#299](http://gitlab.meta.com.br/documentos/issues/299)
* **documentos#299:** correctly posts in saga ([98eec4b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/98eec4ba6e6e1a07c489c1b02737eb1d7784fa43)), closes [documentos#299](http://gitlab.meta.com.br/documentos/issues/299)
* **documentos#299:** creates actions in reducer ([4729a8b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4729a8b0217fc9047a19ba27ced05373d34554be)), closes [documentos#299](http://gitlab.meta.com.br/documentos/issues/299)
* **documentos#299:** init saga ([d493df4](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d493df48c7db72c22d72d0e9e5bd79c348675b8d)), closes [documentos#299](http://gitlab.meta.com.br/documentos/issues/299)
* resolve [#920](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/920) ([d4f7839](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d4f783957ef1027d031ad535835c3b1f9f75a7a3))
* resolve MR Upgrades ([c1feab5](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c1feab5ec5d26f22bf59577300636fa675cfffb9))
* **documentos#491:** sales orders api integration ([db53258](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/db532586160a539d34d858d40742b63b55c0ffbe)), closes [documentos#491](http://gitlab.meta.com.br/documentos/issues/491)
* **documentos#836:** created notifications list ([f062230](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f062230640b49f3df88ad8a2a6ec5b447913dc69)), closes [documentos#836](http://gitlab.meta.com.br/documentos/issues/836)
* **documentos#836:** finished firebase integration ([f6db099](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f6db099715a083b74d4559d0c064c8d0e212ae1c)), closes [documentos#836](http://gitlab.meta.com.br/documentos/issues/836)
* **documentos#836:** finished mobile list ([6cbf8b3](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/6cbf8b38ee2ef54367d0503f607dc38386ca56e8)), closes [documentos#836](http://gitlab.meta.com.br/documentos/issues/836)
* added module selection rules in mobile ([cb56581](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/cb565811f4d57ab809c984beb13b524b5f08fff4))
* wIP ([1fdaea5](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1fdaea5105a25ec89d89395a7db1ab22a87bbc22))
* **documentos#507:** adds update to offline ([d487a5c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d487a5cad4ec2b6e05add6c27df52743dd43ac41)), closes [documentos#507](http://gitlab.meta.com.br/documentos/issues/507)
* **documentos#836:** added notifications for web ([0b7388c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0b7388ca6ec0da439a7acf516d50f4954f224f9c)), closes [documentos#836](http://gitlab.meta.com.br/documentos/issues/836)
* **documentos#836:** firebase login and logout for mobile ([fbd31c4](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/fbd31c47a6b6a9d1d255945a6091b0376aa31fbb)), closes [documentos#836](http://gitlab.meta.com.br/documentos/issues/836)
* **geotiff download:** added button to download a .tiff satelite image ([4ec244c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4ec244cc57d41825fe39934d8c191b0b152bc621))
* resolve Monte's Comments on MR ([b5a6fc7](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b5a6fc7cf86e92d2d0c005b4f75349741f2c2722))





## [0.8.2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.8.1...v0.8.2) (2021-01-06)


### Bug Fixes

* **ajustado deletefield:** ajustado deletefield" ([b8f237c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b8f237cdb945d630ce32bc56140bf8191be70a30))


### Features

* changed securities movement routes ([23b15c7](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/23b15c7672093782869b9e050e4069438e3b5025))
* **documentos#725:** delete machine ([bd2282b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/bd2282bf5634c29596b122ed7a16b7fbb539190d)), closes [documentos#725](http://gitlab.meta.com.br/documentos/issues/725)
* **documentos#725:** fixes rebase stuff, adds temp fake pagination size ([ce9f67c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ce9f67c49bc98b997964bc483853b05fac99ca6d))
* **property actions:** added actions to delete and edit property ([c80765a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c80765aaf6a2352a2c5ef2666eb6d2301280c0e4))
* created sales order forms ([74422e5](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/74422e59f4220564a57c8406580c3bbd9622b1e5))





## [0.8.1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.9.0-alpha.0...v0.8.1) (2021-01-06)

**Note:** Version bump only for package @smartcoop/stores





# [0.9.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.8.0...v0.9.0-alpha.0) (2021-01-05)

**Note:** Version bump only for package @smartcoop/stores




# [0.8.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.8.0-alpha.0...v0.8.0) (2021-01-05)


### Bug Fixes

* **documentos#321:** adicionada exibição de dias na previsão do tempo ([25c8472](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/25c8472a3463d28226a65887b6ddd61c31d68d0c)), closes [documentos#321](http://gitlab.meta.com.br/documentos/issues/321)
* **documentos#69:** get token by organization ([f478fab](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f478fab50847279b60e38491ed90d67fcdd3caae)), closes [documentos#69](http://gitlab.meta.com.br/documentos/issues/69)
* **documentos#800:** fixed setCurrentOrganization saga ([2d1afa6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2d1afa6965ec75a65baf6afaaeeb44914b207827))


### Features

* **docuementos#491:** created sales orders actions ([e1e7778](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e1e7778ef63bae23f81f15e0040540a8944a037f)), closes [docuementos#491](http://gitlab.meta.com.br/docuementos/issues/491)
* **documenots#506:** creates store for machinery ([db72369](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/db7236952b32310e31d334faa082241158e10e3b)), closes [documenots#506](http://gitlab.meta.com.br/documenots/issues/506)
* **documenots#506:** creates store for machinery ([5a8e035](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/5a8e035aaa0d5342e43e1e774a190afb1e0cb65f)), closes [documenots#506](http://gitlab.meta.com.br/documenots/issues/506)
* **documentos#176 documentos#143:** product balance and withdraw list ([076dae2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/076dae26e34aecb1a671e86e4d40eb574521c4db)), closes [documentos#176](http://gitlab.meta.com.br/documentos/issues/176) [documentos#143](http://gitlab.meta.com.br/documentos/issues/143)
* **documentos#27:** some tweaks in saga ([80dec22](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/80dec22a49475ef335c8d11f6d008f42184a6464)), closes [documentos#27](http://gitlab.meta.com.br/documentos/issues/27)
* **documentos#300:** created refresh token mechanism ([54ee2f1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/54ee2f1626aee3e6c60d7a1412305fce7fef5d01)), closes [documentos#300](http://gitlab.meta.com.br/documentos/issues/300)
* **documentos#491:** create order form ([2b8653f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2b8653f777465d35bc1075bcc7578d4259e88863)), closes [documentos#491](http://gitlab.meta.com.br/documentos/issues/491)
* **documentos#492:** adds button to receive ([6785be2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/6785be284961801f320a075216e11ba853fcfe1a)), closes [documentos#492](http://gitlab.meta.com.br/documentos/issues/492)
* **documentos#501:** add new saga attachBankslip ([0ead447](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0ead4474c4aa7f49c7425e3382562d0d7618bec4)), closes [documentos#501](http://gitlab.meta.com.br/documentos/issues/501)
* **documentos#501:** duck and saga ([2701405](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2701405c9dad9b7e837840c5809483c43fa68b96)), closes [documentos#501](http://gitlab.meta.com.br/documentos/issues/501)
* **documentos#506:** adds selector for machinery ([67d538c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/67d538c540329bfce6ca69b88a50eda8beec3177)), closes [documentos#506](http://gitlab.meta.com.br/documentos/issues/506)
* **documentos#506:** adds selector for machinery ([d9a79f9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d9a79f9e1a872817e03ba594c4660d8bfb5ec075)), closes [documentos#506](http://gitlab.meta.com.br/documentos/issues/506)
* **documentos#506:** currentMachine selector ([4633b76](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4633b7675c58021dbea46a267e92eaabadeaf6f2)), closes [documentos#506](http://gitlab.meta.com.br/documentos/issues/506)
* **documentos#506:** get data onSuccess ([1603ca1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1603ca176d11620e17e6b7270225439eace6a04c)), closes [documentos#506](http://gitlab.meta.com.br/documentos/issues/506)
* **documentos#507:** adds new action to duckmachine ([8f4b157](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8f4b157a1f3beb6926a76fc56d61c7c7b6da575f)), closes [documentos#507](http://gitlab.meta.com.br/documentos/issues/507)
* **documentos#507:** fixes reducer duplicate key and selector ([21db2ce](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/21db2cec814026d26ae6994645cca03e9c078996))
* **documentos#688:** finished field details ([f051b4e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f051b4ec43bbd5103f739761adbec3d395561cc2)), closes [documentos#688](http://gitlab.meta.com.br/documentos/issues/688)
* **documentos#71:** added current account in store ([2c5c6cd](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2c5c6cdc429e62b5c72b4b43fd6b4fb5f5b84af0)), closes [documentos#71](http://gitlab.meta.com.br/documentos/issues/71)
* **documentos#71:** created securities movement actions ([ae0a0bc](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ae0a0bc512165ea38731e3ab71b17fcc4d3e0b76)), closes [documentos#71](http://gitlab.meta.com.br/documentos/issues/71)
* **documentos#794:** adjust CreateOrganizationForm component ([7a6bd0c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7a6bd0c479f5fca05771cc7f60748ab0cd454b95)), closes [documentos#794](http://gitlab.meta.com.br/documentos/issues/794)
* **permissions:** accessing modules by permissions ([1d44dcc](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1d44dcc57ccf9ef37df1708b542e070b0a03a84e))





# [0.8.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.7.0...v0.8.0-alpha.0) (2020-12-11)

**Note:** Version bump only for package @smartcoop/stores





# [0.7.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.7.0-alpha.0...v0.7.0) (2020-12-11)


### Bug Fixes

* **documentos#519:** fixed fields list ([d49125e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d49125e26cc24c734367924a5080869368fb55d7))


### Features

* **documentos#103:** added filed list by property ([e6ed808](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e6ed8083b58835813c249666bcb8f5dd835f6add)), closes [documentos#103](http://gitlab.meta.com.br/documentos/issues/103)
* **documentos#195:** added new supplier quotation routes and store ([9edf339](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9edf339e57e5abc1875044736f2315a3f54ef684)), closes [documentos#195](http://gitlab.meta.com.br/documentos/issues/195)
* **documentos#195:** added reject, remove and edit supplier quotation ([c6e80bf](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c6e80bfd4545de18ff4c16c214f88d58f085bf29)), closes [documentos#195](http://gitlab.meta.com.br/documentos/issues/195)
* **documentos#195:** fixed supplier quotation store ([3631eab](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3631eab77a7437ffb2394d81bea782afe6330c56))
* **documentos#223:** changed load best proposal ([0cb6c54](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0cb6c540812a157b877a879b4781c558b263821a)), closes [documentos#223](http://gitlab.meta.com.br/documentos/issues/223)
* **documentos#223:** created best proposal store ([ac3e08e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ac3e08e528051df53d0c1a098af8fe26135a6b42)), closes [documentos#223](http://gitlab.meta.com.br/documentos/issues/223)
* **documentos#233:** adds loading in pages ([d43367c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d43367ce2678aa5c39d498a976f675637e893203)), closes [documentos#233](http://gitlab.meta.com.br/documentos/issues/233)
* **documentos#233:** changed refuse supplier quotation proposal ([13b8177](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/13b817728230a1b564bf934c8df7ff374ea9381c)), closes [documentos#233](http://gitlab.meta.com.br/documentos/issues/233)
* **documentos#233:** validations. fixes and tweaks ([811b105](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/811b105c8eea3fcfbdd07a1858f857874c372620))
* **documentos#262:** creates new methods in sagas and duck ([d9696cb](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d9696cba155d04a4e3ebf4a9a50ae7106b0b50ee)), closes [documentos#262](http://gitlab.meta.com.br/documentos/issues/262)
* **documentos#402:** editing growing season ([321da9c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/321da9cb75b830d048c43afa491b4f2911e58b03)), closes [documentos#402](http://gitlab.meta.com.br/documentos/issues/402)
* **documentos#403:** removing crop from field ([69e1fc4](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/69e1fc43d2b83ecf235e7ce9a9cb5b7e5facc74a)), closes [documentos#403](http://gitlab.meta.com.br/documentos/issues/403)





# [0.7.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.6.0...v0.7.0-alpha.0) (2020-11-27)

**Note:** Version bump only for package @smartcoop/stores





# [0.6.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.5.3...v0.6.0) (2020-11-27)


### Bug Fixes

* **#307:** add verification in the sagasOrganization and SagasProperty ([7b739ef](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7b739ef33a9bbc54c0daa6d44e60054aebe6140d)), closes [#307](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/307) [#92](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/92)
* **documentos#107:** fragmented quotes screen ([ba3ba93](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ba3ba93d6731c0832b89353250c6eb7ba132637a)), closes [documentos#107](http://gitlab.meta.com.br/documentos/issues/107)


### Features

* **#307:** removing an unnecessary conditional ([70a81fb](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/70a81fb7654ea3463fbbd69c612189ea6d0816c3)), closes [#307](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/307) [#92](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/92)
* **documentos#107:** adds params to saga and duck ([c061e02](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c061e02a26d3c207d43e53a657c1050ae7b372b3)), closes [documentos#107](http://gitlab.meta.com.br/documentos/issues/107)
* **documentos#107:** adds resetProductQuotation at duck ([3a2b644](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3a2b644b185fe90c1deadf7e7f022fd7a4ca8db6)), closes [documentos#107](http://gitlab.meta.com.br/documentos/issues/107)
* **documentos#107:** creates sags of productquotation ([d426455](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d42645597a6bf0f886a725fc7e26a4531ead840a)), closes [documentos#107](http://gitlab.meta.com.br/documentos/issues/107)
* **documentos#107:** populates list ([5dadeeb](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/5dadeeb5ce4132191ef29a9fe73b3667ba4b2ca6)), closes [documentos#107](http://gitlab.meta.com.br/documentos/issues/107)
* **documentos#108:** adds list in mobile ([40bff51](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/40bff512d1eadd16fac0b511570842b17ead4530)), closes [documentos#108](http://gitlab.meta.com.br/documentos/issues/108)
* **documentos#108:** creates get accounts ([808e4b0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/808e4b02ad219e55f0c0a6b62a1c80136c0f4fce)), closes [documentos#108](http://gitlab.meta.com.br/documentos/issues/108)
* **documentos#108:** creates storage of accounts and accounts balance ([65f1beb](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/65f1beb25e36efa0f3fb9c2919ad41907cdc87a5)), closes [documentos#108](http://gitlab.meta.com.br/documentos/issues/108)
* **documentos#156:** added dynamic property to field register ([0422c8d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0422c8da3d7c39ccb0bf76da437c429e1ef96009)), closes [documentos#156](http://gitlab.meta.com.br/documentos/issues/156)
* **documentos#16:** added form to growing season register screen ([151cf11](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/151cf11ddbf1c81a9b994c9f002fc1a2d7e3e4a7)), closes [documentos#16](http://gitlab.meta.com.br/documentos/issues/16)
* **documentos#16:** created current field in duck field ([a03f663](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a03f6638b312b1d0b384a02e2e77e0bfc9c92200)), closes [documentos#16](http://gitlab.meta.com.br/documentos/issues/16)
* **documentos#16:** created field details and crops register screens ([c5e82f9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c5e82f9c342973024e6e625b7641b39b3dec1c60)), closes [documentos#16](http://gitlab.meta.com.br/documentos/issues/16)
* **documentos#16:** created growing season store ([b4dd2e5](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b4dd2e5cb4b6672f816232ce998d61ab88a0ec0a)), closes [documentos#16](http://gitlab.meta.com.br/documentos/issues/16)
* **documentos#286:** drawing field on mobile device ([a057a07](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a057a070ddbf50c4f633c65f2c20f68caea519ff)), closes [documentos#286](http://gitlab.meta.com.br/documentos/issues/286)



# [0.6.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.5.0...v0.6.0-alpha.0) (2020-11-10)





# [0.6.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.5.0...v0.6.0-alpha.0) (2020-11-10)

**Note:** Version bump only for package @smartcoop/stores
## [0.5.3](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.5.2...v0.5.3) (2020-11-11)


### Bug Fixes

* **staging:** fixed bugs from release 5 ([aa1cae0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/aa1cae08a05c9653ccaf08d49b3bd630d3c4cd5a))





# [0.5.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.5.0-alpha.0...v0.5.0) (2020-11-10)


### Bug Fixes

* **documentos#10:** getting user by birthdate from first access ([62cc3bf](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/62cc3bfe33a357c259982f16ed0ff17d72bf3131)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#252:** returning to page 1 when query change ([23eb6e6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/23eb6e60811e3f65fdd3d89410c309cf6a03a503)), closes [documentos#252](http://gitlab.meta.com.br/documentos/issues/252)
* **documentos#93:** fixed order selector ([3621926](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3621926743482ce45c9ce25342b0cf296d470a47))
* **first-access:** fixed moment formats ([eeee695](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/eeee69562c01794fb4d491334df887a4a1581c37))


### Features

* **documentos#259:** set selectField to the correct path ([04ae4f4](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/04ae4f4e7ad3e2f5e6e406a5cdd4dfcbd3303932)), closes [documentos#259](http://gitlab.meta.com.br/documentos/issues/259) [#81](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/81)
* **documentos#93:** added current data to edit order ([0dc7de5](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0dc7de5de205be0d7503d7a9e770e425585ee462)), closes [documentos#93](http://gitlab.meta.com.br/documentos/issues/93)
* **documentos#93:** added edit join and exit order, in order store ([c3178ab](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c3178ab7bd1b3a42bd480aec6f74d2c84ae33be9)), closes [documentos#93](http://gitlab.meta.com.br/documentos/issues/93)
* **documentos#93:** added exit order to order actions ([dfdad68](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/dfdad680db78c0063c236af102d8f2397b1c2cb1)), closes [documentos#93](http://gitlab.meta.com.br/documentos/issues/93)
* **documentos#93:** created form ([79dc053](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/79dc053e867209bbb6c2f6b345878daef8e2b73d)), closes [documentos#93](http://gitlab.meta.com.br/documentos/issues/93)
* **documentos#93:** join order ([068d657](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/068d6571483eea2adc35e93596cd8e7023ce76f1)), closes [documentos#93](http://gitlab.meta.com.br/documentos/issues/93)
* **documentos#93:** listing delivery locations by current organization ([a2aa3e2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a2aa3e2e8612159d9218e609af4ae2357ee675b5)), closes [documentos#93](http://gitlab.meta.com.br/documentos/issues/93)





# [0.5.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.4.0...v0.5.0-alpha.0) (2020-10-27)

**Note:** Version bump only for package @smartcoop/stores





# [0.4.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.3.0-alpha.0...v0.4.0) (2020-10-27)


### Bug Fixes

* **documentos#25:** fixed field duck ([1ccb00f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1ccb00f17f6aedc3d4631c8f2650fd0c86661d09))
* **documentos#25:** fixed load fields saga ([1ea751f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1ea751fa3c6f4126f4a5345b012a97366339ec9c))
* **documentos#28:** show onboarding only if it's not created ([a2462f2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a2462f22e675dec8732780280e99abf63441b115)), closes [documentos#28](http://gitlab.meta.com.br/documentos/issues/28) [documentos#111](http://gitlab.meta.com.br/documentos/issues/111)


### Features

* **documentos#135:** redux offline queue ([7deba4f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7deba4f419b4104188247143901b1d7ea38de210)), closes [documentos#135](http://gitlab.meta.com.br/documentos/issues/135)
* **documentos#135:** sorting out user reducer ([f49641f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f49641f9fe65c0e080a2d5750ec281fd72f52843)), closes [documentos#135](http://gitlab.meta.com.br/documentos/issues/135)
* **documentos#185:** created basic navigation for web and mobile ([4b144ed](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4b144edcd2c7d7338100f5da5105df4ccec87969)), closes [documentos#185](http://gitlab.meta.com.br/documentos/issues/185)
* **documentos#25:** created field store ([0373246](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0373246f26c06e6d33e61ec5bdbbce10822a0c35)), closes [documentos#25](http://gitlab.meta.com.br/documentos/issues/25)
* **documentos#49:** added current order to duck order ([8d820f9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8d820f9eff72f2ebccee4bd1b823a83fc9540c25)), closes [documentos#49](http://gitlab.meta.com.br/documentos/issues/49)
* **documentos#49:** created DataTable async for web ([609c7cb](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/609c7cb05d0df3503703363912d3bde9ede825b4)), closes [documentos#49](http://gitlab.meta.com.br/documentos/issues/49)
* **documentos#53:** added web field draw ([31ac01c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/31ac01c1626f479d021bcffa0c3b190d7259db5a)), closes [documentos#53](http://gitlab.meta.com.br/documentos/issues/53)
* **documentos#68:** added current organization to store ([91323f6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/91323f60e2a88107b972d6e5da52e683fe97c0a6)), closes [documentos#68](http://gitlab.meta.com.br/documentos/issues/68)





# [0.3.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.2.0...v0.3.0-alpha.0) (2020-09-28)

**Note:** Version bump only for package @smartcoop/stores





# [0.2.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.2.0-alpha.0...v0.2.0) (2020-09-28)


### Bug Fixes

* **documentos#10:** fix date of birth timezone ([d7208c3](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d7208c3da26f67ca5fa3b55a5eee44c0884d670f))
* **documentos#10:** passing cpf to create verify code ([f08ed72](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f08ed722af228e918abb36980c41d703ddf4762b)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)


### Features

* **documentos#10:** changing reset code because api changed ([42d201d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/42d201d983b0fd1be00356119205b6ad56034d97)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#10:** created web login screen ([34ed647](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/34ed647f1ab16e0c1dc45db8455239335ed6286d)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#10:** created web screens ([496ac64](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/496ac645455e7c7e127a68385e9d8fa8b3b4b9f6)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#10:** finished onboarding w/ recover password for mobile ([d8d7623](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d8d7623f801f871a0473a9d7f1334a975a379207)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#10:** finished productor onboarding ([4eb4a89](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4eb4a89ed42ba9a8161999c29bf941a95c5c1119)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#28:** created property screens for mobile ([10abfa0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/10abfa083e93e992b3fac4c4ec49966b44e28c2c)), closes [documentos#28](http://gitlab.meta.com.br/documentos/issues/28)
* **documentos#32:** removed watermelon. added redux saga ([cd97772](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/cd977725490d8654afc2dad1a0e7bc3d6b16b4dd)), closes [documentos#32](http://gitlab.meta.com.br/documentos/issues/32)
