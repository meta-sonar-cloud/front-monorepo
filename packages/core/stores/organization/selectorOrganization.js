export const selectUserOrganizations = (state) =>
  state.organization.userOrganizations

export const selectUserOrganizationsByModule = (state) =>
  state.organization.userOrganizationsByModule

export const selectCurrentOrganization = (state) =>
  state.organization.currentOrganization

export const selectCurrentOrganizationAddresses = (state) =>
  state.organization.currentOrganizationAddresses
