import { markActionsOffline } from 'redux-offline-queue'
import { createActions, createReducer } from 'reduxsauce'

import { AuthenticationTypes } from '../authentication/duckAuthentication'

// Initial state
const INITIAL_STATE = {
  animals: [],
  currentAnimal: {},
  isDetail: false,
  error: null,
  activeTab: 'registerAnimal',
  pveList: []
}

/**
 * Creating actions and types with reduxsauce.
 */
const { Types, Creators } = createActions({
  loadAnimals: ['params', 'onSuccess', 'onError'],
  loadAnimalsSuccess: ['params', 'page', 'onSuccess'],

  loadCurrentAnimal: ['onSuccess', 'OnError'],
  loadCurrentAnimalSuccess: ['currentAnimal', 'onSuccess', 'OnError'],

  saveAnimal: ['params', 'onSuccess', 'onError'],
  deleteAnimal: ['animalId', 'onSuccess', 'onError'],

  loadAnimalPevList: ['params', 'onSuccess', 'onError'],
  loadAnimalPevListSuccess: ['params', 'page', 'onSuccess'],
  saveAnimalPev: ['pev', 'onSuccess', 'onError'],
  deleteAnimalPev: ['pevId', 'onSuccess', 'onError'],
  resetAnimalPevList: [],

  setCurrentAnimal: ['currentAnimal'],
  resetCurrentAnimal: [],

  setIsDetail: ['isDetail'],
  resetIsDetail: [],

  setActiveTab: ['activeTab'],

  animalError: ['error']
})

markActionsOffline(Creators, [
  'loadAnimals',
  'deleteAnimal'
])

/**
 * Reducers functions
 */

const loadAnimalsSuccess = (state = INITIAL_STATE, { params, page }) => ({
  ...state,
  error: INITIAL_STATE.error,
  animals: page === 1 ? params : [
    ...state.animals,
    ...params
  ]
})

const animalError = (state = INITIAL_STATE, { error }) => ({
  ...state,
  error
})


const setCurrentAnimal = (state = INITIAL_STATE, { currentAnimal }) => ({
  ...state,
  currentAnimal
})

const resetcurrentAnimal = (state = INITIAL_STATE) => ({
  ...state,
  currentAnimal: INITIAL_STATE.currentAnimal
})

const setIsDetail = (state = INITIAL_STATE, { isDetail }) => ({
  ...state,
  isDetail
})

const resetIsDetail = (state = INITIAL_STATE) => ({
  ...state,
  isDetail: INITIAL_STATE.isDetail
})

const setActiveTab = (state = INITIAL_STATE, { activeTab }) => ({
  ...state,
  activeTab
})

const loadAnimalPevListSuccess = (state = INITIAL_STATE, { params, page }) => ({
  ...state,
  error: INITIAL_STATE.error,
  pevList: page === 1 ? params : [
    ...state.pevList,
    ...params
  ]
})

const loadCurrentAnimalSuccess = (state = INITIAL_STATE, { currentAnimal }) => ({
  ...state,
  error: INITIAL_STATE.error,
  currentAnimal
})

const resetAnimalPevList = (state = INITIAL_STATE) => ({
  ...state,
  error: INITIAL_STATE.error,
  pevList: INITIAL_STATE.pevList
})



const logout = () => ({ ...INITIAL_STATE })

/**
 * Creating reducer with Types.
 */

export default createReducer(INITIAL_STATE, {
  [Types.LOAD_ANIMALS_SUCCESS]: loadAnimalsSuccess,

  [Types.ANIMAL_ERROR]: animalError,

  [Types.LOAD_CURRENT_ANIMAL_SUCCESS]: loadCurrentAnimalSuccess,

  [Types.LOAD_ANIMAL_PEV_LIST_SUCCESS]: loadAnimalPevListSuccess,
  [Types.RESET_ANIMAL_PEV_LIST]: resetAnimalPevList,

  [Types.SET_CURRENT_ANIMAL]: setCurrentAnimal,
  [Types.RESET_CURRENT_ANIMAL]: resetcurrentAnimal,

  [Types.SET_IS_DETAIL]: setIsDetail,
  [Types.RESET_IS_DETAIL]: resetIsDetail,

  [Types.SET_ACTIVE_TAB]: setActiveTab,

  [AuthenticationTypes.LOGOUT]: logout
})

export {
  Types as AnimalTypes,
  Creators as AnimalActions
}
