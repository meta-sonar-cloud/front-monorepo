export const selectAnimals = state => state.animal.animals

export const selectCurrentAnimal = state => state.animal.currentAnimal

export const selectIsDetail = state => state.animal.isDetail

export const selectActiveTab = state => state.animal.activeTab

export const selectAnimalPevList = state => state.animal.pevList
