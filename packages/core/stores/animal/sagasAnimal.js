import { REACT_APP_FAKE_PAGINATION_SIZE } from 'react-native-dotenv'

import { select, call, put, takeLatest } from 'redux-saga/effects'

import { isEmpty } from 'lodash'

import {
  getAnimals as getAnimalsService,
  getAnimal as getAnimalService,
  createAnimal as createAnimalService,
  editAnimal as editAnimalService,
  deleteAnimal as deleteAnimalService
} from '@smartcoop/services/apis/smartcoopApi/resources/animal'
import {
  createAnimalPev as createAnimalPevService,
  editAnimalPev as editAnimalPevService,
  deleteAnimalPev as deleteAnimalPevService,
  getAnimalPevList
} from '@smartcoop/services/apis/smartcoopApi/resources/herdsManagement'
import { selectCurrentProperty } from '@smartcoop/stores/property/selectorProperty'

import { AnimalActions, AnimalTypes } from './duckAnimal'
import { selectCurrentAnimal } from './selectorAnimal'

function* loadAnimals({ params = {}, onSuccess= () => {}, onError = () => {} }) {
  try {
    const currentProperty = yield select(selectCurrentProperty)
    const propertyId = currentProperty?.id

    const { data: { data, ...pagination } } = yield call(
      getAnimalsService,
      {
        limit: process.env.REACT_APP_FAKE_PAGINATION_SIZE || REACT_APP_FAKE_PAGINATION_SIZE,
        ...params
      },
      { propertyId }
    )

    yield put(AnimalActions.loadAnimalsSuccess(
      data,
      pagination.page,
      () => onSuccess(data)
    ))
  } catch (err) {
    const error = err.message
    yield put(AnimalActions.animalError(error))
    yield call(onError, error)
  }
}


function* loadAnimalsSuccess({ onSuccess = () => {} }) {
  yield call(onSuccess)
}

function* saveAnimal({ params, onSuccess = () => {}, onError = () => {} }) {
  try {
    const currentProperty = yield select(selectCurrentProperty)

    const { data } = yield call(
      isEmpty(params?.id) ? createAnimalService : editAnimalService,
      params,
      {
        animalId: params?.id ?? null,
        propertyId: currentProperty?.id
      }
    )
    yield call(onSuccess, data)
  } catch (err) {
    const error = err.message
    yield put(AnimalActions.animalError(error))
    yield call(onError, error)
  }
}

function* deleteAnimal({ animalId, onSuccess = () => {}, onError = () => {} }) {
  try {
    const currentProperty = yield select(selectCurrentProperty)
    const propertyId = currentProperty?.id

    yield call(deleteAnimalService, { propertyId, animalId })
    yield call(onSuccess)
  } catch (err) {
    const error = err.message
    yield put(AnimalActions.animalError(error))
    yield call(onError, error)
  }
}


function* loadAnimalPevList({ params = {}, onSuccess= () => {}, onError = () => {} }) {
  try {
    const currentAnimal = yield select(selectCurrentAnimal)

    const { data: { data, ...pagination } } = yield call(
      getAnimalPevList,
      {
        limit: process.env.REACT_APP_FAKE_PAGINATION_SIZE || REACT_APP_FAKE_PAGINATION_SIZE,
        ...params
      },
      { animalId: currentAnimal.id }
    )

    yield put(AnimalActions.loadAnimalPevListSuccess(
      data,
      pagination.page,
      () => onSuccess(data)
    ))
  } catch (err) {
    const error = err.message
    yield put(AnimalActions.animalError(error))
    yield call(onError, error)
  }
}


function* loadAnimalPevListSuccess({ onSuccess = () => {} }) {
  yield call(onSuccess)
}

function* loadCurrentAnimal({ onSuccess = () => {}, onError = () => {} }) {
  try {
    const { id: propertyId } = yield select(selectCurrentProperty)
    const { id: animalId } = yield select(selectCurrentAnimal)

    const { data } = yield call(getAnimalService, {}, { propertyId, animalId })
    yield put(AnimalActions.loadCurrentAnimalSuccess(data))
    yield call(onSuccess, data)
  } catch (err) {
    const error = err.message
    yield put(AnimalActions.animalError(error))
    yield call(onError, error)
  }
}


function* saveAnimalPev({ pev, onSuccess = () => {}, onError = () => {} }) {
  try {
    const currentProperty = yield select(selectCurrentProperty)
    const currentAnimal = yield select(selectCurrentAnimal)

    const { data } = yield call(
      isEmpty(pev?.id) ? createAnimalPevService : editAnimalPevService,
      pev,
      {
        propertyId: currentProperty?.id,
        animalId: currentAnimal?.id ?? null,
        pevId: pev?.id ?? null
      }
    )
    yield call(onSuccess, data)
  } catch (err) {
    const error = err.message
    yield put(AnimalActions.animalError(error))
    yield call(onError, error)
  }
}

function* deleteAnimalPev({ pevId, onSuccess = () => {}, onError = () => {} }) {
  try {
    yield call(deleteAnimalPevService, { pevId })
    yield call(onSuccess)
  } catch (err) {
    const error = err.message
    yield put(AnimalActions.animalError(error))
    yield call(onError, error)
  }
}

function* resetCurrentAnimal() {
  yield put(AnimalActions.resetAnimalPevList())
}

export default [
  takeLatest(AnimalTypes.LOAD_ANIMALS, loadAnimals),
  takeLatest(AnimalTypes.LOAD_ANIMALS_SUCCESS, loadAnimalsSuccess),

  takeLatest(AnimalTypes.LOAD_CURRENT_ANIMAL, loadCurrentAnimal),

  takeLatest(AnimalTypes.SAVE_ANIMAL, saveAnimal),
  takeLatest(AnimalTypes.DELETE_ANIMAL, deleteAnimal),

  takeLatest(AnimalTypes.LOAD_ANIMAL_PEV_LIST, loadAnimalPevList),
  takeLatest(AnimalTypes.LOAD_ANIMAL_PEV_LIST_SUCCESS, loadAnimalPevListSuccess),
  takeLatest(AnimalTypes.SAVE_ANIMAL_PEV, saveAnimalPev),
  takeLatest(AnimalTypes.DELETE_ANIMAL_PEV, deleteAnimalPev),

  takeLatest(AnimalTypes.RESET_CURRENT_ANIMAL, resetCurrentAnimal)
]
