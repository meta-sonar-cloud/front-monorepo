import { markActionsOffline } from 'redux-offline-queue'
import { createActions, createReducer } from 'reduxsauce'

import findIndex from 'lodash/findIndex'
import isEmpty from 'lodash/isEmpty'
import toNumber from 'lodash/toNumber'

import { AuthenticationTypes } from '../authentication'

const INITIAL_PROPERTY = {
  data: {
    name: '',
    totalArea: undefined,
    geolocalization: {
      latitude: undefined,
      longitude: undefined
    },
    state: '',
    city: '',
    addressComplement: '',
    id: null
  },
  activities: [],
  activitiesDetails: {}
}

/* Initial State */
const INITIAL_STATE = {
  properties: [],
  propertyPendingManagements: [],
  currentProperty: {},
  offlineProperty: { ...INITIAL_PROPERTY },
  activities: [],
  suggestPropertyRegister: false,
  reloadData: false,
  error: null
}

/**
 * Creating actions and types with reduxsauce.
 */
const { Types, Creators } = createActions({
  loadProperties: ['onSuccess', 'onError'],
  loadPropertiesSuccess: ['properties', 'onSuccess'],

  reloadCurrentProperty: ['onSuccess', 'onError'],

  updateOfflineProperty: ['property'],
  saveOfflineProperty: [],
  resetOfflineProperty: [],

  saveProperty: ['property'],

  deleteProperty: ['propertyId'],
  onDeletePropertySuccess: ['propertyId'],

  editProperty: ['propertyId', 'propertyData'],
  setEditPropertyData: ['propertyId', 'propertyData'],

  saveCurrentProperty: ['currentProperty'],
  resetCurrentProperty: [],

  loadActivities: [],
  loadActivitiesSuccess: ['activities'],

  loadPropertyPendingManagements: ['propertyId', 'onSuccess', 'onError'],
  loadPropertyPendingManagementsSuccess: ['propertyPendingManagements'],

  setReloadData: ['flag'],

  propertyError: ['error']
})

/**
 * Assigning offline actions
 */
markActionsOffline(Creators, ['saveProperty'])

/**
 * Reducers functions
 */
const loadPropertiesSuccess = (state = INITIAL_STATE, { properties }) => ({
  ...state,
  error: INITIAL_STATE.error,
  properties,
  suggestPropertyRegister: isEmpty(properties)
})

const updateOfflineProperty = (state = INITIAL_STATE, { property }) => {
  let { totalArea } = state.offlineProperty.data
  if (property.data?.totalArea) {
    totalArea = toNumber(property.data.totalArea)
  }
  return {
    ...state,
    offlineProperty: {
      ...state.offlineProperty,
      ...property,
      data: {
        ...state.offlineProperty.data,
        ...(property.data || {}),
        totalArea
      }
    }
  }
}

const resetOfflineProperty = (state = INITIAL_STATE) => ({
  ...state,
  offlineProperty: { ...INITIAL_PROPERTY }
})

const loadActivitiesSuccess = (state = INITIAL_STATE, { activities }) => ({
  ...state,
  error: INITIAL_STATE.error,
  activities
})

const loadPropertyPendingManagementsSuccess = (
  state = INITIAL_STATE,
  { propertyPendingManagements }
) => ({
  ...state,
  error: INITIAL_STATE.error,
  propertyPendingManagements
})

const propertyError = (state = INITIAL_STATE, { error }) => ({
  ...state,
  error
})

const saveCurrentProperty = (state = INITIAL_STATE, { currentProperty }) => ({
  ...state,
  currentProperty,
  propertyPendingManagements: INITIAL_STATE.propertyPendingManagements
})

const resetCurrentProperty = (state = INITIAL_STATE) => ({
  ...state,
  currentProperty: INITIAL_STATE.currentProperty,
  propertyPendingManagements: INITIAL_STATE.propertyPendingManagements
})

const logout = () => ({ ...INITIAL_STATE })

const onDeletePropertySuccess = (state = INITIAL_STATE, { propertyId }) => {
  const propertyIndex = findIndex(
    state.properties,
    (property) => property.id === propertyId
  )

  if (propertyId === -1) return state

  const properties = [...state.properties]
  properties.splice(propertyIndex, 1)

  return {
    ...state,
    properties
  }
}

const setEditPropertyData = (
  state = INITIAL_STATE,
  { propertyId, propertyData }
) => ({
  ...state,
  offlineProperty: {
    data: {
      name: propertyData?.name,
      totalArea: propertyData?.totalArea,
      geolocalization: {
        latitude: propertyData?.geolocalization?.latitude,
        longitude: propertyData?.geolocalization?.longitude
      },
      state: propertyData?.state,
      city: propertyData?.city,
      addressComplement: propertyData?.addressComplement,
      id: propertyId
    },
    activities: propertyData?.activities,
    activitiesDetails: propertyData?.activities
  }
})

const setReloadData = (state = INITIAL_STATE, { flag }) => ({
  ...state,
  reloadData: !!flag
})

/**
 * Creating reducer with Types.
 */
export default createReducer(INITIAL_STATE, {
  [Types.LOAD_PROPERTIES_SUCCESS]: loadPropertiesSuccess,

  [Types.UPDATE_OFFLINE_PROPERTY]: updateOfflineProperty,
  [Types.RESET_OFFLINE_PROPERTY]: resetOfflineProperty,

  [Types.LOAD_ACTIVITIES_SUCCESS]: loadActivitiesSuccess,

  [Types.LOAD_PROPERTY_PENDING_MANAGEMENTS_SUCCESS]: loadPropertyPendingManagementsSuccess,

  [Types.SAVE_CURRENT_PROPERTY]: saveCurrentProperty,
  [Types.RESET_CURRENT_PROPERTY]: resetCurrentProperty,

  [Types.PROPERTY_ERROR]: propertyError,

  [Types.ON_DELETE_PROPERTY_SUCCESS]: onDeletePropertySuccess,
  [Types.SET_EDIT_PROPERTY_DATA]: setEditPropertyData,

  [Types.SET_RELOAD_DATA]: setReloadData,

  [AuthenticationTypes.LOGOUT]: logout
})

export { Types as PropertyTypes, Creators as PropertyActions }
