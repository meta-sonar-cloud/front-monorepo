import { REACT_APP_FAKE_PAGINATION_SIZE } from 'react-native-dotenv'

import { call, put, takeLatest, select } from 'redux-saga/effects'

import { isEmpty } from 'lodash'
import map from 'lodash/map'
import size from 'lodash/size'

import { getActivities } from '@smartcoop/services/apis/smartcoopApi/resources/activity'
import {
  getProperties,
  createProperty,
  editProperty,
  deleteProperty as doDeleteProperty,
  getPendingManagements,
  getProperty
} from '@smartcoop/services/apis/smartcoopApi/resources/property'
import { FieldActions } from '@smartcoop/stores/field'
import { selectModuleIsTechnical } from '@smartcoop/stores/module/selectorModule'
import {
  selectCurrentProperty,
  selectOfflineProperty
} from '@smartcoop/stores/property/selectorProperty'
import { TechnicalActions, TechnicalTypes } from '@smartcoop/stores/technical'
import { selectCurrentOwner } from '@smartcoop/stores/technical/selectorTechnical'

import { selectAuthenticated } from '../authentication/selectorAuthentication'
import { PropertyActions, PropertyTypes } from './duckProperty'

function* loadProperties({ onSuccess = () => {}, onError = () => {} }) {
  try {
    const currentOwner = yield select(selectCurrentOwner)
    const moduleIsTechnical = yield select(selectModuleIsTechnical)

    if (moduleIsTechnical) {
      yield put(TechnicalActions.loadPropertiesByOwner(currentOwner, onSuccess, onError))
    } else {
      const { data } = yield call(getProperties, {
        limit:
          process.env.REACT_APP_FAKE_PAGINATION_SIZE ||
          REACT_APP_FAKE_PAGINATION_SIZE
      })

      if (size(data) === 1) {
        yield put(PropertyActions.saveCurrentProperty(data[0]))
      }

      yield put(PropertyActions.loadPropertiesSuccess(data, onSuccess))
    }
  } catch (err) {
    const error = err.message
    yield put(PropertyActions.propertyError(error))
    yield call(onError, error)
  }
}
function* loadPropertiesSuccess({ onSuccess }) {
  yield call(onSuccess)
}

function* reloadCurrentProperty({ onSuccess = () => {}, onError = () => {} }) {
  try {
    yield put(PropertyActions.loadProperties())
    const currentProperty = yield select(selectCurrentProperty)

    if (!isEmpty(currentProperty)) {
      const { data } = yield call(
        getProperty,
        {},
        { propertyId: currentProperty.id }
      )

      yield put(PropertyActions.saveCurrentProperty(data))

      yield call(onSuccess, data)
    }
  } catch (err) {
    const error = err.message
    yield put(PropertyActions.propertyError(error))
    yield call(onError, error)
  }
}

function* saveCurrentProperty() {
  yield put(FieldActions.resetFields())
}

function* saveOfflineProperty() {
  try {
    const { data, activitiesDetails } = yield select(selectOfflineProperty)

    const activities = map(activitiesDetails, (details, id) => ({
      id,
      details
    }))
    const body = { ...data, activities }

    yield put(PropertyActions.saveProperty(body))

    if (!data.id) {
      yield put(PropertyActions.resetOfflineProperty())
    }
  } catch (err) {
    const error = err.message
    yield put(PropertyActions.propertyError(error))
  }
}

function* saveProperty({ property }) {
  try {
    const { data } = yield call(
      property.id ? editProperty : createProperty,
      property,
      { propertyId: property.id }
    )
    yield put(PropertyActions.saveCurrentProperty(data))

    yield put(PropertyActions.loadProperties())
  } catch (err) {
    const error = err.message
    yield put(PropertyActions.propertyError(error))
  }
}

function* loadActivities() {
  try {
    const { data } = yield call(getActivities, {
      limit:
        process.env.REACT_APP_FAKE_PAGINATION_SIZE ||
        REACT_APP_FAKE_PAGINATION_SIZE
    })
    yield put(PropertyActions.loadActivitiesSuccess(data))
  } catch (err) {
    const error = err.message
    yield put(PropertyActions.propertyError(error))
  }
}

function* loadPropertyPendingManagements({
  propertyId,
  onSuccess = () => {},
  onError = () => {}
}) {
  try {
    const data = yield call(getPendingManagements, { propertyId })
    const authenticated = yield select(selectAuthenticated)
    if (authenticated) {
      yield put(PropertyActions.loadPropertyPendingManagementsSuccess(data))
      yield call(onSuccess)
    }
  } catch (err) {
    const error = err.message
    yield put(PropertyActions.propertyError(error))
    yield call(onError, err)
  }
}

function* deleteProperty({ propertyId }) {
  try {
    yield call(doDeleteProperty, { propertyId })

    yield put(PropertyActions.onDeletePropertySuccess(propertyId))
  } catch (err) {
    const error = err.message
    yield put(PropertyActions.propertyError(error))
  }
}

function* patchProperty({ propertyId, propertyData }) {
  try {
    yield call(editProperty, propertyData, { propertyId })

    yield put(PropertyActions.onEditPropertySuccess(propertyId))
  } catch (err) {
    const error = err.message
    yield put(PropertyActions.propertyError(error))
  }
}

function* resetTechnicalCurrentOwner() {
  yield put(PropertyActions.resetCurrentProperty())
}

export default [
  takeLatest(PropertyTypes.LOAD_PROPERTIES, loadProperties),
  takeLatest(PropertyTypes.LOAD_PROPERTIES_SUCCESS, loadPropertiesSuccess),
  takeLatest(PropertyTypes.RELOAD_CURRENT_PROPERTY, reloadCurrentProperty),
  takeLatest(PropertyTypes.SAVE_OFFLINE_PROPERTY, saveOfflineProperty),
  takeLatest(PropertyTypes.SAVE_PROPERTY, saveProperty),
  takeLatest(PropertyTypes.SAVE_CURRENT_PROPERTY, saveCurrentProperty),
  takeLatest(PropertyTypes.LOAD_ACTIVITIES, loadActivities),
  takeLatest(
    PropertyTypes.LOAD_PROPERTY_PENDING_MANAGEMENTS,
    loadPropertyPendingManagements
  ),
  takeLatest(PropertyTypes.DELETE_PROPERTY, deleteProperty),
  takeLatest(PropertyTypes.EDIT_PROPERTY, patchProperty),
  takeLatest(
    TechnicalTypes.RESET_TECHNICAL_CURRENT_OWNER,
    resetTechnicalCurrentOwner
  )
]
