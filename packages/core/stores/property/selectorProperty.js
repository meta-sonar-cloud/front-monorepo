import { selectModuleIsTechnical } from '@smartcoop/stores/module/selectorModule'
import { selectPropertiesByOwner } from '@smartcoop/stores/technical/selectorTechnical'

export const selectProperties = state => {
  const moduleIsTechnical = selectModuleIsTechnical(state)
  if (moduleIsTechnical) {
    return selectPropertiesByOwner(state)
  }
  return state.property.properties
}

export const selectCurrentProperty = state => state.property.currentProperty

export const selectOfflineProperty = state => state.property.offlineProperty

export const selectReloadData = state => state.property.reloadData

export const selectActivities = state => state.property.activities

export const selectSuggestPropertyRegister = state => {
  const moduleIsTechnical = selectModuleIsTechnical(state)
  return !moduleIsTechnical && state.property.suggestPropertyRegister
}

export const selectPropertyPendingManagements = state => state.property.propertyPendingManagements
