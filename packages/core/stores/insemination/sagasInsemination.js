import { REACT_APP_FAKE_PAGINATION_SIZE } from 'react-native-dotenv'

import { select, call, put, takeLatest } from 'redux-saga/effects'

import { isEmpty } from 'lodash'

import {
  getInseminations as getInseminationsService,
  createInsemination as createInseminationService,
  updateInsemination as updateInseminationService,
  deleteInsemination as deleteInseminationService,
  getInseminationTypes as getInseminationTypesService
} from '@smartcoop/services/apis/smartcoopApi/resources/insemination'
import { selectCurrentAnimal } from '@smartcoop/stores/animal/selectorAnimal'

import {
  InseminationActions,
  InseminationTypes
} from './duckInsemination'

function* loadInseminations({ params = {}, onSuccess = () => {}, onError = () => {} }) {
  try {
    const currentAnimal = yield select(selectCurrentAnimal)
    const { id } = currentAnimal
    const { data: { data, ...pagination } } = yield call(
      getInseminationsService,
      {
        limit: process.env.REACT_APP_FAKE_PAGINATION_SIZE || REACT_APP_FAKE_PAGINATION_SIZE,
        ...params
      },
      { animalId: id }
    )

    yield put(InseminationActions.loadInseminationsSuccess(
      data,
      pagination.page,
      onSuccess(data)
    ))
  } catch (err) {
    const error = err.message
    yield put(InseminationActions.inseminationError(error))
    yield call(onError, error)
  }
}

function* saveInsemination({ params, onSuccess = () => {}, onError = () => {} }) {
  try {
    const currentAnimal = yield select(selectCurrentAnimal)
    const { id } = currentAnimal
    const service = isEmpty(params.id) ? createInseminationService : updateInseminationService
    const data = yield call(service, params, { animalId: id, inseminationId: params?.id })
    yield call(onSuccess, data)
  } catch (error) {
    yield put(InseminationActions.inseminationError(error))
    yield call(onError, error)
  }
}

function* deleteInsemination({ inseminationId, onSuccess = () => {}, onError = () => {} }) {
  try {
    const currentAnimal = yield select(selectCurrentAnimal)
    const { id } = currentAnimal

    yield call(deleteInseminationService, {}, { animalId: id, inseminationId })
    yield call(onSuccess)
  } catch (err) {
    const error = err.message
    yield put(InseminationActions.inseminationError(error))
    yield call(onError, error)
  }
}

function* loadInseminationTypes() {
  try {
    const { id } = yield select(selectCurrentAnimal)
    const { data: types } = yield call(getInseminationTypesService, {}, { animalId: id })
    yield put(InseminationActions.loadInseminationTypesSuccess(types, { animalId: id }))
  } catch (err) {
    const error = err.message
    yield put(InseminationActions.inseminationError(error))
  }
}

function* loadCurrentInsemination({ params }) {
  try {
    const { id } = yield select(selectCurrentAnimal)
    const { data } = yield call(getInseminationsService,
      {
        limit: 1,
        orderBy: '-insemination_date',
        ...params
      }, { animalId: id })

    const currentInsemination = data.data[0]
    yield put(InseminationActions.loadCurrentInseminationSuccess(currentInsemination, { animalId: id }))
  } catch (err) {
    const error = err.message
    yield put(InseminationActions.inseminationError(error))
  }
}

export default [
  takeLatest(InseminationTypes.LOAD_INSEMINATIONS, loadInseminations),

  takeLatest(InseminationTypes.LOAD_CURRENT_INSEMINATION, loadCurrentInsemination),

  takeLatest(InseminationTypes.LOAD_INSEMINATION_TYPES, loadInseminationTypes),

  takeLatest(InseminationTypes.SAVE_INSEMINATION, saveInsemination),
  takeLatest(InseminationTypes.DELETE_INSEMINATION, deleteInsemination)
]
