import { REACT_APP_FAKE_PAGINATION_SIZE } from 'react-native-dotenv'

import { call, put, select, takeLatest } from 'redux-saga/effects'

import isEmpty from 'lodash/isEmpty'

import { getPropertiesByOwner } from '@smartcoop/services/apis/smartcoopApi/resources/property'
import {
  registerTechnicalVisit,
  deleteVisit,
  getTechnicalVisits,
  getTechniciansRequestList,
  requestAccessForProprietary,
  revokeAccess,
  acceptAccessTechnical,
  getTechnicalPortfolio,
  editTechnicalVisit
} from '@smartcoop/services/apis/smartcoopApi/resources/technical'
import { selectCurrentField } from '@smartcoop/stores/field/selectorField'
import { selectCurrentProperty } from '@smartcoop/stores/property/selectorProperty'

import { TechnicalActions, TechnicalTypes } from './duckTechnical'

function* saveTechnicalVisit({
  technicalVisit,
  onSuccess = () => {},
  onError = () => {}
}) {
  try {
    const currentProperty = yield select(selectCurrentProperty)
    const { growingSeason } = yield select(selectCurrentField)
    const field = yield select(selectCurrentField)

    yield call(
      technicalVisit.id ? editTechnicalVisit : registerTechnicalVisit,
      {
        ...technicalVisit,
        cropId: growingSeason ? growingSeason.cropId : technicalVisit.cropId,
        growingSeasonId: growingSeason
          ? growingSeason.id
          : technicalVisit.growingSeasonId,
        fieldId: field.id ? field.id : technicalVisit.fieldId
      },
      !isEmpty(technicalVisit?.id)
        ? { visitId: technicalVisit.id }
        : { propertyId: currentProperty.id }
    )
    yield call(onSuccess)
  } catch (err) {
    const error = err.message
    yield put(TechnicalActions.technicalError(error))
    yield call(onError, error)
  }
}

function* requestAccessTechnicalForProprietary({
  propertyId,
  onSuccess = () => {},
  onError = () => {}
}) {
  try {
    yield call(requestAccessForProprietary, propertyId)
    yield call(onSuccess)
  } catch (err) {
    const error = err.message
    yield put(TechnicalActions.technicalError(error))
    yield call(onError, error)
  }
}

function* acceptTechnicianAccess({
  permission,
  accessRequestId,
  onSuccess = () => {},
  onError = () => {}
}) {
  try {
    yield call(
      acceptAccessTechnical,
      { write: permission },
      { accessRequestId }
    )
    yield call(onSuccess)
  } catch (error) {
    yield put(TechnicalActions.technicalError(error))
    yield call(onError, error)
  }
}

function* loadTechnicalVisits({
  params = {},
  onSuccess = () => {},
  onError = () => {}
}) {
  try {
    const {
      data: { data, ...pagination }
    } = yield call(getTechnicalVisits, params)

    yield put(
      TechnicalActions.loadTechnicalVisitsSuccess(data, pagination.page, () =>
        onSuccess(pagination)
      )
    )
  } catch (err) {
    const error = err.message
    yield put(TechnicalActions.technicalError(error))
    yield call(onError, error)
  }
}

function* loadTechnicalVisitsSuccess({ onSuccess }) {
  yield call(onSuccess)
}

function* loadRequestAccessList({
  params = {},
  onSuccess = () => {},
  onError = () => {}
}) {
  try {
    const {
      data: { data, ...pagination }
    } = yield call(getTechniciansRequestList, params)

    yield put(
      TechnicalActions.loadRequestAccessListSuccess(data, pagination.page, () =>
        onSuccess(pagination)
      )
    )
  } catch (err) {
    const error = err.message
    yield put(TechnicalActions.technicalError(error))
    yield call(onError, error)
  }
}

function* loadRequestAccessListSuccess({ onSuccess }) {
  yield call(onSuccess)
}

function* loadTechnicalOwners({
  params = {},
  onSuccess = () => {},
  onError = () => {}
}) {
  try {
    const {
      data: { data, ...pagination }
    } = yield call(getTechnicalPortfolio, params)

    yield put(
      TechnicalActions.loadTechnicalOwnersSuccess(data, pagination.page, () =>
        onSuccess(pagination)
      )
    )
  } catch (err) {
    const error = err.message
    yield put(TechnicalActions.technicalError(error))
    yield call(onError, error)
  }
}

function* loadTechnicalOwnersSuccess({ onSuccess }) {
  yield call(onSuccess)
}

function* deleteTechnicalVisit({
  technicalVisitId,
  onSuccess = () => {},
  onError = () => {}
}) {
  try {
    yield call(deleteVisit, { technicalVisitId })
    yield call(onSuccess)
  } catch (err) {
    const error = err.message
    yield put(TechnicalActions.technicalError(error))
    yield call(onError, error)
  }
}

function* revokeTechnicalAccess({
  id,
  statusId,
  onSuccess = () => {},
  onError = () => {}
}) {
  try {
    yield call(revokeAccess, { id, statusId })
    yield call(onSuccess)
  } catch (err) {
    const error = err.message
    yield put(TechnicalActions.technicalError(error))
    yield call(onError, error)
  }
}

function* setTechnicalCurrentOwner({
  currentOwner,
  onSuccess = () => {},
  onError = () => {}
}) {
  yield put(
    TechnicalActions.loadPropertiesByOwner(currentOwner, onSuccess, onError)
  )
}

function* loadPropertiesByOwner({
  currentOwner,
  onSuccess = () => {},
  onError = () => {}
}) {
  try {
    const { id: currentOwnerId } = currentOwner

    // request pra descobrir as propriedades
    const {
      data: { data: properties }
    } = yield call(
      getPropertiesByOwner,
      {
        limit:
          process.env.REACT_APP_FAKE_PAGINATION_SIZE ||
          REACT_APP_FAKE_PAGINATION_SIZE
      },
      { ownerId: currentOwnerId }
    )
    if (properties.length > 0) {
      yield put(TechnicalActions.setTechnicalCurrentOwnerSuccess(currentOwner))
      yield put(TechnicalActions.loadPropertiesByOwnerSuccess(properties))
      yield call(onSuccess)
    } else {
      yield call(onError, { message: 'producer has no property' })
    }
  } catch (err) {
    const error = err.message
    yield put(TechnicalActions.technicalError(error))
    yield call(onError, error)
  }
}

export default [
  takeLatest(TechnicalTypes.SAVE_TECHNICAL_VISIT, saveTechnicalVisit),
  takeLatest(
    TechnicalTypes.REQUEST_ACCESS_TECHNICAL_FOR_PROPRIETARY,
    requestAccessTechnicalForProprietary
  ),
  takeLatest(TechnicalTypes.LOAD_TECHNICAL_VISITS, loadTechnicalVisits),
  takeLatest(
    TechnicalTypes.LOAD_TECHNICAL_VISITS_SUCCESS,
    loadTechnicalVisitsSuccess
  ),
  takeLatest(TechnicalTypes.LOAD_REQUEST_ACCESS_LIST, loadRequestAccessList),
  takeLatest(
    TechnicalTypes.LOAD_REQUEST_ACCESS_LIST_SUCCESS,
    loadRequestAccessListSuccess
  ),
  takeLatest(TechnicalTypes.LOAD_TECHNICAL_OWNERS, loadTechnicalOwners),
  takeLatest(
    TechnicalTypes.LOAD_TECHNICAL_OWNERS_SUCCESS,
    loadTechnicalOwnersSuccess
  ),
  takeLatest(TechnicalTypes.ACCEPT_TECHNICIAN_ACCESS, acceptTechnicianAccess),
  takeLatest(TechnicalTypes.DELETE_TECHNICAL_VISIT, deleteTechnicalVisit),
  takeLatest(TechnicalTypes.REVOKE_TECHNICAL_ACCESS, revokeTechnicalAccess),
  takeLatest(
    TechnicalTypes.SET_TECHNICAL_CURRENT_OWNER,
    setTechnicalCurrentOwner
  ),
  takeLatest(TechnicalTypes.LOAD_PROPERTIES_BY_OWNER, loadPropertiesByOwner)
]
