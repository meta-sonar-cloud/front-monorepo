export const selectTechnicalVisit = state => state.technical.technicalVisit

export const selectTechnicalVisits = state => state.technical.technicalVisits

export const selectRequestAccessList = state => state.technical.requestAccessList

export const selectCurrentOwner = state => state.technical.currentOwner

export const selectCurrentOwnerId = state => state.technical.currentOwner?.id

export const selectPropertiesByOwner = state => {
  const { id: ownerId } = selectCurrentOwner(state)
  return state.technical.propertiesByOwner[ownerId]
}

export const selectTechnicalOwners = state => state.technical.owners

export const selectTechnicalWrite = state => state.technical.currentOwner?.proprietaryTechnician?.write
