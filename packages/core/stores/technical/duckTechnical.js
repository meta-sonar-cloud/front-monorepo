import { createActions, createReducer } from 'reduxsauce'

import { AuthenticationTypes } from '../authentication'

/* Initial State */
const INITIAL_STATE = {
  technicalVisits: [],
  technicalVisit: {},
  currentOwner: {},
  propertiesByOwner: {},
  requestAccessList: [],
  owners: [],
  error: null
}

/**
 * Creating actions and types with reduxsauce.
 */
const { Types, Creators } = createActions({
  loadPropertiesByOwner: ['currentOwner', 'onSuccess', 'onError'],
  loadPropertiesByOwnerSuccess: ['properties'],

  saveTechnicalVisit: ['technicalVisit', 'onSuccess', 'onError'],

  requestAccessTechnicalForProprietary: ['propertyId', 'onSuccess', 'onError'],

  acceptTechnicianAccess: [
    'permission',
    'accessRequestId',
    'onSuccess',
    'onError'
  ],

  loadTechnicalVisitById: ['params', 'onSuccess', 'onError'],
  loadTechnicalVisitByIdSuccess: ['technicalVisit', 'onSuccess'],

  loadTechnicalVisits: ['params', 'onSuccess', 'onError'],
  loadTechnicalVisitsSuccess: ['technicalVisits', 'page', 'onSuccess'],

  loadTechnicalOwners: ['params', 'onSuccess', 'onError'],
  loadTechnicalOwnersSuccess: ['owners', 'page', 'onSuccess'],

  loadRequestAccessList: ['params', 'onSuccess', 'onError'],
  loadRequestAccessListSuccess: ['requestAccessList', 'page', 'onSuccess'],

  deleteTechnicalVisit: ['technicalVisitId', 'onSuccess', 'onError'],

  revokeTechnicalAccess: ['id', 'statusId', 'onSuccess', 'onError'],

  setTechnicalCurrentOwner: ['currentOwner', 'onSuccess', 'onError'],
  setTechnicalCurrentOwnerSuccess: ['currentOwner'],
  resetTechnicalCurrentOwner: [],

  technicalError: ['error']
})

const loadPropertiesByOwnerSuccess = (
  state = INITIAL_STATE,
  { properties }
) => ({
  ...state,
  propertiesByOwner: {
    ...state.propertiesByOwner,
    [state.currentOwner.id]: properties
  }
})

const saveTechnicalVisit = (state = INITIAL_STATE, { technicalVisit }) => ({
  ...state,
  technicalVisits: [...state.technicalVisits, technicalVisit]
})

const loadTechnicalVisitByIdSuccess = (
  state = INITIAL_STATE,
  { technicalVisit }
) => ({
  ...state,
  error: INITIAL_STATE.error,
  technicalVisit
})

const loadTechnicalVisitsSuccess = (
  state = INITIAL_STATE,
  { technicalVisits, page }
) => ({
  ...state,
  error: INITIAL_STATE.error,
  technicalVisits:
    page === 1
      ? technicalVisits
      : [...state.technicalVisits, ...technicalVisits]
})

const loadRequestAccessListSuccess = (
  state = INITIAL_STATE,
  { requestAccessList, page }
) => ({
  ...state,
  error: INITIAL_STATE.error,
  requestAccessList:
    page === 1
      ? requestAccessList
      : [...state.requestAccessList, ...requestAccessList]
})

const loadTechnicalOwnersSuccess = (
  state = INITIAL_STATE,
  { owners, page }
) => ({
  ...state,
  error: INITIAL_STATE.error,
  owners: page === 1 ? owners : [...state.owners, ...owners]
})

const setTechnicalCurrentOwnerSuccess = (
  state = INITIAL_STATE,
  { currentOwner }
) => ({
  ...state,
  currentOwner
})

const resetTechnicalCurrentOwner = (state = INITIAL_STATE) => ({
  ...state,
  currentOwner: INITIAL_STATE.currentOwner
})

const technicalError = (state = INITIAL_STATE, { error }) => ({
  ...state,
  error
})

const logout = () => ({ ...INITIAL_STATE })

/**
 * Creating reducer with Types.
 */
export default createReducer(INITIAL_STATE, {
  [Types.LOAD_TECHNICAL_VISIT_BY_ID_SUCCESS]: loadTechnicalVisitByIdSuccess,

  [Types.LOAD_TECHNICAL_VISITS_SUCCESS]: loadTechnicalVisitsSuccess,

  [Types.LOAD_REQUEST_ACCESS_LIST_SUCCESS]: loadRequestAccessListSuccess,

  [Types.LOAD_TECHNICAL_OWNERS_SUCCESS]: loadTechnicalOwnersSuccess,

  [Types.LOAD_PROPERTIES_BY_OWNER_SUCCESS]: loadPropertiesByOwnerSuccess,

  [Types.SAVE_TECHNICAL_VISIT]: saveTechnicalVisit,

  [Types.SET_TECHNICAL_CURRENT_OWNER_SUCCESS]: setTechnicalCurrentOwnerSuccess,
  [Types.RESET_TECHNICAL_CURRENT_OWNER]: resetTechnicalCurrentOwner,

  [Types.TECHNICAL_ERROR]: technicalError,

  [AuthenticationTypes.LOGOUT]: logout
})

export { Types as TechnicalTypes, Creators as TechnicalActions }
