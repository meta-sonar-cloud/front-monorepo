export const selectAccounts = state => state.account.accounts
export const selectCurrentAccount = state => state.account.currentAccount
