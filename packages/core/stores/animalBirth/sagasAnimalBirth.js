import { REACT_APP_FAKE_PAGINATION_SIZE } from 'react-native-dotenv'

import { select, call, put, takeLatest } from 'redux-saga/effects'

import { isEmpty } from 'lodash'

import {
  createAnimalBirth as createAnimalBirthService,
  editAnimalBirth as editAnimalBirthService,
  getAnimalBirthsById as getAnimalBirthsByIdService,
  deleteAnimalBirth as deleteAnimalBirthService
} from '@smartcoop/services/apis/smartcoopApi/resources/animalBirth'
import { selectCurrentProperty } from '@smartcoop/stores/property/selectorProperty'

import { selectCurrentAnimal } from '../animal/selectorAnimal'
import { AnimalBirthActions, AnimalBirthTypes } from './duckAnimalBirth'

function* loadAnimalBirths({ params = {}, onSuccess= () => {}, onError = () => {} }) {
  try {
    const currentProperty = yield select(selectCurrentProperty)
    const currentAnimal = yield select(selectCurrentAnimal)
    const propertyId = currentProperty?.id
    const animalId = currentAnimal?.id

    const { data: { data, ...pagination } } = yield call(
      getAnimalBirthsByIdService,
      {
        limit: process.env.REACT_APP_FAKE_PAGINATION_SIZE || REACT_APP_FAKE_PAGINATION_SIZE,
        ...params
      },
      { propertyId, animalId }
    )

    yield put(AnimalBirthActions.loadAnimalBirthsSuccess(
      data,
      pagination.page,
      () => onSuccess(data)
    ))
  } catch (err) {
    const error = err.message
    yield put(AnimalBirthActions.animalBirthError(error))
    yield call(onError, error)
  }
}


function* loadAnimalBirthsSuccess({ onSuccess = () => {} }) {
  yield call(onSuccess)
}

function* saveAnimalBirth({ params, onSuccess = () => {}, onError = () => {} }) {
  try {
    const currentProperty = yield select(selectCurrentProperty)

    const { data } = yield call(
      isEmpty(params?.id) ? createAnimalBirthService : editAnimalBirthService,
      params,
      {
        animalBirthId: params?.id ?? null,
        propertyId: currentProperty?.id
      }
    )
    yield call(onSuccess, data)
  } catch (err) {
    const error = err.message
    yield put(AnimalBirthActions.animalBirthError(error))
    yield call(onError, error)
  }
}

function* deleteAnimalBirth({ animalBirthId, onSuccess = () => {}, onError = () => {} }) {
  try {
    const currentProperty = yield select(selectCurrentProperty)
    const propertyId = currentProperty?.id

    yield call(deleteAnimalBirthService, { propertyId, animalBirthId })
    yield call(onSuccess)
  } catch (err) {
    const error = err.message
    yield put(AnimalBirthActions.animalBirthError(error))
    yield call(onError, error)
  }
}

export default [
  takeLatest(AnimalBirthTypes.LOAD_ANIMAL_BIRTHS, loadAnimalBirths),
  takeLatest(AnimalBirthTypes.LOAD_ANIMAL_BIRTHS_SUCCESS, loadAnimalBirthsSuccess),

  takeLatest(AnimalBirthTypes.SAVE_ANIMAL_BIRTH, saveAnimalBirth),

  takeLatest(AnimalBirthTypes.DELETE_ANIMAL_BIRTH, deleteAnimalBirth)

]
