import { markActionsOffline } from 'redux-offline-queue'
import { createActions, createReducer } from 'reduxsauce'

import { AuthenticationTypes } from '../authentication/duckAuthentication'

// Initial state
const INITIAL_STATE = {
  pregnancyActions: [],
  error: null
}

/**
 * Creating actions and types with reduxsauce.
 */
const { Types, Creators } = createActions({
  loadDryings: ['params', 'onSuccess', 'onError'],
  loadDryingsSuccess: ['params', 'page', 'onSuccess'],

  saveDrying: ['params', 'onSuccess', 'onError'],
  deleteDrying: ['id', 'onSuccess', 'onError'],

  dryingError: ['error']
})

markActionsOffline(Creators, [
  'loadAnimals',
  'deleteAnimal'
])

/**
 * Reducers functions
 */

const loadDryingsSuccess = (state = INITIAL_STATE, { params, page }) => ({
  ...state,
  error: INITIAL_STATE.error,
  pregnancyActions: page === 1 ? params : [
    ...state.pregnancyActions,
    ...params
  ]
})

const dryingError = (state = INITIAL_STATE, { error }) => ({
  ...state,
  error
})

const logout = () => ({ ...INITIAL_STATE })

/**
 * Creating reducer with Types.
 */

export default createReducer(INITIAL_STATE, {
  [Types.LOAD_DRYINGS_SUCCESS]: loadDryingsSuccess,

  [Types.DRYING_ERROR]: dryingError,

  [AuthenticationTypes.LOGOUT]: logout
})

export {
  Types as AnimalPregnancyActionsTypes,
  Creators as AnimalPregnancyActionsActions
}
