import { REACT_APP_FAKE_PAGINATION_SIZE } from 'react-native-dotenv'

import { select, call, put, takeLatest } from 'redux-saga/effects'

import isEmpty from 'lodash/isEmpty'
import toString from 'lodash/toString'

import {
  getAllAnimalDrying as getAllAnimalDryingService,
  createAnimalDrying as createAnimalDryingService,
  editAnimalDrying as editAnimalDryingService,
  deleteAnimalDrying as deleteAnimalDryingService
} from '@smartcoop/services/apis/smartcoopApi/resources/animalPregnancyActions'
import { selectCurrentAnimal } from '@smartcoop/stores/animal/selectorAnimal'
import { selectCurrentProperty } from '@smartcoop/stores/property/selectorProperty'

import { AnimalPregnancyActionsActions, AnimalPregnancyActionsTypes } from './duckAnimalPregnancyActions'

function* loadDryings({ params = {}, onSuccess= () => {}, onError = () => {} }) {
  try {
    const currentProperty = yield select(selectCurrentProperty)
    const currentAnimal = yield select(selectCurrentAnimal)

    const { data: { data, ...pagination } } = yield call(
      getAllAnimalDryingService,
      {
        limit: process.env.REACT_APP_FAKE_PAGINATION_SIZE || REACT_APP_FAKE_PAGINATION_SIZE,
        ...params
      },
      { propertyId: currentProperty?.id, animalId: currentAnimal?.id }
    )

    yield put(AnimalPregnancyActionsActions.loadDryingsSuccess(
      data,
      pagination.page,
      () => onSuccess(data)
    ))
  } catch (err) {
    const error = err.message
    yield put(AnimalPregnancyActionsActions.dryingError(error))
    yield call(onError, error)
  }
}

function* loadDryingsSuccess({ onSuccess = () => {} }) {
  yield call(onSuccess)
}

function* saveDrying({ params, onSuccess = () => {}, onError = () => {} }) {
  try {
    const currentProperty = yield select(selectCurrentProperty)
    const dryingId = params?.id
    const { data } = yield call(
      isEmpty(toString(dryingId)) ? createAnimalDryingService : editAnimalDryingService,
      {
        animalId: params?.animalId,
        type: params?.type,
        lotId: params?.lotId,
        accomplishedDate: params?.accomplishedDate
      },
      { propertyId: currentProperty?.id, dryingId }

    )
    yield call(onSuccess, data)
  } catch (err) {
    const error = err.message
    yield put(AnimalPregnancyActionsActions.dryingError(error))
    yield call(onError, error)
  }
}

function* deleteDrying({ id, onSuccess = () => {}, onError = () => {} }) {
  try {
    const currentProperty = yield select(selectCurrentProperty)

    yield call(deleteAnimalDryingService, { propertyId: currentProperty?.id, dryingId: id })
    yield call(onSuccess)
  } catch (err) {
    const error = err.message
    yield put(AnimalPregnancyActionsActions.dryingError(error))
    yield call(onError, error)
  }
}

export default [
  takeLatest(AnimalPregnancyActionsTypes.LOAD_DRYINGS, loadDryings),
  takeLatest(AnimalPregnancyActionsTypes.LOAD_DRYINGS_SUCCESS, loadDryingsSuccess),

  takeLatest(AnimalPregnancyActionsTypes.SAVE_DRYING, saveDrying),

  takeLatest(AnimalPregnancyActionsTypes.DELETE_DRYING, deleteDrying)

]
