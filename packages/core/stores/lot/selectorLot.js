export const selectLots = (state) => state.lot.lots
export const selectLotsOptions = (state) => state.lot.lotsOptions
export const selectPreDefinedLots = (state) => state.lot.preDefinedLots
