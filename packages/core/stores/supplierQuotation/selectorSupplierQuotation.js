export const selectCurrentSupplierQuotation = state => state.supplierQuotation.currentSupplierQuotation

export const selectCurrentSupplierQuotationProposal = state => state.supplierQuotation.currentProposal

export const selectCheckboxesState = state => state.supplierQuotation.checkboxesState
