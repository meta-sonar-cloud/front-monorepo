import { call, put, select, takeLatest } from 'redux-saga/effects'

import {
  getUserProductsWithdraw,
  getPropertaryProductsWithdraw
} from '@smartcoop/services/apis/smartcoopApi/resources/productWithdraw'
import { selectCurrentOwnerId } from '@smartcoop/stores/technical/selectorTechnical'

import { selectCurrentOrganization } from '../organization/selectorOrganization'
import { ProductsWithdrawActions, ProductsWithdrawTypes } from './duckProductsWithdraw'

function* loadProductsWithdraw({ params = {}, onSuccess = () => {}, onError = () => {} }) {
  try {
    const currentOrganization = yield select(selectCurrentOrganization)
    const currentOwnerId = yield select(selectCurrentOwnerId)

    let url = getUserProductsWithdraw
    let urlParams = { organizationId: currentOrganization.id }

    if (params?.technical) {
      url = getPropertaryProductsWithdraw
      urlParams = { ...urlParams, userId: currentOwnerId }
    }

    const { data: { data: products, ...pagination } } = yield call(url, {
      limit: 10,
      ...params
    }, urlParams)

    yield put(ProductsWithdrawActions.loadProductsWithdrawSuccess(
      products,
      pagination.page,
      () => onSuccess(pagination)
    ))
  } catch (error) {
    yield put(ProductsWithdrawActions.productsWithdrawError(error))
    yield call(onError, error)
  }
}

function* loadProductsWithdrawSuccess({ onSuccess = () => {} }) {
  yield call(onSuccess)
}

export default [
  takeLatest(ProductsWithdrawTypes.LOAD_PRODUCTS_WITHDRAW, loadProductsWithdraw),
  takeLatest(ProductsWithdrawTypes.LOAD_PRODUCTS_WITHDRAW_SUCCESS, loadProductsWithdrawSuccess)
]
