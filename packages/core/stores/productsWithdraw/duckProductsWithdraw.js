import { createActions, createReducer } from 'reduxsauce'

import { AuthenticationTypes } from '../authentication'

/* Initial State */
const INITIAL_STATE = {
  products: [],
  error: null
}

/**
 * Creating actions and types with reduxsauce.
 */
const { Types, Creators } = createActions({
  loadProductsWithdraw: ['params', 'onSuccess', 'onError'],
  loadProductsWithdrawSuccess: ['products', 'page', 'onSuccess'],

  resetProductsWithdraw: [],

  productsWithdrawError: ['error']
})


const loadProductsWithdraw = (state = INITIAL_STATE) => ({
  ...state,
  error: INITIAL_STATE.error
})

const loadProductsWithdrawSuccess = (state = INITIAL_STATE, { products, page }) => ({
  ...state,
  products: page === 1 ? products : [
    ...state.products,
    ...products
  ]
})

const productsWithdrawError = (state = INITIAL_STATE, { error }) => ({
  ...state,
  error
})

const resetProductsWithdraw = (state = INITIAL_STATE) => ({
  ...state,
  products: INITIAL_STATE.productsWithdraw
})

const logout = () => ({ ...INITIAL_STATE })

/**
 * Creating reducer with Types.
 */
export default createReducer(INITIAL_STATE, {

  [Types.LOAD_PRODUCTS_WITHDRAW]: loadProductsWithdraw,
  [Types.LOAD_PRODUCTS_WITHDRAW_SUCCESS]: loadProductsWithdrawSuccess,

  [Types.RESET_PRODUCTS_WITHDRAW]: resetProductsWithdraw,

  [Types.PRODUCTS_WITHDRAW_ERROR]: productsWithdrawError,

  [AuthenticationTypes.LOGOUT]: logout
})

export {
  Types as ProductsWithdrawTypes,
  Creators as ProductsWithdrawActions
}
