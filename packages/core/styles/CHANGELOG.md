# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [0.18.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.17.0...v0.18.0) (2021-04-19)

**Note:** Version bump only for package @smartcoop/styles





# [0.17.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.16.0...v0.17.0) (2021-04-15)

**Note:** Version bump only for package @smartcoop/styles





# [0.16.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.15.0...v0.16.0) (2021-04-13)

**Note:** Version bump only for package @smartcoop/styles







**Note:** Version bump only for package @smartcoop/styles





# [0.15.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.12.0-alpha.1...v0.15.0) (2021-03-23)


### Features

* **documentos#1323:** new login image ([7b8a88a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7b8a88a5fd1ef60f010c4fcdfbd60a6ec9d44ccb)), closes [documentos#1323](http://gitlab.meta.com.br/documentos/issues/1323)





# [0.15.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.12.0-alpha.1...v0.15.0) (2021-03-23)


### Features

* **documentos#1323:** new login image ([7b8a88a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7b8a88a5fd1ef60f010c4fcdfbd60a6ec9d44ccb)), closes [documentos#1323](http://gitlab.meta.com.br/documentos/issues/1323)





# [0.15.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.12.0-alpha.1...v0.15.0) (2021-03-23)


### Features

* **documentos#1323:** new login image ([7b8a88a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7b8a88a5fd1ef60f010c4fcdfbd60a6ec9d44ccb)), closes [documentos#1323](http://gitlab.meta.com.br/documentos/issues/1323)





# [0.14.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.12.0-alpha.1...v0.14.0) (2021-03-23)


### Features

* **documentos#1323:** new login image ([7b8a88a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7b8a88a5fd1ef60f010c4fcdfbd60a6ec9d44ccb)), closes [documentos#1323](http://gitlab.meta.com.br/documentos/issues/1323)





# [0.13.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.12.0-alpha.1...v0.13.0) (2021-03-23)


### Features

* **documentos#1323:** new login image ([7b8a88a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7b8a88a5fd1ef60f010c4fcdfbd60a6ec9d44ccb)), closes [documentos#1323](http://gitlab.meta.com.br/documentos/issues/1323)





# [0.12.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.12.0-alpha.1...v0.12.0) (2021-03-23)


### Features

* **documentos#1323:** new login image ([7b8a88a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7b8a88a5fd1ef60f010c4fcdfbd60a6ec9d44ccb)), closes [documentos#1323](http://gitlab.meta.com.br/documentos/issues/1323)





# [0.12.0-alpha.1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.12.0-alpha.0...v0.12.0-alpha.1) (2021-02-22)

**Note:** Version bump only for package @smartcoop/styles





# [0.12.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.11.0...v0.12.0-alpha.0) (2021-02-12)

**Note:** Version bump only for package @smartcoop/styles





# [0.11.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.11.0-alpha.0...v0.11.0) (2021-02-12)

**Note:** Version bump only for package @smartcoop/styles





# [0.11.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.10.0...v0.11.0-alpha.0) (2021-02-05)

**Note:** Version bump only for package @smartcoop/styles





# [0.10.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.10.0-alpha.0...v0.10.0) (2021-02-01)


### Features

* **ajuste fino talhao:** ajuste fino talhao ([c67fd1f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c67fd1fba259c20f7b41ca91effa926c2273c899))





# [0.10.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.9.0...v0.10.0-alpha.0) (2021-01-19)

**Note:** Version bump only for package @smartcoop/styles





# [0.9.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.8.2...v0.9.0) (2021-01-19)


### Features

* merge Develop ([4a43e2a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4a43e2a79e976c5459355a3dd2e0892e8c081e24))
* wIP Social Comments ([9f3dad3](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9f3dad309defcb5115fd48a60d7158a466a06855))





## [0.8.1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.9.0-alpha.0...v0.8.1) (2021-01-06)

**Note:** Version bump only for package @smartcoop/styles





# [0.9.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.8.0...v0.9.0-alpha.0) (2021-01-05)

**Note:** Version bump only for package @smartcoop/styles





# [0.8.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.8.0-alpha.0...v0.8.0) (2021-01-05)


### Features

* **321:** previsao do tempo mobile ([0db0a4b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0db0a4b8761cd1deb3c498313fcd0de1e3409fd7))
* **ajustado background login:** ajustado background login ([d8a6955](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d8a695504c16b793d3e4a62c36ff33fa75d0ddf9))
* **documentos#688:** added chart to web ([84a8fcb](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/84a8fcb47c4d7a12a798460379ecc025c71806fb)), closes [documentos#688](http://gitlab.meta.com.br/documentos/issues/688)
* **documentos#688:** adjust field timeline chart ([003022c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/003022c800c0163a13a8abd396551e88f9691481)), closes [documentos#688](http://gitlab.meta.com.br/documentos/issues/688)
* **documentos#71:** added new color ([1257c9c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1257c9ca3bff50341a31183e0d6817e797ad19b2)), closes [documentos#71](http://gitlab.meta.com.br/documentos/issues/71)
* **documentos#79:** added rainMm into field timeline chart ([d258c00](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d258c0043f4b363a13b01870f70964a517b8bdbb)), closes [documentos#79](http://gitlab.meta.com.br/documentos/issues/79)
* **documentos#79:** finished charts ([2b089cd](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2b089cdf380708c35b8fe4a47f65fd8e20b7523f)), closes [documentos#79](http://gitlab.meta.com.br/documentos/issues/79)
* **documentos#79:** merged with develop ([7538e6f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7538e6fd5b6870ade8c87eb3c9f62e1295990fb3)), closes [documentos#79](http://gitlab.meta.com.br/documentos/issues/79)
* **merge com develop:** merge com develop ([fd25aa9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/fd25aa96da3ce80b7aca3568603a80d8a0b7b104))





# [0.8.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.7.0...v0.8.0-alpha.0) (2020-12-11)

**Note:** Version bump only for package @smartcoop/styles





# [0.7.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.7.0-alpha.0...v0.7.0) (2020-12-11)


### Features

* **documentos#308:** changed mobile navigation ([6a06eee](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/6a06eee9034387bee975cf9eab8c20bfe960f2df)), closes [documentos#308](http://gitlab.meta.com.br/documentos/issues/308)
* **documentos#393:** social network finished on mobile devices ([e838996](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e838996f44d0b26d857c47b062b6b1ecf0bdb809)), closes [documentos#393](http://gitlab.meta.com.br/documentos/issues/393)





# [0.7.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.6.0...v0.7.0-alpha.0) (2020-11-27)

**Note:** Version bump only for package @smartcoop/styles





# [0.6.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.5.3...v0.6.0) (2020-11-27)


### Bug Fixes

* **documentos#324:** removed field property from schema for mobile ([b137514](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b13751462dd0a257447cc89b0c1edd7596214c30)), closes [documentos#324](http://gitlab.meta.com.br/documentos/issues/324)


### Features

* **#383:** add snackbar in growing season register ([2aa60d5](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2aa60d52c4bb96e87053e5bec2816e411cd479f4)), closes [#383](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/383) [#129](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/129)
* **documentos#107:** adds new color ([e0ef8ac](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e0ef8acb26799b00369d96941266781336f1311c)), closes [documentos#107](http://gitlab.meta.com.br/documentos/issues/107)



# [0.6.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.5.0...v0.6.0-alpha.0) (2020-11-10)


### Features

* **#279:** add new color darkGrey ([13d24ee](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/13d24eebd20311612852dbc3bc5fdd69d8278127)), closes [#279](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/279) [#85](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/85)





# [0.6.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.5.0...v0.6.0-alpha.0) (2020-11-10)

**Note:** Version bump only for package @smartcoop/styles





# [0.5.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.5.0-alpha.0...v0.5.0) (2020-11-10)


### Features

* **documentos#101:** created module screen for web ([a0d8301](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a0d830137be31ed28c7bad0bc76672012b5af4eb)), closes [documentos#101](http://gitlab.meta.com.br/documentos/issues/101)
* **documentos#259:** add news colors ([0c51bf6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0c51bf6542fc19b136fc1d30a291b746f8740f26)), closes [documentos#259](http://gitlab.meta.com.br/documentos/issues/259) [#81](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/81)





# [0.5.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.4.0...v0.5.0-alpha.0) (2020-10-27)

**Note:** Version bump only for package @smartcoop/styles





# [0.4.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.3.0-alpha.0...v0.4.0) (2020-10-27)


### Bug Fixes

* **ci-cd:** fixed env for mobile and ci-cd pipeline ([8c10dd9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8c10dd9a6b1f88de7982f285315b0d8eaa2825b0))
* **ci-cd:** fixed only ref changes for jobs ([1a47f4f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1a47f4f00aaaccf9b124ece91885835ae132c09b))
* **documentos#132:** fix fonts ([86411d9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/86411d952258466480657462de9c6d8c0c55f059))


### Features

* **ci-cd:** calling aws ecr login on before script ([9a86a83](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9a86a83d8b8edcfe215d51bb62e0ebea82a1781b))
* **ci-cd:** fixed before_script ([5ba69b3](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/5ba69b356900691bede3ff8f51de8b78b192421a))
* **documentos#127:** added light grey color ([b1a60a5](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b1a60a5fff6d1a5f03e7a4bd2a8d9c58d6c2f8a5)), closes [documentos#127](http://gitlab.meta.com.br/documentos/issues/127)
* **documentos#127:** added new fonts ([7352af0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7352af05ae0129f3c88bb123da915af76ded15d7)), closes [documentos#127](http://gitlab.meta.com.br/documentos/issues/127)
* cherry-pick ([5c62bfb](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/5c62bfba63be33f46df72d99e8ac7b55bad3a708))
* **ci-cd:** created ci-cd ([95ea44c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/95ea44c9bbe5df17ded2cf836abe4744e5287814))
* **ci-cd:** running before-script for each job ([1ef87e1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1ef87e1f13fb3593b6a1f4f995186bb09a251cd2))
* **documentos#132:** wIP: Added font to test ([c097181](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c0971819b97b9a2929ffb1723d210ed970fb85de)), closes [documentos#132](http://gitlab.meta.com.br/documentos/issues/132) [#31](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/31)
* **documentos#132:** wIP: Added fonts to web ([c7c16b0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c7c16b0cba1dd3c5c7662e1236c42bd0ef0f3f2e)), closes [documentos#132](http://gitlab.meta.com.br/documentos/issues/132) [#31](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/31)
* **documentos#132:** wIP: Applicated fonts to mobile ([193cd20](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/193cd20899b60aa36eb7d12d3c12121413b0cea6)), closes [documentos#132](http://gitlab.meta.com.br/documentos/issues/132) [#31](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/31)





# [0.3.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.2.0...v0.3.0-alpha.0) (2020-09-28)

**Note:** Version bump only for package @smartcoop/styles




# [0.2.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.2.0-alpha.0...v0.2.0) (2020-09-28)


### Features

* **documentos#10:** created web login screen ([34ed647](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/34ed647f1ab16e0c1dc45db8455239335ed6286d)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#22:** merge ([824f2d2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/824f2d2f9b25aecb685e3b33ab1b4f445665fc08)), closes [documentos#22](http://gitlab.meta.com.br/documentos/issues/22)
* **documentos#32:** added database into web ([1f6ad73](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1f6ad73cc2f1de976bce6aab7835337f1f8d2e05)), closes [documentos#32](http://gitlab.meta.com.br/documentos/issues/32)





# [0.2.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.1.0...v0.2.0-alpha.0) (2020-09-13)

**Note:** Version bump only for package @smartcoop/styles





# [0.1.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.1.0-alpha.0...v0.1.0) (2020-09-13)


### Bug Fixes

* **fonts:** ttf font face to truetype ([6e8151d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/6e8151dbc967cb69412132a12233aec44f0b1db7))


### Features

* **documentos#10:** added navigation ([cdfba1f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/cdfba1f176e4bae75d4dc54477efb3c9d1d2068d)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#10:** added svg renderization ([e7cbfa0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e7cbfa0147cff6c39bb0386d34a213fec6206c46)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#10:** applying documentos[#37](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/37) into documentos[#39](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/39) ([c8e043e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c8e043ee97e72ea17b2ebd347852ea2cd999e755)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#10:** created select input and perf forms ([e4e83ab](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e4e83ab2bda01613b50076155333d3c7ce487607)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#10:** login and signup screens ([e4b9d02](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e4b9d0278f4785aa31928f9ec3bc6e5de4ff98e2)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#10:** mobile inputs styled as material design ([8a9e3cc](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8a9e3cce883733f69190c3eb53ed92aba70e4026)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10) [#7](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/7)
