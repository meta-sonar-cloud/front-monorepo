# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [0.18.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.17.0...v0.18.0) (2021-04-19)

**Note:** Version bump only for package @smartcoop/dialog





# [0.17.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.16.0...v0.17.0) (2021-04-15)

**Note:** Version bump only for package @smartcoop/dialog





# [0.16.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.15.0...v0.16.0) (2021-04-13)

**Note:** Version bump only for package @smartcoop/dialog







**Note:** Version bump only for package @smartcoop/dialog





# [0.15.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.12.0-alpha.1...v0.15.0) (2021-03-23)

**Note:** Version bump only for package @smartcoop/dialog





# [0.15.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.12.0-alpha.1...v0.15.0) (2021-03-23)

**Note:** Version bump only for package @smartcoop/dialog





# [0.15.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.12.0-alpha.1...v0.15.0) (2021-03-23)

**Note:** Version bump only for package @smartcoop/dialog





# [0.14.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.12.0-alpha.1...v0.14.0) (2021-03-23)

**Note:** Version bump only for package @smartcoop/dialog





# [0.13.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.12.0-alpha.1...v0.13.0) (2021-03-23)

**Note:** Version bump only for package @smartcoop/dialog





# [0.12.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.12.0-alpha.1...v0.12.0) (2021-03-23)

**Note:** Version bump only for package @smartcoop/dialog





# [0.12.0-alpha.1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.12.0-alpha.0...v0.12.0-alpha.1) (2021-02-22)

**Note:** Version bump only for package @smartcoop/dialog





# [0.12.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.11.0...v0.12.0-alpha.0) (2021-02-12)

**Note:** Version bump only for package @smartcoop/dialog





# [0.11.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.11.0-alpha.0...v0.11.0) (2021-02-12)

**Note:** Version bump only for package @smartcoop/dialog





# [0.11.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.10.0...v0.11.0-alpha.0) (2021-02-05)

**Note:** Version bump only for package @smartcoop/dialog





# [0.10.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.10.0-alpha.0...v0.10.0) (2021-02-01)

**Note:** Version bump only for package @smartcoop/dialog





# [0.10.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.9.0...v0.10.0-alpha.0) (2021-01-19)

**Note:** Version bump only for package @smartcoop/dialog





# [0.9.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.8.2...v0.9.0) (2021-01-19)

**Note:** Version bump only for package @smartcoop/dialog





## [0.8.1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.9.0-alpha.0...v0.8.1) (2021-01-06)

**Note:** Version bump only for package @smartcoop/dialog





# [0.9.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.8.0...v0.9.0-alpha.0) (2021-01-05)

**Note:** Version bump only for package @smartcoop/dialog





# [0.8.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.8.0-alpha.0...v0.8.0) (2021-01-05)


### Features

* **documentos#688:** created InputDateRange for web ([25b3b3d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/25b3b3d98487e09f0edb61c9e38ec1e5b04d3afd)), closes [documentos#688](http://gitlab.meta.com.br/documentos/issues/688)





# [0.8.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.7.0...v0.8.0-alpha.0) (2020-12-11)

**Note:** Version bump only for package @smartcoop/dialog





# [0.7.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.7.0-alpha.0...v0.7.0) (2020-12-11)

**Note:** Version bump only for package @smartcoop/dialog





# [0.7.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.6.0...v0.7.0-alpha.0) (2020-11-27)

**Note:** Version bump only for package @smartcoop/dialog





# [0.6.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.5.3...v0.6.0) (2020-11-27)



# [0.6.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.5.0...v0.6.0-alpha.0) (2020-11-10)

**Note:** Version bump only for package @smartcoop/dialog





# [0.6.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.5.0...v0.6.0-alpha.0) (2020-11-10)

**Note:** Version bump only for package @smartcoop/dialog





# [0.5.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.5.0-alpha.0...v0.5.0) (2020-11-10)

**Note:** Version bump only for package @smartcoop/dialog





# [0.5.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.4.0...v0.5.0-alpha.0) (2020-10-27)

**Note:** Version bump only for package @smartcoop/dialog





# [0.4.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.3.0-alpha.0...v0.4.0) (2020-10-27)

**Note:** Version bump only for package @smartcoop/dialog





# [0.3.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.2.0...v0.3.0-alpha.0) (2020-09-28)

**Note:** Version bump only for package @smartcoop/dialog





# [0.2.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.2.0-alpha.0...v0.2.0) (2020-09-28)


### Features

* **documentos#10:** craeted loading and error modal ([34bc091](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/34bc091a4766d8b07fda331faf0cbec7c81a7497)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#10:** created web login screen ([34ed647](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/34ed647f1ab16e0c1dc45db8455239335ed6286d)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#57:** created modal component to web and mobile ([c3610ea](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c3610eabd1cecab013f46fe3c116d5f7cea00551)), closes [documentos#57](http://gitlab.meta.com.br/documentos/issues/57) [#18](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/18)
