import moment from 'moment/moment'

import { momentBackMonthYearFormat } from '@smartcoop/utils/dates'

export default ({ t }) => YupInstance => YupInstance
  .test('date', t('invalid date'), (value) => !value || moment(value, momentBackMonthYearFormat, true).isValid())
