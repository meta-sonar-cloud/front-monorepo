import trimMask from '@meta-awesome/functions/src/trimMask'

import size from 'lodash/size'

export default ({ t }) => YupInstance => YupInstance
  .test('code', t('invalid code'), (value) => size(trimMask(value)) === 6)
