import toNumber from 'lodash/toNumber'

export default ({ t, field, limit = 0 }) => (YupInstance) =>
  YupInstance.test(
    'minNumberValue',
    t('{this} must be greater than {number}', { this: field, number: limit }),
    (value) =>
      toNumber(((value || '').replace('.', '') || '').replace(',', '.')) > limit
  )
