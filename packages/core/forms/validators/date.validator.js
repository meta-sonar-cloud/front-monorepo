import moment from 'moment/moment'

import { momentBackDateFormat } from '@smartcoop/utils/dates'

export default ({ t }) => YupInstance => YupInstance
  .test('date', t('invalid date'), (value) => !value || moment(value, momentBackDateFormat, true).isValid())
