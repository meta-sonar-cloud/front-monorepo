import trimMask from '@meta-awesome/functions/src/trimMask'

import size from 'lodash/size'

export default ({ t }) => YupInstance => YupInstance
  .test('phone', t('invalid phone'), (value) => size(trimMask(value)) >= 10)
