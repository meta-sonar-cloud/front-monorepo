import includes from 'lodash/includes'

export default ({ t, text = 'the value is not valid', compareWith }) => YupInstance => YupInstance
  .test('notInclude', t(text), (value) => !includes(value, compareWith))
