export default ({ t, min, name }) => YupInstance => YupInstance
  .min(min, t('{this} must contain at least {size} characters', {
    this: name,
    size: min
  }))
