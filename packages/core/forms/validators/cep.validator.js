import trimMask from '@meta-awesome/functions/src/trimMask'

import size from 'lodash/size'

export default ({ t }) => YupInstance => YupInstance
  .test('cep', t('invalid cep'), (value) => size(trimMask(value)) === 8)
