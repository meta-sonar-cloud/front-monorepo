import isBoolean from 'lodash/isBoolean'
import isEmpty from 'lodash/isEmpty'
import isNumber from 'lodash/isNumber'

export default ({ t, text = t('this field is required') }) => YupInstance => YupInstance
  .test('notEmpty', text, (value) => !isEmpty(value) || isNumber(value) || isBoolean(value))
