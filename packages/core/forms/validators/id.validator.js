export default ({ t, min, name }) => YupInstance => YupInstance
  .min(min, t('select a valid value for {this}', {
    this: name
  }))
