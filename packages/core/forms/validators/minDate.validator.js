import moment from 'moment/moment'

export default ({ t, minReceiptDate }) => YupInstance => YupInstance
  .test('date', t('invalid date'), (date) => {
    const format = 'YYYY-MM-DD'

    let dateMoment = moment()
    if (date !== 'today') {
      dateMoment = moment(date, format)
    }

    const minReceiptDateMoment = moment(minReceiptDate, format)

    return dateMoment.isSameOrAfter(minReceiptDateMoment)
  })
