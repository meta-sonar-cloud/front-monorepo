# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [0.18.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.17.0...v0.18.0) (2021-04-19)


### Bug Fixes

* **#1748:** remove validation required fields ([6172c3f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/6172c3f7e57667483567f1ec52f8a8c015ecdbcc)), closes [#1748](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1748) [#581](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/581)
* **documentos#1755:** adjustments ([096f90b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/096f90b47500de13e083e959f99be6f044dec0b6)), closes [documentos#1755](http://gitlab.meta.com.br/documentos/issues/1755)


### Features

* **develop:** fix on submit in other actions ([163b0ab](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/163b0abea7af14120cecc37f2daaa3d476b9e9cf))
* **documentos#1744:** changes in order list ([d9a3960](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d9a3960d89e1268b1e55363199ee02d526110d71)), closes [documentos#1744](http://gitlab.meta.com.br/documentos/issues/1744)
* **documentos#1767:** omit inputs when its calving ([94aa075](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/94aa0753d92fe5f6ed3c83a31bdb71695d646440)), closes [documentos#1767](http://gitlab.meta.com.br/documentos/issues/1767)





# [0.17.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.16.0...v0.17.0) (2021-04-15)


### Bug Fixes

* **#1684:** adjustment filter cattle management with lotid ([90a24c5](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/90a24c5c66b22cc94ed54aa2380d3dd3eddfae72)), closes [#1684](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1684) [#549](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/549)
* **#1684:** adjustment rules create insemination ([11514e6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/11514e62e7d4197881d70430ff01f83b6d969c19)), closes [#1684](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1684) [#549](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/549)


### Features

* **develop:** adds status filter in cattle management ([a712e97](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a712e979b9373ae7aa8b9ad16afbf478e6b1de64))
* resolve [#1639](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1639) ([28b04f1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/28b04f12bf5ddd480881a3560ffdbc1d1badd018))
* **documentos#1515:** removes earring from filters ([6cf3dea](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/6cf3dea088f4f85fea435ffd5f138e888d95082a)), closes [documentos#1515](http://gitlab.meta.com.br/documentos/issues/1515)
* **documentos#1674:** removes select from lot ([95556de](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/95556ded8b1c169e14381027ef18241053ffdc79)), closes [documentos#1674](http://gitlab.meta.com.br/documentos/issues/1674)





# [0.16.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.15.0...v0.16.0) (2021-04-13)


### Bug Fixes

* **#1649:** change form register animal ([0a65145](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0a6514522fd9f9decc650c73545448a69ce912c9)), closes [#1649](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1649)
* **#1686:** add validation form register animal ([8007e1d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8007e1d294c51117a4091e542fac47bf073d7bb8)), closes [#1686](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1686) [#548](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/548)
* **ajustes finos:** ajustes finos da aplicação ([eefa46f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/eefa46fe613632b216df51736217149a8027dd16))


### Features

* **documentos#1579:** lot changes ([f63dc6d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f63dc6dca603cdd367d6f6497f14e8abe3031ca4)), closes [documentos#1579](http://gitlab.meta.com.br/documentos/issues/1579)
* merge com a develop ([702e143](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/702e1434c3eab83403ccbd51d34a6340a05613b9))
* wip ([8a64b05](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8a64b05a75f08e112e58615cb42c7edf15303438))
* **documentos#1316:** created other animal action tab ([a4fcb22](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a4fcb22e0681b4874db9211a64011c01c6608d5e)), closes [documentos#1316](http://gitlab.meta.com.br/documentos/issues/1316)
* **documentos#1316:** created other animal action tab ([aa26a59](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/aa26a59faa40a485b20c7e5c8543934e379c749e)), closes [documentos#1316](http://gitlab.meta.com.br/documentos/issues/1316)
* **documentos#1325:** created pev crud for mobile ([f0ef8c0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f0ef8c00ffd2a45ad9b3a34ce5fee71fc1eced50)), closes [documentos#1325](http://gitlab.meta.com.br/documentos/issues/1325)
* **documentos#1325:** created PEV on web ([d0d6dda](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d0d6ddae85eeab6cd3b022ac0cf40c8db8aa7d06)), closes [documentos#1325](http://gitlab.meta.com.br/documentos/issues/1325)
* **documentos#1495:** modals, saga and item ([a04f400](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a04f400ff9eb8feddfbef787e6b8fd7121985631)), closes [documentos#1495](http://gitlab.meta.com.br/documentos/issues/1495)
* **documentos#1579:** filters in web ([6494181](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/64941810de441edc856318583f5776dd41749431)), closes [documentos#1579](http://gitlab.meta.com.br/documentos/issues/1579)
* wip ([a1daf61](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a1daf6149af8d967fff4afe4009a7f2a116d8917))
* **documentos#1579:** mobile insemination ([1bf225d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1bf225d6f5ffbafb10226bf7c5c77b13a03ed58e)), closes [documentos#1579](http://gitlab.meta.com.br/documentos/issues/1579)
* **documentos#1579:** wip ([f49c8ca](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f49c8ca24b4a8991eab7db91217da03fa7346e97)), closes [documentos#1579](http://gitlab.meta.com.br/documentos/issues/1579)
* resolve [#1604](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1604) ([06bdda8](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/06bdda8ab85f34d6589443ab77810d75b9663267))
* **documentos#1579:** mobile insemination ([10c2ff2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/10c2ff2171498fa004d5fc58ac3ba9266347753a)), closes [documentos#1579](http://gitlab.meta.com.br/documentos/issues/1579)
* **documentos#1579:** wip ([a8def31](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a8def31a30ea41a4f4700a4eb404572454c18c83)), closes [documentos#1579](http://gitlab.meta.com.br/documentos/issues/1579)







**Note:** Version bump only for package @smartcoop/forms





# [0.15.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.12.0-alpha.1...v0.15.0) (2021-03-23)


### Bug Fixes

* **documentos#1447:** ajustes mapa de chuva e relatorios ([9cd51a9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9cd51a9037291d437f5b847da66e8b7bf8a67998)), closes [documentos#1447](http://gitlab.meta.com.br/documentos/issues/1447)
* uncomment line in barter details schema ([599ba0e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/599ba0edc27b5914885b685045e0722cd0380077))


### Features

* temporary code comment ([e7d6b2e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e7d6b2eaeeb3e3163fd16b6842954749f7f385a9))
* **documentos#1305:** removes birthday from onboarding ([5eda681](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/5eda6815388bc7289d1d33d1e6896faca4e4aa68)), closes [documentos#1305](http://gitlab.meta.com.br/documentos/issues/1305)
* **documentos#1357:** technical modal ([8808db1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8808db1072f9efafa56b3eec0ceb3868b21512cf)), closes [documentos#1357](http://gitlab.meta.com.br/documentos/issues/1357)
* **documentos#1437:** changes to make onboarding works without email ([f91732d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f91732d32c6437b9ca3c407c470238fa9507c76c)), closes [documentos#1437](http://gitlab.meta.com.br/documentos/issues/1437)
* merge develop ([2731148](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2731148d7380e3ab5c8ec38d198b1773467e4364))
* resolve [#1271](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1271) ([1d8c833](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1d8c833327f9a24e71badb366b576d88108628e3))
* resolve [#1271](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1271) ([f9482ad](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f9482ade31bf6d0b1645855dce0e87d4b748300c))
* resolve [#1291](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1291) ([54ffd95](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/54ffd95046cfc813afa98a22a2c583b5968e6e53))
* resolve [#1342](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1342) ([4a1bce3](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4a1bce3235819998a89f0ca7f398b876c5b4e35b))
* resolve [#1382](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1382) ([5778257](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/5778257b16fd42ca9279151eed027281eeb791fc))
* wip ([2cf8dd6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2cf8dd6979ab9a9ee338858a586f2587ca18899e))
* wip ([16b3eef](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/16b3eef778dda67fd818f50aaaefb2eb54901e34))
* **documentos#1240:** confirmPassword schema ([35e067d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/35e067d3e3d200bf37f7c38ab7354197fc759b0a)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1282:** added IE field to sale order form ([d171dac](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d171dac06719c0a06505988e94322852c55ef538)), closes [documentos#1282](http://gitlab.meta.com.br/documentos/issues/1282)
* **documentos#1282:** created barter details form ([25f8826](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/25f8826a6cd03ebce622100c3e8950d9003aab19)), closes [documentos#1282](http://gitlab.meta.com.br/documentos/issues/1282)





# [0.15.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.12.0-alpha.1...v0.15.0) (2021-03-23)


### Bug Fixes

* **documentos#1447:** ajustes mapa de chuva e relatorios ([9cd51a9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9cd51a9037291d437f5b847da66e8b7bf8a67998)), closes [documentos#1447](http://gitlab.meta.com.br/documentos/issues/1447)
* uncomment line in barter details schema ([599ba0e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/599ba0edc27b5914885b685045e0722cd0380077))


### Features

* temporary code comment ([e7d6b2e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e7d6b2eaeeb3e3163fd16b6842954749f7f385a9))
* **documentos#1305:** removes birthday from onboarding ([5eda681](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/5eda6815388bc7289d1d33d1e6896faca4e4aa68)), closes [documentos#1305](http://gitlab.meta.com.br/documentos/issues/1305)
* **documentos#1357:** technical modal ([8808db1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8808db1072f9efafa56b3eec0ceb3868b21512cf)), closes [documentos#1357](http://gitlab.meta.com.br/documentos/issues/1357)
* **documentos#1437:** changes to make onboarding works without email ([f91732d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f91732d32c6437b9ca3c407c470238fa9507c76c)), closes [documentos#1437](http://gitlab.meta.com.br/documentos/issues/1437)
* merge develop ([2731148](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2731148d7380e3ab5c8ec38d198b1773467e4364))
* resolve [#1271](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1271) ([1d8c833](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1d8c833327f9a24e71badb366b576d88108628e3))
* resolve [#1271](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1271) ([f9482ad](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f9482ade31bf6d0b1645855dce0e87d4b748300c))
* resolve [#1291](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1291) ([54ffd95](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/54ffd95046cfc813afa98a22a2c583b5968e6e53))
* resolve [#1342](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1342) ([4a1bce3](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4a1bce3235819998a89f0ca7f398b876c5b4e35b))
* resolve [#1382](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1382) ([5778257](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/5778257b16fd42ca9279151eed027281eeb791fc))
* wip ([2cf8dd6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2cf8dd6979ab9a9ee338858a586f2587ca18899e))
* wip ([16b3eef](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/16b3eef778dda67fd818f50aaaefb2eb54901e34))
* **documentos#1240:** confirmPassword schema ([35e067d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/35e067d3e3d200bf37f7c38ab7354197fc759b0a)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1282:** added IE field to sale order form ([d171dac](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d171dac06719c0a06505988e94322852c55ef538)), closes [documentos#1282](http://gitlab.meta.com.br/documentos/issues/1282)
* **documentos#1282:** created barter details form ([25f8826](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/25f8826a6cd03ebce622100c3e8950d9003aab19)), closes [documentos#1282](http://gitlab.meta.com.br/documentos/issues/1282)





# [0.15.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.12.0-alpha.1...v0.15.0) (2021-03-23)


### Bug Fixes

* **documentos#1447:** ajustes mapa de chuva e relatorios ([9cd51a9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9cd51a9037291d437f5b847da66e8b7bf8a67998)), closes [documentos#1447](http://gitlab.meta.com.br/documentos/issues/1447)
* uncomment line in barter details schema ([599ba0e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/599ba0edc27b5914885b685045e0722cd0380077))


### Features

* temporary code comment ([e7d6b2e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e7d6b2eaeeb3e3163fd16b6842954749f7f385a9))
* **documentos#1305:** removes birthday from onboarding ([5eda681](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/5eda6815388bc7289d1d33d1e6896faca4e4aa68)), closes [documentos#1305](http://gitlab.meta.com.br/documentos/issues/1305)
* **documentos#1357:** technical modal ([8808db1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8808db1072f9efafa56b3eec0ceb3868b21512cf)), closes [documentos#1357](http://gitlab.meta.com.br/documentos/issues/1357)
* **documentos#1437:** changes to make onboarding works without email ([f91732d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f91732d32c6437b9ca3c407c470238fa9507c76c)), closes [documentos#1437](http://gitlab.meta.com.br/documentos/issues/1437)
* merge develop ([2731148](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2731148d7380e3ab5c8ec38d198b1773467e4364))
* resolve [#1271](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1271) ([1d8c833](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1d8c833327f9a24e71badb366b576d88108628e3))
* resolve [#1271](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1271) ([f9482ad](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f9482ade31bf6d0b1645855dce0e87d4b748300c))
* resolve [#1291](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1291) ([54ffd95](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/54ffd95046cfc813afa98a22a2c583b5968e6e53))
* resolve [#1342](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1342) ([4a1bce3](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4a1bce3235819998a89f0ca7f398b876c5b4e35b))
* resolve [#1382](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1382) ([5778257](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/5778257b16fd42ca9279151eed027281eeb791fc))
* wip ([2cf8dd6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2cf8dd6979ab9a9ee338858a586f2587ca18899e))
* wip ([16b3eef](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/16b3eef778dda67fd818f50aaaefb2eb54901e34))
* **documentos#1240:** confirmPassword schema ([35e067d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/35e067d3e3d200bf37f7c38ab7354197fc759b0a)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1282:** added IE field to sale order form ([d171dac](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d171dac06719c0a06505988e94322852c55ef538)), closes [documentos#1282](http://gitlab.meta.com.br/documentos/issues/1282)
* **documentos#1282:** created barter details form ([25f8826](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/25f8826a6cd03ebce622100c3e8950d9003aab19)), closes [documentos#1282](http://gitlab.meta.com.br/documentos/issues/1282)





# [0.14.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.12.0-alpha.1...v0.14.0) (2021-03-23)


### Bug Fixes

* **documentos#1447:** ajustes mapa de chuva e relatorios ([9cd51a9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9cd51a9037291d437f5b847da66e8b7bf8a67998)), closes [documentos#1447](http://gitlab.meta.com.br/documentos/issues/1447)
* uncomment line in barter details schema ([599ba0e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/599ba0edc27b5914885b685045e0722cd0380077))


### Features

* temporary code comment ([e7d6b2e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e7d6b2eaeeb3e3163fd16b6842954749f7f385a9))
* **documentos#1305:** removes birthday from onboarding ([5eda681](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/5eda6815388bc7289d1d33d1e6896faca4e4aa68)), closes [documentos#1305](http://gitlab.meta.com.br/documentos/issues/1305)
* **documentos#1357:** technical modal ([8808db1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8808db1072f9efafa56b3eec0ceb3868b21512cf)), closes [documentos#1357](http://gitlab.meta.com.br/documentos/issues/1357)
* **documentos#1437:** changes to make onboarding works without email ([f91732d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f91732d32c6437b9ca3c407c470238fa9507c76c)), closes [documentos#1437](http://gitlab.meta.com.br/documentos/issues/1437)
* merge develop ([2731148](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2731148d7380e3ab5c8ec38d198b1773467e4364))
* resolve [#1271](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1271) ([1d8c833](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1d8c833327f9a24e71badb366b576d88108628e3))
* resolve [#1271](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1271) ([f9482ad](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f9482ade31bf6d0b1645855dce0e87d4b748300c))
* resolve [#1291](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1291) ([54ffd95](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/54ffd95046cfc813afa98a22a2c583b5968e6e53))
* resolve [#1342](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1342) ([4a1bce3](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4a1bce3235819998a89f0ca7f398b876c5b4e35b))
* resolve [#1382](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1382) ([5778257](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/5778257b16fd42ca9279151eed027281eeb791fc))
* wip ([2cf8dd6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2cf8dd6979ab9a9ee338858a586f2587ca18899e))
* wip ([16b3eef](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/16b3eef778dda67fd818f50aaaefb2eb54901e34))
* **documentos#1240:** confirmPassword schema ([35e067d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/35e067d3e3d200bf37f7c38ab7354197fc759b0a)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1282:** added IE field to sale order form ([d171dac](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d171dac06719c0a06505988e94322852c55ef538)), closes [documentos#1282](http://gitlab.meta.com.br/documentos/issues/1282)
* **documentos#1282:** created barter details form ([25f8826](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/25f8826a6cd03ebce622100c3e8950d9003aab19)), closes [documentos#1282](http://gitlab.meta.com.br/documentos/issues/1282)





# [0.13.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.12.0-alpha.1...v0.13.0) (2021-03-23)


### Bug Fixes

* **documentos#1447:** ajustes mapa de chuva e relatorios ([9cd51a9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9cd51a9037291d437f5b847da66e8b7bf8a67998)), closes [documentos#1447](http://gitlab.meta.com.br/documentos/issues/1447)
* uncomment line in barter details schema ([599ba0e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/599ba0edc27b5914885b685045e0722cd0380077))


### Features

* temporary code comment ([e7d6b2e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e7d6b2eaeeb3e3163fd16b6842954749f7f385a9))
* **documentos#1305:** removes birthday from onboarding ([5eda681](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/5eda6815388bc7289d1d33d1e6896faca4e4aa68)), closes [documentos#1305](http://gitlab.meta.com.br/documentos/issues/1305)
* **documentos#1357:** technical modal ([8808db1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8808db1072f9efafa56b3eec0ceb3868b21512cf)), closes [documentos#1357](http://gitlab.meta.com.br/documentos/issues/1357)
* **documentos#1437:** changes to make onboarding works without email ([f91732d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f91732d32c6437b9ca3c407c470238fa9507c76c)), closes [documentos#1437](http://gitlab.meta.com.br/documentos/issues/1437)
* merge develop ([2731148](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2731148d7380e3ab5c8ec38d198b1773467e4364))
* resolve [#1271](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1271) ([1d8c833](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1d8c833327f9a24e71badb366b576d88108628e3))
* resolve [#1271](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1271) ([f9482ad](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f9482ade31bf6d0b1645855dce0e87d4b748300c))
* resolve [#1291](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1291) ([54ffd95](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/54ffd95046cfc813afa98a22a2c583b5968e6e53))
* resolve [#1342](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1342) ([4a1bce3](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4a1bce3235819998a89f0ca7f398b876c5b4e35b))
* resolve [#1382](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1382) ([5778257](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/5778257b16fd42ca9279151eed027281eeb791fc))
* wip ([2cf8dd6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2cf8dd6979ab9a9ee338858a586f2587ca18899e))
* wip ([16b3eef](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/16b3eef778dda67fd818f50aaaefb2eb54901e34))
* **documentos#1240:** confirmPassword schema ([35e067d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/35e067d3e3d200bf37f7c38ab7354197fc759b0a)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1282:** added IE field to sale order form ([d171dac](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d171dac06719c0a06505988e94322852c55ef538)), closes [documentos#1282](http://gitlab.meta.com.br/documentos/issues/1282)
* **documentos#1282:** created barter details form ([25f8826](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/25f8826a6cd03ebce622100c3e8950d9003aab19)), closes [documentos#1282](http://gitlab.meta.com.br/documentos/issues/1282)





# [0.12.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.12.0-alpha.1...v0.12.0) (2021-03-23)


### Bug Fixes

* **documentos#1447:** ajustes mapa de chuva e relatorios ([9cd51a9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9cd51a9037291d437f5b847da66e8b7bf8a67998)), closes [documentos#1447](http://gitlab.meta.com.br/documentos/issues/1447)
* uncomment line in barter details schema ([599ba0e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/599ba0edc27b5914885b685045e0722cd0380077))


### Features

* temporary code comment ([e7d6b2e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e7d6b2eaeeb3e3163fd16b6842954749f7f385a9))
* **documentos#1305:** removes birthday from onboarding ([5eda681](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/5eda6815388bc7289d1d33d1e6896faca4e4aa68)), closes [documentos#1305](http://gitlab.meta.com.br/documentos/issues/1305)
* **documentos#1357:** technical modal ([8808db1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8808db1072f9efafa56b3eec0ceb3868b21512cf)), closes [documentos#1357](http://gitlab.meta.com.br/documentos/issues/1357)
* **documentos#1437:** changes to make onboarding works without email ([f91732d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f91732d32c6437b9ca3c407c470238fa9507c76c)), closes [documentos#1437](http://gitlab.meta.com.br/documentos/issues/1437)
* merge develop ([2731148](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2731148d7380e3ab5c8ec38d198b1773467e4364))
* resolve [#1271](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1271) ([1d8c833](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1d8c833327f9a24e71badb366b576d88108628e3))
* resolve [#1271](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1271) ([f9482ad](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f9482ade31bf6d0b1645855dce0e87d4b748300c))
* resolve [#1291](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1291) ([54ffd95](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/54ffd95046cfc813afa98a22a2c583b5968e6e53))
* resolve [#1342](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1342) ([4a1bce3](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4a1bce3235819998a89f0ca7f398b876c5b4e35b))
* resolve [#1382](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1382) ([5778257](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/5778257b16fd42ca9279151eed027281eeb791fc))
* wip ([2cf8dd6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2cf8dd6979ab9a9ee338858a586f2587ca18899e))
* wip ([16b3eef](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/16b3eef778dda67fd818f50aaaefb2eb54901e34))
* **documentos#1240:** confirmPassword schema ([35e067d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/35e067d3e3d200bf37f7c38ab7354197fc759b0a)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1282:** added IE field to sale order form ([d171dac](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d171dac06719c0a06505988e94322852c55ef538)), closes [documentos#1282](http://gitlab.meta.com.br/documentos/issues/1282)
* **documentos#1282:** created barter details form ([25f8826](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/25f8826a6cd03ebce622100c3e8950d9003aab19)), closes [documentos#1282](http://gitlab.meta.com.br/documentos/issues/1282)





# [0.12.0-alpha.1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.12.0-alpha.0...v0.12.0-alpha.1) (2021-02-22)


### Bug Fixes

* **#1214:** adjustment schema, add nullable ([5ec2402](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/5ec2402d5e1f1ef5ca74a23570c77895822789d3)), closes [#1214](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1214) [#353](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/353)


### Features

* resolve [#329](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/329) ([1db5e51](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1db5e51565665ed63c4f49558bea53fc99a81751))
* **documentos#1196:** added date validation to trigger ([44d093e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/44d093e4bb70da9020200f68a150149ccbd5e8f4)), closes [documentos#1196](http://gitlab.meta.com.br/documentos/issues/1196)
* resolve [#1223](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1223) ([63b4f42](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/63b4f420a8b31ba0dd810591d679e5140ce6596f))
* resolve [#1243](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1243) ([65fd727](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/65fd727a33bcb04a9cc829b650b9e34eb88d7d3f))
* **documentos#987:** schema changes ([0a9e64e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0a9e64e8c5d5daeb37a2d6d57c92d5368c7aedf8)), closes [documentos#987](http://gitlab.meta.com.br/documentos/issues/987)





# [0.12.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.11.0...v0.12.0-alpha.0) (2021-02-12)

**Note:** Version bump only for package @smartcoop/forms





# [0.11.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.11.0-alpha.0...v0.11.0) (2021-02-12)


### Features

* wip ([0f84c73](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0f84c73ffacca6596a527731c452d161b482a790))
* **#1142:** resolve conflicts ([3abbaa9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3abbaa9ec9eaaa849f653fa5c073f89fabd56167)), closes [#1142](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1142)
* resolve [#1117](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1117) ([f295d11](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f295d11daa1e6f8a733392d15ee431cc8a0db78e))
* resolve [#1117](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1117) ([28e2c23](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/28e2c2337354377dcc21ce4b40037525a7ab0900))
* wip ([9028291](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9028291ac804514bf2d887a9106feba4dd7f79ab))
* wip ([395287f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/395287fad709cf45b1d58c1f8517d60fe4f80bb0))





# [0.11.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.10.0...v0.11.0-alpha.0) (2021-02-05)

**Note:** Version bump only for package @smartcoop/forms





# [0.10.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.10.0-alpha.0...v0.10.0) (2021-02-01)


### Bug Fixes

* resolve [#1028](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1028) ([08f3425](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/08f34250f5333500a31e00b0d281467f19b13d2f))


### Features

* **#1004:** implement edit technical visit ([f8f20c2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f8f20c25a9c93631192366dfed0931dda25aa13d)), closes [#1004](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1004) [#275](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/275)
* resolve [#1028](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1028) ([af6d23f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/af6d23fb5f4c04845fdff2b94610266c148fa3ed))
* **auth:** added login by cpf/cnpj ([3fb8616](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3fb8616f1773e6686c4be2a5a4893884ab86b84d))
* **documentos#102:** creates schema to edit address ([ae92238](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ae92238540dfd14ca1ed42713d78aa1db0cbfa68)), closes [documentos#102](http://gitlab.meta.com.br/documentos/issues/102)
* **documentos#987:** creates register milk quality form ([86d1517](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/86d1517c1f7fcd6ec97ab7a559de7627745d84cb)), closes [documentos#987](http://gitlab.meta.com.br/documentos/issues/987)
* **documentos#987:** filter milk quality schema ([b77414e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b77414eed2ffc097ad3fb8ec8c1fe61bf850c921)), closes [documentos#987](http://gitlab.meta.com.br/documentos/issues/987)
* **documentos#987:** fixes schema to align w backend ([3dfdb30](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3dfdb308df74e4b39846e7593278e40406281212))
* resolve [#1112](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1112) ([b53a69b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b53a69b16adc7daa62512624d244fda83e3ba6f2))
* **documentos#534:** added technical module to mobile ([007789a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/007789a8ca387d04401095875ba867d35388cbc9)), closes [documentos#534](http://gitlab.meta.com.br/documentos/issues/534)
* change identation ([4d4f0ad](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4d4f0ad50fec4229b5c391923afab0712f4b7d20))
* comment identation pt2 ([8584642](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/85846423e160d07469c483af646d79f6d23c9e75))
* fix identation ([48cc60a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/48cc60afeb9f51f1e5d05ee323815a9dae5af3a6))
* resolve [#277](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/277) ([48a2b6e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/48a2b6ef308dd1457e77f0d5713fce292d928d4d))
* resolve mr comments ([d3d3c19](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d3d3c19e74eab9e4e7d4af4e2e6d58f947edc558))
* resolve MR comments ([fdaa67d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/fdaa67de05c3f3809625f59e2d8b0b40a086eed8))
* **documentos#102:** creates schema to edit profile ([b29c610](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b29c610e2256b9f9dd845ee938456067290d92a5)), closes [documentos#102](http://gitlab.meta.com.br/documentos/issues/102)
* **documentos#987:** dateVolume -> volumeDate ([dbb9198](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/dbb91989db615dc4b480ca78b4dc75f8e5312ed5)), closes [documentos#987](http://gitlab.meta.com.br/documentos/issues/987)
* **documentos#987:** dateVolume -> volumeDate ([48e2a68](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/48e2a68db05aa1ca9051a61027d1a958ecb7533f)), closes [documentos#987](http://gitlab.meta.com.br/documentos/issues/987)
* **documentos#987:** filter milk modal ([74ec122](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/74ec1226f8d0023d268ecbd59dc9c00e3dcaad9e)), closes [documentos#987](http://gitlab.meta.com.br/documentos/issues/987)
* **documentos#987:** filter milk modal ([85ac3dc](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/85ac3dc51f8d2e40ca4be1c1cdf713e687e1baa9)), closes [documentos#987](http://gitlab.meta.com.br/documentos/issues/987)
* **documentos#987:** new translations ([146b880](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/146b8804304fec4bd9bba650498f5b6e5d62ca67)), closes [documentos#987](http://gitlab.meta.com.br/documentos/issues/987)
* **documentos#987:** new translations ([5baeffc](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/5baeffc9ad1e87f8026a96ec1e99567f1327dff1)), closes [documentos#987](http://gitlab.meta.com.br/documentos/issues/987)
* **documentos#987:** register milk schema ([9e2bb0e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9e2bb0e67db64339ad57bb53eb1feea117f3bf1e)), closes [documentos#987](http://gitlab.meta.com.br/documentos/issues/987)
* **documentos#987:** removes unused items from schema ([a6ae617](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a6ae617e00c642cc3796e8680bc9dbfc5fae1eb3)), closes [documentos#987](http://gitlab.meta.com.br/documentos/issues/987)
* **documentos#987:** removes unused items from schema ([4d2577a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4d2577a92c8e4681aa41f12af668f689ebb27400)), closes [documentos#987](http://gitlab.meta.com.br/documentos/issues/987)
* resolve merge conflicts ([d76333d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d76333db93b61f9897987ee033d050dcdc48d8fe))
* wip ([ea43347](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ea4334798c6090ec6819e6390596359d2bf439de))
* wip ([094d424](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/094d4242f82345f35ecd7b4227e3c497da75c966))
* **documentos#987:** register milk schema ([12e786b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/12e786b032a95d90fd51ee6a055e6a39dae2e4fe)), closes [documentos#987](http://gitlab.meta.com.br/documentos/issues/987)





# [0.10.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.9.0...v0.10.0-alpha.0) (2021-01-19)

**Note:** Version bump only for package @smartcoop/forms





# [0.9.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.8.2...v0.9.0) (2021-01-19)


### Bug Fixes

* **#892 #893:** adjustment crop management ([664c15f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/664c15f70d5c093987af0636f016374e6f656411)), closes [#892](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/892) [#893](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/893) [#237](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/237)


### Features

* **#873:** create sinature modal, form, schema ([e518de1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e518de1a6cf6c69cd396faa9bdd8344621393ab9)), closes [#873](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/873) [#260](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/260)
* dynamic validation in sales orders form ([8a7a60a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8a7a60ac7a8eb8f39098ac6ded762aaadacb6692))
* merge Develop ([13f52cf](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/13f52cfce97d5e68ce271d48dc7e493ee065e28c))
* **#901:** adjustment price register and show ([7d0182e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7d0182e76fb05b409938966f6b0ab1ac93abde7e)), closes [#901](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/901) [#241](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/241)
* **documentos#299:** removes commentayr ([2036ff1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2036ff108a5312b0e4ddd0ca18aa3f1517056887)), closes [documentos#299](http://gitlab.meta.com.br/documentos/issues/299)
* **documentos#299:** removes commented code ([ec3e9be](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ec3e9be82193814601c95047262f12619e262f81)), closes [documentos#299](http://gitlab.meta.com.br/documentos/issues/299)
* added sales orders forms ([b1e3fe8](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b1e3fe88414dba234ec6c9deff8d6cb7c3616f5a))
* changed order form fields ([6bb288d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/6bb288d853c3ad734ec51ff41c58c287bbd668a0))
* changed sales order forms ([a4dccbe](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a4dccbe39baa24fcd5462a81c56f7ebdffe55e3b))
* merge develop ([5b164a0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/5b164a012ca32970cb8bf82724781871ab529e58))
* resolve [#920](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/920) ([d4f7839](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d4f783957ef1027d031ad535835c3b1f9f75a7a3))
* **documentos#491:** sales orders api integration ([db53258](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/db532586160a539d34d858d40742b63b55c0ffbe)), closes [documentos#491](http://gitlab.meta.com.br/documentos/issues/491)
* rebase with develop ([da7e90e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/da7e90e31d9e8205abbd429866bcb9956f34a0ca))
* rebase with develop ([c17badd](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c17baddb1b38f98b530f474f11e9bec44987ee96))
* resolve Monte's Comments on MR ([b5a6fc7](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b5a6fc7cf86e92d2d0c005b4f75349741f2c2722))
* wIP ([d6f6459](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d6f64598b3e0b34fc8d6eb6c3672c366beb22afe))





## [0.8.2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.8.1...v0.8.2) (2021-01-06)


### Bug Fixes

* **#868:** adjustment mask hour for dynamic ([dec3cd6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/dec3cd6af1f244ed7263014ad158d41856a0f8c5)), closes [#868](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/868) [#228](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/228)


### Features

* added total value calc ([57ea873](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/57ea873489a04f7969c2d86c7c6b944a78328f43))
* **documentos#506:** adds input phone mobile machinery ([0799907](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0799907129b356f0523926b25f6b4b8f44d94707)), closes [documentos#506](http://gitlab.meta.com.br/documentos/issues/506)
* **documentos#725:** adds phone to schema ([488f399](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/488f399810d152f58935436a9f7476540c07e425)), closes [documentos#725](http://gitlab.meta.com.br/documentos/issues/725)
* **documentos#725:** fix rebase stuff ([f470223](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f47022325641772649994eab5685ae631106db5e))
* created sales order forms ([74422e5](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/74422e59f4220564a57c8406580c3bbd9622b1e5))





## [0.8.1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.9.0-alpha.0...v0.8.1) (2021-01-06)


### Features

* **#629:** adjustment schema and add new input ([c4a4ee5](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c4a4ee5ae0ce0d722c0cd295f129b48c29de8139)), closes [#629](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/629)





# [0.9.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.8.0...v0.9.0-alpha.0) (2021-01-05)

**Note:** Version bump only for package @smartcoop/forms





# [0.8.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.8.0-alpha.0...v0.8.0) (2021-01-05)


### Features

* **documentos#491:** create order form ([2b8653f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2b8653f777465d35bc1075bcc7578d4259e88863)), closes [documentos#491](http://gitlab.meta.com.br/documentos/issues/491)
* added custom message to max number validator ([18735eb](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/18735eb647fd8b22b6cfa525f867f323425b8df6))
* **#629:** adjustment mask hour ([d9ae6b6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d9ae6b6aa0b918c66f8ed2ae600ee7fc1c780690)), closes [#629](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/629)
* **#629:** adjustment web ([f53f5b4](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f53f5b405e95c58650e02bee8c6db47483aa82e3)), closes [#629](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/629)
* **documentos#491:** created create order form ([3310313](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/331031336e602a13d72af3ad109b50cf9a197ced)), closes [documentos#491](http://gitlab.meta.com.br/documentos/issues/491)
* **documentos#501:** adapts schema to match w/ backend ([969a024](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/969a024801899223141a58b3afc594839b781896)), closes [documentos#501](http://gitlab.meta.com.br/documentos/issues/501)
* **documentos#501:** created create organization form ([b8033cc](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b8033cc060e4ac231d8eb8a90955d047012b2a73)), closes [documentos#501](http://gitlab.meta.com.br/documentos/issues/501)
* **documentos#501:** creates schema and form of org. filter ([c0a2cee](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c0a2cee1d838f4398528ca14fcc46ec30f716ec7)), closes [documentos#501](http://gitlab.meta.com.br/documentos/issues/501)
* **documentos#501:** fixes create/edit ([536774e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/536774e8372620b0253ceb7c34a0f617b8e9a35f))
* **documentos#501:** fixes filters ([cac3467](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/cac3467d7f18d49000d327d0be977573f6482f79))
* **documentos#501:** temp fixes ([c59e5d2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c59e5d2972c73e2a4cd2d1d8abc15f3153385485)), closes [documentos#501](http://gitlab.meta.com.br/documentos/issues/501)
* **documentos#502:** removes trash button create form ([1bffed7](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1bffed7a514564691bad8088c8ffff988edc5efd)), closes [documentos#502](http://gitlab.meta.com.br/documentos/issues/502)
* **documentos#688:** created InputDateRange for web ([25b3b3d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/25b3b3d98487e09f0edb61c9e38ec1e5b04d3afd)), closes [documentos#688](http://gitlab.meta.com.br/documentos/issues/688)
* **documentos#702:** machinery register schema ([b84c360](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b84c360e25ab8ffa7f3aed3675bff22341e2b561)), closes [documentos#702](http://gitlab.meta.com.br/documentos/issues/702)
* **documentos#785:** adjust CreateOrganizationForm ([a9598e5](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a9598e55a6c23b13f451f9490d1a2dd7480304a9)), closes [documentos#785](http://gitlab.meta.com.br/documentos/issues/785)





# [0.8.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.7.0...v0.8.0-alpha.0) (2020-12-11)

**Note:** Version bump only for package @smartcoop/forms





# [0.7.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.7.0-alpha.0...v0.7.0) (2020-12-11)


### Features

* **documentos#223:** added acept/refuse best proposal ([ad90030](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ad90030224f891f70b01959638fc9f7f0b204800)), closes [documentos#223](http://gitlab.meta.com.br/documentos/issues/223)
* **documentos#262:** creates new schema ([a0feed2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a0feed2f27c2eda37de870319311553905f38180)), closes [documentos#262](http://gitlab.meta.com.br/documentos/issues/262)
* **documentos#454:** changed participation to proposal deadline ([0b9b1b2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0b9b1b28bc73d886796b2da20b7374afcf3f828b)), closes [documentos#454](http://gitlab.meta.com.br/documentos/issues/454)
* **documentos#455:** changed sowing year from monthYear to year ([e563242](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e56324297b37ed0eb3a20a36ee7aa7af808d831b)), closes [documentos#455](http://gitlab.meta.com.br/documentos/issues/455)
* **documentos#455:** created year validator ([4a7e39e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4a7e39ec568bcd59c7801de1255d3002cad33850)), closes [documentos#455](http://gitlab.meta.com.br/documentos/issues/455)





# [0.7.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.6.0...v0.7.0-alpha.0) (2020-11-27)

**Note:** Version bump only for package @smartcoop/forms





# [0.6.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.5.3...v0.6.0) (2020-11-27)


### Bug Fixes

* **documentos#324:** removed field property from schema for mobile ([b137514](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b13751462dd0a257447cc89b0c1edd7596214c30)), closes [documentos#324](http://gitlab.meta.com.br/documentos/issues/324)


### Features

* **#291:** ajustments filter supplier ([c84e24d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c84e24d5c6f486a11822ecb80696598ce89a47c8)), closes [#291](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/291) [#109](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/109)
* **#291:** ajustments in the filter supplier ([c0b2cb3](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c0b2cb343baacfc1e4cf1a5ad97f21c8e4cff15f)), closes [#291](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/291) [#109](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/109)
* **#291:** create new schema for filter supplier ([32139d2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/32139d205ddf2e0db4ccea9e2240ac7def7a9db9)), closes [#291](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/291) [#109](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/109)
* **documentos#156:** added dynamic property to field register ([0422c8d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0422c8da3d7c39ccb0bf76da437c429e1ef96009)), closes [documentos#156](http://gitlab.meta.com.br/documentos/issues/156)
* **documentos#16:** created growing season form ([f033e01](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f033e01531b374d515ccadad0b1b70deed9ab023)), closes [documentos#16](http://gitlab.meta.com.br/documentos/issues/16)
* **documentos#311:** change date by month year ([be9596f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/be9596fed05c8c63200844e751b048040b202059)), closes [documentos#311](http://gitlab.meta.com.br/documentos/issues/311)
* **documentos#311:** created month year input for web ([18b0de2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/18b0de23fdb837bf79e06f0cc3d7e1ec24e0b7b6)), closes [documentos#311](http://gitlab.meta.com.br/documentos/issues/311)
* **documentos#371:** redirects to field list when a field is registered ([5b301d1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/5b301d1bc3c13d20795a81a888c10a6c02f575a2)), closes [documentos#371](http://gitlab.meta.com.br/documentos/issues/371)



# [0.6.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.5.0...v0.6.0-alpha.0) (2020-11-10)





# [0.6.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.5.0...v0.6.0-alpha.0) (2020-11-10)
## [0.5.2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.5.1...v0.5.2) (2020-11-11)

**Note:** Version bump only for package @smartcoop/forms





# [0.5.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.5.0-alpha.0...v0.5.0) (2020-11-10)


### Bug Fixes

* **documentos#214:** removed required from date ([fb97dea](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/fb97dea5dae176c816d25ce096a88147e105f731)), closes [documentos#214](http://gitlab.meta.com.br/documentos/issues/214)
* **documentos#93:** validating payment methods ([2ebecfd](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2ebecfdaa0f8d30992072d2a91a6692756206742)), closes [documentos#93](http://gitlab.meta.com.br/documentos/issues/93)


### Features

* **documentos#49:** added query params to input select ([10b88bb](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/10b88bbeec3ed2e1dd41ea6e1b81214b45e41614)), closes [documentos#49](http://gitlab.meta.com.br/documentos/issues/49)
* **documentos#93:** adds mindate validator, also picker props in inputdate ([7911fde](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7911fdecd504d1884b7f918de61f715266155c27)), closes [documentos#93](http://gitlab.meta.com.br/documentos/issues/93)
* **documentos#93:** adds schemaprops to formprovider ([8f5cb76](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8f5cb7632dd7f05c1d9a9463681c163582c3ccd6)), closes [documentos#93](http://gitlab.meta.com.br/documentos/issues/93)
* **documentos#93:** adds usememo in minreceiptdate ([2e1108f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2e1108f40afe79afc1cd33ebbc036a2ca7fc3217)), closes [documentos#93](http://gitlab.meta.com.br/documentos/issues/93)
* **documentos#93:** created form ([79dc053](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/79dc053e867209bbb6c2f6b345878daef8e2b73d)), closes [documentos#93](http://gitlab.meta.com.br/documentos/issues/93)
* **documentos#93:** fixed payment form in order register ([027cbe9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/027cbe96d6156a4722fb37baa0ea7e98d82c16d0))





# [0.5.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.4.0...v0.5.0-alpha.0) (2020-10-27)

**Note:** Version bump only for package @smartcoop/forms





# [0.4.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.3.0-alpha.0...v0.4.0) (2020-10-27)


### Bug Fixes

* **documento#25:** fixed field provider reset form in mobile ([387a83a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/387a83a839cb18a2250d2ede9bfec41615c5d322))
* **documentos#10:** fix cell phone password rule ([7ad301d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7ad301d40f5c96ec5c0aeb4996db3ff55a653fa1))
* **documentos#127:** creating filter order form ([061619e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/061619e51264314501fda56baf3a70cccaf6538e)), closes [documentos#127](http://gitlab.meta.com.br/documentos/issues/127)
* **documentos#28:** changed address property fields names ([7258879](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/72588794f72a4cd5967ed2942325a5b00592da40)), closes [documentos#28](http://gitlab.meta.com.br/documentos/issues/28) [#34](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/34)
* **documentos#28:** fixed max number value validator ([2340a5b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2340a5bc3f31b98d3a28b161588a854d0819d14e))
* **documentos#28:** fixed min number value error message ([966eaeb](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/966eaebd1a2f396f89d952848772f3233d7a998a))
* **documentos#28:** fixed min number value validator ([8f85be7](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8f85be7753817153e20b1a12c2d1adfa3a1ca389))
* **documentos#28:** fixed property identification schema ([d02c4c0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d02c4c0c7a934c5b49f36e49781fb5be645ff734))
* **documentos#28:** removed required in address complement ([d1a6269](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d1a6269287fe0ec05178787a91b4a4c5460558e7)), closes [documentos#28](http://gitlab.meta.com.br/documentos/issues/28)
* **documentos#63:** fixed confirm password rule ([489107e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/489107e5d0a70d90f0f86b2318d5c255372be416))
* **documentos#63:** removed user passed to password validator ([481da17](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/481da17a3e974f7f8eab56d3d5b8abfc2fcef9ae)), closes [documentos#63](http://gitlab.meta.com.br/documentos/issues/63)


### Features

* **documentos#10:** validating password by cpf phone date of birth ([4aefbe0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4aefbe0b0eab8a548aaac32f4b2b008492f1fed8)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#18:** finished input date with calendar picker for web ([c583f68](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c583f68499aeafb5c6bb21d87e53312d350e3fd1)), closes [documentos#18](http://gitlab.meta.com.br/documentos/issues/18)
* **documentos#185:** created basic navigation for web and mobile ([4b144ed](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4b144edcd2c7d7338100f5da5105df4ccec87969)), closes [documentos#185](http://gitlab.meta.com.br/documentos/issues/185)
* **documentos#28:** added new rule to property identification schema ([eaa69b0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/eaa69b0ec216f99a2ccb39f4f06800b79a77c4ba)), closes [documentos#28](http://gitlab.meta.com.br/documentos/issues/28)
* **documentos#28:** added trim to required validator ([4259a80](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4259a80c2b195c1ce052a3f7c7dad4145f0ca19a)), closes [documentos#28](http://gitlab.meta.com.br/documentos/issues/28)
* **documentos#28:** created max number validator ([61ce96e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/61ce96e9040eebfe117bfef6499463304eb498e4)), closes [documentos#28](http://gitlab.meta.com.br/documentos/issues/28)
* **documentos#28:** created min number value validator ([d26f01f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d26f01f4ab964cfe307099b6d851b712aef50260)), closes [documentos#28](http://gitlab.meta.com.br/documentos/issues/28)
* **documentos#49:** add layout to register orden screen ([76a4d2a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/76a4d2aeb4e1cec775173b80adad379639911660)), closes [documentos#49](http://gitlab.meta.com.br/documentos/issues/49)
* **documentos#49:** added isNotText optin to required validator ([b42fec7](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b42fec75a062aa3d4f06122afaf11a6344bf8c22)), closes [documentos#49](http://gitlab.meta.com.br/documentos/issues/49)
* **documentos#49:** created create order schema ([a5c4cae](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a5c4cae5b275ea2e58abeb5cc2c13efdb3557ff8)), closes [documentos#49](http://gitlab.meta.com.br/documentos/issues/49)
* **documentos#53:** added new rules to area in field register ([e7089c1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e7089c1e5ce4a661e2d2fd434c9c06bfd203218b)), closes [documentos#53](http://gitlab.meta.com.br/documentos/issues/53)
* **documentos#53:** changed min number value validator error message ([ddcb025](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ddcb0258904f6eb853cfa169bef1657cd910a8d1)), closes [documentos#53](http://gitlab.meta.com.br/documentos/issues/53)
* **forms:** added form.validateField(fieldName) ([5effc5c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/5effc5cf477a400358ba2d4c162b560e008c4a15))





# [0.3.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.2.0...v0.3.0-alpha.0) (2020-09-28)

**Note:** Version bump only for package @smartcoop/forms





# [0.2.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.2.0-alpha.0...v0.2.0) (2020-09-28)


### Bug Fixes

* **forms:** fixed form package ([85e9656](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/85e965662d9ebcd4780f737474ef922e4f120d2a))


### Features

* **documentos#10:** auto fill forms with user api data ([9909ac6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9909ac6498d857e0f5ecb587d0334fab4b064036)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#10:** created identification and address screen on web ([57b0f74](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/57b0f7432fdabf9ac59cdfa52760d1a56a1e49f2)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#10:** created web screens ([496ac64](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/496ac645455e7c7e127a68385e9d8fa8b3b4b9f6)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#25:** changed web field form style ([7636b12](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7636b126893c4a69ac658da201f5c1d6b4e07634)), closes [documentos#25](http://gitlab.meta.com.br/documentos/issues/25)
* **documentos#25:** created field form documentos[#60](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/60) ([3ea4e41](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3ea4e412579de3801b4c94168212192098cda2da)), closes [documentos#25](http://gitlab.meta.com.br/documentos/issues/25)
* **documentos#28:** created property activity form ([301dc7c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/301dc7cfda1c6005ab75d263575c22eb613db704)), closes [documentos#28](http://gitlab.meta.com.br/documentos/issues/28)
* **documentos#28:** created web/mobile property identification form ([0882def](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0882def6d575f26dd51bfffabf4af7f61fb8d36d)), closes [documentos#28](http://gitlab.meta.com.br/documentos/issues/28)
* **documentos#28:** created web/mobile property identification form ([476f89f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/476f89f70d2d16e44512a823bbd1cde7724c3440)), closes [documentos#28](http://gitlab.meta.com.br/documentos/issues/28)
* **documentos#32:** created user database schema ([1a4ef98](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1a4ef983580dde6bff81f77c7eba368cda7880ff)), closes [documentos#32](http://gitlab.meta.com.br/documentos/issues/32)
* **documentos#32:** removed watermelon. added redux saga ([cd97772](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/cd977725490d8654afc2dad1a0e7bc3d6b16b4dd)), closes [documentos#32](http://gitlab.meta.com.br/documentos/issues/32)





# [0.2.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.1.0...v0.2.0-alpha.0) (2020-09-13)

**Note:** Version bump only for package @smartcoop/forms





# [0.1.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.1.0-alpha.0...v0.1.0) (2020-09-13)


### Bug Fixes

* **documentos#10:** fix equal default error message ([9f0dbf2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9f0dbf2a8241919af56fd992e4edc1611074e4f2))
* **documentos#10:** fix min length password rule ([8964668](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8964668add41c3b7286256712813b0879ffaadb8))
* **documentos#10:** fixed select \o/ documentos[#30](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/30) ([c043c42](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c043c4211b46564f7bf0a50b323a8043d1dfbccc))
* **documentos#10:** reset form ([61be317](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/61be317be8449425670aad04159fe176d510c97d)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **forms:** fix validations ([7c413a5](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7c413a5c916867af845ec3726b4c3d2deae0ddec))
* **forms:** validating cpf and cnpj ([a16da5b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a16da5b080973e282b84400f5be9ce1764db812a))


### Features

* **#10:** created web InputCpfCnpj with mask and validations ([a28957a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a28957ae1578d15e1d20395b6d3df7124638cdc1)), closes [#10](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/10) [#17](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/17)
* **documentos#10:** added address form documentos[#51](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/51) ([a062106](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a06210666826d66582caff88756428ca770a2621)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#10:** added number validation addressForm documentos[#51](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/51) ([3f416bc](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3f416bcee40bf11bf7586659a5aa53da59a86f85)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#10:** added producer identification form documentos[#51](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/51) ([c49234c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c49234cb768879b3a56da13bcfff3b511772bec2)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#10:** added signup form documentos[#51](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/51) ([cb1c455](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/cb1c4555593c10e3f11a613e32a4060454e9046c)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#10:** changed password validator rules ([dde9c05](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/dde9c05b9a046a5c872c9e369b0b311682cc5045)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#10:** created create password form ([a181c58](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a181c5891bfa34a21b64e338699d61c524d6c77a)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#10:** created equal validator ([93fed92](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/93fed92f30a9e8ba3968730d5ed9ccd7cb76a683)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#10:** created identification screen ([5342ee6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/5342ee65eb7129e62b3de0367b3ef086c8e05a4f)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#10:** created input code documentos[#51](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/51) ([1a9d25d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1a9d25d87fb5371aeab53fe9e8c0b38f4284ccbd)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#10:** created input select for mobile ([f4756b6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f4756b692e534db401e7781a8e584f141b688e1a)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10) [#3](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/3)
* **documentos#10:** created InputCep for mobile and web ([fb3753e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/fb3753e38e6a918c2a11d8b10979bc58f684b612)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10) [#6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/6)
* **documentos#10:** created inputDate web and mobile documentos[#18](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/18) ([fd30c7d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/fd30c7d25403c4fbbef7aeb6ed1d1f6f9ba226f5)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#10:** created InputNumber on mobile and web ([fa73f47](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/fa73f477330111329a6249f7a851e029fc2eede4)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10) [#31](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/31)
* **documentos#10:** created InputPhone on mobile and web ([ca48da4](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ca48da4fe7fa83d3051a9925b73452b5aa4a68ba)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10) [#2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/2)
* **documentos#10:** created reset code form ([8f53188](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8f53188f6eaa28929e6034c0cc7700b5b96ad45b)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#10:** created select input and perf forms ([e4e83ab](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e4e83ab2bda01613b50076155333d3c7ce487607)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#10:** finished onboard mocked ([b20abfa](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b20abfaf5a51d791934136a03c7c4714e1cda6b3)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#10:** login and signup screens ([4558694](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4558694308db08a04873004ca9e0db6dce6bbba7)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#10:** mobile inputs styled as material design ([8a9e3cc](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8a9e3cce883733f69190c3eb53ed92aba70e4026)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10) [#7](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/7)
* **documentos#10:** selects ok \o/ ([7f691f4](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7f691f4b648911711a598bb30c447533bf29ad88)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **forms:** @smarcoop/forms finished ([d95dc20](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d95dc20603902124c3b35f531986dcb08e108879))
* **forms:** single validate for web and mobile forms ([9bbeb71](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9bbeb71f1a9c0acac8d6e817e614159eda808a5d))
* **mobile-components:** added input masks ([1e5a7fd](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1e5a7fd25a7faec83814cd330741c420f494a4c1))
