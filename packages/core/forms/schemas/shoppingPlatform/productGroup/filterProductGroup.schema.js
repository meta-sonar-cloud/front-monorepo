import * as Yup from 'yup'

const filterProductGroup = () => Yup.object().shape({
  q: Yup.string(),
  description: Yup.string(),
  cropId: Yup.string()
})

export default filterProductGroup
