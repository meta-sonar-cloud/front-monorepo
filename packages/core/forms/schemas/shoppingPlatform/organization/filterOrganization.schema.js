import * as Yup from 'yup'

const filterOrganization = () => Yup.object().shape({

  isSubsidiary: Yup.string(),

  isActive: Yup.string()
})

export default filterOrganization
