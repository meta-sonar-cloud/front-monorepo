import * as Yup from 'yup'

import flow from 'lodash/fp/flow'

import required from '@smartcoop/forms/validators/required.validator'

const acceptProposal = ({ t }) => Yup.object().shape({
  proposalPaymentFormId: flow(
    required({ t })
  )(Yup.string())
})

export default acceptProposal
