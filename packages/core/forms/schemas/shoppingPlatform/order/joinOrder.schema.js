import * as Yup from 'yup'

import flow from 'lodash/fp/flow'

import required from '@smartcoop/forms/validators/required.validator'

const joinOrder = ({ t }) => Yup.object().shape({
  deliveryLocations: flow(
    required({ t })
  )(Yup.string())
})

export default joinOrder
