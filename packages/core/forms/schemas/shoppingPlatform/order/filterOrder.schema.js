import * as Yup from 'yup'

import date from '@smartcoop/forms/validators/date.validator'

const filterOrder = ({ t }) => Yup.object().shape({
  startDate: date({ t })(Yup.string()),

  productId: Yup.string(),

  statusId: Yup.string()
})

export default filterOrder
