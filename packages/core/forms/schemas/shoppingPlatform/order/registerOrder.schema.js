import * as Yup from 'yup'

import flow from 'lodash/fp/flow'

import date from '@smartcoop/forms/validators/date.validator'
import minDate from '@smartcoop/forms/validators/minDate.validator'
import notEmpty from '@smartcoop/forms/validators/notEmpty.validator'
import required from '@smartcoop/forms/validators/required.validator'

const registerOrder = ({ t, props: { minReceiptDate } }) => Yup.object().shape({
  productGroupId: flow(
    required({ t })
  )(Yup.string()),

  productId: flow(
    required({ t })
  )(Yup.string()),

  receiptDeadline: flow(
    date({ t }),
    minDate({ t, minReceiptDate }),
    required({ t })
  )(Yup.string()),

  paymentForms: flow(
    required({ t, isNotText: true }),
    notEmpty({ t, text: t('this field is required') })
  )(Yup.object()),

  deliveryLocations: flow(
    required({ t })
  )(Yup.string())
})

export default registerOrder
