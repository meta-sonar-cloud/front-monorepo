import * as Yup from 'yup'

import flow from 'lodash/fp/flow'

import required from '@smartcoop/forms/validators/required.validator'

const nonComplianceComment = ({ t }) => Yup.object().shape({
  comment: flow(
    required({ t })
  )(Yup.string())
})

export default nonComplianceComment
