import * as Yup from 'yup'

import flow from 'lodash/fp/flow'

import required from '@smartcoop/forms/validators/required.validator'

const nonCompliance = ({ t }) => Yup.object().shape({
  observations: flow(
    required({ t })
  )(Yup.string())
})

export default nonCompliance
