import * as Yup from 'yup'

import flow from 'lodash/fp/flow'

import float from '@smartcoop/forms/validators/float.validator'
import required from '@smartcoop/forms/validators/required.validator'

const rainRecordFields = ({ t }) =>
  Yup.object().shape({
    occurrenceDate: Yup.string(),

    rainMm: float({ t })(
      Yup.string().required('Preencha a quantidade de chuva')
    ),

    fieldsSelected: flow(
      required({ t, isNotText: true })
    )(Yup.array().nullable()),

    events: Yup.array().nullable(),
    registerFor: required({ t })(Yup.string()),
    fieldId: Yup.string().when('registerFor', {
      is: 'one',
      then:  required({ t })(Yup.string())
    })
  })

export default rainRecordFields
