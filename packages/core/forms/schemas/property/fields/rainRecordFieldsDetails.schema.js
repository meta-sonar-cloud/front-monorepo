import * as Yup from 'yup'

import float from '@smartcoop/forms/validators/float.validator'
import required from '@smartcoop/forms/validators/required.validator'

const rainRecordFieldsDetails = ({ t }) =>
  Yup.object().shape({
    occurrenceDate: Yup.string(),

    rainMm: float({ t })(
      required({ t })(Yup.string())
    ),

    events: Yup.array().nullable()
  })

export default rainRecordFieldsDetails
