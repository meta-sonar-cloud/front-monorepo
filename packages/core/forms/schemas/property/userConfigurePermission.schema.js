import * as Yup from 'yup'

import flow from 'lodash/fp/flow'

import required from '@smartcoop/forms/validators/required.validator'

const userConfigurePermission = ({ t }) => Yup.object().shape({

  permission: flow(
    required({ t })
  )(Yup.string())
})

export default userConfigurePermission
