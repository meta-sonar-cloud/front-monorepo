import * as Yup from 'yup'


import flow from 'lodash/fp/flow'

import date from '@smartcoop/forms/validators/date.validator'
import required from '@smartcoop/forms/validators/required.validator'

const technicalVisit = ({ t }) => Yup.object().shape({
  cultivate: Yup.string(),

  dateVisit: flow(
    date({ t }),
    required({ t })
  )(Yup.string()),

  estimateUnity: Yup.string(),

  productivityEstimate: Yup.string(),

  observations: Yup.string(),

  hour: flow(
    required({ t })
  )(Yup.string())
})

export default technicalVisit
