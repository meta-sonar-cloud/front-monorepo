import * as Yup from 'yup'

import flow from 'lodash/fp/flow'

import dateRange from '@smartcoop/forms/validators/dateRange.validator'
import required from '@smartcoop/forms/validators/required.validator'

const filterTechnicalVisit = ({ t }) => Yup.object().shape({

  proprietary: flow(
    required({ t })
  )(Yup.string()),

  propertyId: flow(
    required({ t })
  )(Yup.string()),

  fieldId: Yup.string(),

  rangeDate: dateRange({ t })(Yup.object())
})

export default filterTechnicalVisit
