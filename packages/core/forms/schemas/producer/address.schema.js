import * as Yup from 'yup'

import flow from 'lodash/fp/flow'

import cep from '@smartcoop/forms/validators/cep.validator'
import number from '@smartcoop/forms/validators/number.validator'
import required from '@smartcoop/forms/validators/required.validator'

const address = ({ t }) => Yup.object().shape({
  postalCode: flow(
    cep({ t }),
    required({ t })
  )(Yup.string()),

  state: flow(
    required({ t })
  )(Yup.string()),

  city: flow(
    required({ t })
  )(Yup.string()),

  district: flow(
  )(Yup.string()),

  street: flow(
  )(Yup.string()),

  number: flow(
    number({ t })
  )(Yup.string())
})

export default address
