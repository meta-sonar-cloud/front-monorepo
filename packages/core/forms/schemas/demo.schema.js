import * as Yup from 'yup'

import flow from 'lodash/fp/flow'

import email from '@smartcoop/forms/validators/email.validator'
import min from '@smartcoop/forms/validators/min.validator'
import required from '@smartcoop/forms/validators/required.validator'

const demoSchema = ({ t, data }) => Yup.object().shape({
  name: required({ t })(Yup.string()),
  email: flow(
    email({ t }),
    required({ t })
  )(Yup.string()),
  address: Yup.object().shape({
    city: flow(
      min({
        t,
        min: 3,
        name: t('city', { howMany: 1 })
      }),
      required({ t })
    )(Yup.string()),
    neighborhood: (() => {
      let YupInstance = Yup.string()
      if (data?.address?.street) {
        YupInstance = required({ t })(YupInstance)
      }
      return YupInstance
    })(),
    street: Yup.string()
  })
})

export default demoSchema
