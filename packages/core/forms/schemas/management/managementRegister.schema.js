import * as Yup from 'yup'

import flow from 'lodash/fp/flow'

import date from '@smartcoop/forms/validators/date.validator'
import number from '@smartcoop/forms/validators/number.validator'
import required from '@smartcoop/forms/validators/required.validator'


const managementSchemaRegister = ({ t }) => Yup.object().shape({

  predictedDate: flow(
    date({ t }),
    required({ t })
  )(Yup.string()),

  operationId: flow(
    required({ t })
  )(Yup.string()),

  productGroupSeeding: (Yup.string()),

  cultivate: (Yup.string()),

  numberOfPlants: Yup.string(),

  lineSpacing: flow(
    number({ t })
  )(Yup.string()),

  replanting: (Yup.string()),

  germination: flow(
    number({ t })
  )(Yup.string()),

  vigor: flow(
    number({ t })
  )(Yup.string()),

  product: (Yup.string()),

  n: flow(
    number({ t })
  )(Yup.string()),

  p: flow(
    number({ t })
  )(Yup.string()),

  k: flow(
    number({ t })
  )(Yup.string()),

  dose: flow(
    number({ t })
  )(Yup.string()),

  applicationMode: (Yup.string().nullable()),

  typeOfApplication: (Yup.string().nullable()),

  productGroup: (Yup.string()),

  products: (Yup.string()),

  hour: (Yup.string()),

  dosePhyntosanitary: flow(
    number({ t })
  )(Yup.string()),

  unit: (Yup.string()),

  unitProductivity: (Yup.string()),

  productivityKg: flow(
    number({ t })
  )(Yup.string()),

  productivityBags: Yup.string(),

  ph: flow(
    number({ t })
  )(Yup.string()),

  wholeGrain: flow(
    number({ t })
  )(Yup.string()),

  germinationProductivity: flow(
    number({ t })
  )(Yup.string()),

  shellAndBran: flow(
    number({ t })
  )(Yup.string()),

  irrigationPlates: flow(
    number({ t })
  )(Yup.string()),

  typeOfIrrigation: flow(
  )(Yup.string()),

  productionSilage: (Yup.string()),

  balesRollsPerHa: flow(
    number({ t })
  )(Yup.string()),

  balesWeightRollsPerHa: flow(
    number({ t })
  )(Yup.string()),

  numberOfAnimals: flow(
    number({ t })
  )(Yup.string()),

  grazingTimePeriod: flow(
    number({ t })
  )(Yup.string()),

  description: (Yup.string()),

  observations: (Yup.string())

})

export default managementSchemaRegister
