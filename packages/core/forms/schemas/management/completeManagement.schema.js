import * as Yup from 'yup'

const field = () => Yup.object().shape({
  closeGrowingSeason: (Yup.string().nullable())
})

export default field
