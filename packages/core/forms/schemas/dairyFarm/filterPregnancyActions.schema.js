import * as Yup from 'yup'

const filterPregnancyActions = () => Yup.object().shape({
  accomplishedDate: Yup.string(),
  type: Yup.string()
})

export default filterPregnancyActions
