import * as Yup from 'yup'

const filterMilkDelivery = () => Yup.object().shape({

  volumeDate: Yup.string(),

  status: Yup.string()
})

export default filterMilkDelivery
