import * as Yup from 'yup'

import flow from 'lodash/fp/flow'

import required from '@smartcoop/forms/validators/required.validator'

const registerPregnancyActions = ({ t }) => Yup.object().shape({
  earring: flow(
    required({ t })
  )(Yup.string()),
  lotId: flow(
    required({ t })
  )(Yup.string()),
  animalId: flow(
  )(Yup.string()),
  type: flow(
    required({ t })
  )(Yup.string()),
  accomplishedDate: flow(
    required({ t })
  )(Yup.string())
})

export default registerPregnancyActions
