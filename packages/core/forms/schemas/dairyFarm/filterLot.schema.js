import * as Yup from 'yup'

const filterLot = () => Yup.object().shape({
  code: Yup.string(),
  name: Yup.string(),
  trinket: Yup.string()
})

export default filterLot
