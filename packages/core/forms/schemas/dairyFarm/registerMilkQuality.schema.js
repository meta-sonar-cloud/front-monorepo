import * as Yup from 'yup'

import flow from 'lodash/fp/flow'

import date from '@smartcoop/forms/validators/date.validator'
// import number from '@smartcoop/forms/validators/number.validator'
import required from '@smartcoop/forms/validators/required.validator'

const registerMilkQuality = ({ t }) => Yup.object().shape({
  date: flow(
    date({ t }),
    required({ t })
  )(Yup.string()),

  protein: flow()(Yup.string()),
  fat: flow()(Yup.string()),
  ccs: flow()(Yup.string()),
  ctb: flow()(Yup.string())

})

export default registerMilkQuality
