import * as Yup from 'yup'

import flow from 'lodash/fp/flow'

import date from '@smartcoop/forms/validators/date.validator'
import required from '@smartcoop/forms/validators/required.validator'

const registerInsemination = ({ t }) => Yup.object().shape({
  // animalId: Yup.string(),
  inseminationDate: flow(
    date({ t }),
    required({ t })
  )(Yup.string()),
  inseminationType: Yup.string(),
  inseminator: Yup.string(),
  bullId: Yup.string(),

  embryoBull: Yup.string(),
  embryoMother: Yup.string()
})

export default registerInsemination
