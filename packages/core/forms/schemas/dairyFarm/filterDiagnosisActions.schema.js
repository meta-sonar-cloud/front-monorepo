import * as Yup from 'yup'

const filterDiagnosisActions = () => Yup.object().shape({
  accomplishedDate: Yup.string(),
  type: Yup.string(),
  result: Yup.string()
})

export default filterDiagnosisActions
