---
name: forms
menu: Core
route: '/core/forms'
---

import { useRef, useState, useCallback, useEffect } from 'react'

import { Form as Unform } from '@unform/web'
import { Playground, Props } from 'docz'
import * as Yup from 'yup'

import {
  FormProvider,
  withField,
  useField,
  Scope,
  reloadSchema
} from '@smartcoop/forms'
import FieldProvider from '@smartcoop/forms/src/providers/FieldProvider'
import demoSchema from '@smartcoop/forms/schemas/demo.schema'
import required from '@smartcoop/forms/validators/required.validator'
import { useT } from '@smartcoop/i18n'

import Button from '@smartcoop/web-components/Button'
import Form from '@smartcoop/web-components/Form'
import InputEmail from '@smartcoop/web-components/InputEmail'
import InputText from '@smartcoop/web-components/InputText'
import JsonViewer from '@smartcoop/web-components/JsonViewer'

import ButtonMobile from '@smartcoop/mobile-components/Button'
import FormMobile from '@smartcoop/mobile-components/Form'
import InputEmailMobile from '@smartcoop/mobile-components/InputEmail'
import InputTextMobile from '@smartcoop/mobile-components/InputText'

# @smartcoop/forms
```js
import {
  FormProvider,
  withField,
  useField,
  Scope,
  reloadSchema
} from '@smartcoop/forms'
```

> **This package provide an [Unform](https://unform.dev/) wrapper core**
> **to make more easy form manipulations  on web and mobile platforms at same time, with same validations on both them.**


## -> Provider
### `FormProvider`
This component provide a **Form scoped** to your application. Can be used to create the Form component for web and mobile platforms.

### Props
<Props of={ FormProvider } />


## HOC
### `withField`
High Order Component that receive an function to provide the Unform registerField options.
It is used to inject the field context into field component.
Visit the Unform official documentation to see all options.

Each field created with this HOC, will be able to receive this props:
<Props of={ FieldProvider } />

----


## Hook
### `useField`
Usage is only inside field components that use HOC `withField`.
```js
const {
  fieldName,
  defaultValue,
  error,
  clearError,
  fieldRef,
  handleChange,
  handleBlur,
  handleChangeNative,
  handleBlurNative,
  required,
  validateField
} = useField()
```

----


## -> `Scope`
This component format all inputs children as a nested object into the data submitted, where the key is the `path` prop.

____


## Schema Constructor
Schemas Constructors are simple functions that receive one object param like:
```json
{
  t, // i18n translator
  data // form data live snapshot
}
```
and return an [Yup](https://github.com/jquense/yup) schema object
with all form fields configurations and validations.

### Example
```js
/* @smartcoop/forms/schemas/demo.schema.js */

import * as Yup from 'yup'

import flow from 'lodash/fp/flow'

import email from '@smartcoop/forms/validators/email.validator'
import min from '@smartcoop/forms/validators/min.validator'
import required from '@smartcoop/forms/validators/required.validator'

const demoSchema = ({ t, data }) => Yup.object().shape({
  name: required({ t })(Yup.string()),
  email: flow(
    email({ t }),
    required({ t })
  )(Yup.string()),
  address: Yup.object().shape({
    city: flow(
      min({
        t,
        min: 3,
        name: t('city', { howMany: 1 })
      }),
      required({ t })
    )(Yup.string()),
    neighborhood: (() => {
      let YupInstance = Yup.string()
      if (data?.address?.street) {
        YupInstance = required({ t })(YupInstance)
      }
      return YupInstance
    })(),
    street: Yup.string()
  })
})

export default demoSchema


```
**Important:** Business schemas are maintained from `@smartcoop/forms/schemas/**/*`

----


## reloadSchema
### `reloadSchema(formRef<React.Ref>, doubleCheck<boolean>)`
Debounced function that reload the form schema. The `doubleCheck` enable reload on first and last function hit.

____


## Validators
Each validator provide a incremented `Yup` intance with the rules to use in your form schema.

Every validator is an high order function that receive the `t` and some configs in the first function,
and receive the `Yup` instance as the parameter of the second function.

So, the validator will return the Yup instance with the rules incremented.

### Creating a custom rule
```js

export default ({ t }) => YupInstance => YupInstance
  .test('my validation', t('my error string'), (value) => {
    return !value // return `false` if has any errors on this input data
  })

```

### Flow validation
You can use many validators to compose the input data validation using the `flow`, provided by
[lodash functional programming](https://github.com/lodash/lodash/wiki/FP-Guide).

The error message will be applied by the last rule that doesn't satisfy the test.
```js
import * as Yup from 'yup'

import flow from 'lodash/fp/flow'

import email from '@smartcoop/forms/validators/email.validator'
import password from '@smartcoop/forms/validators/password.validator'
import required from '@smartcoop/forms/validators/required.validator'

const mySchema = ({ t }) => Yup.object().shape({
  name: required({ t })(Yup.string()),
  email: flow(
    email({ t }),
    required({ t })
  )(Yup.string())
})
```
**Important:** Business validators are maintained from `@smartcoop/forms/validators/*.validator`

----

## Masks
You can use an mask to control and format an data input.
To do it, you need define the input prop `setMask` as a function that receive the input value and some configs, respectivally.

### Mobile mask component
The `@smartcoop/mobile-components/InputText` is masking using the [react-native-masked-text](https://github.com/benhurott/react-native-masked-text) thirty library.
You can define the props `type` and `maskOptions` to use all features about that library.

### Web mask component
The `@smartcoop/web-components/InputText` is masking using the [react-input-mask](https://github.com/sanniassin/react-input-mask) thirty library.

### Example
```js
/* @smartcoop/forms/masks/cpfCnpj.mask.js */
import trimMask from '@meta-awesome/functions/src/trimMask'

import size from 'lodash/size'

export default (value = '', { onlyCpf, onlyCnpj }) => {
  const cpfSize = 11 // without mask
  const isCpf = size(trimMask(value)) <= cpfSize
  if (onlyCpf || (!onlyCnpj && isCpf)) {
    // CPF mask
    // the last digit enables the mask to change to CNPJ
    return `999.999.999-99${ !onlyCpf ? '9' : '' }`
  }
  // CNPJ mask
  return '99.999.999/9999-99'
}
```
and to use:
```js
import React, { useCallback } from 'react'

import PropTypes from 'prop-types'

import cpfCnpjMask from '@smartcoop/forms/masks/cpfCnpj.mask'
import InputText from '@smartcoop/(web|mobile)-components/InputText'

const InputCpfCnpj = (props) => {
  const {
    onlyCpf,
    onlyCnpj,
    ...otherProps
  } = props

  const setMask = useCallback(
    (value) => cpfCnpjMask(value, { onlyCpf, onlyCnpj }),
    [onlyCpf, onlyCnpj]
  )

  return (
    <InputText
      { ...otherProps }
      setMask={ setMask }
    />
  )
}
```
**Important:** Business masks are maintained from `@smartcoop/forms/masks/*`


## Live basic usage

### Web
```js
import React, { useRef, useState, useCallback } from 'react'

import debounce from 'lodash/debounce'

import { Scope, reloadSchema } from '@smartcoop/forms'
import demoSchema from '@smartcoop/forms/schemas/demo.schema'
import { useT } from '@smartcoop/i18n'
import Form from '@smartcoop/web-components/Form'
import Button from '@smartcoop/web-components/Button'
import InputEmail from '@smartcoop/web-components/InputEmail'
import InputText from '@smartcoop/web-components/InputText'
```

<Playground>
  {() => {
    const formRef = useRef(null) // create form DOM reference
    const [data, setData] = useState({})
    const t = useT()
    //
    // subscribe street onChange to regenerate schema
    // and refresh neighborhood rules from lambda function
    const handleStreetChange = useCallback(
      () => reloadSchema(formRef, true),
      [formRef]
    )
    return (
      <Form
        ref={ formRef }
        schemaConstructor={ demoSchema }
        onSubmit={ setData }
      >
        <InputText name="name" label={ t('name') } defaultValue="nome" />
        <InputEmail name="email" label={ t('email') } defaultValue="email@email.com" />
        <Scope path="address">
          <InputText name="city" label={ t('city', { howMany: 1 }) } defaultValue="cidade" />
          <InputText name="neighborhood" label={ t('neighborhood') } />
          <InputText
            name="street"
            label={ t('street') }
            onChange={ handleStreetChange }
          />
        </Scope>
        <Button
          id="submit"
          variant="contained"
          onClick={ () => formRef.current.submit() }
        >
          submit
        </Button>
        <JsonViewer name="data submitted" data={ data } />
      </Form>
    )
  }}
</Playground>

### Mobile
```js
import React, { useRef, useState, useCallback } from 'react'

import debounce from 'lodash/debounce'

import { Scope, reloadSchema } from '@smartcoop/forms'
import demoSchema from '@smartcoop/forms/schemas/demo.schema'
import { useT } from '@smartcoop/i18n'
import FormMobile from '@smartcoop/mobile-components/Form'
import ButtonMobile from '@smartcoop/mobile-components/Button'
import InputEmailMobile from '@smartcoop/mobile-components/InputEmail'
import InputTextMobile from '@smartcoop/mobile-components/InputText'
```

<Playground>
  {() => {
    const formRef = useRef(null) // create form DOM reference
    const [data, setData] = useState({})
    const t = useT()
    //
    // subscribe street onChange to regenerate schema
    // and refresh neighborhood rules from lambda function
    const handleStreetChange = useCallback(
      () => reloadSchema(formRef, true),
      [formRef]
    )
    return (
      <FormMobile
        ref={ formRef }
        schemaConstructor={ demoSchema }
        onSubmit={ setData }
      >
        <InputTextMobile name="name" label={ t('name') } />
        <InputEmailMobile name="email" label={ t('email') } />
        <Scope path="address">
          <InputTextMobile name="city" label={ t('city', { howMany: 1 }) } />
          <InputTextMobile name="neighborhood" label={ t('neighborhood') } />
          <InputTextMobile
            name="street"
            label={ t('street') }
            onChange={ handleStreetChange }
          />
        </Scope>
        <ButtonMobile
          onPress={ () => formRef.current.submit() }
          title="submit"
        />
        <JsonViewer name="data submitted" data={ data } />
      </FormMobile>
    )
  }}
</Playground>


## Manipulating form/field value
```js
import { useRef, useCallback, useEffect } from 'react'

import required from '@smartcoop/forms/validators/required.validator'
import Form from '@smartcoop/web-components/Form'
import InputText from '@smartcoop/web-components/InputText'
```
<Playground>
  {() => {
    const formRef = useRef(null)
    const schemaConstructor = useCallback(
      ({ t }) => Yup.object().shape({
        defaultField: required({ t })(Yup.string()),
        asyncField: required({ t })(Yup.string())
      }),
      []
    )
    return (
      <Form ref={ formRef } schemaConstructor={ schemaConstructor }>
        <InputText
          label="Default field"
          name="defaultField"
          defaultValue="default value"
        />
        <InputText
          name="asyncField"
          label="Async field"
        />
        <div style={ { display: 'flex' } }>
          <Button
            id="set-field-value-web"
            size="small"
            onClick={ () => {
              formRef.current.setData({ asyncField: 'async text' })
            } }
          >
            create async field value
          </Button>
          <Button
            id="clear-field-web"
            size="small"
            onClick={ () => {
              formRef.current.clearField('asyncField')
            } }
          >
            clear async field
          </Button>
          <Button
            id="reset-web"
            size="small"
            onClick={ () => {
              formRef.current.reset()
            } }
          >
            reset form
          </Button>
        </div>
      </Form>
    )
  }}
</Playground>
