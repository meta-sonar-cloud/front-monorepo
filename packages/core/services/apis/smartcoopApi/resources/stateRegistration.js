import api from '../api'

export const getUserStateRegistrations = (params) => api.get('/state-registrations/user', { params })
