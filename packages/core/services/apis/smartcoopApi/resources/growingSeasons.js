import api from '../api'

export const getGrowingSeasons = async (params) => {
  const { data } = await api.get('/growing-seasons', { params })
  return data
}

export const createGrowingSeasons = (params) => api.post('/growing-seasons', params)

export const editGrowingSeasons = (params, { growingSeasonId }) => api.patch(`/growing-seasons/${ growingSeasonId }`, params)

export const deleteGrowingSeasons = ({ growingSeasonId }) => api.delete(`/growing-seasons/${ growingSeasonId }`)
