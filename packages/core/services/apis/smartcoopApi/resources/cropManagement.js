import api from '../api'

export const getCropsManagements = async (params, { growingSeasonId }) => {
  const { data } = await api.get(`/growing-season/${ growingSeasonId }/crops-management`, { params })
  return data
}

export const getCropManagement = async (params, { growingSeasonId, cropManagementId }) => api.get(`growing-season/${ growingSeasonId }/crops-management/${ cropManagementId }`, { params })

export const createCropManagement = (params, { growingSeasonId }) => api.post(`/growing-season/${ growingSeasonId }/crops-management`, params)

export const editCropManagement = (params, { growingSeasonId, cropManagementId }) => api.patch(`/growing-season/${ growingSeasonId }/crops-management/${ cropManagementId }`, params)

export const deleteCropManagements = ({ growingSeasonId, cropManagementId }) => api.delete(`/growing-season/${ growingSeasonId }/crops-management/${ cropManagementId }`)


