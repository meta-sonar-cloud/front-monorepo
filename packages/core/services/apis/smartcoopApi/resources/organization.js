import api from '../api'

export const getAddressesByOrganization = (params, { organizationId }) =>
  api.get(`/orgs/${ organizationId }/related`, { params })

export const getOrganizations = (params) =>
  api.get('/orgs', { params })

export const getOrganizationsByUser = (params) =>
  api.get('/orgs-by-user', { params })

export const getSuppliers = (params) =>
  api.get('/suppliers', { params })

export const getOrganization = (params, { organizationId }) =>
  api.get(`/orgs/${ organizationId }`, { params })

export const createOrganization = (params) =>
  api.post('/orgs', params)

export const editOrganization = (params) =>
  api.put('/orgs', params)

export const deleteOrganization = (params, { organizationId }) =>
  api.delete(`/orgs/${ organizationId }`, params)

export const addOrganizationFiles = (params, { organizationId }) =>
  api.post(`/orgs/${ organizationId }/file`, params, {
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'multipart/form-data'
    }
  })

export const updateSignature = (params, { organizationId }) =>
  api.post(`/eletronic-signature/organization/${ organizationId }`, params)

export const getOrganizationsWithSignatures = (params, { userId }) =>
  api.get(`/eletronic-signature/user/${ userId }`, params)
