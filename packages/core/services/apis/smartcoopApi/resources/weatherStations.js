import map from 'lodash/map'

import api from '../api'


export const getAllWeatherStations = (params) => api.get('/weather-stations/equipments', { params })

export const getWeatherStation = async (params, { weatherStationId }) => {
  const response = await api.get(`/weather-stations/equipments/${ weatherStationId }/measures/latest`, { params })

  const descriptions = map(response.data.descriptions, itemDescription => {
    const dataValue = response.data.data[itemDescription.name]
    return {
      ...itemDescription,
      value: dataValue
    }
  })

  return {
    ...response,
    data: {
      ...response.data,
      descriptions
    }
  }
}
