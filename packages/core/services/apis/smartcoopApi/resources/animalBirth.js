import api from '../api'

export const getAnimalBirths = (params, { propertyId }) =>
  api.get(`/herds-management/property/${ propertyId }/animals-birth`, { params })

export const getAnimalBirthsById = (params, { propertyId, animalId }) =>
  api.get(`/herds-management/property/${ propertyId }/animals-birth/animal/${ animalId }`, { params })

export const createAnimalBirth = (params, { propertyId }) =>
  api.post(`/herds-management/property/${ propertyId }/animals-birth`, params)

export const editAnimalBirth = (params, { animalBirthId, propertyId }) =>
  api.patch(`/herds-management/property/${ propertyId }/animals-birth/${ animalBirthId }`, params)

export const deleteAnimalBirth = ({ propertyId, animalBirthId }) =>
  api.delete(`/herds-management/property/${ propertyId }/animals-birth/${ animalBirthId }`)
