import trimMask from '@meta-awesome/functions/src/trimMask'

import api from '../api'

export const getUser = async (params, { userId }) => {
  const { data } = await api.get(`/users/${ userId }`, { params })
  return data
}

export const getUserByCpf = async (params, { document: doc }) => {
  const { data } = await api.get(`/users/document/${ trimMask(doc) }`, { params })
  return data
}

export const getUserByCpfDateOfBirth = async (
  params,
  { document: doc, dateOfBirth }
) => {
  const { data } = await api.get(`/users/${ trimMask(doc) }/${ dateOfBirth }`, {
    params
  })
  return data
}

export const getUserOrganizations = async ({ userId }) => {
  const { data } = await api.get(`/users/${ userId }/organizations`)
  return data
}

export const getUserOrganizationsByModule = async ({ userId, moduleSlug }) => {
  const { data } = await api.get(
    `/users/${ userId }/organizations/module/${ moduleSlug }`
  )
  return data
}

export const updateUser = async (params, { userId }) =>
  api.patch(`/users/${ userId }`, params)

export const createProfilePicture = async (params, { userId }) =>
  api.post(`/users/${ userId }/profile-photo`, params, {
    headers: {
      Accept: 'application/json',
      'Content-Type': 'multipart/form-data'
    }
  })

export const updateProfilePicture = async (params, { userId, photoId }) =>
  api.put(`/users/${ userId }/profile-photo/${ photoId }`, params, {
    headers: {
      Accept: 'application/json',
      'Content-Type': 'multipart/form-data'
    }
  })
