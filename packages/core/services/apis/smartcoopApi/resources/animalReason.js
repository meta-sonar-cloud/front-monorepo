import api from '../api'

export const getAnimalReasons = (params) =>
  api.get('/herds-management/slaughter-reason', { params })
