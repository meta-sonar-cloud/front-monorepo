import api from '../api'

export const getUserProductsBalance = (params, { organizationId }) => api.get(`/products-balance/organization/${ organizationId }`, { params })

export const getUserProductsBalanceWithQuotation = (params, { organizationId }) => api.get(`/products-balance/product-quotation/organization/${ organizationId }`, { params })
