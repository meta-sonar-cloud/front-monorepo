import api from '../api'

export const getOperations = (params) => api.get('/operations', { params })

export const getOperationsById = (params, { operationId }) => api.get(`/operations/${ operationId }`, { params })

export const getOperation = async (operationId) => {
  const { data } = await api.get(`/operations/${ operationId }`)
  return data
}

