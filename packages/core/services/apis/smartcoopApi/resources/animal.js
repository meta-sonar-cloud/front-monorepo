import api from '../api'

export const getAnimals = (params, { propertyId }) =>
  api.get(`/herds-management/property/${ propertyId }/animals`, { params })

export const getAnimal = (params, { propertyId, animalId }) =>
  api.get(`/herds-management/property/${ propertyId }/animals/${ animalId }`, { params })


export const createAnimal = (params, { propertyId }) =>
  api.post(`/herds-management/property/${ propertyId }/animals`, params)

export const editAnimal = (params, { animalId, propertyId }) =>
  api.patch(`/herds-management/property/${ propertyId }/animals/${ animalId }`, params)

export const deleteAnimal = ({ animalId, propertyId }) =>
  api.delete(`/herds-management/property/${ propertyId }/animals${ animalId }`)
