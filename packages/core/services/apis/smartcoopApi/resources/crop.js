import api from '../api'

export const getCrops = (params) => api.get('/crops', { params })

export const getCropById = (params, { cropId }) => api.get(`/crops/${ cropId }`, { params })

