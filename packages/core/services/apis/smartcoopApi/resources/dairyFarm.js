import isNil from 'lodash/isNil'
import map from 'lodash/map'

import api from '../api'

export const getMilkQualities = async (params, { dairyFarmId }) => {
  const response = await api.get(`/dairy-farms/${ dairyFarmId }/milk-qualities`, { params })

  const data = map(response.data.data, item => {
    const groupedItems = []
    const addGroup = (fieldName) => {
      groupedItems.push({
        date: item.date,
        type: fieldName,
        result: item[fieldName],
        limit: item[`${ fieldName  }Limit`]
      })
    }

    if (!isNil(item.ccs)) addGroup('ccs')
    if (!isNil(item.ctb)) addGroup('ctb')
    if (!isNil(item.protein)) addGroup('protein')
    if (!isNil(item.fat)) addGroup('fat')

    return {
      ...item,
      groupedItems
    }
  })

  return {
    ...response,
    data: {
      ...response.data,
      data
    }
  }
}

export const createMilkQuality = async (params, { dairyFarmId }) => {
  const { data } = await api.post(`/dairy-farms/${ dairyFarmId }/milk-qualities`, params)
  return data
}


export const updateMilkQuality = async (params, { dairyFarmId, milkQualityId }) => {
  const { data } = await api.patch(`/dairy-farms/${ dairyFarmId }/milk-qualities/${ milkQualityId }`, params)
  return data
}

export const deleteMilkQuality = async ({ dairyFarmId, milkQualityId }) => {
  const { data } = await api.delete(`/dairy-farms/${ dairyFarmId }/milk-qualities/${ milkQualityId }`)
  return data
}

export const getMilkDeliveries = (params, { dairyFarmId }) =>  api.get(`/dairy-farms/${ dairyFarmId }/milk-deliveries`, { params })

export const createMilkDelivery = async (params, { dairyFarmId }) => {
  const { data } = await api.post(`/dairy-farms/${ dairyFarmId }/milk-deliveries`, params)
  return data
}

export const getDashboardData = async (params, { dairyFarmId }) =>
  api.get(`/dairy-farms/${ dairyFarmId }/dashboard-data`, { params })

export const createDairyFarm = async (params, { propertyId }) => {
  const { data } = await api.post(`/properties/${ propertyId }/dairy-farms`, params)
  return data
}

export const getDairyFarm = async ({ propertyId } ) => {
  const { data } = await api.get(`/properties/${ propertyId }/dairy-farms`)
  return data
}
export const updateDairyFarm = async (params, { propertyId }) => {
  const { data } = await api.patch(`/properties/${ propertyId }/dairy-farms`, params)
  return data
}

export const updateMilkDelivery = async (params, { dairyFarmId, milkDeliveryId }) => {
  const { data } = await api.put(`/dairy-farms/${ dairyFarmId }/milk-deliveries/${ milkDeliveryId }`, params)
  return data
}

export const deleteMilkDelivery = async ({ dairyFarmId, milkDeliveryId }) => {
  const { data } = await api.delete(`/dairy-farms/${ dairyFarmId }/milk-deliveries/${ milkDeliveryId }`)
  return data
}

export const getPriceData = (params, { dairyFarmId }) =>
  api.get(`/dairy-farms/${ dairyFarmId }/price-datas`, { params })

export const createPriceData = (params, { dairyFarmId }) =>
  api.post(`/dairy-farms/${ dairyFarmId }/price-datas`, params)

export const editPriceData =  (params, { dairyFarmId, priceDataId }) =>
  api.patch(`/dairy-farms/${ dairyFarmId }/price-datas/${ priceDataId }`, params)

export const deletePriceData = ({ dairyFarmId, priceDataId }) =>
  api.delete(`/dairy-farms/${ dairyFarmId }/price-datas/${ priceDataId }`)
