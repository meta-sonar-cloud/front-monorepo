import map from 'lodash/map'
import orderBy from 'lodash/orderBy'
import reduce from 'lodash/reduce'

import api from '../api'

export const getProperties = async (params) => {
  const { data } = await api.get('/properties', { params })
  return data
}

export const getPropertiesByOwner = async (params, { ownerId }) =>
  api.get(`/properties/all-properties/${ ownerId }`, { params })

export const getPropertiesWithData = async (params) =>
  api.get('/properties', { params })

export const selectGetProperties = async (params) =>
  api.get('/properties', { params })

export const createProperty = (params) => api.post('/properties', params)

export const getProperty = (params, { propertyId }) =>
  api.get(`/properties/${ propertyId }`, { params })

export const editProperty = (params, { propertyId }) =>
  api.patch(`/properties/${ propertyId }`, params)

export const deleteProperty = ({ propertyId }) =>
  api.delete(`/properties/${ propertyId }`)

export const getPropertyActivitiesConsolidate = ({ propertyId }) =>
  api.get(`/property/${ propertyId }/activities/consolidate`)

export const getPropertyGrowingSeasonsConsolidate = ({ propertyId }) =>
  api.get(`/growing-seasons/consolidate/${ propertyId }`)

export const getPendingManagements = async ({ propertyId }) => {
  const {
    data: { data }
  } = await api.get(`/properties/${ propertyId }/crop-management`)

  const managements = reduce(
    data,
    (acc, { cropManagement, ...growingSeason }) => [
      ...acc,
      ...map(cropManagement, (crop) => ({
        ...crop,
        growingSeason
      }))
    ],
    []
  )

  return orderBy(managements, 'predictedDate', 'asc')
}
