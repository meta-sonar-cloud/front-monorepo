import api from '../api'

export const getBulls = (params) =>
  api.get('/herds-management/bulls', { params })
