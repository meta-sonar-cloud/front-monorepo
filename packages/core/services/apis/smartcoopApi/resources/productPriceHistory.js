import api from '../api'

export const registerProductPriceHistory = (params, { productId, organizationId }) => api.post(`/products/${ productId }/organization/${ organizationId }/history`, params)

export const getProductsPriceHistoryByOrganization = (params, { productId, organizationId }) => api.get(`/products/${ productId }/organization/${ organizationId }/history`, { params })

export const getProductsPriceHistory = (params, { productId }) => api.get(`/products/${ productId }/history`, { params })

export const deleteProductsPriceHistory = ({ productId, organizationId, historyId }) => api.delete(`/products/${ productId }/organization/${ organizationId }/history/${ historyId }`)




