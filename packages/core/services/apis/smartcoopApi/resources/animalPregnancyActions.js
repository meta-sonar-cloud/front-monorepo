import api from '../api'

export const getAllAnimalDrying = (params, { propertyId, animalId }) =>
  api.get(`/herds-management/property/${ propertyId }/drying/animal/${ animalId }`, { params })

export const createAnimalDrying = (params, { propertyId }) =>
  api.post(`/herds-management/property/${ propertyId }/drying`, params)

export const editAnimalDrying = (params, { dryingId, propertyId }) =>
  api.patch(`/herds-management/property/${ propertyId }/drying/${ dryingId }`, params)

export const deleteAnimalDrying = ({ dryingId, propertyId }) =>
  api.delete(`/herds-management/property/${ propertyId }/drying/${ dryingId }`)
