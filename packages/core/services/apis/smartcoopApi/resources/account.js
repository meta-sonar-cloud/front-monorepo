import api from '../api'

export const getAccountsBalance = (params, { organizationId }) =>
  api.get(`/accounts/organization/${ organizationId }`, { params })
