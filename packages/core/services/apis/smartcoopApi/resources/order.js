import api from '../api'

export const getOrders = (params) => api.get('/purchase-demands', { params })

export const getOrdersByOrganization = (params, { organizationId }) =>
  api.get(`/purchase-demands/orgs/${ organizationId }`, { params })

export const getMyOrdersByOrganization = (params, { organizationId }) =>
  api.get(`/purchase-demands/orgs/${ organizationId }/my-demands`, { params })


export const getOrdersBySupplier = (params, { supplierId }) =>
  api.get(`/purchase-demands/supplier/${ supplierId }`, { params })

export const getOrderById = ({ orderId }) => api.get(`/purchase-demands/${ orderId }`)

export const createOrder = async (params) => api.post('/purchase-demands', params)

export const joinOrder = (params, { orderId }) => api.post(`/purchase-demands/${ orderId }/orgs`, params)

export const exitOrder = ({ organizationPurchaseId, orderId }) => api.delete(`/purchase-demands/${ orderId }/orgs/${ organizationPurchaseId }`)

export const editJoinOrder = (params, { organizationPurchaseId, orderId }) => api.put(`/purchase-demands/${ orderId }/orgs/${ organizationPurchaseId }`, params)

export const acceptProposal = async ({ proposalId }) => api.patch(`/purchase-demands/proposal/${ proposalId }/accept`)
export const rejectProposals = async ({ mainPurchaseId }) => api.patch(`/purchase-demands/proposal/${ mainPurchaseId }/reject-all`)

export const receiveProduct = async (params, { deliveryLocationId }) => api.post(`/purchase-demands/delivery-locations/${ deliveryLocationId }/receipt`, params)

export const attachBankSlip = (params, { deliveryLocationId, supplierId }) =>
  api.post(`/purchase-demands/delivery-locations/${ deliveryLocationId }/bank-slip/${ supplierId }`, params, {
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'multipart/form-data'
    }
  })

export const getBankSlips = async (params) => api.get('/purchase-demands/orgs/bank-slips', { params })

export const deleteBankSlip = async ({ bankSlipId }) =>
  api.delete(`/purchase-demands/bank-slip/${ bankSlipId }`)
