import api from '../api'

export const getCurrent = async (lat, long) => {
  const { data } = await api.get(`/weather/current/${ lat }/${ long }`)
  return data
}

export const getForecast = async (lat, long) => {
  const { data } = await api.get(`/weather/forecast/${ lat }/${ long }`)
  return data
}

export const getAggregatedRain = async (initialDate, finalDate) => {
  const { data } = await api.get('/fields-monitoration/aggregated-rain', {
    params: { initialDate, finalDate }
  })
  return data
}
