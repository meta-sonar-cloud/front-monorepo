import api from '../api'

export const getSalesOrdersByOrg = (params, { organizationId }) => api.get(`/sales-orders/organization/${ organizationId }`, { params })

export const getSettlementDateByOrg = async (params, { organizationId }) => {
  const response = await api.get(`/sales-orders/settlement-date/${ organizationId }`, { params })
  return { data: { data: response.data.settlementDates } }
}

export const getProductBatchByOrg = async (params, { organizationId }) => {
  const response = await api.get(`/sales-orders/product-batch/organization/${ organizationId }`, { params })
  return { data: { data: response.data.productBatchs } }
}

export const createSalesOrder = async (params, { organizationId }) => api.post(`/sales-orders/cash-sale/${ organizationId }`, params)
export const createFutureOrder = async (params, { organizationId }) => api.post(`/sales-orders/future-sale/${ organizationId }`, params)
export const createScheduledOrder = async (params, { organizationId }) => api.post(`/sales-orders/scheduled-sale/${ organizationId }`, params)

export const deleteSaleOrder = async (params, { saleOrderId, organizationId }) => api.delete(`/sales-orders/${ saleOrderId }/organization/${ organizationId }`, params)

export const validationSignatureEletronic = async (params, { organizationId }) => api.post(`eletronic-signature/organization/${ organizationId }/validate`, params)
