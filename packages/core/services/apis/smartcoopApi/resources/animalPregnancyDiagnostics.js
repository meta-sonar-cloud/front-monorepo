import api from '../api'

export const getAllAnimalPregnancyDiagnostics = (params, { animalId }) =>
  api.get(`/herds-management/animals/${ animalId }/pregnancy-diagnostics`, {
    params
  })

export const getAnimalPregnancyDiagnostic = (
  params,
  { animalId, pregnancyId }
) =>
  api.get(
    `/herds-management/animals/${ animalId }/pregnancy-diagnostics/${ pregnancyId }`,
    {
      params
    }
  )

export const createAnimalPregnancyDiagnostic = (params, { animalId }) =>
  api.post(
    `/herds-management/animals/${ animalId }/pregnancy-diagnostics`,
    params
  )

export const editAnimalPregnancyDiagnostic = (
  params,
  { animalId, pregnancyId }
) =>
  api.put(
    `/herds-management/animals/${ animalId }/pregnancy-diagnostics/${ pregnancyId }`,
    params
  )

export const getAnimalPregnancyDiagnosticsTypes = (params, { animalId }) =>
  api.get(`/herds-management/animals/${ animalId }/pregnancy-diagnostics/types`, {
    params
  })
