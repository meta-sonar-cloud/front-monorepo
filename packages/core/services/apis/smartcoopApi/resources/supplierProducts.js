import api from '../api'

const URL_DEFAULT = '/suppliers/:supplierId/products'

export const getSupplierProducts = async (params, { supplierId }) =>
  api.get(URL_DEFAULT.replace(':supplierId', supplierId), params)

export const postSupplierProducts = async (params, { supplierId }) =>
  api.post(URL_DEFAULT.replace(':supplierId', supplierId), params)

export const deleteSupplierProduct = async (params, { supplierId, productId }) =>
  api.delete(`/suppliers/${ supplierId }/products/${ productId }`, params)
