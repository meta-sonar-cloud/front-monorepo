import api from '../api'

export const getAnimalBreeds = (params) =>
  api.get('/herds-management/breeds', { params })
