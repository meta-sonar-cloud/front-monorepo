import api from '../api'

export const getAnimalPevList = (params, { animalId }) =>
  api.get(`/herds-management/animal/${ animalId }/pev`, { params })

export const createAnimalPev = (params, { propertyId, animalId }) =>
  api.post(`/herds-management/property/${ propertyId }/pev/${ animalId }`, params)

export const editAnimalPev = (params, { propertyId, pevId }) =>
  api.patch(`/herds-management/property/${ propertyId }/pev/${ pevId }`, params)

export const deleteAnimalPev = ({ pevId }) =>
  api.delete(`/herds-management/pev/${ pevId }`)
