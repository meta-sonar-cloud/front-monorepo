import api from '../api'

export const getProductGroups = (params) =>
  api.get('/product-groups', { params })

export const createProductGroup = (params) =>
  api.post('/product-groups', params)

export const editProductGroup = (params, { productGroupId }) =>
  api.patch(`/product-groups/${ productGroupId }`, params)

export const deleteProductGroup = ({ productGroupId }) =>
  api.delete(`/product-groups/${ productGroupId }`)
