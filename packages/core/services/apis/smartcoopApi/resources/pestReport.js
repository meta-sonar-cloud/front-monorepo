import api from '../api'

export const getPestReportTypes = (params) =>
  api.get('/plagues/growing-seasons/report-types', { params })

export const createPestReport = (params, { growingSeasonId }) =>
  api.post(`/plagues/growing-seasons/${ growingSeasonId }/report`, params)

export const editPestReport = (params, { reportId }) =>
  api.patch(`/plagues/growing-seasons/report/${ reportId }`, params)

export const deletePestReport = ({ reportId }) =>
  api.delete(`/plagues/growing-seasons/report/${ reportId }`)

export const postFilesPestReport = (params, { reportId }) =>
  api.post(`/plagues/growing-seasons/report/${ reportId }/file`, params, {
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'multipart/form-data'
    }
  })

export const putFilesPestReport = (params, { reportId }) =>
  api.put(`/plagues/growing-seasons/report/${ reportId }/file`, params)

export const getDiseases = (params) =>
  api.get('/disease', { params })

export const getWeeds = (params) =>
  api.get('/weed', { params })

export const getPests = (params) =>
  api.get('/pest', { params })
