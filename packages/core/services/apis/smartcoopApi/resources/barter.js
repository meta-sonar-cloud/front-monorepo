import api from '../api'

export const getBartersByOrg = (params, { organizationId }) => api.get(`/barters/organization/${ organizationId }`, { params })
export const getPackagesByOrg = (params, { organizationId }) => api.get(`/barters/packages/organization/${ organizationId }`, { params })
export const createBarter = async (params) => api.post('/barters', params)
export const joinBarter = async (params) => api.post('/barters/participate', params)
export const editBarter = async (params, { barterOrderNumber, organizationId }) => api.patch(`/barters/${ barterOrderNumber }/organization/${ organizationId }`, params)
export const editBarterPackageOrder = async (params, { barterOrderNumber, organizationId }) => api.patch(`/barters/order/${ barterOrderNumber }/organization/${ organizationId }`, params)
export const deleteBarter = async (params, { barterOrderNumber, organizationId }) => api.delete(`/barters/${ barterOrderNumber }/organization/${ organizationId }`, params)