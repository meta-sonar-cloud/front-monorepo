import api from '../api'

export const getBestProposal = async (params, { orderId }) => api.get(`/purchase-demands/proposal/better/${ orderId }`, { params })

export const acceptBestProposal = async (params, { proposalId }) => api.post(`/purchase-demands/proposal/better/${ proposalId }`, { acceptedProposal: true, ...params })

export const refuseBestProposal = async (params, { proposalId }) => api.post(`/purchase-demands/proposal/better/${ proposalId }`, { acceptedProposal: false, ...params })
