import api from '../api'

export const getAccountSercuritiesMovement = (params,  { accountId, organizationId }) => api.get(`/securities-movement/account/${ accountId }/organization/${ organizationId }`, { params })
export const getSecuritieMovementInitialDate = (params,  { accountId, organizationId }) => api.get(`/securities-movement/initial-date/account/${ accountId }/organization/${ organizationId }`, { params })
