import api from '../api'

export const getProductsQuotationByOrg = (params, { organizationId }) =>
  api.get(`/products-quotation/organization/${ organizationId }`, { params })
