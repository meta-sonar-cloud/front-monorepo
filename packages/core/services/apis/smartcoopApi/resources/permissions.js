import api from '../api'

export const getPermissions = (params) => api.get('/permissions', { params })
