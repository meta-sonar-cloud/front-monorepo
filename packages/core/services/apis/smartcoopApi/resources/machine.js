import api from '../api'

export const getMachinery = (params) => api.get('/machines', { params })

export const deleteMachine = ({ machineId }) => api.delete(`/machines/${ machineId }`)

export const createMachine = (params) =>
  api.post('/machines', params)

export const getMachinesTypes = (params) => api.get('/machines/types', { params })

export const getMachinesBrands = (params) => api.get('/machines/brands', { params })

export const editMachine = (params, { machineId }) =>
  api.patch(`/machines/${ machineId }`, params)

export const addMachineFiles = (params, { machineId }) =>
  api.post(`/machines/${ machineId }/file`, params, {
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'multipart/form-data'
    }
  })

export const editMachineFiles = (params, { machineId }) =>
  api.put(`/machines/${ machineId }/file`, params)
