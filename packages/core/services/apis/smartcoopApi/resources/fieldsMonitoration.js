import api from '../api'

const URL_BASE = '/fields-monitoration/property'

export const getFieldMonitoration = async (params, { fieldId }) =>
  api.get(`/fields-monitoration/field/${ fieldId }/monitoration`, { params })

export const getFieldWeatherHistoric = async (params, { fieldId }) =>
  api.get(`/weather/historic/${ fieldId }`, { params })

export const getPropertyPrecipitations = (params, { propertyId }) => api.get(`${ URL_BASE }/${ propertyId }/precipitations`, { params })

export const createFieldPrecipations = (params, { propertyId }) => api.post(`${ URL_BASE }/${ propertyId }/precipitations`, params)

export const editFieldPrecipations = (params, { propertyId, precipitationId }) => api.put(`${ URL_BASE }/${ propertyId }/precipitations/${ precipitationId }`, params)

export const getFieldPrecipitation = async (params, { fieldId }) =>
  api.get(`${ URL_BASE }/${ fieldId }/precipitation`, { params })

export const deleteFieldPrecipitation = async (
  { propertyId, precipitationId }
) => api.delete(`${ URL_BASE }/${ propertyId }/precipitations/${ precipitationId }`)
