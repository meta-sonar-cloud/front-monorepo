import api from '../api'

export const getActivities = async (params) => {
  const { data } = await api.get('/activities', { params })
  return data
}

export const createActivity = (params) => api.post('/activities', params)

export const editActivity = ({ params }, { activityId }) => api.patch(`/activities/${ activityId }`, params)

export const deleteActivity = ({ activityId }) => api.delete(`/activities/${ activityId }`)
