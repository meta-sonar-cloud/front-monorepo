import api from '../api'

export const getAnimalStatus = (params) =>
  api.get('/herds-management/status', { params })
