import moment from 'moment/moment'

import map from 'lodash/map'

import { momentBackDateTimeFormat, momentFriendlyDateFormat } from '@smartcoop/utils/dates'

import api from '../api'

const transformTechnicalVisit = (item) => ({
  ...item,
  groupDate: moment(item.visitDateTime, momentBackDateTimeFormat).format(momentFriendlyDateFormat)
})

export const registerTechnicalVisit = (params, { propertyId }) => api.post(`/technical-visits/${ propertyId }`, params)

export const editTechnicalVisit = (params, { visitId }) => api.patch(`/technical-visits/${ visitId }`, params)

export const requestAccessForProprietary = (params) => api.post('/technicians/access/request', params)

export const acceptAccessTechnical = (params, { accessRequestId }) => api.patch(`/technicians/access/${ accessRequestId }/accept`, params)

export const getTechnicalVisitUsersList = (params) => api.get('/technical-visits/users', { params })

export const getTechnicalPortfolio = (params) => api.get('/technicians/portfolio', { params })

export const getTechniciansRequestList = (params) => api.get('/technicians/proprietary/requests', { params })

export const getTechnicalVisitById = async (params, { technicalVisitId }) => {
  const response = api.get(`/technical-visits/${ technicalVisitId }`, { params })
  response.data = transformTechnicalVisit(response.data)
  return response
}

export const getTechnicalVisits = async (params) => {
  const response = await api.get('/technical-visits', { params })

  return {
    ...response,
    data: {
      ...response.data,
      data: map(response.data.data, transformTechnicalVisit)
    }
  }
}

export const deleteVisit = ({ technicalVisitId }) => api.delete(`/technical-visits/${ technicalVisitId }`)

export const revokeAccess = ({ id, statusId }) => api.patch(`/technicians/access/${ id }/revoke/${ statusId }`)





