import api from '../api'

export const getInseminations = (params, { animalId }) =>
  api.get(`/herds-management/animals/${ animalId }/inseminations`, { params })

export const getInseminationTypes = (params, { animalId }) =>
  api.get(`/herds-management/animals/${ animalId }/inseminations/types`, { params })

export const getBulls = (params) =>
  api.get('/herds-management/bulls', { params })

export const createInsemination = async (params, { animalId }) => {
  const { data } = await api.post(`/herds-management/animals/${ animalId }/inseminations`, params)
  return data
}

export const updateInsemination = async (params, { animalId, inseminationId }) => {
  const { data } = await api.patch(`/herds-management/animals/${ animalId }/inseminations/${ inseminationId }`, params)
  return data
}

export const deleteInsemination = async (params, { animalId, inseminationId }) => {
  const { data } = await api.delete(`/herds-management/animals/${ animalId }/inseminations/${ inseminationId }`, params)
  return data
}
