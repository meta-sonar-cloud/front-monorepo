import api from '../api'

export const getLots = (params, { propertyId }) =>
  api.get(`/property/${ propertyId }/lots`, { params })

export const createLot = (params, { propertyId }) =>
  api.post(`/property/${ propertyId }/lots`, params)

export const updateLot =  (params, { propertyId, lotId }) =>
  api.patch(`/property/${ propertyId }/lots/${ lotId }`, params)

export const deleteLot = ({ propertyId, lotId }) =>
  api.delete(`/property/${ propertyId }/lots/${ lotId }`)
