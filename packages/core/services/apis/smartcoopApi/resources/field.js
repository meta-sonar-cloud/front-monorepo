import isEmpty from 'lodash/isEmpty'
import last from 'lodash/last'
import map from 'lodash/map'

import api from '../api'
import { getCropById } from './crop'
import { getCropsManagements } from './cropManagement'
import { getCultivationGoalById } from './cultivationsGoal'

export const getFields = async (params) => {
  const { data } = await api.get('/fields', { params })
  return map(data, item => {
    const growingSeason = last(item.growingSeasons) || {}
    let { crop, cropId } = growingSeason
    if (growingSeason?.otherCrop) {
      crop = {
        ...crop,
        description: growingSeason?.otherCrop
      }
      cropId = growingSeason?.otherCrop
    }
    return ({
      ...item,
      growingSeason: {
        ...growingSeason,
        ...api(!isEmpty(growingSeason) ? { crop, cropId } : {})
      }
    })
  })
}

export const getFieldsByPropertyId = async (params, { propertyId }) => {
  const { data } = await api.get(`/fields/property/${ propertyId }`, { params })
  return map(data, item => {
    const growingSeason = last(item.growingSeasons) || {}
    let { crop, cropId } = growingSeason

    if (growingSeason?.otherCrop) {
      crop = {
        ...crop,
        description: growingSeason?.otherCrop
      }
      cropId = growingSeason?.otherCrop
    }
    return ({
      ...item,
      growingSeason: {
        ...growingSeason,
        ...(!isEmpty(growingSeason) ? { crop, cropId } : {})
      }
    })
  })
}

export const getField = async ({ fieldId }) => {
  const { data } = await api.get(`/fields/${ fieldId }`)
  const growingSeason = last(data.growingSeasons)

  let newKeys = {
    cultivation: {},
    crop: {}
  }
  if (!growingSeason) {
    return data
  }

  const promises = [
    getCultivationGoalById({}, { cultivationId: growingSeason.cultivationGoalId })
  ]

  let { cropId, crop = {} } = growingSeason
  if (cropId) {
    promises.push(getCropById({}, { cropId }))
  }

  const [
    { data: cultivation },
    cropResponse
  ] = await Promise.all(promises)
  if (cropResponse?.data) {
    crop = cropResponse.data
  } else if (growingSeason?.otherCrop) {
    crop = {
      ...crop,
      description: growingSeason?.otherCrop
    }
    cropId = growingSeason?.otherCrop
  }

  newKeys = {
    cropId,
    crop: crop || newKeys.crop,
    cultivation: cultivation || newKeys.cultivation
  }

  const { data: cropsManagements } = await getCropsManagements({}, { growingSeasonId: growingSeason.id })

  return {
    ...data,
    growingSeason: {
      ...growingSeason,
      cropsManagements,
      ...newKeys
    }
  }
}

export const createField = (params) => api.post('/fields', params)

export const editField = (params, { fieldId }) => api.patch(`/fields/${ fieldId }`, params)

export const deleteField = ({ fieldId }) => api.delete(`/fields/${ fieldId }`)
