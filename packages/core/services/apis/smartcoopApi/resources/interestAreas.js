import api from '../api'

export const getInterestAreas = (params) => api.get('/interest-areas', { params })
