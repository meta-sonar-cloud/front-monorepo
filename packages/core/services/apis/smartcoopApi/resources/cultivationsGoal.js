import api from '../api'

export const getCultivationsGoal = (params) => api.get('/cultivations-goal', { params })

export const getCultivationGoalById = (params, { cultivationId }) => api.get(`/cultivations-goal/${ cultivationId }`, { params })
