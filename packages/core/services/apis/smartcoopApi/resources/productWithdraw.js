import api from '../api'

export const getUserProductsWithdraw = (params, { organizationId }) => api.get(`/products-withdraw/organization/${ organizationId }`, { params })

export const getPropertaryProductsWithdraw = (params, { organizationId, userId }) => api.get(`/products-withdraw/organization/${ organizationId }/user/${ userId }`, { params })
