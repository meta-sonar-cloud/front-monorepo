import { REACT_APP_FIREBASE_PROJECT_ID } from 'react-native-dotenv'

import firebaseAuth from '@react-native-firebase/auth'
import firebaseFirestore from '@react-native-firebase/firestore'
import messaging from '@react-native-firebase/messaging'
import axios from 'axios'

const firebaseFunctions = axios.create({
  baseURL: `http://us-central1-${ REACT_APP_FIREBASE_PROJECT_ID }.cloudfunctions.net/`
})

const firebaseMessaging = messaging()

export {
  firebaseAuth,
  firebaseFirestore,
  firebaseFunctions,
  messaging,
  firebaseMessaging
}
