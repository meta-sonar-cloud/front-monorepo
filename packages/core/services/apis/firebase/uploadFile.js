import { firebaseStorage } from './firebase'

const uploadFile = ({ file, fileName = '', onProgress = () => {} } = {}) => new Promise((resolve, reject) => {
  const storageRef = firebaseStorage().ref(fileName)
  const task = storageRef.put(file)

  const progress = (snapshot) => {
    const percent = (snapshot.bytesTransferred / snapshot.totalBytes) * 100
    onProgress(percent, `${ percent }%`, snapshot)
  }

  const complete = () => {
    try {
      const url = task.snapshot.ref.getDownloadURL()
      resolve(url)
    } catch (e) {
      reject(e)
    }
  }

  const error = (e) => {
    reject(e)
  }

  task.on('state_changed', progress, error, complete)
})

export default uploadFile
