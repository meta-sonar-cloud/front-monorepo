import { REACT_APP_FIREBASE_API_KEY } from 'react-native-dotenv'

import axios from 'axios'

import get from 'lodash/get'

const http = (options = {}) => {
  if (options.method === 'GET') {
    // eslint-disable-next-line no-param-reassign
    options = {
      ...options,
      params: options.data
    }
  }
  return axios(options)
}


async function firebaseDynamicLinksApi(options = {}) {
  try {

    const headers = {
      'Content-Type': 'application/json',
      ...get(options, 'headers', {})
    }

    const response = await http({
      baseURL: 'https://firebasedynamiclinks.googleapis.com/v1/',
      ...options,
      headers
    })

    return response
  } catch (e) {
    const error = get(e, 'response.data.error', e)
    throw error
  }
}

export const createDynamicLink = async (link) => {
  const { data: { shortLink } } = await firebaseDynamicLinksApi({
    method: 'POST',
    url: '/shortLinks',
    params: {
      key: process.env.REACT_APP_FIREBASE_API_KEY || REACT_APP_FIREBASE_API_KEY
    },
    data: {
      longDynamicLink: link
    }
  })

  return shortLink
}
