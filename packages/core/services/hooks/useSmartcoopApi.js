import smartcoopApi from '../apis/smartcoopApi/api'
import useFetch from './useFetch'

const useSmartcoopApi = (url, ...axiosParams) => useFetch(smartcoopApi, url, ...axiosParams)

export default useSmartcoopApi
