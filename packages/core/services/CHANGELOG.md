# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [0.18.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.17.0...v0.18.0) (2021-04-19)


### Features

* merge develop ([65962b2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/65962b2d2d1752343c77861b94a416495ea3cb77))
* wip ([a20c949](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a20c949da809ca74018f6f2ff36d63d6fa877bcc))
* **documentos#1737:** fixes date ([7655dc3](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7655dc31745f35cd9747b837b5fe56763cf5ba26))
* wip ([ed88867](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ed88867dbcb9af5b51eff1164ff708db209a3b95))





# [0.17.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.16.0...v0.17.0) (2021-04-15)


### Bug Fixes

* **develop:** fix change organization modal in dairyfarm ([5677ec0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/5677ec08c6fd8b9db7884be5231923490e672d20))


### Features

* **documentos#1715:** renaming things and logic changes ([7b0ad3c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/7b0ad3c5f4fd006fd458cfb31d4817270f4d561b)), closes [documentos#1715](http://gitlab.meta.com.br/documentos/issues/1715)





# [0.16.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.15.0...v0.16.0) (2021-04-13)


### Features

* **documentos#1579:** lot changes ([f63dc6d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f63dc6dca603cdd367d6f6497f14e8abe3031ca4)), closes [documentos#1579](http://gitlab.meta.com.br/documentos/issues/1579)
* merge com a develop ([702e143](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/702e1434c3eab83403ccbd51d34a6340a05613b9))
* wip ([e0606ab](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e0606ab15f17990390f972120a71bef8ffb7304e))
* **#1484:** integration with backend weather stations details ([c776b4e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c776b4eb5720ad698dc40ce90bc003c42b18027a)), closes [#1484](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1484) [#484](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/484)
* **#1484:** map station list  integration api, web and mobile ([828c5e9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/828c5e9871594d5a6ad6623348cfcaf4cebea8c2)), closes [#1484](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1484) [#484](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/484)
* **documentos#1032:** added permission validator ([9b65368](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9b6536896ee9eea1c56af979b18d3850f4e25c40)), closes [documentos#1032](http://gitlab.meta.com.br/documentos/issues/1032)
* **documentos#1032:** created list all suppliers route ([b826ce2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b826ce26f7dbaab60c748435940c8526d4562404)), closes [documentos#1032](http://gitlab.meta.com.br/documentos/issues/1032)
* **documentos#1316:** created animal pregnancy resource ([94ccc6a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/94ccc6a483198ec9f430ed012e31c9296775d558)), closes [documentos#1316](http://gitlab.meta.com.br/documentos/issues/1316)
* **documentos#1325:** created PEV on web ([d0d6dda](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d0d6ddae85eeab6cd3b022ac0cf40c8db8aa7d06)), closes [documentos#1325](http://gitlab.meta.com.br/documentos/issues/1325)
* wip ([a1daf61](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a1daf6149af8d967fff4afe4009a7f2a116d8917))
* **documentos#1490:** wip LotList ([ebcbf71](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ebcbf71bdfaa03e8f18c82bb005479bba921932d)), closes [documentos#1490](http://gitlab.meta.com.br/documentos/issues/1490)
* **documentos#1495:** modals, saga and item ([a04f400](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a04f400ff9eb8feddfbef787e6b8fd7121985631)), closes [documentos#1495](http://gitlab.meta.com.br/documentos/issues/1495)
* **documentos#1579:** creates dropdown menu ([3cd3342](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3cd334246c3e3ec8b6aedac15559ee82d209149b)), closes [documentos#1579](http://gitlab.meta.com.br/documentos/issues/1579)
* **documentos#1579:** wip ([a8def31](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a8def31a30ea41a4f4700a4eb404572454c18c83)), closes [documentos#1579](http://gitlab.meta.com.br/documentos/issues/1579)







**Note:** Version bump only for package @smartcoop/services





# [0.15.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.12.0-alpha.1...v0.15.0) (2021-03-23)


### Features

* **documentos#1305:** removes birthday from onboarding ([5eda681](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/5eda6815388bc7289d1d33d1e6896faca4e4aa68)), closes [documentos#1305](http://gitlab.meta.com.br/documentos/issues/1305)
* resolve [#1285](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1285) ([c0b4d91](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c0b4d91ccdb7ff7adcaaf87175f1c32fe17b1c48))
* **documentos#1287:** added min and max into timeline chart ([4397278](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4397278c144645bfccd6d7bbd6045816e3e0e4d0)), closes [documentos#1287](http://gitlab.meta.com.br/documentos/issues/1287)
* merge develop ([2731148](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2731148d7380e3ab5c8ec38d198b1773467e4364))
* **documenots#1292:** added new securitie movement route ([63e6408](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/63e6408aaad104ffe36b50634c0c15c823ee7352)), closes [documenots#1292](http://gitlab.meta.com.br/documenots/issues/1292)
* **documenots#1292:** changed get user stat registrations route ([1c4493b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1c4493bb0081d328cad110c75ec06a42c01465f1)), closes [documenots#1292](http://gitlab.meta.com.br/documenots/issues/1292)
* **documentos#1240:** add updateSignature to organzationStore ([283be60](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/283be607a59b92d1c6241dad577db4d557d52de7)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** new route to validate password ([260ac94](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/260ac94e7a6c56b2a791f4218fb6e55c0e49aa59)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** updateSignature calling correctyl ([15b1a4e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/15b1a4e5a824249a89f0e087861d920bda53b20e)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1282:** created state registration service ([07c6e8c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/07c6e8c3ef1464a0d7bcb77cc5b9ecdd5e8ad5e2)), closes [documentos#1282](http://gitlab.meta.com.br/documentos/issues/1282)
* **documentos#1290:** authentication services ([13ea1c5](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/13ea1c51404c6234f95bec84b11c05b20d673c68)), closes [documentos#1290](http://gitlab.meta.com.br/documentos/issues/1290)
* **documentos#1290:** changes API ([84de8d8](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/84de8d8b4460adc9d56d1cceb40e9f8dc3d01dee)), closes [documentos#1290](http://gitlab.meta.com.br/documentos/issues/1290)
* **documentos#1290:** fixes ([93e2f6f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/93e2f6f1ca671d12b34b2f21fbd7c531bb4a0ef5)), closes [documentos#1290](http://gitlab.meta.com.br/documentos/issues/1290)
* **documentos#1290:** fixes onboarding web ([bf7068d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/bf7068d2fe2cd278cefb1d8bc4186dc8a40b8fb3))
* **documentos#1323:** changes routes to correct ones ([fe3980d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/fe3980dd6b2f6e0ba924e29c497abdfa876652ae)), closes [documentos#1323](http://gitlab.meta.com.br/documentos/issues/1323)
* wip ([16b3eef](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/16b3eef778dda67fd818f50aaaefb2eb54901e34))





# [0.15.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.12.0-alpha.1...v0.15.0) (2021-03-23)


### Features

* **documentos#1305:** removes birthday from onboarding ([5eda681](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/5eda6815388bc7289d1d33d1e6896faca4e4aa68)), closes [documentos#1305](http://gitlab.meta.com.br/documentos/issues/1305)
* resolve [#1285](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1285) ([c0b4d91](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c0b4d91ccdb7ff7adcaaf87175f1c32fe17b1c48))
* **documentos#1287:** added min and max into timeline chart ([4397278](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4397278c144645bfccd6d7bbd6045816e3e0e4d0)), closes [documentos#1287](http://gitlab.meta.com.br/documentos/issues/1287)
* merge develop ([2731148](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2731148d7380e3ab5c8ec38d198b1773467e4364))
* **documenots#1292:** added new securitie movement route ([63e6408](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/63e6408aaad104ffe36b50634c0c15c823ee7352)), closes [documenots#1292](http://gitlab.meta.com.br/documenots/issues/1292)
* **documenots#1292:** changed get user stat registrations route ([1c4493b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1c4493bb0081d328cad110c75ec06a42c01465f1)), closes [documenots#1292](http://gitlab.meta.com.br/documenots/issues/1292)
* **documentos#1240:** add updateSignature to organzationStore ([283be60](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/283be607a59b92d1c6241dad577db4d557d52de7)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** new route to validate password ([260ac94](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/260ac94e7a6c56b2a791f4218fb6e55c0e49aa59)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** updateSignature calling correctyl ([15b1a4e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/15b1a4e5a824249a89f0e087861d920bda53b20e)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1282:** created state registration service ([07c6e8c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/07c6e8c3ef1464a0d7bcb77cc5b9ecdd5e8ad5e2)), closes [documentos#1282](http://gitlab.meta.com.br/documentos/issues/1282)
* **documentos#1290:** authentication services ([13ea1c5](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/13ea1c51404c6234f95bec84b11c05b20d673c68)), closes [documentos#1290](http://gitlab.meta.com.br/documentos/issues/1290)
* **documentos#1290:** changes API ([84de8d8](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/84de8d8b4460adc9d56d1cceb40e9f8dc3d01dee)), closes [documentos#1290](http://gitlab.meta.com.br/documentos/issues/1290)
* **documentos#1290:** fixes ([93e2f6f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/93e2f6f1ca671d12b34b2f21fbd7c531bb4a0ef5)), closes [documentos#1290](http://gitlab.meta.com.br/documentos/issues/1290)
* **documentos#1290:** fixes onboarding web ([bf7068d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/bf7068d2fe2cd278cefb1d8bc4186dc8a40b8fb3))
* **documentos#1323:** changes routes to correct ones ([fe3980d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/fe3980dd6b2f6e0ba924e29c497abdfa876652ae)), closes [documentos#1323](http://gitlab.meta.com.br/documentos/issues/1323)
* wip ([16b3eef](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/16b3eef778dda67fd818f50aaaefb2eb54901e34))





# [0.15.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.12.0-alpha.1...v0.15.0) (2021-03-23)


### Features

* **documentos#1305:** removes birthday from onboarding ([5eda681](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/5eda6815388bc7289d1d33d1e6896faca4e4aa68)), closes [documentos#1305](http://gitlab.meta.com.br/documentos/issues/1305)
* resolve [#1285](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1285) ([c0b4d91](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c0b4d91ccdb7ff7adcaaf87175f1c32fe17b1c48))
* **documentos#1287:** added min and max into timeline chart ([4397278](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4397278c144645bfccd6d7bbd6045816e3e0e4d0)), closes [documentos#1287](http://gitlab.meta.com.br/documentos/issues/1287)
* merge develop ([2731148](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2731148d7380e3ab5c8ec38d198b1773467e4364))
* **documenots#1292:** added new securitie movement route ([63e6408](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/63e6408aaad104ffe36b50634c0c15c823ee7352)), closes [documenots#1292](http://gitlab.meta.com.br/documenots/issues/1292)
* **documenots#1292:** changed get user stat registrations route ([1c4493b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1c4493bb0081d328cad110c75ec06a42c01465f1)), closes [documenots#1292](http://gitlab.meta.com.br/documenots/issues/1292)
* **documentos#1240:** add updateSignature to organzationStore ([283be60](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/283be607a59b92d1c6241dad577db4d557d52de7)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** new route to validate password ([260ac94](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/260ac94e7a6c56b2a791f4218fb6e55c0e49aa59)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** updateSignature calling correctyl ([15b1a4e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/15b1a4e5a824249a89f0e087861d920bda53b20e)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1282:** created state registration service ([07c6e8c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/07c6e8c3ef1464a0d7bcb77cc5b9ecdd5e8ad5e2)), closes [documentos#1282](http://gitlab.meta.com.br/documentos/issues/1282)
* **documentos#1290:** authentication services ([13ea1c5](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/13ea1c51404c6234f95bec84b11c05b20d673c68)), closes [documentos#1290](http://gitlab.meta.com.br/documentos/issues/1290)
* **documentos#1290:** changes API ([84de8d8](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/84de8d8b4460adc9d56d1cceb40e9f8dc3d01dee)), closes [documentos#1290](http://gitlab.meta.com.br/documentos/issues/1290)
* **documentos#1290:** fixes ([93e2f6f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/93e2f6f1ca671d12b34b2f21fbd7c531bb4a0ef5)), closes [documentos#1290](http://gitlab.meta.com.br/documentos/issues/1290)
* **documentos#1290:** fixes onboarding web ([bf7068d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/bf7068d2fe2cd278cefb1d8bc4186dc8a40b8fb3))
* **documentos#1323:** changes routes to correct ones ([fe3980d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/fe3980dd6b2f6e0ba924e29c497abdfa876652ae)), closes [documentos#1323](http://gitlab.meta.com.br/documentos/issues/1323)
* wip ([16b3eef](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/16b3eef778dda67fd818f50aaaefb2eb54901e34))





# [0.14.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.12.0-alpha.1...v0.14.0) (2021-03-23)


### Features

* **documentos#1305:** removes birthday from onboarding ([5eda681](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/5eda6815388bc7289d1d33d1e6896faca4e4aa68)), closes [documentos#1305](http://gitlab.meta.com.br/documentos/issues/1305)
* resolve [#1285](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1285) ([c0b4d91](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c0b4d91ccdb7ff7adcaaf87175f1c32fe17b1c48))
* **documentos#1287:** added min and max into timeline chart ([4397278](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4397278c144645bfccd6d7bbd6045816e3e0e4d0)), closes [documentos#1287](http://gitlab.meta.com.br/documentos/issues/1287)
* merge develop ([2731148](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2731148d7380e3ab5c8ec38d198b1773467e4364))
* **documenots#1292:** added new securitie movement route ([63e6408](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/63e6408aaad104ffe36b50634c0c15c823ee7352)), closes [documenots#1292](http://gitlab.meta.com.br/documenots/issues/1292)
* **documenots#1292:** changed get user stat registrations route ([1c4493b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1c4493bb0081d328cad110c75ec06a42c01465f1)), closes [documenots#1292](http://gitlab.meta.com.br/documenots/issues/1292)
* **documentos#1240:** add updateSignature to organzationStore ([283be60](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/283be607a59b92d1c6241dad577db4d557d52de7)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** new route to validate password ([260ac94](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/260ac94e7a6c56b2a791f4218fb6e55c0e49aa59)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** updateSignature calling correctyl ([15b1a4e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/15b1a4e5a824249a89f0e087861d920bda53b20e)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1282:** created state registration service ([07c6e8c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/07c6e8c3ef1464a0d7bcb77cc5b9ecdd5e8ad5e2)), closes [documentos#1282](http://gitlab.meta.com.br/documentos/issues/1282)
* **documentos#1290:** authentication services ([13ea1c5](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/13ea1c51404c6234f95bec84b11c05b20d673c68)), closes [documentos#1290](http://gitlab.meta.com.br/documentos/issues/1290)
* **documentos#1290:** changes API ([84de8d8](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/84de8d8b4460adc9d56d1cceb40e9f8dc3d01dee)), closes [documentos#1290](http://gitlab.meta.com.br/documentos/issues/1290)
* **documentos#1290:** fixes ([93e2f6f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/93e2f6f1ca671d12b34b2f21fbd7c531bb4a0ef5)), closes [documentos#1290](http://gitlab.meta.com.br/documentos/issues/1290)
* **documentos#1290:** fixes onboarding web ([bf7068d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/bf7068d2fe2cd278cefb1d8bc4186dc8a40b8fb3))
* **documentos#1323:** changes routes to correct ones ([fe3980d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/fe3980dd6b2f6e0ba924e29c497abdfa876652ae)), closes [documentos#1323](http://gitlab.meta.com.br/documentos/issues/1323)
* wip ([16b3eef](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/16b3eef778dda67fd818f50aaaefb2eb54901e34))





# [0.13.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.12.0-alpha.1...v0.13.0) (2021-03-23)


### Features

* **documentos#1305:** removes birthday from onboarding ([5eda681](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/5eda6815388bc7289d1d33d1e6896faca4e4aa68)), closes [documentos#1305](http://gitlab.meta.com.br/documentos/issues/1305)
* resolve [#1285](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1285) ([c0b4d91](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c0b4d91ccdb7ff7adcaaf87175f1c32fe17b1c48))
* **documentos#1287:** added min and max into timeline chart ([4397278](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4397278c144645bfccd6d7bbd6045816e3e0e4d0)), closes [documentos#1287](http://gitlab.meta.com.br/documentos/issues/1287)
* merge develop ([2731148](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2731148d7380e3ab5c8ec38d198b1773467e4364))
* **documenots#1292:** added new securitie movement route ([63e6408](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/63e6408aaad104ffe36b50634c0c15c823ee7352)), closes [documenots#1292](http://gitlab.meta.com.br/documenots/issues/1292)
* **documenots#1292:** changed get user stat registrations route ([1c4493b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1c4493bb0081d328cad110c75ec06a42c01465f1)), closes [documenots#1292](http://gitlab.meta.com.br/documenots/issues/1292)
* **documentos#1240:** add updateSignature to organzationStore ([283be60](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/283be607a59b92d1c6241dad577db4d557d52de7)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** new route to validate password ([260ac94](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/260ac94e7a6c56b2a791f4218fb6e55c0e49aa59)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** updateSignature calling correctyl ([15b1a4e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/15b1a4e5a824249a89f0e087861d920bda53b20e)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1282:** created state registration service ([07c6e8c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/07c6e8c3ef1464a0d7bcb77cc5b9ecdd5e8ad5e2)), closes [documentos#1282](http://gitlab.meta.com.br/documentos/issues/1282)
* **documentos#1290:** authentication services ([13ea1c5](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/13ea1c51404c6234f95bec84b11c05b20d673c68)), closes [documentos#1290](http://gitlab.meta.com.br/documentos/issues/1290)
* **documentos#1290:** changes API ([84de8d8](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/84de8d8b4460adc9d56d1cceb40e9f8dc3d01dee)), closes [documentos#1290](http://gitlab.meta.com.br/documentos/issues/1290)
* **documentos#1290:** fixes ([93e2f6f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/93e2f6f1ca671d12b34b2f21fbd7c531bb4a0ef5)), closes [documentos#1290](http://gitlab.meta.com.br/documentos/issues/1290)
* **documentos#1290:** fixes onboarding web ([bf7068d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/bf7068d2fe2cd278cefb1d8bc4186dc8a40b8fb3))
* **documentos#1323:** changes routes to correct ones ([fe3980d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/fe3980dd6b2f6e0ba924e29c497abdfa876652ae)), closes [documentos#1323](http://gitlab.meta.com.br/documentos/issues/1323)
* wip ([16b3eef](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/16b3eef778dda67fd818f50aaaefb2eb54901e34))





# [0.12.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.12.0-alpha.1...v0.12.0) (2021-03-23)


### Features

* **documentos#1305:** removes birthday from onboarding ([5eda681](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/5eda6815388bc7289d1d33d1e6896faca4e4aa68)), closes [documentos#1305](http://gitlab.meta.com.br/documentos/issues/1305)
* resolve [#1285](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1285) ([c0b4d91](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c0b4d91ccdb7ff7adcaaf87175f1c32fe17b1c48))
* **documentos#1287:** added min and max into timeline chart ([4397278](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4397278c144645bfccd6d7bbd6045816e3e0e4d0)), closes [documentos#1287](http://gitlab.meta.com.br/documentos/issues/1287)
* merge develop ([2731148](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2731148d7380e3ab5c8ec38d198b1773467e4364))
* **documenots#1292:** added new securitie movement route ([63e6408](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/63e6408aaad104ffe36b50634c0c15c823ee7352)), closes [documenots#1292](http://gitlab.meta.com.br/documenots/issues/1292)
* **documenots#1292:** changed get user stat registrations route ([1c4493b](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1c4493bb0081d328cad110c75ec06a42c01465f1)), closes [documenots#1292](http://gitlab.meta.com.br/documenots/issues/1292)
* **documentos#1240:** add updateSignature to organzationStore ([283be60](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/283be607a59b92d1c6241dad577db4d557d52de7)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** new route to validate password ([260ac94](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/260ac94e7a6c56b2a791f4218fb6e55c0e49aa59)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1240:** updateSignature calling correctyl ([15b1a4e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/15b1a4e5a824249a89f0e087861d920bda53b20e)), closes [documentos#1240](http://gitlab.meta.com.br/documentos/issues/1240)
* **documentos#1282:** created state registration service ([07c6e8c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/07c6e8c3ef1464a0d7bcb77cc5b9ecdd5e8ad5e2)), closes [documentos#1282](http://gitlab.meta.com.br/documentos/issues/1282)
* **documentos#1290:** authentication services ([13ea1c5](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/13ea1c51404c6234f95bec84b11c05b20d673c68)), closes [documentos#1290](http://gitlab.meta.com.br/documentos/issues/1290)
* **documentos#1290:** changes API ([84de8d8](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/84de8d8b4460adc9d56d1cceb40e9f8dc3d01dee)), closes [documentos#1290](http://gitlab.meta.com.br/documentos/issues/1290)
* **documentos#1290:** fixes ([93e2f6f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/93e2f6f1ca671d12b34b2f21fbd7c531bb4a0ef5)), closes [documentos#1290](http://gitlab.meta.com.br/documentos/issues/1290)
* **documentos#1290:** fixes onboarding web ([bf7068d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/bf7068d2fe2cd278cefb1d8bc4186dc8a40b8fb3))
* **documentos#1323:** changes routes to correct ones ([fe3980d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/fe3980dd6b2f6e0ba924e29c497abdfa876652ae)), closes [documentos#1323](http://gitlab.meta.com.br/documentos/issues/1323)
* wip ([16b3eef](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/16b3eef778dda67fd818f50aaaefb2eb54901e34))





# [0.12.0-alpha.1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.12.0-alpha.0...v0.12.0-alpha.1) (2021-02-22)


### Bug Fixes

* **#1173:** fix function get field monitoration ([b36b5d5](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b36b5d5cf37f2d116ccf7334a7097f1f2fe4b9fe))
* **auth:** fixed multiples refresh token ([b976597](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b976597b48bfce6639d77453f295a8a3437d836a))


### Features

* merge develop ([c7c8f96](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c7c8f96d843e6c6e4edffbcef241c3c9f096257b))
* **documentos#1115:** created barter store and api resource ([9a35185](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9a3518557cc700310607d6920ae6f739a4733bb1)), closes [documentos#1115](http://gitlab.meta.com.br/documentos/issues/1115)
* **documentos#535:** finished home screen for mobile ([b595eee](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b595eeed9efc2c6371bb04276a22abfd74a1d9db)), closes [documentos#535](http://gitlab.meta.com.br/documentos/issues/535)
* **documentos#947:** added new barter routes ([a6822b8](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a6822b87f205b32733f20f0b439655ee4d94e9f7)), closes [documentos#947](http://gitlab.meta.com.br/documentos/issues/947)
* wip ([452dd2d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/452dd2d1eaeeddea616d23ca757bcba9864baab8))
* **documentos#987:** add milk quality to store ([dd7384c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/dd7384c01c3f6f573345b8c13d7953e7c70f4625)), closes [documentos#987](http://gitlab.meta.com.br/documentos/issues/987)
* **documentos#987:** response manipulation on dairyFarm resources ([68ce277](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/68ce27712df23018a9627cdc29c4abc3c32065cc)), closes [documentos#987](http://gitlab.meta.com.br/documentos/issues/987)





# [0.12.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.11.0...v0.12.0-alpha.0) (2021-02-12)

**Note:** Version bump only for package @smartcoop/services





# [0.11.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.11.0-alpha.0...v0.11.0) (2021-02-12)


### Features

* **documentos#1197:** dashboard service ([e73c70f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e73c70f9e67eb07afca145e57b945250c6c62527)), closes [documentos#1197](http://gitlab.meta.com.br/documentos/issues/1197)
* resolve [#1117](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1117) ([f295d11](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f295d11daa1e6f8a733392d15ee431cc8a0db78e))
* resolve [#1117](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1117) ([28e2c23](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/28e2c2337354377dcc21ce4b40037525a7ab0900))





# [0.11.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.10.0...v0.11.0-alpha.0) (2021-02-05)

**Note:** Version bump only for package @smartcoop/services





# [0.10.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.10.0-alpha.0...v0.10.0) (2021-02-01)


### Bug Fixes

* **firebase:** fixed on web ([e048482](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e04848292e5f0c5b775a497e364b39ec0ea3cf8d))


### Features

* **#1004:** adjustments ([cd86db0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/cd86db0251fd3ae7c8091fb957696cedbf822053)), closes [#1004](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1004)
* **#1004:** implement edit technical visit ([f8f20c2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f8f20c25a9c93631192366dfed0931dda25aa13d)), closes [#1004](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/1004) [#275](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/275)
* **auth:** added login by cpf/cnpj ([3fb8616](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3fb8616f1773e6686c4be2a5a4893884ab86b84d))
* **documentos#102:** avatar related services ([85c2c1a](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/85c2c1ab8e2408b5b6b7461967e14c8162a730a8)), closes [documentos#102](http://gitlab.meta.com.br/documentos/issues/102)
* **documentos#974:** added products withdraw list in technical ([11a7a23](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/11a7a23a4ee992f6a3ee60b88c35f792c2bfe1b5)), closes [documentos#974](http://gitlab.meta.com.br/documentos/issues/974)
* **documentos#987:** changes resources to align with back ([d1e8784](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d1e878414bcd2b9ef35fe1c7c9b2a2d01ba120b6)), closes [documentos#987](http://gitlab.meta.com.br/documentos/issues/987)
* **documentos#987:** correct resources in dairyfarm ([60e2418](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/60e2418334e45ed80c909090520738107a350f97)), closes [documentos#987](http://gitlab.meta.com.br/documentos/issues/987)
* **documentos#987:** resources that doesnt exist yet ([4ed56b5](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4ed56b51ea95d90db129ef467976752985bbcac8)), closes [documentos#987](http://gitlab.meta.com.br/documentos/issues/987)
* **documentos#987:** updates services ([8a476b8](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8a476b8d66725ed9591f51748fdf639ce2bd1592)), closes [documentos#987](http://gitlab.meta.com.br/documentos/issues/987)
* **doucmentos#987:** fixes in resouces and duck ([2372c5e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2372c5e7ceb653a3b90002064adfaf94f0083ea8))
* resolve merge conflicts ([0646a07](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0646a07c03edcc03fe7ad4c3b58200a3fdaf5369))
* resolve merge conflicts ([d76333d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d76333db93b61f9897987ee033d050dcdc48d8fe))
* wip ([ea43347](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ea4334798c6090ec6819e6390596359d2bf439de))
* wip ([094d424](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/094d4242f82345f35ecd7b4227e3c497da75c966))





# [0.10.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.9.0...v0.10.0-alpha.0) (2021-01-19)

**Note:** Version bump only for package @smartcoop/services





# [0.9.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.8.2...v0.9.0) (2021-01-19)


### Features

* **#873:** adjustment resource signature ([931a6a2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/931a6a27516e13c8aebfceda9be5ffb4d8b27f1b)), closes [#873](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/873) [#260](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/260)
* **#873:** integration order create and signature electronic ([1f72cb3](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1f72cb3e3cc5d70f80d31c8e4fe48b8658a4ba2f)), closes [#873](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/873) [#260](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/260)
* added sales order delete ([ff4f588](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ff4f588585eb4c76f9a544ee4c650d5551eae85b))
* added sales orders forms ([b1e3fe8](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b1e3fe88414dba234ec6c9deff8d6cb7c3616f5a))
* changed order form fields ([6bb288d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/6bb288d853c3ad734ec51ff41c58c287bbd668a0))
* merge Develop ([13f52cf](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/13f52cfce97d5e68ce271d48dc7e493ee065e28c))
* **#862:** adjustment register and edit field ([4aa85af](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4aa85af740e380188f806588a53dbe96b006fb55)), closes [#862](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/862)
* **#901:** create actions for price history ([a60b133](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a60b13355f3b721a6c056f893c82a3838eeb1401)), closes [#901](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/901) [#241](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/241)
* **documentos#299:** adds pestReport service ([54b032e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/54b032eb317ac2787c72c082b0678e648a8ab47b)), closes [documentos#299](http://gitlab.meta.com.br/documentos/issues/299)
* **documentos#299:** changes in delete service ([c303f67](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c303f6794dbf0c8b28640d92ac3d90cfc3429825)), closes [documentos#299](http://gitlab.meta.com.br/documentos/issues/299)
* **documentos#299:** fix service ([72bbeff](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/72bbeff2ee0514c8574a6d1792380a56adfbbc6e))
* **documentos#299:** service to get report types ([89d0c53](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/89d0c53033d45c06cb2362ca0a50adb1c6bc1fdd)), closes [documentos#299](http://gitlab.meta.com.br/documentos/issues/299)
* **documentos#299:** updated pestReport service ([45884f4](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/45884f4fc0d863546caac6d1bfeeaa1f28ec8a17)), closes [documentos#299](http://gitlab.meta.com.br/documentos/issues/299)
* added settlement date integration ([a76eb57](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/a76eb578c4f2ae8ea26e2b9d02729f11d45d3137))
* resolve [#920](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/920) ([d4f7839](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d4f783957ef1027d031ad535835c3b1f9f75a7a3))
* resolve MR Upgrades ([c1feab5](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c1feab5ec5d26f22bf59577300636fa675cfffb9))
* **documentos#491:** changed sales orders list route ([fa13541](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/fa13541dfe00c2d855c50b7c1903e828ddadbacb)), closes [documentos#491](http://gitlab.meta.com.br/documentos/issues/491)
* **documentos#836:** created notifications list ([f062230](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f062230640b49f3df88ad8a2a6ec5b447913dc69)), closes [documentos#836](http://gitlab.meta.com.br/documentos/issues/836)
* **documentos#836:** finished mobile list ([6cbf8b3](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/6cbf8b38ee2ef54367d0503f607dc38386ca56e8)), closes [documentos#836](http://gitlab.meta.com.br/documentos/issues/836)
* **documentos#836:** firebase login and logout for mobile ([fbd31c4](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/fbd31c47a6b6a9d1d255945a6091b0376aa31fbb)), closes [documentos#836](http://gitlab.meta.com.br/documentos/issues/836)
* **geotiff download:** added button to download a .tiff satelite image ([4ec244c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4ec244cc57d41825fe39934d8c191b0b152bc621))
* merge develop ([5b164a0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/5b164a012ca32970cb8bf82724781871ab529e58))
* rebase with develop ([da7e90e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/da7e90e31d9e8205abbd429866bcb9956f34a0ca))
* resolve [#805](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/805) ([1f50f3d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1f50f3db7106888842074940651e632681517f6f))
* resolve Monte's Comments on MR ([b5a6fc7](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b5a6fc7cf86e92d2d0c005b4f75349741f2c2722))
* wIP ([d6f6459](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d6f64598b3e0b34fc8d6eb6c3672c366beb22afe))





## [0.8.2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.8.1...v0.8.2) (2021-01-06)


### Features

* changed securities movement routes ([23b15c7](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/23b15c7672093782869b9e050e4069438e3b5025))
* **documentos#725:** add delete to service ([2add0d4](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2add0d4ec69dcd0d41f68850ba21d323ce6c660d)), closes [documentos#725](http://gitlab.meta.com.br/documentos/issues/725)
* **documentos#725:** fixes rebase stuff, adds temp fake pagination size ([ce9f67c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ce9f67c49bc98b997964bc483853b05fac99ca6d))
* created sales order forms ([74422e5](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/74422e59f4220564a57c8406580c3bbd9622b1e5))





## [0.8.1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.9.0-alpha.0...v0.8.1) (2021-01-06)

**Note:** Version bump only for package @smartcoop/services





# [0.9.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.8.0...v0.9.0-alpha.0) (2021-01-05)

**Note:** Version bump only for package @smartcoop/services





# [0.8.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.8.0-alpha.0...v0.8.0) (2021-01-05)


### Features

* **documentos#491:** create order form ([2b8653f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/2b8653f777465d35bc1075bcc7578d4259e88863)), closes [documentos#491](http://gitlab.meta.com.br/documentos/issues/491)
* **permissions:** accessing modules by permissions ([1d44dcc](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1d44dcc57ccf9ef37df1708b542e070b0a03a84e))
* added property list route ([8d724d8](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8d724d8ba2ac4f919e37943b0a1ea1df51196166))
* added route to get product quotation ([610f344](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/610f344b0f396c77879874e88145d047d73c83be))
* **docuementos#491:** created sales orders actions ([e1e7778](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e1e7778ef63bae23f81f15e0040540a8944a037f)), closes [docuementos#491](http://gitlab.meta.com.br/docuementos/issues/491)
* **documentos#176 documentos#143:** product balance and withdraw list ([076dae2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/076dae26e34aecb1a671e86e4d40eb574521c4db)), closes [documentos#176](http://gitlab.meta.com.br/documentos/issues/176) [documentos#143](http://gitlab.meta.com.br/documentos/issues/143)
* **documentos#27:** adds request to cropsmanagement in Promises array ([fdcb277](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/fdcb277a8956bc7c28e672ae918cae4ea7085448)), closes [documentos#27](http://gitlab.meta.com.br/documentos/issues/27)
* **documentos#300:** created refresh token mechanism ([54ee2f1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/54ee2f1626aee3e6c60d7a1412305fce7fef5d01)), closes [documentos#300](http://gitlab.meta.com.br/documentos/issues/300)
* **documentos#492:** adds button to receive ([6785be2](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/6785be284961801f320a075216e11ba853fcfe1a)), closes [documentos#492](http://gitlab.meta.com.br/documentos/issues/492)
* **documentos#501:** created create organization form ([b8033cc](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/b8033cc060e4ac231d8eb8a90955d047012b2a73)), closes [documentos#501](http://gitlab.meta.com.br/documentos/issues/501)
* **documentos#501:** expands organization service ([205e7f4](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/205e7f4c72baa47de65de75cec4e7709f338129b)), closes [documentos#501](http://gitlab.meta.com.br/documentos/issues/501)
* **documentos#501:** image upload as binary ([9ba2175](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9ba2175289f2050021a42407b2390de9c358ebae)), closes [documentos#501](http://gitlab.meta.com.br/documentos/issues/501)
* **documentos#502:** bank slips services ([781b0bc](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/781b0bcad4e1b5cdfc958a0bed36d2166b62ebec)), closes [documentos#502](http://gitlab.meta.com.br/documentos/issues/502)
* **documentos#506:** get/post in machinery ([4ea1924](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4ea19241b551222ff372c162a0388d78b5365e96)), closes [documentos#506](http://gitlab.meta.com.br/documentos/issues/506)
* **documentos#688:** added chart to web ([84a8fcb](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/84a8fcb47c4d7a12a798460379ecc025c71806fb)), closes [documentos#688](http://gitlab.meta.com.br/documentos/issues/688)
* **documentos#702:** new services ([e34ef3f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e34ef3f68dd79eaa4b3c38f88234b346e6ba7c64)), closes [documentos#702](http://gitlab.meta.com.br/documentos/issues/702)
* created sales orders list ([294f2f3](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/294f2f3f7a9181f5bf44c0ec4b49d4b9fc50538e))
* **documentos#71:** created securities movement list screen ([ff7f6f8](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ff7f6f8d33c16e6468eab269a9f4384b95cde697)), closes [documentos#71](http://gitlab.meta.com.br/documentos/issues/71)





# [0.8.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.7.0...v0.8.0-alpha.0) (2020-12-11)

**Note:** Version bump only for package @smartcoop/services





# [0.7.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.7.0-alpha.0...v0.7.0) (2020-12-11)


### Bug Fixes

* **documentos#440:** showing backend message errors ([e3d55ec](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e3d55ec742957bdcfd313571e6336d58cdaee507)), closes [documentos#440](http://gitlab.meta.com.br/documentos/issues/440)
* **documentos#475:** documentos[#476](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/476) ([dd92020](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/dd92020797be5bb1c15d306d85765a006fca800a)), closes [documentos#475](http://gitlab.meta.com.br/documentos/issues/475)
* **satelites:** removido apontamento para localhost ([d4bc3d1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/d4bc3d147b36ad068031bb4f740b15d13f6c8871))


### Features

* **documentos#103:** added filed list by property ([e6ed808](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/e6ed8083b58835813c249666bcb8f5dd835f6add)), closes [documentos#103](http://gitlab.meta.com.br/documentos/issues/103)
* **documentos#195:** added new supplier quotation routes and store ([9edf339](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/9edf339e57e5abc1875044736f2315a3f54ef684)), closes [documentos#195](http://gitlab.meta.com.br/documentos/issues/195)
* **documentos#195:** fixed supplier quotation service ([98e8e0f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/98e8e0f1134dcf49ed1352957c3738c5d80a8c32))
* **documentos#223:** created best proposal store ([ac3e08e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ac3e08e528051df53d0c1a098af8fe26135a6b42)), closes [documentos#223](http://gitlab.meta.com.br/documentos/issues/223)
* **documentos#233:** changed refuse supplier quotation proposal ([13b8177](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/13b817728230a1b564bf934c8df7ff374ea9381c)), closes [documentos#233](http://gitlab.meta.com.br/documentos/issues/233)
* **documentos#233:** validations. fixes and tweaks ([811b105](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/811b105c8eea3fcfbdd07a1858f857874c372620))
* **documentos#262:** adds methods to accept and reject a proposal ([ba665d1](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ba665d1787208df0dc8203fea6df6a598dd19457)), closes [documentos#262](http://gitlab.meta.com.br/documentos/issues/262)
* **documentos#262:** creates services ([367101c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/367101c4000b937e36e9082cffd0fed8d83c904c)), closes [documentos#262](http://gitlab.meta.com.br/documentos/issues/262)
* **documentos#402:** editing growing season ([321da9c](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/321da9cb75b830d048c43afa491b4f2911e58b03)), closes [documentos#402](http://gitlab.meta.com.br/documentos/issues/402)





# [0.7.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.6.0...v0.7.0-alpha.0) (2020-11-27)

**Note:** Version bump only for package @smartcoop/services





# [0.6.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.5.3...v0.6.0) (2020-11-27)


### Features

* **documentos#107:** created products quotation service ([3f1f521](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/3f1f521231ef1de224919a1d03a559a92028552f)), closes [documentos#107](http://gitlab.meta.com.br/documentos/issues/107)
* **documentos#107:** populates list ([5dadeeb](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/5dadeeb5ce4132191ef29a9fe73b3667ba4b2ca6)), closes [documentos#107](http://gitlab.meta.com.br/documentos/issues/107)
* **documentos#108:** creates get accounts ([808e4b0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/808e4b02ad219e55f0c0a6b62a1c80136c0f4fce)), closes [documentos#108](http://gitlab.meta.com.br/documentos/issues/108)
* **documentos#108:** creates storage of accounts and accounts balance ([65f1beb](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/65f1beb25e36efa0f3fb9c2919ad41907cdc87a5)), closes [documentos#108](http://gitlab.meta.com.br/documentos/issues/108)
* **documentos#16:** created crop resource ([78600e9](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/78600e949f802f75eed2117c1fb5005adbe0643b)), closes [documentos#16](http://gitlab.meta.com.br/documentos/issues/16)
* **documentos#16:** created cultivations goal resource ([4e1dfcb](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4e1dfcbb331c7b8afa9972c11871f995d21f3fe4)), closes [documentos#16](http://gitlab.meta.com.br/documentos/issues/16)
* **documentos#16:** created growing season resource ([c1476c8](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c1476c8311e53b345eef9ce0bb4cf4505b7ae8ad)), closes [documentos#16](http://gitlab.meta.com.br/documentos/issues/16)
* **documentos#194:** integrated supplier quotation list ([97d2c4d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/97d2c4dbd9b706dfbf949a64f5d4ae6acd3ec778)), closes [documentos#194](http://gitlab.meta.com.br/documentos/issues/194)
* **documentos#194:** updates products quotation service ([ee0945f](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/ee0945f5ccff88207c05cdd5cc0018ab2eb8e8b5)), closes [documentos#194](http://gitlab.meta.com.br/documentos/issues/194)



# [0.6.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.5.0...v0.6.0-alpha.0) (2020-11-10)





# [0.6.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.5.0...v0.6.0-alpha.0) (2020-11-10)

**Note:** Version bump only for package @smartcoop/services





# [0.5.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.5.0-alpha.0...v0.5.0) (2020-11-10)


### Bug Fixes

* **documentos#10:** getting user by birthdate from first access ([62cc3bf](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/62cc3bfe33a357c259982f16ed0ff17d72bf3131)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)


### Features

* **documentos#127:** filtering list by product ([8349c62](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/8349c62ec4983477093e51c92ceab516069977d9)), closes [documentos#127](http://gitlab.meta.com.br/documentos/issues/127)
* **documentos#93:** added edit join and exit order, in order store ([c3178ab](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/c3178ab7bd1b3a42bd480aec6f74d2c84ae33be9)), closes [documentos#93](http://gitlab.meta.com.br/documentos/issues/93)
* **documentos#93:** join order ([068d657](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/068d6571483eea2adc35e93596cd8e7023ce76f1)), closes [documentos#93](http://gitlab.meta.com.br/documentos/issues/93)





# [0.5.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.4.0...v0.5.0-alpha.0) (2020-10-27)

**Note:** Version bump only for package @smartcoop/services





# [0.4.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.3.0-alpha.0...v0.4.0) (2020-10-27)


### Features

* **documentos#25:** created field store ([0373246](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0373246f26c06e6d33e61ec5bdbbce10822a0c35)), closes [documentos#25](http://gitlab.meta.com.br/documentos/issues/25)
* **documentos#49:** created DataTable async for web ([609c7cb](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/609c7cb05d0df3503703363912d3bde9ede825b4)), closes [documentos#49](http://gitlab.meta.com.br/documentos/issues/49)
* **documentos#49:** created order api resource ([33ca865](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/33ca8655c24288aa24e46c74277c9cd5c2137c4f)), closes [documentos#49](http://gitlab.meta.com.br/documentos/issues/49)





# [0.3.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.2.0...v0.3.0-alpha.0) (2020-09-28)

**Note:** Version bump only for package @smartcoop/services





# [0.2.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.2.0-alpha.0...v0.2.0) (2020-09-28)


### Bug Fixes

* **documentos#10:** passing cpf to create verify code ([f08ed72](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/f08ed722af228e918abb36980c41d703ddf4762b)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)


### Features

* **documentos#10:** changing reset code because api changed ([42d201d](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/42d201d983b0fd1be00356119205b6ad56034d97)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#10:** created web screens ([496ac64](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/496ac645455e7c7e127a68385e9d8fa8b3b4b9f6)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#10:** finished productor onboarding ([4eb4a89](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/4eb4a89ed42ba9a8161999c29bf941a95c5c1119)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#32:** added database into web ([1f6ad73](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1f6ad73cc2f1de976bce6aab7835337f1f8d2e05)), closes [documentos#32](http://gitlab.meta.com.br/documentos/issues/32)
* **documentos#32:** created service smartcoopApi ([0568f87](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/0568f873e9d2f0d1fccde3f53d4c29651222cadd)), closes [documentos#32](http://gitlab.meta.com.br/documentos/issues/32)





# [0.2.0-alpha.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.1.0...v0.2.0-alpha.0) (2020-09-13)

**Note:** Version bump only for package @smartcoop/services





# [0.1.0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/compare/v0.1.0-alpha.0...v0.1.0) (2020-09-13)


### Bug Fixes

* **services:** fix imports exports ([1cdb001](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/1cdb001b1695662347659cc6d60cd8332794a7b8))


### Features

* **documentos#10:** added connection to ibge api documentos[#51](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/51) ([36431e0](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/36431e0da48a7a9c02f3f1ea38228468d811405b)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10)
* **documentos#10:** created InputCep for mobile and web ([fb3753e](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/commit/fb3753e38e6a918c2a11d8b10979bc58f684b612)), closes [documentos#10](http://gitlab.meta.com.br/documentos/issues/10) [#6](http://gitlab.meta.com.br/smartcoop/source/front-monorepo/issues/6)
